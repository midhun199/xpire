using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Biz_Maxx
{
    public partial class frmBillDialog : Form
    {
        public frmBillDialog()
        {
            InitializeComponent();
            this.DialogResult = DialogResult.None;
        }

        public string vPending 
        {
            get
            {
                if (this.DialogResult == DialogResult.None) { return string.Empty; }
                else { return cmbType.Text.Substring(0, 1); }
            }
        }

        private void frmBillDialog_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void frmBillDialog_Load(object sender, EventArgs e)
        {
            cmbType.SelectedIndex = 0;

           
        }
    }   
 }
