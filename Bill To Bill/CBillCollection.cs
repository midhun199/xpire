using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace Xpire.BillToBill
{
    class CBillCollection
    {
        # region Private Declarations

        private DateTime _date;
        private string _transType;
        private string _transNo;
        private string _transSeries;
        private string _transFlag;
        private string _orgSeries = string.Empty;
        private string _custCode;
        private string _firmCode;
        private decimal _billAmt;
        private CBillToBill.module _module;
        private CBillToBill.mode _mode;
        private CBillToBill.dataedit _dataedit; 
        private Boolean _delAllEntry;        

        private SqlCommand _cmd = null;

        public  List<CBillToBill> _cBill = new List<CBillToBill>();

        #endregion
       

        # region Propertices

        public DateTime Date
        {
            set { _date = value; }
        }

        public string TransType
        {
            set { _transType = value; }
        }
        public string TransNo
        {
            set { _transNo = value; }
        }
        public string TransSeries
        {
            set { _transSeries = value; }
        }
        public string TransFlag
        {
            set { _transFlag = value; }
        }
        public string FirmCode
        {
            set { _firmCode = value; }
        }
        public string CustCode
        {
            set { _custCode = value; }
        }
        public string OrgSeries
        {
            set { _orgSeries = value; }
        }

        public decimal BillAmt
        {
            set { _billAmt = value; }
        }

        public SqlCommand Cmd
        {
            set { _cmd = value; }
        }

        public CBillToBill.mode Mode
        {
            set { _mode = value; }
        }

        public CBillToBill.module Module
        {
            set { _module = value; }
        }

        public CBillToBill.dataedit Dataedit
        {
            set { _dataedit = value; }
        }

        public Boolean DelAllEntry
        {
            set { _delAllEntry = value; }
        }

        #endregion
        
        public CBillCollection() 
        {
            
        }

        public void ShowSettlement() 
        {
            foreach (CBillToBill cBill in _cBill) 
            {
                if (cBill.CustCode == _custCode)
                {
                    cBill.Dataedit = _dataedit; 
                    cBill.Cmd = _cmd;
                    cBill.BillAmt = _billAmt;
                    cBill.OrgSeries = _orgSeries;
                    cBill.ShowSettlement();
                    return;
                }
            }

            CBillToBill cBil = new CBillToBill();
            cBil.Cmd = _cmd;
            cBil.CustCode = _custCode;
            cBil.BillAmt = _billAmt;
            cBil.Date = _date;
            cBil.TransNo = _transNo;
            cBil.FirmCode = _firmCode;
            cBil.TransType = _transType;
            cBil.TransFlag = _transFlag;
            cBil.OrgSeries = _orgSeries;
            cBil.Mode = _mode;
            cBil.Module = _module;
            cBil.Dataedit = _dataedit; 
            if (cBil.ShowSettlement())
                _cBill.Add(cBil);
        }

        public void SavePendingBill()
        {
            if (_mode != CBillToBill.mode.Entry)
            {
                _delAllEntry = true;
            }
            if (_mode == CBillToBill.mode.Cancel)
            {
                CBillToBill bill = new CBillToBill();
                bill.TransNo = _transNo;
                bill.TransType = _transType;
                bill.TransFlag = _transFlag;
                bill.FirmCode = _firmCode;
                bill.Mode = _mode;
                bill.Module = _module;
                _cBill.Add(bill);
            }
            foreach (CBillToBill cBill in _cBill)
            {
                cBill.Cmd = _cmd;
                cBill.TransNo = _transNo;
                cBill.DelAllEntry = _delAllEntry;
                cBill.SavePendingBill();
                _delAllEntry = false;
                if (_mode == CBillToBill.mode.Cancel) { return; }
            }
        }


    }
}
