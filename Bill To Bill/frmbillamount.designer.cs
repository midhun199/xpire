namespace Biz_Maxx
{
    partial class frmbillamount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmbillamount));
            this.c1FlexGrid1 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.txtbillamount = new System.Windows.Forms.TextBox();
            this.lblbalancegrid = new System.Windows.Forms.Label();
            this.lblsplit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblstaus = new System.Windows.Forms.Label();
            this.txtsplitamount = new System.Windows.Forms.TextBox();
            this.txtdifference = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // c1FlexGrid1
            // 
            this.c1FlexGrid1.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.c1FlexGrid1.BackColor = System.Drawing.Color.White;
            this.c1FlexGrid1.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.FixedSingle;
            this.c1FlexGrid1.ColumnInfo = resources.GetString("c1FlexGrid1.ColumnInfo");
            this.c1FlexGrid1.ExtendLastCol = true;
            this.c1FlexGrid1.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.c1FlexGrid1.KeyActionEnter = C1.Win.C1FlexGrid.KeyActionEnum.None;
            this.c1FlexGrid1.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross;
            this.c1FlexGrid1.Location = new System.Drawing.Point(7, 0);
            this.c1FlexGrid1.Name = "c1FlexGrid1";
            this.c1FlexGrid1.Rows.MinSize = 22;
            this.c1FlexGrid1.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.c1FlexGrid1.ShowSort = false;
            this.c1FlexGrid1.Size = new System.Drawing.Size(990, 420);
            this.c1FlexGrid1.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("c1FlexGrid1.Styles"));
            this.c1FlexGrid1.TabIndex = 0;
            this.c1FlexGrid1.EnterCell += new System.EventHandler(this.c1FlexGrid1_EnterCell);
            this.c1FlexGrid1.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.c1FlexGrid1_SetupEditor);
            this.c1FlexGrid1.ValidateEdit += new C1.Win.C1FlexGrid.ValidateEditEventHandler(this.c1FlexGrid1_ValidateEdit);
            this.c1FlexGrid1.KeyDownEdit += new C1.Win.C1FlexGrid.KeyEditEventHandler(this.c1FlexGrid1_KeyDownEdit);
            this.c1FlexGrid1.KeyPressEdit += new C1.Win.C1FlexGrid.KeyPressEditEventHandler(this.c1FlexGrid1_KeyPressEdit);
            this.c1FlexGrid1.Click += new System.EventHandler(this.c1FlexGrid1_Click);
            this.c1FlexGrid1.Enter += new System.EventHandler(this.c1FlexGrid1_Enter);
            // 
            // txtbillamount
            // 
            this.txtbillamount.BackColor = System.Drawing.Color.White;
            this.txtbillamount.Enabled = false;
            this.txtbillamount.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtbillamount.ForeColor = System.Drawing.Color.Black;
            this.txtbillamount.Location = new System.Drawing.Point(806, 8);
            this.txtbillamount.MaxLength = 13;
            this.txtbillamount.Name = "txtbillamount";
            this.txtbillamount.Size = new System.Drawing.Size(141, 23);
            this.txtbillamount.TabIndex = 19;
            this.txtbillamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblbalancegrid
            // 
            this.lblbalancegrid.AutoSize = true;
            this.lblbalancegrid.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbalancegrid.Location = new System.Drawing.Point(693, 11);
            this.lblbalancegrid.Name = "lblbalancegrid";
            this.lblbalancegrid.Size = new System.Drawing.Size(105, 16);
            this.lblbalancegrid.TabIndex = 18;
            this.lblbalancegrid.Text = "Total Amount";
            // 
            // lblsplit
            // 
            this.lblsplit.AutoSize = true;
            this.lblsplit.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblsplit.Location = new System.Drawing.Point(693, 39);
            this.lblsplit.Name = "lblsplit";
            this.lblsplit.Size = new System.Drawing.Size(101, 16);
            this.lblsplit.TabIndex = 18;
            this.lblsplit.Text = "Split Amount";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(693, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "Difference";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.lblstaus);
            this.panel1.Controls.Add(this.txtsplitamount);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblsplit);
            this.panel1.Controls.Add(this.txtdifference);
            this.panel1.Controls.Add(this.lblbalancegrid);
            this.panel1.Controls.Add(this.txtbillamount);
            this.panel1.Location = new System.Drawing.Point(7, 436);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(993, 93);
            this.panel1.TabIndex = 1;
            // 
            // lblstaus
            // 
            this.lblstaus.AutoSize = true;
            this.lblstaus.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblstaus.Location = new System.Drawing.Point(953, 11);
            this.lblstaus.Name = "lblstaus";
            this.lblstaus.Size = new System.Drawing.Size(24, 16);
            this.lblstaus.TabIndex = 21;
            this.lblstaus.Text = "Dr";
            // 
            // txtsplitamount
            // 
            this.txtsplitamount.BackColor = System.Drawing.Color.White;
            this.txtsplitamount.Enabled = false;
            this.txtsplitamount.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtsplitamount.ForeColor = System.Drawing.Color.Black;
            this.txtsplitamount.Location = new System.Drawing.Point(806, 36);
            this.txtsplitamount.MaxLength = 13;
            this.txtsplitamount.Name = "txtsplitamount";
            this.txtsplitamount.Size = new System.Drawing.Size(141, 23);
            this.txtsplitamount.TabIndex = 19;
            this.txtsplitamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtdifference
            // 
            this.txtdifference.BackColor = System.Drawing.Color.White;
            this.txtdifference.Enabled = false;
            this.txtdifference.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtdifference.ForeColor = System.Drawing.Color.Black;
            this.txtdifference.Location = new System.Drawing.Point(806, 64);
            this.txtdifference.MaxLength = 13;
            this.txtdifference.Name = "txtdifference";
            this.txtdifference.Size = new System.Drawing.Size(141, 23);
            this.txtdifference.TabIndex = 19;
            this.txtdifference.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.BackgroundImage = global::Xpire.Properties.Resources.save1;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Maroon;
            this.btnSave.Location = new System.Drawing.Point(406, 11);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 71);
            this.btnSave.TabIndex = 22;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Maroon;
            this.btnExit.Location = new System.Drawing.Point(482, 11);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 71);
            this.btnExit.TabIndex = 23;
            this.btnExit.TabStop = false;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmbillamount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.ClientSize = new System.Drawing.Size(1008, 541);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.c1FlexGrid1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmbillamount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Amount";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmbillamount_FormClosing);
            this.Load += new System.EventHandler(this.frmbillamount_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmbillamount_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public  System.Windows.Forms.TextBox txtbillamount;
        private System.Windows.Forms.Label lblbalancegrid;
        private System.Windows.Forms.Label lblsplit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        public C1.Win.C1FlexGrid.C1FlexGrid c1FlexGrid1;
        public System.Windows.Forms.TextBox txtsplitamount;
        public System.Windows.Forms.TextBox txtdifference;
        public System.Windows.Forms.Label lblstaus;
        internal System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.Button btnExit;
    }
}