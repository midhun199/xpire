namespace Biz_Maxx
{
    partial class frmopbill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmopbill));
            this.c1FlexGrid1 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.txtseries = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.gpCmd = new System.Windows.Forms.GroupBox();
            this.cmdEdit = new System.Windows.Forms.Button();
            this.cmdAdd = new System.Windows.Forms.Button();
            this.cmdcancel = new System.Windows.Forms.Button();
            this.cmdDelete = new System.Windows.Forms.Button();
            this.lblbalance = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblsetteledamount = new System.Windows.Forms.Label();
            this.lblopeningbalance = new System.Windows.Forms.Label();
            this.lblsettledamnt = new System.Windows.Forms.Label();
            this.lblopbal = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpInvoicedate = new System.Windows.Forms.DateTimePicker();
            this.dtpdate = new System.Windows.Forms.DateTimePicker();
            this.txtbillamount = new System.Windows.Forms.TextBox();
            this.txtInvoice = new System.Windows.Forms.TextBox();
            this.txtbillno = new System.Windows.Forms.TextBox();
            this.cmbtranstype = new System.Windows.Forms.ComboBox();
            this.lblinvdate = new System.Windows.Forms.Label();
            this.lblfinyear = new System.Windows.Forms.Label();
            this.lbldate = new System.Windows.Forms.Label();
            this.lblbalancegrid = new System.Windows.Forms.Label();
            this.lblbillno = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbltranstype = new System.Windows.Forms.Label();
            this.lblseries = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).BeginInit();
            this.gpCmd.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // c1FlexGrid1
            // 
            this.c1FlexGrid1.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.c1FlexGrid1.BackColor = System.Drawing.SystemColors.Window;
            this.c1FlexGrid1.ColumnInfo = resources.GetString("c1FlexGrid1.ColumnInfo");
            this.c1FlexGrid1.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.c1FlexGrid1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1FlexGrid1.Location = new System.Drawing.Point(9, 10);
            this.c1FlexGrid1.Name = "c1FlexGrid1";
            this.c1FlexGrid1.Rows.MinSize = 22;
            this.c1FlexGrid1.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.c1FlexGrid1.Size = new System.Drawing.Size(894, 263);
            this.c1FlexGrid1.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("c1FlexGrid1.Styles"));
            this.c1FlexGrid1.TabIndex = 14;
            this.c1FlexGrid1.Click += new System.EventHandler(this.c1FlexGrid1_Click);
            // 
            // txtseries
            // 
            this.txtseries.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtseries.Location = new System.Drawing.Point(133, 42);
            this.txtseries.MaxLength = 1;
            this.txtseries.Name = "txtseries";
            this.txtseries.Size = new System.Drawing.Size(40, 23);
            this.txtseries.TabIndex = 1;
            this.txtseries.Enter += new System.EventHandler(this.txtbillno_Enter);
            this.txtseries.Leave += new System.EventHandler(this.txtseries_Leave);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(427, 70);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(102, 24);
            this.comboBox1.TabIndex = 6;
            // 
            // gpCmd
            // 
            this.gpCmd.Controls.Add(this.cmdEdit);
            this.gpCmd.Controls.Add(this.cmdAdd);
            this.gpCmd.Controls.Add(this.cmdcancel);
            this.gpCmd.Controls.Add(this.cmdDelete);
            this.gpCmd.Location = new System.Drawing.Point(552, 97);
            this.gpCmd.Name = "gpCmd";
            this.gpCmd.Size = new System.Drawing.Size(315, 51);
            this.gpCmd.TabIndex = 8;
            this.gpCmd.TabStop = false;
            // 
            // cmdEdit
            // 
            this.cmdEdit.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEdit.ForeColor = System.Drawing.Color.Blue;
            this.cmdEdit.Location = new System.Drawing.Point(85, 15);
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Size = new System.Drawing.Size(75, 27);
            this.cmdEdit.TabIndex = 1;
            this.cmdEdit.Text = "Edit";
            this.cmdEdit.UseVisualStyleBackColor = true;
            this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAdd.ForeColor = System.Drawing.Color.Blue;
            this.cmdAdd.Location = new System.Drawing.Point(6, 15);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(75, 27);
            this.cmdAdd.TabIndex = 0;
            this.cmdAdd.Text = "Add";
            this.cmdAdd.UseVisualStyleBackColor = true;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cmdcancel
            // 
            this.cmdcancel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdcancel.ForeColor = System.Drawing.Color.Blue;
            this.cmdcancel.Location = new System.Drawing.Point(242, 48);
            this.cmdcancel.Name = "cmdcancel";
            this.cmdcancel.Size = new System.Drawing.Size(67, 27);
            this.cmdcancel.TabIndex = 3;
            this.cmdcancel.Text = "Cancel";
            this.cmdcancel.UseVisualStyleBackColor = true;
            this.cmdcancel.Click += new System.EventHandler(this.cmdcancel_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDelete.ForeColor = System.Drawing.Color.Blue;
            this.cmdDelete.Location = new System.Drawing.Point(164, 15);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(75, 27);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.UseVisualStyleBackColor = true;
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // lblbalance
            // 
            this.lblbalance.Location = new System.Drawing.Point(134, 64);
            this.lblbalance.Name = "lblbalance";
            this.lblbalance.Size = new System.Drawing.Size(175, 14);
            this.lblbalance.TabIndex = 0;
            this.lblbalance.Text = "0.00";
            this.lblbalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.groupBox2.Controls.Add(this.lblsetteledamount);
            this.groupBox2.Controls.Add(this.lblopeningbalance);
            this.groupBox2.Controls.Add(this.lblsettledamnt);
            this.groupBox2.Controls.Add(this.lblbalance);
            this.groupBox2.Controls.Add(this.lblopbal);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Blue;
            this.groupBox2.Location = new System.Drawing.Point(552, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(315, 91);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Balance ";
            // 
            // lblsetteledamount
            // 
            this.lblsetteledamount.Location = new System.Drawing.Point(134, 43);
            this.lblsetteledamount.Name = "lblsetteledamount";
            this.lblsetteledamount.Size = new System.Drawing.Size(175, 14);
            this.lblsetteledamount.TabIndex = 0;
            this.lblsetteledamount.Text = "0.00";
            this.lblsetteledamount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblopeningbalance
            // 
            this.lblopeningbalance.Location = new System.Drawing.Point(134, 22);
            this.lblopeningbalance.Name = "lblopeningbalance";
            this.lblopeningbalance.Size = new System.Drawing.Size(175, 14);
            this.lblopeningbalance.TabIndex = 0;
            this.lblopeningbalance.Text = "0.00";
            this.lblopeningbalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblsettledamnt
            // 
            this.lblsettledamnt.AutoSize = true;
            this.lblsettledamnt.Location = new System.Drawing.Point(6, 43);
            this.lblsettledamnt.Name = "lblsettledamnt";
            this.lblsettledamnt.Size = new System.Drawing.Size(132, 16);
            this.lblsettledamnt.TabIndex = 0;
            this.lblsettledamnt.Text = "Settled Amount   :";
            // 
            // lblopbal
            // 
            this.lblopbal.AutoSize = true;
            this.lblopbal.Location = new System.Drawing.Point(6, 22);
            this.lblopbal.Name = "lblopbal";
            this.lblopbal.Size = new System.Drawing.Size(128, 16);
            this.lblopbal.TabIndex = 0;
            this.lblopbal.Text = "Opening Balance :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Balance               :";
            // 
            // dtpInvoicedate
            // 
            this.dtpInvoicedate.CustomFormat = "dd/MM/yyyy";
            this.dtpInvoicedate.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpInvoicedate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInvoicedate.Location = new System.Drawing.Point(426, 100);
            this.dtpInvoicedate.Name = "dtpInvoicedate";
            this.dtpInvoicedate.Size = new System.Drawing.Size(103, 23);
            this.dtpInvoicedate.TabIndex = 7;
            // 
            // dtpdate
            // 
            this.dtpdate.CustomFormat = "dd/MM/yyyy";
            this.dtpdate.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpdate.Location = new System.Drawing.Point(426, 13);
            this.dtpdate.Name = "dtpdate";
            this.dtpdate.Size = new System.Drawing.Size(103, 23);
            this.dtpdate.TabIndex = 5;
            // 
            // txtbillamount
            // 
            this.txtbillamount.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbillamount.Location = new System.Drawing.Point(132, 100);
            this.txtbillamount.MaxLength = 13;
            this.txtbillamount.Name = "txtbillamount";
            this.txtbillamount.Size = new System.Drawing.Size(211, 23);
            this.txtbillamount.TabIndex = 4;
            this.txtbillamount.Text = "0.00";
            this.txtbillamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtbillamount.Enter += new System.EventHandler(this.txtbillno_Enter);
            this.txtbillamount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbillamount_KeyPress);
            this.txtbillamount.Leave += new System.EventHandler(this.txtbillamount_Leave);
            // 
            // txtInvoice
            // 
            this.txtInvoice.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoice.Location = new System.Drawing.Point(133, 71);
            this.txtInvoice.MaxLength = 20;
            this.txtInvoice.Name = "txtInvoice";
            this.txtInvoice.Size = new System.Drawing.Size(211, 23);
            this.txtInvoice.TabIndex = 3;
            this.txtInvoice.Enter += new System.EventHandler(this.txtbillno_Enter);
            // 
            // txtbillno
            // 
            this.txtbillno.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbillno.Location = new System.Drawing.Point(240, 42);
            this.txtbillno.MaxLength = 6;
            this.txtbillno.Name = "txtbillno";
            this.txtbillno.Size = new System.Drawing.Size(103, 23);
            this.txtbillno.TabIndex = 2;
            this.txtbillno.Enter += new System.EventHandler(this.txtbillno_Enter);
            this.txtbillno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbillno_KeyPress);
            // 
            // cmbtranstype
            // 
            this.cmbtranstype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbtranstype.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbtranstype.FormattingEnabled = true;
            this.cmbtranstype.Items.AddRange(new object[] {
            "Sales",
            "Purchase"});
            this.cmbtranstype.Location = new System.Drawing.Point(132, 12);
            this.cmbtranstype.Name = "cmbtranstype";
            this.cmbtranstype.Size = new System.Drawing.Size(211, 24);
            this.cmbtranstype.TabIndex = 0;
            this.cmbtranstype.SelectedIndexChanged += new System.EventHandler(this.cmbtranstype_SelectedIndexChanged);
            // 
            // lblinvdate
            // 
            this.lblinvdate.AutoSize = true;
            this.lblinvdate.Location = new System.Drawing.Point(363, 104);
            this.lblinvdate.Name = "lblinvdate";
            this.lblinvdate.Size = new System.Drawing.Size(61, 14);
            this.lblinvdate.TabIndex = 0;
            this.lblinvdate.Text = "Inv Date";
            // 
            // lblfinyear
            // 
            this.lblfinyear.AutoSize = true;
            this.lblfinyear.Location = new System.Drawing.Point(364, 75);
            this.lblfinyear.Name = "lblfinyear";
            this.lblfinyear.Size = new System.Drawing.Size(57, 14);
            this.lblfinyear.TabIndex = 0;
            this.lblfinyear.Text = "Fin year";
            // 
            // lbldate
            // 
            this.lbldate.AutoSize = true;
            this.lbldate.Location = new System.Drawing.Point(363, 17);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(37, 14);
            this.lbldate.TabIndex = 0;
            this.lbldate.Text = "Date";
            // 
            // lblbalancegrid
            // 
            this.lblbalancegrid.AutoSize = true;
            this.lblbalancegrid.Location = new System.Drawing.Point(17, 104);
            this.lblbalancegrid.Name = "lblbalancegrid";
            this.lblbalancegrid.Size = new System.Drawing.Size(76, 14);
            this.lblbalancegrid.TabIndex = 0;
            this.lblbalancegrid.Text = "Bill Amount";
            // 
            // lblbillno
            // 
            this.lblbillno.AutoSize = true;
            this.lblbillno.Location = new System.Drawing.Point(189, 46);
            this.lblbillno.Name = "lblbillno";
            this.lblbillno.Size = new System.Drawing.Size(45, 14);
            this.lblbillno.TabIndex = 0;
            this.lblbillno.Text = "Bill No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Invoice Number";
            // 
            // lbltranstype
            // 
            this.lbltranstype.AutoSize = true;
            this.lbltranstype.Location = new System.Drawing.Point(17, 17);
            this.lbltranstype.Name = "lbltranstype";
            this.lbltranstype.Size = new System.Drawing.Size(114, 14);
            this.lbltranstype.TabIndex = 0;
            this.lbltranstype.Text = "Transaction Type";
            // 
            // lblseries
            // 
            this.lblseries.AutoSize = true;
            this.lblseries.Location = new System.Drawing.Point(17, 46);
            this.lblseries.Name = "lblseries";
            this.lblseries.Size = new System.Drawing.Size(46, 14);
            this.lblseries.TabIndex = 0;
            this.lblseries.Text = "Series";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.txtseries);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.gpCmd);
            this.panel1.Controls.Add(this.dtpInvoicedate);
            this.panel1.Controls.Add(this.cmbtranstype);
            this.panel1.Controls.Add(this.dtpdate);
            this.panel1.Controls.Add(this.lblseries);
            this.panel1.Controls.Add(this.txtbillamount);
            this.panel1.Controls.Add(this.lbltranstype);
            this.panel1.Controls.Add(this.txtInvoice);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtbillno);
            this.panel1.Controls.Add(this.lblbillno);
            this.panel1.Controls.Add(this.lblbalancegrid);
            this.panel1.Controls.Add(this.lblinvdate);
            this.panel1.Controls.Add(this.lbldate);
            this.panel1.Controls.Add(this.lblfinyear);
            this.panel1.Location = new System.Drawing.Point(9, 280);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(894, 161);
            this.panel1.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.BackgroundImage = global::Xpire.Properties.Resources.save1;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Maroon;
            this.btnSave.Location = new System.Drawing.Point(363, 442);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 71);
            this.btnSave.TabIndex = 15;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Maroon;
            this.btnExit.Location = new System.Drawing.Point(439, 442);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 71);
            this.btnExit.TabIndex = 20;
            this.btnExit.TabStop = false;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmopbill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.ClientSize = new System.Drawing.Size(913, 512);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.c1FlexGrid1);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmopbill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Opening Bill";
            this.Load += new System.EventHandler(this.frmopbill_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmopbill_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).EndInit();
            this.gpCmd.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1FlexGrid.C1FlexGrid c1FlexGrid1;
        private System.Windows.Forms.Label lblseries;
        private System.Windows.Forms.DateTimePicker dtpdate;
        private System.Windows.Forms.Label lbldate;
        private System.Windows.Forms.Label lblbillno;
        private System.Windows.Forms.Label lbltranstype;
        private System.Windows.Forms.Label lblfinyear;
        private System.Windows.Forms.TextBox txtInvoice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbtranstype;
        private System.Windows.Forms.Label lblinvdate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblopbal;
        private System.Windows.Forms.DateTimePicker dtpInvoicedate;
        public  System.Windows.Forms.Label lblopeningbalance;
        private System.Windows.Forms.Label lblsettledamnt;
        private System.Windows.Forms.Label lblsetteledamount;
        private System.Windows.Forms.Label lblbalance;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cmdAdd;
        private System.Windows.Forms.Button cmdEdit;
        private System.Windows.Forms.Button cmdDelete;
        private System.Windows.Forms.GroupBox gpCmd;
        public System.Windows.Forms.TextBox txtbillno;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox txtbillamount;
        private System.Windows.Forms.Label lblbalancegrid;
        private System.Windows.Forms.TextBox txtseries;
        private System.Windows.Forms.Button cmdcancel;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.Button btnExit;
    }
}