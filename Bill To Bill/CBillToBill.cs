using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using C1.Win.C1FlexGrid;
using Microsoft.VisualBasic;

namespace Xpire.BillToBill
{
    public class CBillToBill
    {
        # region Private Declarations

        private DateTime _date;
        private string _transType;
        private string _transNo;
        private string _transSeries;
        private string _transFlag;
        private string _orgSeries = string.Empty;
        private string _custCode=string.Empty ;        
        private string _firmCode;
        private DateTime _invDate;
        private string _invNo;
        private decimal _billAmt;
        private decimal _DifferenceAmt;
        private module _module;
        private mode _mode;
        private dataedit _dataedit;
        private Boolean _delAllEntry;

        private string _vPending = string.Empty;        
        
        private C1FlexGrid _cfg = new C1FlexGrid();

        private SqlCommand _cmd = null;
        private cConnection _con = new cConnection();
        private cGeneral _cgen = new cGeneral();

        private Boolean _newTransaction = false;

        private frmbillamount objBillAmt = null;

        #endregion

        # region Enum
        
        public enum module
        {
            Inventry,
            Accounts
        };

        public enum mode
        {
            Entry,
            Modify,
            Cancel
        };

        public enum dataedit
        {
            Yes, No
        };

        #endregion

        # region Propertices

        public DateTime Date
        {
            set { _date = value; }
        }
        public DateTime InvDate
        {
            set { _invDate = value; }
        }

        public string TransType
        {            
            set { _transType = value; }
            get { return _transType; }
        }
        public string TransNo
        {
            set { _transNo = value; }
            get { return _transNo; }
        }
        public string TransFlag
        {
            set { _transFlag = value; }
        }
        public string FirmCode
        {
            set { _firmCode = value; }
        }
        public string CustCode
        {
            set { _custCode = value; }
            get { return _custCode; }
        }        
        public string InvNo
        {
            set { _invNo = value; }
        }
        public string OrgSeries
        {
            set { _orgSeries = value; }
        }

        public module Module
        {
            set { _module = value; }
        }
        public mode Mode
        {
            set { _mode = value; }
        }
        public dataedit Dataedit
        {
            set { _dataedit = value; }
        }
        public decimal BillAmt
        {
            set { _billAmt = value; }
            get { return _billAmt; }
        }
        public decimal DifferenceAmt
        {
            set { _DifferenceAmt = value; }
            get { return _DifferenceAmt; }
        }

        public SqlCommand Cmd
        {
            set { _cmd = value; }
        }

        public Boolean DelAllEntry
        {
            set { _delAllEntry = value; }
        }

        
	
        #endregion

        #region Private Methods

        private void setSeries()
        {

            switch (_transType)
            {
                case "H":
                    _orgSeries = string.Empty;
                    break;
                case "U":
                    _orgSeries = string.Empty;
                    _transSeries = "A";
                    break;
                case "S":
                    _orgSeries = _transSeries;
                    break;
                case "N":
                    _orgSeries = _transSeries;
                    //_transSeries = "H";
                    break;
                default:
                    _transSeries = "*";
                    break;
            }
        }

        private PendingBill SetPendingBillValues(PendingBill obj)
        {
            //setSeries();
            obj.TransType = _transType;
            obj.Series = _transSeries;
            obj.OrginalSeries = _orgSeries;
            obj.Orderno = _transNo;
            obj.Flag = _transFlag;
            obj.CustCode = _custCode;
            obj.Date1 = _date;
            
            switch (_transType) 
            {
                case "H":
                case "N": 
                    obj.BillAmt = _billAmt;
                    obj.Balance = _billAmt; 
                    break;
                case "S":
                case "U": 
                    obj.BillAmt = _billAmt * -1;
                    obj.Balance = _billAmt * -1;
                    break;
            }

            obj.Received = 0;
            obj.Bill = "Y";
            obj.Status = cPublic.g_OpDate.ToString("yy/MM/dd").Substring(0,2) + "-" + cPublic.g_ClDate.ToString("yy/MM/dd").Substring(0, 2);
            obj.CrDays = "0";            
            obj.InvDate = _invDate;
            obj.Invno = "";
            obj.Paid = 0;
            obj.Remarks = string.Empty;
            obj.UserId = _cgen.GetUserDet();
            return obj;
        }

        private PendingBill SetPendingBillValues(PendingBill obj,int row)
        {
            setSeries();
            obj.TransType = _transType;
            obj.Series = _transSeries;
            obj.OrginalSeries = _orgSeries;
            obj.Orderno = _transNo;
            obj.Flag = _transFlag;
            obj.CustCode = _custCode;
            obj.Date1 = _date;
            
            switch (_transType)
            {
                case "H":
                case "N":                    
                    obj.BillAmt = Convert.ToDecimal(_cfg[row, "amount"] + "");
                    obj.Balance = Convert.ToDecimal(_cfg[row, "amount"] + "");
                    break;
                case "S":
                case "U":
                    obj.BillAmt = Convert.ToDecimal(_cfg[row, "amount"] + "") * -1;
                    obj.Balance = Convert.ToDecimal(_cfg[row, "amount"] + "") * -1;
                    break;
                default:  //  Accounts
                    if (_orgSeries == "R") 
                    {
                        obj.BillAmt = Convert.ToDecimal(_cfg[row, "amount"] + "");
                        obj.Balance = Convert.ToDecimal(_cfg[row, "amount"] + "");
                    }
                    else if (_orgSeries == "P")
                    {
                        obj.BillAmt = Convert.ToDecimal(_cfg[row, "amount"] + "") * -1;
                        obj.Balance = Convert.ToDecimal(_cfg[row, "amount"] + "") * -1;
                    }
                    break;
            }

            obj.Received = 0;
            obj.Bill = "Y";
            obj.Status = cPublic.g_OpDate.ToString("yy/MM/dd").Substring(0, 2) + "-" + cPublic.g_ClDate.ToString("yy/MM/dd").Substring(0, 2);
            obj.CrDays = "0";
            obj.InvDate = _invDate;
            obj.Invno = "";
            obj.Paid = 0;
            obj.Remarks = string.Empty;
            obj.UserId = _cgen.GetUserDet();
            return obj;
        }

        private PBillSub SetPBillSubValues(PBillSub obj, int row)
        {
            switch(_module)
            {
                case module.Inventry:

                    obj.BillFrom = _transType;
                    obj.BillNo = _transNo;
                    obj.Series = _transSeries;
                    obj.OrginalSeries = _orgSeries;
                    obj.Flag = _transFlag;
                    obj.CustCode = _custCode;
                    obj.DateOfPay = _date;
                    obj.InvDate = _invDate;
                    obj.InvNo = _invNo;
                    switch ((_cfg[row, "TransType"] + "").Trim().ToUpper())
                    {
                        case "S":
                        case "U":
                        case "P":                        
                            obj.AmountGiven = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                            obj.AmountPaid = 0;
                            break;
                        case "H":
                        case "N":
                        case "R":
                            obj.AmountGiven = 0;
                            obj.AmountPaid = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                            break;
                        case "J":
                            if ((_cfg[row, "OrgSeries"] + "").Trim().ToUpper() == "R") 
                            {
                                obj.AmountGiven = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                                obj.AmountPaid = 0;
                            }
                            else if ((_cfg[row, "OrgSeries"] + "").Trim().ToUpper() == "P") 
                            {
                                obj.AmountGiven = 0;
                                obj.AmountPaid = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                            } 
                            break;
                    }
                    obj.Balance = 0;
                    obj.BillAmt = _billAmt;
                    obj.Paid = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                    obj.Status = cPublic.g_OpDate.ToString("yy/MM/dd").Substring(0, 2) + "-" + cPublic.g_ClDate.ToString("yy/MM/dd").Substring(0, 2);
                    obj.ModiUser = _cgen.GetUserDet();                    
                    obj.TransNo = _cfg[row, "BillNo"] + "";
                    obj.TransType = _cfg[row, "TransType"] + "";
                    obj.TransSeries = _cfg[row, "trans_series"] + "";
                    obj.TransFlag = _cfg[row, "trans_flag"] + "";
                    obj.Type = "S";
                    obj.UserId = _cgen.GetUserDet();
                    break;

                case module.Accounts:

                    obj.BillFrom = _cfg[row, "TransType"] + "";
                    obj.BillNo = _cfg[row, "BillNo"] + "";
                    obj.Series = _cfg[row, "series"] + "";
                    obj.OrginalSeries = _cfg[row, "OrgSeries"] + "";
                    obj.Flag = _cfg[row, "flag"] + "";
                    obj.CustCode = _custCode;
                    obj.DateOfPay = _date;
                    if (Information.IsDate(_cfg[row, "InvDate"]))
                    { obj.InvDate = Convert.ToDateTime(_cfg[row, "InvDate"]); }
                    obj.InvNo = _cfg[row, "InvNo"] + "";
                    switch (_transType)
                    {
                        case "S":
                        case "U":
                        case "P":
                            obj.AmountGiven = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                            obj.AmountPaid = 0;
                            break;
                        case "H":
                        case "N":
                        case "R":
                            obj.AmountGiven = 0;
                            obj.AmountPaid = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                            break;
                        case "J":
                            if (_orgSeries == "R")
                            {
                                obj.AmountGiven = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                                obj.AmountPaid = 0;
                            }
                            else if (_orgSeries == "P")
                            {
                                obj.AmountGiven = 0;
                                obj.AmountPaid = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                            }
                            break;
                    }
                    obj.Balance = Convert.ToDecimal(_cfg[row, "Balance"] + "") - Convert.ToDecimal(_cfg[row, "Amount"] + "");
                    obj.BillAmt = Convert.ToDecimal(_cfg[row, "BillAmt"] + "");
                    obj.Paid = Convert.ToDecimal(_cfg[row, "Amount"] + "");
                    obj.Status = _cfg[row, "Year"] + "";
                    obj.ModiUser = _cgen.GetUserDet();
                    obj.TransNo = _transNo;
                    obj.TransType = _transType;
                    obj.TransSeries = _orgSeries;
                    obj.TransFlag = _transFlag;
                    obj.Type = string.Empty;
                    obj.UserId = _cgen.GetUserDet();
                    break;                
            }
            return obj;
        }

        private Boolean CheckAccount() 
        {
            _cmd.CommandText = "select bill from accounts" + _firmCode + " where code=@code";
            _cmd.CommandType = CommandType.Text;
            _cmd.Parameters.Clear();
            _cmd.Parameters.AddWithValue("@code", _custCode);
            if (_cmd.ExecuteScalar() + "" == "Y") { return true; }
            return false;
        }

        private Boolean CheckPendingbill()
        {
            string sql = string.Empty;

            switch (_transType)
            {
                case "S":
                    sql = "select count(a.orderno) from sales" + _firmCode + " a join accounts" + _firmCode + " b on a.custcode=b.code \n"
                    + "and a.billtype=2 and b.bill='Y' where a.orderno=@orderno ";
                    break;
                case "N":
                    sql = "select count(a.orderno) from sreturn" + _firmCode + " a join accounts" + _firmCode + " b on a.custcode=b.code \n"
                    + "and a.billtype=2 and b.bill='Y' where a.orderno=@orderno ";
                    break;
                case "H":
                    sql = "select count(a.orderno) from purchase" + _firmCode + " a join accounts" + _firmCode + " b on a.suppcode=b.code \n"
                    + "and a.type=1 and b.bill='Y' where a.orderno=@orderno";
                    break;
                case "U":
                    sql = "select count(a.orderno) from preturn" + _firmCode + " a join accounts" + _firmCode + " b on a.suppcode=b.code \n"
                    + "and a.type=1 and b.bill='Y' where a.orderno=@orderno";
                    break;
                default:
                    sql = "select count(bill) from accounts" + _firmCode + " where code=@code and bill='Y'";
                    break;
            }

            _cmd.CommandText = sql;
            _cmd.Parameters.Clear();
            _cmd.CommandType = CommandType.Text;
            if (_module == module.Inventry)
            {
                _cmd.Parameters.AddWithValue("@orderno", _transNo);
            }
            else if (_module == module.Accounts)
            { _cmd.Parameters.AddWithValue("@code", _custCode); }

            if (Convert.ToDecimal(_cmd.ExecuteScalar()) == 0)
            { return false; }


            _cmd.CommandText = "select count(*) from voucher" + _firmCode + " where code=@custcode  " + Environment.NewLine
                   + " and transtype=@transtype and transno=@transno ";
            _cmd.CommandType = System.Data.CommandType.Text;
            _cmd.Parameters.Clear();
            _cmd.Parameters.AddWithValue("@custcode", CustCode);
            _cmd.Parameters.AddWithValue("@transtype", TransType);
            _cmd.Parameters.AddWithValue("@transno", TransNo);
            if (Convert.ToInt32(_cmd.ExecuteScalar()) == 0) { return false; }

            return true;
        }

        private Boolean CheckTransType() 
        {
            if (_transType == "H" ||
                _transType == "U" ||
                _transType == "S" ||
                _transType == "N" ||
                _transType == "R" ||
                _transType == "P" ||
                _transType == "D" ||
                _transType == "C" ||
                _transType == "J" ||
                _transType == "V")
            { }
            else { return false; }

            return true;
        }

        private Boolean InsertPendingBill(PendingBill objBill)
        {
            _cmd.Parameters.Clear();
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandText = "SP_PENDINGBILL_INSERT" + _firmCode;
            
            _cmd.Parameters.AddWithValue("@date1", objBill.Date1.ToString("yyyy-MM-dd"));
            _cmd.Parameters.AddWithValue("@custcode", objBill.CustCode);
            _cmd.Parameters.AddWithValue("@transtype", objBill.TransType);
            _cmd.Parameters.AddWithValue("@orderno", objBill.Orderno);
            _cmd.Parameters.AddWithValue("@billamt", objBill.BillAmt);
            _cmd.Parameters.AddWithValue("@series", objBill.Series);
            _cmd.Parameters.AddWithValue("@orginalseries", objBill.OrginalSeries);
            _cmd.Parameters.AddWithValue("@crdays", objBill.CrDays);
            _cmd.Parameters.AddWithValue("@bill", objBill.Bill);
            _cmd.Parameters.AddWithValue("@status", objBill.Status);
            _cmd.Parameters.AddWithValue("@received", objBill.Received);
            _cmd.Parameters.AddWithValue("@balance", objBill.Balance);
            _cmd.Parameters.AddWithValue("@flag", objBill.Flag);
            _cmd.Parameters.AddWithValue("@paid", objBill.Paid);            
            _cmd.Parameters.AddWithValue("@invno", objBill.Invno);
            if (_transType == "H" || _transType == "U")
            { _cmd.Parameters.AddWithValue("@invdate", objBill.InvDate.ToString("yyyy-MM-dd")); }
            else { _cmd.Parameters.AddWithValue("@invdate", DBNull.Value); }
            _cmd.Parameters.AddWithValue("@remarks", objBill.Remarks);
            _cmd.Parameters.AddWithValue("@userid", objBill.UserId);
            _cmd.ExecuteNonQuery();
            return true;
        }        

        private Boolean InsertPBillSub(PBillSub objBillSub)
        {
            _cmd.Parameters.Clear();
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandText = "SP_PBILLSUB_INSERT" + _firmCode;   
         
            _cmd.Parameters.AddWithValue("@custcode", objBillSub.CustCode);
            _cmd.Parameters.AddWithValue("@series", objBillSub.Series);
            _cmd.Parameters.AddWithValue("@orginalseries", objBillSub.OrginalSeries);
            _cmd.Parameters.AddWithValue("@bill_no", objBillSub.BillNo);
            _cmd.Parameters.AddWithValue("@bill_from", objBillSub.BillFrom);
            _cmd.Parameters.AddWithValue("@bill_amount", objBillSub.BillAmt);
            _cmd.Parameters.AddWithValue("@amount_paid", objBillSub.AmountPaid);
            _cmd.Parameters.AddWithValue("@amount_given", objBillSub.AmountGiven);
            _cmd.Parameters.AddWithValue("@balance", objBillSub.Balance);
            _cmd.Parameters.AddWithValue("@status", objBillSub.Status);
            _cmd.Parameters.AddWithValue("@date_of_pay", objBillSub.DateOfPay.ToString("yyyy/MM/dd"));
            _cmd.Parameters.AddWithValue("@trans_no", objBillSub.TransNo);
            _cmd.Parameters.AddWithValue("@trans_type", objBillSub.TransType);
            _cmd.Parameters.AddWithValue("@trans_series", objBillSub.TransSeries);
            _cmd.Parameters.AddWithValue("@trans_flag", objBillSub.TransFlag);
            _cmd.Parameters.AddWithValue("@flag", objBillSub.Flag);
            _cmd.Parameters.AddWithValue("@paid", objBillSub.Paid);
            _cmd.Parameters.AddWithValue("@invno", objBillSub.InvNo);
            if (objBillSub.BillFrom != "H" && objBillSub.BillFrom != "U")
            { _cmd.Parameters.AddWithValue("@invdate", DBNull.Value); }
            else { _cmd.Parameters.AddWithValue("@invdate", objBillSub.InvDate.ToString("yyyy/MM/dd")); }
            _cmd.Parameters.AddWithValue("@modiuser", objBillSub.ModiUser);
            _cmd.Parameters.AddWithValue("@userid", objBillSub.UserId);
            _cmd.Parameters.AddWithValue("@type", objBillSub.Type);
            _cmd.ExecuteNonQuery();
            return true;
        }

        private Boolean DeletePendingBill()
        {
            //_cmd.Parameters.Clear();
            //_cmd.CommandType = CommandType.StoredProcedure;
            //_cmd.CommandText = "SP_DELETE_PBILLSUB" + _firmCode;


            return true;
        }

        private Boolean DeletePBillSub()
        {
            if (!_delAllEntry) { return true; }
            _cmd.Parameters.Clear();
            _cmd.CommandType = CommandType.StoredProcedure;
            _cmd.CommandText = "SP_DELETE_PBILLSUB" + _firmCode;

            _cmd.Parameters.AddWithValue("@trans_no", _transNo);
            _cmd.Parameters.AddWithValue("@trans_type", _transType);
            _cmd.Parameters.AddWithValue("@trans_series", _transSeries);
            _cmd.Parameters.AddWithValue("@Trans_Flag", _transFlag);
            _cmd.Parameters.AddWithValue("@modiuser", _cgen.GetUserDet());
            _cmd.ExecuteNonQuery();
            return true;
        }

        private Boolean ReverseAllBills()
        {
            _cmd.Parameters.Clear();
            _cmd.CommandType = CommandType.Text;
            _cmd.CommandText = "insert into pendingbill" + _firmCode + "([transtype],[series], [orginalseries],[orderno], \n"
            + "[date1],[custcode],[crdays],[billamt], [bill],[received],[balance],[status], \n"
            + "[flag],[paid],invno,invdate,remarks,modified, modiuser, userid) \n"
            + "select trans_type [transtype],case when trans_series='' then '*' else trans_series end [series], \n"
            + "case when amount_paid<>0 and trans_type not in ('S','H','N','U') then 'P' \n"
            + "     when amount_paid=0 and trans_type not in ('S','H','N','U') then 'R' \n"
            + "     else trans_series end [orginalseries], \n"
            + "trans_no [orderno],date_of_pay  [date1],[custcode],0 [crdays],0 [billamt], \n"
            + "'Y' [bill],0 [received], 0 [balance],[status],trans_flag [flag],0 [paid],'' invno, \n"
            + "null invdate,'' remarks,0 modified,'' modiuser,'' userid from pbillsub" + _firmCode + " \n"
            + "where bill_no=@trans_no and bill_from=@trans_type and flag=@trans_flag and series=@trans_series and \n"
            + "recno not in (select  distinct b.recno from pendingbill" + _firmCode + " a join pbillsub" + _firmCode + " b \n"
            + "on a.transtype=b.bill_from and a.orderno=b.bill_no and a.series=b.series and a.flag=b.flag)";
            _cmd.Parameters.AddWithValue("@trans_no", _transNo);
            _cmd.Parameters.AddWithValue("@trans_type", _transType);
            _cmd.Parameters.AddWithValue("@trans_flag", _transFlag);
            _cmd.Parameters.AddWithValue("@trans_series", _transSeries+"");
            _cmd.Parameters.AddWithValue("@modiuser", _cgen.GetUserDet());
            _cmd.ExecuteNonQuery();

            _cmd.CommandText = "update a set a.balance = a.balance+(case when ((a.transtype in ('S','P') or a.orginalseries in ('P') or b.bill_from in ('N'))  and a.billamt=0) or a.billamt>0 then b.amount else b.amount*-1 end), \n"
            + "a.received=(a.received-b.amount),a.paid=0,a.modified=a.modified+1,a.modiuser=@modiuser \n"
            + "from pendingbill" + _firmCode + " a join \n"
            + "( \n"
            + "    select bill_amount,bill_no,series,orginalseries,custcode,status,bill_from,flag,paid[amount] \n"
            + "    from pbillsub" + _firmCode + "   a where trans_no=@Trans_No and trans_type=@Trans_Type \n"
            + "    and trans_series=@trans_series and trans_flag=@trans_flag  \n"
            + "    union all \n"
            + "    select max(bill_amount) bill_amount,bill_no,series,orginalseries,custcode,status,bill_from,flag,isnull(sum(paid),0)[amount] \n"
            + "    from pbillsub" + _firmCode + " where bill_no=@trans_no and bill_from=@trans_type and series=@trans_series \n"
            + "    and flag=@trans_flag group by bill_no,series,orginalseries,custcode,status,bill_from,flag \n"
            + " ) b \n"
            + " on a.custcode=b.custcode and a.orderno=b.bill_no and a.transtype=b.bill_from and \n"
            + " a.series=case when (b.orginalseries='R' or b.orginalseries='P') then a.series else b.series end \n"
            + " and a.orginalseries=b.orginalseries and a.status=b.status and a.flag=b.flag";
            _cmd.ExecuteNonQuery();

            _cmd.CommandText = "update a set a.billamt=a.billamt+(case when ((a.transtype in ('S','P') or a.orginalseries in ('P'))  and a.billamt=0) or a.billamt>0 then b.paid*-1 else b.paid end), \n"
            + "a.balance=a.balance+(case when ((a.transtype in ('S','P') or a.orginalseries in ('P'))  and a.billamt=0) or a.billamt>0 then b.paid*-1 else b.paid end) \n"    
            + "from pendingbill" + _firmCode + " a join \n" 
            + "( \n"
  	        + "    select bill_amount,paid,trans_no,trans_type,trans_series,trans_flag from pbillsub" + _firmCode + " where \n"
            + "    trans_type=@trans_type and trans_no=@trans_no and trans_series=@trans_series and trans_flag=@trans_flag \n" 
	        + "    union all \n"
            + "    select  bill_amount,paid,trans_no,trans_type,trans_series,trans_flag from pbillsub" + _firmCode + " where \n"
            + "    bill_from=@trans_type and bill_no=@trans_no and series=@trans_series and flag=@trans_flag \n"  
            + ") b on a.transtype=b.trans_type and a.orderno=b.trans_no and \n"   
            + "a.series=case when a.transtype in ('H','U','S','N') then  b.trans_series else a.series end \n"    
            + "and a.flag=b.trans_flag";
            _cmd.ExecuteNonQuery();

            _cmd.CommandText = "delete from pbillsub" + _firmCode + " where \n"
            + "(trans_no=@trans_no and trans_type=@trans_type and \n"
            + "trans_series=@trans_series and trans_flag=@trans_flag) or \n"
            + "(bill_from=@trans_type and bill_no=@trans_no and series=@trans_series and flag=@trans_flag )";
            _cmd.ExecuteNonQuery();

            _cmd.CommandText = "delete from pendingbill" + _firmCode + " where \n"
            + "transtype=@trans_type and orderno=@trans_no and (series=@trans_series or (transtype='N' and (series='*' or series='H')) or transtype in ('H','U')) and flag=@trans_flag";
            _cmd.ExecuteNonQuery();
            return true;
        }   

        #endregion

        # region Public Methods

        public Boolean Checkreference() 
        {
            if (_transNo == string.Empty || _transType == string.Empty || _transFlag == string.Empty || _firmCode == string.Empty)
            { throw new Exception("Invalid Parameters.."); }

            if (_module == module.Inventry) { return true; }
            
            _cmd.Parameters.Clear();
            _cmd.CommandText = "select count(*) from pbillsub" + _firmCode + " where \n"
            + "bill_no=@transno and bill_from=@transtype and flag=@flag";
            _cmd.Parameters.AddWithValue("@transno", _transNo);
            _cmd.Parameters.AddWithValue("@transtype", _transType);
            _cmd.Parameters.AddWithValue("@flag", _transFlag);
            if (Convert.ToDecimal(_cmd.ExecuteScalar()) == 0) { return true; }
            return false;
        }

        public Boolean ShowSettlement()
        {
            if (!CheckTransType())
            {
                return true ; 
            }
            else if (!CheckAccount())
            {
                return true;
            }

            if (_module == module.Inventry)
            {
                //if (cPublic.g_UserLevel >= 8)
                //{
                    frmBillDialog objBillDlg = new frmBillDialog();
                    if (objBillDlg.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    { return false; }

                    _vPending = objBillDlg.vPending;
                //}
                //else { _vPending = "P"; }
                if (_vPending == "A")
                {
                    using (frmbillamountReverse objBillAmtRev = new frmbillamountReverse())
                    {
                        objBillAmtRev.CustCode = _custCode;
                        objBillAmtRev.TransNo = _transNo;
                        objBillAmtRev.TransType = _transType;
                        objBillAmtRev.Series = _transSeries;
                        if (_transType == "H" || _transType == "N") { objBillAmtRev.lblstaus.Text = "Cr"; }
                        else { objBillAmtRev.lblstaus.Text = "Dr"; } 
                        objBillAmtRev.BillAmt = _billAmt;
                        if (objBillAmtRev.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                        { return false; }
                        _cfg = objBillAmtRev.FlxBills;
                    }
                }
            }
            else if (_module == module.Accounts)
            {
                if (objBillAmt == null || !objBillAmt.hide ||
                    objBillAmt.CustCode != _custCode || objBillAmt.OrgSeries != _orgSeries)
                { objBillAmt = new frmbillamount(); }

                objBillAmt.CustCode = _custCode;
                objBillAmt.TransNo = _transNo;
                objBillAmt.TransType = _transType;
                objBillAmt.BillAmt = _billAmt;
                objBillAmt.OrgSeries = _orgSeries;
                if (_orgSeries == "R") { objBillAmt.lblstaus.Text  = "Cr"; }
                else { objBillAmt.lblstaus.Text = "Dr"; } 
                if (_mode == mode.Modify)
                { objBillAmt.Tag = "M"; }
                else { objBillAmt.Tag = string.Empty; }

                if (_dataedit != dataedit.No)
                {
                    if (objBillAmt.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    { return false; }
                }
                else
                {
                    objBillAmt.Visible = false;
                    //objBillAmt.Show();
                    objBillAmt.Load_Screen();
                    objBillAmt.save_screen();
                }
                _cfg = objBillAmt.FlxBills;
                _DifferenceAmt = objBillAmt.DifferenceAmt;
            }

            return true;
        }

        public Boolean SavePendingBill()
        {
            try
            {
                if (!CheckTransType())
                {
                    return false;
                }

                setSeries();

                if (_cmd == null)
                {
                    _cmd = new SqlCommand();
                    _cmd.Connection = cPublic.Connection;
                }

                if (_cmd.Transaction == null)
                {
                    _con.RefreshConn();
                    _cmd.Connection = cPublic.Connection;
                    _cmd.Transaction = cPublic.Connection.BeginTransaction();

                    _newTransaction = true;
                }

                if (_mode != mode.Entry)
                {
                    if (_module == module.Inventry)
                    {
                        ReverseAllBills();
                    }
                    else if (_module == module.Accounts)
                    {
                        DeletePBillSub();
                        DeletePendingBill();
                    }
                }

                if (CheckPendingbill() && _mode != mode.Cancel)
                {
                    if (_module == module.Inventry)
                    { InsertPendingBill(SetPendingBillValues(new PendingBill())); }

                    for (int i = 1; i < _cfg.Rows.Count; i++)
                    {
                        if (_module == module.Inventry && _vPending == "P") { break; }

                        if (Convert.ToDecimal(_cfg[i, "amount"] + "") != 0)
                        {
                            if ((_cfg[i, "series"] + "").ToLower() == "adv" && _module == module.Accounts)
                            {
                                InsertPendingBill(SetPendingBillValues(new PendingBill(), i));
                            }
                            else
                            {
                                if ((_cfg[i, "series"] + "").ToLower() != "adv")
                                    InsertPBillSub(SetPBillSubValues(new PBillSub(), i));
                            }
                        }
                    }
                }

                if (_newTransaction)
                { _cmd.Transaction.Commit(); }
            }
            catch (Exception ex)
            {
                if (_newTransaction)
                { _cmd.Transaction.Rollback(); }
                throw new Exception(ex.Message);
            }

            return true;
        }

        #endregion
    }

    class PendingBill
    {
        private string _transType;
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }

        private string _series;
        public string Series
        {
            get { return _series; }
            set { _series = value; }
        }

        private string _orginalSeries;
        public string OrginalSeries
        {
            get { return _orginalSeries; }
            set { _orginalSeries = value; }
        }

        private string _orderno;
        public string Orderno
        {
            get { return _orderno; }
            set { _orderno = value; }
        }

        private DateTime _date1;
        public DateTime Date1
        {
            get { return _date1; }
            set { _date1 = value; }
        }

        private string _custCode;
        public string CustCode
        {
            get { return _custCode; }
            set { _custCode = value; }
        }

        private string _crDays;
        public string CrDays
        {
            get { return _crDays; }
            set { _crDays = value; }
        }

        private decimal _billAmt;
        public decimal BillAmt
        {
            get { return _billAmt; }
            set { _billAmt = value; }
        }

        private string _bill;
        public string Bill
        {
            get { return _bill; }
            set { _bill = value; }
        }

        private decimal _received;
        public decimal Received
        {
            get { return _received; }
            set { _received = value; }
        }

        private decimal _balance;
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string _flag;
        public string Flag
        {
            get { return _flag; }
            set { _flag = value; }
        }

        private decimal _paid;
        public decimal Paid
        {
            get { return _paid; }
            set { _paid = value; }
        }

        private string _invno;
        public string Invno
        {
            get { return _invno; }
            set { _invno = value; }
        }

        private DateTime _invDate;
        public DateTime InvDate
        {
            get { return _invDate; }
            set { _invDate = value; }
        }

        private string _remarks;
        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }

        private string _userId;
        public string UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }      
	
    }

    class PBillSub
    {
        private string _custCode;
        public string CustCode
        {
            get { return _custCode; }
            set { _custCode = value; }
        }

        private string _series;
        public string Series
        {
            get { return _series; }
            set { _series = value; }
        }

        private string _orginalSeries;
        public string OrginalSeries
        {
            get { return _orginalSeries; }
            set { _orginalSeries = value; }
        }

        private string _billNo;
        public string BillNo
        {
            get { return _billNo; }
            set { _billNo = value; }
        }

        private string _billFrom;
        public string BillFrom
        {
            get { return _billFrom; }
            set { _billFrom = value; }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string _flag;
        public string Flag
        {
            get { return _flag; }
            set { _flag = value; }
        }

        private string _invNo;
        public string InvNo
        {
            get { return _invNo; }
            set { _invNo = value; }
        }

        private DateTime _dateOfPay;
        public DateTime DateOfPay
        {
            get { return _dateOfPay; }
            set { _dateOfPay = value; }
        }

        private DateTime _invDate;
        public DateTime InvDate
        {
            get { return _invDate; }
            set { _invDate = value; }
        }

        private decimal _billAmt;
        public decimal BillAmt
        {
            get { return _billAmt; }
            set { _billAmt = value; }
        }

        private decimal _amountPaid;
        public decimal AmountPaid
        {
            get { return _amountPaid; }
            set { _amountPaid = value; }
        }

        private decimal _amountGiven;
        public decimal AmountGiven
        {
            get { return _amountGiven; }
            set { _amountGiven = value; }
        }

        private decimal _paid;
        public decimal Paid
        {
            get { return _paid; }
            set { _paid = value; }
        }
        
        private decimal _balance;
        public decimal Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }

        private string _transNo;
        public string TransNo
        {
            get { return _transNo; }
            set { _transNo = value; }
        }

        private string _transType;
        public string TransType
        {
            get { return _transType; }
            set { _transType = value; }
        }

        private string _transSeries;
        public string TransSeries
        {
            get { return _transSeries; }
            set { _transSeries = value; }
        }

        private string _transFlag;
        public string TransFlag
        {
            get { return _transFlag; }
            set { _transFlag = value; }
        }

        private string _modiUser;
        public string ModiUser
        {
            get { return _modiUser; }
            set { _modiUser = value; }
        }

        private string _userId;
        public string UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        private string _type;
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
    }
}
