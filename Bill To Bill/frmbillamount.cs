using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using C1.Win.C1FlexGrid;
using Microsoft.VisualBasic;
using System.Data.SqlClient;

namespace Biz_Maxx
{
    public partial class frmbillamount : Form
    {
        public Boolean hide = false;
        public Boolean close = false;

        private string _custCode;
        public string CustCode
        {
            set { _custCode = value; }
            get { return _custCode;}
        }

        private string _transType;
        public string TransType
        {
            set { _transType = value; }
        }

        private string _transNo;
        public string TransNo
        {
            set { _transNo = value; }
        }

        private string _series;
        public string Series
        {
            set { _series = value; }
        }

        private string _orgSeries;
        public string OrgSeries
        {
            set { _orgSeries = value; }
            get { return _orgSeries; }
        }

        private decimal _billAmt;
        public decimal BillAmt
        {
            set { _billAmt = value; }
        }

        public decimal DifferenceAmt
        {
            get
            {
                if (Information.IsNumeric(txtdifference.Text) == false) { txtdifference.Text = "0.00"; } 
                return Convert.ToDecimal(txtdifference.Text);
            }
        }

        public C1FlexGrid FlxBills
        {
            get { return c1FlexGrid1; }
        }

        fssgen.fssgen gen = new fssgen.fssgen();
        cGeneral cGen = new cGeneral();

        public frmbillamount()
        {
            InitializeComponent();
        }

        public bool loadpendingbill()
        {
            string query = string.Empty, criteria = string.Empty,transtype = string.Empty;
            string status = cPublic.g_OpDate.Year.ToString().Substring(2, 2) + "-" + cPublic.g_ClDate.Year.ToString().Substring(2, 2);
            string rDate = cPublic.g_Rdate.ToString("yyyy/MM/dd");
          
            SqlDataAdapter da = new SqlDataAdapter("", cPublic.Connection);
            if (this.Tag.ToString().Contains("M"))
            {
                if (cPublic.referablebills.Trim().ToUpper() == "YES")
                {
                    criteria += " and (Series='Adv' or orginalseries in ('P','R') and orderno=@transno and transtype=@transtype and flag='Y'  \n"
                    + "   \n" 
                    + " or (case when orginalseries in ('P','R') and orderno=@transno and transtype=@transtype and flag='Y' \n"
                              + " then abs(balance) else abs(balance)+isnull(amount,0) end)<>0 )  ";
                }

                query = "select date1[Date],[TransType],case \n"
                + "       when orginalseries in ('P','R') and orderno=@transno and transtype=@transtype and flag='Y' then 'Adv' \n"
                + "       when orginalseries in ('P','R') then transtype else series end [Series], \n"
                + "orderno[BillNo],[InvNo],[InvDate],datediff(day,date1,'" + cPublic.g_Rdate.ToString("yyyy-MM-dd") + "') [Days],  \n"
                + "      orginalseries[OrgSeries],abs(billamt)[BillAmt],status[Year], \n"
                + "case when orginalseries in ('P','R') and orderno=@transno and transtype=@transtype and flag='Y' \n"
                + "     then received else received-isnull(amount,0) end [Received], \n"
                + "case when orginalseries in ('P','R') and orderno=@transno and transtype=@transtype and flag='Y' \n"
                + "     then abs(balance) else abs(balance)+isnull(amount,0) end [Balance], \n"
                + "[Amount],case when orginalseries in ('P','R') and orderno=@transno and transtype=@transtype and flag='Y'  \n"
                + " then '' when TransType in ('H','N','R') or orginalseries in ('R') then 'Cr'   else 'Dr' End [Remarks],[Flag] from \n"
                + "(  \n"
                + "    select series,date1,orderno,[InvNo],[InvDate],orginalseries,billamt,status, \n"
                + "    [Received],[Balance], \n"
                + "    (select case \n"
                + "            when (a.orginalseries in ('P','R') and a.orderno=@transno and a.transtype=@transtype and a.flag='Y' and a.custcode=@custcode) \n"
                + "            then abs(a.balance) else isnull(sum(paid),0) end \n"
                + "            from pbillsub" + cPublic.g_firmcode + " where custcode=a.custcode and \n"
                + "            series=case when (a.orginalseries in ('P','R')) then series else a.series end \n"
                + "            and orginalseries=a.orginalseries and flag=a.flag and bill_no=a.orderno and bill_from=a.transtype \n"
                + "            and trans_type=@transtype and trans_no=@transno and trans_flag='Y')[Amount], \n"
                + "    [Remarks],[TransType],[Flag] from pendingbill" + cPublic.g_firmcode + " a where custcode=@custcode     \n"
                + "    union all  \n"
                + "    select 'Adv'[Series],'" + rDate + "'[Date1],''[orderno],''[InvNo],'" + rDate + "'[InvDate], \n"
                + "    ''[OrgSeries],0[BillAmt],'" + status + "'[Year],0[Received],0[Balance],0[Amount],''[Remarks],''[Transtype],'Y'[flag] \n"
                + "    where (select count(*) from pendingbill" + cPublic.g_firmcode + " where transtype=@transtype and orderno=@transno and flag='Y' and custcode=@custcode)=0 \n"
                + ")[table] where series is not null " + criteria + " order by date1,remarks desc, orderno desc,series";

                da.SelectCommand.CommandText = query;
                da.SelectCommand.Parameters.AddWithValue("@custcode", _custCode);
                da.SelectCommand.Parameters.AddWithValue("@transno", _transNo);
                da.SelectCommand.Parameters.AddWithValue("@transtype", _transType);
            }
            else if (this.Tag.ToString() == "XA" || this.Tag + "" == "RS")
            {
                query = "select * from (  select Date1[Date],transtype[TransType],[Series],orderno[BillNo],invno[InvNo],[InvDate], \n"
                  + ",datediff(day,date1,'" + cPublic.g_Rdate.ToString("yyyy-MM-dd") + "') [Days],orginalseries[OrgSeries],abs(BillAmt)[BillAmt],status[Year],[Received],abs(Balance)[Balance], \n"
                  + "convert(numeric(9,2),0)[Amount],case when Series='Adv' then '' when TransType in ('H','N','R') or orginalseries in ('R') then 'Dr'   else 'Cr' End Remarks,[Flag] from pendingbill" + cPublic.g_firmcode + " \n"
                  + "where custcode=@custcode " + criteria + "\n"
                  + "union all \n"
                  + "select '" + rDate + "'[Date],''[Transtype],'Adv'[Series],'999999'[Billno],''[InvNo],'" + rDate + "'[InvDate], null Days, \n"
                  + "''[OrgSeries],0[BillAmt],'" + status + "'[Year], \n"
                  + "(select isnull(sum(paid),0) from pbillsub" + cPublic.g_firmcode + " where custcode=@custcode \n"
                  + "and series='*')[Received],0[Balance],0[Amount],''[Remarks],'Y'[flag] ) tt  where billno<>'999999' ";

                da.SelectCommand.CommandText = query;
                da.SelectCommand.Parameters.AddWithValue("@custcode", _custCode);
            }
            else
            {
                 if (cPublic.referablebills.Trim().ToUpper() == "YES")
                { criteria += " and balance<>0"; }

                query = "select Date1[Date],transtype[TransType],case when orginalseries in ('P','R') then transtype else series end [Series], \n"
                + "orderno[BillNo],invno[InvNo],[InvDate],datediff(day,date1,'" + cPublic.g_Rdate.ToString("yyyy-MM-dd") + "') [Days], orginalseries[OrgSeries], \n"
                + "abs(BillAmt)[BillAmt],status[Year],[Received],abs(Balance)[Balance], \n"
                + "convert(numeric(9,2),0)[Amount],case when Series='Adv' then '' when TransType in ('H','N','R') or orginalseries in ('R') then 'Cr'   else 'Dr' End Remarks,[Flag] from pendingbill" + cPublic.g_firmcode + " \n"
                + "where custcode=@custcode "+criteria +" \n"
                + "union all \n"
                + "select '" + rDate + "'[Date],''[Transtype],'Adv'[Series],''[Billno],''[InvNo],'" + rDate + "'[InvDate],null Days,''[OrgSeries], \n"
                + "0[BillAmt],'" + status + "'[Year],0[Received],0[Balance],0[Amount],''[Remarks],'Y'[flag]";
                da.SelectCommand.CommandText = query;
                da.SelectCommand.Parameters.AddWithValue("@custcode", _custCode);
            }

            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Referable Bills not found,Please check !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            c1FlexGrid1.DataSource = dt;

            grid_initialize();
            gridindex();
            balance();

            return true;
        }

        public void grid_initialize()
        {

            //****************************************************************************************
            //         Initializing the Grid and Setting Name,Width and Caption for the Grid          
            //***********************************START************************************************
            c1FlexGrid1.Cols.Count = 16;

            c1FlexGrid1[0, 0] = "Nos";
            c1FlexGrid1.Cols[0].Name = "slno";
            c1FlexGrid1.Cols["slno"].DataType = typeof(decimal);
            c1FlexGrid1.Cols["slno"].Width = 40;

            c1FlexGrid1.Cols["Series"].Width = 50;
            c1FlexGrid1.Cols["Date"].Width = 80;
            c1FlexGrid1.Cols["BillNo"].Width = 80;
            c1FlexGrid1.Cols["InvNo"].Width = 80;
            c1FlexGrid1.Cols["InvDate"].Width = 80;
            c1FlexGrid1.Cols["OrgSeries"].Width = 45;
            c1FlexGrid1.Cols["BillAmt"].Width = 100;
            c1FlexGrid1.Cols["Year"].Width = 50;
            c1FlexGrid1.Cols["Received"].Width = 100;
            c1FlexGrid1.Cols["Balance"].Width = 100;
            c1FlexGrid1.Cols["Amount"].Width = 100;
            c1FlexGrid1.Cols["Remarks"].Width = 60;


            c1FlexGrid1.Cols["Received"].Caption = "Paid/Recv.";
            c1FlexGrid1.Cols["Amount"].Format = "#0.00";

            c1FlexGrid1.Cols["TransType"].Visible = true;
            c1FlexGrid1.Cols["TransType"].Width = 20;
            c1FlexGrid1.Cols["TransType"].Caption = "T-Type";
            c1FlexGrid1.Cols["Days"].Width = 60;
            c1FlexGrid1.Cols["TransType"].AllowEditing = false;
            c1FlexGrid1.Cols["Days"].AllowEditing = false;
            c1FlexGrid1.Cols["Remarks"].Visible = true;
            c1FlexGrid1.Cols["Remarks"].Caption = "Status";


            c1FlexGrid1.Cols["OrgSeries"].Visible = false;
            c1FlexGrid1.Cols["flag"].Visible = false;

            c1FlexGrid1.Cols["Series"].AllowEditing = false;
            c1FlexGrid1.Cols["Date"].AllowEditing = false;
            c1FlexGrid1.Cols["BillNo"].AllowEditing = false;
            c1FlexGrid1.Cols["InvNo"].AllowEditing = false;
            c1FlexGrid1.Cols["InvDate"].AllowEditing = false;
            c1FlexGrid1.Cols["OrgSeries"].AllowEditing = false;
            c1FlexGrid1.Cols["BillAmt"].AllowEditing = false;
            c1FlexGrid1.Cols["Year"].AllowEditing = false;
            c1FlexGrid1.Cols["Received"].AllowEditing = false;
            c1FlexGrid1.Cols["Balance"].AllowEditing = false;
            c1FlexGrid1.Cols["Remarks"].AllowEditing = false;
        }      
        public void gridindex()//----------FOR LOADING SERIAL NUMBER---------\\
        {
            if (c1FlexGrid1.Rows.Count > 1)
            {
                for (int i = 1; i < c1FlexGrid1.Rows.Count; i++)
                {
                    if (c1FlexGrid1[i, "Series"] == null) { break; }
                    c1FlexGrid1[i, "slno"] = i;
                }
            }
        }
        public void balance()
        {
            decimal setteleamount = 0;
            if (c1FlexGrid1.Rows.Count >= 1)
            {
                for (int i = 1; i < c1FlexGrid1.Rows.Count; i++)
                {
                    if ((i == c1FlexGrid1.Row) && (c1FlexGrid1.Editor != null))
                    { setteleamount = setteleamount + (Convert.ToDecimal(c1FlexGrid1.Editor.Text.Trim())); }
                    else
                    {
                        if (c1FlexGrid1[i, "Amount"].ToString() != "")
                            setteleamount = setteleamount + (Convert.ToDecimal(c1FlexGrid1[i, "Amount"]));
                    }
                }
                txtsplitamount.Text = setteleamount.ToString("#0.00");
                txtdifference.Text = Convert.ToString((Convert.ToDecimal(txtbillamount.Text) - setteleamount));
                if (Convert.ToDecimal(txtdifference.Text) == 0)
                { btnSave.Enabled = true; }
                else { btnSave.Enabled = false; }
            }
        }        

        private void frmbillamount_Load(object sender, EventArgs e)
        {
            Load_Screen(); 
        }

        public void Load_Screen()
        {
            Customername();
            txtbillamount.Text = _billAmt.ToString();
            this.DialogResult = DialogResult.None;

            close = true;
            txtbillamount.Text = Convert.ToDecimal(txtbillamount.Text).ToString("#0.00");
            c1FlexGrid1.Cols.Count = 16;
            if (!hide) { if (loadpendingbill() == false) { this.Close(); return; } }
            gridindex();
            balance();
            c1FlexGrid1.Row = 1;
            c1FlexGrid1.Col = c1FlexGrid1.Cols["Amount"].Index;
            c1FlexGrid1.Focus();
            if (hide) { SendKeys.Send("{Tab}"); }
        }

        private void frmbillamount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) { SendKeys.Send("{Tab}"); }
            if (e.KeyCode == Keys.Escape) { this.Close(); }
        }
        private void frmbillamount_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (close) { hide = false; }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            save_screen();
        }

        public void save_screen()
        {
            this.DialogResult = DialogResult.OK;
            hide = true;
            close = false;
            this.Hide(); 
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Sure to Close", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                close = true; hide = false;
                this.Close();
            }
        }

        private void c1FlexGrid1_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{F2}");
        }        
        private void c1FlexGrid1_Enter(object sender, EventArgs e)
        {
            SendKeys.Send("{F2}");
        }
        private void c1FlexGrid1_EnterCell(object sender, EventArgs e)
        {
            int row = c1FlexGrid1.Row;
            int col = c1FlexGrid1.Col;
            if (c1FlexGrid1.Cols[c1FlexGrid1.Col].AllowEditing == false)
            { SendKeys.Send("{Tab}"); }
            SendKeys.Send("{F2}");
        }
        private void c1FlexGrid1_SetupEditor(object sender, RowColEventArgs e)
        {
            if (e.Col == c1FlexGrid1.Cols["Amount"].Index)
            {
                TextBox tb = (TextBox)c1FlexGrid1.Editor;
                tb.MaxLength = 9;
            }
        }
        private void c1FlexGrid1_KeyPressEdit(object sender, KeyPressEditEventArgs e)
        {
            C1FlexGrid FlexGrd = (C1FlexGrid)sender;
            if (FlexGrd.Editor == null) { return; }
            if (e.KeyChar != Convert.ToChar(Keys.Enter) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                if (e.Col == c1FlexGrid1.Cols["Amount"].Index)  //=========== NUMERIC NUMBER VALIDATION FOR "Amount" COLUMN ==============\\
                {
                    if (Convert.ToDecimal(c1FlexGrid1[e.Row, "Balance"]) == 0 && (Convert.ToString(c1FlexGrid1[e.Row, "Series"] + "").Trim().ToUpper() != "ADV"))
                    { e.Handled = true; }
                    else if (Convert.ToString(c1FlexGrid1[e.Row, "Series"] + "").Trim().ToUpper() != "ADV")
                    {
                        switch (_transType)
                        {
                            case "H":
                            case "N":
                                if (((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "S") &&
                                    ((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "U") &&
                                    ((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "R"))
                                { e.Handled = true; }
                                break;
                            case "S":
                            case "U":
                                if (((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "H") &&
                                    ((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "N") &&
                                    ((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "P"))
                                { e.Handled = true; }
                                break;
                            default:  // Accounts
                                if (_orgSeries == "R")
                                {
                                    if (((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "S") &&
                                        ((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "U") &&
                                        ((c1FlexGrid1[e.Row, "orgseries"] + "").Trim().ToUpper() != "P"))
                                    { e.Handled = true; }
                                }
                                else if (_orgSeries == "P")
                                {
                                    if (((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "H") &&
                                        ((c1FlexGrid1[e.Row, "transtype"] + "").Trim().ToUpper() != "N") &&
                                        ((c1FlexGrid1[e.Row, "orgseries"] + "").Trim().ToUpper() != "R"))
                                    { e.Handled = true; }
                                }

                                break;
                        } 
                    }

                    //gen.ValidFlexNumber(e, ref FlexGrd, 9, 2);
                }
            }
        }

        private void c1FlexGrid1_KeyDownEdit(object sender, KeyEditEventArgs e)
        {
            if ((e.Control) && e.KeyCode == Keys.Enter)
            {
                c1FlexGrid1.Col = 1;
                e.Handled = true;
                if (btnSave.Enabled == true)
                    btnSave.Focus();
                else
                    btnExit.Focus();
            }
        }
        private void c1FlexGrid1_ValidateEdit(object sender, ValidateEditEventArgs e)
        {
            if (!cGen.isFormClosing())
            {
                if (c1FlexGrid1.Cols["Amount"].Index == e.Col && c1FlexGrid1.Editor != null)
                {
                    if (!Information.IsNumeric(c1FlexGrid1.Editor.Text))
                    { c1FlexGrid1.Editor.Text = "0.00"; }
                    balance();
                    if (Convert.ToDecimal(txtdifference.Text.Trim()) < 0 && Convert.ToDecimal(c1FlexGrid1.Editor.Text) != 0)
                    {
                        MessageBox.Show("Amount Greater than Bill Amount", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                        ((TextBox)c1FlexGrid1.Editor).SelectAll();
                        return;
                    }
                    else { e.Cancel = false; }

                    if ((Convert.ToString(c1FlexGrid1[e.Row, "Series"] + "").Trim().ToUpper() != "ADV") && (Convert.ToDecimal(c1FlexGrid1[e.Row, "Balance"]) < Convert.ToDecimal(c1FlexGrid1.Editor.Text.Trim())))
                    {
                        MessageBox.Show("Amount Must Be Less than or Equal to Balance..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        ((TextBox)c1FlexGrid1.Editor).SelectAll();
                        return;
                    }
                    else { e.Cancel = false; }
                }
            }
        }

        private void Customername()
        {
            SqlCommand cmd = new SqlCommand("select head from Accounts" + cPublic.g_firmcode + " Where code=@code ", cPublic.Connection);
            cmd.Parameters.AddWithValue("@code", _custCode);
            this.Text = "Bill Amount -" + cmd.ExecuteScalar() + "";
        }

    }
}