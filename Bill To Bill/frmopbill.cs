using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;

using Microsoft.VisualBasic;
using System.IO;

namespace Biz_Maxx
{
    public partial class frmopbill : Form
    {
        public frmopbill()
        {
            InitializeComponent();
        }

        int counterflag = 0;
        int rows = 0;
        public  string custcode = "";
        fssgen.fssgen gen = new fssgen.fssgen();
        cGeneral cgen = new cGeneral();
        string sql = string.Empty;
        SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);  

        #region grid_initialize
        private void grid_initialize()
        {
            
            //*************************************************************************************
            //         Initializing the Grid and Setting Name,Width and Caption for the Grid          
            //**********************************START********************************************** .
            //c1FlexGrid1.Rows.Count = 1;
            //c1FlexGrid1.Cols.Count = 9;
          
            c1FlexGrid1[0, 0] = "Nos";
            c1FlexGrid1.Cols[0].Name = "SL NO";
            c1FlexGrid1.Cols["SL NO"].DataType = typeof(decimal);
            c1FlexGrid1.Cols["SL NO"].Width = 40;

            c1FlexGrid1[0, 1] = "Series";
            c1FlexGrid1.Cols[1].Name = "Series";
            c1FlexGrid1.Cols["Series"].Width = 100;
            c1FlexGrid1.Cols["Series"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter ;

            c1FlexGrid1[0, 2] = "Date";
            c1FlexGrid1.Cols[2].Name = "Date";
            c1FlexGrid1.Cols["Date"].Width = 130;
            c1FlexGrid1.Cols["Date"].DataType = typeof(DateTime);
            c1FlexGrid1.Cols["Date"].Format = "dd/MM/yyyy";
            c1FlexGrid1.Cols["Date"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;

            c1FlexGrid1[0, 3] = "Bill No";
            c1FlexGrid1.Cols[3].Name = "Billno";
            c1FlexGrid1.Cols["Billno"].Width = 110;
            c1FlexGrid1.Cols["Billno"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter;

            c1FlexGrid1[0, 4] = "Invoice No";
            c1FlexGrid1.Cols[4].Name = "Invoiceno";
            c1FlexGrid1.Cols["Invoiceno"].Width = 120;
            c1FlexGrid1.Cols["Invoiceno"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;

            c1FlexGrid1[0, 5] = "Invoice Date";
            c1FlexGrid1.Cols[5].Name = "Invdate";
            c1FlexGrid1.Cols["Invdate"].Width = 130;
            c1FlexGrid1.Cols["Invdate"].DataType = typeof(DateTime);
            c1FlexGrid1.Cols["Invdate"].Format = "dd/MM/yyyy";
            c1FlexGrid1.Cols["Invdate"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;

            c1FlexGrid1[0, 6] = "Bill Amount";
            c1FlexGrid1.Cols[6].Name = "Billamt";
            c1FlexGrid1.Cols["Billamt"].Width = 140;
            c1FlexGrid1.Cols["Billamt"].DataType = typeof(double);
            c1FlexGrid1.Cols["Billamt"].Format = "#0.00";
            c1FlexGrid1.Cols["Billamt"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter;

            c1FlexGrid1[0, 7] = "Year";
            c1FlexGrid1.Cols[7].Name = "Year";
            c1FlexGrid1.Cols["Year"].Width = 110;
            c1FlexGrid1.Cols["Year"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter ;

            c1FlexGrid1[0, 8] = "Received";
            c1FlexGrid1.Cols[8].Name = "Received";
            c1FlexGrid1.Cols["Received"].Width = 110;
            c1FlexGrid1.Cols["Received"].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;


            c1FlexGrid1.Cols["Series"].AllowEditing = false;
            c1FlexGrid1.Cols["Date"].AllowEditing = false;
            c1FlexGrid1.Cols["Invoiceno"].AllowEditing = false;
            c1FlexGrid1.Cols["Billno"].AllowEditing = false;
            c1FlexGrid1.Cols["Invdate"].AllowEditing = false;
            c1FlexGrid1.Cols["Year"].AllowEditing = false;
            c1FlexGrid1.Cols["Received"].AllowEditing = false;
            c1FlexGrid1.Cols["Billamt"].AllowEditing = false ;
           

        }
        # endregion

        # region gridindex
        private void gridindex()//----------FOR LOADING SERIAL NUMBER---------\\
        {
            if (c1FlexGrid1.Rows.Count > 1)
            {
                for (int i = 1; i < c1FlexGrid1.Rows.Count; i++)
                {
                    if (c1FlexGrid1[i, "Series"] == null) { break; }
                    c1FlexGrid1[i, "SL NO"] = i;
                    counterflag = i;
                }
            }
            else
            {
                counterflag = 0;
            }
        }

        # endregion

        # region defaultValueLoad
        private void defaultValueLoad()
        {
            int  year =Convert.ToInt32(cPublic.g_ClDate.Year.ToString().Substring(2));
            
            for (int i = 1; i <= 5; i++)
            {
                string item = ((year - 1).ToString("00") + "-" + (year.ToString("00")));
                comboBox1.Items.Add(item);
                year = year - 1;
            }
            comboBox1.SelectedIndex = 0;
                
               cmbtranstype.SelectedIndex = 0;
            
            btnSave.Enabled = false;
            if (Convert.ToDecimal(lblopeningbalance.Tag) < 0)
            {
                lblopeningbalance.Text = Strings.FormatNumber(Convert.ToString(Math.Abs(Convert.ToDecimal(lblopeningbalance.Tag))), 2, TriState.True, TriState.True, TriState.True) + " Dr";

            }
            else
            {
                lblopeningbalance.Text = Strings.FormatNumber(Convert.ToString(Math.Abs(Convert.ToDecimal(lblopeningbalance.Tag))), 2, TriState.True, TriState.True, TriState.True) + " Cr";
            }
            lblopeningbalance.Tag = (Convert.ToDecimal(lblopeningbalance.Tag) * -1);
        }
        # endregion       

        # region refreshFn
        private void refreshFn()
        {
            txtbillno.Text = string.Empty;
            txtbillno.Text=string.Empty;
            txtInvoice.Text=string.Empty;
            //cmbtranstype.SelectedIndex = 0;
            //dtpdate.Value = Convert.ToDateTime("31/03/" + Convert.ToString(DateTime.Today.Year - 1));
           // dtpInvoicedate.Value = Convert.ToDateTime("31/03/" + Convert.ToString(DateTime.Today.Year - 1));            
            txtbillamount.Text = string.Empty;
            
 
        }
        # endregion

        # region refreshgrid
        private void refreshgrid()
        {
            refreshFn();
            c1FlexGrid1.Clear();
            c1FlexGrid1.Refresh();
            grid_initialize();
        }
        # endregion

        private void frmopbill_Load(object sender, EventArgs e)
        {            
            c1FlexGrid1.Rows.Count = 1;
            c1FlexGrid1.Cols.Count = 9;
            grid_initialize();
            defaultValueLoad();
            txtseries.Focus();

            dtpdate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, cPublic.g_OpDate);
            dtpInvoicedate.Value = DateAndTime.DateAdd(DateInterval.Day, -1, cPublic.g_OpDate);

            //dtpdate.Value = Convert.ToDateTime ("31/03/" + Convert.ToString(DateTime.Today.Year  - 1));
            //dtpInvoicedate.Value = Convert.ToDateTime("31/03/" + Convert.ToString(DateTime.Today.Year - 1));
            if (this.Tag+""  == "PBM")
            {
                gridload(custcode);
            }            
        }

        private void cmbtranstype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbtranstype.Text == "Purchase")
            { dtpInvoicedate.Enabled = true; txtInvoice.Enabled = true; txtseries.Text = "H"; txtseries.Enabled = false; }

            else
            { dtpInvoicedate.Enabled = false; txtInvoice.Text = ""; txtInvoice.Enabled = false; txtseries.Text = "A"; txtseries.Enabled = true; }

        }

        # region exit
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Sure to Close", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Information).ToString() == "Yes")
            {
                this.Close();
            }
        }
        #endregion

        # region  functions
        //on add click buuton
        # region add button click
        private void cmdAdd_Click(object sender, EventArgs e)
        {
            if (checking())
            {
                c1FlexGrid1.Rows.Add();
                c1FlexGrid1[counterflag + 1, "Series"] = txtseries.Text.Trim();
                c1FlexGrid1[counterflag + 1, "Date"] = dtpdate.Value;
                c1FlexGrid1[counterflag + 1, "Billno"] = txtbillno.Text.Trim();
                c1FlexGrid1[counterflag + 1, "Invoiceno"] = txtInvoice.Text.Trim();
                c1FlexGrid1[counterflag + 1, "Invdate"] = dtpInvoicedate.Value;
                c1FlexGrid1[counterflag + 1, "Billamt"] = Convert.ToDecimal(txtbillamount.Text.Trim()).ToString("#0.00");
                c1FlexGrid1[counterflag + 1, "Year"] = comboBox1.SelectedItem.ToString();

                if (this.Tag+"" == "PBM")
                {
                    c1FlexGrid1[counterflag + 1, "Received"] = "0";
                }                
                gridindex();
                refreshFn();
                balance();

                if (Convert.ToDecimal(lblbalance.Tag) == 0)
                {
                    btnSave.Enabled = true;
                }
                else
                {
                    btnSave.Enabled = false;
                }
                txtseries.Focus();
                c1FlexGrid1.Select(0, 0);
            }
        }
        #endregion

        # region edit,update button click
        private void cmdEdit_Click(object sender, EventArgs e)
        {
            if (c1FlexGrid1.Rows.Count > 1)
            {

                if (cmdEdit.Text == "Edit")
                {

                    int row = c1FlexGrid1.RowSel;
                    rows = row;
                    if (row != 0)
                    {
                        if (this.Tag+"" == "PBM")
                        {
                            if (Convert.ToDecimal(c1FlexGrid1[row, "Received"]) != 0)
                            {
                                MessageBox.Show("Not editable \n Payment against this bill is already made ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }

                        txtseries.Text = c1FlexGrid1[row, "Series"].ToString();
                        txtbillno.Text = c1FlexGrid1[row, "Billno"].ToString();
                        txtbillamount.Text = c1FlexGrid1[row, "Billamt"].ToString();
                        dtpdate.Value = Convert.ToDateTime(c1FlexGrid1[row, "Date"]);
                        comboBox1.SelectedText = c1FlexGrid1[row, "Year"].ToString();
                        
                        if (c1FlexGrid1[row, "Invoiceno"] + "" != "")
                        {
                            cmbtranstype.SelectedIndex = 1;
                            txtInvoice.Text = c1FlexGrid1[row, "Invoiceno"].ToString();
                            dtpInvoicedate.Value = Convert.ToDateTime(c1FlexGrid1[row, "Invdate"]);

                        }
                        else
                        {
                            cmbtranstype.SelectedIndex = 0;
                        }
                        
                        cmdEdit.Text = "Update";
                        cmdAdd.Enabled = false;
                        txtseries.Focus();
                        txtbillno.Enabled = false;
                    }
                }
                else
                {
                    if (checking())
                    {
                        c1FlexGrid1[rows, "Series"] = txtseries.Text.Trim();
                        c1FlexGrid1[rows, "Date"] = dtpdate.Value;
                        c1FlexGrid1[rows, "Billno"] = txtbillno.Text.Trim();
                        c1FlexGrid1[rows, "Invoiceno"] = txtInvoice.Text.Trim();
                        if (cmbtranstype.SelectedIndex == 1)
                            c1FlexGrid1[rows, "Invdate"] = dtpInvoicedate.Value;
                        c1FlexGrid1[rows, "Billamt"] = Convert.ToDecimal(txtbillamount.Text.Trim()).ToString("#0.00");
                        c1FlexGrid1[rows, "Year"] = comboBox1.SelectedItem.ToString();

                        //function to check update
                        gridindex();
                        refreshFn();
                        balance();

                        if (Convert.ToDecimal(lblbalance.Tag) == 0)
                        {
                            btnSave.Enabled = true;
                        }
                        else
                        {
                            btnSave.Enabled = false;
                        }

                        cmdEdit.Text = "Edit";
                        cmdAdd.Enabled = true;
                        txtbillno.Enabled = true;
                        txtseries.Focus();

                    }
                }
            }
        }

        #endregion

        # region delete button click

        private void cmdDelete_Click(object sender, EventArgs e)
        {
            if (c1FlexGrid1.Rows.Count > 1)
            {
                if (cmdEdit.Text.Trim() != "Update")
                {
                    int row = c1FlexGrid1.RowSel;
                    if (row != 0)
                    {
                        if (this.Tag + "" == "PBM")
                        {
                            if (Convert.ToDecimal(c1FlexGrid1[row, "Received"]) != 0)
                            {
                                MessageBox.Show("Not editable \n Payment against this bill is already made ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }
                        c1FlexGrid1.Rows.Remove(row);

                        //function to check update
                        gridindex();
                        refreshFn();
                        balance();

                        if (Convert.ToDecimal(lblbalance.Tag) == 0)
                        {
                            btnSave.Enabled = true;
                        }
                        else
                        {
                            btnSave.Enabled = false;
                        }
                        txtseries.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Please Complete the editing");
                    cmdEdit.Focus();
                    return;
                }
            }
        }

        #endregion

        #region cancel

        private void cmdcancel_Click(object sender, EventArgs e)
        {
            if (cmdEdit.Text != "Edit")
            {
                cmdEdit.Text = "Update";
                refreshFn();
                cmdAdd.Enabled = true;
                txtbillno.Enabled = true;
            }
        }

        #endregion

        # region balance calculation

        public void balance()
        {
            decimal setteleamount = 0;

            if (c1FlexGrid1.Rows.Count >= 1)
            {
                for (int i = 1; i < c1FlexGrid1.Rows.Count; i++)
                {
                    if (c1FlexGrid1[i, "Invoiceno"]+"".ToString() == "")
                    {
                        setteleamount = setteleamount + (Convert.ToDecimal(c1FlexGrid1[i,"Billamt"])* -1);
                    }
                    else
                    {
                        setteleamount = setteleamount + (Convert.ToDecimal(c1FlexGrid1[i, "Billamt"]) );
                    }

                }
                
                lblsetteledamount.Tag = setteleamount;

                if (Convert.ToDecimal(lblsetteledamount.Tag) < 0)
                {
                    lblsetteledamount.Text = Strings.FormatNumber(Convert.ToString(Math.Abs(setteleamount)),2,TriState.True ,TriState.True ,TriState.True ) + " Dr";
                }
                else if (Convert.ToDecimal(lblsetteledamount.Tag) > 0)
                {
                    lblsetteledamount.Text = Strings.FormatNumber(Convert.ToString(Math.Abs(setteleamount)), 2, TriState.True, TriState.True, TriState.True) + " Cr";
                }
                else
                {
                    lblsetteledamount.Text = Strings.FormatNumber(Convert.ToString(Math.Abs(setteleamount)), 2, TriState.True, TriState.True, TriState.True) ;

                }


            }

            lblbalance.Tag = Convert.ToDecimal (lblopeningbalance.Tag) + Convert.ToDecimal (lblsetteledamount.Tag);
            
            decimal  balanceafter = 0;
            balanceafter=Convert.ToDecimal(lblbalance.Tag );
           
            if(balanceafter<0)
            {
                lblbalance.Text  = Strings.FormatNumber(Convert.ToString(Math.Abs(balanceafter)), 2, TriState.True, TriState.True, TriState.True) + " Cr";
                
            }
            else
            {
                lblbalance.Text  = Strings.FormatNumber(Convert.ToString(Math.Abs(balanceafter)), 2, TriState.True, TriState.True, TriState.True) + " Dr";
            }
        }

        #endregion

        # region validation

        private Boolean checking()
        {
            if (txtseries.Text.Trim() == "")
            {
                MessageBox.Show("please enter series");
                txtseries.Focus();
                return false;
            }
            if (txtbillno.Text.Trim() == "")
            {
                MessageBox.Show("please enter bill number");
                txtbillno.Focus();
                return false;
            }

            if (cmbtranstype.SelectedIndex == 1)
            {
                if (txtInvoice.Text.Trim() == "")
                {
                    MessageBox.Show("please enter invoice number");
                    txtInvoice.Focus();
                    return false;
                }
                //if (dtpInvoicedate.Value.Year >= cPublic.g_Rdate.Year)
                //{
                //    MessageBox.Show("invoice date is out of financial year");
                //    dtpInvoicedate.Focus();
                //    return false;
                //}
                // bill-bill 
                if (txtseries.Text.Trim() != "H")
                {
                    MessageBox.Show("for purchase series should be 'H'");
                    txtseries.Focus();
                    return false;
                }
            }
            else if (cmbtranstype.SelectedIndex == 0)
            {
                if (txtseries.Text.Trim() == "H")
                {
                    MessageBox.Show("for sales series cannot be 'H'");
                    txtseries.Focus();
                    return false;
                }
            }

            if (Convert.ToDecimal(txtbillamount.Text.Trim()) == 0)
            {
                MessageBox.Show("please enter amount");
                txtbillamount.Focus();
                return false;
            }


            if (cmdEdit.Text != "Update")
            {
                sql = "select * from pendingbill" + cPublic.g_firmcode + " where orderno=@orderno and series=@series and status=@status";
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("@orderno", txtbillno.Text.Trim());
                cmd.Parameters.AddWithValue("@series", txtseries.Text.Trim());
                cmd.Parameters.AddWithValue("@status", "N");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show(" Invalid Bill number", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtbillno.Focus();
                    return false;
                }
            }

            //bill no check in grids
            if (c1FlexGrid1.Rows.Count > 1)
            {
                for (int i = 1; i < c1FlexGrid1.Rows.Count; i++)
                {
                    if (cmdEdit.Text != "Update")
                    {
                        if (c1FlexGrid1[i, "Billno"].ToString() == txtbillno.Text.Trim() && c1FlexGrid1[i, "Series"].ToString() == txtseries.Text.Trim())
                        {
                            MessageBox.Show(" Invalid Bill number", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtbillno.Focus();
                            return false;
                        }
                    }
                    if (cmdEdit.Text == "Update")
                    {
                        if ((c1FlexGrid1[i, "Billno"].ToString() == txtbillno.Text.Trim() && c1FlexGrid1[i, "Series"].ToString() == txtseries.Text.Trim()) && i != c1FlexGrid1.RowSel)
                        {
                            MessageBox.Show(" Invalid Bill number", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtbillno.Focus();
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        #endregion


        # region keypress,enter, leave 

        private void txtbillno_Enter(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text.Trim() != "")
            {
                ((TextBox)sender).SelectAll();
            }
        }

        private void txtbillno_KeyPress(object sender, KeyPressEventArgs e)
        {
            gen.ValidDecimalNumber(e, ref txtbillno, 13, 0);
            
        }

        private void txtbillamount_KeyPress(object sender, KeyPressEventArgs e)
        {
            gen.ValidDecimalNumber(e, ref txtbillamount , 6, 2);
        }

        private void frmopbill_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.Control == true && e.KeyCode == Keys.Up)
            {
                c1FlexGrid1.Styles.Highlight.BackColor = Color.DeepSkyBlue;
            
                c1FlexGrid1.Focus();

                if (c1FlexGrid1.Rows.Count > 1)
                {
                    c1FlexGrid1.Select(1, 0);
                }
            }
            else if (e.Control == true && e.KeyCode == Keys.Down)
            {
                c1FlexGrid1.Styles.Highlight.Clear();
                c1FlexGrid1.Select(0, 0);
                gpCmd.Focus();
                cmdAdd.Focus();
            }
            if (e.KeyCode == Keys.Enter)
            { 
                SendKeys.Send("{TAB}"); 
            }

        }

        private void c1FlexGrid1_Click(object sender, EventArgs e)
        {
            c1FlexGrid1.Styles.Highlight.BackColor = Color.DeepSkyBlue;
            
        }

        private void txtseries_Leave(object sender, EventArgs e)
        {
            if (txtseries.Text.Trim() != "")
            {
                txtseries.Text = txtseries.Text.Trim().ToUpper();
            }
        }

        #endregion

        # region save to database

        private void btnSave_Click(object sender, EventArgs e)
        {
            //this.Hide();
            Save_Det();

            #region //
            ////if (this.Tag + "" == "PBM")
            ////{
            ////    SqlCommand cmd = new SqlCommand();
            ////    cmd.Connection = cPublic.Connection;
            ////    cmd = cPublic.Connection.CreateCommand();

            ////    SqlTransaction trans;
            ////    trans = cPublic.Connection.BeginTransaction();
            ////    cmd.Transaction = trans;


            ////    cmd.CommandText = "Select recno from pendingbill" + cPublic.g_firmcode + " where custcode=@custcode and flag ='N' and received =0 ";
            ////    cmd.CommandType = CommandType.Text;
            ////    cmd.Parameters.AddWithValue("@custcode", custcode);
            ////    try
            ////    {
            ////        //string checkpendingbill = "Select recno from pendingbill" + cPublic.g_firmcode + " where custcode=@custcode and flag ='N' and received =0 ";
            ////        //SqlCommand cmd = new SqlCommand(checkpendingbill, cPublic.Connection);
                    
            ////        SqlDataAdapter da = new SqlDataAdapter(cmd);
            ////        DataSet ds = new DataSet();
            ////        da.Fill(ds);
            ////        if (ds.Tables[0].Rows.Count > 0)
            ////        {
            ////            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            ////            {
            ////                cmd.CommandText = "delete from pendingbill" + cPublic.g_firmcode + " where recno=@recno";
            ////                cmd.Parameters.AddWithValue("@recno", ds.Tables[0].Rows[i]["recno"].ToString());
            ////                cmd.ExecuteNonQuery();
            ////                cmd.Parameters.Clear();
            ////            }
            ////        }
            ////        trans.Commit();
            ////    }
                   
            ////    catch (Exception)
            ////    {
            ////        trans.Rollback();
            ////        MessageBox.Show("Please contact your software vendor", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error);
            ////    }
            ////}

            ////if (c1FlexGrid1.Rows.Count >= 1)
            ////{

            ////    SqlCommand cmd = new SqlCommand();
            ////    cmd.Connection = cPublic.Connection;
            ////    cmd = cPublic.Connection.CreateCommand();

            ////    SqlTransaction trans;
            ////    trans = cPublic.Connection.BeginTransaction();
            ////    cmd.Transaction = trans;

                
            ////    cmd.CommandText = "SP_PendingBill_INSERT" + cPublic.g_firmcode;
            ////    cmd.CommandType = CommandType.StoredProcedure;

            ////    try
            ////    {
            ////         for (int i = 1; i < c1FlexGrid1.Rows.Count; i++)
            ////        {
            ////        g:
            ////            if (i < c1FlexGrid1.Rows.Count)
            ////            {
            ////                if (this.Tag + "" == "PBM")
            ////                {
            ////                    if (Convert.ToDecimal(c1FlexGrid1[i, "Received"]) != 0)
            ////                    {
            ////                        i++;
            ////                        goto g;

            ////                    }

            ////                }
            ////                cmd.Parameters.AddWithValue("@DATE1", Convert.ToDateTime(c1FlexGrid1[i, "Date"]).ToString("yyyy-MM-dd"));
            ////                cmd.Parameters.AddWithValue("@custcode", custcode);
            ////                if (c1FlexGrid1[i, "invoiceno"].ToString() == "")
            ////                {
            ////                    cmd.Parameters.AddWithValue("@TRANSTYPE", "S");
            ////                    cmd.Parameters.AddWithValue("@invno", "");
            ////                    cmd.Parameters.AddWithValue("@orginalseries", c1FlexGrid1[i, "SERIES"]);
            ////                    cmd.Parameters.AddWithValue("@Invdate", "");
            ////                    cmd.Parameters.AddWithValue("@billamt", Convert.ToDecimal(c1FlexGrid1[i, "Billamt"]) * -1);
            ////                    cmd.Parameters.AddWithValue("@balance", Convert.ToDecimal(c1FlexGrid1[i, "Billamt"]) * -1);
            ////                }
            ////                else
            ////                {
            ////                    cmd.Parameters.AddWithValue("@TRANSTYPE", "H");
            ////                    cmd.Parameters.AddWithValue("@invno", c1FlexGrid1[i, "Invoiceno"]);
            ////                    cmd.Parameters.AddWithValue("@orginalseries", " ");
            ////                    cmd.Parameters.AddWithValue("@Invdate", Convert.ToDateTime(c1FlexGrid1[i, "Invdate"]).ToString("yyyy-MM-dd"));
            ////                    cmd.Parameters.AddWithValue("@billamt", Convert.ToDecimal(c1FlexGrid1[i, "Billamt"]));
            ////                    cmd.Parameters.AddWithValue("@balance", Convert.ToDecimal(c1FlexGrid1[i, "Billamt"]));
            ////                }
            ////                cmd.Parameters.AddWithValue("@orderno", c1FlexGrid1[i, "Billno"]);
            ////                cmd.Parameters.AddWithValue("@SERIES", c1FlexGrid1[i, "SERIES"]);                            
            ////                cmd.Parameters.AddWithValue("@crdays", "");
            ////                cmd.Parameters.AddWithValue("@bill", "Y");
            ////                cmd.Parameters.AddWithValue("@status", c1FlexGrid1[i, "Year"]);
            ////                if (this.Tag + "" == "PBM")
            ////                {
            ////                    cmd.Parameters.AddWithValue("@received", Convert.ToDecimal(c1FlexGrid1[i, "Received"]));
            ////                }
            ////                else
            ////                {
            ////                    cmd.Parameters.AddWithValue("@received", 0);
            ////                    cmd.Parameters.AddWithValue("@userid", cgen.GetUserDet());
            ////                }
            ////                cmd.Parameters.AddWithValue("@flag", "N");
            ////                cmd.Parameters.AddWithValue("@paid", 0.0);
            ////                //
                            

            ////                cmd.ExecuteNonQuery();
            ////                cmd.Parameters.Clear();
            ////            }
            ////        }
            ////        trans.Commit();
            ////        MessageBox.Show("Successfully records are inserted ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information );
            ////        refreshgrid();
            ////        this.Close();
            ////    }
            ////    catch (Exception)
            ////    {
            ////        trans.Rollback();
            ////        MessageBox.Show("Please contact your software vendor",cPublic.messagename,MessageBoxButtons.OK,MessageBoxIcon.Error );
            ////    }
            ////}
            #endregion
        }

        #endregion

        #region load for edit in grid

        public void gridload(string custcode)
        {
            sql  = "Select Series,Date1 as Date,orderno as Billno,invno as Invoiceno,Invdate,Billamt,status as Year,received as Received from pendingbill" + cPublic.g_firmcode + " where custcode=@custcode and flag =@flag ";
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@flag", "N");
            cmd.Parameters.AddWithValue("@custcode", custcode);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds=new DataSet() ;
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int c = 0; c < ds.Tables[0].Rows.Count; c++)
                {
                    ds.Tables[0].Rows[c]["Billamt"] = Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[c]["Billamt"]));
                    if (Information.IsDate ( ds.Tables[0].Rows[c]["Invdate"])==false  )
                    {
                        ds.Tables[0].Rows[c]["Invdate"] = DBNull.Value;
                    }
                    else if (Convert.ToDateTime(ds.Tables[0].Rows[c]["Invdate"].ToString()) == Convert.ToDateTime("01/01/1900"))
                    {
                        ds.Tables[0].Rows[c]["Invdate"] = DBNull.Value   ;
                    }
                }
                    c1FlexGrid1.DataSource = ds.Tables[0];
            }
            //c1FlexGrid1.Cols.Add();
            //c1FlexGrid1.Cols["received"].Visible = false;
            grid_initialize();            
            gridindex();
            balance();
            
        }
        #endregion

        private void txtbillamount_Leave(object sender, EventArgs e)
        {
            if (txtbillamount.Text != "")
            {
                txtbillamount.Text = Convert.ToDecimal(txtbillamount.Text.Trim()).ToString("0.00");
            }
            else
            {
                txtbillamount.Text = "0.00";
            }
        }

        #endregion

        #region save

        public void Save_Det()
        {
            if (this.Tag + "" == "PBM")
            {
                SqlTransaction trans;
                trans = cPublic.Connection.BeginTransaction();
                cmd.Transaction = trans;

                cmd.Parameters.Clear();
                cmd.CommandText = "Select recno from pendingbill" + cPublic.g_firmcode + " where custcode=@custcode and flag =flag and received =@received ";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@custcode", custcode);
                cmd.Parameters.AddWithValue("@flag", "N");
                cmd.Parameters.AddWithValue("@received", 0);
                try
                {                  
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            cmd.CommandType = CommandType.Text; 
                            cmd.CommandText = "delete from pendingbill" + cPublic.g_firmcode + " where recno=@recno";
                            cmd.Parameters.AddWithValue("@recno", ds.Tables[0].Rows[i]["recno"].ToString());
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                    }
                    trans.Commit();
                }

                catch (Exception)
                {
                    trans.Rollback();
                    MessageBox.Show("Please contact your software vendor", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (c1FlexGrid1.Rows.Count >= 1)
            {              
                SqlTransaction trans;
                trans = cPublic.Connection.BeginTransaction();
                cmd.Transaction = trans;

                cmd.Parameters.Clear();
                cmd.CommandText = "SP_PendingBill_INSERT" + cPublic.g_firmcode;
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    for (int i = 1; i < c1FlexGrid1.Rows.Count; i++)
                    {
                    g:
                        if (i < c1FlexGrid1.Rows.Count)
                        {
                            if (this.Tag + "" == "PBM")
                            {
                                if (Convert.ToDecimal(c1FlexGrid1[i, "Received"]) != 0)
                                {
                                    i++;
                                    goto g;

                                }

                            }
                            cmd.Parameters.AddWithValue("@DATE1", Convert.ToDateTime(c1FlexGrid1[i, "Date"]).ToString("yyyy-MM-dd"));
                            cmd.Parameters.AddWithValue("@custcode", custcode);
                            if (c1FlexGrid1[i, "invoiceno"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@TRANSTYPE", "S");
                                cmd.Parameters.AddWithValue("@invno", "");
                                cmd.Parameters.AddWithValue("@orginalseries", c1FlexGrid1[i, "SERIES"]);
                                cmd.Parameters.AddWithValue("@Invdate", "");
                                cmd.Parameters.AddWithValue("@billamt", Convert.ToDecimal(c1FlexGrid1[i, "Billamt"]) * -1);
                                cmd.Parameters.AddWithValue("@balance", Convert.ToDecimal(c1FlexGrid1[i, "Billamt"]) * -1);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@TRANSTYPE", "H");
                                cmd.Parameters.AddWithValue("@invno", c1FlexGrid1[i, "Invoiceno"]);
                                cmd.Parameters.AddWithValue("@orginalseries", " ");
                                cmd.Parameters.AddWithValue("@Invdate", Convert.ToDateTime(c1FlexGrid1[i, "Invdate"]).ToString("yyyy-MM-dd"));
                                cmd.Parameters.AddWithValue("@billamt", Convert.ToDecimal(c1FlexGrid1[i, "Billamt"]));
                                cmd.Parameters.AddWithValue("@balance", Convert.ToDecimal(c1FlexGrid1[i, "Billamt"]));
                            }
                            cmd.Parameters.AddWithValue("@orderno", c1FlexGrid1[i, "Billno"]);
                            cmd.Parameters.AddWithValue("@SERIES", c1FlexGrid1[i, "SERIES"]);
                            cmd.Parameters.AddWithValue("@crdays", "");
                            cmd.Parameters.AddWithValue("@bill", "Y");
                            cmd.Parameters.AddWithValue("@status", c1FlexGrid1[i, "Year"]);
                            if (this.Tag + "" == "PBM")
                            {
                                cmd.Parameters.AddWithValue("@received", Convert.ToDecimal(c1FlexGrid1[i, "Received"]));
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@received", 0);
                                cmd.Parameters.AddWithValue("@userid", cgen.GetUserDet());
                            }
                            cmd.Parameters.AddWithValue("@flag", "N");
                            cmd.Parameters.AddWithValue("@paid", 0.0);
                            //


                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                    }
                    trans.Commit();
                    MessageBox.Show("Successfully records are inserted ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    refreshgrid();
                    this.Close();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    MessageBox.Show("Please contact your software vendor", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #endregion

    }
}