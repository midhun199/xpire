﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xpire.BL
{
    public  class CustomerBO
    {
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
    }
}
