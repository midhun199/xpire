﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.ComponentModel;

using Xpire.DL;

namespace Xpire.BL
{
  public  class ClsCustomer:BindingList<ClsCustomerBL>
    {
      ClsCustDl objcustdl = new ClsCustDl();
        internal void save(ClsCustomerBL objCustomer)
        {
            objcustdl.save(objCustomer); 
        }

        internal ClsCustomer Select()
        {
         return  objcustdl.Select();
        }

        internal void Update(ClsCustomerBL objCustomer)
        {
            objcustdl.Update(objCustomer);
        }

        internal void Delete(ClsCustomerBL objCustomer)
        {
            objcustdl.Delete(objCustomer);
        }
    }
}
