﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using Xpire.Classes;

namespace Xpire.BL
{
    internal class SoftBO
    {
        internal void ReCalculateStock()
        {
            if (MessageBox.Show("Sure to Re-Calculate Stock !", cPublic.messagename, MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }

            SqlTransaction trans = null;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                trans = cPublic.Connection.BeginTransaction();
                cmd.Transaction = trans;


                cmd.CommandText = " UPDATE STOCKMST" + cPublic.g_firmcode + "    set qty=0 " + Environment.NewLine
                + "   " + Environment.NewLine
                + " ------------- " + Environment.NewLine
                + "   " + Environment.NewLine
                + " select itemcode,ISNULL(sum(qty),0.000) Qty " + Environment.NewLine
                + " into #stockmst " + Environment.NewLine
                + " from  " + Environment.NewLine
                + " (select itemcode,sum((qty+foc)*unitvalue*-1) qty from sitem" + cPublic.g_firmcode + "   group by itemcode  " + Environment.NewLine
                + " union all   " + Environment.NewLine
                + " select itemcode,sum((qty+foc)*unitvalue) from sritem" + cPublic.g_firmcode + "  group by itemcode  " + Environment.NewLine
                + " union all  " + Environment.NewLine
                + " select itemcode,sum((qty+foc)*unitvalue) from pitem" + cPublic.g_firmcode + "    group by itemcode  " + Environment.NewLine
                + " union all  " + Environment.NewLine
                + " select itemcode,sum((qty+foc)*unitvalue*-1) qty from pritem" + cPublic.g_firmcode + "   group by itemcode  " + Environment.NewLine
                + " ) Sn   " + Environment.NewLine
                + " group by itemcode  " + Environment.NewLine
                + "   " + Environment.NewLine
                + " --------------  " + Environment.NewLine
                + "   " + Environment.NewLine
                + " UPDATE A SET A.QTY=B.Qty " + Environment.NewLine
                + " FROM STOCKMST" + cPublic.g_firmcode + "  A join #stockmst B on a.itemcode=b.itemcode " + Environment.NewLine
                + "  " + Environment.NewLine
                + "";
                cmd.ExecuteNonQuery();

                trans.Commit();

                MessageBox.Show("Stock Recalculation Completed Successfully !", cPublic.messagename);
            }
            catch (Exception Ex)
            {
                trans.Rollback();
                MessageBox.Show(Ex.Message, cPublic.messagename);
            }
            finally
            { }
        }

        internal void Updates(bool isMessage)
        {
            if (isMessage == true)
            {
                if (MessageBox.Show("Sure to Update !", cPublic.messagename, MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    return;
                }
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "print 'Hello' ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "insert into PUBLIC_VAR(var_name,var_values) select 'PrintMode','DOS' " + Environment.NewLine +
                    " where 'PrintMode' not in (select var_name from PUBLIC_VAR) ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "insert into PUBLIC_VAR(var_name,var_values) select 'AutoPriceUpdation','NO' " + Environment.NewLine +
                    " where 'AutoPriceUpdation' not in (select var_name from PUBLIC_VAR) ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "INSERT " + cPublic.g_PrjCode + "000..[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno])"
                    + " select  1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 'Sales Performance','Sales Performance',601,695  "
                    + " where 695 not in (select Slno from " + cPublic.g_PrjCode + "000..SETRIGHTS )  "
                    ;
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "INSERT " + cPublic.g_PrjCode + "000..[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno])"
                    + " select  1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 'Initialize','Initialize',701,750 "
                    + " where 750 not in (select Slno from " + cPublic.g_PrjCode + "000..SETRIGHTS )  "
                    ;
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table purchase" + cPublic.g_firmcode + " alter column [INVAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table purchase" + cPublic.g_firmcode + " alter column [EXPENSE]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table purchase" + cPublic.g_firmcode + " alter column [DISCAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table purchase" + cPublic.g_firmcode + " alter column [TAXAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table purchase" + cPublic.g_firmcode + " alter column [CESSAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table purchase" + cPublic.g_firmcode + " alter column [ADISCAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table purchase" + cPublic.g_firmcode + " alter column [ROUNDOFF]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();


                cmd.CommandText = "alter table PRETURN" + cPublic.g_firmcode + " alter column [INVAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table PRETURN" + cPublic.g_firmcode + " alter column [EXPENSE]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table PRETURN" + cPublic.g_firmcode + " alter column [DISCAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table PRETURN" + cPublic.g_firmcode + " alter column [TAXAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table PRETURN" + cPublic.g_firmcode + " alter column [CESSAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table PRETURN" + cPublic.g_firmcode + " alter column [ADISCAMT]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "alter table PRETURN" + cPublic.g_firmcode + " alter column [ROUNDOFF]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "CREATE TABLE [dbo].[Profit" + cPublic.g_firmcode + "] "
                 + "  ( "
                 + "   [Profit] numeric(17,4) NULL "
                 + "  ) ON [PRIMARY]";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "insert into Profit"+cPublic.g_firmcode +"([Profit]) "
                + "  select 1.063 "
                + "  where 0=(select count(*) from [Profit"+cPublic.g_firmcode +"] )";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }



           try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter PROCEDURE SP_INSERT_PITEM" + cPublic.g_firmcode + Constants.vbCrLf
            + "(@orderNo    [bigint],  " + Constants.vbCrLf
            + " @date1       [datetime], " + Constants.vbCrLf
            + " @itemCode    [varchar] (20) = '', " + Constants.vbCrLf
            + " @ITEMNAME    [varchar] (40) = '', " + Constants.vbCrLf
            + " @qty         [numeric] (17,3) = 0, " + Constants.vbCrLf
            + " @foc         [numeric] (17,3) = 0, " + Constants.vbCrLf
            + " @UNIT	 	[varchar](20)=''," + Constants.vbCrLf
            + " @UNITVALUE	[numeric](18,3)=0," + Constants.vbCrLf
            + " @price       [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @netAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @discPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
            + " @discAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @taxPer      [numeric] (17,2) = 0, " + Constants.vbCrLf
            + " @taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @lc          [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @tdiscount   [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @accCode     [varchar] (10) = '', " + Constants.vbCrLf
            + " @postAmount  [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @vatAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @cessPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
            + " @cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @cessPostAmt [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @barCode     [varchar] (1) = '', " + Constants.vbCrLf
            + " @mrp         [numeric] (17,4) = 0, " + Constants.vbCrLf
            + " @priceupdation      bit = 0 " + Constants.vbCrLf
            + ")" + Constants.vbCrLf
            + "AS  " + Constants.vbCrLf
            + "   " + Constants.vbCrLf
            + " INSERT INTO PITEM" + cPublic.g_firmcode + "([ORDERNO], [DATE1], [ITEMCODE],[ITEMNAME], [QTY],[FOC],[UNIT],[UNITVALUE], [PRICE], [NETAMT], " + Constants.vbCrLf
            + "[DISCPER], [DISCAMT], [TAXPER], [TAXAMT], [LC], [TDISCOUNT], [ACCCODE],  [POSTAMOUNT], " + Constants.vbCrLf
            + "[VATAMT],[CESSPER],[CESSAMT],[CESSPOSTAMT],[BARCODE], [MRP]) " + Constants.vbCrLf
            + "values(@orderNo, @date1, @itemCode,@ITEMNAME, @qty,@foc,@unit,@unitvalue, @price, @netAmt, @discPer,@discAmt, @taxPer, @taxAmt, " + Constants.vbCrLf
            + "@lc, @tdiscount,  @accCode,@postAmount, @vatAmt,@cessPer,@cessAmt,@cessPostAmt,@barCode,  @mrp)  " + Constants.vbCrLf
            + " update a set QTY=QTY+((@QTY+@FOC)*@unitvalue),LC=@LC  " + Environment.NewLine
            + " ,rprofit2= " + Environment.NewLine
            + "  case when @priceupdation=0 then rprofit2 " + Environment.NewLine
            + "  else  " + Environment.NewLine
            + " ( " + Environment.NewLine
            + " ( " + Environment.NewLine
            + " @price +  " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 then @price*(taxper/100 ) " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " + " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " ) " + Environment.NewLine
            + " + " + Environment.NewLine
            + " ( " + Environment.NewLine
            + " @price +  " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 then @price*(taxper/100 ) " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " + " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " ) * (select profit/100 from profit" + cPublic.g_firmcode + ") " + Environment.NewLine
            + " ) end " + Environment.NewLine
            + " , " + Environment.NewLine
            + " rprofit1= " + Environment.NewLine
            + "  case when @priceupdation=0 then rprofit1 " + Environment.NewLine
            + "  else  " + Environment.NewLine
            + " ( " + Environment.NewLine
            + " ( " + Environment.NewLine
            + " @price +  " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 then @price*(taxper/100 ) " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " + " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " ) " + Environment.NewLine
            + " + " + Environment.NewLine
            + " ( " + Environment.NewLine
            + " @price +  " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 then @price*(taxper/100 ) " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " + " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " ) * (select profit/100 from profit" + cPublic.g_firmcode + ") " + Environment.NewLine
            + " ) end " + Environment.NewLine
            + " , " + Environment.NewLine
            + " rprofit3= " + Environment.NewLine
            + "  case when @priceupdation=0 then rprofit3 " + Environment.NewLine
            + "  else  " + Environment.NewLine
            + " ( " + Environment.NewLine
            + " ( " + Environment.NewLine
            + " @price +  " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 then @price*(taxper/100 ) " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " + " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " ) " + Environment.NewLine
            + " + " + Environment.NewLine
            + " ( " + Environment.NewLine
            + " @price +  " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 then @price*(taxper/100 ) " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " + " + Environment.NewLine
            + " (  " + Environment.NewLine
            + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " + Environment.NewLine
            + " else 0 end) " + Environment.NewLine
            + " ) * (select profit/100 from profit" + cPublic.g_firmcode + ") " + Environment.NewLine
            + " ) end  from stockmst" + cPublic.g_firmcode   + " a where itemcode=@itemcode " + Constants.vbCrLf
            + "  " + Constants.vbCrLf
            + " ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }

           try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [UNITVALUE]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
             try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [PRICE]    [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [NETAMT]    [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
             try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [DISCAMT]   [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [TAXAMT]    [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [LC]       [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [TDISCOUNT]    [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [POSTAMOUNT]   [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [VATAMT]      [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [CESSAMT]     [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [CESSPOSTAMT]   [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem"+cPublic.g_firmcode  +" alter column [BAR]      [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pitem" + cPublic.g_firmcode + " alter column [MRP]     [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }









            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [UNITVALUE]  [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [PRICE]    [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [NETAMT]    [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [DISCAMT]   [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [TAXAMT]    [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [LC]       [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [TDISCOUNT]    [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [POSTAMOUNT]   [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [VATAMT]      [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [CESSAMT]     [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [CESSPOSTAMT]   [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [BAR]      [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter table pritem" + cPublic.g_firmcode + " alter column [MRP]     [numeric](17,4) NULL ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }


          

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter PROCEDURE SP_UPDATE_PURCHASE" + cPublic.g_firmcode + Constants.vbCrLf
            + "(@date1      [datetime], " + Constants.vbCrLf
            + "@orderNo     [bigint], " + Constants.vbCrLf
            + "@PORDERNO    [bigint]=null, " + Constants.vbCrLf
            + "@suppCode    [varchar] (10) = '', " + Constants.vbCrLf
            + "@suppName    [varchar] (40) = '', " + Constants.vbCrLf
            + "@invNo       [varchar] (20) = '', " + Constants.vbCrLf
            + "@invDate     [datetime], " + Constants.vbCrLf
            + "@invAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
            + "@type        [varchar] (1) = '', " + Constants.vbCrLf
            + "@remarks     [varchar] (50) = '', " + Constants.vbCrLf
            + "@aDisAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
            + "@expense     [numeric] (17,4) = 0, " + Constants.vbCrLf
            + "@roundOff    [numeric] (17,4) = 0, " + Constants.vbCrLf
            + "@disAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
            + "@cstBill     [varchar] (1) = '', " + Constants.vbCrLf
            + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
            + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
            + "@userID      [varchar] (15) = '', " + Constants.vbCrLf
            + "@cForm       [varchar] (1) = '', " + Constants.vbCrLf
            + "@cFromNo     [varchar] (20) = '', " + Constants.vbCrLf
            + "@cFormDate   [datetime], " + Constants.vbCrLf
            + "@cFormBookNO [varchar] (20) = '') " + Constants.vbCrLf
            + "AS UPDATE PURCHASE" + cPublic.g_firmcode + " set [date1] = @date1,[PORDERNO]=@PORDERNO, [SUPPCODE] = @suppCode, [SUPPNAME] = @suppName, " + Constants.vbCrLf
            + "[INVNO] = @invNo, [INVDATE] = @invDate, [INVAMT] = @invAmt, [type] = @type, [REMARKS] = @remarks, " + Constants.vbCrLf
            + "[ADISCAMT] = @aDisAmt, [EXPENSE] = @expense, " + Constants.vbCrLf
            + "[ROUNDOFF] = @roundOff, [DISCAMT] = @disAmt, [CSTBILL] = @cstBill, [TAXAMT] = @taxAmt, [CESSAMT]=@cessAmt, " + Constants.vbCrLf
            + "[USERID] = @userID, [CFORM] = @cForm, [CFORMNO] = @cFromNo, [CFORMDATE] = @cFormDate, " + Constants.vbCrLf
            + "[CFORMBOOKNO] = @cFormBookNO  " + Constants.vbCrLf
            + " " + Constants.vbCrLf
            + "WHERE [ORDERNO] = @orderNo";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }


            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter PROCEDURE SP_INSERT_PRETURN" + cPublic.g_firmcode + Constants.vbCrLf
                      + "(@date1      [datetime], " + Constants.vbCrLf
                      + "@PORDERNO    [bigint] = null, " + Constants.vbCrLf
                      + "@suppCode    [varchar] (10) = '', " + Constants.vbCrLf
                      + "@suppName    [varchar] (40) = '', " + Constants.vbCrLf
                      + "@invNo       [varchar] (20) = '', " + Constants.vbCrLf
                      + "@invDate     [datetime], " + Constants.vbCrLf
                      + "@invAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@type        [varchar] (1) = '', " + Constants.vbCrLf
                      + "@remarks     [varchar] (50) = '', " + Constants.vbCrLf
                      + "@expense     [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@roundOff    [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@disAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@cstBill     [varchar] (1) = '', " + Constants.vbCrLf
                      + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@AdisAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@userID      [varchar] (15) = '', " + Constants.vbCrLf
                      + "@cForm       [varchar] (1) = '', " + Constants.vbCrLf
                      + "@cFromNo     [varchar] (20) = '', " + Constants.vbCrLf
                      + "@cFormDate   [datetime], " + Constants.vbCrLf
                      + "@cFormBookNO [varchar] (20) = '') " + Constants.vbCrLf
                      + "AS " + Constants.vbCrLf
                      + "BEGIN TRAN T1 " + Constants.vbCrLf
                      + "declare @orderNo  [bigint] " + Constants.vbCrLf
                      + "set @orderNo = (select isnull(max(orderno),0)+1 from PRETURN" + cPublic.g_firmcode + " with(xlock)) " + Constants.vbCrLf
                      + "INSERT INTO PRETURN" + cPublic.g_firmcode + "([date1], [ORDERNO],[PORDERNO], [SUPPCODE], [SUPPNAME], [INVNO], [INVDATE], " + Constants.vbCrLf
                      + "[INVAMT], [type], [REMARKS], [ADISCAMT], [EXPENSE],[ROUNDOFF], [DISCAMT], " + Constants.vbCrLf
                      + "[CSTBILL], [TAXAMT], [CESSAMT], [USERID], [CFORM], [CFORMNO], [CFORMDATE], [CFORMBOOKNO])  " + Constants.vbCrLf
                      + " VALUES (@date1, @orderNo,@PORDERNO, @suppCode, @suppName, @invNo, @invDate, @invAmt, @type, " + Constants.vbCrLf
                      + "@remarks, @AdisAmt, @expense,@roundOff, @disAmt, @cstBill, @taxAmt, @cessAmt, @userID, " + Constants.vbCrLf
                      + "@cForm, @cFromNo, @cFormDate, @cFormBookNO ) " + Constants.vbCrLf
                      + "select @orderNo " + Constants.vbCrLf
                      + "COMMIT TRAN T1";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }



             try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter PROCEDURE SP_INSERT_PRITEM" + cPublic.g_firmcode + Constants.vbCrLf
                      + "(@orderNo    [bigint],  " + Constants.vbCrLf
                      + "@date1       [datetime], " + Constants.vbCrLf
                      + "@itemCode    [varchar] (20) = '', " + Constants.vbCrLf
                      + "@ITEMNAME    [varchar] (40) = '', " + Constants.vbCrLf
                      + "@qty         [numeric] (17,3) = 0, " + Constants.vbCrLf
                      + "@foc         [numeric] (17,3) = 0, " + Constants.vbCrLf
                      + "@UNIT	 	[varchar](20)=''," + Constants.vbCrLf
                      + "@UNITVALUE	[numeric](18,3)=0," + Constants.vbCrLf
                      + "@price       [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@netAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@discPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
                      + "@discAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@taxPer      [numeric] (17,2) = 0, " + Constants.vbCrLf
                      + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@lc          [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@tdiscount   [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@accCode     [varchar] (10) = '', " + Constants.vbCrLf
                      + "@postAmount  [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@vatAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@cessPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
                      + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@cessPostAmt [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@barCode     [varchar] (1) = '', " + Constants.vbCrLf
                      + "@mrp         [numeric] (17,2) = 0 " + Constants.vbCrLf
                      + ")" + Constants.vbCrLf
                      + "AS  " + Constants.vbCrLf
                      + "   " + Constants.vbCrLf
                      + " INSERT INTO PRITEM" + cPublic.g_firmcode + "([ORDERNO], [DATE1], [ITEMCODE],[ITEMNAME], [QTY],[FOC],[UNIT],[UNITVALUE], [PRICE], [NETAMT], " + Constants.vbCrLf
                      + "[DISCPER], [DISCAMT], [TAXPER], [TAXAMT], [LC], [TDISCOUNT], [ACCCODE],  [POSTAMOUNT], " + Constants.vbCrLf
                      + "[VATAMT],[CESSPER],[CESSAMT],[CESSPOSTAMT],[BARCODE], [MRP]) " + Constants.vbCrLf
                      + "values(@orderNo, @date1, @itemCode,@ITEMNAME, @qty,@foc,@unit,@unitvalue, @price, @netAmt, @discPer,@discAmt, @taxPer, @taxAmt, " + Constants.vbCrLf
                      + "@lc, @tdiscount,  @accCode,@postAmount, @vatAmt,@cessPer,@cessAmt,@cessPostAmt,@barCode,  @mrp)  " + Constants.vbCrLf
                      + " update a set QTY=QTY-((@QTY+@FOC)*@unitvalue)  from stockmst" + cPublic.g_firmcode + " a where itemcode=@itemcode " + Constants.vbCrLf
                      + "  " + Constants.vbCrLf
                      + " ";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }


            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "alter PROCEDURE SP_UPDATE_PRETURN" + cPublic.g_firmcode + Constants.vbCrLf
                      + "(@date1      [datetime], " + Constants.vbCrLf
                      + "@orderNo     [bigint], " + Constants.vbCrLf
                      + "@PORDERNO    [bigint]=null, " + Constants.vbCrLf
                      + "@suppCode    [varchar] (10) = '', " + Constants.vbCrLf
                      + "@suppName    [varchar] (40) = '', " + Constants.vbCrLf
                      + "@invNo       [varchar] (20) = '', " + Constants.vbCrLf
                      + "@invDate     [datetime], " + Constants.vbCrLf
                      + "@invAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@type        [varchar] (1) = '', " + Constants.vbCrLf
                      + "@remarks     [varchar] (50) = '', " + Constants.vbCrLf
                      + "@aDisAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@expense     [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@roundOff    [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@disAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@cstBill     [varchar] (1) = '', " + Constants.vbCrLf
                      + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
                      + "@userID      [varchar] (15) = '', " + Constants.vbCrLf
                      + "@cForm       [varchar] (1) = '', " + Constants.vbCrLf
                      + "@cFromNo     [varchar] (20) = '', " + Constants.vbCrLf
                      + "@cFormDate   [datetime], " + Constants.vbCrLf
                      + "@cFormBookNO [varchar] (20) = '') " + Constants.vbCrLf
                      + "AS UPDATE PRETURN" + cPublic.g_firmcode + " set [date1] = @date1,[PORDERNO]=@PORDERNO, [SUPPCODE] = @suppCode, [SUPPNAME] = @suppName, " + Constants.vbCrLf
                      + "[INVNO] = @invNo, [INVDATE] = @invDate, [INVAMT] = @invAmt, [type] = @type, [REMARKS] = @remarks, " + Constants.vbCrLf
                      + "[ADISCAMT] = @aDisAmt, [EXPENSE] = @expense, " + Constants.vbCrLf
                      + "[ROUNDOFF] = @roundOff, [DISCAMT] = @disAmt, [CSTBILL] = @cstBill, [TAXAMT] = @taxAmt, [CESSAMT]=@cessAmt, " + Constants.vbCrLf
                      + "[USERID] = @userID, [CFORM] = @cForm, [CFORMNO] = @cFromNo, [CFORMDATE] = @cFormDate, " + Constants.vbCrLf
                      + "[CFORMBOOKNO] = @cFormBookNO  " + Constants.vbCrLf
                      + " " + Constants.vbCrLf
                      + "WHERE [ORDERNO] = @orderNo";
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }



        }

        internal void CheckforUpdates()
        {
            if (MessageBox.Show("Sure to Check For Updates !", cPublic.messagename, MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
        }


    }
}
