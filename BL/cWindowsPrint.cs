﻿
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Xpire.Classes;
using Xpire.Forms;

namespace Xpire.BL
{
    internal class cWindowsPrint
    {
        PrintDocument printPage = new PrintDocument();

        PrintPreviewDialog printprev = new PrintPreviewDialog();

        DataTable dataTable = new DataTable();

     
        string custname = "";
        string custcode = "";
   
        DateTime fromdate = new DateTime();
        DateTime todate = new DateTime();
        DateTime date1 = new DateTime();

        string type = "";
        string leavetype = "";
        string form = "";
        int balleave = 0;
        string SalesNumber = "";
        string leaveapp = "";
        string department = "";
        string cov = "";

        public string DOJ { get; private set; }
        public string DOA { get; private set; }
        public string StaffID { get; private set; }
        public string StaffName { get; private set; }
        public string Department { get; private set; }
        public string Qualifications { get; private set; }
        public string Designation { get; private set; }
        public string Basic { get; private set; }

        public string Allowance { get; private set; }
        public string DrvLicense { get; private set; }
        public string PrevEmp { get; private set; }
        public string YOP { get; private set; }
        public string PhoneNo { get; private set; }
        public string MobileNo { get; private set; }
        public string Email { get; private set; }
        public string DOB { get; private set; }
        public string Marital { get; private set; }
        public string Gender { get; private set; }
        public string Spouse { get; private set; }
        public string Religion { get; private set; }
        public string Dependant { get; private set; }
        public string Nationality { get; private set; }
        public string FatherName { get; private set; }
        public string MotherName { get; private set; }
        public string NName { get; private set; }
        public string NRelation { get; private set; }
        public string NMobileNo { get; private set; }
        public string NPhoneNo { get; private set; }
        public string NPAddress1 { get; private set; }
        public string NPAddress2 { get; private set; }
        public string NPAddress3 { get; private set; }
        public string NPAddress4 { get; private set; }
        public string NTAddress1 { get; private set; }
        public string NTAddress2 { get; private set; }
        public string NTAddress3 { get; private set; }
        public string NTAddress4 { get; private set; }
        public string DName { get; private set; }
        public string DRelation { get; private set; }
        public string DMobileNo { get; private set; }
        public string DEmail { get; private set; }
        public string DPAddress1 { get; private set; }
        public string DPAddress2 { get; private set; }
        public string DPAddress3 { get; private set; }
        public string DPAddress4 { get; private set; }
        public string DTAddress1 { get; private set; }
        public string DTAddress2 { get; private set; }
        public string DTAddress3 { get; private set; }
        public string DTAddress4 { get; private set; }
        public string DPhoneNo { get; private set; }
       
        public string NEmail { get; private set; }
        public string IssDate { get; private set; }
        public string ExpDate { get; private set; }
        public string DocumentNo { get; private set; }
        public string PlaceofIss { get; private set; }
        

        internal cWindowsPrint(string MasterTable, string SubTable, string salesnumber)
        {
            SalesNumber = salesnumber;

            
            if (ReadData(MasterTable, SubTable, salesnumber) == false) { return; }

            IEnumerable<PaperSize> paperSizes = printPage.PrinterSettings.PaperSizes.Cast<PaperSize>();
            PaperSize sizeA4 = paperSizes.First<PaperSize>(size => size.Kind == PaperKind.A4); // setting paper size to A4 size
            //recordDoc.DefaultPageSettings.PaperSize = sizeA4;
            printPage.PrinterSettings.DefaultPageSettings.PaperSize = sizeA4;
            printPage.PrintPage += printPage_PrintPage;
            printprev.Document = printPage;
            printprev.ShowDialog();

            //printPage.Print(); 
        }


        internal cWindowsPrint(string MasterTable, string SubTable, string salesnumber,string Type)
        {
            SalesNumber = salesnumber;
            type = Type;
            if (type == "Employee")
            { if (ReadEmplyeeData(MasterTable, SubTable, salesnumber) == false) { return; } }
            else if (type == "Rejoining")
            { if (ReadRejoinigData(MasterTable, SubTable, salesnumber) == false) { return; } }
            else if (type == "Leave Application")
            { if (ReadData(MasterTable, SubTable, salesnumber) == false) { return; } }

            IEnumerable<PaperSize> paperSizes = printPage.PrinterSettings.PaperSizes.Cast<PaperSize>();
            PaperSize sizeA4 = paperSizes.First<PaperSize>(size => size.Kind == PaperKind.A4); // setting paper size to A4 size
            //recordDoc.DefaultPageSettings.PaperSize = sizeA4;
            printPage.PrinterSettings.DefaultPageSettings.PaperSize = sizeA4;
            printPage.PrintPage += printPage_PrintPage;
            printprev.Document = printPage;

            //printPage.Print();
            printprev.ShowDialog();

            //printPage.Print(); 
        }

        void printPage_PrintPage(object sender, PrintPageEventArgs e)
        {
            
                printleave(e,type);
           
        }
        int next = 0;
        int lineheight2 = 0;
        int lineheight1 = 0;

        System.Drawing.Image img;
        Point loc;
        private string remarks;

        public void printleave(PrintPageEventArgs e,string type)
        {
            #region Rejoining

            if (type == "Rejoining")
            {
                Font printFont = new Font("Arial", 12);




                int yAxis = 0;
                //printPage.PrinterSettings.DefaultPageSettings.PaperSize.Width = 827;
                //printPage.PrinterSettings.DefaultPageSettings.PaperSize.Height = 1169;

                int Width = printPage.PrinterSettings.DefaultPageSettings.PaperSize.Width;
                int Height = printPage.PrinterSettings.DefaultPageSettings.PaperSize.Height;


                Graphics g = e.Graphics;

                // If you set printDocumet.OriginAtMargins to 'false' this event 
                // will print the largest rectangle your printer is physically 
                // capable of. This is often 1/8" - 1/4" from each page edge.
                // ----------
                // If you set printDocument.OriginAtMargins to 'false' this event
                // will print the largest rectangle permitted by the currently 
                // configured page margins. By default the page margins are 
                // usually 1" from each page edge but can be configured by the end
                // user or overridden in your code.
                // (ex: printDocument.DefaultPageSettings.Margins)

                // Grab a copy of our "soft margins" (configured printer settings)
                // Defaults to 1 inch margins, but could be configured otherwise by 
                // the end user. You can also specify some default page margins in 
                // your printDocument.DefaultPageSetting properties.
                RectangleF marginBounds = e.MarginBounds;

                // Grab a copy of our "hard margins" (printer's capabilities) 
                // This varies between printer models. Software printers like 
                // CutePDF will have no "physical limitations" and so will return 
                // the full page size 850,1100 for a letter page size.
                RectangleF printableArea = e.PageSettings.PrintableArea;

                // If we are print to a print preview control, the origin won't have 
                // been automatically adjusted for the printer's physical limitations. 
                // So let's adjust the origin for preview to reflect the printer's 
                // hard margins.
                //if (printAction == PrintAction.PrintToPreview)
                g.TranslateTransform(printableArea.X, printableArea.Y);

                // Are we using soft margins or hard margins? Lets grab the correct 
                // width/height from either the soft/hard margin rectangles. The 
                // hard margins are usually a little wider than the soft margins.
                // ----------
                // Note: Margins are automatically applied to the rotated page size 
                // when the page is set to landscape, but physical hard margins are 
                // not (the printer is not physically rotating any mechanics inside, 
                // the paper still travels through the printer the same way. So we 
                // rotate in software for landscape)
                int availableWidth = (int)Math.Floor(printPage.OriginAtMargins ? marginBounds.Width : (e.PageSettings.Landscape ? printableArea.Height : printableArea.Width));
                int availableHeight = (int)Math.Floor(printPage.OriginAtMargins ? marginBounds.Height : (e.PageSettings.Landscape ? printableArea.Width : printableArea.Height));

                // Draw our rectangle which will either be the soft margin rectangle 
                // or the hard margin (printer capabilities) rectangle.
                // ----------
                // Note: we adjust the width and height minus one as it is a zero, 
                // zero based co-ordinates system. This will put the rectangle just 
                // inside the available width and height.
                //g.DrawRectangle(Pens.Black, 0, 0, availableWidth - 1, availableHeight - 1);

                yAxis += 10;
                System.Drawing.Image img = cPublic.image;
                Point loc = new Point(40, yAxis);
                e.Graphics.DrawImage(img, loc);



                yAxis += img.Height + 10;

                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                SizeF f1 = e.Graphics.MeasureString(cPublic.g_firmname, printFont);



                string value = "Leave Application";

                yAxis += 20;

                f1 = e.Graphics.MeasureString(value, printFont);
                e.Graphics.DrawString(value, new Font("Agency FB", 20, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });


                printFont = new Font("Century Gothic", 11);
                yAxis += 50;
                e.Graphics.DrawString("Department :" + department, printFont, Brushes.Black, new PointF() { X = Width - 250, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("Date :" + System.DateTime.Now.Date.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = Width - 250, Y = yAxis });


                e.Graphics.DrawString("Staff Id : " + custcode, printFont, Brushes.Black, new PointF() { X = 60, Y = yAxis - 20 });
                e.Graphics.DrawString("Staff Name : " + custname, printFont, Brushes.Black, new PointF() { X = 60, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("Date Of Joining :" + date1.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 60, Y = yAxis });



                //e.Graphics.DrawString("Date" , printFont, Brushes.Black, new PointF() { X = Width - 200, Y = yAxis - 20 });
                //e.Graphics.DrawString(":" + date1.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = Width - 150, Y = yAxis - 20 });

                yAxis += 50;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Type of leave", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                e.Graphics.DrawString("Leave From", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 235, Y = yAxis });
                e.Graphics.DrawString("Leave To", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 340, Y = yAxis });
                e.Graphics.DrawString("Rejoining Date", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 440, Y = yAxis });
                e.Graphics.DrawString("Excess Leave", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 625, Y = yAxis });


                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });


                int lineheight2 = Height - 160;
                if (form == "A") { lineheight2 -= 100; }
                lineheight2 += 30;

                int lineheight1 = yAxis - 30;

                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis + 140 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 230, Y = lineheight1 }, new Point() { X = 230, Y = yAxis + 30 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 330, Y = lineheight1 }, new Point() { X = 330, Y = yAxis + 30 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 430, Y = lineheight1 }, new Point() { X = 430, Y = yAxis + 30 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 600, Y = lineheight1 }, new Point() { X = 600, Y = yAxis + 30 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis + 140 });

                yAxis += 10;
                e.Graphics.DrawString(leavetype, printFont, Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(fromdate.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 240, Y = yAxis });
                e.Graphics.DrawString(todate.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 340, Y = yAxis });
                e.Graphics.DrawString(leaveapp, new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 510, Y = yAxis });
                e.Graphics.DrawString(balleave.ToString(), new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Red, new PointF() { X = 670, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Country to be visited:  " + cov, new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });

                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Period Of Stay:", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Remarks:", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                yAxis += 40;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 20;
                e.Graphics.DrawString("Leave History", new Font("Century Gothic", 12, FontStyle.Bold), Brushes.Black, new PointF() { X = 60, Y = yAxis });

                yAxis += 30;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Type of Leave", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                e.Graphics.DrawString("Leave From", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 235, Y = yAxis });
                e.Graphics.DrawString("Leave To", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 340, Y = yAxis });
                e.Graphics.DrawString("No: of Leave Taken", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 440, Y = yAxis });
                e.Graphics.DrawString("Excess Leave Taken", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 610, Y = yAxis });



                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });



                lineheight2 = Height - 160;
                if (form == "A") { lineheight2 -= 100; }
                lineheight2 += 30;

                lineheight1 = yAxis - 30;

                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 230, Y = lineheight1 }, new Point() { X = 230, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 330, Y = lineheight1 }, new Point() { X = 330, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 430, Y = lineheight1 }, new Point() { X = 430, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 600, Y = lineheight1 }, new Point() { X = 600, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis });

                printFont = new Font("Century Gothic", 10);
                int vCount = 1;
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    if (vCount == 1)
                    { yAxis += 10; }
                    else { yAxis += 20; }

                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 230, Y = lineheight1 }, new Point() { X = 230, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 330, Y = lineheight1 }, new Point() { X = 330, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 430, Y = lineheight1 }, new Point() { X = 430, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 600, Y = lineheight1 }, new Point() { X = 600, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis + 20 });

                    e.Graphics.DrawString(dataRow["LeaveType"].ToString(), printFont, Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(Strings.Format(dataRow["From"], "dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 240, Y = yAxis });
                    e.Graphics.DrawString(Strings.Format(dataRow["to"], "dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 340, Y = yAxis });
                    e.Graphics.DrawString(dataRow["Leave Approved"].ToString(), printFont, Brushes.Black, new PointF() { X = 510, Y = yAxis });
                    e.Graphics.DrawString(dataRow["Excess Leave Taken"].ToString(), printFont, Brushes.Black, new PointF() { X = 670, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                    //e.Graphics.DrawString(Strings.Format(dataRow["Excess Leave Taken"], "#0.000"), printFont, Brushes.Black, new PointF() { X = 359, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(Convert.ToDouble(dataRow["Price"]) + Convert.ToDouble(dataRow["DISCAMT"]) / Convert.ToDouble(dataRow["QTY"]), "#0.00"), printFont, Brushes.Black, new PointF() { X = 429, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    ////e.Graphics.DrawString(Strings.Format(Convert.ToDouble(dataRow["netvalue"]) + Convert.ToDouble(dataRow["DISCAMT"]), "#0.00"), printFont, Brushes.Black, new PointF() { X = 420, Y = yAxis });
                    //e.Graphics.DrawString(Strings.Format(dataRow["discamt"], "#0.00"), printFont, Brushes.Black, new PointF() { X = 499, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(Convert.ToDouble(dataRow["netvalue"]), "#0.00"), printFont, Brushes.Black, new PointF() { X = 579, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(dataRow["taxamt"], "#0.00"), printFont, Brushes.Black, new PointF() { X = 659, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(dataRow["cessamt"], "#0.00"), printFont, Brushes.Black, new PointF() { X = 739, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(dataRow["NetAmt"], "#0.00"), printFont, Brushes.Black, new PointF() { X = 820, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });

                    //prtQty = prtQty + Convert.ToDouble(dataRow["qty"]);
                    //prtNetVlu = prtNetVlu + Convert.ToDouble(dataRow["netvalue"]);
                    ////prtGv = prtGv + Convert.ToDouble(dataRow["netvalue"]) + Convert.ToDouble(dataRow["DISCAMT"]);

                    //prtTaxamt = prtTaxamt + Convert.ToDouble(dataRow["taxamt"]);
                    //prtCessamt = prtCessamt + Convert.ToDouble(dataRow["cessamt"]);
                    //prtNetamt = prtNetamt + Convert.ToDouble(dataRow["NetAmt"]);
                    //prtDisc = prtDisc + Convert.ToDouble(dataRow["discamt"]);

                    //vCount += 1;
                }


                yAxis += 30;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Applicant's Signature", printFont, Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString("Approved by Dept. Head", printFont, Brushes.Black, new PointF() { X = 300, Y = yAxis });
                e.Graphics.DrawString("Endorsed by Human Resource" + Environment.NewLine + "Dep.", printFont, Brushes.Black, new PointF() { X = 530, Y = yAxis });

                lineheight1 = yAxis + 140;
                //yAxis += 50;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis - 10 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 290, Y = lineheight1 }, new Point() { X = 290, Y = yAxis - 10 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 520, Y = lineheight1 }, new Point() { X = 520, Y = yAxis - 10 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis - 10 });
                //yAxis = Height - 160;
                //if (form == "A") { yAxis -= 100; } 
                yAxis += 110;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Date", printFont, Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString("Date", printFont, Brushes.Black, new PointF() { X = 300, Y = yAxis });
                e.Graphics.DrawString("Date", printFont, Brushes.Black, new PointF() { X = 530, Y = yAxis });

                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 30;
                e.Graphics.DrawString("Note", printFont, Brushes.Black, new PointF() { X = 60, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("1. Leave will not granted if application is not submitted 40 Days in advance,except sick leave", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("2. Application for annual leave should be submitted 40 days before leave commences", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("3. Other than annual leaves, please attahc relevant supporting documnets for reference.", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("4. If sick leave exceeds two days, medical proof should be attched.", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("5. Failure of applicant to resume duty after the leave period will be deemed negligence of ", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("   duty and may be subject to summary dismissal by the Company.", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                //e.Graphics.DrawString(Strings.Format(prtNetVlu, "#0.00"), printFont, Brushes.Black, new PointF() { X = 579, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                //e.Graphics.DrawString(Strings.Format(prtTaxamt, "#0.00"), printFont, Brushes.Black, new PointF() { X = 659, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                //e.Graphics.DrawString(Strings.Format(prtCessamt, "#0.00"), printFont, Brushes.Black, new PointF() { X = 739, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                //e.Graphics.DrawString(Strings.Format(prtNetamt, "#0.00"), printFont, Brushes.Black, new PointF() { X = 820, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });

                //yAxis += 20;
                //e.Graphics.DrawLine(Pens.Black, new Point() { X = 5, Y = yAxis }, new Point() { X = Width - 5, Y = yAxis });

                //printFont = new Font("Arial", 11);
                //yAxis += 10;
                //e.Graphics.DrawString("Round Off", printFont, Brushes.Black, new PointF() { X = Width - 250, Y = yAxis });
                //e.Graphics.DrawString(":", printFont, Brushes.Black, new PointF() { X = Width - 150, Y = yAxis });
                //e.Graphics.DrawString(Strings.Format(roundoff, "#0.00"), printFont, Brushes.Black, new PointF() { X = Width - 10, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });

                //yAxis += 20;
                //e.Graphics.DrawString("Grand Total" , printFont, Brushes.Black, new PointF() { X = Width - 250, Y = yAxis });
                //e.Graphics.DrawString(":", printFont, Brushes.Black, new PointF() { X = Width - 150, Y = yAxis });
                //e.Graphics.DrawString(Strings.Format(billamt, "#0.00"), printFont, Brushes.Black, new PointF() { X = Width - 10, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });


                //yAxis += 20;
                //fssgen.fssgen gen = new fssgen.fssgen();
                //double lng = Convert.ToInt32(billamt);
                //e.Graphics.DrawString("Amount In Words : Rs. " + gen.NumToChars(ref lng) + " Only", printFont, Brushes.Black, new PointF() { X =10, Y = yAxis });

                //yAxis += 20;           
                //e.Graphics.DrawLine(Pens.Black, new Point() { X = 5, Y = yAxis }, new Point() { X = Width - 5, Y = yAxis });

                //if (form.Trim() == "A")
                //{
                //    yAxis += 10;
                //    f1 = e.Graphics.MeasureString("DECLARATION", printFont);
                //    e.Graphics.DrawString("DECLARATION", printFont, Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });

                //    yAxis += 20;
                //    e.Graphics.DrawLine(Pens.Black, (Width - f1.Width) / 2, yAxis, ((Width - f1.Width) / 2) + f1.Width, yAxis);

                //    printFont = new Font("Arial", 9);
                //    yAxis += 10;
                //    e.Graphics.DrawString("Certified that all the particulars shown in the above Tax Invoice are true and correct in all respects and the  goods on which the tax", printFont, Brushes.Black, new PointF() { X = 10, Y = yAxis });

                //    yAxis += 20;
                //    e.Graphics.DrawString("charged and collected are in accordance with the provisions of the KVAT Act 2003 and the rules made thereunder.  It is also  certified", printFont, Brushes.Black, new PointF() { X = 10, Y = yAxis });
                //    yAxis += 20;
                //    e.Graphics.DrawString("that my/our Registration under KVAT Act 2003 is not subject to any suspension/cancellation and it is valid as on the date of this bill.", printFont, Brushes.Black, new PointF() { X = 10, Y = yAxis });

                //    printFont = new Font("Arial", 10);
                //    yAxis += 20;
                //    e.Graphics.DrawLine(Pens.Black, new Point() { X = 5, Y = yAxis }, new Point() { X = Width - 5, Y = yAxis });
                //}

                //yAxis += 10;
                //f1 = e.Graphics.MeasureString("For " + cPublic.g_firmname + "   ", printFont);
                //e.Graphics.DrawString("For " + cPublic.g_firmname + "   ", printFont, Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });


                e.HasMorePages = false;
            }
            #endregion

            #region Leave Application

            else if (type== "Leave Application")
            {
                Font printFont = new Font("Arial", 12);




                int  yAxis = 0;
                //printPage.PrinterSettings.DefaultPageSettings.PaperSize.Width = 827;
                //printPage.PrinterSettings.DefaultPageSettings.PaperSize.Height = 1169;

                int Width = printPage.PrinterSettings.DefaultPageSettings.PaperSize.Width;
                int Height = printPage.PrinterSettings.DefaultPageSettings.PaperSize.Height;


                Graphics g = e.Graphics;

                // If you set printDocumet.OriginAtMargins to 'false' this event 
                // will print the largest rectangle your printer is physically 
                // capable of. This is often 1/8" - 1/4" from each page edge.
                // ----------
                // If you set printDocument.OriginAtMargins to 'false' this event
                // will print the largest rectangle permitted by the currently 
                // configured page margins. By default the page margins are 
                // usually 1" from each page edge but can be configured by the end
                // user or overridden in your code.
                // (ex: printDocument.DefaultPageSettings.Margins)

                // Grab a copy of our "soft margins" (configured printer settings)
                // Defaults to 1 inch margins, but could be configured otherwise by 
                // the end user. You can also specify some default page margins in 
                // your printDocument.DefaultPageSetting properties.
                RectangleF marginBounds = e.MarginBounds;

                // Grab a copy of our "hard margins" (printer's capabilities) 
                // This varies between printer models. Software printers like 
                // CutePDF will have no "physical limitations" and so will return 
                // the full page size 850,1100 for a letter page size.
                RectangleF printableArea = e.PageSettings.PrintableArea;

                // If we are print to a print preview control, the origin won't have 
                // been automatically adjusted for the printer's physical limitations. 
                // So let's adjust the origin for preview to reflect the printer's 
                // hard margins.
                //if (printAction == PrintAction.PrintToPreview)
                g.TranslateTransform(printableArea.X, printableArea.Y);

                // Are we using soft margins or hard margins? Lets grab the correct 
                // width/height from either the soft/hard margin rectangles. The 
                // hard margins are usually a little wider than the soft margins.
                // ----------
                // Note: Margins are automatically applied to the rotated page size 
                // when the page is set to landscape, but physical hard margins are 
                // not (the printer is not physically rotating any mechanics inside, 
                // the paper still travels through the printer the same way. So we 
                // rotate in software for landscape)
                int availableWidth = (int)Math.Floor(printPage.OriginAtMargins ? marginBounds.Width : (e.PageSettings.Landscape ? printableArea.Height : printableArea.Width));
                int availableHeight = (int)Math.Floor(printPage.OriginAtMargins ? marginBounds.Height : (e.PageSettings.Landscape ? printableArea.Width : printableArea.Height));

                // Draw our rectangle which will either be the soft margin rectangle 
                // or the hard margin (printer capabilities) rectangle.
                // ----------
                // Note: we adjust the width and height minus one as it is a zero, 
                // zero based co-ordinates system. This will put the rectangle just 
                // inside the available width and height.
                //g.DrawRectangle(Pens.Black, 0, 0, availableWidth - 1, availableHeight - 1);

                yAxis += 10;
                System.Drawing.Image img = cPublic.image;
                Point loc = new Point(40, yAxis);
                e.Graphics.DrawImage(img, loc);



                yAxis += img.Height + 10;

                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                SizeF f1 = e.Graphics.MeasureString(cPublic.g_firmname, printFont);



                string value = "Leave Application";

                yAxis += 20;

                f1 = e.Graphics.MeasureString(value, printFont);
                e.Graphics.DrawString(value, new Font("Agency FB", 20, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });


                printFont = new Font("Century Gothic", 11);
                yAxis += 50;
                e.Graphics.DrawString("Department :" + department, printFont, Brushes.Black, new PointF() { X = Width - 250, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("Date :" + System.DateTime.Now.Date.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = Width - 250, Y = yAxis });


                e.Graphics.DrawString("Staff Id : " + custcode, printFont, Brushes.Black, new PointF() { X = 60, Y = yAxis - 20 });
                e.Graphics.DrawString("Staff Name : " + custname, printFont, Brushes.Black, new PointF() { X = 60, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("Date Of Joining :" + date1.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 60, Y = yAxis });



                //e.Graphics.DrawString("Date" , printFont, Brushes.Black, new PointF() { X = Width - 200, Y = yAxis - 20 });
                //e.Graphics.DrawString(":" + date1.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = Width - 150, Y = yAxis - 20 });

                yAxis += 50;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Type of leave", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                e.Graphics.DrawString("Leave From", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 235, Y = yAxis });
                e.Graphics.DrawString("Leave To", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 340, Y = yAxis });
                e.Graphics.DrawString("Total Leave Applied", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 440, Y = yAxis });
                e.Graphics.DrawString("Balance Leave", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 625, Y = yAxis });


                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });


                int lineheight2 = Height - 160;
                if (form == "A") { lineheight2 -= 100; }
                lineheight2 += 30;

                int lineheight1 = yAxis - 30;

                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis + 140 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 230, Y = lineheight1 }, new Point() { X = 230, Y = yAxis + 30 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 330, Y = lineheight1 }, new Point() { X = 330, Y = yAxis + 30 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 430, Y = lineheight1 }, new Point() { X = 430, Y = yAxis + 30 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 600, Y = lineheight1 }, new Point() { X = 600, Y = yAxis + 30 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis + 140 });

                yAxis += 10;
                e.Graphics.DrawString(leavetype, printFont, Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(fromdate.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 240, Y = yAxis });
                e.Graphics.DrawString(todate.ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 340, Y = yAxis });
                e.Graphics.DrawString(leaveapp, new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 510, Y = yAxis });
                e.Graphics.DrawString(balleave.ToString(), new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Red, new PointF() { X = 670, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Country to be visited:  " + cov, new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });

                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Period Of Stay:", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Remarks:" + remarks, new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                yAxis += 40;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 20;
                e.Graphics.DrawString("Leave History", new Font("Century Gothic", 12, FontStyle.Bold), Brushes.Black, new PointF() { X = 60, Y = yAxis });

                yAxis += 30;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Type of Leave", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                e.Graphics.DrawString("Leave From", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 235, Y = yAxis });
                e.Graphics.DrawString("Leave To", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 340, Y = yAxis });
                e.Graphics.DrawString("No: of Leave Taken", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 440, Y = yAxis });
                e.Graphics.DrawString("Excess Leave Taken", new Font("Century Gothic", 10, FontStyle.Bold), Brushes.Black, new PointF() { X = 610, Y = yAxis });



                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });



                lineheight2 = Height - 160;
                if (form == "A") { lineheight2 -= 100; }
                lineheight2 += 30;

                lineheight1 = yAxis - 30;

                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 230, Y = lineheight1 }, new Point() { X = 230, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 330, Y = lineheight1 }, new Point() { X = 330, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 430, Y = lineheight1 }, new Point() { X = 430, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 600, Y = lineheight1 }, new Point() { X = 600, Y = yAxis });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis });

                printFont = new Font("Century Gothic", 10);
                int vCount = 1;
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    if (vCount == 1)
                    { yAxis += 10; }
                    else { yAxis += 20; }

                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 230, Y = lineheight1 }, new Point() { X = 230, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 330, Y = lineheight1 }, new Point() { X = 330, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 430, Y = lineheight1 }, new Point() { X = 430, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 600, Y = lineheight1 }, new Point() { X = 600, Y = yAxis + 20 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis + 20 });

                    e.Graphics.DrawString(dataRow["LeaveType"].ToString(), printFont, Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(Strings.Format(dataRow["From"], "dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 240, Y = yAxis });
                    e.Graphics.DrawString(Strings.Format(dataRow["to"], "dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 340, Y = yAxis });
                    e.Graphics.DrawString(dataRow["Leave Approved"].ToString(), printFont, Brushes.Black, new PointF() { X = 510, Y = yAxis });
                    e.Graphics.DrawString(dataRow["Excess Leave Taken"].ToString(), printFont, Brushes.Black, new PointF() { X = 670, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                    //e.Graphics.DrawString(Strings.Format(dataRow["Excess Leave Taken"], "#0.000"), printFont, Brushes.Black, new PointF() { X = 359, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(Convert.ToDouble(dataRow["Price"]) + Convert.ToDouble(dataRow["DISCAMT"]) / Convert.ToDouble(dataRow["QTY"]), "#0.00"), printFont, Brushes.Black, new PointF() { X = 429, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    ////e.Graphics.DrawString(Strings.Format(Convert.ToDouble(dataRow["netvalue"]) + Convert.ToDouble(dataRow["DISCAMT"]), "#0.00"), printFont, Brushes.Black, new PointF() { X = 420, Y = yAxis });
                    //e.Graphics.DrawString(Strings.Format(dataRow["discamt"], "#0.00"), printFont, Brushes.Black, new PointF() { X = 499, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(Convert.ToDouble(dataRow["netvalue"]), "#0.00"), printFont, Brushes.Black, new PointF() { X = 579, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(dataRow["taxamt"], "#0.00"), printFont, Brushes.Black, new PointF() { X = 659, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(dataRow["cessamt"], "#0.00"), printFont, Brushes.Black, new PointF() { X = 739, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                    //e.Graphics.DrawString(Strings.Format(dataRow["NetAmt"], "#0.00"), printFont, Brushes.Black, new PointF() { X = 820, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });

                    //prtQty = prtQty + Convert.ToDouble(dataRow["qty"]);
                    //prtNetVlu = prtNetVlu + Convert.ToDouble(dataRow["netvalue"]);
                    ////prtGv = prtGv + Convert.ToDouble(dataRow["netvalue"]) + Convert.ToDouble(dataRow["DISCAMT"]);

                    //prtTaxamt = prtTaxamt + Convert.ToDouble(dataRow["taxamt"]);
                    //prtCessamt = prtCessamt + Convert.ToDouble(dataRow["cessamt"]);
                    //prtNetamt = prtNetamt + Convert.ToDouble(dataRow["NetAmt"]);
                    //prtDisc = prtDisc + Convert.ToDouble(dataRow["discamt"]);

                    //vCount += 1;
                }


                yAxis += 30;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Applicant's Signature", printFont, Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString("Approved by Dept. Head", printFont, Brushes.Black, new PointF() { X = 300, Y = yAxis });
                e.Graphics.DrawString("Endorsed by Human Resource" + Environment.NewLine + "Dep.", printFont, Brushes.Black, new PointF() { X = 530, Y = yAxis });

                lineheight1 = yAxis + 140;
                //yAxis += 50;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis - 10 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 290, Y = lineheight1 }, new Point() { X = 290, Y = yAxis - 10 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 520, Y = lineheight1 }, new Point() { X = 520, Y = yAxis - 10 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis - 10 });
                //yAxis = Height - 160;
                //if (form == "A") { yAxis -= 100; } 
                yAxis += 110;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Date", printFont, Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString("Date", printFont, Brushes.Black, new PointF() { X = 300, Y = yAxis });
                e.Graphics.DrawString("Date", printFont, Brushes.Black, new PointF() { X = 530, Y = yAxis });

                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 30;
                e.Graphics.DrawString("Note", printFont, Brushes.Black, new PointF() { X = 60, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("1. Leave will not granted if application is not submitted 40 Days in advance,except sick leave", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("2. Application for annual leave should be submitted 40 days before leave commences", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("3. Other than annual leaves, please attahc relevant supporting documnets for reference.", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("4. If sick leave exceeds two days, medical proof should be attched.", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("5. Failure of applicant to resume duty after the leave period will be deemed negligence of ", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawString("   duty and may be subject to summary dismissal by the Company.", new Font("Century Gothic", 10), Brushes.Black, new PointF() { X = 90, Y = yAxis });
                //e.Graphics.DrawString(Strings.Format(prtNetVlu, "#0.00"), printFont, Brushes.Black, new PointF() { X = 579, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                //e.Graphics.DrawString(Strings.Format(prtTaxamt, "#0.00"), printFont, Brushes.Black, new PointF() { X = 659, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                //e.Graphics.DrawString(Strings.Format(prtCessamt, "#0.00"), printFont, Brushes.Black, new PointF() { X = 739, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });
                //e.Graphics.DrawString(Strings.Format(prtNetamt, "#0.00"), printFont, Brushes.Black, new PointF() { X = 820, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });

                //yAxis += 20;
                //e.Graphics.DrawLine(Pens.Black, new Point() { X = 5, Y = yAxis }, new Point() { X = Width - 5, Y = yAxis });

                //printFont = new Font("Arial", 11);
                //yAxis += 10;
                //e.Graphics.DrawString("Round Off", printFont, Brushes.Black, new PointF() { X = Width - 250, Y = yAxis });
                //e.Graphics.DrawString(":", printFont, Brushes.Black, new PointF() { X = Width - 150, Y = yAxis });
                //e.Graphics.DrawString(Strings.Format(roundoff, "#0.00"), printFont, Brushes.Black, new PointF() { X = Width - 10, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });

                //yAxis += 20;
                //e.Graphics.DrawString("Grand Total" , printFont, Brushes.Black, new PointF() { X = Width - 250, Y = yAxis });
                //e.Graphics.DrawString(":", printFont, Brushes.Black, new PointF() { X = Width - 150, Y = yAxis });
                //e.Graphics.DrawString(Strings.Format(billamt, "#0.00"), printFont, Brushes.Black, new PointF() { X = Width - 10, Y = yAxis }, new StringFormat() { Alignment = StringAlignment.Far });


                //yAxis += 20;
                //fssgen.fssgen gen = new fssgen.fssgen();
                //double lng = Convert.ToInt32(billamt);
                //e.Graphics.DrawString("Amount In Words : Rs. " + gen.NumToChars(ref lng) + " Only", printFont, Brushes.Black, new PointF() { X =10, Y = yAxis });

                //yAxis += 20;           
                //e.Graphics.DrawLine(Pens.Black, new Point() { X = 5, Y = yAxis }, new Point() { X = Width - 5, Y = yAxis });

                //if (form.Trim() == "A")
                //{
                //    yAxis += 10;
                //    f1 = e.Graphics.MeasureString("DECLARATION", printFont);
                //    e.Graphics.DrawString("DECLARATION", printFont, Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });

                //    yAxis += 20;
                //    e.Graphics.DrawLine(Pens.Black, (Width - f1.Width) / 2, yAxis, ((Width - f1.Width) / 2) + f1.Width, yAxis);

                //    printFont = new Font("Arial", 9);
                //    yAxis += 10;
                //    e.Graphics.DrawString("Certified that all the particulars shown in the above Tax Invoice are true and correct in all respects and the  goods on which the tax", printFont, Brushes.Black, new PointF() { X = 10, Y = yAxis });

                //    yAxis += 20;
                //    e.Graphics.DrawString("charged and collected are in accordance with the provisions of the KVAT Act 2003 and the rules made thereunder.  It is also  certified", printFont, Brushes.Black, new PointF() { X = 10, Y = yAxis });
                //    yAxis += 20;
                //    e.Graphics.DrawString("that my/our Registration under KVAT Act 2003 is not subject to any suspension/cancellation and it is valid as on the date of this bill.", printFont, Brushes.Black, new PointF() { X = 10, Y = yAxis });

                //    printFont = new Font("Arial", 10);
                //    yAxis += 20;
                //    e.Graphics.DrawLine(Pens.Black, new Point() { X = 5, Y = yAxis }, new Point() { X = Width - 5, Y = yAxis });
                //}

                //yAxis += 10;
                //f1 = e.Graphics.MeasureString("For " + cPublic.g_firmname + "   ", printFont);
                //e.Graphics.DrawString("For " + cPublic.g_firmname + "   ", printFont, Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });


                e.HasMorePages = false;
            }
            #endregion

            #region Print Employee

            else if (type == "Employee")
            {
                Font printFont = new Font("Arial", 12);


                int yAxis = 0;


                int Width = printPage.PrinterSettings.DefaultPageSettings.PaperSize.Width;
                int Height = printPage.PrinterSettings.DefaultPageSettings.PaperSize.Height;


                Graphics g = e.Graphics;
                // If you set printDocumet.OriginAtMargins to 'false' this event 
                // will print the largest rectangle your printer is physically 
                // capable of. This is often 1/8" - 1/4" from each page edge.
                // ----------
                // If you set printDocument.OriginAtMargins to 'false' this event
                // will print the largest rectangle permitted by the currently 
                // configured page margins. By default the page margins are 
                // usually 1" from each page edge but can be configured by the end
                // user or overridden in your code.
                // (ex: printDocument.DefaultPageSettings.Margins)

                // Grab a copy of our "soft margins" (configured printer settings)
                // Defaults to 1 inch margins, but could be configured otherwise by 
                // the end user. You can also specify some default page margins in 
                // your printDocument.DefaultPageSetting properties.
                RectangleF marginBounds = e.MarginBounds;

                // Grab a copy of our "hard margins" (printer's capabilities) 
                // This varies between printer models. Software printers like 
                // CutePDF will have no "physical limitations" and so will return 
                // the full page size 850,1100 for a letter page size.
                RectangleF printableArea = e.PageSettings.PrintableArea;

                // If we are print to a print preview control, the origin won't have 
                // been automatically adjusted for the printer's physical limitations. 
                // So let's adjust the origin for preview to reflect the printer's 
                // hard margins.
                //if (printAction == PrintAction.PrintToPreview)
                g.TranslateTransform(printableArea.X, printableArea.Y);

                // Are we using soft margins or hard margins? Lets grab the correct 
                // width/height from either the soft/hard margin rectangles. The 
                // hard margins are usually a little wider than the soft margins.
                // ----------
                // Note: Margins are automatically applied to the rotated page size 
                // when the page is set to landscape, but physical hard margins are 
                // not (the printer is not physically rotating any mechanics inside, 
                // the paper still travels through the printer the same way. So we 
                // rotate in software for landscape)
                int availableWidth = (int)Math.Floor(printPage.OriginAtMargins ? marginBounds.Width : (e.PageSettings.Landscape ? printableArea.Height : printableArea.Width));
                int availableHeight = (int)Math.Floor(printPage.OriginAtMargins ? marginBounds.Height : (e.PageSettings.Landscape ? printableArea.Width : printableArea.Height));

                // Draw our rectangle which will either be the soft margin rectangle 
                // or the hard margin (printer capabilities) rectangle.
                // ----------
                // Note: we adjust the width and height minus one as it is a zero, 
                // zero based co-ordinates system. This will put the rectangle just 
                // inside the available width and height.
                g.DrawRectangle(Pens.Black, 20, 20, availableWidth - 40, availableHeight - 40);


                SizeF f1;
                lineheight2 = 0;
                lineheight1 = 0;
                System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.LightGray);
                System.Drawing.Graphics formGraphics = e.Graphics;
                Size s = new Size(Width - 120, 40);
                if (next == 0)
                {
                    yAxis += 30;
                     img = cPublic.image;
                     loc = new Point(40, yAxis);
                    e.Graphics.DrawImage(img, loc);


                    yAxis += img.Height + 10;

                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                    f1 = e.Graphics.MeasureString(cPublic.g_firmname, printFont);


                    string value = "EMPLOYEE JOINING FORM";

                    yAxis += 20;

                    f1 = e.Graphics.MeasureString(value, printFont);
                    e.Graphics.DrawString(value, new Font("Agency FB", 20, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });


                    printFont = new Font("Century Gothic", 11);
                    yAxis += 80;
                    e.Graphics.DrawString("Arrival Date :" + Convert.ToDateTime(DOA).ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = Width - 250, Y = yAxis });


                    e.Graphics.DrawString("Date  : " + Convert.ToDateTime(DOJ).ToString("dd/MM/yyyy"), printFont, Brushes.Black, new PointF() { X = 60, Y = yAxis });


                    yAxis += 50;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                    formGraphics.FillRectangle(myBrush, new Rectangle(new Point() { X = 60, Y = yAxis }, s));
                    yAxis += 10;
                    f1 = e.Graphics.MeasureString("Employee Details", printFont);
                    e.Graphics.DrawString("Employee Details", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });

                    yAxis += 30;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });


                    lineheight2 = Height - 160;
                    if (form == "A") { lineheight2 -= 100; }
                    lineheight2 += 30;

                    lineheight1 = yAxis - 40;

                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis + 210 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 190, Y = yAxis }, new Point() { X = 190, Y = yAxis + 210 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 440, Y = yAxis }, new Point() { X = 440, Y = yAxis + 210 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 600, Y = yAxis }, new Point() { X = 600, Y = yAxis + 210 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis + 210 });

                    yAxis += 10;
                    e.Graphics.DrawString("Employee ID", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(StaffID, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                    e.Graphics.DrawString("Nationality", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 450, Y = yAxis });
                    e.Graphics.DrawString(Nationality, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 610, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                    yAxis += 10;
                    e.Graphics.DrawString("Name", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(StaffName, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                    e.Graphics.DrawString("Gender", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 450, Y = yAxis });
                    e.Graphics.DrawString(Gender, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 610, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                    yAxis += 10;
                    e.Graphics.DrawString("Marital Status", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(Marital, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                    e.Graphics.DrawString("Date of Birth", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 450, Y = yAxis });
                    e.Graphics.DrawString(Convert.ToDateTime(DOB).ToString("dd/MM/yyyy") , new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 610, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                    yAxis += 10;
                    e.Graphics.DrawString("Qualification", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(Qualifications, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                    e.Graphics.DrawString("Designation", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 450, Y = yAxis });
                    e.Graphics.DrawString(Designation, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 610, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });


                    yAxis += 10;
                    e.Graphics.DrawString("Department", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(Department, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                    e.Graphics.DrawString("Previous Employer", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 450, Y = yAxis });
                    e.Graphics.DrawString(PrevEmp, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 610, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                    yAxis += 10;
                    e.Graphics.DrawString("Phone", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(PhoneNo, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                    e.Graphics.DrawString("Mobile No", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 450, Y = yAxis });
                    e.Graphics.DrawString(MobileNo, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 610, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                    yAxis += 10;
                    e.Graphics.DrawString("Email", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(Email, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                    e.Graphics.DrawString("Driving License", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 450, Y = yAxis });
                    e.Graphics.DrawString(DrvLicense, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 610, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });


                    yAxis += 50;
                    formGraphics.FillRectangle(myBrush, new Rectangle(new Point() { X = 60, Y = yAxis }, s));
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                   
                    yAxis += 10;
                    f1 = e.Graphics.MeasureString("Passport Details", printFont);
                    e.Graphics.DrawString("Passport Details", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });
                    yAxis += 30;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });


                    lineheight2 = Height - 160;
                    if (form == "A") { lineheight2 -= 100; }
                    lineheight2 += 30;

                    lineheight1 = yAxis - 40;

                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis + 60 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 220, Y = yAxis }, new Point() { X = 220, Y = yAxis + 60 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 440, Y = yAxis }, new Point() { X = 440, Y = yAxis + 60 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 600, Y = yAxis }, new Point() { X = 600, Y = yAxis + 60 });
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis + 60 });

                    yAxis += 10;
                    e.Graphics.DrawString("Passport No", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(DocumentNo, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 230, Y = yAxis });
                    e.Graphics.DrawString("Date of Issue", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 450, Y = yAxis });
                    e.Graphics.DrawString(Convert.ToDateTime(IssDate).ToString("dd/MM/yyyy"), new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 610, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                    yAxis += 10;
                    e.Graphics.DrawString("Place of Issue", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(PlaceofIss, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 230, Y = yAxis });
                    e.Graphics.DrawString("Date of Expiry", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 450, Y = yAxis });
                    e.Graphics.DrawString(Convert.ToDateTime(ExpDate).ToString("dd/MM/yyyy"), new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 610, Y = yAxis });
                    yAxis += 20;
                    e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });


                    yAxis += 40;
                    e.Graphics.DrawString("I here by declare that the above details are true and correct to the best of my knowlodege", new Font("Century Gothic", 11, FontStyle.Regular), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    yAxis += 70;
                    e.Graphics.DrawString("Signature", new Font("Century Gothic", 11, FontStyle.Regular), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    yAxis += 50;
                    e.Graphics.DrawString("Name", new Font("Century Gothic", 11, FontStyle.Regular), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                    e.Graphics.DrawString(StaffName, new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 230, Y = yAxis });

                    yAxis += 100;
                    e.Graphics.DrawString("HR", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });

                    e.Graphics.DrawString("Director", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                    e.Graphics.DrawString("Store", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 350, Y = yAxis });
                    e.Graphics.DrawString("Accounts", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = Width - 150, Y = yAxis });
                    next = yAxis + 300;
                }

                
                if (yAxis + 300 > Height)
                {

                    e.HasMorePages = true;
                    return;
                }


                yAxis += 10;

                e.Graphics.DrawImage(img, loc);


                yAxis += img.Height + 10;

                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 80;

                
                formGraphics.FillRectangle(myBrush, new Rectangle(new Point() { X = 60, Y = yAxis }, s));

     


                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                f1 = e.Graphics.MeasureString("Personal Details", printFont);
                e.Graphics.DrawString("Personal Details", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });

                yAxis += 30;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                lineheight2 = Height - 160;
                if (form == "A") { lineheight2 -= 100; }
                lineheight2 += 30;

                lineheight1 = yAxis - 40;

                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = lineheight1 }, new Point() { X = 60, Y = yAxis + 680 });
                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width/2, Y = yAxis }, new Point() { X = Width / 2, Y = yAxis + 60 });

                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width - 60, Y = lineheight1 }, new Point() { X = Width - 60, Y = yAxis + 680 });

                yAxis += 10;
                e.Graphics.DrawString("Father Name", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(FatherName, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                e.Graphics.DrawString("Mother Name", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width / 2) + 10, Y = yAxis });
                e.Graphics.DrawString(MotherName, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = (Width / 2) + 130, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Religion", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(Religion, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 200, Y = yAxis });
                e.Graphics.DrawString("Spouse Name", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width / 2) + 10, Y = yAxis });
                e.Graphics.DrawString(Spouse, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = (Width / 2) + 130, Y = yAxis });
                yAxis += 20;
                formGraphics.FillRectangle(myBrush, new Rectangle(new Point() { X = 60, Y = yAxis }, s));
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

 

                yAxis += 10;
                f1 = e.Graphics.MeasureString("Native Contact Details", printFont);
                e.Graphics.DrawString("Native Contact Details", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });

                yAxis += 30;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width / 2, Y = yAxis }, new Point() { X = Width / 2, Y = yAxis + 60 });


                yAxis += 10;
                e.Graphics.DrawString("Name", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(NName, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 160, Y = yAxis });
                e.Graphics.DrawString("Relation", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width / 2) + 10, Y = yAxis });
                e.Graphics.DrawString(NRelation, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = (Width / 2) + 130, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Phone", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(NPhoneNo, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 160, Y = yAxis });
                e.Graphics.DrawString("Mobile No", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width / 2) + 10, Y = yAxis });
                e.Graphics.DrawString(NMobileNo, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = (Width / 2) + 130, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Email", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(NEmail, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 160, Y = yAxis });

                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Permanent Address", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(NPAddress1 + "\n" + NPAddress2 + "\n" + NPAddress3 + "\n" + NPAddress4, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 230, Y = yAxis });
               
                yAxis += 80;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Temperory Address", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(NTAddress1 + "\n" + NTAddress2 + "\n" + NTAddress3 + "\n" + NTAddress4, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 230, Y = yAxis });

                yAxis += 80;
                formGraphics.FillRectangle(myBrush, new Rectangle(new Point() { X = 60, Y = yAxis }, s));
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                
                yAxis += 10;
                f1 = e.Graphics.MeasureString("Dubai Contact Details", printFont);
                e.Graphics.DrawString("Dubai Contact Details", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width - f1.Width) / 2, Y = yAxis });

                yAxis += 30;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                e.Graphics.DrawLine(Pens.Black, new Point() { X = Width / 2, Y = yAxis }, new Point() { X = Width / 2, Y = yAxis + 60 });


                yAxis += 10;
                e.Graphics.DrawString("Name", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(DName, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 160, Y = yAxis });
                e.Graphics.DrawString("Relation", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width / 2) + 10, Y = yAxis });
                e.Graphics.DrawString(DRelation, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = (Width / 2) + 130, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Phone", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(DPhoneNo, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 160, Y = yAxis });
                e.Graphics.DrawString("Mobile No", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = (Width / 2) + 10, Y = yAxis });
                e.Graphics.DrawString(DMobileNo, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = (Width / 2) + 130, Y = yAxis });
                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });

                yAxis += 10;
                e.Graphics.DrawString("Email", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(DEmail, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 160, Y = yAxis });

                yAxis += 20;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Permanent Address", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(DPAddress1 + "\n" + DPAddress2 +"\n"+DPAddress3+"\n"+DPAddress4, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 230, Y = yAxis });

                yAxis += 80;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
                yAxis += 10;
                e.Graphics.DrawString("Temperory Address", new Font("Century Gothic", 11, FontStyle.Bold), Brushes.Black, new PointF() { X = 70, Y = yAxis });
                e.Graphics.DrawString(DTAddress1 + "\n" + DTAddress2 + "\n" + DTAddress3 + "\n" + DTAddress4, new Font("Century Gothic", 10, FontStyle.Regular), Brushes.Black, new PointF() { X = 230, Y = yAxis });

                yAxis += 80;
                e.Graphics.DrawLine(Pens.Black, new Point() { X = 60, Y = yAxis }, new Point() { X = Width - 60, Y = yAxis });
               
            }
            #endregion
        }

        private bool ReadEmplyeeData(string rptmaster1, string SubTable, string RptOrderno)
        {
            SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);
            SqlDataReader dr = null;
            cmd.Parameters.Clear();
            string sqlQuery;
            sqlQuery = "select a.id,a.[DateOfJoining],a.[DateOfArrival],a.[StaffID],a.[StaffName],b.Code DepCode, b.Details DepDetails, d.Code QlfCode, d.Details QlfDetails," +
                        "c.Code DesCode, c.Details DesDetails,a.[basic],a.[allowance],a.[DrivingLicence],a.[PrevEmployer], a.YearOfExperience,a.PhoneNo,a.MobileNo, a.Email,a.DOB,a.Gender,a.[Marital]," +
                        "a.[Spouse],e.Code NatCode, e.Details NatDetails,a.[Religion],a.[Dependent],a.FatherName,a.[MotherName],a.[NName],a.[NRelation],a.[NPhoneNo],a.[NMobileNo]," +
                        "a.[NEmail],a.NPermanentAddress1,a.NPermanentAddress2,a.NPermanentAddress3,a.NPermanentAddress4,a.NTemporaryAddress1,a.NTemporaryAddress2,a.NTemporaryAddress3,a.NTemporaryAddress4," +
                        "a.[DName],a.[DRelation],a.[DPhoneNo],a.[DMobileNo],a.[DEmail],a.DPermanentAddress1,a.DPermanentAddress2,a.DPermanentAddress3,a.DPermanentAddress4," +
                        "a.DTemporaryAddress1,a.DTemporaryAddress2,a.DTemporaryAddress3,a.DTemporaryAddress4, " +
                        "a.Picture from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code " +
                        "join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code   where a.[id] =@id ";
            cmd.CommandType = CommandType.Text;

            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            string ID = string.IsNullOrEmpty(RptOrderno.Trim()) ? "0" : RptOrderno.Trim();
            cmd.Parameters.AddWithValue("@id", ID);
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                
                 DOJ = dr["DateOfJoining"].ToString();
                 DOA  = dr["DateOfArrival"].ToString();
                StaffID  = dr["StaffID"].ToString();
                StaffName  = dr["StaffName"].ToString();
               
                Department  = dr["DepDetails"].ToString();
                Qualifications  = dr["QlfDetails"].ToString();
                Designation  = dr["DesDetails"].ToString();
                Basic  = dr["basic"].ToString();
                Allowance = dr["basic"].ToString();
                DrvLicense  = dr["DrivingLicence"].ToString();
                 PrevEmp  = dr["PrevEmployer"].ToString();
                 YOP  = dr["YearOfExperience"].ToString();
                 PhoneNo  = dr["PhoneNo"].ToString();
                 MobileNo  = dr["MobileNo"].ToString();
                 Email  = dr["Email"].ToString();

                 DOB  = dr["DOB"].ToString();
                 Gender  = dr["Gender"].ToString();
                 Marital  = dr["Marital"].ToString();
                 Spouse  = dr["Spouse"].ToString();
                
                 Nationality  = dr["NatDetails"].ToString();
                 Religion  = dr["Religion"].ToString();
                 Dependant  = dr["Dependent"].ToString();
                 FatherName  = dr["FatherName"].ToString();
                 MotherName  = dr["MotherName"].ToString();

                 NName  = dr["NName"].ToString();
                 NRelation  = dr["NRelation"].ToString();
                 NPhoneNo  = dr["NPhoneNo"].ToString();
                 NMobileNo  = dr["NMobileNo"].ToString();
                 NEmail  = dr["NEmail"].ToString();
                 NPAddress1  = dr["NPermanentAddress1"].ToString();
                NPAddress2 = dr["NPermanentAddress2"].ToString();
                NPAddress3 = dr["NPermanentAddress3"].ToString();
                NPAddress4 = dr["NPermanentAddress4"].ToString();
                NTAddress1  = dr["NTemporaryAddress1"].ToString();
                NTAddress2 = dr["NTemporaryAddress2"].ToString();
                NTAddress3 = dr["NTemporaryAddress3"].ToString();
                NTAddress4 = dr["NTemporaryAddress4"].ToString();
            
                DName  = dr["DName"].ToString();
                 DRelation  = dr["DRelation"].ToString();
                 DPhoneNo  = dr["DPhoneNo"].ToString();
                 DMobileNo  = dr["DMobileNo"].ToString();
                 DEmail  = dr["DEmail"].ToString();
                 DPAddress1  = dr["DPermanentAddress1"].ToString();
                DPAddress2 = dr["DPermanentAddress2"].ToString();
                DPAddress3 = dr["DPermanentAddress3"].ToString();
                DPAddress4 = dr["DPermanentAddress4"].ToString();
                DTAddress1  = dr["DTemporaryAddress1"].ToString();
                DTAddress2 = dr["DTemporaryAddress2"].ToString();
                DTAddress3 = dr["DTemporaryAddress3"].ToString();
                DTAddress4 = dr["DTemporaryAddress4"].ToString();

            }
            dr.Close();


            cmd = new SqlCommand(string.Empty, cPublic.Connection);
            dr = null;
            cmd.Parameters.Clear();
            sqlQuery = "select *  from [dbo].[Employee" + cPublic.g_firmcode + "] a join [EmployeeDocument" + cPublic.g_firmcode + "] b on a.id=b.staffid  join LOOKUP c on b.[DocumentType]= c.code  " +
                                        "where a.[id] =@id  and c.code='PP'";
            cmd.CommandType = CommandType.Text;
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            ID = string.IsNullOrEmpty(RptOrderno.Trim()) ? "0" : RptOrderno.Trim();
            cmd.Parameters.AddWithValue("@id", ID);
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {

                IssDate = dr["IssDate"].ToString();
                ExpDate = dr["ExpDate"].ToString();
                DocumentNo = dr["DocumentNo"].ToString();
                PlaceofIss = dr["PlaceofIss"].ToString();
            }
            dr.Close();

            return true;
        }

        private bool ReadData(string rptmaster1, string SubTable, string RptOrderno)
        {
            SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);
            SqlDataReader dr = null;
            cmd.Parameters.Clear();
            cmd.CommandText = "select a.id,b.[StaffID],b.[StaffName],b.[DateOfJoining],a.DOA,a.[From]  ,a.[To] ,c.Code LeaveTypeCode, c.Details LeaveTypeDetails,DATEDIFF(DAY,a.[From], a.[To])+1 'LeaveApplied',d.Code DepCode ,d.Details DepDetails,e.Code CovCode ,e.Details CovDetails,a.remarks  from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID]= b.[StaffID] join LOOKUP  c on a.[LeaveType]= c.code join LOOKUP d on b.[Department]= d.code join LOOKUP  e on a.[cov]= e.code   where a.[id] ='" + RptOrderno + " ' ";
            cmd.CommandType = CommandType.Text;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                custcode = dr["StaffID"].ToString().ToUpper();
                custname = dr["StaffName"].ToString().ToUpper();
                leavetype = dr["LeaveTypeDetails"].ToString().ToUpper();
                cov = dr["CovDetails"].ToString().ToUpper();
                date1 = Convert.ToDateTime(dr["DateOfJoining"].ToString());
                department= dr["DepDetails"].ToString().ToUpper();
                fromdate = Convert.ToDateTime(dr["From"].ToString()); 
                todate= Convert.ToDateTime(dr["To"].ToString());
                leaveapp = dr["LeaveApplied"].ToString().ToUpper();
                remarks = dr["remarks"].ToString().ToUpper();

            }

            dr.Close();
            ucLeaveApplication ucleave = new ucLeaveApplication();

            balleave = ucLeaveApplication.balleave;

            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select a.[From]  ,a.[To] ,d.Details LeaveType ,DATEDIFF(DAY,a.[from], a.[to])+1 'Leave Approved',(DATEDIFF(DAY,a.[from], rej.DOR)+1)-(DATEDIFF(DAY,a.[from], a.[to])+1) 'Excess Leave Taken'  from [dbo].[REJOINING" + cPublic.g_firmcode + "] rej join  [dbo].[LeaveApproval" + cPublic.g_firmcode + "] a on rej.[approvalid]=a.id join [dbo].[LeaveApplication" + cPublic.g_firmcode + "] b on a.[applicationid]=b.id   join [Employee" + cPublic.g_firmcode + "] c on b.[StaffID]= c.[StaffID] join LOOKUP  d on b.[LeaveType]= d.code  where b.[StaffID] ='" + custcode + " ' ";
            SqlDataAdapter dad = new SqlDataAdapter(cmd);
            dataTable.Reset();
            
            dad.Fill(dataTable);

            return true; 
        }

        private bool ReadRejoinigData(string rptmaster1, string SubTable, string RptOrderno)
        {
            SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);
            SqlDataReader dr = null;
            cmd.Parameters.Clear();
            cmd.CommandText = "select a.id,b.[StaffID],b.[StaffName],b.[DateOfJoining],a.DOA,a.[From]  ,a.[To] ,c.Code LeaveTypeCode, c.Details LeaveTypeDetails,DATEDIFF(DAY,a.[From], a.[To])+1 'LeaveApplied',d.Code DepCode ,d.Details DepDetails,e.Code CovCode ,e.Details CovDetails  from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID]= b.[StaffID] join LOOKUP  c on a.[LeaveType]= c.code join LOOKUP d on b.[Department]= d.code join LOOKUP  e on a.[cov]= e.code   where a.[id] ='" + RptOrderno + " ' ";
            cmd.CommandType = CommandType.Text;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                custcode = dr["StaffID"].ToString().ToUpper();
                custname = dr["StaffName"].ToString().ToUpper();
                leavetype = dr["LeaveTypeDetails"].ToString().ToUpper();
                cov = dr["CovDetails"].ToString().ToUpper();
                date1 = Convert.ToDateTime(dr["DateOfJoining"].ToString());
                department = dr["DepDetails"].ToString().ToUpper();
                fromdate = Convert.ToDateTime(dr["From"].ToString());
                todate = Convert.ToDateTime(dr["To"].ToString());
                leaveapp = dr["LeaveApplied"].ToString().ToUpper();

            }

            dr.Close();
            ucLeaveApplication ucleave = new ucLeaveApplication();

            balleave = ucLeaveApplication.balleave;

            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select a.[From]  ,a.[To] ,d.Details LeaveType ,DATEDIFF(DAY,a.[from], a.[to])+1 'Leave Approved',(DATEDIFF(DAY,a.[from], rej.DOR)+1)-(DATEDIFF(DAY,a.[from], a.[to])+1) 'Excess Leave Taken'  from [dbo].[REJOINING" + cPublic.g_firmcode + "] rej join  [dbo].[LeaveApproval" + cPublic.g_firmcode + "] a on rej.[approvalid]=a.id join [dbo].[LeaveApplication" + cPublic.g_firmcode + "] b on a.[applicationid]=b.id   join [Employee" + cPublic.g_firmcode + "] c on b.[StaffID]= c.[StaffID] join LOOKUP  d on b.[LeaveType]= d.code  where b.[StaffID] ='" + custcode + " ' ";
            SqlDataAdapter dad = new SqlDataAdapter(cmd);
            dataTable.Reset();

            dad.Fill(dataTable);

            return true;
        }
    }
}
