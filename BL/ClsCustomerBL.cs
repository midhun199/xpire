﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace Xpire.BL
{
  public  class ClsCustomerBL
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public String  EmailId { get; set; }
        public bool  Active { get; set; }

    }
}
