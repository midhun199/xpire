﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Xpire.BL
{
    public class RenewalBL: INotifyPropertyChanged
    {
        int id;
        string name,documenttype,documentno;
        DateTime issueDate;
        DateTime expiryDate;

        public event PropertyChangedEventHandler PropertyChanged;

        public RenewalBL(int id, string name, string documenttype, string documentno, DateTime issueDate, DateTime expiryDate)
        {
            this.id = id;
            this.name = name;
            this.documenttype = documenttype;
            this.documentno = documentno;
            this.issueDate = issueDate;
            this.expiryDate = expiryDate;
          
        }

        public RenewalBL()
        {
       

        }
        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        } 
        public DateTime IssueDate
        {
            get { return issueDate; }
            set { issueDate = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("issueDate"));
                }
            }
        }
        public DateTime ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("ExpiryDate"));
                }
            }
        }

        public string DocumrentType
        {
            get { return documenttype; }
            set { documenttype = value; }
        }

        public string DocumrentNo
        {
            get { return documentno; }
            set { documentno = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("ExpiryDate"));
                }
            }
        }
    }
}
