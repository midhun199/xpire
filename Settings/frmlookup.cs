using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.VisualBasic;
using Xpire.Classes;

namespace Xpire.Settings
{
    public partial class frmlookup : Form
    {
        int fldcount = 0;
        int count = 28;
        string[] arrwdth;
        string[] arrflds;
        string[] arrdisp;
        DataTable dt = new DataTable();
        Boolean dtfor = false;/////for refresh after dateformat set

        public string m_fields = string.Empty;
        public string m_table = string.Empty;
        public string m_condition = string.Empty;
        public string m_order = string.Empty;
        public string m_dispname = string.Empty;
        public string m_fldwidth = string.Empty;
        public Boolean m_search = false;
        public bool m_searchmrp = false;
        public string m_selectfield = string.Empty;
        public string m_selectvalue = string.Empty;
        public string m_strat = string.Empty;
        public string m_caption = "Lookup";
        public string q_type= string.Empty;
        public SqlConnection m_con = cPublic.Connection;
        public Form m_parent;
        public int m_height = 500;
        public ArrayList m_values = new ArrayList();
        fssgen.fssgen gen = new fssgen.fssgen();

        public frmlookup()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int wdth = 0;

                arrflds = m_fields.Split(',');
                fldcount = arrflds.Length;

                if (m_dispname == "")
                    arrdisp = m_fields.ToUpper().Split(',');
                else
                    arrdisp = m_dispname.Split(',');
                int w = 150;
                if (m_fldwidth == "")
                    for (int j = 0; j < fldcount; j++)
                        arrwdth[j] = w.ToString();
                else
                    arrwdth = m_fldwidth.Split(',');
                cfglookup.Cols.Count = fldcount;

                for (int j = 0; j < fldcount; j++)
                {
                    arrdisp[j] = arrdisp[j].Trim();
                    cfglookup.Cols[j].Name = arrdisp[j].Trim();
                    if (Convert.ToInt16(arrwdth[j]) == 0)
                        cfglookup.Cols[arrdisp[j]].Visible = false;
                    else
                        cfglookup.Cols[arrdisp[j]].Width = Convert.ToInt16(arrwdth[j]);
                    cfglookup[0, j] = arrdisp[j];
                    cmbFields.Items.Add(arrdisp[j]);
                    wdth += Convert.ToInt16(arrwdth[j]);
                }
                cfglookup.Width = wdth + 25;
                Width = wdth + 50;
                cmbFields.SelectedIndex = 0;
                txtcond.Text = m_strat;
                txtcond.SelectionStart = txtcond.Text.Length;

                //////////////Set Formats//////////
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    switch (dt.Columns[j].DataType.ToString())
                    {
                        case "System.String":
                            cfglookup.Cols[j].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter;
                            cfglookup.Cols[j].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter;
                            break;
                        case "System.DateTime":
                            cfglookup.Cols[j].DataType = typeof(DateTime);
                            cfglookup.Cols[j].Format = "dd/MM/yyyy";
                            dtfor = true;
                            break;
                        default:
                            cfglookup.Cols[j].TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter;
                            cfglookup.Cols[j].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter;
                            break;
                    }
                }
                if (dtfor)  //Refresh only if datetime field is there
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (i >= dt.Rows.Count)
                            break;
                        for (int j = 0; j < fldcount; j++)
                        {
                            cfglookup[i + 1, arrdisp[j]] = dt.Rows[i].ItemArray[j].ToString();
                        }
                    }
                }
                ///////////////////////////////////
                ///////////location of combo and text///////////////////

                Point pt = cmbFields.Location;
                pt.X = 10;
                cmbFields.Location = pt;
                cmbFields.Width = Width * 40 / 100 - 10;  //Set to 40%

                pt.X = cmbFields.Location.X + cmbFields.Width + 10;
                txtcond.Location = pt;
                txtcond.Width = Width - txtcond.Location.X - 15;
                //////////////////////////////////////////////
                ///////////location of Form///////////////////
                Form mdi = new Form();
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.IsMdiContainer)
                    {
                        mdi = frm;
                        break;
                    }
                }
                if (m_parent == null || m_parent.IsMdiContainer)
                {
                    Top = mdi.Top + 60;
                    Left = mdi.Right - Width - 10;
                }
                else
                {
                    Top = mdi.Top + m_parent.Top + 90;
                    Left = mdi.Left + m_parent.Right - Width;
                }
                //////////////////////////////////////////////
                ///////////Height/////////////////////////////
                if (m_height < 500)
                    Height = m_height;
                //////////////////////////////////////////////
                Text = m_caption;
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frmlookup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Escape))
                Close();
            else if (e.KeyCode.Equals(Keys.F5))
            {
                if (cmbFields.SelectedIndex == fldcount - 1)
                    cmbFields.SelectedIndex = 0;
                else
                    cmbFields.SelectedIndex++;
            }
            else if (e.KeyCode == Keys.F6)
            {
                if (m_searchmrp) { m_searchmrp = false; }
                else { m_searchmrp = true; }
            }
        }

        private void textchange()
        {

            string ashok = string.Empty;
            try
            {
                if (q_type == "SearchApproval")
                {
                    string sql = string.Empty;

                    sql = "select d.id,b.StaffID,b.StaffName from [dbo].[LeaveApplication0001] a join[Employee0001] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code join [dbo].[LeaveApproval0001] d on d.[applicationid] = a.id ";
                    SqlDataAdapter ap = new SqlDataAdapter(sql, m_con);
                    dt.Clear();
                    ap.Fill(dt);
                }

                else if (q_type == "Approval")
                {
                    string sql = string.Empty;

                    sql = "select a.id,b.[StaffID],b.[StaffName] from [dbo].[LeaveApplication0001] a join[Employee0001] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code left outer join[dbo].[LeaveApproval0001] d on d.[applicationid] = a.id where d.id is null ";
                    SqlDataAdapter ap = new SqlDataAdapter(sql, m_con);
                    dt.Clear();
                    ap.Fill(dt);
                }
                else if (q_type == "SearchRejoining")
                {
                    string sql = string.Empty;

                    sql = "select e.id,b.[StaffID],b.[StaffName],e.[DOR] from [dbo].[LeaveApplication0001] a join [Employee0001] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code  join [dbo].[LeaveApproval0001] d on d.[applicationid] = a.id join  [dbo].[REJOINING0001] e on e.approvalid=d.id";
                    SqlDataAdapter ap = new SqlDataAdapter(sql, m_con);
                    dt.Clear();
                    ap.Fill(dt);
                }
                else if (q_type == "Rejoining")
                {
                    string sql = string.Empty;

                    sql = "select d.id,b.[StaffID],b.[StaffName] from [dbo].[LeaveApplication0001] a join [Employee0001] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code  join [dbo].[LeaveApproval0001] d on d.[applicationid] = a.id left outer join  [dbo].[REJOINING0001] e on e.approvalid=d.id where e.id is null";
                    SqlDataAdapter ap = new SqlDataAdapter(sql, m_con);
                    dt.Clear();
                    ap.Fill(dt);
                }
                else if (q_type == "Cancel")
                {
                    string sql = string.Empty;

                    sql = "select StaffName,StaffID,id from [dbo].[Employee0001]  where cancel=1";
                    SqlDataAdapter ap = new SqlDataAdapter(sql, m_con);
                    dt.Clear();
                    ap.Fill(dt);
                }
                else
                {
                    string sql = "Select " + m_fields + " from  " + m_table;
                    if (txtcond.Text.Trim() != "")
                    {
                        sql = sql + " where " + arrflds[cmbFields.SelectedIndex] + " like @cond";
                        if (m_condition.Trim() != "")
                            sql = sql + " and " + m_condition;
                    }
                    else
                        if (m_condition.Trim() != "")
                        sql = sql + " where " + m_condition;
                    if (m_order.Trim() != "")
                        sql = sql + " Order by " + m_order;
                    else
                        sql = sql + " Order by " + arrflds[cmbFields.SelectedIndex];

                    //FOR TEMPORARY SEARCH NOT GENERALIZED--ASHOK
                    if (m_search)
                    {
                        if (txtcond.Text.Trim() != "")
                        {
                            sql = string.Empty;
                            sql = "Select " + m_fields + " from  " + m_table;
                            //ashok=Convert.ToString(Strings.Replace(txtcond.Text.Trim(), " ", "%", 1, -1, CompareMethod.Text));                        
                            //for (int ash = 0; ash < txtcond.Text.Length; ash++)
                            //{
                            //ashok = ashok + "%" + txtcond.Text.Substring(ash, 1) + "%";
                            //}

                            sql = sql + " where " + arrflds[cmbFields.SelectedIndex] + " like  '" + txtcond.Text + "%' ";

                            if (m_condition.Trim() != "")
                            { sql = sql + " and " + m_condition; }

                            if (m_order.Trim() != "")
                            { sql = sql + " Order by " + m_order; }
                            else
                            { sql = sql + " Order by " + arrflds[cmbFields.SelectedIndex]; }
                        }
                    }
                    //-------------------------------------------------

                    if (txtcond.Text.Trim() != string.Empty && m_searchmrp == true && arrflds[cmbFields.SelectedIndex].ToString().ToUpper() == "ITEMNAME")
                    {
                        decimal Sn = 0;
                        string value = string.Empty;

                        for (int i = txtcond.Text.Length - 1; i >= 0; i--)
                        {
                            if (!Information.IsNumeric(txtcond.Text.Substring(i, 1)) && (txtcond.Text.Substring(i, 1) != "."))
                            {
                                if (i != txtcond.Text.Length - 1)
                                    Sn = Convert.ToDecimal(txtcond.Text.Substring(i + 1, txtcond.Text.Length - (i + 1)));
                                value = txtcond.Text.Substring(0, i + 1);
                                break;
                            }
                        }

                        if (value == string.Empty)
                        { value = txtcond.Text.Trim(); }
                        if (m_condition == "") { m_condition = " itemname<>'' "; }

                        if (Sn != 0)
                        {
                            sql = "select " + m_fields + " from " + m_table + " where   itemname like '%" + gen.SQLFormat(value.Trim()) + "%' and " + m_condition + "";
                        } //mrp like  '%" + Sn + "%'  "
                        else
                        { sql = "select " + m_fields + " from " + m_table + "  where   itemname like '%" + gen.SQLFormat(value.Trim()) + "%' and " + m_condition + ""; }

                        if (m_order.Trim() != "")
                        { sql = sql + " Order by " + m_order; }
                        else
                        { sql = sql + " Order by " + arrflds[cmbFields.SelectedIndex]; }

                        SqlDataAdapter ap = new SqlDataAdapter(sql, m_con);
                        // ap.SelectCommand.Parameters.AddWithValue("@cond", txtcond.Text.Trim() + "%");
                        dt.Clear();
                        ap.Fill(dt);
                    }
                    else if (txtcond.Text.Trim() != string.Empty && m_searchmrp == true)
                    {
                        decimal Sn = 0;
                        string value = string.Empty;

                        for (int i = 0; i < txtcond.Text.Length; i++)
                        {
                            value += txtcond.Text.Substring(i, 1) + "%";
                            //if (txtcond.Text.Length - 1 != i) { value =+ txtcond.Text.Substring(i, 1) + "%"; }
                        }

                        if (value == string.Empty)
                        { value = txtcond.Text.Trim(); }
                        if (m_condition == "") { m_condition = " " + arrflds[cmbFields.SelectedIndex].ToString() + "<>'' "; }

                        if (Sn != 0)
                        {
                            sql = "select " + m_fields + " from " + m_table + " where   " + arrflds[cmbFields.SelectedIndex].ToString() + " like '%" + gen.SQLFormat(value.Trim()) + "%' and " + m_condition + "";
                        } //mrp like  '%" + Sn + "%'  "
                        else
                        { sql = "select " + m_fields + " from " + m_table + "  where   " + arrflds[cmbFields.SelectedIndex].ToString() + " like '%" + gen.SQLFormat(value.Trim()) + "%' and " + m_condition + ""; }

                        if (m_order.Trim() != "")
                        { sql = sql + " Order by " + m_order; }
                        else
                        { sql = sql + " Order by " + arrflds[cmbFields.SelectedIndex]; }

                        SqlDataAdapter ap = new SqlDataAdapter(sql, m_con);
                        // ap.SelectCommand.Parameters.AddWithValue("@cond", txtcond.Text.Trim() + "%");
                        dt.Clear();
                        ap.Fill(dt);
                    }
                    else
                    {
                        SqlDataAdapter ap = new SqlDataAdapter(sql, m_con);
                        ap.SelectCommand.Parameters.AddWithValue("@cond", txtcond.Text.Trim() + "%");
                        dt.Clear();
                        ap.Fill(dt);
                    }
                }
                    //////////////////////////////////////////////
                    cfglookup.Rows.Count = dt.Rows.Count + 1;
                    for (int i = 0; i < count; i++)
                    {
                        if (i >= dt.Rows.Count)
                            break;
                        for (int j = 0; j < fldcount; j++)
                        {
                            cfglookup[i + 1, arrdisp[j]] = dt.Rows[i].ItemArray[j].ToString();
                        }

                        if (cfglookup.Cols[m_selectfield] != null)
                        {
                            if (Convert.ToString(cfglookup[i + 1, m_selectfield] + "").Trim().ToUpper() == m_selectvalue.Trim().ToUpper())
                            { cfglookup.Row = i + 1; }
                        }
                    }

                
            }

            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cfglookup.Cols.Move(cfglookup.Cols[cmbFields.Text].Index, 0);
            textchange();
        }

        private void txtcond_TextChanged(object sender, EventArgs e)
        {
            textchange();
        }

        private void txtcond_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Down) && cfglookup.Rows.Count > cfglookup.RowSel + 1)
            {
                cfglookup.Select(cfglookup.RowSel + 1, 0);
                cfglookup.Focus();
            }
            else if (e.KeyCode.Equals(Keys.Up))
            {
                if (!cfglookup.RowSel.Equals(1))
                    cfglookup.Select(cfglookup.RowSel - 1, 0);
                cfglookup.Focus();
            }
            else if (e.KeyCode.Equals(Keys.Enter))
            {
                if (cfglookup.RowSel >= 1)
                    for (int j = 0; j < fldcount; j++)
                        m_values.Add(cfglookup[cfglookup.RowSel, arrdisp[j]].ToString());
                Close();
            }
        }

        private void cfglookup_AfterScroll(object sender, C1.Win.C1FlexGrid.RangeEventArgs e)
        {
            if (e.NewRange.r1 > 0)
            {
                C1.Win.C1FlexGrid.CellRange crange = e.NewRange;
                AfterScroll(crange);
            }
        }

        private void AfterScroll(C1.Win.C1FlexGrid.CellRange crange)
        {
            for (int i = crange.TopRow - 1; i < crange.BottomRow; i++)
            {
                for (int j = 0; j < fldcount; j++)
                { cfglookup[i + 1, arrdisp[j]] = dt.Rows[i][j].ToString(); }                
            }
        }

        private void cfglookup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                if (cfglookup.RowSel >= 1)
                    for (int j = 0; j < fldcount; j++)
                        m_values.Add(cfglookup[cfglookup.RowSel, arrdisp[j]].ToString());
                Close();
            }
        }

        private void cfglookup_DoubleClick(object sender, EventArgs e)
        {
            if (cfglookup.MouseRow > 0)
            {
                for (int j = 0; j < cfglookup.Cols.Count; j++)
                    m_values.Add(cfglookup[cfglookup.RowSel, arrdisp[j]].ToString());
                Close();
            }
        }

        private void cfglookup_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '\b')
                txtcond.Text += ((KeyPressEventArgs)e).KeyChar.ToString();
            else if (txtcond.Text.Length > 0)
                txtcond.Text = txtcond.Text.Substring(0, txtcond.Text.Length - 1);
            else
                txtcond.Text = "";
            txtcond.Focus();
            txtcond.SelectionStart = txtcond.Text.Length;
        }


        private void cfglookup_BeforeSort(object sender, C1.Win.C1FlexGrid.SortColEventArgs e)
        {
            cfglookup.Select(cfglookup.Rows.Count - 1, cfglookup.Col);
        }

        private void cmbFields_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) { SendKeys.Send("{TAB}"); }
        }
    }
}