using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Xpire.Classes;


namespace Xpire.Settings
{
    public partial class frmFirmSelection : Form
    {
        public frmFirmSelection()
        {
            
            InitializeComponent();
        }
        cConnection con = new cConnection();
        cGeneral gen = new cGeneral();
        string currentcompcode = cPublic.g_compcode;
        public string comCode = "";
        Boolean frmclose = true;

        private void frmFirmSelection_Load(object sender, EventArgs e)
        {

            string str = string.Empty; 
           
            if (this.Tag == null)
            {
                this.Tag = "C";
            }
            if (this.Tag+"" == "CD")
            {
              str =  " where code not in ('" + cPublic.g_compcode + "') ";
            }
            string sql = "";
            if (this.Tag.ToString() == "C" || this.Tag.ToString() == "CS"||this.Tag.ToString().Substring(0,1)=="C")
            {
                
                gen.ServerDet();
                con.ConnectMe(cPublic.password,cPublic.Sqluserid,  cPublic.g_PrjCode + "000", cPublic.Sqlservername);

                
                if ((cPublic.g_UserLevel != "9" && cPublic.g_UserLevel != "8"))
                {
                    if (cPublic.g_UserLevel != "1")
                    { sql = "select  a.dispname,a.code from company a, usercompanies b where b.userid='" + cPublic.g_UserId + "' and a.code=b.compcode order by a.dispname"; }
                    else
                    {
                        sql = "select dispname [ NAME OF COMPANY],code from company  " + str + " oorder by dispname";
                    }
                }
                else
                {
                    sql = "select dispname [ NAME OF COMPANY],code from company "+str +" order by dispname";
                }
            }           
            SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
            DataSet ds = new DataSet();
            da.Fill(ds);
            cfgcompany.DataSource = ds.Tables[0];
            InitializeFlex();

            if ((this.Tag.ToString() == "CE" || this.Tag.ToString() == "FE"))
            {
                btnok.Text = "Edit";
            }
            else if (this.Tag.ToString() == "CD" || this.Tag.ToString() == "FD")
            {
                btnok.Text = "Delete";
            }
            else if (this.Tag.ToString() == "FS")
            {
                btnok.Text = "Select";
            }

           
        }

      
        private void InitializeFlex()
        {
            cfgcompany.Cols[0].AllowEditing = false;
            cfgcompany.Cols[0].Width = 390;
            cfgcompany.Cols[1].Visible = false;
            cfgcompany.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            cfgcompany.Styles.Highlight.BackColor = Color.LavenderBlush;
            cfgcompany.Styles.Highlight.ForeColor = Color.Purple ;            
            cfgcompany.Cols[0].Style.Margins.Left  = 10;

        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            cPublic.closing = false ;  
           this.Close();
            //cPublic.closing = true;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            frmclose = false;
            cPublic.closing = false;

            //if (!gen.RegistryKey(cPublic.LoginDate, gen.GetMacAddress(), false))
            //{
            //    MessageBox.Show("Trial Period Expired ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return;
            //}
            if (this.Tag+"" == "BK")
            {
                comCode = cfgcompany[cfgcompany.RowSel, 1].ToString();
                this.Close();
                return;
            }
            if (this.Tag.ToString() == "CE")
            {
                frmcompany editcomp = new frmcompany();
                editcomp.Tag = "EC";
                editcomp.load(cfgcompany[cfgcompany.Row, 1].ToString());
                editcomp.txtcompcode.Enabled = false;
                this.Hide();
                editcomp.ShowDialog();
                this.Close();
                return;
            }
            else if (this.Tag.ToString() == "CD")
            {
                if (cfgcompany.Rows.Count-1 > 0)
                {
                    if (!check((Convert.ToInt64(cfgcompany[cfgcompany.Row, 1].ToString())).ToString("000")))
                    {
                        frmcompany deletecomp = new frmcompany();
                        deletecomp.Tag = "DC";
                        deletecomp.load(cfgcompany[cfgcompany.Row, 1].ToString());
                       
                        this.Hide();
                        deletecomp.ShowDialog();
                        this.Close();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Transactions Found!  Not Deleted !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Atleat one company should be here", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }            
            else if (cfgcompany.Rows.Count > 1)
            {
                gen.LoadCompdetails(cfgcompany[cfgcompany.RowSel, 1].ToString());
                this.Close();
            }
       }             
        
        
        private void cfgcompany_DoubleClick(object sender, EventArgs e)
        {
            btnok_Click(sender, e);
        }

        private void frmFirmSelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Tag.ToString().Substring(0, 1) != "F")
            {
                if (Application.OpenForms.Count > 1)
                {
                    for (int i = 0; i < Application.OpenForms.Count; i++)
                    {
                        if (Application.OpenForms[i].IsMdiContainer)
                        {
                            cPublic.mainmenuload = false; 
                            break;
                        }
                    }
                }
                else
                    cPublic.mainmenuload = false;
            }
        }
       
        private bool  firmcheck(string code)
        {
            string sql = "";
            SqlDataReader dr = null;
                sql = "select * from voucher" + code;
                SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
                dr = comm.ExecuteReader();
                if (dr.Read())
                {
                    dr.Close();
                    return true;
                    
                }
                dr.Close();
                return false;
        }

        private bool check(string code)
        {
            string sql = "";
            con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + code, cPublic.Sqlservername);
            sql = "select code from firms";
            SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
            DataSet ds = new DataSet();
            da.Fill(ds);
            for (int i = 0; i < (ds.Tables[0].Rows.Count); i++)
            {
                sql = "select * from voucher" + ds.Tables[0].Rows[i]["code"].ToString();
                SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
                SqlDataReader dr = comm.ExecuteReader();
                if (dr.Read())
                {
                    dr.Close();
                    return true;
                }
                dr.Close();
            }
            return false;
        }

        private void frmFirmSelection_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!frmclose)
            {
                if (this.Tag.ToString().Substring(0, 1) != "F" && this.Tag.ToString() != "CE" && this.Tag.ToString() != "CD" && this.Tag.ToString() != "BK" && this.Tag.ToString() != "RT")
                {
                    Hide();
                    string sql = "select getdate()";
                    SqlCommand cmd = new SqlCommand(sql, cPublic.Connection);
                    cPublic.g_Rdate = Convert.ToDateTime(cmd.ExecuteScalar());
                }
            }          
        }

        private void frmFirmSelection_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            { cPublic.closing = false; this.Close(); }

            if (e.KeyCode == Keys.Enter)
            {
                frmclose = false;
                cPublic.closing = false;

                //if (!gen.RegistryKey(cPublic.LoginDate, gen.GetMacAddress(), false))
                //{
                //    MessageBox.Show("Trial Period Expired ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //    return;
                //}
                if (this.Tag + "" == "BK")
                {
                    comCode = cfgcompany[cfgcompany.RowSel, 1].ToString();
                    this.Close();
                    return;
                }
                if (this.Tag.ToString() == "CE")
                {
                    frmcompany editcomp = new frmcompany();
                    editcomp.Tag = "EC";
                    editcomp.load(cfgcompany[cfgcompany.Row, 1].ToString());
                    editcomp.txtcompcode.Enabled = false;
                    this.Hide();
                    editcomp.ShowDialog();
                    this.Close();
                    return;
                }
                else if (this.Tag.ToString() == "CD")
                {
                    if (cfgcompany.Rows.Count - 1 > 0)
                    {
                        if (!check((Convert.ToInt64(cfgcompany[cfgcompany.Row, 1].ToString())).ToString("000")))
                        {
                            frmcompany deletecomp = new frmcompany();
                            deletecomp.Tag = "DC";
                            deletecomp.load(cfgcompany[cfgcompany.Row, 1].ToString());

                            this.Hide();
                            deletecomp.ShowDialog();
                            this.Close();
                            return;
                        }
                        else
                        {
                            MessageBox.Show("Transactions Found!  Not Deleted !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Atleat one company should be here", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else if (cfgcompany.Rows.Count > 1)
                {
                    gen.LoadCompdetails(cfgcompany[cfgcompany.RowSel, 1].ToString());
                    this.Close();
                }
            }
        }
    }
}



