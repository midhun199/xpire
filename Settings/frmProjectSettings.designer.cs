namespace Xpire.Settings
{
    partial class frmProjectSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnsave = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.lblserveruser = new System.Windows.Forms.Label();
            this.txtserverusername = new System.Windows.Forms.TextBox();
            this.lblserverpswd = new System.Windows.Forms.Label();
            this.lblprojectcode = new System.Windows.Forms.Label();
            this.txtserverpswd = new System.Windows.Forms.TextBox();
            this.txtprojectcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnsave
            // 
            this.btnsave.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Location = new System.Drawing.Point(93, 135);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 23);
            this.btnsave.TabIndex = 5;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btncancel
            // 
            this.btncancel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancel.Location = new System.Drawing.Point(176, 135);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(75, 23);
            this.btncancel.TabIndex = 6;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // lblserveruser
            // 
            this.lblserveruser.AutoSize = true;
            this.lblserveruser.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblserveruser.ForeColor = System.Drawing.Color.Black;
            this.lblserveruser.Location = new System.Drawing.Point(16, 23);
            this.lblserveruser.Name = "lblserveruser";
            this.lblserveruser.Size = new System.Drawing.Size(103, 14);
            this.lblserveruser.TabIndex = 2;
            this.lblserveruser.Text = "Server UserID";
            // 
            // txtserverusername
            // 
            this.txtserverusername.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtserverusername.Location = new System.Drawing.Point(141, 20);
            this.txtserverusername.Name = "txtserverusername";
            this.txtserverusername.Size = new System.Drawing.Size(166, 21);
            this.txtserverusername.TabIndex = 1;
            this.txtserverusername.Text = "sa";
            // 
            // lblserverpswd
            // 
            this.lblserverpswd.AutoSize = true;
            this.lblserverpswd.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblserverpswd.ForeColor = System.Drawing.Color.Black;
            this.lblserverpswd.Location = new System.Drawing.Point(16, 50);
            this.lblserverpswd.Name = "lblserverpswd";
            this.lblserverpswd.Size = new System.Drawing.Size(125, 14);
            this.lblserverpswd.TabIndex = 2;
            this.lblserverpswd.Text = "Server Password ";
            // 
            // lblprojectcode
            // 
            this.lblprojectcode.AutoSize = true;
            this.lblprojectcode.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblprojectcode.ForeColor = System.Drawing.Color.Black;
            this.lblprojectcode.Location = new System.Drawing.Point(16, 78);
            this.lblprojectcode.Name = "lblprojectcode";
            this.lblprojectcode.Size = new System.Drawing.Size(90, 14);
            this.lblprojectcode.TabIndex = 2;
            this.lblprojectcode.Text = "Project code";
            // 
            // txtserverpswd
            // 
            this.txtserverpswd.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtserverpswd.Location = new System.Drawing.Point(141, 47);
            this.txtserverpswd.Name = "txtserverpswd";
            this.txtserverpswd.PasswordChar = '#';
            this.txtserverpswd.Size = new System.Drawing.Size(166, 21);
            this.txtserverpswd.TabIndex = 2;
            this.txtserverpswd.Text = "server";
            // 
            // txtprojectcode
            // 
            this.txtprojectcode.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprojectcode.Location = new System.Drawing.Point(141, 75);
            this.txtprojectcode.Name = "txtprojectcode";
            this.txtprojectcode.Size = new System.Drawing.Size(166, 21);
            this.txtprojectcode.TabIndex = 3;
            this.txtprojectcode.Text = "GS007";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "Password";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(141, 104);
            this.textBox1.MaxLength = 6;
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '#';
            this.textBox1.Size = new System.Drawing.Size(166, 21);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "246810";
            // 
            // frmProjectSettings
            // 
            this.ForeColor = System.Drawing.Color.Black;

            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 171);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtprojectcode);
            this.Controls.Add(this.txtserverpswd);
            this.Controls.Add(this.txtserverusername);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblprojectcode);
            this.Controls.Add(this.lblserverpswd);
            this.Controls.Add(this.lblserveruser);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnsave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmProjectSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Project Settings";
            this.TransparencyKey = System.Drawing.Color.LightGray;
            this.Load += new System.EventHandler(this.frmProjectSettings_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmproject_settings_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Label lblserveruser;
        private System.Windows.Forms.TextBox txtserverusername;
        private System.Windows.Forms.Label lblserverpswd;
        private System.Windows.Forms.Label lblprojectcode;
        private System.Windows.Forms.TextBox txtserverpswd;
        private System.Windows.Forms.TextBox txtprojectcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
    }
}