using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Xpire.Classes;

namespace Xpire.Settings
{    
    public partial class frmSplash : Form
    {
        int i;
        int j;
        public frmSplash()
        {
            InitializeComponent();
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {
            lblVer.Text = "Version: 1.0.0.1";
            i = 1;
            timer1.Start();
            this.Opacity = 1.0;
            j = 1;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Interval = 700;
            switch (i)
            {
                case 1: 
                    lblMsg.Text = "Initializing : Checking Lock Mode.....";
                    
                    this.Refresh();
                    break;
                case 2:
                    lblMsg.Text = "Initializing : Lock Releasing.....";
                    break;
                case 3:
                    lblMsg.Text = " Validating Administrator.....";
                    break;
                case 4:
                    lblMsg.Text = "Initializing : Refreshing Registry.....";
                    break;
                case 5:
                    lblMsg.Text = " Permission allowed for user.....";
                    break;
                case 6:
                    lblMsg.Text = " Starting the Application.....";
                    break;
                case 8:
                    timer1.Stop();
                    timer2.Start();
                    break;

            }
            i = i + 1;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Interval = 60;
            switch (j)
            {
                case 1:
                    this.Opacity = 0.90;
                    break;
                case 2:
                    this.Opacity = 0.80;
                    break;
                case 3:
                    this.Opacity = 0.70;
                    break;
                case 4:
                    this.Opacity = 0.60;
                    break;
                case 5:
                    this.Opacity = 0.50;
                    break;
                case 6:
                    this.Opacity = 0.40;
                    break;
                case 7:
                    this.Opacity = 0.30;
                    break;
                case 8:
                    this.Opacity = 0.20;
                    break;
                case 9:
                    this.Opacity = 0.10;
                    break;
                case 10:
                    timer2.Stop();
                    this.Close();
                    break;
            }
            j++;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            MessageBox.Show(" All Rights Reserved, Biz-Maxx �  2008 \n " + cPublic.CompanyName + " Thiruvananthapuram,Kerala", "A3", MessageBoxButtons.OK, MessageBoxIcon.Information);
            timer1.Start();
        }
    }
}