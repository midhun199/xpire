using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using C1.Win.C1FlexGrid;
using System.Data.SqlClient;
using Xpire.Classes;

namespace Xpire.Settings
{
    public partial class frmSetRights : Form
    {
        public frmSetRights()
        {
            InitializeComponent();
        }

        SqlCommand cmd = null;
        SqlDataAdapter ap = null;
        DataTable dt = null; 

        private void frmSetRights_Load(object sender, EventArgs e)
        {
            Load_Screen();
        }

        private void Load_Screen()
        {
            int vCol = 0;
            flex.Tree.Column = 0;
            flex.Rows.Count = 1;
            flex.Cols.Fixed = 0;
            flex.Cols.Count = 20;

            flex.Cols[vCol++].Name = "MenuName";
            flex.Cols["MenuName"].Width = 300;
            flex.Cols["MenuName"].AllowEditing = false;
            flex.Cols["MenuName"].Caption = "Menu Name";
            flex.Cols["MenuName"].TextAlign = TextAlignEnum.LeftCenter;

            flex.Cols[vCol++].Name = "LEVEL1";
            flex.Cols["LEVEL1"].Caption = "1";
            flex.Cols["LEVEL1"].Width = 60;
            flex.Cols["LEVEL1"].AllowEditing = true;
            flex.Cols["LEVEL1"].DataType = typeof(Boolean);
            flex.Cols["LEVEL1"].TextAlign = TextAlignEnum.GeneralCenter;
            flex.Cols["LEVEL1"].TextAlignFixed = TextAlignEnum.LeftCenter;

            flex.Cols[vCol++].Name = "LEVEL2";
            flex.Cols["LEVEL2"].Caption = "2";
            flex.Cols["LEVEL2"].DataType = typeof(Boolean);
            flex.Cols["LEVEL2"].AllowEditing = true;
            flex.Cols["LEVEL2"].Width = 60;
            flex.Cols["LEVEL2"].TextAlign = TextAlignEnum.GeneralCenter;
            flex.Cols["LEVEL2"].TextAlignFixed = TextAlignEnum.LeftCenter;

            flex.Cols[vCol++].Name = "LEVEL3";
            flex.Cols["LEVEL3"].Caption = "3";
            flex.Cols["LEVEL3"].DataType = typeof(Boolean);
            flex.Cols["LEVEL3"].AllowEditing = true;
            flex.Cols["LEVEL3"].Width = 60;
            flex.Cols["LEVEL3"].TextAlign = TextAlignEnum.GeneralCenter;
            flex.Cols["LEVEL3"].TextAlignFixed = TextAlignEnum.LeftCenter;


            flex.Cols[vCol++].Name = "LEVEL4";
            flex.Cols["LEVEL4"].Caption = "4";
            flex.Cols["LEVEL4"].DataType = typeof(Boolean);
            flex.Cols["LEVEL4"].AllowEditing = true;
            flex.Cols["LEVEL4"].Width = 60;
            flex.Cols["LEVEL4"].TextAlign = TextAlignEnum.GeneralCenter;
            flex.Cols["LEVEL4"].TextAlignFixed = TextAlignEnum.LeftCenter;


            flex.Cols[vCol++].Name = "LEVEL5";
            flex.Cols["LEVEL5"].Caption = "5";
            flex.Cols["LEVEL5"].DataType = typeof(Boolean);
            flex.Cols["LEVEL5"].AllowEditing = true;
            flex.Cols["LEVEL5"].Width = 60;
            flex.Cols["LEVEL5"].TextAlign = TextAlignEnum.GeneralCenter;
            flex.Cols["LEVEL5"].TextAlignFixed = TextAlignEnum.LeftCenter;


            flex.Cols[vCol++].Name = "LEVEL6";
            flex.Cols["LEVEL6"].Caption = "6";
            flex.Cols["LEVEL6"].DataType = typeof(Boolean);
            flex.Cols["LEVEL6"].AllowEditing = true;
            flex.Cols["LEVEL6"].Width = 60;
            flex.Cols["LEVEL6"].TextAlign = TextAlignEnum.GeneralCenter;
            flex.Cols["LEVEL6"].TextAlignFixed = TextAlignEnum.LeftCenter;


            flex.Cols[vCol++].Name = "LEVEL7";
            flex.Cols["LEVEL7"].Caption = "7";
            flex.Cols["LEVEL7"].DataType = typeof(Boolean);
            flex.Cols["LEVEL7"].AllowEditing = true;
            flex.Cols["LEVEL7"].Width = 60;
            flex.Cols["LEVEL7"].TextAlign = TextAlignEnum.GeneralCenter;
            flex.Cols["LEVEL7"].TextAlignFixed = TextAlignEnum.LeftCenter;


            flex.Cols[vCol++].Name = "LEVEL8";
            flex.Cols["LEVEL8"].Caption = "8";
            flex.Cols["LEVEL8"].DataType = typeof(Boolean);
            flex.Cols["LEVEL8"].AllowEditing = true;
            flex.Cols["LEVEL8"].Width = 60;
            flex.Cols["LEVEL8"].TextAlign = TextAlignEnum.GeneralCenter;
            flex.Cols["LEVEL8"].TextAlignFixed = TextAlignEnum.LeftCenter;

            flex.Cols[vCol++].Name = "REF";
            flex.Cols["REF"].DataType = typeof(String);
            flex.Cols["REF"].Visible = false;

            flex.Cols.Count = vCol;

            flex.Tree.Style = TreeStyleFlags.Simple;
            flex.AllowMerging = AllowMergingEnum.FixedOnly;

            string sqlQuery = "select * from " + cPublic.g_PrjCode + "000" + ".DBO.SETRIGHTS Order By Slno";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            ap = new SqlDataAdapter(cmd);
            dt = new DataTable();
            ap.Fill(dt);
            flex.Rows.Count = dt.Rows.Count + 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {                
                flex[i + 1, "REF"] = dt.Rows[i]["slno"];
                flex[i + 1, 0] = dt.Rows[i]["MenuDesc"].ToString();
                flex.Rows[i + 1].IsNode = true;
                flex.Rows[i + 1].Node.Level = Convert.ToInt32(dt.Rows[i]["Menulevel"].ToString());
               
                for (int j = 1; j < 9; j++)
                {
                    C1.Win.C1FlexGrid.CellRange tbs = flex.GetCellRange(i + 1, flex.Cols["LEVEL" + j].Index);
                    tbs.Checkbox = CheckEnum.Unchecked;
                    if (Convert.ToBoolean(dt.Rows[i]["LEVEL" + j]) == true)
                    {
                        tbs.Checkbox = CheckEnum.Checked;
                    }                   
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save_Screen(); 
        }

        private void frmSetRights_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }           
        }

        private void Save_Screen()
        {
            if (MessageBox.Show("Are you sure to Save Details !", this.Text, MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }

            for (int j = 1; j < flex.Rows.Count; j++)
            {
                string sqlQuery = "Update " + cPublic.g_PrjCode + "000" + ".DBO.SETRIGHTS Set LEVEL1=@LEVEL1, "
                + " LEVEL2=@LEVEL2,LEVEL3=@LEVEL3,LEVEL4=@LEVEL4,LEVEL5=@LEVEL5,LEVEL6=@LEVEL6,LEVEL7=@LEVEL7,LEVEL8=@LEVEL8 " + Environment.NewLine
                + "  "
                + "  "
                + " WHERE SLNO=@SLNO ";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);

                for (int vJ = 1; vJ < 9; vJ++)
                {
                    C1.Win.C1FlexGrid.CellRange tbs = flex.GetCellRange(j, flex.Cols["LEVEL" + vJ].Index);
                    Boolean vBoo = false;
                    if (tbs.Checkbox == CheckEnum.Checked)
                    { vBoo = true; }
                    cmd.Parameters.AddWithValue("@LEVEL" + vJ, vBoo);
                }
                cmd.Parameters.AddWithValue("@SLNO", flex.GetData(j, "REF"));
                cmd.ExecuteNonQuery();
            }

            MessageBox.Show("SetRights Setting Successfully !");
            this.Close();
            return;
        }

        Boolean v_check = false; 

        private void flex_AfterEdit(object sender, RowColEventArgs e)
        {
            if (e.Col != 0)
            {
                v_check = true; 
                C1.Win.C1FlexGrid.Node   tbs = flex.Rows[e.Row].Node;
                C1.Win.C1FlexGrid.CellRange vCellRange = flex.GetCellRange(e.Row , e.Col );
                CheckTreenode_Top(tbs, e.Col, vCellRange);
                v_check = false; 
            }

        }
     
        private void CheckTreenode_Top(C1.Win.C1FlexGrid.Node tbs,int vCol, C1.Win.C1FlexGrid.CellRange vCellRange)
        {
            for (int j = tbs.Row.Index + 1; j < flex.Rows.Count; j++)
            {
                if (flex.Rows[j].Node.Level > tbs.Level)
                {
                    C1.Win.C1FlexGrid.CellRange vCurrentRange = flex.GetCellRange(j, vCol);
                    vCurrentRange.Checkbox = vCellRange.Checkbox;
                }
                else
                {
                    break;
                }
            }

            if (vCellRange.Checkbox == CheckEnum.Checked)
            { checkchange(tbs, vCol, vCellRange); }

            if (vCellRange.Checkbox == CheckEnum.Unchecked)
            {
                switch (vCol)
                {
                    case 1:
                        chk1.Checked = false;
                        break;
                    case 2:
                        chk2.Checked = false; 
                        break;  
                    case 3:
                        chk3.Checked = false;
                        break;
                    case 4:
                        chk4.Checked = false;
                        break;
                    case 5:
                        chk5.Checked = false;
                        break;
                    case 6:
                        chk6.Checked = false;
                        break;
                    case 7:
                        chk7.Checked = false;
                        break;
                    case 8:
                        chk8.Checked = false;
                        break;                   
                }

                return; 
            }

            int vLevel = tbs.Level; 
            for (int j = tbs.Row.Index-1; j > 0; j--)
            {
                if (flex.Rows[j].Node.Level < vLevel)
                {
                    C1.Win.C1FlexGrid.CellRange vCurrentRange = flex.GetCellRange(j, vCol);

                    vCurrentRange.Checkbox = vCellRange.Checkbox;
                    if (flex.Rows[j].Node.Level == 1)
                    {
                        break;
                    }
                    vLevel = vLevel - 1;
                }              
            }
        }

        private void chk2_CheckedChanged(object sender, EventArgs e)
        {
            if (v_check == true)
            {
                return; 
            }
            CheckBox tbs = (CheckBox)sender;
            CheckChange(tbs, tbs.Checked);
        }

        private void CheckChange(CheckBox sender, Boolean Check)
        {
            int vCol = 0;
            if (sender == chk1)
            {
                vCol = 1;
            }
            else if (sender == chk2)
            {
                vCol = 2;
            }
            else if (sender == chk3)
            {
                vCol = 3;
            }
            else if (sender == chk4)
            {
                vCol = 4;
            }
            else if (sender == chk5)
            {
                vCol = 5;
            }
            else if (sender == chk6)
            {
                vCol = 6;
            }
            else if (sender == chk7)
            {
                vCol = 7;
            }
            else if (sender == chk8)
            {
                vCol = 8;
            }

            if (vCol > 0)
            {
                C1.Win.C1FlexGrid.CellRange vCurrentRange = flex.GetCellRange(1, vCol, flex.Rows.Count - 1, vCol);
                if (Check == true)
                {
                    vCurrentRange.Checkbox = CheckEnum.Checked;
                }
                else
                {
                    vCurrentRange.Checkbox = CheckEnum.Unchecked;
                }

            }
        }

        private void checkchange(Node tbs, int vCol, CellRange vCellRange)
        {
            if (tbs.GetNode(NodeTypeEnum.Parent) != null)
            {
                C1.Win.C1FlexGrid.Node tb = tbs.GetNode(NodeTypeEnum.Parent);
                
                C1.Win.C1FlexGrid.CellRange vCurrentRange = flex.GetCellRange(tb.Row.Index  , vCol);
                vCurrentRange.Checkbox = vCellRange.Checkbox; 

                checkchange(tb,vCol,vCellRange  );
            }
        }


    }
}