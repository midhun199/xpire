namespace Xpire.Settings
{
    partial class frmParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmParameters));
            this.flxSettings = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.flxSettings)).BeginInit();
            this.SuspendLayout();
            // 
            // flxSettings
            // 
            this.flxSettings.BackColor = System.Drawing.SystemColors.Window;
            this.flxSettings.ColumnInfo = "3,1,0,0,0,90,Columns:0{Width:32;}\t1{Width:228;Name:\"Parameters\";Caption:\"Paramete" +
                "rs\";}\t2{Width:96;Name:\"values\";Caption:\"Values\";}\t";
            this.flxSettings.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flxSettings.KeyActionEnter = C1.Win.C1FlexGrid.KeyActionEnum.None;
            this.flxSettings.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross;
            this.flxSettings.Location = new System.Drawing.Point(8, 8);
            this.flxSettings.Name = "flxSettings";
            this.flxSettings.Rows.MinSize = 23;
            this.flxSettings.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.flxSettings.Size = new System.Drawing.Size(378, 301);
            this.flxSettings.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("flxSettings.Styles"));
            this.flxSettings.TabIndex = 1;
            this.flxSettings.Enter += new System.EventHandler(this.flxSettings_Enter);
            this.flxSettings.Click += new System.EventHandler(this.flxSettings_Click);
            this.flxSettings.EnterCell += new System.EventHandler(this.flxSettings_EnterCell);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(108, 324);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(72, 27);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReturn.Location = new System.Drawing.Point(186, 324);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(72, 27);
            this.btnReturn.TabIndex = 2;
            this.btnReturn.Text = "&Return";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // frmParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 363);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.flxSettings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmParameters";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Parameters";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmParameters_KeyDown);
            this.Load += new System.EventHandler(this.frmParameters_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flxSettings)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private C1.Win.C1FlexGrid.C1FlexGrid flxSettings;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReturn;
    }
}