using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Xpire.Classes;

namespace Xpire.Settings
{
    public partial class frmUser : Form
    {
        private string sqlQuery = string.Empty;
        private SqlCommand cmd;
        private SqlDataReader dr;
        cConnection con = new cConnection();
        cGeneral cGen = new cGeneral();
        fssgen.fssgen Gen = new fssgen.fssgen();
        public frmUser()
        {
            InitializeComponent();
        }

        private Boolean Ckeckcompselection()
        {
            if (this.Tag.ToString() == "Add" || this.Tag.ToString() == "Edit")
            {
                for (int i = 1; i < cfgcompany.Rows.Count; i++)
                {
                    if ((Boolean)cfgcompany[i, 2])
                        return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Tag.ToString().Equals("Delete") == true || this.Tag.ToString().Equals("Change") == true)
            {
                if (txtUserID.Text.Trim().ToUpper() == cPublic.g_UserId.Trim().ToUpper() && this.Tag.Equals("Delete") == true)
                {
                    showMessage("Currently used", "red");
                    txtUserID.Select();
                    txtUserID.SelectAll();
                    return;
                }
                else
                {
                    showMessage("", "");
                }

                Save_Screen();
            }
            else
            {
                if (groupBox1.Visible == true)
                {
                    v_Check = false;
                    if (validateUserID())
                    {
                        if (txtUserName.Text.Equals(string.Empty))
                        {
                            showMessage("Enter User Name.", "red");
                            txtUserName.Enabled = true;
                            txtUserName.Focus();
                        }
                        else if (checkPassword())
                        {
                            Next();
                        }
                    }
                }
                else
                {
                    Next();
                }
            }
        }

        private void Save_Screen()
        {
            v_Check = false;
            if (validateUserID())
            {
                if (txtUserName.Text.Equals(string.Empty))
                {
                    showMessage("Enter User Name.", "red");
                    txtUserName.Enabled = true;
                    txtUserName.Focus();
                }
                else if (checkPassword())
                {
                    if (Convert.ToInt32(cmbUserLevel.Text) < 9)
                        if (Ckeckcompselection())
                        {
                            // tabControl1.SelectedIndex = 1;
                            showMessage("Select atleast 1 company..", "red");
                            return;
                        }
                    if (this.Tag.ToString() == "Delete")
                    {
                        if (txtUserID.Text.Trim().ToUpper() == "ADMIN")
                        {
                            MessageBox.Show("You don't have System privellages to Delete this user", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    if (MessageBox.Show("Are you sure to " + this.Tag.ToString() + " User Details", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }

                    if (saveUserDetails())
                    {
                        clearFields();
                        if (this.Tag.ToString().Equals("Add"))
                        {
                            showMessage("User Created Successfully..", "blue");
                            this.groupBox2.Visible = true;
                            groupBox1.Visible = false;
                            Next();
                        }
                        else
                        {
                            showMessage("User Updated Successfully..", "blue");
                            this.Close();
                        }
                        // tabControl1.SelectedIndex = 0;
                        this.txtUserID.Focus();
                    }
                    else
                    {
                        if (this.Tag.ToString().Equals("Add"))
                            showMessage("User Creation Failed..", "red");

                        else
                            showMessage("User Updation Failed..", "red");
                    }
                }
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            { SendKeys.Send("{TAB}"); }
            if (e.KeyCode == Keys.Escape)
            { this.Close(); }
        }

        private void txtConPassword_Validating(object sender, CancelEventArgs e)
        {
            checkPassword();
        }

        private void txtUserID_Validating(object sender, CancelEventArgs e)
        {
            if (txtUserID.Enabled == false)
            {
                return;
            }
            if (txtUserID.Text.Trim().ToUpper() == cPublic.g_UserId.Trim().ToUpper() && this.Tag.Equals("Delete") == true)
            {
                showMessage("Currently used", "red");
                txtUserID.Select();
                txtUserID.SelectAll();
                return;
            }
            else
            {
                showMessage("", "");
            }
            v_Check = true;
            if (validateUserID())
            {
                sqlQuery = "select compcode from " + cPublic.g_PrjCode + "000" + ".DBO.USERCOMPANIES where userid = @userid";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@userid", txtUserID.Text.Trim());
                dr = cmd.ExecuteReader();
                int i;
                while (dr.Read())
                {
                    i = cfgcompany.FindRow(dr["compcode"], 0, 0, true);
                    if (i > 0)
                    {
                        cfgcompany[i, 2] = true;
                    }
                }
                dr.Close();
            }
        }

        bool v_Check = true;

        private Boolean checkUserID(string uID)
        {
            Boolean status = false;
            sqlQuery = "select * from " + cPublic.g_PrjCode + "000" + ".DBO.LOGIN where userid = @userid";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@userid", uID.Trim());
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                if (v_Check == false)
                {
                    status = true;
                    if (this.Tag.Equals("Delete") == true && dr["userid"].ToString() == cPublic.g_UserId)
                    {
                        status = false;
                    }
                }
                else
                {
                    if (this.Tag.Equals("Add") == false)
                    {
                        txtUserName.Text = dr["UserName"].ToString();
                        cmbUserLevel.Text = dr["UserLevel"].ToString();
                        Level_Change();
                    }
                    status = true;
                    if (this.Tag.Equals("Delete") == true && dr["userid"].ToString().ToUpper() == cPublic.g_UserId.ToUpper())
                    {
                        txtUserName.Text = "";
                        cmbUserLevel.Text = "0";
                        status = false;
                    }
                }

            }
            dr.Close();
            return status;
        }

        private Boolean validateUserID()
        {
            if (txtUserID.Text.Trim() == "")
            {
                showMessage("Please Enter UserID", "red");
                txtUserID.Focus();
                return true;
            }
            else if (checkUserID(txtUserID.Text.Trim()))
            {
                if (this.Tag.Equals("Add"))
                {
                    showMessage("Choose another UserID..", "red");
                    txtUserID.Focus();
                    return false;
                }
                else
                {
                    txtUserID.Enabled = false;
                    txtUserName.Focus();
                    showMessage("", "");
                }
            }
            else
            {
                if (this.Tag.Equals("Add"))
                {
                    showMessage("", "");
                }
                else
                {
                    showMessage("User Id not found", "red");
                    this.txtUserID.Focus();
                }
            }
            return true;
        }

        private void showMessage(string message, string color)
        {
            msgLabel.Text = message;
            switch (color)
            {
                case "red":
                    msgLabel.ForeColor = Color.Red;
                    break;
                case "blue":
                    msgLabel.ForeColor = Color.Blue;
                    break;
            }
        }

        private void txtUserName_Validating(object sender, CancelEventArgs e)
        {
            if (txtUserName.Text.Equals(""))
            {
                showMessage("Please Enter User Name", "red");
                txtUserName.Focus();
            }
            else
                showMessage("", "");
        }

        private Boolean checkPassword()
        {
            if (this.Tag + "".ToString() == "Change")
            {
                if (txtOldPassword.Text.Trim() == string.Empty)
                { showMessage("Enter Your Old Password..", "red"); txtOldPassword.Focus(); return false; }
            }
            if (this.Tag + "".ToString() == "Add" || this.Tag + "".ToString() == "Change")
            {
                if (txtPassword.Text.Trim() == string.Empty)
                { showMessage("Password cannot be Blank..", "red"); txtPassword.Focus(); return false; }
                if (!txtPassword.Text.Trim().Equals(txtConPassword.Text.Trim()))
                {
                    showMessage("Password doesnot Match..", "red");
                    txtPassword.Text = ""; txtConPassword.Text = "";
                    txtPassword.Focus();
                    return false;
                }
            }
            else
                showMessage("", "");
            return true;
        }

        private Boolean saveUserDetails()
        {
            try
            {
               

                addParameters();
                cmd.ExecuteNonQuery();
                for (int i = 1; i < cfgcompany.Rows.Count; i++)
                {
                    if ((Boolean)cfgcompany[i, 2])
                    {
                        cmd = new SqlCommand("" + cPublic.g_PrjCode + "000" + ".DBO.SP_INSERT_USERCOMP", cPublic.Connection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@userid", txtUserID.Text.Trim());
                        cmd.Parameters.AddWithValue("@code", cfgcompany[i, 0].ToString().Trim());
                        cmd.ExecuteNonQuery();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void addParameters()
        {
            switch (this.Tag.ToString())
            {
                case "Add":
                case "Edit":
                case "Change":
                    if (this.Tag.ToString().Equals("Add"))
                        cmd = new SqlCommand("" + cPublic.g_PrjCode + "000" + ".DBO.SP_INSERT_LOGIN", cPublic.Connection);
                    else if (this.Tag.ToString().Equals("Edit"))
                        cmd = new SqlCommand("" + cPublic.g_PrjCode + "000" + ".DBO.SP_UPDATE_LOGIN", cPublic.Connection);
                    else if (this.Tag.ToString().Equals("Change"))
                        cmd = new SqlCommand("" + cPublic.g_PrjCode + "000" + ".DBO.SP_UPDATE_LOGIN_PASSWORD", cPublic.Connection);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserLevel", Convert.ToInt32(cmbUserLevel.SelectedItem.ToString().Trim()));
                    cmd.Parameters.AddWithValue("@UserName", txtUserName.Text.Trim());
                    cmd.Parameters.AddWithValue("@userid", txtUserID.Text.Trim());
                    if (this.Tag.ToString().Equals("Add") || this.Tag.ToString().Equals("Change"))
                    {
                        cmd.Parameters.AddWithValue("@Password", txtPassword.Text.Trim());
                    }
                    break;
                case "Delete":
                    cmd = new SqlCommand("" + cPublic.g_PrjCode + "000" + ".DBO.SP_DELETE_LOGIN", cPublic.Connection);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid_3", txtUserID.Text.Trim());
                    break;
            }
        }
        private void FillFlex(DataTable dt)
        {
            cfgcompany.Rows.Count = 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cfgcompany.Rows.Add();
                cfgcompany[i + 1, 0] = dt.Rows[i][0];
                cfgcompany[i + 1, 1] = dt.Rows[i][1];
                cfgcompany[i + 1, 2] = false;
            }
        }
        private void frmUser_Load(object sender, EventArgs e)
        {
           
            //this.msgLabel.BackColor = Color.FromArgb(229, 229, 229);
            cfgcompany.Dock = DockStyle.Fill;
            adjustFormSize();

            con.ConnectMe(cPublic.password,cPublic.Sqluserid ,  cPublic.g_PrjCode +"000"  , cPublic.Sqlservername );
            cmbUserLevel.SelectedIndex = 0;
            string sql = "select code,dispname[NAME OF COMPANY] from " + cPublic.g_PrjCode + "000" + ".DBO.company order by code";
            SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            FillFlex(dt);
            switch (this.Tag.ToString())
            {
                case "Add":
                    btnSave.Text = "&Next";

                    break;
                case "Edit":
                    btnSave.Text = "&Next";
                    this.groupBox2.Size = new Size(this.groupBox2.Width, 160);
                    break;
                case "Delete":
                    btnSave.Text = "&Delete";
                    this.cmbUserLevel.Enabled = false;
                    this.txtUserName.Enabled = false;
                    break;
                case "Change":
                    btnSave.Text = "&Ok";
                    label3.Text = "New Password";
                    break;
            }

            this.txtUserID.Select();
            switch (this.Tag.ToString())
            {
                case "Change":
                    txtUserID.Size = new Size(80, 23);
                    this.label5.Location = new Point(220, 16);
                    this.cmbUserLevel.Location = new Point(305, 14);
                    this.groupBox1.Size = new Size(387, 151);
                    this.msgLabel.Location = new Point(7, 170);
                    this.btnSave.Location = new Point(108, 195);
                    this.btnCancel.Location = new Point(189, 195);
                    this.Size = new Size(410, 260);
                    txtUserID.Text = cPublic.g_UserId;
                    SendKeys.Send("{TAB}");
                    break;
            }

            this.StartPosition = FormStartPosition.CenterScreen;

        }

        private void clearFields()
        {
            txtUserID.Enabled = true;
            txtUserID.Text = string.Empty;
            txtUserName.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtConPassword.Text = string.Empty;
            txtOldPassword.Enabled = true;
            txtOldPassword.Text = string.Empty;
            cmbUserLevel.SelectedIndex = 0;
            for (int i = 1; i < cfgcompany.Rows.Count; i++)
                cfgcompany[i, 2] = false;
            txtUserID.Focus();
        }

        private void txtUserID_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.Tag.Equals("Edit") || this.Tag.Equals("Delete") || this.Tag.Equals("Change"))
            {

                if (e.KeyCode.Equals(Keys.Enter))
                {
                   

                    cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cPublic.Connection;
                    cmd.Parameters.Clear();
                    if (this.Tag.Equals("Edit") || this.Tag.Equals("Change"))
                    {
                        cmd.CommandText = "select userid,UserName,Password,UserLevel from " + cPublic.g_PrjCode + "000" + ".DBO.LOGIN where userid=@str ";
                    }
                    else if (this.Tag.Equals("Delete"))
                    {
                        cmd.CommandText = "select userid,UserName,Password,UserLevel from " + cPublic.g_PrjCode + "000" + ".DBO.LOGIN where userid=@str and userid<>'Admin' and userid<>'Super' ";
                    }
                    cmd.Parameters.AddWithValue("@str", txtUserID.Text.Trim());

                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        txtUserName.Text = dr["UserName"].ToString();
                        cmbUserLevel.Text = dr["UserLevel"].ToString();
                    }
                    else
                    {
                        showMessage("Invalid UserId..", "red");
                        txtUserID.Focus();
                    }
                    dr.Close();
                }

                if (e.KeyCode.Equals(Keys.F5))
                {
                   

                    frmlookup lookUp = new frmlookup();
                    lookUp.m_con = cPublic.Connection;
                    lookUp.m_table = "" + cPublic.g_PrjCode + "000" + ".DBO.LOGIN";
                    lookUp.m_fields = "userid,UserName,Password,UserLevel";
                    lookUp.m_dispname = "User ID, User Name,Password,UserLevel";
                    lookUp.m_fldwidth = "75,150,0,0";
                    if (this.Tag.Equals("Edit") || this.Tag.Equals("Change"))
                    {
                        lookUp.m_condition = "";
                    }
                    else if (this.Tag.Equals("Delete"))
                    {
                        lookUp.m_condition = " userid<>'" + Gen.SQLFormat(cPublic.g_UserId) + "' ";
                    }
                    lookUp.ShowDialog();
                    if (lookUp.m_values.Count > 0)
                    {
                        txtUserID.Text = lookUp.m_values[0].ToString();
                        txtUserName.Text = lookUp.m_values[1].ToString();
                        cmbUserLevel.SelectedItem = lookUp.m_values[3].ToString();
                        txtUserID.Enabled = false;
                    }
                }
            }
        }


        private void cfgcompany_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSave.Focus();
            }
        }

        private void frmUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            showMessage("", "");
        }

        private void adjustFormSize()
        {
            switch (this.Tag.ToString())
            {
                case "Add":
                    label6.Visible = false;
                    txtOldPassword.Visible = false;
                    label3.Location = new Point(8, 69);
                    txtPassword.Location = new Point(134, 66);
                    label4.Location = new Point(8, 95);
                    txtConPassword.Location = new Point(134, 92);
                    label5.Location = new Point(8, 121);
                    cmbUserLevel.Location = new Point(134, 118);
                    //tabControl1.Size = new Size(378, 186);
                    groupBox1.Size = new Size(390, 151);
                    //pannel.Size = new Size(374, 174);
                    msgLabel.Location = new Point(7, 192);
                    btnSave.Location = new Point(103, 217);
                    btnCancel.Location = new Point(184, 217);
                    this.btnfinish.Location = new Point(270, 217);
                    this.btnfinish.Visible = false;
                    this.Size = new Size(410, 288);
                    break;
                case "Edit":
                    this.txtPassword.Visible = false;
                    this.txtOldPassword.Visible = false;
                    this.txtConPassword.Visible = false;
                    this.label6.Visible = false;
                    this.label3.Visible = false;
                    this.label4.Visible = false;
                    label5.Location = new Point(8, 69);
                    cmbUserLevel.Location = new Point(134, 66);
                    groupBox1.Size = new Size(390, 100);

                    msgLabel.Location = new Point(7, 150);
                    btnSave.Location = new Point(103, 175);
                    btnCancel.Location = new Point(184, 175);
                    this.btnfinish.Location = new Point(270, 175);
                    this.btnfinish.Visible = false;
                    this.Size = new Size(410, 250);
                    break;
                case "Delete":
                    this.txtPassword.Visible = false;
                    this.txtOldPassword.Visible = false;
                    this.txtConPassword.Visible = false;
                    this.label6.Visible = false;
                    this.label3.Visible = false;
                    this.label4.Visible = false;
                    this.cfgcompany.AllowEditing = false;

                    this.groupBox2.Visible = true;

                    label5.Location = new Point(8, 69);
                    cmbUserLevel.Location = new Point(134, 66);
                    groupBox1.Size = new Size(387, 100);
                    groupBox2.Location = new Point(9, 110);

                    msgLabel.Location = new Point(7, 300);

                    btnSave.Location = new Point(103, 320);
                    btnCancel.Location = new Point(184, 320);
                    this.btnfinish.Location = new Point(270, 320);


                    this.btnfinish.Visible = false;


                    this.Size = new Size(410, 400);

                    break;
                case "Change":
                    this.txtUserName.Enabled = false;
                    this.cmbUserLevel.Enabled = false;
                    this.cfgcompany.AllowEditing = false;
                    this.btnfinish.Visible = false;
                    this.Size = new Size(410, 320);
                    this.groupBox2.Visible = false;
                    break;
            }
        }

        private Boolean checkPassword(string userID, string password)
        {
           

            Boolean status = false;
            string query = string.Empty;
            query = "select * from " + cPublic.g_PrjCode + "000" + ".DBO.LOGIN where userid = @userID and Password = @password";
            SqlCommand cmd = new SqlCommand(query, cPublic.Connection);
            cmd.Parameters.AddWithValue("@userID", userID.Trim());
            cmd.Parameters.AddWithValue("@password", password.Trim());
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read()) { status = true; }
            dr.Close();
            return status;
        }

        private void txtOldPassword_Validating(object sender, CancelEventArgs e)
        {
            if (!cGen.isFormClosing())
            {
                if (!checkPassword(txtUserID.Text.Trim(), txtOldPassword.Text.Trim()))
                {
                    showMessage("Invalid Password ..", "red");
                    txtOldPassword.Focus();
                }
                else { txtOldPassword.Enabled = false; }
            }
        }

        private void txtUserID_Enter(object sender, EventArgs e)
        {
            this.txtUserID.SelectAll();
        }

        private void txtUserName_Enter(object sender, EventArgs e)
        {
            this.txtUserName.SelectAll();
        }

        private void txtOldPassword_Enter(object sender, EventArgs e)
        {
            this.txtOldPassword.SelectAll();
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            this.txtPassword.SelectAll();
        }

        private void txtConPassword_Enter(object sender, EventArgs e)
        {
            this.txtConPassword.SelectAll();
        }


        private void Next()
        {
            if (groupBox1.Visible == true)
            {
                groupBox2.Visible = true;
                groupBox1.Visible = false;
                groupBox2.Location = groupBox1.Location;
                this.btnSave.Text = "&Back";
                this.btnCancel.Visible = true;
                this.btnfinish.Visible = true;
                if (this.Tag.ToString() == "Add")
                {
                    this.btnCancel.Location = new Point(270, 217);
                    this.btnfinish.Location = new Point(184, 217);
                }
                else
                {
                    this.btnCancel.Location = new Point(270, 175);
                    this.btnfinish.Location = new Point(184, 175);
                }
                this.cfgcompany.Select(1, 1);
            }
            else if (groupBox2.Visible == true)
            {
                groupBox1.Visible = true;
                groupBox2.Visible = false;
                this.btnfinish.Visible = false;
                this.btnSave.Text = "&Next";
                adjustFormSize();
                if (this.Tag.ToString() == "Add")
                {
                    this.btnCancel.Location = new Point(184, 217);
                    this.btnfinish.Location = new Point(184, 217);

                }
                else
                {
                    this.btnCancel.Location = new Point(184, 175);
                    this.btnfinish.Location = new Point(103, 175);
                }
                // this.btnCancel.Location = new Point(184, 217);
                this.txtUserID.Select();
            }

            if (this.Tag.ToString() == "Add" || this.Tag.ToString() == "Edit")
            {
                if (Ckeckcompselection())
                {
                    this.btnfinish.Enabled = false;
                }
                else
                {
                    this.btnfinish.Enabled = true;
                }
            }
        }

        private void btnfinish_Click(object sender, EventArgs e)
        {
            v_Check = false;
            Save_Screen();
        }



        private void cfgcompany_AfterEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (this.Tag.ToString() == "Add" || this.Tag.ToString() == "Edit")
            {
                if (Ckeckcompselection())
                {
                    this.btnfinish.Enabled = false;
                    showMessage("Select atleast 1 company..", "red");
                }
                else
                {
                    this.btnfinish.Enabled = true;
                    showMessage("", "");
                }
            }
        }

        private void cmbUserLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            Level_Change();
        }
        private void Level_Change()
        {
            if (cmbUserLevel.Text == "9")
            {
                if (this.Tag.ToString() == "Add")
                {
                    //btnSave.Location = new Point(270, 217);
                    this.btnfinish.Location = new Point(103, 217);
                    this.btnfinish.Visible = true;
                    this.btnfinish.Enabled = true;
                    this.btnSave.Visible = false;
                }
                else if (this.Tag.ToString() == "Edit")
                {
                    //btnSave.Location = new Point(270, 175);                  
                    this.btnfinish.Location = new Point(103, 175);
                    this.btnfinish.Visible = true;
                    this.btnfinish.Enabled = true;
                    this.btnSave.Visible = false;
                }
            }
            else
            {
                if (this.Tag.ToString() == "Add")
                {
                    //btnSave.Location = new Point(103, 217);
                    this.btnfinish.Location = new Point(270, 217);
                    this.btnfinish.Visible = false;
                    this.btnSave.Visible = true;
                }
                else if (this.Tag.ToString() == "Edit")
                {
                    //btnSave.Location = new Point(103, 175);
                    this.btnfinish.Location = new Point(270, 175);
                    this.btnfinish.Visible = false;
                    this.btnSave.Visible = true;
                }
            }
        }

      
    }
}