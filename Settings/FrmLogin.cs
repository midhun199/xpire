using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using fssgen;
using System.Data.SqlClient;
using Xpire.Classes;

namespace Xpire.Settings
{
    public partial class FrmLogin : Form
    {
        fssgen.fssgen gen = new fssgen.fssgen();
        cGeneral cgen = new cGeneral();
        public FrmLogin()
        {
            InitializeComponent();
        }     
       
        private void Loads()
        {
            try
            {
                cGeneral gen1 = new cGeneral();
                string userid = txtuserid.Text.Trim();
                string pwd = txtpwd.Text.Trim();
                string sql = " declare @password varchar(20) "
                + " set @password='" + gen.SQLFormat(pwd).ToString() + "' select * from login where userid='" + gen.SQLFormat(userid) + "' and password=@password  COLLATE Latin1_General_CS_AS";
                SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
                SqlDataReader dr = comm.ExecuteReader();
                if (dr.Read())
                {
                    cPublic.Login = true;
                    cPublic.g_UserId = userid;
                    cPublic.g_UserLevel = dr["Userlevel"].ToString();
                    //if (Application.OpenForms.Count > 1)
                    //{
                    //    for (int i = 0; i < Application.OpenForms.Count; i++)
                    //    {
                    //        if (Application.OpenForms[i].IsMdiContainer)
                    //        {
                    //            ((frmMain)Application.OpenForms[i]).StatusbarUid();
                    //            break;
                    //        }
                    //    }
                    //}

                    dr.Close();
                    if (Convert.ToInt32( cPublic.g_UserLevel)  <9)
                    {
                        string sqls = "select * from usercompanies where userid='" + cPublic.g_UserId + "' ";
                        SqlDataAdapter das = new SqlDataAdapter(sqls, cPublic.Connection);
                        DataSet dss = new DataSet();
                        das.Fill(dss);
                        if (dss.Tables[0].Rows.Count > 0)
                        {
                            cPublic.Login = true;
                        }
                        else
                        {
                            cPublic.Login = false;
                        }
                    }


                    sql = "select distinct path from company";
                    SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    cPublic.drive = ds.Tables[0].Rows[0]["path"].ToString();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("UserId/Password mismatch !", cPublic.messagename,MessageBoxButtons.OK,MessageBoxIcon.Warning);
                    txtuserid.Focus();
                    txtuserid.SelectAll();
                    dr.Close();
                    cPublic.Login = false;
                    return;
                }
                dr.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("Please contact software vendor !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms.Count == 1)
            {
                cPublic.closing = true;
                Close();
            }
            else
                Application.ExitThread();
               
        }

         private void FrmLogin_Load(object sender, EventArgs e)
        {
            cConnection con = new cConnection();
            if (!con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername))
            {
                MessageBox.Show("Database connection failed!", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Application.Exit();
            }
        }
        
        private void FrmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btncancel_Click(sender, e);
            }
        }

        private void txtuserid_Enter(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text != String.Empty)
            {
                ((TextBox)sender).SelectAll();
            }
        }

        private void txtuserid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtpwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Loads();
                
                if (cPublic.Login ==true )
                {
                    cPublic.LoginDate = System.DateTime.Now;
                    cgen.Firmselection();
                }
                e.Handled = true; 
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Loads();
            if ( cPublic.Login == true)
            {
                cgen.Firmselection();
            }
           
        }   
 
    }
}