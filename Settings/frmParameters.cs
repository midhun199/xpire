using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Xpire.Classes;

namespace Xpire.Settings
{
    public partial class frmParameters : Form
    {
        string query = string.Empty;
        SqlCommand cmd;

        public frmParameters()
        {
            InitializeComponent();
        }

        private void grid_initialize()
        {
            flxSettings.Cols.Count = 3;
            flxSettings.Rows.Count = 10;

            flxSettings[0, 0] = "SL";
            flxSettings.Cols[0].Name = "SL";
            flxSettings.Cols["SL"].Width = 25;

            flxSettings[0, 1] = "Parameters";
            flxSettings.Cols[1].Name = "Name";

            flxSettings[0, 2] = "Values";
            flxSettings.Cols[2].Name = "Values";

            flxSettings.Cols["Name"].AllowEditing = false;
        }

        private void loadParameters() 
        {
            query = "select * from public_var";
            SqlDataAdapter da = new  SqlDataAdapter(query, cPublic.Connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            flxSettings.Rows.Count = dt.Rows.Count + 1;
            for (int i = 0; i < dt.Rows.Count; i++) 
            {
                flxSettings[i + 1, 0] = i + 1;
                flxSettings[i + 1, "Name"] = dt.Rows[i]["var_name"] + "";
                flxSettings[i + 1, "Values"] = dt.Rows[i]["var_values"] + "";
            }
        }

        private void frmParameters_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) { SendKeys.Send("{Tab}"); }
            else if (e.KeyCode == Keys.Escape) { Close(); }
        }

        private void frmParameters_Load(object sender, EventArgs e)
        {
            grid_initialize();
            loadParameters();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void flxSettings_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{F2}");
        }

        private void flxSettings_Enter(object sender, EventArgs e)
        {
            SendKeys.Send("{F2}");
        }

        private void flxSettings_EnterCell(object sender, EventArgs e)
        {
            flxSettings.Col = flxSettings.Cols["values"].Index;
            SendKeys.Send("{F2}");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                query = "delete from public_var";
                cmd = new SqlCommand(query, cPublic.Connection);
                cmd.ExecuteNonQuery();

                query = "insert into public_var values (@varname,@varvalues)";
                cmd = new SqlCommand(query, cPublic.Connection);
                for (int i = 1; i < flxSettings.Rows.Count; i++)
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@varname", flxSettings[i, "name"] + "");
                    cmd.Parameters.AddWithValue("@varvalues", flxSettings[i, "values"] + "");
                    cmd.ExecuteNonQuery();
                }

                cGeneral gens = new cGeneral();
                gens.setparameters(); 

                MessageBox.Show("Parameters Saved Successfully..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex) 
            {
                MessageBox.Show("Invalid Data..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(ex.Message);
            }
            Close();
        }
    }
}