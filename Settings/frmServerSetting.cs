using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using Xpire.Classes;

namespace Xpire.Settings
{
    public partial class frmServerSetting : Form
    {
        CAddFirm Firm = new CAddFirm();
        public frmServerSetting()
        {
            InitializeComponent();
        }
        cConnection con = new cConnection();
        CSP sp = new CSP();
        cGeneral gen = new cGeneral();
        string code = "";
        private void btnok_Click(object sender, EventArgs e)
        {
            if (txtserver.Visible == true)
            {
                if (txtserver.Text == string.Empty)
                { MessageBox.Show("Server Name cannot be blank..."); txtserver.Focus(); return; }

                string machinename = txtserver.Text;
                string[] split = machinename.Split(new Char[] { '\\' });
                foreach (string s in split)
                {
                    if (s.Trim() != "")
                    { machinename = s.Trim(); break; }
                }

                //-----------------------------------------------------------------------
                if (con.ConnectMe(cPublic.password,cPublic.Sqluserid , "master", txtserver.Text))
                {
                    txtserver.Visible = false;


                    if ((Environment.MachineName).ToUpper() == (machinename).ToUpper())
                    {
                        cmbdrive.Visible = true;
                        cmbdrive.Focus();
                        label3.Text = "Drive";
                        drive(machinename);
                    }
                    else
                    {
                        if (con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", txtserver.Text))
                        {
                            gen.WriteNetcon(txtserver.Text, cPublic.Sqluserid, cPublic.password, cPublic.g_PrjCode);
                            frmFirmSelection frmfirm = new frmFirmSelection();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("please set database in the server machine", this.Text);
                            this.Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Server connection failed!", this.Text);
                    txtserver.Focus();
                }

            }

            else if (cmbdrive.Visible == true)
            {
                cGeneral gen = new cGeneral();
                cPublic.drive = cmbdrive.SelectedItem.ToString();

                string machinename = txtserver.Text;
                string[] split = machinename.Split(new Char[] { '\\' });
                foreach (string s in split)
                {
                    if (s.Trim() != "")
                    { machinename = s.Trim(); break; }
                }

                if ((Environment.MachineName).ToUpper() == (machinename).ToUpper())
                {

                    gen.WriteNetcon(txtserver.Text, cPublic.Sqluserid, cPublic.password, cPublic.g_PrjCode);
                    gen.ServerDet();
                    string path = cPublic.drive + "\\" + cPublic.g_PrjCode;
                    gen.createpath("000");
                    if (!Firm.CreateDB(cPublic.g_PrjCode + "000", path))
                    {
                        con.ConnectMe(cPublic.password,cPublic.Sqluserid , cPublic.g_PrjCode + "000", cPublic.Sqlservername );
                        sp.CreateTableCompany("company");
                        sp.CreateMail();
                        gen.createpath("001");
                        Firm.CreateDB(cPublic.g_PrjCode + "001", path);


                        con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "001", cPublic.Sqlservername);
                        sp.tbl_public_var();
                        Firm.FirmAdd(cPublic.g_PrjCode + "001", "0001");
                        sp.firminsert("0001","DEMO");
                        //sp.headinsert("0001");

                    }
                    this.Close();

                }
                else
                {
                    if (con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", txtserver.Text))
                    {

                        frmFirmSelection frmfirm = new frmFirmSelection();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Server connection failed!", this.Text);
                        this.Close();
                    }
                }

                //cPublic.Connection;
            }
        }

        public void drive(string machinename)
        {
            if ((Environment.MachineName).ToUpper() == (machinename).ToUpper())
            {
                if (!con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", txtserver.Text))
                {

                    DriveInfo[] getInfo = DriveInfo.GetDrives();
                    getInfo = DriveInfo.GetDrives();
                    foreach (DriveInfo d in getInfo)
                    {
                        if (d.DriveType == DriveType.Fixed)
                        {
                            cmbdrive.Items.Add(d.Name);
                        }
                    }
                    cmbdrive.SelectedIndex = 0;
                    //server = false;
                    return;
                }
                else
                {
                    if (check())
                    {
                        gen.WriteNetcon(txtserver.Text, cPublic.Sqluserid, cPublic.password, cPublic.g_PrjCode);
                        gen.ServerDet();
                        MessageBox.Show("Server Creation is sucessfully completed ", this.Text);
                        this.Close();
                    }
                    else
                    {
                        if ((MessageBox.Show("Do you want to continue partial company creation ", this.Text, MessageBoxButtons.YesNo)).ToString() == "Yes")
                        {
                            gen.createpath("001");
                            string path = cPublic.drive + "\\" + cPublic.g_PrjCode;
                            Firm.CreateDB(cPublic.g_PrjCode + "001", path);
                            con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "001", cPublic.Sqlservername);
                            Firm.FirmAdd(cPublic.g_PrjCode + "001", "0001");
                            sp.firminsert("0001", "DEMO");
                        }
                    }

                }
            }
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            cPublic.closing = true;
            if (this.Tag + "" != "SE")
            {
                Application.Exit();
            }
        }

        private void frmServerSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Application.Exit();
        }

        private void txtserver_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtpassword_TextChanged(object sender, EventArgs e)
        {
            if (txtpassword.Text == "246810")
            {
                label3.Text = "Server Name";
                txtpassword.Visible = false;
                txtserver.Visible = true;
            }
        }

        private void frmServerSetting_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            { SendKeys.Send("{TAB}"); }
            else if (e.KeyCode.Equals(Keys.Escape))
            { this.Close(); }
        }

        private Boolean check()
        {
            string sql = "select code from company order by code";
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            SqlDataReader dr = null;
            dr = comm.ExecuteReader();
            if (dr.Read())
            {
                code = Convert.ToString(dr.GetValue(0));
                dr.Close();
                return true;
            }
            else
            {
                dr.Close();
                return false;
            }

        }
    }
}