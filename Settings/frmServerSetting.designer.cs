namespace Xpire.Settings
{
    partial class frmServerSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btncancel = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.txtserver = new System.Windows.Forms.TextBox();
            this.cmbdrive = new System.Windows.Forms.ComboBox();
            this.pnlserver = new System.Windows.Forms.Panel();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlserver.SuspendLayout();
            this.SuspendLayout();
            // 
            // btncancel
            // 
            this.btncancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btncancel.Location = new System.Drawing.Point(198, 126);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(69, 24);
            this.btncancel.TabIndex = 2;
            this.btncancel.Text = "&Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(93, 126);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(69, 24);
            this.btnok.TabIndex = 1;
            this.btnok.Text = "&OK";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // txtserver
            // 
            this.txtserver.Location = new System.Drawing.Point(155, 35);
            this.txtserver.MaximumSize = new System.Drawing.Size(149, 21);
            this.txtserver.MaxLength = 256;
            this.txtserver.Name = "txtserver";
            this.txtserver.Size = new System.Drawing.Size(149, 20);
            this.txtserver.TabIndex = 0;
            // 
            // cmbdrive
            // 
            this.cmbdrive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbdrive.FormattingEnabled = true;
            this.cmbdrive.Location = new System.Drawing.Point(155, 34);
            this.cmbdrive.Name = "cmbdrive";
            this.cmbdrive.Size = new System.Drawing.Size(89, 21);
            this.cmbdrive.TabIndex = 1;
            this.cmbdrive.Visible = false;
            // 
            // pnlserver
            // 
            this.pnlserver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.pnlserver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlserver.Controls.Add(this.txtpassword);
            this.pnlserver.Controls.Add(this.label3);
            this.pnlserver.Controls.Add(this.cmbdrive);
            this.pnlserver.Controls.Add(this.txtserver);
            this.pnlserver.Location = new System.Drawing.Point(13, 12);
            this.pnlserver.Name = "pnlserver";
            this.pnlserver.Size = new System.Drawing.Size(336, 91);
            this.pnlserver.TabIndex = 0;
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(155, 35);
            this.txtpassword.MaximumSize = new System.Drawing.Size(149, 21);
            this.txtpassword.MaxLength = 256;
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.PasswordChar = '*';
            this.txtpassword.Size = new System.Drawing.Size(149, 20);
            this.txtpassword.TabIndex = 0;
            this.txtpassword.TextChanged += new System.EventHandler(this.txtpassword_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Password";
            // 
            // frmServerSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(361, 163);
            this.ControlBox = false;
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.pnlserver);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmServerSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Server Settings";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmServerSetting_KeyDown);
            this.pnlserver.ResumeLayout(false);
            this.pnlserver.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.TextBox txtserver;
        private System.Windows.Forms.ComboBox cmbdrive;
        private System.Windows.Forms.Panel pnlserver;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtpassword;
    }
}