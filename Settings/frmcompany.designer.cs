namespace Xpire.Settings
{
    partial class frmcompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlselection = new System.Windows.Forms.Panel();
            this.lstSource = new System.Windows.Forms.ComboBox();
            this.cmbcreation1 = new System.Windows.Forms.ComboBox();
            this.lblcopy = new System.Windows.Forms.Label();
            this.lblcompcre = new System.Windows.Forms.Label();
            this.pnlhistory = new System.Windows.Forms.Panel();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txtphone2 = new System.Windows.Forms.TextBox();
            this.txtphone1 = new System.Windows.Forms.TextBox();
            this.txtadd4 = new System.Windows.Forms.TextBox();
            this.txtadd3 = new System.Windows.Forms.TextBox();
            this.txtadd2 = new System.Windows.Forms.TextBox();
            this.txtadd1 = new System.Windows.Forms.TextBox();
            this.lblemail = new System.Windows.Forms.Label();
            this.lblphone2 = new System.Windows.Forms.Label();
            this.lblphone1 = new System.Windows.Forms.Label();
            this.lbladdress1 = new System.Windows.Forms.Label();
            this.txtcomptitle = new System.Windows.Forms.TextBox();
            this.lblcomptitle = new System.Windows.Forms.Label();
            this.txtcompname = new System.Windows.Forms.TextBox();
            this.lblcompname = new System.Windows.Forms.Label();
            this.txttinno = new System.Windows.Forms.TextBox();
            this.pnltin = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtcompcode = new System.Windows.Forms.TextBox();
            this.txtFCode = new System.Windows.Forms.TextBox();
            this.lblcompcode = new System.Windows.Forms.Label();
            this.lblfirmcode = new System.Windows.Forms.Label();
            this.cmbacctype = new System.Windows.Forms.ComboBox();
            this.txtjur = new System.Windows.Forms.TextBox();
            this.txtcstno = new System.Windows.Forms.TextBox();
            this.lbljuridification = new System.Windows.Forms.Label();
            this.lblcstno = new System.Windows.Forms.Label();
            this.txtcfor = new System.Windows.Forms.TextBox();
            this.txtcurrency = new System.Windows.Forms.TextBox();
            this.txtpanno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblacctext = new System.Windows.Forms.Label();
            this.lblcurrency = new System.Windows.Forms.Label();
            this.lblpanno = new System.Windows.Forms.Label();
            this.lbltinno = new System.Windows.Forms.Label();
            this.dtpcl = new System.Windows.Forms.DateTimePicker();
            this.dtpOP = new System.Windows.Forms.DateTimePicker();
            this.lblcldate = new System.Windows.Forms.Label();
            this.lblopdate = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnnext = new System.Windows.Forms.Button();
            this.btnok = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Browse = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtAbbr = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlselection.SuspendLayout();
            this.pnlhistory.SuspendLayout();
            this.pnltin.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlselection
            // 
            this.pnlselection.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlselection.Controls.Add(this.lstSource);
            this.pnlselection.Controls.Add(this.cmbcreation1);
            this.pnlselection.Controls.Add(this.lblcopy);
            this.pnlselection.Controls.Add(this.lblcompcre);
            this.pnlselection.Location = new System.Drawing.Point(4, 4);
            this.pnlselection.Name = "pnlselection";
            this.pnlselection.Size = new System.Drawing.Size(443, 67);
            this.pnlselection.TabIndex = 0;
            // 
            // lstSource
            // 
            this.lstSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lstSource.Enabled = false;
            this.lstSource.FormattingEnabled = true;
            this.lstSource.Location = new System.Drawing.Point(102, 36);
            this.lstSource.Name = "lstSource";
            this.lstSource.Size = new System.Drawing.Size(331, 21);
            this.lstSource.TabIndex = 1;
            this.lstSource.SelectedIndexChanged += new System.EventHandler(this.lstSource_SelectedIndexChanged);
            // 
            // cmbcreation1
            // 
            this.cmbcreation1.FormattingEnabled = true;
            this.cmbcreation1.Items.AddRange(new object[] {
            " As Fresh",
            " From Existing Company"});
            this.cmbcreation1.Location = new System.Drawing.Point(102, 9);
            this.cmbcreation1.Name = "cmbcreation1";
            this.cmbcreation1.Size = new System.Drawing.Size(331, 21);
            this.cmbcreation1.TabIndex = 0;
            this.cmbcreation1.SelectedIndexChanged += new System.EventHandler(this.cmbcreation1_SelectedIndexChanged);
            this.cmbcreation1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbcreation1_KeyDown);
            // 
            // lblcopy
            // 
            this.lblcopy.BackColor = System.Drawing.Color.Transparent;
            this.lblcopy.Location = new System.Drawing.Point(11, 36);
            this.lblcopy.Name = "lblcopy";
            this.lblcopy.Size = new System.Drawing.Size(100, 20);
            this.lblcopy.TabIndex = 9;
            this.lblcopy.Text = "Copy From";
            this.lblcopy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblcompcre
            // 
            this.lblcompcre.Location = new System.Drawing.Point(9, 9);
            this.lblcompcre.Name = "lblcompcre";
            this.lblcompcre.Size = new System.Drawing.Size(87, 20);
            this.lblcompcre.TabIndex = 8;
            this.lblcompcre.Text = "Create Company";
            this.lblcompcre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlhistory
            // 
            this.pnlhistory.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlhistory.Controls.Add(this.txtemail);
            this.pnlhistory.Controls.Add(this.txtphone2);
            this.pnlhistory.Controls.Add(this.txtphone1);
            this.pnlhistory.Controls.Add(this.txtadd4);
            this.pnlhistory.Controls.Add(this.txtadd3);
            this.pnlhistory.Controls.Add(this.txtadd2);
            this.pnlhistory.Controls.Add(this.txtadd1);
            this.pnlhistory.Controls.Add(this.lblemail);
            this.pnlhistory.Controls.Add(this.lblphone2);
            this.pnlhistory.Controls.Add(this.lblphone1);
            this.pnlhistory.Controls.Add(this.lbladdress1);
            this.pnlhistory.Controls.Add(this.txtAbbr);
            this.pnlhistory.Controls.Add(this.txtcomptitle);
            this.pnlhistory.Controls.Add(this.label2);
            this.pnlhistory.Controls.Add(this.lblcomptitle);
            this.pnlhistory.Controls.Add(this.txtcompname);
            this.pnlhistory.Controls.Add(this.lblcompname);
            this.pnlhistory.Location = new System.Drawing.Point(4, 75);
            this.pnlhistory.Name = "pnlhistory";
            this.pnlhistory.Size = new System.Drawing.Size(443, 277);
            this.pnlhistory.TabIndex = 1;
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(102, 243);
            this.txtemail.MaxLength = 40;
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(332, 20);
            this.txtemail.TabIndex = 11;
            this.txtemail.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // txtphone2
            // 
            this.txtphone2.Location = new System.Drawing.Point(102, 217);
            this.txtphone2.MaxLength = 40;
            this.txtphone2.Name = "txtphone2";
            this.txtphone2.Size = new System.Drawing.Size(332, 20);
            this.txtphone2.TabIndex = 10;
            this.txtphone2.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // txtphone1
            // 
            this.txtphone1.Location = new System.Drawing.Point(102, 191);
            this.txtphone1.MaxLength = 40;
            this.txtphone1.Name = "txtphone1";
            this.txtphone1.Size = new System.Drawing.Size(332, 20);
            this.txtphone1.TabIndex = 9;
            this.txtphone1.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // txtadd4
            // 
            this.txtadd4.Location = new System.Drawing.Point(102, 166);
            this.txtadd4.MaxLength = 40;
            this.txtadd4.Name = "txtadd4";
            this.txtadd4.Size = new System.Drawing.Size(332, 20);
            this.txtadd4.TabIndex = 8;
            this.txtadd4.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // txtadd3
            // 
            this.txtadd3.Location = new System.Drawing.Point(102, 140);
            this.txtadd3.MaxLength = 40;
            this.txtadd3.Name = "txtadd3";
            this.txtadd3.Size = new System.Drawing.Size(332, 20);
            this.txtadd3.TabIndex = 7;
            this.txtadd3.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // txtadd2
            // 
            this.txtadd2.Location = new System.Drawing.Point(102, 114);
            this.txtadd2.MaxLength = 40;
            this.txtadd2.Name = "txtadd2";
            this.txtadd2.Size = new System.Drawing.Size(332, 20);
            this.txtadd2.TabIndex = 6;
            this.txtadd2.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // txtadd1
            // 
            this.txtadd1.Location = new System.Drawing.Point(102, 88);
            this.txtadd1.MaxLength = 40;
            this.txtadd1.Name = "txtadd1";
            this.txtadd1.Size = new System.Drawing.Size(332, 20);
            this.txtadd1.TabIndex = 5;
            this.txtadd1.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // lblemail
            // 
            this.lblemail.Location = new System.Drawing.Point(9, 242);
            this.lblemail.Name = "lblemail";
            this.lblemail.Size = new System.Drawing.Size(100, 20);
            this.lblemail.TabIndex = 9;
            this.lblemail.Text = "Email-ID :";
            this.lblemail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblphone2
            // 
            this.lblphone2.Location = new System.Drawing.Point(9, 216);
            this.lblphone2.Name = "lblphone2";
            this.lblphone2.Size = new System.Drawing.Size(100, 20);
            this.lblphone2.TabIndex = 9;
            this.lblphone2.Text = "Mobile No :";
            this.lblphone2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblphone1
            // 
            this.lblphone1.Location = new System.Drawing.Point(9, 190);
            this.lblphone1.Name = "lblphone1";
            this.lblphone1.Size = new System.Drawing.Size(100, 20);
            this.lblphone1.TabIndex = 9;
            this.lblphone1.Text = "Phone No :";
            this.lblphone1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbladdress1
            // 
            this.lbladdress1.Location = new System.Drawing.Point(9, 88);
            this.lbladdress1.Name = "lbladdress1";
            this.lbladdress1.Size = new System.Drawing.Size(100, 20);
            this.lbladdress1.TabIndex = 9;
            this.lbladdress1.Text = "Address";
            this.lbladdress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtcomptitle
            // 
            this.txtcomptitle.Location = new System.Drawing.Point(102, 35);
            this.txtcomptitle.MaxLength = 40;
            this.txtcomptitle.Name = "txtcomptitle";
            this.txtcomptitle.Size = new System.Drawing.Size(332, 20);
            this.txtcomptitle.TabIndex = 3;
            this.txtcomptitle.Enter += new System.EventHandler(this.txtcompname_Enter);
            this.txtcomptitle.Leave += new System.EventHandler(this.txtcompname_TextChanged);
            // 
            // lblcomptitle
            // 
            this.lblcomptitle.Location = new System.Drawing.Point(9, 34);
            this.lblcomptitle.Name = "lblcomptitle";
            this.lblcomptitle.Size = new System.Drawing.Size(100, 20);
            this.lblcomptitle.TabIndex = 9;
            this.lblcomptitle.Text = "Company Title";
            this.lblcomptitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtcompname
            // 
            this.txtcompname.Location = new System.Drawing.Point(102, 9);
            this.txtcompname.MaxLength = 40;
            this.txtcompname.Name = "txtcompname";
            this.txtcompname.Size = new System.Drawing.Size(331, 20);
            this.txtcompname.TabIndex = 2;
            this.txtcompname.Enter += new System.EventHandler(this.txtcompname_Enter);
            this.txtcompname.Leave += new System.EventHandler(this.txtcompname_TextChanged);
            // 
            // lblcompname
            // 
            this.lblcompname.Location = new System.Drawing.Point(9, 8);
            this.lblcompname.Name = "lblcompname";
            this.lblcompname.Size = new System.Drawing.Size(87, 20);
            this.lblcompname.TabIndex = 9;
            this.lblcompname.Text = "Display Name";
            this.lblcompname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txttinno
            // 
            this.txttinno.Location = new System.Drawing.Point(113, 17);
            this.txttinno.MaxLength = 20;
            this.txttinno.Name = "txttinno";
            this.txttinno.Size = new System.Drawing.Size(126, 20);
            this.txttinno.TabIndex = 0;
            this.txttinno.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // pnltin
            // 
            this.pnltin.BackColor = System.Drawing.Color.Silver;
            this.pnltin.Controls.Add(this.checkBox1);
            this.pnltin.Controls.Add(this.txtcompcode);
            this.pnltin.Controls.Add(this.txtFCode);
            this.pnltin.Controls.Add(this.lblcompcode);
            this.pnltin.Controls.Add(this.lblfirmcode);
            this.pnltin.Controls.Add(this.cmbacctype);
            this.pnltin.Controls.Add(this.txtjur);
            this.pnltin.Controls.Add(this.txtcstno);
            this.pnltin.Controls.Add(this.lbljuridification);
            this.pnltin.Controls.Add(this.lblcstno);
            this.pnltin.Controls.Add(this.txtcfor);
            this.pnltin.Controls.Add(this.txtcurrency);
            this.pnltin.Controls.Add(this.txtpanno);
            this.pnltin.Controls.Add(this.txttinno);
            this.pnltin.Controls.Add(this.label1);
            this.pnltin.Controls.Add(this.lblacctext);
            this.pnltin.Controls.Add(this.lblcurrency);
            this.pnltin.Controls.Add(this.lblpanno);
            this.pnltin.Controls.Add(this.lbltinno);
            this.pnltin.Location = new System.Drawing.Point(452, 4);
            this.pnltin.Name = "pnltin";
            this.pnltin.Size = new System.Drawing.Size(255, 347);
            this.pnltin.TabIndex = 2;
            this.pnltin.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(92, 231);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox1.Size = new System.Drawing.Size(144, 17);
            this.checkBox1.TabIndex = 55;
            this.checkBox1.Text = "Set Company as default  ";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // txtcompcode
            // 
            this.txtcompcode.AcceptsReturn = true;
            this.txtcompcode.BackColor = System.Drawing.SystemColors.Window;
            this.txtcompcode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtcompcode.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcompcode.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtcompcode.Location = new System.Drawing.Point(111, 199);
            this.txtcompcode.MaxLength = 3;
            this.txtcompcode.Name = "txtcompcode";
            this.txtcompcode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtcompcode.Size = new System.Drawing.Size(126, 20);
            this.txtcompcode.TabIndex = 5;
            this.txtcompcode.Enter += new System.EventHandler(this.txtcompname_Enter);
            this.txtcompcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFCode_KeyPress);
            // 
            // txtFCode
            // 
            this.txtFCode.AcceptsReturn = true;
            this.txtFCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtFCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtFCode.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFCode.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtFCode.Location = new System.Drawing.Point(111, 173);
            this.txtFCode.MaxLength = 4;
            this.txtFCode.Name = "txtFCode";
            this.txtFCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFCode.Size = new System.Drawing.Size(126, 20);
            this.txtFCode.TabIndex = 4;
            this.txtFCode.Enter += new System.EventHandler(this.txtcompname_Enter);
            this.txtFCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFCode_KeyPress);
            // 
            // lblcompcode
            // 
            this.lblcompcode.BackColor = System.Drawing.Color.Transparent;
            this.lblcompcode.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblcompcode.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcompcode.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblcompcode.Location = new System.Drawing.Point(18, 202);
            this.lblcompcode.Name = "lblcompcode";
            this.lblcompcode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblcompcode.Size = new System.Drawing.Size(87, 17);
            this.lblcompcode.TabIndex = 54;
            this.lblcompcode.Text = "Company Code";
            // 
            // lblfirmcode
            // 
            this.lblfirmcode.BackColor = System.Drawing.Color.Transparent;
            this.lblfirmcode.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblfirmcode.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfirmcode.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblfirmcode.Location = new System.Drawing.Point(18, 176);
            this.lblfirmcode.Name = "lblfirmcode";
            this.lblfirmcode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblfirmcode.Size = new System.Drawing.Size(87, 17);
            this.lblfirmcode.TabIndex = 54;
            this.lblfirmcode.Text = "Firm Code";
            // 
            // cmbacctype
            // 
            this.cmbacctype.FormattingEnabled = true;
            this.cmbacctype.Items.AddRange(new object[] {
            "SINGLE",
            "DOUBLE"});
            this.cmbacctype.Location = new System.Drawing.Point(111, 261);
            this.cmbacctype.Name = "cmbacctype";
            this.cmbacctype.Size = new System.Drawing.Size(126, 21);
            this.cmbacctype.TabIndex = 7;
            this.cmbacctype.Visible = false;
            this.cmbacctype.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbcreation1_KeyDown);
            this.cmbacctype.Validating += new System.ComponentModel.CancelEventHandler(this.cmbacctype_Validating);
            // 
            // txtjur
            // 
            this.txtjur.Location = new System.Drawing.Point(111, 147);
            this.txtjur.MaxLength = 40;
            this.txtjur.Name = "txtjur";
            this.txtjur.Size = new System.Drawing.Size(126, 20);
            this.txtjur.TabIndex = 3;
            this.txtjur.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // txtcstno
            // 
            this.txtcstno.Location = new System.Drawing.Point(111, 118);
            this.txtcstno.MaxLength = 20;
            this.txtcstno.Name = "txtcstno";
            this.txtcstno.Size = new System.Drawing.Size(126, 20);
            this.txtcstno.TabIndex = 2;
            this.txtcstno.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // lbljuridification
            // 
            this.lbljuridification.Location = new System.Drawing.Point(18, 146);
            this.lbljuridification.Name = "lbljuridification";
            this.lbljuridification.Size = new System.Drawing.Size(76, 20);
            this.lbljuridification.TabIndex = 9;
            this.lbljuridification.Text = "Juridification";
            this.lbljuridification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblcstno
            // 
            this.lblcstno.Location = new System.Drawing.Point(18, 117);
            this.lblcstno.Name = "lblcstno";
            this.lblcstno.Size = new System.Drawing.Size(53, 20);
            this.lblcstno.TabIndex = 9;
            this.lblcstno.Text = "CST No :";
            this.lblcstno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtcfor
            // 
            this.txtcfor.Location = new System.Drawing.Point(111, 313);
            this.txtcfor.MaxLength = 40;
            this.txtcfor.Name = "txtcfor";
            this.txtcfor.Size = new System.Drawing.Size(126, 20);
            this.txtcfor.TabIndex = 9;
            this.txtcfor.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // txtcurrency
            // 
            this.txtcurrency.Location = new System.Drawing.Point(111, 288);
            this.txtcurrency.MaxLength = 40;
            this.txtcurrency.Name = "txtcurrency";
            this.txtcurrency.Size = new System.Drawing.Size(126, 20);
            this.txtcurrency.TabIndex = 8;
            this.txtcurrency.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // txtpanno
            // 
            this.txtpanno.Location = new System.Drawing.Point(113, 48);
            this.txtpanno.MaxLength = 20;
            this.txtpanno.Name = "txtpanno";
            this.txtpanno.Size = new System.Drawing.Size(126, 20);
            this.txtpanno.TabIndex = 1;
            this.txtpanno.Enter += new System.EventHandler(this.txtcompname_Enter);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(18, 315);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Currency Format";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblacctext
            // 
            this.lblacctext.Location = new System.Drawing.Point(18, 261);
            this.lblacctext.Name = "lblacctext";
            this.lblacctext.Size = new System.Drawing.Size(87, 20);
            this.lblacctext.TabIndex = 9;
            this.lblacctext.Text = "Accounts Type";
            this.lblacctext.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblacctext.Visible = false;
            // 
            // lblcurrency
            // 
            this.lblcurrency.Location = new System.Drawing.Point(18, 288);
            this.lblcurrency.Name = "lblcurrency";
            this.lblcurrency.Size = new System.Drawing.Size(53, 20);
            this.lblcurrency.TabIndex = 9;
            this.lblcurrency.Text = "Currency";
            this.lblcurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblpanno
            // 
            this.lblpanno.Location = new System.Drawing.Point(18, 48);
            this.lblpanno.Name = "lblpanno";
            this.lblpanno.Size = new System.Drawing.Size(53, 20);
            this.lblpanno.TabIndex = 9;
            this.lblpanno.Text = "PAN No :";
            this.lblpanno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbltinno
            // 
            this.lbltinno.Location = new System.Drawing.Point(18, 16);
            this.lbltinno.Name = "lbltinno";
            this.lbltinno.Size = new System.Drawing.Size(53, 20);
            this.lbltinno.TabIndex = 9;
            this.lbltinno.Text = "TIN No :";
            this.lbltinno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpcl
            // 
            this.dtpcl.CustomFormat = "dd/MM/yyyy";
            this.dtpcl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpcl.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpcl.Location = new System.Drawing.Point(320, 8);
            this.dtpcl.Name = "dtpcl";
            this.dtpcl.Size = new System.Drawing.Size(113, 20);
            this.dtpcl.TabIndex = 21;
            this.dtpcl.Validating += new System.ComponentModel.CancelEventHandler(this.dtpOP_Validating);
            // 
            // dtpOP
            // 
            this.dtpOP.CustomFormat = "dd/MM/yyyy";
            this.dtpOP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpOP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpOP.Location = new System.Drawing.Point(102, 8);
            this.dtpOP.Name = "dtpOP";
            this.dtpOP.Size = new System.Drawing.Size(114, 20);
            this.dtpOP.TabIndex = 20;
            this.dtpOP.Validating += new System.ComponentModel.CancelEventHandler(this.dtpOP_Validating);
            // 
            // lblcldate
            // 
            this.lblcldate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcldate.Location = new System.Drawing.Point(222, 8);
            this.lblcldate.Name = "lblcldate";
            this.lblcldate.Size = new System.Drawing.Size(95, 20);
            this.lblcldate.TabIndex = 9;
            this.lblcldate.Text = "Closing Date :";
            this.lblcldate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblopdate
            // 
            this.lblopdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblopdate.Location = new System.Drawing.Point(11, 8);
            this.lblopdate.Name = "lblopdate";
            this.lblopdate.Size = new System.Drawing.Size(158, 20);
            this.lblopdate.TabIndex = 9;
            this.lblopdate.Text = "Opening Date :";
            this.lblopdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(4, 395);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(444, 15);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.CausesValidation = false;
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnnext);
            this.panel1.Controls.Add(this.btnok);
            this.panel1.Location = new System.Drawing.Point(4, 582);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(443, 38);
            this.panel1.TabIndex = 22;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnCancel.CausesValidation = false;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(260, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 26);
            this.btnCancel.TabIndex = 24;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnCancel.Enter += new System.EventHandler(this.btnCancel_Enter);
            // 
            // btnnext
            // 
            this.btnnext.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnnext.Location = new System.Drawing.Point(98, 6);
            this.btnnext.Name = "btnnext";
            this.btnnext.Size = new System.Drawing.Size(75, 26);
            this.btnnext.TabIndex = 23;
            this.btnnext.Text = "Next";
            this.btnnext.UseVisualStyleBackColor = false;
            this.btnnext.Click += new System.EventHandler(this.btnnext_Click);
            // 
            // btnok
            // 
            this.btnok.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnok.Location = new System.Drawing.Point(179, 6);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 26);
            this.btnok.TabIndex = 23;
            this.btnok.Text = "OK";
            this.btnok.UseVisualStyleBackColor = false;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel2.Controls.Add(this.dtpOP);
            this.panel2.Controls.Add(this.lblopdate);
            this.panel2.Controls.Add(this.lblcldate);
            this.panel2.Controls.Add(this.dtpcl);
            this.panel2.Location = new System.Drawing.Point(4, 356);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(444, 37);
            this.panel2.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Browse);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(4, 415);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(444, 161);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            // 
            // Browse
            // 
            this.Browse.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Browse.ForeColor = System.Drawing.Color.Black;
            this.Browse.Location = new System.Drawing.Point(288, 134);
            this.Browse.Name = "Browse";
            this.Browse.Size = new System.Drawing.Size(155, 23);
            this.Browse.TabIndex = 122;
            this.Browse.Text = "Browse...";
            this.Browse.UseVisualStyleBackColor = true;
            this.Browse.Click += new System.EventHandler(this.Browse_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(443, 127);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 121;
            this.pictureBox1.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // txtAbbr
            // 
            this.txtAbbr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAbbr.Location = new System.Drawing.Point(102, 61);
            this.txtAbbr.MaxLength = 3;
            this.txtAbbr.Name = "txtAbbr";
            this.txtAbbr.Size = new System.Drawing.Size(78, 20);
            this.txtAbbr.TabIndex = 4;
            this.txtAbbr.Enter += new System.EventHandler(this.txtcompname_Enter);
            this.txtAbbr.Leave += new System.EventHandler(this.txtcompname_TextChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(9, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Abbrevation";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmcompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(452, 620);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.pnlhistory);
            this.Controls.Add(this.pnltin);
            this.Controls.Add(this.pnlselection);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "frmcompany";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company";
            this.Load += new System.EventHandler(this.frmcompany_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmcompany_KeyDown);
            this.pnlselection.ResumeLayout(false);
            this.pnlhistory.ResumeLayout(false);
            this.pnlhistory.PerformLayout();
            this.pnltin.ResumeLayout(false);
            this.pnltin.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlselection;
        internal System.Windows.Forms.ComboBox lstSource;
        internal System.Windows.Forms.ComboBox cmbcreation1;
        internal System.Windows.Forms.Label lblcopy;
        internal System.Windows.Forms.Label lblcompcre;
        private System.Windows.Forms.Panel pnlhistory;
        private System.Windows.Forms.TextBox txtcompname;
        internal System.Windows.Forms.Label lblcompname;
        private System.Windows.Forms.TextBox txtcomptitle;
        internal System.Windows.Forms.Label lblcomptitle;
        private System.Windows.Forms.TextBox txtphone2;
        private System.Windows.Forms.TextBox txtphone1;
        private System.Windows.Forms.TextBox txtadd4;
        private System.Windows.Forms.TextBox txtadd3;
        private System.Windows.Forms.TextBox txtadd2;
        private System.Windows.Forms.TextBox txtadd1;
        internal System.Windows.Forms.Label lblphone2;
        internal System.Windows.Forms.Label lblphone1;
        internal System.Windows.Forms.Label lbladdress1;
        private System.Windows.Forms.TextBox txttinno;
        internal System.Windows.Forms.Label lblemail;
        private System.Windows.Forms.Panel pnltin;
        internal System.Windows.Forms.Label lbltinno;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.TextBox txtcstno;
        internal System.Windows.Forms.Label lblcstno;
        private System.Windows.Forms.TextBox txtpanno;
        internal System.Windows.Forms.Label lblopdate;
        internal System.Windows.Forms.Label lblpanno;
        private System.Windows.Forms.TextBox txtjur;
        internal System.Windows.Forms.Label lbljuridification;
        private System.Windows.Forms.DateTimePicker dtpcl;
        private System.Windows.Forms.DateTimePicker dtpOP;
        private System.Windows.Forms.TextBox txtcurrency;
        internal System.Windows.Forms.Label lblcldate;
        internal System.Windows.Forms.Label lblcurrency;
        private System.Windows.Forms.TextBox txtcfor;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbacctype;
        internal System.Windows.Forms.Label lblacctext;
        public System.Windows.Forms.TextBox txtFCode;
        public System.Windows.Forms.Label lblfirmcode;
        public System.Windows.Forms.TextBox txtcompcode;
        public System.Windows.Forms.Label lblcompcode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnnext;
        public System.Windows.Forms.Button btnok;
        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Browse;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtAbbr;
        internal System.Windows.Forms.Label label2;
    }
}