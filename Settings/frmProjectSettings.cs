using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Xpire.Classes;

namespace Xpire.Settings
{
    public partial class frmProjectSettings : Form
    {
        public frmProjectSettings()
        {
            InitializeComponent();
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (txtserverusername.Text.Trim() == "")
            {
                MessageBox.Show("Please enter server user name");
                txtserverusername.Focus();
                return;
            }

            else if (txtserverpswd.Text.Trim() == "")
            {
                MessageBox.Show("Please enter server password");
                txtserverpswd.Focus();
                return;
            }
            else if(txtprojectcode.Text.Trim()=="")
            {
                MessageBox.Show("Please enter Project Code");
                txtprojectcode.Focus();
                return;
            }
            else if (txtprojectcode.Text.Trim() != cPublic.g_PrjCode )
            {
                MessageBox.Show(" Project Code is not valid !");
                txtprojectcode.Focus();
                return;
            }
            else if (textBox1.Text.Trim() != "246810")
            {
                MessageBox.Show(" Please enter a valid password !");
                textBox1 .Focus();
                return;
            }
            cPublic.password = txtserverpswd.Text.Trim();
            cPublic.g_PrjCode = txtprojectcode.Text.Trim();
            cPublic.Sqluserid = txtserverusername.Text.Trim(); 
            cPublic.form = true;      
            this.Close();
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to close ? ","Biz_Maxx", MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString() == "Yes")
            {
                this.Close();
            }
        }
      
        private void frmproject_settings_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void frmProjectSettings_Load(object sender, EventArgs e)
        {
            txtprojectcode.Text = cPublic.g_PrjCode;
        }

           
    }
}