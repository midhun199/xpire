namespace Xpire.Settings
{
    partial class frmFirmSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFirmSelection));
            this.btnok = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.cfgcompany = new C1.Win.C1FlexGrid.C1FlexGrid();
            ((System.ComponentModel.ISupportInitialize)(this.cfgcompany)).BeginInit();
            this.SuspendLayout();
            // 
            // btnok
            // 
            this.btnok.BackColor = System.Drawing.Color.DarkGray;
            this.btnok.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.ForeColor = System.Drawing.Color.Black;
            this.btnok.Location = new System.Drawing.Point(116, 168);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(71, 24);
            this.btnok.TabIndex = 7;
            this.btnok.Text = "&OK";
            this.btnok.UseVisualStyleBackColor = false;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // btncancel
            // 
            this.btncancel.BackColor = System.Drawing.Color.DarkGray;
            this.btncancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btncancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancel.ForeColor = System.Drawing.Color.Black;
            this.btncancel.Location = new System.Drawing.Point(184, 168);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(71, 24);
            this.btncancel.TabIndex = 6;
            this.btncancel.Text = "&Cancel";
            this.btncancel.UseVisualStyleBackColor = false;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // cfgcompany
            // 
            this.cfgcompany.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.cfgcompany.BackColor = System.Drawing.SystemColors.Window;
            this.cfgcompany.ColumnInfo = "2,0,0,0,0,80,Columns:0{Width:400;}\t";
            this.cfgcompany.KeyActionEnter = C1.Win.C1FlexGrid.KeyActionEnum.None;
            this.cfgcompany.Location = new System.Drawing.Point(1, 2);
            this.cfgcompany.Name = "cfgcompany";
            this.cfgcompany.Rows.MinSize = 20;
            this.cfgcompany.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.cfgcompany.Size = new System.Drawing.Size(378, 169);
            this.cfgcompany.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("cfgcompany.Styles"));
            this.cfgcompany.TabIndex = 5;
            this.cfgcompany.DoubleClick += new System.EventHandler(this.cfgcompany_DoubleClick);
            this.cfgcompany.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmFirmSelection_KeyDown);
            // 
            // frmFirmSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(379, 198);
            this.ControlBox = false;
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.cfgcompany);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFirmSelection";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company Selection";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmFirmSelection_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmFirmSelection_FormClosed);
            this.Load += new System.EventHandler(this.frmFirmSelection_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmFirmSelection_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.cfgcompany)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Button btncancel;
        public C1.Win.C1FlexGrid.C1FlexGrid cfgcompany;



    }
}