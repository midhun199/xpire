using System.Windows.Forms;
namespace Xpire.Settings
{
    partial class frmlookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmlookup));
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtcond = new TextBox();
            this.cmbFields = new ComboBox();
            this.cfglookup = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.panel1.SuspendLayout();
         
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.panel1.Controls.Add(this.txtcond);
            this.panel1.Controls.Add(this.cmbFields);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(482, 53);
            this.panel1.TabIndex = 0;
            // 
            // txtcond
            // 
            this.txtcond.Location = new System.Drawing.Point(178, 17);
            this.txtcond.Name = "txtcond";
            this.txtcond.Size = new System.Drawing.Size(292, 20);
            this.txtcond.TabIndex = 2;
            this.txtcond.TextChanged += new System.EventHandler(this.txtcond_TextChanged);
            this.txtcond.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtcond_KeyDown);
            // 
            // cmbFields
            // 
            this.cmbFields.Location = new System.Drawing.Point(4, 17);
            this.cmbFields.Name = "cmbFields";
            //this.cmbFields.Properties.Buttons.AddRange(new Controls.EditorButton[] {
            //new Controls.EditorButton(Controls.ButtonPredefines.Combo)});
            this.cmbFields.Size = new System.Drawing.Size(168, 20);
            this.cmbFields.TabIndex = 1;
            this.cmbFields.TabStop = false;
            this.cmbFields.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // cfglookup
            // 
            this.cfglookup.AllowEditing = false;
            this.cfglookup.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.cfglookup.BackColor = System.Drawing.SystemColors.Window;
            //this.cfglookup.ColumnInfo = "10,0,0,0,0,90,Columns:";
            this.cfglookup.ExtendLastCol = true;
            this.cfglookup.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.cfglookup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cfglookup.Location = new System.Drawing.Point(0, 54);
            this.cfglookup.Name = "cfglookup";
            this.cfglookup.Rows.MinSize = 22;
            this.cfglookup.ScrollTips = true;
            this.cfglookup.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.cfglookup.Size = new System.Drawing.Size(482, 403);
            //this.cfglookup.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("cfglookup.Styles"));
            this.cfglookup.TabIndex = 1;
            this.cfglookup.BeforeSort += new C1.Win.C1FlexGrid.SortColEventHandler(this.cfglookup_BeforeSort);
            this.cfglookup.AfterScroll += new C1.Win.C1FlexGrid.RangeEventHandler(this.cfglookup_AfterScroll);
            this.cfglookup.DoubleClick += new System.EventHandler(this.cfglookup_DoubleClick);
            this.cfglookup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cfglookup_KeyDown);
            this.cfglookup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cfglookup_KeyPress);
            // 
            // frmlookup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 467);
            this.Controls.Add(this.cfglookup);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmlookup";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmlookup_KeyDown);
            this.panel1.ResumeLayout(false);
          

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private C1.Win.C1FlexGrid.C1FlexGrid cfglookup;
        private TextBox txtcond;
        private ComboBox cmbFields;

    }
}

