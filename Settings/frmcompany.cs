using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using System.Diagnostics;
using System.Net;

using System.IO;
using Xpire.Classes;

namespace Xpire.Settings
{
    public partial class frmcompany : Form
    {
        public frmcompany()
        {
            InitializeComponent();
        
        }
        
        fssgen.fssgen see = new fssgen.fssgen();
        cConnection cen = new cConnection();
        cGeneral gen = new cGeneral();
        CSP trig = new CSP();
        CAddFirm add = new CAddFirm();
        string currentcompcode = cPublic.g_compcode;
        ArrayList code = new ArrayList();
        bool check;
        bool created;
        bool cancel;

        private void frmcompany_Load(object sender, EventArgs e)
        {
            cPublic.company = this;
            cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
            this.CenterToScreen();
            if (this.Tag.ToString() == "EC")
            {
                cmbacctype.Enabled = false;
                cmbcreation1.Enabled = false;
                txtFCode.Enabled = false;
                btnok.Text = "&Update";
                btnnext.Visible = false;
            }
            else if (this.Tag.ToString() == "DC")
            {
                gen.EDControls(this, false);
                btnok.Enabled = true;
                btnCancel.Enabled = true;
                gen.edcolors(this, Color.White);
                cmbacctype.Enabled = false;
                cmbcreation1.Enabled = false;
                txtFCode.Enabled = false;
                btnok.Text = "&Delete";
                btnnext.Visible = false;
            }
            else//company entry
            {
                cmbcreation1.SelectedIndex = 0;
                cmbacctype.SelectedIndex = 0;
                txtFCode.Text = "0001";
                txtcompcode.Text = (Convert.ToInt32(gen.getMaxNumber("Company", "code")) + 1).ToString("000");
                this.btnok.Visible = false;
                this.btnnext.Location = new Point(310, 9);

                if (cPublic.g_Rdate.Month >= 4)
                {
                    DateTime dt = new DateTime(DateTime.Now.Year, 04, 1);
                    dtpOP.Value = dt;
                    dt = new DateTime((DateTime.Now.Year + 1), 03, 31);
                    dtpcl.Value = dt;
                }
                else
                {

                    DateTime dt = new DateTime((DateTime.Now.Year - 1), 04, 1);
                    dtpOP.Value = dt;
                    dt = new DateTime((DateTime.Now.Year), 03, 31);
                    dtpcl.Value = dt;

                }
                btnok.Enabled = false;
            }
            cancel = false;
        }

        private void cmbcreation1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == cmbcreation1)
            {
                if (cmbcreation1.SelectedIndex == 1)
                {
                    loadcompany();
                    lstSource.Enabled = true;
                    lstSource.SelectedIndex = 0;
                    btnok.Visible = true;
                    btnnext.Visible = false;
                    btnok.Enabled = true;
                }
                else
                {
                    lstSource.Enabled = false;
                    lstSource.SelectedIndex = -1;
                    btnok.Visible = false;
                    btnnext.Visible = true;
                    btnok.Enabled = false;
                }
            }
        }

        private void loadcompany()
        {
            int i = 0;
            lstSource.Items.Clear();
            cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
            string sql = "select dispname,code from Company  ";
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {

                lstSource.Items.Add(dr.GetString(0));
                code.Add(dr.GetString(1));
                i++;
            }
            dr.Close();
        }

        private void dtpOP_Validating(object sender, CancelEventArgs e)
        {
            if (sender == dtpOP)
            {
                if (dtpOP.Value > dtpcl.Value)
                {
                    MessageBox.Show("Financial Year date is incorrect");
                    dtpOP.Focus();
                }
            }
            else
            {
                if (dtpcl.Value < dtpOP.Value)
                {
                    MessageBox.Show("Financial year date is incorrect");
                    dtpcl.Focus();
                }
            }
        }

        public void checks()
        {
            if (cancel != true)
            {
                if (!(gen.isFormClosing()))
                {
                    progressBar1.Value = 10;
                    if (txtcompname.Text.Trim() == "")
                    {
                        cPublic.company.Show();
                        MessageBox.Show("Company Name Should Not Be Blank", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cPublic.company.txtcompname.Focus();
                        progressBar1.Value = 0;
                        return;
                    }

                    if ((txtcompcode.Text.Trim() == "") || (txtcompcode.Text.Trim() == "000"))
                    {
                        cPublic.company.Show();
                        MessageBox.Show("Company Code Should Not Be Blank", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cPublic.company.txtcompcode.Focus();
                        progressBar1.Value = 0;
                        return;
                    }
                    if (txtcomptitle.Text.Trim() == "")
                    {
                        cPublic.company.Show();
                        MessageBox.Show("Company Title Should Not Be Blank", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cPublic.company.txtcomptitle.Focus();
                        progressBar1.Value = 0;
                        return;
                    }
                    if (txtAbbr.Text.Trim() == "")
                    {
                        cPublic.company.Show();
                        MessageBox.Show("Abbrevation Should Not Be Blank", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cPublic.company.txtAbbr.Focus();
                        progressBar1.Value = 0;
                        return;
                    }
                    if (pictureBox1.Image == null)
                    {
                        cPublic.company.Show();
                        MessageBox.Show("Select Picture", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cPublic.company.Browse.Focus();
                        progressBar1.Value = 0;
                        return;
                    }
                    progressBar1.Value = 20;
                    if (this.Tag.ToString() == "EC")//company editprovision
                    {
                        updatecomp();

                    }
                    else if (this.Tag.ToString() == "DC")//company delete
                    {
                        Delete();
                    }
                    else
                    {
                        if (cmbcreation1.SelectedIndex == 1)
                        {
                            saveexisting();
                        }
                        else
                        {
                            Save();
                        }
                    }

                }
            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            checks();
        }

        private void Delete()
        {

            try
            {
                if (cPublic.Connection.State == System.Data.ConnectionState.Open)
                    cPublic.Connection.Close();
                cPublic.Connection.Dispose();
                SqlConnection.ClearAllPools();
                SqlConnection.ClearPool(cPublic.Connection);
                cPublic.Connection = new SqlConnection();
                cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
                string sql2 = "drop database " + cPublic.g_PrjCode + txtcompcode.Text;
                SqlCommand comm2 = new SqlCommand(sql2, cPublic.Connection);
                comm2.ExecuteNonQuery();
                string sql1 = "delete from company where code=" + txtcompcode.Text.Trim();
                SqlCommand comm1 = new SqlCommand(sql1, cPublic.Connection);
                comm1.ExecuteNonQuery();
                MessageBox.Show("Company Successfully Deleted ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
                MessageBox.Show(this.txtcompname.Text + " is currently in use.");
                this.Close();
            }
        }
        private void updatecomp()
        {
            cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
            checking();
            if (check == false)
            {
                             
                SqlCommand comm = new SqlCommand("sp_update_company", cPublic.Connection);
                comm.CommandType = CommandType.StoredProcedure;
                string code = Convert.ToInt32(txtcompcode.Text.Trim()).ToString("000"), name = "";
                update(comm, code, txtcompname.Text.Trim());
               
                cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + txtcompcode.Text.Trim(), cPublic.Sqlservername);

                string sql = "select code,dispname from Firms where status='D'";
                comm = new SqlCommand(sql, cPublic.Connection);
                SqlDataReader dr = comm.ExecuteReader();
                if (dr.Read())
                {
                    code = dr.GetString(0);
                    name = dr["dispname"].ToString();
                }
                dr.Close();
              
                comm = new SqlCommand("sp_update_firms", cPublic.Connection);
                comm.CommandType = CommandType.StoredProcedure;
                update(comm, code, txtcompname.Text.Trim());

               

                MessageBox.Show("Company Successfully Edited ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + currentcompcode, cPublic.Sqlservername);
            this.Close();
        }

        public void update(SqlCommand comm, string code, string dispname)
        {
            comm.Parameters.AddWithValue("@code", code);
            comm.Parameters.AddWithValue("@name", txtcomptitle.Text.Trim());
            comm.Parameters.AddWithValue("@dispname", dispname);

            comm.Parameters.AddWithValue("@clddate", Convert.ToDateTime(dtpcl.Value.ToShortDateString()));
            comm.Parameters.AddWithValue("@opdate", Convert.ToDateTime(dtpOP.Value.ToShortDateString()));
            comm.Parameters.AddWithValue("@addr1", txtadd1.Text.Trim());
            comm.Parameters.AddWithValue("@addr2 ", txtadd2.Text.Trim());
            comm.Parameters.AddWithValue("@addr3", txtadd3.Text.Trim());
            comm.Parameters.AddWithValue("@addr4", txtadd4.Text.Trim());
            comm.Parameters.AddWithValue("@cstno", txtcstno.Text.Trim());
            comm.Parameters.AddWithValue("@cformat", txtcfor.Text.Trim());
            comm.Parameters.AddWithValue("@csymbol", txtcurrency.Text.Trim());
            comm.Parameters.AddWithValue("@email ", txtemail.Text.Trim());
            comm.Parameters.AddWithValue("@jurid", txtjur.Text.Trim());
            comm.Parameters.AddWithValue("@kgst", txttinno.Text.Trim());
            comm.Parameters.AddWithValue("@panno", txtpanno.Text.Trim());
            comm.Parameters.AddWithValue("@phone1", txtphone1.Text.Trim());
            comm.Parameters.AddWithValue("@phone2 ", txtphone2.Text.Trim());
            if (checkBox1.Checked == true && comm.CommandText.ToUpper().Contains("SP_UPDATE_FIRMS")==false )
            {
                comm.Parameters.AddWithValue("@default", "D");
            }
            comm.Parameters.AddWithValue("@path ", cPublic.drive);
            comm.ExecuteNonQuery();
        }

        private void checking()
        {
            string sql1 = "";
            progressBar1.Value = 25;
            check = false;
            cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
            if (this.Tag.ToString().Trim() == "EC")
            {
                sql1 = "select dispname from company where dispname='" + see.SQLFormat(txtcompname.Text.Trim()) + "'and code<>'" + (Convert.ToInt32(txtcompcode.Text.Trim())).ToString("000") + "' ";
            }
            else
            {
                sql1 = "select dispname from company where dispname='" + see.SQLFormat(txtcompname.Text.Trim()) + "'";
            }

            SqlCommand comm2 = new SqlCommand(sql1, cPublic.Connection);
            SqlDataReader dr = comm2.ExecuteReader();
            if (dr.Read())
            {
                cPublic.company.Show();
                MessageBox.Show("Company Name Already Exist", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                dr.Close();
                check = true;
                txtcompname.Focus();
                txtcompname.SelectAll();
                progressBar1.Value = 0;
                return;
            }
            dr.Close();

            progressBar1.Value = 0;
            if (this.Tag.ToString().Trim().Substring(0, 1) != "E")
            {
                string sql2 = "select code from company where code ='" + (Convert.ToInt32(txtcompcode.Text.Trim())).ToString("000") + "'";
                SqlCommand comm1 = new SqlCommand(sql2, cPublic.Connection);
                SqlDataReader dr1 = comm1.ExecuteReader();
                if (dr1.Read())
                {
                    cPublic.company.Show();
                    MessageBox.Show("Company Code Already Exist", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    check = true;
                    txtcompcode.Focus();
                    txtcompcode.SelectAll();
                    progressBar1.Value = 0;
                    dr1.Close();
                    return;

                }
                dr1.Close();
            }
        }

        private void creation()
        {
            progressBar1.Value = 40;
            string path = "";
            if (Environment.MachineName.ToUpper() == cPublic.Servermachinename.ToUpper())
            {
                path = cPublic.drive + "Maxx\\" + cPublic.g_PrjCode;
                gen.createpath(Convert.ToInt16(txtcompcode.Text.Trim()).ToString("000"));
            }
            else
            {
                try
                {
                    //NetworkCredential myCredentials = new NetworkCredential("mithun", "mithun199","FOURSQUARE");
                    //using (new NetworkConnection(@"\\server", myCredentials))
                    //using (new NetworkConnection(@"\\server", myCredentials))
                    //{
                        path = cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\Maxx\\" + cPublic.g_PrjCode + "\\" + cPublic.g_PrjCode + (Convert.ToInt16(txtcompcode.Text.Trim()).ToString("000"));
                        gen.CreateRemotePath(Convert.ToInt16(txtcompcode.Text.Trim()).ToString("000"));
                    //}
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    return;
                }
            }
            progressBar1.Value = 50;
            if (!add.CreateDB(cPublic.g_PrjCode + (Convert.ToInt16(txtcompcode.Text.Trim()).ToString("000")), path))
            {
                cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
                SqlCommand comm = new SqlCommand("sp_insert_company", cPublic.Connection);
                comm.CommandType = CommandType.StoredProcedure;

                comm.Parameters.AddWithValue("@code", Convert.ToInt32(txtcompcode.Text.Trim()).ToString("000"));
                comm.Parameters.AddWithValue("@name", txtcomptitle.Text.Trim());
                comm.Parameters.AddWithValue("@dispname", txtcompname.Text.Trim());
                comm.Parameters.AddWithValue("@clddate", Convert.ToDateTime(dtpcl.Value.ToShortDateString()));
                comm.Parameters.AddWithValue("@opdate", Convert.ToDateTime(dtpOP.Value.ToShortDateString()));
                comm.Parameters.AddWithValue("@addr1", txtadd1.Text.Trim());
                comm.Parameters.AddWithValue("@addr2 ", txtadd2.Text.Trim());
                comm.Parameters.AddWithValue("@addr3", txtadd3.Text.Trim());
                comm.Parameters.AddWithValue("@addr4", txtadd4.Text.Trim());
                comm.Parameters.AddWithValue("@cstno", txtcstno.Text.Trim());
                comm.Parameters.AddWithValue("@cformat", txtcfor.Text.Trim());
                comm.Parameters.AddWithValue("@csymbol", txtcurrency.Text.Trim());
                comm.Parameters.AddWithValue("@email ", txtemail.Text.Trim());
                comm.Parameters.AddWithValue("@jurid", txtjur.Text.Trim());
                comm.Parameters.AddWithValue("@kgst", txttinno.Text.Trim());
                comm.Parameters.AddWithValue("@panno", txtpanno.Text.Trim());
                comm.Parameters.AddWithValue("@phone1", txtphone1.Text.Trim());
                comm.Parameters.AddWithValue("@phone2 ", txtphone2.Text.Trim());
                comm.Parameters.AddWithValue("@path ", cPublic.drive);
                comm.Parameters.AddWithValue("@abbr ", txtAbbr.Text.Trim());

                MemoryStream ms = new MemoryStream();
                Bitmap bmpImage = new Bitmap(pictureBox1.Image);

                bmpImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                byte[] data = ms.GetBuffer();
                SqlParameter p = new SqlParameter("@Picture", SqlDbType.Image);
                p.Value = data;
                comm.Parameters.Add(p);

                comm.ExecuteNonQuery();
                created = true;

            }
            else
            {
                cPublic.company.Show();
                MessageBox.Show("Company Already Exist", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                created = false;
                progressBar1.Value = 0;
            }
        }

        private void Save()
        {
            checking();
            if (check == false)
            {
                creation();
                if (created == true)
                {
                    progressBar1.Value = 60;

                    cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + (Convert.ToInt16(txtcompcode.Text.Trim()).ToString("000")), cPublic.Sqlservername);
                    if (txtFCode.Text == "")
                    {
                        add.FirmAdd(cPublic.g_PrjCode + (Convert.ToInt16(txtcompcode.Text.Trim()).ToString("000")), "0001");
                        SqlCommand comm = new SqlCommand("sp_insert_firms", cPublic.Connection);
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@code", "0001");
                        comm.Parameters.AddWithValue("@Status", "D");
                        firminsert(comm);            
                    }
                    else
                    {
                        add.FirmAdd(cPublic.g_PrjCode + (Convert.ToInt16(txtcompcode.Text.Trim()).ToString("000")), (Convert.ToInt16(txtFCode.Text.Trim()).ToString("0000")));
                        SqlCommand comm = new SqlCommand("sp_insert_firms", cPublic.Connection);
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@code", Convert.ToInt32(txtFCode.Text.Trim()).ToString("0000"));
                        comm.Parameters.AddWithValue("@Status", "D");
                        firminsert(comm);    
                    }
                    progressBar1.Value = 100;                  
                    MessageBox.Show("New Company Created Successfully", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);

                }
                this.Close();
            }

        }
        public void firminsert(SqlCommand comm)
        {
            comm.Parameters.AddWithValue("@name", txtcomptitle.Text.Trim());
            comm.Parameters.AddWithValue("@dispname", txtcompname.Text.Trim());
            comm.Parameters.AddWithValue("@clddate", Convert.ToDateTime(dtpcl.Value.ToShortDateString()));
            comm.Parameters.AddWithValue("@opdate", Convert.ToDateTime(dtpOP.Value.ToShortDateString()));
            comm.Parameters.AddWithValue("@addr1", txtadd1.Text.Trim());
            comm.Parameters.AddWithValue("@addr2 ", txtadd2.Text.Trim());
            comm.Parameters.AddWithValue("@addr3", txtadd3.Text.Trim());
            comm.Parameters.AddWithValue("@addr4", txtadd4.Text.Trim());
            comm.Parameters.AddWithValue("@cstno", txtcstno.Text.Trim());
            comm.Parameters.AddWithValue("@cformat", txtcfor.Text.Trim());
            comm.Parameters.AddWithValue("@csymbol", txtcurrency.Text.Trim());
            comm.Parameters.AddWithValue("@email ", txtemail.Text.Trim());
            comm.Parameters.AddWithValue("@jurid", txtjur.Text.Trim());
            comm.Parameters.AddWithValue("@kgst", txttinno.Text.Trim());
            comm.Parameters.AddWithValue("@panno", txtpanno.Text.Trim());
            comm.Parameters.AddWithValue("@phone1", txtphone1.Text.Trim());
            comm.Parameters.AddWithValue("@phone2 ", txtphone2.Text.Trim());

            comm.ExecuteNonQuery();
        }
        private void saveexisting()
        {
            checking();
            if (check == false)
            {
                creation();
                if (created == true)
                {
                    progressBar1.Value = 60;

                    cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + code[lstSource.SelectedIndex], cPublic.Sqlservername);
                    string sql = "select name from sysobjects where (xtype='U') and name <>'dtproperties' and name not like '%TEMP%'  ";
                    SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    sql = "select code from firms";
                    SqlDataAdapter dr = new SqlDataAdapter(sql, cPublic.Connection);
                    DataSet fg = new DataSet();
                    dr.Fill(fg);
                    for (int f = 0; f <= ds.Tables[0].Rows.Count - 1; f++)
                    {
                        try
                        {
                            string sqlinsert = "select * into " + cPublic.g_PrjCode + (Convert.ToInt16(txtcompcode.Text.Trim()).ToString("000")) + ".dbo." + ds.Tables[0].Rows[f]["name"] + " from " + cPublic.g_PrjCode + code[lstSource.SelectedIndex] + ".DBO." + ds.Tables[0].Rows[f]["name"];//+ " where 1=1";
                            SqlCommand comms = new SqlCommand(sqlinsert, cPublic.Connection);
                            comms.ExecuteNonQuery();

                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.ToString());
                        }
                    }

                    cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + (Convert.ToInt16(txtcompcode.Text.Trim()).ToString("000")), cPublic.Sqlservername);
                    for (int i = 0; i < fg.Tables[0].Rows.Count; i++)
                    {
                        trig.Triggers(fg.Tables[0].Rows[i]["code"].ToString());
                        trig.sPS(fg.Tables[0].Rows[i]["code"].ToString());
                        trig.OtherSPs(fg.Tables[0].Rows[i]["code"].ToString());
                        trig.firmsp();
                    }
                    progressBar1.Value = 100;
                    sql = "update firms set status='D', name='" + txtcomptitle.Text.Trim() + "' ,dispname='" + txtcompname.Text.Trim() + "'  where code= '" + (Convert.ToInt16(txtFCode.Text.Trim())).ToString("0000") + "'";
                    SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
                    comm.ExecuteNonQuery();
                    MessageBox.Show("New Company Created Successfully", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + currentcompcode, cPublic.Sqlservername);

                }
                this.Close();

            }
        }

        private void cmbacctype_Validating(object sender, CancelEventArgs e)
        {
            if (cmbacctype.Text == "")
            {
                MessageBox.Show("Please Select an Account Type", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Sure to Close?", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question).Equals(DialogResult.Yes))
            {
                cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + currentcompcode, cPublic.Sqlservername);
                this.Close();

            }

        }


        private void btnCancel_Enter(object sender, EventArgs e)
        {
            cancel = true;
        }

        private void txtFCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == 46))
            {
                e.Handled = true;
            }
            TextBox tb = (TextBox)sender;
            see.ValidDecimalNumber(e, ref tb, 4, 0);
        }

        private void frmcompany_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{tab}");
            }
        }

        private void txtcompname_Enter(object sender, EventArgs e)
        {
            if (!(((TextBox)sender).Text == ""))
            {
                ((TextBox)sender).SelectAll();
            }
        }

        private void cmbcreation1_KeyDown(object sender, KeyEventArgs e)
        {
            if (!(e.KeyValue == 38 || e.KeyValue == 40))
            {
                e.SuppressKeyPress = true;
            }
        }

        public void load(string compcode)
        {
            cen.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
            string sql = "select * from company where code=" + compcode;
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            SqlDataReader dr = comm.ExecuteReader();
            if (dr.Read())
            {
                dtpcl.Value = Convert.ToDateTime(dr.IsDBNull(0) ? "" : dr.GetValue(0));
                txtadd1.Text = Convert.ToString(dr.IsDBNull(1) ? "" : dr.GetValue(1));
                txtadd2.Text = Convert.ToString(dr.IsDBNull(2) ? "" : dr.GetValue(2));
                txtadd3.Text = Convert.ToString(dr.IsDBNull(3) ? "" : dr.GetValue(3));
                txtadd4.Text = Convert.ToString(dr.IsDBNull(4) ? "" : dr.GetValue(4));
                txtcompcode.Text = Convert.ToString(dr.IsDBNull(5) ? "" : dr.GetValue(5));
                txtcomptitle.Text = Convert.ToString(dr.IsDBNull(6) ? "" : dr.GetValue(6));
                txtcompname.Text = Convert.ToString(dr.IsDBNull(7) ? "" : dr.GetValue(7));
                txtAbbr.Text = Convert.ToString(dr.IsDBNull(8) ? "" : dr.GetValue(8));
                txtcstno.Text = Convert.ToString(dr.IsDBNull(9) ? "" : dr.GetValue(9));
                txtcfor.Text = Convert.ToString(dr.IsDBNull(10) ? "" : dr.GetValue(10));
                txtcurrency.Text = Convert.ToString(dr.IsDBNull(11) ? "" : dr.GetValue(11));
                txtemail.Text = Convert.ToString(dr.IsDBNull(12) ? "" : dr.GetValue(12));
                txtjur.Text = Convert.ToString(dr.IsDBNull(13) ? "" : dr.GetValue(13));
                txttinno.Text = Convert.ToString(dr.IsDBNull(14) ? "" : dr.GetValue(14));
                dtpOP.Value = Convert.ToDateTime(dr.IsDBNull(15) ? "" : dr.GetValue(15));
                txtpanno.Text = Convert.ToString(dr.IsDBNull(16) ? "" : dr.GetValue(16));
                txtphone1.Text = Convert.ToString(dr.IsDBNull(17) ? "" : dr.GetValue(17));
                txtphone2.Text = Convert.ToString(dr.IsDBNull(18) ? "" : dr.GetValue(18));

                object theValue = dr["Picture"];

                if (DBNull.Value != theValue)
                {
                    byte[] data = (byte[])dr["Picture"];
                    MemoryStream ms = new MemoryStream(data);
                    pictureBox1.Image = Image.FromStream(ms);
                }
                else
                    pictureBox1.Image = null;

                if (dr.GetValue(19).ToString() == "SINGLE")
                {
                    cmbacctype.SelectedIndex = 0;
                }
                else
                {
                    cmbacctype.SelectedIndex = 1;
                }

            }
            dr.Close();

        }

        private void txtcompname_TextChanged(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text != "")
            {
                ((TextBox)sender).Text = ((TextBox)sender).Text.Trim().ToUpper();
            }
        }

        private void btnnext_Click(object sender, EventArgs e)
        {

            if (txtcompname.Text.Trim() == "")
            {
                cPublic.company.Show();
                MessageBox.Show("Company Name Should Not Be Blank", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cPublic.company.txtcompname.Focus();
                progressBar1.Value = 0;
                return;
            }

            if ((txtcompcode.Text.Trim() == "") || (txtcompcode.Text.Trim() == "000"))
            {
                cPublic.company.Show();
                MessageBox.Show("Company Code Should Not Be Blank", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cPublic.company.txtcompcode.Focus();
                progressBar1.Value = 0;
                return;
            }
            if (txtcomptitle.Text.Trim() == "")
            {
                cPublic.company.Show();
                MessageBox.Show("Company Title Should Not Be Blank", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cPublic.company.txtcomptitle.Focus();
                progressBar1.Value = 0;
                return;
            }
            if (txtAbbr.Text.Trim() == "")
            {
                cPublic.company.Show();
                MessageBox.Show("Abbrevation Should Not Be Blank", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cPublic.company.txtAbbr.Focus();
                progressBar1.Value = 0;
                return;
            }
            if (pictureBox1.Image==null)
            {
                cPublic.company.Show();
                MessageBox.Show("Select Picture", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cPublic.company.Browse.Focus();
                progressBar1.Value = 0;
                return;
            }

            checks();
        }

        private void lstSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtFCode.Enabled = false;
        }

        private void Browse_Click(object sender, EventArgs e)
        {
            var _with1 = openFileDialog1;

            _with1.Filter = ("Images |*.png; *.bmp; *.jpg;*.jpeg; *.gif; *.ico");
            _with1.FilterIndex = 4;

            //Clear the file name
            openFileDialog1.FileName = "";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
            }
        }
    }
}