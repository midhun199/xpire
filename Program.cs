

using SplashScreen;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.DoEvents();
            //Application.Run(new Form1());
            cConnection  con = new cConnection();
            cGeneral gen = new cGeneral();

            cPublic.PrintMode = "DOS";
            cPublic.AutoPriceUpdation = "No";

            if (!gen.NetExist())
            {
                Application.Run(new frmProjectSettings());
                if (cPublic.form == true)
                {
                    Application.Run(new frmServerSetting());

                    if (gen.ServerDet())
                    {
                        if (con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername))
                        {
                            if (!cPublic.closing)
                            {

                                LoadSplash();
                            }
                            if (!cPublic.closing)
                                Application.Run(new FrmLogin());
                            if (!cPublic.closing && gen.GetUserCompcount())
                                Application.Run(new frmFirmSelection());
                            if (!cPublic.closing)
                                Application.Run(new Xpire.Forms.frmMainNew());
                        }
                        else
                        {
                            MessageBox.Show("Database Connection Failed ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            Application.Run(new frmServerSetting());
                        }
                    }
                }
            }
            else
            {
                if (gen.ServerDet())
                {
                    if (con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername))
                    {
                        LoadSplash();

                        if (!cPublic.closing)
                        {
                            System.Threading.Thread.Sleep(1000);
                            Application.Run(new FrmLogin());
                        }
                        if (cPublic.Login == true)
                        {

                            if (!cPublic.closing && gen.GetUserCompcount())
                                Application.Run(new frmFirmSelection());
                            if (!cPublic.closing)
                                //Application.Run(new Form1());
                                Application.Run(new Xpire.Forms.frmMainNew());
                        }
                        else
                        {
                            Application.Exit();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Database Connection Failed ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        Application.Run(new frmServerSetting());
                    }
                }
            }
        }

        private static void LoadSplash()
        {
            SplashScreen.frmsplash.ShowSplashScreen();
            Application.DoEvents();
            SplashScreen.frmsplash.SetStatus("Initializing : Initializing Tools... ");
            System.Threading.Thread.Sleep(1000);
            SplashScreen.frmsplash.SetStatus("Loading Environment...");
            System.Threading.Thread.Sleep(1000);
            SplashScreen.frmsplash.SetStatus("Validating Administrator.....");
            System.Threading.Thread.Sleep(1000);
            SplashScreen.frmsplash.SetStatus("Building Workspace...");
            System.Threading.Thread.Sleep(1000);
            SplashScreen.frmsplash.SetStatus("Initializing : Refreshing Registry.....");
            System.Threading.Thread.Sleep(1000);
            SplashScreen.frmsplash.SetStatus("Permission allowed for user.....");
            System.Threading.Thread.Sleep(1000);
            SplashScreen.frmsplash.SetStatus("Loading module 1");
            System.Threading.Thread.Sleep(50);
            SplashScreen.frmsplash.SetStatus("Loading module 2");
            System.Threading.Thread.Sleep(240);
            SplashScreen.frmsplash.SetStatus("Loading module 3");
            System.Threading.Thread.Sleep(240);
            SplashScreen.frmsplash.SetStatus("Loading module 4");
            System.Threading.Thread.Sleep(240);
            SplashScreen.frmsplash.SetStatus("Loading module 5");
            System.Threading.Thread.Sleep(240);
            SplashScreen.frmsplash.SetStatus(" Starting the Application.....");
            System.Threading.Thread.Sleep(1000);
            SplashScreen.frmsplash.CloseForm();
        }
    }

}