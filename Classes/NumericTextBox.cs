using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using System.Globalization;


namespace Xpire.Classes
{
    internal  class NumericTextBox : TextBox
    {
        const int WM_PASTE = 0x0302;

        private int defaultValue;
        public int DefaultValue
        {
            get { return defaultValue; }
            set
            {
                defaultValue = value;
                this.Text = defaultValue.ToString();
            }
        }

        private int numericLength;
        public int NumericLength
        {
            get { return numericLength; }
            set
            {
                if (value < defaultValue.ToString().Length)
                    numericLength = defaultValue.ToString().Length;
                else
                    numericLength = value;
            }
        }

        private int decimalLength;
        public int DecimalLength
        {
            get { return decimalLength; }
            set { decimalLength = value; }
        }

        private NumberFormatInfo numberFormatInfo
        {
            get { return System.Globalization.CultureInfo.CurrentCulture.NumberFormat; }
        }
        private string decimalSeparator
        {
            get { return numberFormatInfo.NumberDecimalSeparator; }
        }

        public NumericTextBox()
            : base()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
         
            //this.Properties.TextAlign = HorizontalAlignment.Right;
            this.Text = defaultValue.ToString();
            this.ResumeLayout(false);
        }


        bool isFormClosing()
        {
            StackTrace sTrace = new StackTrace();
            for (byte i = 2; i <= sTrace.FrameCount - 1; i = (byte)(i + 1))
            {
                if (sTrace.GetFrame(i).GetMethod().Name == "WmClose")
                { return true; }
            }
            return false;
        }

        bool validateText(string text)
        {
            string intPart = string.Empty;
            string decPart = string.Empty;

            int decimalIndex = text.Trim().IndexOf(decimalSeparator);

            if (this.DecimalLength == 0 && decimalIndex >= 0) { return false; }

            if (decimalIndex >= 0 && decimalIndex != text.LastIndexOf(decimalSeparator))
            { return false; }

            if (decimalIndex >= 0) { intPart = text.Trim().Substring(0, decimalIndex); }
            else { intPart = text.Trim(); }

            if (decimalIndex >= 0 && (decimalIndex + 1) < text.Length)
            { decPart = text.Trim().Substring(decimalIndex + 1, (text.Trim().Length - (decimalIndex + 1))); }

            if (this.DecimalLength > 0)
            {
                if (intPart.Length > this.numericLength || decPart.Length > this.DecimalLength) { return false; }
            }
            else if (intPart.Length > this.numericLength) { return false; }
            return true;
        }

        bool checkText(string thisText, string clipText)
        {
            char[] data = clipText.ToCharArray();
            foreach (char c in data)
            {
                if (!char.IsDigit(c))
                { return false; }
            }

            if (!validateText(thisText + clipText))
            { return false; }
            return true;
        }

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            if (this.Text.Trim() == string.Empty)
            { this.Text = DefaultValue.ToString(); }
            this.SelectAll();
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            string keyInput = e.KeyChar.ToString();

            if (this.SelectionLength == this.Text.Trim().Length)
            { this.Text = string.Empty; }

            if (e.KeyChar.Equals('-') == true && this.Text.Contains("-") == false && this.SelectionStart == 0) { }
            else
            {
                this.SelectionStart = this.Text.Length;

                if (Char.IsDigit(e.KeyChar) && validateText(this.Text.Trim() + e.KeyChar)) { }
                else if (keyInput == decimalSeparator && validateText(this.Text.Trim() + e.KeyChar)) { }
                else if (e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Enter) { }
                else { e.Handled = true; }
            }
        }

        protected override void OnValidating(System.ComponentModel.CancelEventArgs e)
        {
            if (isFormClosing()) { return; }

            if (this.Text.Trim() == string.Empty)
            { this.Text = DefaultValue.ToString(); }

            if (!validateText(this.Text.Trim()))
            { e.Cancel = true; this.SelectAll(); return; }
            e.Cancel = false;

            base.OnValidating(e);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_PASTE)
            {
                IDataObject iData = Clipboard.GetDataObject();
                string clipData = iData.GetData(DataFormats.Text).ToString();
                if (!checkText(this.Text.Trim(), clipData)) { return; }
                this.SelectionStart = this.Text.Trim().Length;
            }
            base.WndProc(ref m);
        }
    }
}
