using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Collections;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Drawing;
using Xpire.Settings;

namespace Xpire.Classes
{

    public static class cPublic
    {
        private static Boolean Login1 = true;

        private static DateTime logindate ;
        private static string messagename1 = "Biz-Max 1.1.0.1";
        private static string g_TinNo1 = "";
        private static string g_CstNo1 = "";
        private static string g_firmcode1 = "";
        private static string _firmname = "";
        private static string g_compcode1 = "";
        private static string g_Userdetails1 = "";
        private static string g_UserId1 = "";
        private static string g_UserLevel1 = "";
        private static Boolean form1 = false;

        private static SqlConnection Comm_con1 = new SqlConnection();
        private static string password1 = "server";
        private static string drive1 = "";
        private static string g_PrjCode1 = "MAXX";
        private static Int32 g_NoFirms1 = 1;
        public static ArrayList g_FirmArray = new ArrayList();
        public static DateTime dt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
        private static DateTime g_Rdate1 = dt;
        private static DateTime g_OpDate1 = DateTime.Today;
        private static DateTime g_ClDate1 = DateTime.Today;
        private static string g_CompName1 = "";
        private static string g_DispName1 = "";
        private static string g_Add11 = "";
        private static string g_Add21 = "";
        private static string g_Add31 = "";
        private static string g_Add41 = "";
        private static string g_Phone11 = "";
        private static string g_Phone21 = "";

        public static frmcompany company = new frmcompany();
        public static double GrpNos = 0;
        public static double AccNos = 0;

        public static string[,] AccArray = new string[5000, 6];
        public static double g_opbalance;
        public static double Gpno;
        public static double G_TotBalance;

        private static string g_rpthead1 = "";
        private static DateTime g_from1 = DateTime.Today;
        private static DateTime g_to1 = DateTime.Today;

        public static System.Windows.Forms.MenuStrip MainMenu = null;

        private static string BchDW1 = Strings.Chr(27).ToString() + Strings.Chr(87).ToString() + Strings.Chr(1).ToString();
        private static string BchNDW1 = Strings.Chr(27).ToString() + Strings.Chr(87).ToString() + Strings.Chr(0).ToString();
        private static string ChC1 = Strings.Chr(27).ToString() + Strings.Chr(15).ToString();
        private static string ChE1 = Strings.Chr(27).ToString() + Strings.Chr(69).ToString();
        private static string ChNE1 = Strings.Chr(27).ToString() + Strings.Chr(70).ToString();
        private static string Ch101 = Strings.Chr(27).ToString() + Strings.Chr(80).ToString();
        private static string ChN1 = Strings.Chr(18).ToString();
        private static string Ch121 = Strings.Chr(27).ToString() + Strings.Chr(77).ToString();
        private static string PageSkip1 = Strings.Chr(12).ToString();



        private static string _companyname = string.Empty;

        public static string CompanyName
        {
            get { return _companyname; }
            set { _companyname = value; }
        }


        private static bool closing1 = false;

        public static Boolean Login
        {
            get { return Login1; }
            set { Login1 = value; }
        }

        public static DateTime LoginDate
        {
            get { return logindate; }
            set { logindate = value; }
        }

        public static string g_DispName
        {
            get { return g_DispName1; }
            set { g_DispName1 = value; }
        }


        public static string g_CompName
        {
            get { return g_CompName1; }
            set { g_CompName1 = value; }
        }

        public static string g_add1
        {
            get { return g_Add11; }
            set { g_Add11 = value; }
        }

        public static string g_add2
        {
            get { return g_Add21; }
            set { g_Add21 = value; }
        }

        public static string g_add3
        {
            get { return g_Add31; }
            set { g_Add31 = value; }
        }
        public static string g_add4
        {
            get { return g_Add41; }
            set { g_Add41 = value; }
        }

        public static string g_Phone1
        {
            get { return g_Phone11; }
            set { g_Phone11 = value; }
        }

        public static string g_Phone2
        {
            get { return g_Phone21; }
            set { g_Phone21 = value; }
        }

        public static string g_rpthead
        {
            get { return g_rpthead1; }
            set { g_rpthead1 = value; }
        }

        public static string BchDW
        {
            get { return BchDW1; }
        }
        public static string BchNDW
        {
            get { return BchNDW1; }
        }
        public static string ChC
        {
            get { return ChC1; }
        }
        public static string ChE
        {
            get { return ChE1; }
        }
        public static string ChNE
        {
            get { return ChNE1; }
        }
        public static string Ch10
        {
            get { return Ch101; }
        }
        public static string ChN
        {
            get { return ChN1; }
        }
        public static string Ch12
        {
            get { return Ch121; }
        }
        public static string PageSkip
        {
            get { return PageSkip1; }
        }

        public static Boolean closing
        {
            get { return closing1; }
            set { closing1 = value; }
        }


        public static DateTime g_OpDate // keeps opening date of accounting year
        {
            get { return g_OpDate1; }
            set { g_OpDate1 = value; }
        }

        public static string messagename // keeps message title 
        {
            get { return messagename1; }
            set { messagename1 = value; }
        }
        public static DateTime g_ClDate //keeps closing date of accouting year
        {
            get { return g_ClDate1; }
            set { g_ClDate1 = value; }
        }

        public static DateTime g_Rdate //keeps the current date 
        {


            get { return g_Rdate1; }
            set { g_Rdate1 = value; }
        }

        public static DateTime g_from //keeps closing date of accouting year
        {
            get { return g_from1; }
            set { g_from1 = value; }
        }

        public static DateTime g_to //keeps closing date of accouting year
        {
            get { return g_to1; }
            set { g_to1 = value; }
        }


        public static Int32 g_NoFirms //keeps no of firms in a company
        {
            get { return g_NoFirms1; }
            set { g_NoFirms1 = value; }
        }

        public static string g_PrjCode // keeps the project code
        {
            get { return g_PrjCode1; }
            set { g_PrjCode1 = value; }
        }
        public static string drive // keeps the drive name where database is saved
        {
            get { return drive1; }
            set { drive1 = value; }
        }

        public static string g_Userdetails // keeps userdetails
        {
            get { return g_Userdetails1; }
            set { g_Userdetails1 = value; }
        }

        public static string g_UserId // keeps the userid of the user
        {
            get { return g_UserId1; }
            set { g_UserId1 = value; }
        }

        public static string g_UserLevel // keeps the user level of the user
        {
            get { return g_UserLevel1; }
            set { g_UserLevel1 = value; }
        }


        public static string password //keeps the password for the connection
        {
            get { return password1; }
            set { password1 = value; }
        }
        public static string g_firmcode //keeps the current firmcode
        {
            get { return g_firmcode1; }
            set { g_firmcode1 = value; }
        }

        public static string g_firmname //keeps the current firmcode
        {
            get { return _firmname; }
            set { _firmname = value; }
        }

        public static string g_TinNo //keeps the current firmcode
        {
            get { return g_TinNo1; }
            set { g_TinNo1 = value; }
        }

        public static string g_CstNo
        {
            get { return g_CstNo1; }
            set { g_CstNo1 = value; }
        }

        public static string g_compcode //keeps the current company code
        {
            get
            { return g_compcode1; }
            set { g_compcode1 = value; }
        }
        public static SqlConnection Connection // sqlconnection
        {
            get { return Comm_con1; }
            set { Comm_con1 = value; }
        }

        public static Boolean form
        {
            get { return form1; }
            set { form1 = value; }
        }


        private static string _filepath = Application.StartupPath;

        public static string FilePath
        {
            get { return _filepath; }
            set { _filepath = value; }
        }

        private static string sqlservername = string.Empty;

        public static string Sqlservername
        {
            get { return sqlservername; }
            set { sqlservername = value; }
        }

        private static string servermachinename = string.Empty;

        public static string Servermachinename
        {
            get { return servermachinename; }
            set { servermachinename = value; }
        }

        private static string _sqluserid = string.Empty;

        public static string Sqluserid
        {
            get { if (_sqluserid == string.Empty) { _sqluserid = "sa"; } return _sqluserid; }
            set { _sqluserid = value; }
        }



        private static string _referablebills = "YES";

        public static string referablebills
        {
            get { return _referablebills; }
            set { _referablebills = value; }
        }

        private static string _LookupDefault = "Itemcode";

        public static string LookupDefault
        {
            get { return _LookupDefault; }
            set { _LookupDefault = value; }
        }
        private static string _FormType = "A";

        public static string FormType
        {
            get { return _FormType; }
            set { _FormType = value; }
        }
        private static string _BillType = "Cash";

        public static string BillType
        {
            get { return _BillType; }
            set { _BillType = value; }
        }
        private static string _PriceType = "Retail";

        public static string PriceType
        {
            get { return _PriceType; }
            set { _PriceType = value; }
        }

        private static string _SalesEntryDate = "Editable";

        public static string SalesEntryDate
        {
            get { return _SalesEntryDate; }
            set { _SalesEntryDate = value; }
        }


        private static bool _taxincludeprice = true;

        public static bool TaxIncludePrice
        {
            get { return _taxincludeprice; }
            set { _taxincludeprice = value; }
        }

        private static string _RoundOff = "Paise";

        public static string RoundOff
        {
            get { return _RoundOff; }
            set { _RoundOff = value; }
        }

        private static string _AutoQty = "YES";

        public static string AutoQty
        {
            get { return _AutoQty.ToUpper().Trim(); }
            set { _AutoQty = value; }
        }

        private static bool  _ZeroQtySave = false ;

        public static bool ZeroQtySave
        {
            get { return _ZeroQtySave; }
            set { _ZeroQtySave = value; }
        }

        private static string _printType="Plain";
        public static string PrintType
        {
            get { return _printType; }
            set { _printType = value; }
        }

        private static string BillSize1 = "FULLPAGE";
        public static string BillSize
        {
            get { return BillSize1.ToUpper().Trim(); }
            set { BillSize1 = value; }
        }

        private static string LinestoReverse1 = "0";
        public static string LinestoReverse
        {
            get { return LinestoReverse1; }
            set { LinestoReverse1 = value; }
        }

        public static string BillPrinterName { get; set; }
        public static string SlipPrinterName { get; set; }
        public static string PrintMode { get; set; }
        public static string AutoPriceUpdation { get; set; }

        private static string LinestoSkip1 = "0";
        public static string LinestoSkip
        {
            get { return LinestoSkip1; }
            set { LinestoSkip1 = value; }
        }

        private static Boolean _mainmenuload = true;
        public static Boolean mainmenuload
        {
            get { return _mainmenuload; }
            set { _mainmenuload = value; }
        }
        private static Image _image =null;
        public static Image image // keeps the drive name where database is saved
        {
            get { return _image; }
            set { _image = value; }
        }

        private static string _abbr = "";
        public static string Abbr
        {
            get { return _abbr; }
            set { _abbr = value; }
        }

    }
}
