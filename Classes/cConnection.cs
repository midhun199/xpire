using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient ;

namespace Xpire.Classes
{
    public class cConnection
    {
        public SqlConnection Comm_con = new SqlConnection();

        public Boolean ConnectMe(string strPassword,string strUserId, string strDatabase, string strDataSource)
        {
            try
            {
                if (cPublic.Connection.State == System.Data.ConnectionState.Open)
                {
                    cPublic.Connection.Close();
                    cPublic.Connection.Dispose();
                }

                cPublic.Connection = new SqlConnection("uid=" + strUserId + ";pwd=" + strPassword + ";database=" + strDatabase + ";data source=" + strDataSource + ";Connection TimeOut=300");
                if (cPublic.Connection.State == System.Data.ConnectionState.Closed)
                {
                    cPublic.Connection.Open();
                }
                return true;
            }
            catch (Exception ex)
            {
                string error = ex.Message; 
                System.Threading.Thread.Sleep(1000);
                SqlConnection.ClearAllPools();
                return false;
            }
        }

        public void RefreshConn()
        {
            if (cPublic.Connection.State .Equals(System.Data.ConnectionState.Open))
            {
                cPublic.Connection.Close();                
            }
            cPublic.Connection.Open();
        }
    }
}
