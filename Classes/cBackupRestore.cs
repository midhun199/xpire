using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace Xpire.Classes
{
    public class cBackupRestore
    {
        public void Backup()
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.Filter = "All files (*.*)|*.*";
            savefile.RestoreDirectory = false;
            if (savefile.ShowDialog() == DialogResult.OK)
            {
                Backup_Sql(savefile.FileName);
            }
        }

        private void Backup_Sql(string filename)
        {
            try
            {
                string query="backup database " + cPublic.g_PrjCode  + cPublic.g_compcode+  " to disk = '" + filename + "'";
                SqlCommand cmd = new SqlCommand(query, cPublic.Connection);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Backup Creation Sucessful " + Environment.NewLine + " Path: " + filename, cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void Restore()
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Filter = "All files (*.*)|*.*";
            openfile.Multiselect = false;
            openfile.RestoreDirectory = false;
            if (openfile.ShowDialog() == DialogResult.OK)
            {
                Restore_Sql(openfile.FileName);
            }
        }

        private void Restore_Sql(String filename)
        {
            try
            {
                cPublic.Connection.Close();
                cPublic.Connection.Dispose();
                SqlConnection.ClearAllPools();

                cConnection con = new cConnection();
                con.ConnectMe(cPublic.password, cPublic.Sqluserid, "MASTER", cPublic.Sqlservername);


                string query = "Restore database " + cPublic.g_PrjCode + cPublic.g_compcode + " from disk = '" + filename + "'";
                SqlCommand cmd = new SqlCommand(query, cPublic.Connection);
                cmd.ExecuteNonQuery();

                con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + cPublic.g_compcode, cPublic.Sqlservername);

                MessageBox.Show("Restore Sucessful !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


    }
}
