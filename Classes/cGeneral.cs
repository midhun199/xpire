using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Collections;
using System.Diagnostics;
using C1.Win.C1FlexGrid;
using Microsoft.Win32;
using System.Security.AccessControl;
using System.Net.NetworkInformation;

using CodeBlock_Demo_Author;
using System.Net;

using Xpire.Settings;

namespace Xpire.Classes
{
    public class cGeneral
    {
        string numstr, RUP;
        string shhead;
        string sql = string.Empty;

        int i;
        int d1, d2;
        string[] ones ={ " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve", " Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Ninteen" };
        // string[] tens ={ " Zero", " One", " Twenty", " Thirty", " Fourty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety" };
        string[] tens ={ " Twenty", " Thirty", " Fourty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety" };
        string[] others ={ " Crore", " Lakh", " Thousand" };
        cConnection con = new cConnection();

        public SqlCommand cmd=new SqlCommand(string.Empty,cPublic.Connection ) ;
        public SqlDataReader dread;
        
        public static string formname = "";
        public static string formtag = "";
        public static string formtext = "";
        public static double slNo;
        public static int maxline;
        public static int lineno;
        public static int coun;
       
        public static double prtQty;
        public static double prtGv;
        public static double prtTaxamt;
        public static double prtCessamt;
        public static double prtNetamt;

        public static double prtNetVlu;
        public static double prtItem;
        public static double prtDisc;
        public static int skiplines;
        public static double mrptotal;
        public static double itemt;
        public static StreamWriter tstream;

        public void ClearFieldsInMe(Control obj)
        {
            for (int i = 0; i < obj.Controls.Count; i++)
            {
                if (obj.Controls[i].HasChildren)
                { ClearFieldsInMe(obj.Controls[i]); }
                if (obj.Controls[i].GetType().ToString().EndsWith("TextBox"))
                { obj.Controls[i].Text = string.Empty; obj.Controls[i].Tag = string.Empty; }
            }
        }

        public void EDControls(Control obj, bool status)
        {
            for (int i = 0; i < obj.Controls.Count; i++)
            {
                if (obj.Controls[i].HasChildren)
                    EDControls(obj.Controls[i], status);
                if (obj.Controls[i].GetType() != typeof(Label) && obj.Controls[i].GetType() != typeof(Panel))
                    obj.Controls[i].Enabled = status;
                if (obj.Controls[i].GetType() != typeof(Label) && obj.Controls[i].GetType() != typeof(Panel))
                    obj.Controls[i].Enabled = status;
            }
        }

        # region EnCrypt
        //FOR ENCRYPTION METHOD
        public byte[] EnCrypt(string EnCryptText)
        {
            Int32 Asci = 0;
            ArrayList wr = new ArrayList();
            string Enc = string.Empty;

            for (int i = 0; i < EnCryptText.Length; i++)
            {
                Asci = (Strings.Asc(EnCryptText.Substring(i, 1)) * 5) + 21;
                Enc = Strings.ChrW(Asci).ToString() + Strings.Chr(i + 1);
                wr.Add(Enc);

            }
            Enc = string.Empty;
            for (int i = 0; i < wr.Count; i++)
            {
                Enc += wr[i].ToString();

            }

            byte[] buffer = new byte[Enc.Length];
            for (int j = 0; j < Enc.Length; j++)
            {
                buffer.SetValue(Convert.ToByte(Strings.Asc(Enc.Substring(j, 1))), j);
            }

            return buffer;
        }
        # endregion

        # region DeCrypt
        //FOR DECRYPTION METHOD
        private string DeCrypt(string DeCryptText)
        {
            int Asci = 0;
            string Decry = string.Empty;
            ArrayList wr = new ArrayList();

            for (int i = 0; i < DeCryptText.Length; i++)
            {
                if (i % 2 == 0)
                {
                    Asci = (Strings.AscW(Convert.ToChar(DeCryptText.Substring(i, 1)).ToString()) - 21) / 5;
                    wr.Add(Strings.ChrW(Asci).ToString());
                }
            }

            for (int i = 0; i < wr.Count; i++)
            {
                Decry += wr[i].ToString();
            }
            return Decry;
        }

        # endregion

        public void edcolors(Control obj, Color newcolor)
        {
            for (int i = 0; i < obj.Controls.Count; i++)
            {
                if (obj.Controls[i].HasChildren)
                    edcolors(obj.Controls[i], newcolor);
                if (obj.Controls[i].GetType() != typeof(Label) && obj.Controls[i].GetType() != typeof(Panel) && obj.Controls[i].GetType() != typeof(Button) && obj.Controls[i].GetType() != typeof(GroupBox))
                    obj.Controls[i].BackColor = newcolor;
            }
        }

        public string Words(int Number)
        {
            string Word = string.Empty;
            if (Number > 999999999.99)
            {
                MessageBox.Show("Large Number - Cannot Convert To Words");
                return Word;
            }

            ones[0] = " One";
            ones[1] = " Two";
            ones[2] = " Three";
            ones[3] = " Four";
            ones[4] = " Five";
            ones[5] = " Six";
            ones[6] = " Seven";
            ones[7] = " Eight";
            ones[8] = " Nine";
            ones[9] = " Ten";
            ones[10] = " Eleven";
            ones[11] = " Twelve";
            ones[12] = " Thirteen";
            ones[13] = " Fourteen";
            ones[14] = " Fifteen";
            ones[15] = " Sixteen";
            ones[16] = " Seventeen";
            ones[17] = " Eighteen";
            ones[18] = " Nineteen";

            //tens[0] = " Zero";
            //tens[1] = " One";
            tens[0] = " Twenty";
            tens[1] = " Thirty";
            tens[2] = " Fourty";
            tens[3] = " Fifty";
            tens[4] = " Sixty";
            tens[5] = " Seventy";
            tens[6] = " Eighty";
            tens[7] = " Ninety";

            others[0] = " Crore";
            others[1] = " Lakh";
            others[2] = " Thousand";
            numstr = (String.Format(Number.ToString(),"0#########.00"));
            RUP = string.Empty;

            for (i = 1; i < 3; i++)
            {
                d1 = Convert.ToInt32(Strings.Mid(numstr, (i * 2) - 1, 1));
                d2 = Convert.ToInt32(Strings.Mid(numstr, i * 2, 1));
                if ((d1 > 0) || (d2 > 0))
                    RUP = RUP + twodigit(d1, d2) + others[i];
            }

            // d1 = Convert.ToInt32(Strings.Mid(numstr, 7, 1));
            if (d1 > 0)
                RUP = RUP + ones[d1] + " Hundred";


            d1 = Convert.ToInt32(Strings.Mid(numstr, 8, 1));
            d2 = Convert.ToInt32(Strings.Mid(numstr, 9, 1));
            if ((d1 > 0) || (d2 > 0))
                if (RUP != "")
                    RUP = RUP + " and" + twodigit(d1, d2);
                else
                    RUP = twodigit(d1, d2);


            if (RUP != "")
                RUP = " Rupees" + RUP;


            //d1 = Convert.ToInt32(Strings.Mid(numstr, 11, 1));
            //d2 = Convert.ToInt32(Strings.Mid(numstr, 12, 1));
            if ((d1 > 0) || (d2 > 0))
            {
                if (RUP != "")
                    RUP = RUP + " and" + twodigit(d1, d2) + " Paise";
                else
                    RUP = twodigit(d1, d2);
            }

            
            if (RUP != "")
                Word = RUP + " Only";
            else
                Word = "Nil";

            return Word;

        }

        public string twodigit(int d1, int d2)
        {
            int d12;
            string twodig = "";
            d12 = d1 * 10 + d2;
            if (d12 > 0)
                if (d12 < 20)
                    twodig = ones[d12];
                else
                    twodig = tens[d1] + Interaction.IIf(Convert.ToBoolean(d2 = 0), "", ones[d2]);

            return twodig;
        }

        public Boolean ServerDet()
        {
            try
            {
                String File_Name = cPublic.FilePath  + "\\" + cPublic.g_PrjCode + ".dll";
                int i = 1;

                if (File.Exists(File_Name))
                {

                    using (StreamReader sr = new StreamReader(File_Name))
                    {
                        String line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            switch (i)
                            {
                                case 1:
                                    cPublic.Sqlservername = line;
                                    cPublic.Servermachinename = cPublic.Sqlservername;
                                    string[] split = cPublic.Servermachinename.Split(new Char[] { '\\' });
                                    foreach (string s in split)
                                    {
                                        if (s.Trim() != "")
                                        { cPublic.Servermachinename = s.Trim(); break; }
                                    }
                                    break;
                                case 2:
                                    cPublic.Sqluserid = line;
                                    break;
                                case 3:
                                    cPublic.password = line;
                                    break;
                                case 4:
                                    //cPublic.g_compcode = line;
                                    break;
                            }
                            i++;
                        }
                        sr.Close();
                        return true;


                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public Boolean WriteNetcon(string servername,string sqluserid, string password, string prjcode)
        {
            try
            {
                String File_Name = cPublic.FilePath  + "\\"+ cPublic.g_PrjCode + ".dll";

                using (StreamWriter sw = new StreamWriter(File_Name, false))
                {
                    sw.WriteLine(servername);
                    sw.WriteLine(sqluserid);
                    sw.WriteLine(password);
                    sw.WriteLine(prjcode);
                    sw.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        public Boolean NetExist()
        {
            try
            {
                String File_Name = cPublic.FilePath   + "\\" + cPublic.g_PrjCode + ".dll";
                if (!File.Exists(File_Name))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public string GetUserDet()
        {
            return cPublic.g_UserId + Strings.Space(6 - cPublic.g_UserId.Length) + " " + DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00") + " " + Environment.MachineName.Substring(0, 2);
        }

       
        public static void EnableDisableAllFields(Control obj, bool state)
        {
            Control ctrl = new Control();
            int i;
            try
            {
                foreach (Control tempLoopVar_ctrl in obj.Controls)
                {
                    ctrl = tempLoopVar_ctrl;
                    for (i = 0; i <= ctrl.Controls.Count - 1; i++)
                    {
                        EnableDisableAllFields(ctrl, state);
                    }
                    if (!(ctrl is Label || ctrl is GroupBox || ctrl is Panel))
                    {
                        ctrl.Enabled = state;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        internal static void NumericValidation(TextBox Numberic, System.Windows.Forms.KeyPressEventArgs e)
        {
            decimal Sn = 0;
            if (Numberic.Text.Trim() == "")
            {
                Sn = (decimal)0;
            }
            else
            {
                Sn = System.Convert.ToDecimal(Numberic.Text);
            }

            if ((Strings.Asc(e.KeyChar) >= 48 && Strings.Asc(e.KeyChar) <= 57) || Strings.Asc(e.KeyChar) == 46 || Strings.Asc(e.KeyChar) == 8)
            {
                if ((Strings.Asc(e.KeyChar) >= 48 && Strings.Asc(e.KeyChar) <= 57) || Strings.Asc(e.KeyChar) == 46)
                {
                    if (Information.IsNumeric(Sn + e.KeyChar) == false)
                    {
                        e.Handled = true;
                    }
                    else if (System.Convert.ToInt32(System.Convert.ToDecimal(Sn) + e.KeyChar) > 9999999)
                    {
                        e.Handled = true;
                    }
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        public static void ClearAllFields(Control obj)
        {
            Control ctrl = new Control();
            int i;
            try
            {
                foreach (Control tempLoopVar_ctrl in obj.Controls)
                {
                    ctrl = tempLoopVar_ctrl;
                    for (i = 0; i <= ctrl.Controls.Count - 1; i++)
                    {
                        ClearAllFields(ctrl);
                    }
                    if (ctrl is TextBox)
                    {
                        ctrl.Text = "";
                    }
                    else if (ctrl is ComboBox)
                    {
                        ComboBox cb;
                        cb = (ComboBox)ctrl;
                        if (cb.Items.Count != 0)
                        {
                            cb.SelectedIndex = 0;
                        }
                    }
                    else if (ctrl is ListBox)
                    {
                        ListBox lb;
                        lb = (ListBox)ctrl;
                        lb.Items.Clear();
                    }
                    else if (ctrl is DateTimePicker)
                    {
                        DateTimePicker dtp;
                        dtp = (DateTimePicker)ctrl;
                        dtp.Value = cPublic.g_Rdate;
                        dtp.Checked = false;
                    }
                    else if (ctrl is PictureBox)
                    {
                        PictureBox pb;
                        pb = (PictureBox)ctrl;
                        pb.Image = null;
                    }
                    else if (ctrl is CheckBox)
                    {
                        CheckBox cb;
                        cb = (CheckBox)ctrl;
                        cb.Checked = false;
                    }
                    else if (ctrl is RadioButton)
                    {
                        RadioButton rb;
                        rb = (RadioButton)ctrl;
                        rb.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void Firmselection(string vCode)
        {

            con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + cPublic.g_compcode, cPublic.Sqlservername);
            if (vCode == "")
            {
                sql = "select code,dispname,name,addr1, addr2, addr3, addr4, phone1, phone2,clddate,opdate,kgst,cstno from firms order by code";
            }
            else
            {
                sql = "select code,dispname,name,addr1, addr2, addr3, addr4, phone1, phone2,clddate,opdate,kgst,cstno from firms  where code='" + vCode + "' order  by code";
            }

            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                cPublic.g_FirmArray.Add(dr["code"].ToString());
            }

            cPublic.g_firmcode = ds.Tables[0].Rows[0]["code"].ToString();
            cPublic.g_DispName = ds.Tables[0].Rows[0]["dispname"].ToString();
            cPublic.g_firmname = ds.Tables[0].Rows[0]["name"].ToString();
            cPublic.g_add1 = ds.Tables[0].Rows[0]["addr1"].ToString();
            cPublic.g_add2 = ds.Tables[0].Rows[0]["addr2"].ToString();
            cPublic.g_add3 = ds.Tables[0].Rows[0]["addr3"].ToString();
            cPublic.g_add4 = ds.Tables[0].Rows[0]["addr4"].ToString();
            cPublic.g_Phone1 = ds.Tables[0].Rows[0]["phone1"].ToString();
            cPublic.g_Phone2 = ds.Tables[0].Rows[0]["phone2"].ToString();
            cPublic.g_TinNo = ds.Tables[0].Rows[0]["kgst"].ToString();
            cPublic.g_CstNo = ds.Tables[0].Rows[0]["cstno"].ToString();
            cPublic.g_ClDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["clddate"].ToString());
            cPublic.g_OpDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["opdate"].ToString());

            cPublic.mainmenuload = true;
        }

        public void LoadCompdetails(String code)
        {
            con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
            string sql = "select code,name,dispname,abbr,acctype,opdate,clddate,path,picture from company where code=" + code;
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            SqlDataReader dr = comm.ExecuteReader();
            if (dr.Read())
            {
                cPublic.g_compcode = dr["code"].ToString();
                cPublic.g_ClDate =Convert.ToDateTime( dr["clddate"].ToString());
                cPublic.g_OpDate = Convert.ToDateTime(dr["opdate"].ToString());
                cPublic.drive = dr["path"].ToString();
                cPublic.Abbr= dr["abbr"].ToString();

                cPublic.mainmenuload = true;

                object theValue = dr["picture"];

                if (DBNull.Value != theValue)
                {

                    byte[] data = (byte[])dr["picture"];
                    MemoryStream ms = new MemoryStream(data);
                    cPublic.image = Image.FromStream(ms);
                }

                dr.Close();
                con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + cPublic.g_compcode, cPublic.Sqlservername);
                setparameters();
            }
            dr.Close();

            Firmselection("");
        }

        public Boolean GetUserCompcount()
        {
            if ((cPublic.g_UserLevel != "9" && cPublic.g_UserLevel != "8"))
            {
                string sql = "select  a.dispname,a.code from " + cPublic.g_PrjCode + "000..company a, " + cPublic.g_PrjCode + "000..usercompanies b where b.userid='" + cPublic.g_UserId + "' and a.code=b.compcode";
                SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 1)
                {
                    return true;
                }
                else
                {
                    sql = "select getdate()";
                    SqlCommand cmd = new SqlCommand(sql, cPublic.Connection);
                    cPublic.g_Rdate = Convert.ToDateTime(cmd.ExecuteScalar());
                    LoadCompdetails(dt.Rows[0][1].ToString());
                }
                return false;
            }
            return true;
        }

        public Boolean isOpenForm(String frmName, string tag)
        {
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (tag != string.Empty)
                {
                    if (Application.OpenForms[i].Name == frmName && Application.OpenForms[i].Tag.ToString() == tag)
                    { return true; }
                }
                else
                { if (Application.OpenForms[i].Name == frmName) { return true; } }
            }
            return false;
        }

        public void createpath(string PartDir)
        {
            if (!Directory.Exists(cPublic.drive + "Maxx"))
                Directory.CreateDirectory(cPublic.drive + "Maxx");
            if (!Directory.Exists(cPublic.drive + "Maxx\\" + cPublic.g_PrjCode))
                Directory.CreateDirectory(cPublic.drive + "Maxx\\" + cPublic.g_PrjCode);
            if (!Directory.Exists(cPublic.drive + "Maxx\\" + cPublic.g_PrjCode + "\\" + cPublic.g_PrjCode + PartDir))
                Directory.CreateDirectory(cPublic.drive + "Maxx\\" + cPublic.g_PrjCode + "\\" + cPublic.g_PrjCode + PartDir);
        }
        public void CreateRemotePath(string PartDir)
        {
            NetworkCredential myCredentials = new NetworkCredential("mithun", "mithun199", "FOURSQUARE");
            using (new NetworkConnection(@"\\server", myCredentials))
            using (new NetworkConnection(@"\\server", myCredentials))
            {
                if ((!Directory.Exists("\\\\server\\Desiners -2\\Maxx")))
                    Directory.CreateDirectory("\\\\server\\Desiners -2\\Maxx");
                if ((!Directory.Exists("\\\\server\\Desiners -2\\Maxx\\" + cPublic.g_PrjCode)))
                    Directory.CreateDirectory("\\\\server\\Desiners -2\\Maxx\\" + cPublic.g_PrjCode);
                if ((!Directory.Exists("\\\\server\\Desiners -2\\Maxx\\" + cPublic.g_PrjCode + "\\" + cPublic.g_PrjCode + PartDir)))
                    Directory.CreateDirectory("\\\\server\\Desiners -2\\Maxx\\" + cPublic.g_PrjCode + "\\" + cPublic.g_PrjCode + PartDir);
            }
            //if ((!Directory.Exists("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Maxx")))
            //    Directory.CreateDirectory("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Maxx");
            //if ((!Directory.Exists("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Maxx" + "\\" + cPublic.g_PrjCode)))
            //    Directory.CreateDirectory("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Maxx" + "\\" + cPublic.g_PrjCode);
            //if ((!Directory.Exists("\\\\" + cPublic.Servermachinename  + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Maxx" + "\\" + cPublic.g_PrjCode + "\\" + cPublic.g_PrjCode + PartDir)))
            //    Directory.CreateDirectory("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Maxx" + "\\" + cPublic.g_PrjCode + "\\" + cPublic.g_PrjCode + PartDir);
        }
        public bool isFormClosing()
        {
            StackTrace sTrace = new StackTrace();

            for (byte i = 2; i <= sTrace.FrameCount - 1; i = (byte)(i + 1))
            {
                if (sTrace.GetFrame(i).GetMethod().Name == "WmClose")
                {
                    return true;
                }
            }
            return false;
        }
        public void RepPrint(string _vFileName)
        {
            try
            {
                string _Filepath = cPublic.FilePath + "\\" + _vFileName + ".txt";
                System.Drawing.Printing.PrinterSettings ps = new System.Drawing.Printing.PrinterSettings();
                //string _PName = cPublic.PrintType ;

                //if (_PName.Substring(0, 2).Equals("\\\\"))

                if (string.IsNullOrEmpty(cPublic.BillPrinterName) == true)
                {
                    System.IO.File.Copy(_Filepath, "prn");
                }
                else
                {
                    System.IO.File.Copy(_Filepath, cPublic.BillPrinterName);
                }
                //else
                //    System.IO.File.Copy(_Filepath, "prn");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Printer settings not found" + Environment.NewLine + ex.Message, cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public string FnAlignString(string _asCap, string _asType, Int32 _StrLen)
        {
            int _lispacelen;
            string _lsretstr;
            if (_asCap.Length > _StrLen)
                _asCap = _asCap.Substring(0, _StrLen);
            _lispacelen = _StrLen - _asCap.Length;
            _lsretstr = Strings.Space(_StrLen);

            if (_asCap.Length <= 0) _lispacelen = 0;

            switch (_asType)
            {
                case "R":
                    _lsretstr = Strings.Space(_lispacelen) + _asCap;
                    break;
                case "L":
                    _lsretstr = _asCap + Strings.Space(_lispacelen);
                    break;
                case "C":
                    if (_lispacelen >= 0)
                        if (_lispacelen / 2 == 0)
                            _lsretstr = (Strings.Space(_lispacelen+ 1) ) + _asCap;
                        else
                            if (_lispacelen % 2 == 1)
                                _lsretstr = (Strings.Space(_lispacelen / 2 + 1)) + _asCap + Strings.Space((_lispacelen / 2 + 1));
                            else
                                _lsretstr = (Strings.Space(_lispacelen / 2 + 1)) + _asCap + Strings.Space((_lispacelen / 2));
                    break;
            }
            return _lsretstr;
        }
        public string Replicate(String _Repls, Int32 _ReplNos)
        {
            Int32 _Repli;
            String _RepliTemp;
            _RepliTemp = "";
            for (_Repli = 1; _Repli <= _ReplNos; _Repli++)
            {
                _RepliTemp += _Repls;
            }
            return _RepliTemp;
        }

        public void setparameters()
        {
            if (cPublic.g_firmcode == string.Empty) { cPublic.g_firmcode = "0001"; }


            cmd = new SqlCommand("select * from public_var", cPublic.Connection);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                switch (dr["var_name"].ToString())
                {
                    case "Walkin User Password":

                        break;
                    case "RoundOff":
                        cPublic.RoundOff = dr["var_values"].ToString();
                        break;
                    case "PriceType":
                        cPublic.PriceType = dr["var_values"].ToString();
                        break;
                    case "FormType":
                        cPublic.FormType = dr["var_values"].ToString();
                        break;
                    case "Referable Bills":
                        cPublic.referablebills = dr["var_values"].ToString();
                        break;
                    case "LookupDefault":
                        cPublic.LookupDefault = dr["var_values"].ToString();
                        break;
                    case "Modification Date":
                        cPublic.SalesEntryDate = dr["var_values"].ToString();
                        break;
                    case "BillType":
                        cPublic.BillType = dr["var_values"].ToString();
                        break;

                    case "AutoQty":
                        cPublic.AutoQty = dr["var_values"].ToString();
                        break;

                    case "PrintType":
                        cPublic.PrintType = dr["var_values"].ToString();
                        break;

                    case "BillSize":
                        cPublic.BillSize = dr["var_values"].ToString();
                        break;

                    case "TaxIncludePrice":
                        if (dr["var_values"].ToString().ToUpper() == "YES")
                        { cPublic.TaxIncludePrice = true; }
                        else { cPublic.TaxIncludePrice = false; }
                        break;
                    case "ZeroQtySave":
                        if (dr["var_values"].ToString().ToUpper() == "YES")
                        { cPublic.ZeroQtySave = true; }
                        else { cPublic.ZeroQtySave = false; }
                        break;

                    case "Lines to Skip":
                        cPublic.LinestoSkip = dr["var_values"].ToString();
                        break;

                    case "Lines to Reverse":
                        cPublic.LinestoReverse = dr["var_values"].ToString();
                        break;

                    case "Bill Printer":
                        cPublic.BillPrinterName = dr["var_values"].ToString();
                        break;

                    case "Slip Printer":
                        cPublic.SlipPrinterName = dr["var_values"].ToString();
                        break;

                    case "PrintMode":
                        cPublic.PrintMode = dr["var_values"].ToString();
                        break;
                    case "AutoPriceUpdation":
                        cPublic.AutoPriceUpdation = dr["var_values"].ToString();
                        break;

                }
            }
            dr.Close();
        }
        
        public void flexnum(C1FlexGrid ControlName, KeyPressEditEventArgs e)
        {
            int key = (int)e.KeyChar;

            if (key > 47 && key < 58)
            {
                return;
            }
            else if (key == 46)
                if (ControlName.Editor.Text.IndexOf('.').Equals(-1))
                    return;
            e.Handled = true;


        }

        public int getMaxNumber(string tableName, string fieldName)
        {
            SqlCommand cmd = new SqlCommand("select isnull(max(" + fieldName + "),0) from " + tableName, cPublic.Connection);
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        public int MaxVoucher(string Transtype, SqlCommand cmd)
        {
            cmd.Parameters.Clear();
            cmd.CommandText = "select isnull(max(transno),0) from voucher" + cPublic.g_firmcode + " where transtype='" + Transtype + "'";
            cmd.CommandType = CommandType.Text;
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        public string FnAlignStringlarge(string p_head, object P_rwidth)
        {
            //'        Dim space1 As Integer, space2 As String
            int X;
            int XX;
            X = (Convert.ToInt32(P_rwidth) - p_head.Trim().Length * 2) / 2;
            XX = Conversion.Int(X / 2);
            p_head = Strings.Space(XX) + p_head.Trim();
            return p_head;
        }
       
        public RadioButton getCheckedRadio(Control obj)
        {
            RadioButton rd = new RadioButton();
            for (int i = 0; i < obj.Controls.Count; i++)
                if (obj.Controls[i].GetType() == typeof(RadioButton))
                {
                    rd = (RadioButton)obj.Controls[i];
                    if (rd.Checked)
                        break;
                }
            return rd;
        }

        public void Firmselection()
        {
            if (cPublic.Login == true)
            {
                cGeneral gen = new cGeneral();
                if (gen.GetUserCompcount())
                {
                    frmFirmSelection sel = new frmFirmSelection();
                    sel.ShowDialog();
                }
                if (!cPublic.closing)
                {
                   
                }
            }
        }

        public string head(string heading)
        {
            int positon = 0;
            string newword = "", newstr = "";

            heading = heading.ToLower();
            positon = heading.IndexOf(" ", 0);

            while (positon >= 0)
            {
                newword = heading.Substring(0, positon + 1);
                newstr = newstr + (newword.Substring(0, 1)).ToUpper() + (newword.Substring(1));
                heading = heading.Substring(positon + 1);
                positon = heading.IndexOf(" ", 0);
            }
            newword = heading;
            newstr = newstr + (newword.Substring(0, 1)).ToUpper() + (newword.Substring(1));
            return newstr;
        }

        public string GetHeadBal(string code)
        {
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Clear();
                cmd.CommandText = "select balance from accounts" + cPublic.g_firmcode + " where code='" + code + "'";
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    double val = (Convert.IsDBNull(dr.GetValue(0))) ? 0 : Convert.ToDouble(dr.GetValue(0));
                    string type;
                    if (val < 0)
                    {
                        type = "Dr.";
                    }
                    else
                    {
                        type = "Cr.";
                    }
                    dr.Close();
                    return type + " " + Math.Abs(val).ToString("##,###0.00");
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return "0.00";
        }


        public Boolean ChekAccYear(DateTime varDate)
        {
            if (varDate.Date >= cPublic.g_OpDate.Date)
            {
                if (varDate.Date <= cPublic.g_ClDate.Date)
                {
                    return true;
                }
            }
            return false;
        }

        public string groupcode(string code)
        {
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select groupcode from accounts" + cPublic.g_firmcode + " where code='" + code + "'";
            return cmd.ExecuteScalar() + "";
        }

        public bool checkNoChecking(string checkno, string orderno, string transtype)
        {
            if (checkno.Trim() == string.Empty) { return false; }
            string query = "select * from VOUCHER" + cPublic.g_firmcode + " where  chqno=@chqno and recno not in (select recno from VOUCHER" + cPublic.g_firmcode + " where  transno=@transno  and transtype=@transtype) ";
            cmd.CommandText = query;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@transno", orderno);
            cmd.Parameters.AddWithValue("@chqno", checkno);
            cmd.Parameters.AddWithValue("@transtype", transtype);
            if (cmd.ExecuteScalar() == null)
            { return false; }
            else { return true; }
        }

        public string GetHead(string code)
        {
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select head from accounts" + cPublic.g_firmcode  + " where code='" + code + "'";
            cmd.Parameters.Clear();
            return cmd.ExecuteScalar() + "";
        }

        public ArrayList head(string _field, string _table, string _fieldname, string _condition)
        {
            fssgen.fssgen gen = new fssgen.fssgen();
            int i = 0;
            int len = 0;
            ArrayList head1 = new ArrayList();
            head1.Add("");
            string sql = "select " + _field + " from " + _table + cPublic.g_firmcode  + " where " + _fieldname + "='" + gen.SQLFormat(_condition) + "'";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Clear();
            cmd.CommandText = sql;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                len = dr.FieldCount;
                head1 = new ArrayList();

                for (i = 0; i < len; i++)
                {
                    head1.Add(dr.GetString(i));
                }
            }
            dr.Close();
            return head1;
        }



        public void MultVouchprint(string transtype, string transno, string bookcode, decimal amount, string code)
        {

            if (bookcode + "" == "000000")
            {
                shhead = "JOURNAL";
            }
            else if (Convert.ToDecimal(amount) < 0)
            {

                shhead = "PAYMENT VOUCHER";
            }
            else if (Convert.ToDecimal(amount) > 0)
            {
                shhead = "RECEIPT VOUCHER";


            }

            string strdata;


            if (!Directory.Exists("C:\\Maxx"))
            {
                Directory.CreateDirectory("C:\\Maxx");
            }
            int  vPath = Convert.ToInt32(1000 * VBMath.Rnd()) + 1;

            if (Directory.Exists(cPublic.FilePath + "\\" + vPath + ".txt"))
            {
                Directory.Delete(cPublic.FilePath + "\\" + vPath + ".txt");
            }
            string path = cPublic.FilePath + "\\" + vPath + ".txt";
            tstream = File.CreateText(path);

            DataSet ds2 = new DataSet();
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();

            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text; 
            cmd.CommandText  = "select * from Voucher" + cPublic.g_firmcode + " where transtype='" + transtype + "' and transno=" + transno + " and code=" + code + "";
            SqlDataAdapter da = new SqlDataAdapter(cmd );
            ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Voucher Not Found");
                vPath = 0;
                return;
            }
            else
            {
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from Accounts" + cPublic.g_firmcode + " where code='" + (ds.Tables[0].Rows[0]["CODE"].ToString()) + "'";
                SqlDataAdapter da2 = new SqlDataAdapter(cmd);
                ds2 = new DataSet();
                da2.Fill(ds2);
                if (bookcode + "" == "000000")
                {

                    cmd.Parameters.Clear();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select * from Accounts" + cPublic.g_firmcode + " where code='" + (ds.Tables[0].Rows[0]["CODE"].ToString()) + "'";
                    SqlDataAdapter da1 = new SqlDataAdapter(cmd);
                    ds1 = new DataSet();
                    da1.Fill(ds1);
                }
            }

          
            tstream.Write(cPublic.ChN);
            tstream.Write(cPublic.Ch10 + cPublic.ChN);
            tstream.WriteLine(cPublic.BchDW);
            tstream.WriteLine(FnAlignStringlarge(cPublic.g_firmname, 75));
            tstream.Write(cPublic.BchNDW);
            tstream.Write(cPublic.Ch10 + cPublic.ChN);
            tstream.Write(cPublic.Ch12);
            tstream.WriteLine(FnAlignString(cPublic.g_add1 + " " + cPublic.g_add2, "C", 78));
            tstream.WriteLine(FnAlignString(cPublic.g_add3 + " " + cPublic.g_add4, "C", 78));
            tstream.WriteLine(FnAlignString(shhead, "C", 78));
            tstream.Write(FnAlignString("VoucherNo. ", "L", 10));
            strdata = ((transno));
            tstream.Write(FnAlignString(strdata, "L", 6));
            tstream.Write(Strings.Space(44));
            tstream.WriteLine(FnAlignString("Date : " + Convert.ToDateTime(ds.Tables[0].Rows[0]["date1"]).ToString("dd/MM/yyyy"), "L", 18));
            tstream.WriteLine("");
            //lineno = lineno + 6;


            if (bookcode + "" == "000000")
            {

                MultJourprintline(ds2,ds,ds1   );
            }
            else if (Convert.ToDecimal(amount) < 0)
            {

                MultPayprintline(ds2,ds  );
            }
            else if (Convert.ToDecimal(amount) > 0)
            {
                MultRecprintLine(ds2,ds );


            }
            if (vPath > 0)
            {
                RepPrint(vPath.ToString());
            }
        }

        public void MultRecprintLine(DataSet ds2,DataSet ds)
        {
            string strdata;
            tstream.WriteLine();
            tstream.WriteLine();
            //double lineNo = lineNo + 2;

            strdata = "Received with thanks from " + (ds2.Tables[0].Rows[0]["HEAD"].ToString());
            if ((ds2.Tables[0].Rows[0]["ADDRESS"].ToString()) == "")
            {
                strdata = strdata + ", " + (ds2.Tables[0].Rows[0]["ADDRESS"].ToString());
            }
            if (ds2.Tables[0].Rows[0]["STREET"].ToString() == "")
            {
                strdata = strdata + ", " + (ds2.Tables[0].Rows[0]["STREET"].ToString());
            }

            if (ds2.Tables[0].Rows[0]["CITY"].ToString() == "")
                strdata = strdata + ", " + (ds2.Tables[0].Rows[0]["CITY"].ToString());

            strdata = strdata + Replicate(".", 250);

            string ln1;
            string ln2;
            string ln3;

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            ln3 = Strings.Mid(strdata, 157, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln3, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln3, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"]));
            strdata = strdata + "/- ( ";
            string curr;
            double amt = Math.Abs(Convert.ToDouble(ds.Tables[0].Rows[0]["AMOUNT"]));
            fssgen.fssgen gen = new fssgen.fssgen();
            curr = gen.NumToChars(ref amt);

            strdata = strdata + "Rupees " + Strings.Trim(curr) + ")" + Replicate(".", 200);

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"]));
            strdata = strdata + "/- ( ";

            strdata = "Being " + (ds.Tables[0].Rows[0]["NARRATION"].ToString());

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Len(Strings.Trim(ln1)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Len(Strings.Trim(ln2)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            tstream.WriteLine();
            tstream.WriteLine();
            lineno = lineno + 2;
            if (Convert.ToInt32(ds.Tables[0].Rows[0]["BOOKCODE"].ToString()) > 200002)
            {
                tstream.Write(FnAlignString("Cheque / Draft No.:", "L", 20));
                tstream.WriteLine(FnAlignString((ds.Tables[0].Rows[0]["CHQNO"].ToString()), "L", 10));
                tstream.Write(FnAlignString("Dated             :", "L", 20));
                tstream.Write(FnAlignString((ds.Tables[0].Rows[0]["CHQDATE"].ToString()), "L", 10));
                lineno = lineno + 1;
            }

            tstream.WriteLine();
            lineno = lineno + 1;
            //Ch10 + chN +
            tstream.Write(cPublic.BchDW);
            tstream.Write(FnAlignString("Rs.", "L", 3));
            tstream.Write(FnAlignString(Strings.Format(Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"])), "#0.00"), "L", 10));
            tstream.Write(cPublic.BchNDW);
            tstream.WriteLine(); //'Ch10 & chN
            tstream.WriteLine("Cheques & Drafts Subject to realisation");
            tstream.WriteLine(Replicate("-", 78));
            tstream.WriteLine();
            tstream.Write("A/c : ");
            tstream.WriteLine(ds2.Tables[0].Rows[0]["HEAD"].ToString());
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine("Cashier                                              Partner   /    Accountant");
            lineno = lineno + 9;
            tstream.Flush();
            tstream.Close();
        }

        public void MultPayprintline(DataSet ds2,DataSet ds)
        {
            string strdata;
            tstream.WriteLine();
            tstream.WriteLine();
            lineno = lineno + 2;
            string cb;
            if (ds.Tables[0].Rows[0]["Cheque"].ToString() == "1")
            {
                cb = " Cheque ";
            }
            else
            {
                cb = " Cash ";
            }
            strdata = "Pay Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]));
            strdata = strdata + "/-" + cb + "as per details given below / Attached";
            tstream.WriteLine(FnAlignString(strdata, "L", 78));
            strdata = Replicate(".", 200);
            tstream.WriteLine(FnAlignString(strdata, "L", 78));
            tstream.WriteLine();
            lineno = lineno + 3;
            strdata = "Received with thanks from " + Strings.Trim(cPublic.g_firmname);
            strdata = strdata + Replicate(" .", 100);
            tstream.WriteLine(FnAlignString(strdata, "L", 78));
            tstream.WriteLine();
            lineno = lineno + 2;

            string curr;
            fssgen.fssgen gen = new fssgen.fssgen();
            double amt = (Math.Abs(Convert.ToDouble(ds.Tables[0].Rows[0]["Amount"])));
            curr = gen.NumToChars(ref amt);

            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"])) + "/- (Rupees " + Strings.Trim(curr) + ")" + Replicate(".", 200);
            string ln1;
            string ln2;
            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;

            }
            strdata = "Being " + Strings.Trim(ds.Tables[0].Rows[0]["Narration"].ToString());
            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Len(Strings.Trim(ln1)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Len(Strings.Trim(ln2)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            tstream.WriteLine();
            tstream.WriteLine();
            lineno = lineno + 2;

            if (Convert.ToInt32(ds.Tables[0].Rows[0]["BOOKCODE"].ToString()) > 200002)
            {
                tstream.Write(FnAlignString("Cheque / Draft No.:", "L", 20));
                tstream.WriteLine(FnAlignString((ds.Tables[0].Rows[0]["CHQNO"].ToString()), "L", 10));
                tstream.Write(FnAlignString("Dated             :", "L", 20));
                tstream.Write(FnAlignString(ds.Tables[0].Rows[0]["chqdate"].ToString(), "L", 10));
                lineno = lineno + 1;
            }
            tstream.WriteLine();
            lineno = lineno + 1;
            tstream.Write(cPublic.BchDW);
            tstream.Write(FnAlignString("Rs.", "L", 3));
            // tstream.Write (FnAlignString(Math.Abs(Convert.ToInt32 (ds.Tables[0].Rows[0]["Amount"].ToString())),"L",10));
            tstream.Write(FnAlignString(Strings.Format(Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"])), "#0.00"), "L", 10));
            tstream.Write(cPublic.BchNDW);
            tstream.WriteLine("Cashier                          Payee's Signature");
            tstream.WriteLine(Replicate("-", 78));
            tstream.WriteLine();
            tstream.Write("A/c : ");
            tstream.WriteLine(ds2.Tables[0].Rows[0]["Head"]);
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine("        Cashier                                Partner   /    Accountant");
            lineno = lineno + 8;
            tstream.Flush();
            tstream.Close();

        }

        public void MultJourprintline(DataSet ds2,DataSet ds,DataSet ds1)
        {
            string strdata;
            tstream.WriteLine();
            lineno = lineno + 1;


            //----Credit------------------------------------
            strdata = "Credit. " + ds1.Tables[0].Rows[0]["Head"];
            if (ds1.Tables[0].Rows[0]["Address"].ToString() == "")
            {
                strdata = strdata + ", " + ds1.Tables[0].Rows[0]["Address"];
            }
            if (ds1.Tables[0].Rows[0]["STREET"].ToString() == "")
            {
                strdata = strdata + ", " + ds1.Tables[0].Rows[0]["STREET"];
            }

            if (ds1.Tables[0].Rows[0]["CITY"].ToString() == "")
            {
                strdata = strdata + ", " + ds1.Tables[0].Rows[0]["CITY"];
            }
            strdata = strdata + Replicate(".", 250);

            string ln1;
            string ln2;
            string ln3;

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            ln3 = Strings.Mid(strdata, 157, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            if (Strings.Left(ln3, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln3, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            //---------------------------------------------------
            lineno = lineno + 1;
            strdata = "Debit. " + ds2.Tables[0].Rows[0]["Head"].ToString();
            if (ds2.Tables[0].Rows[0]["ADDRESS"].ToString() == "")
            {
                strdata = strdata + ", " + ds2.Tables[0].Rows[0]["ADDRESS"].ToString();
            }
            if (ds2.Tables[0].Rows[0]["STREET"].ToString() == "")
            {
                strdata = strdata + ", " + ds2.Tables[0].Rows[0]["STREET"].ToString();
            }

            if (ds2.Tables[0].Rows[0]["CITY"].ToString() == "")
            {
                strdata = strdata + ", " + ds2.Tables[0].Rows[0]["CITY"].ToString();
            }
            strdata = strdata + Replicate(".", 250);

            ln1 = "";
            ln2 = "";
            ln3 = "";


            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            ln3 = Strings.Mid(strdata, 157, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln3, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln3, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }




            tstream.WriteLine();
            lineno = lineno + 1;


            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]));
            strdata = strdata + "/- ( ";
            string curr;
            fssgen.fssgen gen = new fssgen.fssgen();
            double amt = Math.Abs(Convert.ToInt32(ds.Tables[0].Rows[0]["Amount"]));
            curr = (gen.NumToChars(ref amt));

            strdata = strdata + "Rupees " + Strings.Trim(curr) + ")" + Replicate(".", 200);

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]));
            strdata = strdata + "/- ( ";

            strdata = "Being " + (ds.Tables[0].Rows[0]["Narration"]);

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Len(Strings.Trim(ln1)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            if (Strings.Len(Strings.Trim(ln2)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            tstream.WriteLine();
            tstream.WriteLine();
            lineno = lineno + 2;
            if (Convert.ToInt32(ds.Tables[0].Rows[0]["BOOKCODE"].ToString()) > 200002)
            {
                tstream.Write(FnAlignString("Cheque / Draft No.:", "L", 20));
                tstream.WriteLine(FnAlignString((ds.Tables[0].Rows[0]["CHQNO"].ToString()), "L", 10));
                tstream.Write(FnAlignString("Dated             :", "L", 20));
                tstream.Write(FnAlignString(ds.Tables[0].Rows[0]["chqdate"].ToString(), "L", 10));
                lineno = lineno + 1;

            }
            tstream.WriteLine();
            lineno = lineno + 1;
            // 'Ch10 + chN +
            tstream.Write(cPublic.BchDW);
            tstream.Write(FnAlignString("Rs.", "L", 3));
            //tstream.Write (FnAlignString(Math.Abs(Convert.ToInt32(ds.Tables[0].Rows[0]["Amount"].ToString())), "L", 10));
            tstream.Write(FnAlignString(Strings.Format(Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"])), "#0.00"), "L", 10));
            tstream.Write(cPublic.BchNDW);
            tstream.WriteLine();
            tstream.WriteLine(); //'Ch10 & chN
            tstream.WriteLine("Cheques & Drafts Subject to realisation");
            tstream.WriteLine(Replicate("-", 78));
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine("Cashier                                              Partner   /    Accountant");
            lineno = lineno + 9;
            tstream.Flush();
            tstream.Close();
        }


        public string rprint(string transtype, string transno)
        {

            string strdata;
            switch (transtype)
            {
                case "R":
                    shhead = "RECEIPT VOUCHER";
                    break;
                case "P":
                    shhead = "PAYMENT VOUCHER";
                    break;
                case "D":
                    shhead = "DEBIT NOTE";
                    break;
                case "C":
                    shhead = "CREDIT NOTE";
                    break;
                case "J":
                    shhead = "JOURNAL";
                    break;

            }
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();

            int notepadpath = (Convert.ToInt32(1000 * VBMath.Rnd()) + 1);

            if (Directory.Exists(cPublic.FilePath + "\\" + notepadpath + ".txt"))
            {
                Directory.Delete(cPublic.FilePath + "\\" + notepadpath + ".txt");
            }
            string path = cPublic.FilePath + "\\" + notepadpath + ".txt";
            tstream = File.CreateText(path);

            string sql = "select * from Voucher" + cPublic.g_firmcode + " where transtype='" + transtype + "' and transno=" + transno + "";
            SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);

            da.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                MessageBox.Show("Voucher Not Found");
                notepadpath = 0;
                return string.Empty;
            }
            else
            {
                string sql2 = "select * from Accounts" + cPublic.g_firmcode + " where code='" + (ds.Tables[0].Rows[0]["CODE"].ToString()) + "'";
                SqlDataAdapter da2 = new SqlDataAdapter(sql2, cPublic.Connection);
                da2.Fill(ds2);
                if ((transtype != "R") & (transtype != "P"))
                {

                    string sql1 = "select * from Accounts" + cPublic.g_firmcode + " where code='" + (ds.Tables[0].Rows[1]["CODE"].ToString()) + "'";
                    SqlDataAdapter da1 = new SqlDataAdapter(sql1, cPublic.Connection);

                    da1.Fill(ds1);
                }
            }


            tstream.Write(cPublic.ChN);
            tstream.Write(cPublic.Ch10 + cPublic.ChN);
            tstream.WriteLine(cPublic.BchDW);
            tstream.WriteLine(FnAlignStringlarge(cPublic.g_firmname, 75));
            tstream.Write(cPublic.BchNDW);
            tstream.Write(cPublic.Ch10 + cPublic.ChN);
            tstream.Write(cPublic.Ch12);
            tstream.WriteLine(FnAlignString(cPublic.g_add1 + " " + cPublic.g_add2, "C", 78));
            tstream.WriteLine(FnAlignString(cPublic.g_add3 + " " + cPublic.g_add4, "C", 78));
            tstream.WriteLine(FnAlignString(shhead, "C", 78));
            tstream.Write(FnAlignString("VoucherNo. ", "L", 10));
            strdata = ((transno));
            tstream.Write(FnAlignString(strdata, "L", 6));
            tstream.Write(Strings.Space(44));
            tstream.WriteLine(FnAlignString("Date : " + Convert.ToDateTime(ds.Tables[0].Rows[0]["date1"]).ToString("dd/MM/yyyy"), "L", 18));
            tstream.WriteLine("");
            //lineno = lineno + 6;
            switch (transtype)
            {
                case "R":
                    rprintLine(ds,ds1,ds2);
                    break; 
                case "P":
                    vprintline(ds, ds1, ds2);
                    break; 
                case "D":
                    rprintLine(ds, ds1, ds2);
                    break; 
                case "C":
                    rprintLine(ds, ds1, ds2);
                    break; 
                case "J":
                    jprintline(ds, ds1, ds2);
                    break; 

            }

            return notepadpath.ToString();
        }

        public void rprintLine(DataSet ds,DataSet ds1,DataSet ds2)
        {
            string strdata;
            tstream.WriteLine();
            tstream.WriteLine();
            //double lineNo = lineNo + 2;

            strdata = "Received with thanks from " + (ds2.Tables[0].Rows[0]["HEAD"].ToString());
            if ((ds2.Tables[0].Rows[0]["ADDRESS"].ToString()) == "")
            {
                strdata = strdata + ", " + (ds2.Tables[0].Rows[0]["ADDRESS"].ToString());
            }
            if (ds2.Tables[0].Rows[0]["STREET"].ToString() == "")
            {
                strdata = strdata + ", " + (ds2.Tables[0].Rows[0]["STREET"].ToString());
            }

            if (ds2.Tables[0].Rows[0]["CITY"].ToString() == "")
                strdata = strdata + ", " + (ds2.Tables[0].Rows[0]["CITY"].ToString());

            strdata = strdata + Replicate(".", 250);

            string ln1;
            string ln2;
            string ln3;
            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            ln3 = Strings.Mid(strdata, 157, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln3, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln3, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"]));
            strdata = strdata + "/- ( ";
            string curr;
            double amt = Math.Abs(Convert.ToDouble(ds.Tables[0].Rows[0]["AMOUNT"]));
            fssgen.fssgen gen = new fssgen.fssgen();
            curr = gen.NumToChars(ref amt);

            strdata = strdata + "Rupees " + Strings.Trim(curr) + ")" + Replicate(".", 200);

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"]));
            strdata = strdata + "/- ( ";

            strdata = "Being " + (ds.Tables[0].Rows[0]["NARRATION"].ToString());

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Len(Strings.Trim(ln1)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Len(Strings.Trim(ln2)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            tstream.WriteLine();
            tstream.WriteLine();
            lineno = lineno + 2;
            if (Convert.ToInt32(ds.Tables[0].Rows[0]["BOOKCODE"].ToString()) > 200002)
            {
                tstream.Write(FnAlignString("Cheque / Draft No.:", "L", 20));
                tstream.WriteLine(FnAlignString((ds.Tables[0].Rows[0]["CHQNO"].ToString()), "L", 10));
                tstream.Write(FnAlignString("Dated             :", "L", 20));
                tstream.Write(FnAlignString((ds.Tables[0].Rows[0]["CHQDATE"].ToString()), "L", 10));
                lineno = lineno + 1;
            }

            tstream.WriteLine();
            lineno = lineno + 1;
            //Ch10 + chN +
            tstream.Write(cPublic.BchDW);
            tstream.Write(FnAlignString("Rs.", "L", 3));
            tstream.Write(FnAlignString(Strings.Format(Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"])), "#0.00"), "L", 10));
            tstream.Write(cPublic.BchNDW);
            tstream.WriteLine(); //'Ch10 & chN
            tstream.WriteLine("Cheques & Drafts Subject to realisation");
            tstream.WriteLine(Replicate("-", 78));
            tstream.WriteLine();
            tstream.Write("A/c : ");
            tstream.WriteLine(ds2.Tables[0].Rows[0]["HEAD"].ToString());
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine("Cashier                                              Partner   /    Accountant");
            lineno = lineno + 9;
            tstream.Flush();
            tstream.Close();
        }

        public void vprintline(DataSet ds, DataSet ds1, DataSet ds2)
        {
            string strdata;
            tstream.WriteLine();
            tstream.WriteLine();
            lineno = lineno + 2;
            string cb;
            if (ds.Tables[0].Rows[0]["Cheque"].ToString() == "1")
            {
                cb = " Cheque ";
            }
            else
            {
                cb = " Cash ";
            }
            strdata = "Pay Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]));
            strdata = strdata + "/-" + cb + "as per details given below / Attached";
            tstream.WriteLine(FnAlignString(strdata, "L", 78));
            strdata = Replicate(".", 200);
            tstream.WriteLine(FnAlignString(strdata, "L", 78));
            tstream.WriteLine();
            lineno = lineno + 3;
            strdata = "Received with thanks from " + Strings.Trim(cPublic.g_firmname);
            strdata = strdata + Replicate(" .", 100);
            tstream.WriteLine(FnAlignString(strdata, "L", 78));
            tstream.WriteLine();
            lineno = lineno + 2;

            string curr;
            fssgen.fssgen gen = new fssgen.fssgen();
            double amt = (Math.Abs(Convert.ToDouble(ds.Tables[0].Rows[0]["Amount"])));
            curr = gen.NumToChars(ref amt);

            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"])) + "/- (Rupees " + Strings.Trim(curr) + ")" + Replicate(".", 200);
            string ln1;
            string ln2;
            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;

            }
            strdata = "Being " + Strings.Trim(ds.Tables[0].Rows[0]["Narration"].ToString());
            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Len(Strings.Trim(ln1)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Len(Strings.Trim(ln2)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            tstream.WriteLine();
            tstream.WriteLine();
            lineno = lineno + 2;

            if (Convert.ToInt32(ds.Tables[0].Rows[0]["BOOKCODE"].ToString()) > 200002)
            {
                tstream.Write(FnAlignString("Cheque / Draft No.:", "L", 20));
                tstream.WriteLine(FnAlignString((ds.Tables[0].Rows[0]["CHQNO"].ToString()), "L", 10));
                tstream.Write(FnAlignString("Dated             :", "L", 20));
                tstream.Write(FnAlignString(ds.Tables[0].Rows[0]["chqdate"].ToString(), "L", 10));
                lineno = lineno + 1;
            }
            tstream.WriteLine();
            lineno = lineno + 1;
            tstream.Write(cPublic.BchDW);
            tstream.Write(FnAlignString("Rs.", "L", 3));
            // tstream.Write (FnAlignString(Math.Abs(Convert.ToInt32 (ds.Tables[0].Rows[0]["Amount"].ToString())),"L",10));
            tstream.Write(FnAlignString(Strings.Format(Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"])), "#0.00"), "L", 10));
            tstream.Write(cPublic.BchNDW);
            tstream.WriteLine("Cashier                          Payee's Signature");
            tstream.WriteLine(Replicate("-", 78));
            tstream.WriteLine();
            tstream.Write("A/c : ");
            tstream.WriteLine(ds2.Tables[0].Rows[0]["Head"]);
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine("        Cashier                                Partner   /    Accountant");
            lineno = lineno + 8;
            tstream.Flush();
            tstream.Close();

        }

        public void jprintline(DataSet ds, DataSet ds1, DataSet ds2)
        {
            string strdata;
            tstream.WriteLine();
            lineno = lineno + 1;


            //----Credit------------------------------------
            strdata = "Credit. " + ds1.Tables[0].Rows[0]["Head"];
            if (ds1.Tables[0].Rows[0]["ADDRESS"].ToString() == "")
            {
                strdata = strdata + ", " + ds1.Tables[0].Rows[0]["ADDRESS"];
            }
            if (ds1.Tables[0].Rows[0]["STREET"].ToString() == "")
            {
                strdata = strdata + ", " + ds1.Tables[0].Rows[0]["STREET"];
            }

            if (ds1.Tables[0].Rows[0]["CITY"].ToString() == "")
            {
                strdata = strdata + ", " + ds1.Tables[0].Rows[0]["CITY"];
            }
            strdata = strdata + Replicate(".", 250);

            string ln1;
            string ln2;
            string ln3;

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            ln3 = Strings.Mid(strdata, 157, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            if (Strings.Left(ln3, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln3, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            //---------------------------------------------------
            lineno = lineno + 1;
            strdata = "Debit. " + ds2.Tables[0].Rows[0]["Head"].ToString();
            if (ds2.Tables[0].Rows[0]["ADDRESS"].ToString() == "")
            {
                strdata = strdata + ", " + ds2.Tables[0].Rows[0]["ADDRESS"].ToString();
            }
            if (ds2.Tables[0].Rows[0]["STREET"].ToString() == "")
            {
                strdata = strdata + ", " + ds2.Tables[0].Rows[0]["STREET"].ToString();
            }

            if (ds2.Tables[0].Rows[0]["CITY"].ToString() == "")
            {
                strdata = strdata + ", " + ds2.Tables[0].Rows[0]["CITY"].ToString();
            }
            strdata = strdata + Replicate(".", 250);

            ln1 = "";
            ln2 = "";
            ln3 = "";

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            ln3 = Strings.Mid(strdata, 157, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln3, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln3, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            tstream.WriteLine();
            lineno = lineno + 1;


            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]));
            strdata = strdata + "/- ( ";
            string curr;
            fssgen.fssgen gen = new fssgen.fssgen();
            double amt = Math.Abs(Convert.ToInt32(ds.Tables[0].Rows[0]["Amount"]));
            curr = (gen.NumToChars(ref amt));

            strdata = strdata + "Rupees " + Strings.Trim(curr) + ")" + Replicate(".", 200);

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Left(ln1, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            if (Strings.Left(ln2, 1) != ".")
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            strdata = "the sum of Rs. " + Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["Amount"]));
            strdata = strdata + "/- ( ";

            strdata = "Being " + (ds.Tables[0].Rows[0]["Narration"]);

            ln1 = Strings.Mid(strdata, 1, 78);
            ln2 = Strings.Mid(strdata, 79, 78);
            if (Strings.Len(Strings.Trim(ln1)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln1, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }

            if (Strings.Len(Strings.Trim(ln2)) != 0)
            {
                tstream.WriteLine(FnAlignString(ln2, "L", 78));
                tstream.WriteLine();
                lineno = lineno + 2;
            }
            tstream.WriteLine();
            tstream.WriteLine();
            lineno = lineno + 2;
            if (Convert.ToInt32(ds.Tables[0].Rows[0]["BOOKCODE"].ToString()) > 200002)
            {
                tstream.Write(FnAlignString("Cheque / Draft No.:", "L", 20));
                tstream.WriteLine(FnAlignString((ds.Tables[0].Rows[0]["CHQNO"].ToString()), "L", 10));
                tstream.Write(FnAlignString("Dated             :", "L", 20));
                tstream.Write(FnAlignString(ds.Tables[0].Rows[0]["chqdate"].ToString(), "L", 10));
                lineno = lineno + 1;

            }
            tstream.WriteLine();
            lineno = lineno + 1;
            // 'Ch10 + chN +
            tstream.Write(cPublic.BchDW);
            tstream.Write(FnAlignString("Rs.", "L", 3));
            //tstream.Write (FnAlignString(Math.Abs(Convert.ToInt32(ds.Tables[0].Rows[0]["Amount"].ToString())), "L", 10));
            tstream.Write(FnAlignString(Strings.Format(Math.Abs(Convert.ToDecimal(ds.Tables[0].Rows[0]["AMOUNT"])), "#0.00"), "L", 10));
            tstream.Write(cPublic.BchNDW);
            tstream.WriteLine();
            tstream.WriteLine(); //'Ch10 & chN
            tstream.WriteLine("Cheques & Drafts Subject to realisation");
            tstream.WriteLine(Replicate("-", 78));
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine();
            tstream.WriteLine("Cashier                                              Partner   /    Accountant");
            lineno = lineno + 9;
            tstream.Flush();
            tstream.Close();

        }


        #region FindHead
        ArrayList vGroups = new ArrayList();


        public DataTable GroupDetails(string GroupCode)
        {
            vGroups.Clear();
            vGroups.Add(GroupCode);

            string sk = Groups(GroupCode);
            sk = "";
            for (int j = 0; j < vGroups.Count; j++)
            {
                if (sk.Trim() != "")
                { sk = sk + ",'" + vGroups[j].ToString() + "'"; }
                else
                { sk = "'" + vGroups[j].ToString() + "'"; }
            }
            sk = " SELECT * FROM [ACCOUNTS" + cPublic.g_firmcode + "] WHERE GROUPCODE IN (" + sk + ")";
            cmd.CommandText = sk;
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;  
            SqlDataAdapter Rn = new SqlDataAdapter(cmd);
            DataTable Sn = new DataTable();
            Rn.Fill(Sn);
            return Sn;
        }

        private string Groups(string GroupCode)
        {
            cmd.CommandText = "SELECT * FROM [GROUPS" + cPublic.g_firmcode + "] WHERE GROUPCODE='" + GroupCode + "'";
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;  
            SqlDataAdapter Rn = new SqlDataAdapter(cmd);
            DataTable Sn = new DataTable();
            Rn.Fill(Sn);
            if (Sn.Rows.Count > 0)
            {
                vGroups.Add(Sn.Rows[0]["code"].ToString());
                Groups(Sn.Rows[0]["code"].ToString());
                cPublic.GrpNos += 1;
            }
            else
            { return ""; }
            return "";
        }

        #endregion

        public void FindHeads(string Acccode)
        {
            SqlDataAdapter ap = new SqlDataAdapter();
            DataSet dt = new DataSet();
            int i = 0;
            cPublic.g_opbalance = 0;
            cPublic.G_TotBalance = 0;
            cPublic.GrpNos = 0;
            cPublic.AccNos = 0;

            cPublic.AccArray = new string[0, 0];
            cPublic.AccArray = new string[5000, 6];

            ap = new SqlDataAdapter("select * from Accounts" + cPublic.g_firmcode + " where code='" + Acccode + "'", cPublic.Connection);
            dt = new DataSet();
            ap.Fill(dt);
            if (dt.Tables[0].Rows.Count > 0)
            {
                cPublic.GrpNos = 0;
                cPublic.AccNos = 1;
                cPublic.AccArray[1, 1] = dt.Tables[0].Rows[0]["code"] + "";
                cPublic.AccArray[1, 2] = dt.Tables[0].Rows[0]["head"] + "";
                cPublic.AccArray[1, 3] = dt.Tables[0].Rows[0]["opbalance"] + "";
                cPublic.AccArray[1, 4] = dt.Tables[0].Rows[0]["BALANCE"] + "";
                cPublic.AccArray[1, 5] = dt.Tables[0].Rows[0]["groupcode"] + "";
                cPublic.G_TotBalance = Convert.ToDouble(dt.Tables[0].Rows[0]["BALANCE"]);
                cPublic.g_opbalance = Convert.ToDouble(dt.Tables[0].Rows[0]["opbalance"]);
                return;
            }
            cPublic.GrpNos = 0;
            vGroups.Clear();
            DataTable sn = GroupDetails(Acccode);
            cPublic.AccNos = 0;
            for (i = 0; i < sn.Rows.Count; i++)
            {
                cPublic.AccNos += 1;

                cPublic.AccArray[1 + i, 1] = sn.Rows[i]["code"] + "";
                cPublic.AccArray[1 + i, 2] = sn.Rows[i]["head"] + "";
                cPublic.AccArray[1 + i, 3] = sn.Rows[i]["opbalance"] + "";
                cPublic.AccArray[1 + i, 4] = sn.Rows[i]["BALANCE"] + "";
                cPublic.AccArray[1 + i, 5] = sn.Rows[i]["groupcode"] + "";
                cPublic.G_TotBalance = Convert.ToDouble(sn.Rows[i]["BALANCE"]);
                cPublic.g_opbalance = Convert.ToDouble(sn.Rows[i]["opbalance"]);
            }

        }


        public string AlignWord(string Word, string Alignment, int Length)
        {
            int Difference;
            string alignWord;

            if (Word.Length > Length)
                Word = Word.Substring(0, Length);

            Difference = Length - Word.Length;
            alignWord = Strings.Space(Length);

            if (Word.Length == 0) Difference = 0;

            switch (Alignment)
            {
                case "R":
                    alignWord = Word.PadLeft(Length, ' ');
                    break;
                case "L":
                    alignWord = Word.PadRight(Length, ' ');
                    break;
                case "C":

                    if (Difference >= 0)
                        if (Difference / 2 == 0)
                            alignWord = (Strings.Space(Difference + 1)) + Word;
                        else
                            if (Difference % 2 == 1)
                                alignWord = (Strings.Space(Difference / 2 + 1)) + Word + Strings.Space((Difference / 2 + 1));
                            else
                                alignWord = (Strings.Space(Difference / 2 + 1)) + Word + Strings.Space((Difference / 2));
                    break;
            }
            return alignWord;
        }


        public string RepackingPrint(long RptOrderno)
        {
            slNo = 1;
            int ii = 0;
            skiplines = 0;
            coun = 0;
            prtQty = 0;
            prtGv = 0;
            prtTaxamt = 0;
            prtNetamt = 0;
            prtItem = 0;
            prtDisc = 0;
            string rptsub;
            int lineperpage = 60;

            string shead = "";
            string add = "";
            string add1 = "";
            DateTime date1 = new DateTime();
            string orderno = "";

            double billamt = 0;
            string remarks1 = "";

            add = Strings.Left(cPublic.g_add1, 36);
            add1 = Strings.Mid(cPublic.g_add1, 37, 72);
            if (RptOrderno == 0)
            {
                MessageBox.Show("Order Number Not Found", cPublic.messagename);
                return "";
            }

            if (!Directory.Exists(cPublic.FilePath))
            { Directory.CreateDirectory(cPublic.FilePath); }


            // cPublic.notepadpath = (Convert.ToInt32(1000 * Math.Round(1000.00)) + 1);
            int notepadpath = (Convert.ToInt32(1000 * VBMath.Rnd()) + 1);

            if (Directory.Exists(cPublic.FilePath + "\\" + notepadpath + ".txt"))
            {
                Directory.Delete(cPublic.FilePath + "\\" + notepadpath + ".txt");
            }

            string path = cPublic.FilePath + "\\" + notepadpath + ".txt";
            cGeneral.tstream = File.CreateText(path);

            shead = "REPACKING";
            cmd = new SqlCommand("select * from manfact" + cPublic.g_firmcode + " where orderno = " + RptOrderno + " ", cPublic.Connection);
            SqlDataReader dread = cmd.ExecuteReader();
            while (dread.Read())
            {

                date1 = Convert.ToDateTime(dread["date1"].ToString());
                orderno = dread["orderno"].ToString();
                billamt = Convert.ToDouble(dread["netamt"].ToString());
                remarks1 = dread["remarks"].ToString();

                if (dread.HasRows == false)
                    MessageBox.Show("Order Not Found", cPublic.messagename);
            }
            dread.Close();
            int count = 1;
            rptsub = "mitemin" + cPublic.g_firmcode;

            while (count <= 2)
            {
                DataSet dset = new DataSet();
                cmd.CommandText = ("select *  from " + rptsub + " where orderno = " + RptOrderno + "  order by recno");
                cmd.Connection = cPublic.Connection;
                dset.Tables.Clear();
                SqlDataAdapter dad = new SqlDataAdapter(cmd);
                dad.Fill(dset);
                string strdata = "";

                if (ii != dset.Tables[0].Rows.Count)
                {
                    if (count == 1)
                    {
                        tstream.Write(cPublic.Ch10 + cPublic.ChN + cPublic.BchDW);
                        tstream.Write(FnAlignStringlarge(add, 80));
                        tstream.Write(FnAlignStringlarge(add1, 80));
                        tstream.WriteLine(cPublic.BchNDW);
                        skiplines = skiplines + 1;
                        tstream.Write(cPublic.Ch12 + cPublic.ChN);
                        tstream.WriteLine(FnAlignString(cPublic.g_add2 + "," + cPublic.g_add3 + ", Phone No:" + cPublic.g_Phone1, "C", 80));
                        //tstream.WriteLine(FnAlignString("THE KERALA VALUE ADDED TAX RULES 2006 FORM NO. 8" + form + "[See Rule 58(10)]", "C", 80));
                        skiplines = skiplines + 1;

                        if (cPublic.g_TinNo.StartsWith("T") == true) { tstream.Write(FnAlignString("TIN.:" + cPublic.g_TinNo.Substring(2), "L", 15)); }
                        else if (cPublic.g_TinNo.StartsWith("P") == true) { tstream.Write(FnAlignString("PIN.:" + cPublic.g_TinNo.Substring(2), "L", 15)); }
                        else { tstream.Write(FnAlignString("TIN.:" + cPublic.g_TinNo, "L", 15)); }

                        tstream.Write(Strings.Space(15));

                        tstream.WriteLine(FnAlignString(shead, "L", 34));
                        //tstream.Write(Strings.Space(16));

                        tstream.Write(cPublic.ChNE);
                        tstream.WriteLine(FnAlignString("CST.:", "L", 20));
                        tstream.Write(cPublic.ChE);
                        tstream.Write(FnAlignString("No.:", "L", 9));
                        //tstream.Write(FnAlignString(series, "L", 1));

                        strdata = Strings.Format(RptOrderno, "#0");
                        tstream.WriteLine(FnAlignString(strdata, "L", 6));
                        tstream.WriteLine("Date.:" + FnAlignString(date1.ToString("dd-MM-yyyy"), "L", 16));
                        skiplines = skiplines + 4;

                        tstream.WriteLine(FnAlignString("Items Used In Re Packing", "L", 30));
                        skiplines++;
                    }
                    tstream.Write(cPublic.Ch10 + cPublic.ChN);
                    tstream.WriteLine(Replicate("-", 83));
                    //tstream.WriteLine(cPublic.ChC);

                    //tstream.WriteLine(FnAlignString("|Sl.|Commodity Item                            |  Quantity| Tax %|      Price|   Gr.Value|   Discount|   Net Amt.| Tax.Amt.|      Total|", "L", 137));
                    tstream.WriteLine(FnAlignString("|Sl.| Item Code   | Item Name                               |  Quantity|     Price|", "L", 83));

                    tstream.Write(Replicate("=", 83));
                    skiplines = skiplines + 3;
                    lineno = lineno + 7;
                    RepackingLine(count, dset, lineperpage);
                    tstream.WriteLine();
                    skiplines = skiplines + 1;
                    tstream.WriteLine(Replicate("-", 83));
                    skiplines = skiplines + 1;
                    if (dset.Tables[0].Rows.Count == 0)
                        goto g;
                }
                count++;

                if (count == 2)
                {
                    rptsub = "mitemout" + cPublic.g_firmcode;
                    tstream.WriteLine();
                    tstream.WriteLine();
                    tstream.WriteLine();
                    tstream.WriteLine();
                    slNo = 1;

                    tstream.WriteLine(FnAlignString("Out Put Items of Repacking", "L", 30));
                    skiplines += 5;
                }
            }
        g:
            skiplines = skiplines + 1;

            tstream.WriteLine();
            skiplines = skiplines + 1;
            tstream.Write(FnAlignString("STORE KEEPER", "L", 43));
            skiplines = skiplines + 1;
            tstream.Write(cPublic.ChN + cPublic.ChE);
            tstream.WriteLine(FnAlignString("For " + cPublic.g_add4, "R", 160));
            tstream.Write(cPublic.ChN + cPublic.ChNE);
            skiplines = skiplines + 1;
            for (int i = skiplines; i < 72 - 1; i++)
            { tstream.WriteLine(); }
            for (int i = 1; i < Convert.ToInt64(cPublic.LinestoSkip); i++)
            { tstream.WriteLine(); }

            tstream.Flush();
            tstream.Close();


            return notepadpath.ToString();
        }


        public void RepackingLine(int count, DataSet dset, int lineperpage)
        {
            coun = 1;
            string itname = "";
            string strdata = "";
            for (int ii = 0; ii < dset.Tables[0].Rows.Count; ii++)
            {
                tstream.WriteLine();
                tstream.Write("|");
                tstream.Write(FnAlignString(Convert.ToString(slNo), "R", 3));
                slNo = slNo + 1;

                tstream.Write("|");
                tstream.Write(FnAlignString(dset.Tables[0].Rows[ii]["Itemcode"].ToString(), "L", 13));
                tstream.Write("|");
                cmd = new SqlCommand("select itemname from stockmst" + cPublic.g_firmcode + " where itemcode='" + dset.Tables[0].Rows[ii]["Itemcode"].ToString() + "'", cPublic.Connection);
                itname = cmd.ExecuteScalar() + "";
                tstream.Write(FnAlignString(itname, "L", 41));
                tstream.Write("|");
                //quantity
                strdata = Strings.Format(dset.Tables[0].Rows[ii]["qty"], "#0.000");
                tstream.Write(FnAlignString(strdata, "R", 10));
                tstream.Write("|");

                strdata = Strings.Format(Convert.ToDouble(dset.Tables[0].Rows[ii]["Price"]), "0.00");
                tstream.Write(FnAlignString(strdata, "R", 10));
                tstream.Write("|");
                //gr.value
                skiplines++;

                coun = coun + 1;

                if (skiplines >= lineperpage)
                {
                    if (ii < dset.Tables[0].Rows.Count - 1)
                    {
                        tstream.WriteLine();
                        tstream.WriteLine(Replicate("-", 83));
                        tstream.WriteLine(FnAlignString("Contd...", "R", 80));

                        for (int i = 1; i <= 5; i++)
                            tstream.WriteLine();

                        tstream.Write(cPublic.Ch10 + cPublic.ChN);
                        tstream.WriteLine(Replicate("-", 83));
                        tstream.WriteLine(FnAlignString("|Sl.| Item Code   | Item Name                               |  Quantity|     Price|", "L", 83));

                        tstream.Write(Replicate("=", 83));
                        skiplines = 4;
                    }
                }
            }
        }


        public string GetMacAddress()
        {
            string macAddr =
                         (
                             from nic in NetworkInterface.GetAllNetworkInterfaces()
                             where nic.OperationalStatus == OperationalStatus.Up
                             select nic.GetPhysicalAddress().ToString()
                         ).FirstOrDefault();

            return macAddr;
        }

        public bool RegistryKey(DateTime logindate,string MacAddress,bool isregister)
        {
            RegistryKey subKey =
            Registry.LocalMachine.OpenSubKey("Software\\" + MacAddress, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl);


            //Registry.LocalMachine.DeleteSubKeyTree("Software\\74E543594E1D");
            //Registry.LocalMachine.DeleteSubKeyTree("Software\\#LOTZINDIA");
            if (isregister)
            {
                if (!checkIfKeyExists(subKey))
                {
                    subKey = Registry.LocalMachine.CreateSubKey("Software\\" + MacAddress, RegistryKeyPermissionCheck.ReadWriteSubTree);

                    subKey.SetValue(MacAddress, logindate.ToShortDateString() + ",365");
                }
                else
                {
                    MessageBox.Show("Already Registered ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else if (!isregister)
            {
                subKey =
           Registry.LocalMachine.OpenSubKey("Software\\" + MacAddress, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl);
                if (!checkIfKeyExists(subKey))
                {
                    subKey =
                    Registry.LocalMachine.OpenSubKey("Software\\#LOTZINDIA", RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl);
                    if (!checkIfKeyExists(subKey))
                    {
                        subKey = Registry.LocalMachine.CreateSubKey("Software\\#LOTZINDIA", RegistryKeyPermissionCheck.ReadWriteSubTree);

                        subKey.SetValue("#LOTZINDIA", logindate.ToShortDateString() + "," + logindate.AddDays(30) +",30");
                    }
                    else if (checkIfKeyExists(subKey))
                    {
                        subKey =
                        Registry.LocalMachine.OpenSubKey("Software\\#LOTZINDIA", RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl);

                        if (checkIfKeyExists(subKey))
                        {

                            string[] a = subKey.GetValue("#LOTZINDIA").ToString().Split(',');
                            TimeSpan ts = Convert.ToDateTime(logindate.ToShortDateString()) - Convert.ToDateTime(a[0]);


                            if (Convert.ToDateTime(logindate.ToShortDateString()) > Convert.ToDateTime(a[1]))
                            {
                                return false;
                            }
                            else if (ts.TotalDays != 0)
                            {
                                subKey.SetValue("#LOTZINDIA", a[0] + "," + a[1] + "," + (Convert.ToInt32(a[2]) - ts.TotalDays));

                                subKey.Close();
                            }
                        }
                    }
                }
            }

            return true;
        }

        private static bool checkIfKeyExists(RegistryKey subKey)
        {
            bool status = true;
            if (subKey == null)
            {
                status = false;
            }
            return status;
        }
    }
}
