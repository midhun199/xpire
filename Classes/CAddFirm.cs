using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Net;


namespace Xpire.Classes
{
    
    public class CAddFirm
    {
        //cConnection con = new cConnection();
       

        public void FirmAdd(string strDatabaseName, string firmcode)
        {
            try
            {
                cConnection con = new cConnection();
                CSP CREATABLE = new CSP();
                con.ConnectMe(cPublic.password, cPublic.Sqluserid, strDatabaseName, cPublic.Sqlservername);
                CREATABLE.tbl_public_var(); 
                CREATABLE.Tables(firmcode);
                CREATABLE.job(strDatabaseName,firmcode);
                CREATABLE.OtherTables(firmcode);
                CREATABLE.Triggers(firmcode);
                CREATABLE.firmcreation(firmcode);
                CREATABLE.headinsert (firmcode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public Boolean CreateDB(string strDBName, string strPath)
        {
            try
            {
               
                    cConnection con = new cConnection();
                    if (!ChekDB(strDBName))
                    {
                        
                            string filename = cPublic.drive + "Maxx\\" + cPublic.g_PrjCode + "\\" + strDBName + "\\" + strDBName + "_dat.mdf";
                            if (System.IO.File.Exists(filename) == true)
                            {
                                MessageBox.Show(filename + " ,this file exsist Please Check", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return true;
                            }
                            string sql = "create database " + strDBName + " on (name=" + strDBName + ",filename='" + filename + "')";
                            SqlCommand comm = new SqlCommand();
                            comm = new SqlCommand(sql, cPublic.Connection);
                            comm.ExecuteNonQuery();
                            return false;
                       

                    }
                    else
                    {
                        return true;
                    }
                
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,cPublic.messagename,MessageBoxButtons.OK,MessageBoxIcon.Warning   );
                return true;
            }
        }

        public Boolean ChekDB(string DBname)
        {
            try
            {
                cConnection con = new cConnection();
                cGeneral gen = new cGeneral();
                gen.ServerDet();
                con.ConnectMe(cPublic.password, cPublic.Sqluserid, "master", cPublic.Sqlservername);
                string sql = "select * from sysdatabases where name='" + DBname + "'";
                SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
                SqlDataReader dr = comm.ExecuteReader();
                if (dr.Read())
                {
                    dr.Close();
                    return true;

                }
                else
                {
                    dr.Close();
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }

        }
    }
}
