using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;  
using System.Data.OleDb;
using System.IO;
using C1.Win.C1FlexGrid;
using System.Reflection;
using System.Runtime.InteropServices;
namespace Xpire.Classes
{
    internal class DataExport
    {
        SqlCommand cmd = new SqlCommand("", cPublic.Connection);

        public enum type
        { stockprice, muliunitprice, stockmaster, accountmaster }

        public void flex_Excel(C1FlexGrid tbs)
        {
            SaveFileDialog ass = new SaveFileDialog();
            ass.Filter = "Microsoft Office Excel WorkBook (*.xls)|*.xls";
            ass.ShowDialog();
            if (ass.FileName != "")
            {
                StreamWriter Sn = new StreamWriter(ass.FileName);
                for (int jj = 0; jj < tbs.Rows.Count; jj++)
                {
                    if (jj > 0) { Sn.Write("\n"); }

                    for (int ii = 1; ii <= tbs.Cols.Count - 1; ii++)
                    {
                        if (tbs.Cols[ii].Visible && tbs.Cols[ii].Name.Trim().ToUpper() != "SELECT")
                        {
                            string str = tbs.GetData(jj, ii) + "".ToString();
                            str = str.Replace("\n", "");
                            str = str.Replace("\t", "");
                            Sn.Write(str + "\t");
                        }
                    }
                }
                Sn.Close();
            }
        }

    }
}
