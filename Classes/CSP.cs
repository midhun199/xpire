
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Smo.Agent;
using Microsoft.SqlServer.Management.Smo.Mail;

namespace Xpire.Classes
{
    public class CSP
    {
        cConnection con = new cConnection();
        public void tbl_public_var()
        {
            string sql = string.Empty;
            SqlCommand comm;

            sql = "if exists (select * from dbo.sysobjects where id = "
            + "object_id(N'[dbo].[PUBLIC_VAR]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            + "drop table [dbo].[PUBLIC_VAR]"
            + "CREATE TABLE [dbo].[PUBLIC_VAR] ("
            + "[var_name]   [varchar] (80) NULL ,"
            + "[var_values] [varchar] (120) NULL,"
            + "[PASS]      [varchar](1) NULL"
            + " ) ON [PRIMARY]";

            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "insert into PUBLIC_VAR(var_name,var_values) values('BillType','Cash')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('PriceType','Retail')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('FormType','A')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();            
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('RoundOff','Rupees')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('Referable Bills','Yes')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('PrintType','Plain')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('BillSize','FullPage')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "insert into PUBLIC_VAR(var_name,var_values) values('AutoQty','Yes')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('ZeroQtySave','Yes')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('Modification Date','No')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('TaxIncludePrice','Yes')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "insert into PUBLIC_VAR(var_name,var_values) values('LookupDefault','Itemcode')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('Lines to Skip','5')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('Lines to Reverse','5')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();    


            sql = "insert into PUBLIC_VAR(var_name,var_values) values('Bill Printer','')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "insert into PUBLIC_VAR(var_name,var_values) values('Slip Printer','')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "insert into PUBLIC_VAR(var_name,var_values) values('PrintMode','DOS')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            sql = "insert into PUBLIC_VAR(var_name,var_values) values('AutoPriceUpdation','No')";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LOOKUP]')"
             + "and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[LOOKUP]"
             + "CREATE TABLE [dbo].[LOOKUP] ("
             + "[Field1]     [varchar] (15) NULL ,"
             + "[Code]       [varchar] (100) NULL ,"
             + "[Details]    [varchar] (100) NULL ,"
             + "[check]      [bit] null,"
             + "[PASS]      [varchar](1) NULL,"
             + "[USERID]     [varchar] (150) NULL ) ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();



            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'ACCCODE', N'U', N'Unregistered', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'TAXPER', N'0.00', N'0% Taxable', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'TAXPER', N'1.00', N'1% Taxable', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'TAXPER', N'12.50', N'12.5% Taxable', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'TAXPER', N'4.00', N'4% Taxable', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'CESSPER', N'0.00', N'0% Taxable', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'CESSPER', N'1.00', N'1% Taxable', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'MANUFACTURER', N'1', N'1', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'PRODUCT', N'1', N'1', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'GROUP', N'1', N'1', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'UNIT', N'NOS', N'NOS', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DOCUMENT TYPE', N'CL', N'COMMERCIAL LICENSE', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DOCUMENT TYPE', N'EI', N'EMIRATES ID', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DOCUMENT TYPE', N'LC', N'LABOUR CARD', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DOCUMENT TYPE', N'PP', N'PASSPORT', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DOCUMENT TYPE', N'MLK', N'MULKIYA', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DOCUMENT TYPE', N'INS', N'INSURANCE', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DEPARTMENT', N'HR', N'H R', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'QUALIFICATION', N'BC', N'B.COM', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'SA', N'SALES', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'LEAVE TYPE', N'AN', N'ANNUAL', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DEPARTMENT', N'ACCOUNTS', N'ACCOUNTS', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Afghanistan', N'Afghanistan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Albania', N'Albania', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Algeria', N'Algeria', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'QUALIFICATION', N'GN', N'GENERAL', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Helper', N'Helper', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'RELIGION', N'HND', N'HINDU', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'RELIGION', N'MLM', N'MUSLIM', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'QUALIFICATION', N'ITI', N'I T I  CIVIL', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'QUALIFICATION', N'DPLA', N'DIPLOMA', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'QUALIFICATION', N'BA', N'BA', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'QUALIFICATION', N'BTECH', N'B TECH', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'QUALIFICATION', N'MC', N'Mcom', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'QUALIFICATION', N'BBA', N'BBA', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'QUALIFICATION', N'ITC', N'ITC', NULL, NULL, N'SYSTEM')";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'SITE ENGINEER', N'SITE ENGINEER', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Andorra', N'Andorra', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Angola', N'Angola', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Antigua and Barbuda', N'Antigua and Barbuda', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Argentina', N'Argentina', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Armenia', N'Armenia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Australia', N'Australia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Austria', N'Austria', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Azerbaijan', N'Azerbaijan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Bahamas', N'Bahamas', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Bahrain', N'Bahrain', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Bangladesh', N'Bangladesh', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Barbados', N'Barbados', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Belarus', N'Belarus', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Belgium', N'Belgium', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Belize', N'Belize', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Benin', N'Benin', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Bhutan', N'Bhutan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Bolivia', N'Bolivia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Bosnia and Herzegovina', N'Bosnia and Herzegovina', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Botswana', N'Botswana', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Brazil', N'Brazil', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Brunei Darussalam', N'Brunei Darussalam', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Bulgaria', N'Bulgaria', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Burkina Faso', N'Burkina Faso', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Burundi', N'Burundi', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Cabo Verde', N'Cabo Verde', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Cambodia', N'Cambodia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Cameroon', N'Cameroon', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Canada', N'Canada', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Central African Republic', N'Central African Republic', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Chad', N'Chad', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Chile', N'Chile', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'China', N'China', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Colombia', N'Colombia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Comoros', N'Comoros', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Congo', N'Congo', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Costa Rica', N'Costa Rica', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'C�te d''Ivoire', N'C�te d''Ivoire', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Croatia', N'Croatia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Cuba', N'Cuba', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Cyprus', N'Cyprus', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Czech Republic', N'Czech Republic', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Democratic People''s Republic of Korea (North Korea)', N'Democratic People''s Republic of Korea (North Korea)', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Democratic Republic of the Cong', N'Democratic Republic of the Cong', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Denmark', N'Denmark', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Djibouti', N'Djibouti', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Dominica', N'Dominica', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Dominican Republic', N'Dominican Republic', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Ecuador', N'Ecuador', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Egypt', N'Egypt', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'El Salvador', N'El Salvador', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Equatorial Guinea', N'Equatorial Guinea', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Eritrea', N'Eritrea', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Estonia', N'Estonia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Ethiopia', N'Ethiopia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Fiji', N'Fiji', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Finland', N'Finland', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'France', N'France', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Gabon', N'Gabon', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Gambia', N'Gambia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Georgia', N'Georgia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Germany', N'Germany', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Ghana', N'Ghana', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Greece', N'Greece', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Grenada', N'Grenada', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Guatemala', N'Guatemala', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Guinea', N'Guinea', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Guinea-Bissau', N'Guinea-Bissau', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Guyana', N'Guyana', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Haiti', N'Haiti', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Honduras', N'Honduras', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Hungary', N'Hungary', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Iceland', N'Iceland', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'India', N'India', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Indonesia', N'Indonesia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Iran', N'Iran', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Iraq', N'Iraq', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Ireland', N'Ireland', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Israel', N'Israel', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Italy', N'Italy', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Jamaica', N'Jamaica', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Japan', N'Japan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Jordan', N'Jordan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Kazakhstan', N'Kazakhstan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Kenya', N'Kenya', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Kiribati', N'Kiribati', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Kuwait', N'Kuwait', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Kyrgyzstan', N'Kyrgyzstan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Lao People''s Democratic Republic (Laos)', N'Lao People''s Democratic Republic (Laos)', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Latvia', N'Latvia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Lebanon', N'Lebanon', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Lesotho', N'Lesotho', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Liberia', N'Liberia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Libya', N'Libya', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Liechtenstein', N'Liechtenstein', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Lithuania', N'Lithuania', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Luxembourg', N'Luxembourg', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Macedonia', N'Macedonia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Madagascar', N'Madagascar', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Malawi', N'Malawi', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Malaysia', N'Malaysia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Maldives', N'Maldives', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Mali', N'Mali', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Malta', N'Malta', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Marshall Islands', N'Marshall Islands', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Mauritania', N'Mauritania', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Mauritius', N'Mauritius', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Mexico', N'Mexico', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Micronesia (Federated States of)', N'Micronesia (Federated States of)', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Monaco', N'Monaco', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Mongolia', N'Mongolia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Montenegro', N'Montenegro', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Morocco', N'Morocco', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Mozambique', N'Mozambique', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Myanmar', N'Myanmar', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Namibia', N'Namibia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Nauru', N'Nauru', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Nepal', N'Nepal', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Netherlands', N'Netherlands', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'New Zealand', N'New Zealand', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Nicaragua', N'Nicaragua', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Niger', N'Niger', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Nigeria', N'Nigeria', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Norway', N'Norway', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Oman', N'Oman', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Pakistan', N'Pakistan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Palau', N'Palau', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Panama', N'Panama', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Papua New Guinea', N'Papua New Guinea', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Paraguay', N'Paraguay', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Peru', N'Peru', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Philippines', N'Philippines', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Poland', N'Poland', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Portugal', N'Portugal', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Qatar', N'Qatar', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Republic of Korea (South Korea)', N'Republic of Korea (South Korea)', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Republic of Moldova', N'Republic of Moldova', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Romania', N'Romania', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Russian Federation', N'Russian Federation', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Rwanda', N'Rwanda', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Saint Kitts and Nevis', N'Saint Kitts and Nevis', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Saint Lucia', N'Saint Lucia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Saint Vincent and the Grenadines', N'Saint Vincent and the Grenadines', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Samoa', N'Samoa', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'San Marino', N'San Marino', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Sao Tome and Principe', N'Sao Tome and Principe', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Saudi Arabia', N'Saudi Arabia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Senegal', N'Senegal', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Serbia', N'Serbia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Seychelles', N'Seychelles', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Sierra Leone', N'Sierra Leone', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Singapore', N'Singapore', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Slovakia', N'Slovakia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Slovenia', N'Slovenia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Solomon Islands', N'Solomon Islands', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Somalia', N'Somalia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'South Africa', N'South Africa', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'South Sudan', N'South Sudan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Spain', N'Spain', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Sri Lanka', N'Sri Lanka', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Sudan', N'Sudan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Suriname', N'Suriname', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Papua New Guinean', N'Papua New Guinean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Paraguayan', N'Paraguayan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Peruvian', N'Peruvian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Polish', N'Polish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Portuguese', N'Portuguese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Qatari', N'Qatari', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Romanian', N'Romanian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Russian', N'Russian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Rwandan', N'Rwandan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Saint Lucian', N'Saint Lucian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Salvadoran', N'Salvadoran', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Samoan', N'Samoan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'San Marinese', N'San Marinese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Sao Tomean', N'Sao Tomean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Saudi', N'Saudi', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Scottish', N'Scottish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Senegalese', N'Senegalese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Serbian', N'Serbian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Seychellois', N'Seychellois', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Sierra Leonean', N'Sierra Leonean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Singaporean', N'Singaporean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Swaziland', N'Swaziland', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Sweden', N'Sweden', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Switzerland', N'Switzerland', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Syrian Arab Republic', N'Syrian Arab Republic', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Tajikistan', N'Tajikistan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Thailand', N'Thailand', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Timor-Leste', N'Timor-Leste', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Togo', N'Togo', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Tonga', N'Tonga', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Trinidad and Tobago', N'Trinidad and Tobago', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Tunisia', N'Tunisia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Turkey', N'Turkey', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Turkmenistan', N'Turkmenistan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Tuvalu', N'Tuvalu', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Uganda', N'Uganda', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Ukraine', N'Ukraine', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'United Arab Emirates', N'United Arab Emirates', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'United Kingdom of Great Britain and Northern Ireland', N'United Kingdom of Great Britain and Northern Ireland', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'United Republic of Tanzania', N'United Republic of Tanzania', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'United States of America', N'United States of America', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Uruguay', N'Uruguay', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Uzbekistan', N'Uzbekistan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Vanuatu', N'Vanuatu', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Venezuela', N'Venezuela', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Vietnam', N'Vietnam', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Yemen', N'Yemen', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Zambia', N'Zambia', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Country', N'Zimbabwe', N'Zimbabwe', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Afghan', N'Afghan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Albanian', N'Albanian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Algerian', N'Algerian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'American', N'American', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Andorran', N'Andorran', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Angolan', N'Angolan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Antiguans', N'Antiguans', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Argentinean', N'Argentinean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Armenian', N'Armenian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Australian', N'Australian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Austrian', N'Austrian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Azerbaijani', N'Azerbaijani', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Bahamian', N'Bahamian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Bahraini', N'Bahraini', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Bangladeshi', N'Bangladeshi', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Barbadian', N'Barbadian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Barbudans', N'Barbudans', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Batswana', N'Batswana', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Belarusian', N'Belarusian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Belgian', N'Belgian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Belizean', N'Belizean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Beninese', N'Beninese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Bhutanese', N'Bhutanese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Bolivian', N'Bolivian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Bosnian', N'Bosnian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Brazilian', N'Brazilian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'British', N'British', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Bruneian', N'Bruneian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Bulgarian', N'Bulgarian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Burkinabe', N'Burkinabe', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Burmese', N'Burmese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Burundian', N'Burundian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Cambodian', N'Cambodian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Cameroonian', N'Cameroonian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Canadian', N'Canadian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Cape Verdean', N'Cape Verdean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Central African', N'Central African', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Chadian', N'Chadian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Chilean', N'Chilean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Chinese', N'Chinese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Colombian', N'Colombian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Comoran', N'Comoran', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Congolese', N'Congolese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Congolese', N'Congolese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Costa Rican', N'Costa Rican', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Croatian', N'Croatian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Cuban', N'Cuban', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Cypriot', N'Cypriot', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Czech', N'Czech', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Danish', N'Danish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Djibouti', N'Djibouti', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Dominican', N'Dominican', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Dominican', N'Dominican', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Dutch', N'Dutch', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Dutchman', N'Dutchman', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Dutchwoman', N'Dutchwoman', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'East Timorese', N'East Timorese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Ecuadorean', N'Ecuadorean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Egyptian', N'Egyptian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Emirian', N'Emirian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Equatorial Guinean', N'Equatorial Guinean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Eritrean', N'Eritrean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Estonian', N'Estonian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Ethiopian', N'Ethiopian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Fijian', N'Fijian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Filipino', N'Filipino', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Finnish', N'Finnish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'French', N'French', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Gabonese', N'Gabonese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Gambian', N'Gambian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Georgian', N'Georgian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'German', N'German', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Ghanaian', N'Ghanaian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Greek', N'Greek', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Grenadian', N'Grenadian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Guatemalan', N'Guatemalan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Guinea-Bissauan', N'Guinea-Bissauan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Guinean', N'Guinean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Guyanese', N'Guyanese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Haitian', N'Haitian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Herzegovinian', N'Herzegovinian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Honduran', N'Honduran', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Hungarian', N'Hungarian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'I-Kiribati', N'I-Kiribati', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Icelander', N'Icelander', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Indian', N'Indian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Indonesian', N'Indonesian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Iranian', N'Iranian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Iraqi', N'Iraqi', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Irish', N'Irish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Irish', N'Irish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Israeli', N'Israeli', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Italian', N'Italian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Ivorian', N'Ivorian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Jamaican', N'Jamaican', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Japanese', N'Japanese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Jordanian', N'Jordanian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Kazakhstani', N'Kazakhstani', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Kenyan', N'Kenyan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Kittian and Nevisian', N'Kittian and Nevisian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Kuwaiti', N'Kuwaiti', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Kyrgyz', N'Kyrgyz', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Laotian', N'Laotian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Latvian', N'Latvian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Lebanese', N'Lebanese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Liberian', N'Liberian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Libyan', N'Libyan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Liechtensteiner', N'Liechtensteiner', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Lithuanian', N'Lithuanian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Luxembourger', N'Luxembourger', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Macedonian', N'Macedonian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Malagasy', N'Malagasy', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Malawian', N'Malawian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Malaysian', N'Malaysian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Maldivan', N'Maldivan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Malian', N'Malian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Maltese', N'Maltese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Marshallese', N'Marshallese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Mauritanian', N'Mauritanian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Mauritian', N'Mauritian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Mexican', N'Mexican', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Micronesian', N'Micronesian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Moldovan', N'Moldovan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Monacan', N'Monacan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Mongolian', N'Mongolian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Moroccan', N'Moroccan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Mosotho', N'Mosotho', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Motswana', N'Motswana', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Mozambican', N'Mozambican', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Namibian', N'Namibian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Nauruan', N'Nauruan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Nepalese', N'Nepalese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Netherlander', N'Netherlander', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'New Zealander', N'New Zealander', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Ni-Vanuatu', N'Ni-Vanuatu', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Nicaraguan', N'Nicaraguan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Nigerian', N'Nigerian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Nigerien', N'Nigerien', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'North Korean', N'North Korean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Northern Irish', N'Northern Irish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Norwegian', N'Norwegian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Omani', N'Omani', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Pakistani', N'Pakistani', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Palauan', N'Palauan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Panamanian', N'Panamanian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Slovakian', N'Slovakian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Slovenian', N'Slovenian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Solomon Islander', N'Solomon Islander', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Somali', N'Somali', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'South African', N'South African', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'South Korean', N'South Korean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Spanish', N'Spanish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Sri Lankan', N'Sri Lankan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Sudanese', N'Sudanese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Surinamer', N'Surinamer', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Swazi', N'Swazi', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Swedish', N'Swedish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Swiss', N'Swiss', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Syrian', N'Syrian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Taiwanese', N'Taiwanese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Tajik', N'Tajik', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Tanzanian', N'Tanzanian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Thai', N'Thai', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Togolese', N'Togolese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Tongan', N'Tongan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Trinidadian or Tobagonian', N'Trinidadian or Tobagonian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Tunisian', N'Tunisian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Turkish', N'Turkish', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Tuvaluan', N'Tuvaluan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Ugandan', N'Ugandan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Ukrainian', N'Ukrainian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Uruguayan', N'Uruguayan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Uzbekistani', N'Uzbekistani', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Venezuelan', N'Venezuelan', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Vietnamese', N'Vietnamese', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Welsh', N'Welsh', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Welsh', N'Welsh', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Yemenite', N'Yemenite', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Zambian', N'Zambian', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'Nationality', N'Zimbabwean', N'Zimbabwean', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Carpenter', N'Carpenter', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Accountant', N'Accountant', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Electrician', N'Electrician', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Driver  ', N'Driver  ', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Site suprervisor', N'Site suprervisor', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'ELECTRICAL ENGINEER', N'ELECTRICAL ENGINEER', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Painter', N'Painter', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Plumbing site supervisor', N'Plumbing site supervisor', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Helper general', N'Helper general', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Mason', N'Mason', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Welder', N'Welder', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Plumber', N'Plumber', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Accomodation labour camp incharge', N'Accomodation labour camp incharge', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'DIRECTOR', N'DIRECTOR', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Store incharge', N'Store incharge', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'ACCOUNTS MANAGER', N'ACCOUNTS MANAGER', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Procurement', N'Procurement', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'AC Technician', N'AC Technician', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = " INSERT [dbo].[LOOKUP] ([Field1], [Code], [Details], [check], [PASS], [USERID]) VALUES (N'DESIGNATION', N'Foreman', N'Foreman', NULL, NULL, NULL)";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();

        }

        public void CreateMail()
        {
            string sql = string.Empty;
            SqlCommand comm;

            sql = " IF NOT EXISTS(SELECT * FROM msdb.dbo.sysmail_account WHERE  name = 'New Account') \n"
                   + "BEGIN \n"
                       + "EXECUTE msdb.dbo.sysmail_add_account_sp \n"
                                + "@account_name = 'New Account', \n"
                                + "@description = 'Mail account for use by all database users.', \n"
                                + "@email_address = 'midhun@foursquareuae.ae', \n"
                                + "@replyto_address = 'midhun@foursquareuae.ae', \n"
                                + "@display_name = 'ADMIN', \n"
                                + "@mailserver_name = 'mailv.emirates.net.ae', \n"
                                + "@port =  25, \n"
                                + "@username = 'midhun@foursquareuae.ae', \n"
                                + "@password ='midhun'; \n"
                             + "END";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            //-- Create a Database Mail profile

            sql = "IF NOT EXISTS(SELECT * FROM msdb.dbo.sysmail_profile WHERE  name = 'My Profile') \n"
                  + "BEGIN \n"
                      + "EXECUTE msdb.dbo.sysmail_add_profile_sp \n"
                               + "@profile_name = 'My Profile', \n"
                               + "@description = 'Profile used for administrative mail.' ; \n"  
                               + "END";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            //-- Add the account to the profile

            sql = "IF NOT EXISTS(SELECT * FROM msdb.dbo.sysmail_profileaccount pa \n"
                + "INNER JOIN msdb.dbo.sysmail_profile p ON pa.profile_id = p.profile_id \n"
                + "INNER JOIN msdb.dbo.sysmail_account a ON pa.account_id = a.account_id  \n"
              + "WHERE p.name = 'My Profile' \n"
                + "AND a.name = 'New Account')  \n"
                       + "BEGIN \n"
                            + "EXECUTE msdb.dbo.sysmail_add_profileaccount_sp \n"
                                + "@profile_name = 'My Profile', \n"
                                + "@account_name = 'New Account', \n"
                                + "@sequence_number =1 ; \n"
                               + "END";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
            //-- Grant access to the profile to all users in the msdb database
            
            //sql = "DECLARE @ProfileID INT \n"
            //     + "SELECT @ProfileID = profile_id FROM msdb.dbo.sysmail_profileaccount pa \n"
            //     + "JOIN msdb.dbo.sysmail_account a ON a.account_id=pa.account_id \n"
            //     + "WHERE name = 'My Profile' \n"
            //     + "IF @ProfileID IS NULL  \n"
               //  + "IF NOT EXISTS (SELECT * FROM msdb.dbo.sysmail_principalprofile \n"
               //+ "WHERE is_default = 1 AND principal_sid = 0 \n"
               //+ "AND profile_id = @ProfileID) \n"

            sql = "IF NOT EXISTS(SELECT * FROM msdb.dbo.sysmail_profileaccount pa \n"
              + "INNER JOIN msdb.dbo.sysmail_profile p ON pa.profile_id = p.profile_id \n"
              + "INNER JOIN msdb.dbo.sysmail_principalprofile a ON pa.profile_id= a.profile_id  \n"
            + "WHERE p.name = 'My Profile') \n"
                + "BEGIN \n"
                     + "EXECUTE msdb.dbo.sysmail_add_principalprofile_sp \n"
                        + "@profile_name = 'My Profile', \n"
                        + "@principal_name = 'public', \n"
                        + "@is_default = 1 ; \n"
                + "END";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
        }

        public void job(string databasename,string FrmCode)
        {
            //CreateMail();
            ////Connect to the local, default instance of SQL Server.
            //Server srv = default(Server);
            //srv = new Server();
            ////Define the Database Mail service with a SqlMail object variable 
            ////and reference it using the Server Mail property. 
            //SqlMail sm;
            //sm = srv.Mail;
            ////Define and create a mail account by supplying the Database Mail
            ////service, name, description, display name, and email address
            ////arguments in the constructor. 
            //MailAccount a = default(MailAccount);
            //a = new MailAccount(sm, "AdventureWorks2008R2 Administrator", "AdventureWorks2008R2 Automated Mailer", "Mail account for administrative e-mail.", "dba@Adventure-Works.com");
            //a.Create(); 



            try
            {

                string steps = "EXECUTE msdb.dbo.sysmail_configure_sp 'MaxFileSize', '10000000'; \n"

                + "DECLARE @name VARCHAR(50), \n"
                + "@expdate datetime, \n"
                + "@tempdate datetime, \n"
                + "@DocumentName VARCHAR(50), \n"
                + "@attachment NVARCHAR(1000), \n"
                + "@email VARCHAR(max), \n"
                + "@priorday numeric(18,0), \n"
                + "@id VARCHAR(50) \n"


                + "DECLARE @body NVARCHAR(1000) \n"
                + "USE [" + databasename + "] \n"
                + "DECLARE C1 CURSOR READ_ONLY \n"
                + "FOR \n"
                + "SELECT a.[Name],a.[ExpDate],b.[Details],a.[Attachment], a.Email, c.[Priorday],a.id from [dbo].[Details" + FrmCode + "] a join [dbo].[LOOKUP] b  \n"
                + "on a.[DocumentType]=b.[Code] left outer join [dbo].[Prior0001] c on c.[detid]=a.id where c.sent=0 \n"


                + "OPEN C1 \n"
                + "FETCH NEXT FROM C1 INTO \n"
                + "@name, @expdate, @DocumentName, @attachment,@email,@priorday,@id \n"
                + "WHILE @@FETCH_STATUS = 0 \n"
                + "BEGIN \n"
                + "set @tempdate=@expdate-@priorday \n"

                      + "IF DATEPART(DAY,@tempdate) = DATEPART(DAY,GETDATE()) \n"
                      + "AND DATEPART(MONTH,@tempdate) = DATEPART(MONTH,GETDATE()) \n"
                      + "AND DATEPART(YEAR,@tempdate) = DATEPART(YEAR,GETDATE()) \n"
                      + "BEGIN \n"
                      + "update [dbo].[Prior" + FrmCode + "] set sent='1' where Priorday=@priorday and detid=@id \n"
                            + "SET @body = 'Dear All,'+ CHAR(13) + CHAR(13) +'Greetings And Good Day!!!'+ CHAR(13) + CHAR(13) +'This is a gentle reminder to inform you that '+@DocumentName+' of ' + @name +' is going to expire on '+CONVERT(VARCHAR(11),@expdate,106)+'.'+ CHAR(13) + CHAR(13) +'Kindly renew the documnet asap.'+ CHAR(13) + CHAR(13) +'Regards,'+ CHAR(13) + CHAR(13) +'ADMIN' \n"



                            + "USE msdb  \n"
                            + "EXEC sp_send_dbmail  \n"
                    + "@profile_name = 'My Profile', \n"
                    + "@recipients = @email,  \n"
                    + "@subject = 'ATTENTION DOCUMENT EXPIRY',  \n"
                    + "@body=@body , \n"
                    + "@file_attachments=@attachment   \n"
                       + "END \n"

                      + "FETCH NEXT FROM C1 INTO \n"
                      + "@name, @expdate, @DocumentName, @attachment,@email,@priorday,@id \n"
                + "END \n"
                + "CLOSE C1 \n"
                + "DEALLOCATE C1";




                Server server = new Server(cPublic.Servermachinename);

                // Get instance of SQL Agent SMO object
                JobServer jobServer = server.JobServer;


                Job job = null;
                JobStep step = null;
                JobSchedule schedule = null;

                // Create a schedule
                schedule = new JobSchedule(jobServer, "Schedule" + databasename + "");
                schedule.FrequencyTypes = FrequencyTypes.Daily;
                schedule.FrequencySubDayTypes = FrequencySubDayTypes.Minute;
                schedule.FrequencySubDayInterval = 60;
                TimeSpan ts1 = new TimeSpan(0, 0, 0);
                schedule.ActiveStartTimeOfDay = ts1;

                TimeSpan ts2 = new TimeSpan(24, 0, 0);
                schedule.ActiveEndTimeOfDay = ts2;
                schedule.FrequencyInterval = 1;

                schedule.ActiveStartDate = DateTime.Today;

                schedule.Create();

                // Create Job
                job = new Job(jobServer, "Job" + databasename + "");
                job.Create();
                job.AddSharedSchedule(schedule.ID);
                job.ApplyToTargetServer(server.Name);

                // Create JobStep
                step = new JobStep(job, "Step_1");
                step.Command = steps;
                step.SubSystem = AgentSubSystem.TransactSql;
                step.Create();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void Tables(string FrmCode)
        {



            string sql = string.Empty;
            SqlCommand comm;




            sql = "CREATE TABLE [dbo].[admin_login]"
            + "("
               + "[id] [int] IDENTITY(1,1) NOT NULL,"
               + "[username] [varchar](50) NULL,"
               + "[password] [varchar](50) NULL,"
            + "CONSTRAINT [PK_admin_login] PRIMARY KEY CLUSTERED "
           + "("
               + "[id] ASC"
           + ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]"
           + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE TABLE [dbo].[Details" + FrmCode + "]"
            + "("
               + "[id] [varchar](50) NULL,"
               + "[Name] [varchar](50) NULL,"
               + "[IssDate] [datetime] NULL,"
               + "[ExpDate] [datetime] NULL,"
               + "[DocumentType] [varchar](50) NULL,"
               + "[DocumentNo] [varchar](50) NULL,"
               + "[PlaceofIss] [varchar](max) NULL,"
               + "[Email] [varchar](max) NULL,"
               + "[Attachment] [varchar](max) NULL,"
               + "[userid] [varchar](15) NULL"
           + ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE TABLE [dbo].[Employee" + FrmCode + "]"
            + "("
               + "[id] [numeric](18, 0) NULL,"
               + "[DateOfJoining] [datetime] NULL,"
               + "[DateOfArrival] [datetime] NULL,"
               + "[StaffID] [varchar](50) NULL,"
               + "[StaffName] [varchar](100) NULL,"
               + "[Grade][varchar](50) NULL,"
               + "[Department] [varchar](50) NULL,"
               + "[Qualification] [varchar](50) NULL,"
               + "[Designation] [varchar](50) NULL,"
               + "[Basic] [numeric](18, 0) NULL,"
               + "[Allowance] [numeric](18, 0) NULL,"
               + "[DrivingLicence] [varchar](50) NULL,"
                + "[DrivingLicenceNo] [varchar](50) NULL,"
               + "[PrevEmployer] [varchar](50) NULL,"
               + "[YearOfExperience] [numeric](18, 0) NULL,"
               + "[PhoneNo] [varchar](50) NULL,"
               + "[MobileNo] [varchar](50) NULL,"
               + "[Email] [varchar](50) NULL,"
               + "[DOB] [datetime] NULL,"
               + "[Gender] [varchar](50) NULL,"
               + "[Marital] [varchar](50) NULL,"
               + "[Spouse] [varchar](50) NULL,"
               + "[Nationality] [varchar](100) NULL,"
               + "[Religion] [varchar](100) NULL,"
               + "[Dependent] [numeric](18, 0) NULL,"
               + "[FatherName] [varchar](100) NULL,"
               + "[MotherName] [varchar](100) NULL,"
               + "[NName] [varchar](100) NULL,"
               + "[NRelation] [varchar](100) NULL,"
               + "[NPhoneNo] [varchar](50) NULL,"
               + "[NMobileNo] [varchar](50) NULL,"
               + "[NEmail] [varchar](50) NULL,"
               + "[NPermanentAddress1][varchar](max) NULL,"

                + "[NPermanentAddress2] [varchar](max) NULL,"
                + "[NPermanentAddress3] [varchar](max) NULL,"
                + "[NPermanentAddress4] [varchar](max) NULL,"
                + "[NTemporaryAddress1] [varchar](max) NULL,"
                + "[NTemporaryAddress2] [varchar](max) NULL,"
                + "[NTemporaryAddress3] [varchar](max) NULL,"
                + "[NTemporaryAddress4] [varchar](max) NULL,"
               + "[DName] [varchar](100) NULL,"
               + "[DRelation] [varchar](100) NULL,"
               + "[DPhoneNo] [varchar](50) NULL,"
               + "[DMobileNo] [varchar](50) NULL,"
               + "[DEmail] [varchar](50) NULL,"
               + "[DPermanentAddress1] [varchar](max) NULL,"
               + "[DPermanentAddress2] [varchar](max) NULL,"
               + "[DPermanentAddress3] [varchar](max) NULL,"
               + "[DPermanentAddress4] [varchar](max) NULL,"
               + "[DTemporaryAddress1] [varchar](max) NULL,"
                + "[DTemporaryAddress2] [varchar](max) NULL,"
                 + "[DTemporaryAddress3] [varchar](max) NULL,"
                  + "[DTemporaryAddress4] [varchar](max) NULL,"
               + "[Picture] [image] NULL,"
               + "[Cancel] [bit] NULL,"
               +"[Remarks][varchar](max) NULL"
          + ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE TABLE [dbo].[EmployeeDocument" + FrmCode + "]"
            + "("
               + "[id] [varchar](50) NULL,"
               + "[staffid] [varchar](50) NULL,"
               + "[Name] [varchar](50) NULL,"
               + "[IssDate] [datetime] NULL,"
               + "[ExpDate] [datetime] NULL,"
               + "[DocumentType] [varchar](50) NULL,"
               + "[DocumentNo] [varchar](50) NULL,"
               + "[PlaceofIss] [varchar](max) NULL,"
               + "[FileName] [varchar](max) NULL,"
               + "[Attachment] [varbinary](max) NULL,"
               + "[userid] [varchar](15) NULL"
           + ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = " CREATE TABLE [dbo].[Docref" + FrmCode + "]"
               + "("
                    + "[id]  [varchar](50) NULL,"
                    + "[docid] [varchar](50) NULL,"
                    + "[empid] [varchar](50) NULL"
                + ") ON[PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE TABLE [dbo].[Firms]"
            + "("
               + "[slno] [bigint] IDENTITY(1,1) NOT NULL,"
               + "[clddate] [datetime] NULL,"
               + "[addr1] [varchar](40) NULL,"
               + "[addr2] [varchar](40) NULL,"
               + "[addr3] [varchar](40) NULL,"
               + "[addr4] [varchar](40) NULL,"
               + "[code] [varchar](20) NOT NULL,"
               + "[name] [varchar](50) NULL,"
               + "[dispname] [varchar](50) NULL,"
               + "[cstno] [varchar](30) NULL,"
               + "[cformat] [varchar](10) NULL,"
               + "[csymbol] [varchar](10) NULL,"
               + "[email] [varchar](50) NULL,"
               + "[jurid] [varchar](40) NULL,"
               + "[kgst] [varchar](30) NULL,"
               + "[opdate] [datetime] NULL,"
               + "[panno] [varchar](20) NULL,"
               + "[phone1] [varchar](40) NULL,"
               + "[phone2] [varchar](40) NULL,"
               + "[Path] [varchar](4) NULL,"
               + "[SecondPath] [varchar](4) NULL,"
               + "[Status] [varchar](4) NULL"
           + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE TABLE [dbo].[GROUPS" + FrmCode + "]"
            + "("
               + "[CODE] [varchar](10) NOT NULL,"
               + "[HEAD] [varchar](40) NULL,"
               + "[LEVEL1] [varchar](2) NULL,"
               + "[GROUPLEVEL] [varchar](2) NULL,"
               + "[GROUPCODE] [varchar](10) NULL,"
               + "[BALANCE] [numeric](15, 2) NOT NULL,"
               + "[OPBALANCE] [numeric](15, 2) NOT NULL,"
               + "[LEGAL] [varchar](1) NULL,"
               + "[USERID] [varchar](15) NULL,"
            + "CONSTRAINT [PK_GROUPS0001] PRIMARY KEY CLUSTERED "
           + "("
           + "	[CODE] ASC"
           + ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]"
           + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE TABLE [dbo].[LeaveApplication" + FrmCode + "]"
            + "("
                + "[id][numeric](18, 0) NULL,"
               + "[Staffid][varchar](50) NULL,"
             + " [DOA][datetime] NULL,"
             + "[LeaveType][varchar](50) NULL,"
            + "[From][datetime] NULL,"
          + " [To][datetime] NULL,"
          + "[COV][varchar](100) NULL,"
           + "[Remarks][varchar](max) NULL"
    + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE TABLE[dbo].[LeaveApproval" + FrmCode + "]"
            + "("
                + "[id] [numeric](18, 0) NULL,"
                + "[applicationid] [varchar](50) NULL,"
                + "[From] [datetime] NULL,"
                + "[To] [datetime] NULL,"
                + "[balleave] [numeric](18, 0) NULL"
            + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();




            sql = "CREATE TABLE [dbo].[Prior" + FrmCode + "]"
            + "("
               + "[id] [varchar](50) NULL,"
               + "[Priorday] [varchar](50) NULL,"
               + "[detid] [varchar](50) NULL,"
               + "[userid] [varchar](15) NULL,"
               + "[Sent] [varchar](50) NULL"
           + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


  


            sql = "CREATE TABLE [dbo].[REJOINING" + FrmCode + "]"
            + "("
               + "[id] [numeric](18, 0) NULL,"
               + "[approvalid] [varchar](50) NULL,"
               + "[DOR] [datetime] NULL,"
               + "[DOARR] [datetime] NULL,"
               + "[DOJ] [datetime] NULL,"
               + "[balleave] [numeric](18, 0) NULL,"
                + "[Remarks][varchar](max) NULL"
           + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();



            // sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Details" + FrmCode + "]')"
            //         + "and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[Details" + FrmCode + "]"
            //         + "CREATE TABLE Details" + FrmCode + "("
            //         + "[id] [varchar](50) NULL,"
            //         + "[Name] [varchar](50) NULL,"
            //         + "[IssDate] [datetime] NULL,"
            //         + "[ExpDate] [datetime] NULL,"
            //         + "[DocumentType] [varchar](50) NULL,"
            //         + "[DocumentNo] [varchar](50) NULL,"
            //         + "[Email] [varchar](max) NULL,"
            //         + "[Attachment] [varchar](max) NULL,"
            //         + "[userid] [varchar](15) NULL"
            //         + ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();

            // sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Prior" + FrmCode + "]')"
            //         + "and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[Prior" + FrmCode + "]"
            //         + " CREATE TABLE [dbo].[Prior" + FrmCode + "]("
            //         + "[id] [varchar](50) NULL,"
            //         + "[Priorday] [varchar](50) NULL,"
            //         + "[detid] [varchar](50) NULL,"
            //         + "[userid] [varchar](15) NULL,"
            //         + "[Sent] [varchar](50) NULL"
            //         + ") ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();

            // sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VOUCHER" + FrmCode + "]')"
            //  + "and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[VOUCHER" + FrmCode + "]"
            //  + " CREATE TABLE VOUCHER" + FrmCode + "("
            //  + "[DATE1]      [datetime] NOT NULL ,"
            //  + "[BOOKCODE]   [varchar] (10) NULL ,"
            //  + "[CODE]       [varchar] (10) NULL ,"
            //  + "[TRANSTYPE]  [varchar] (6) NULL ,"
            //  + "[TRANSNO]    [bigint] NOT NULL ,"
            //  + "[NARRATION]  [varchar] (200) NULL ,"
            //  + "[AMOUNT]     [numeric](15, 2) NOT NULL ,"
            //  + "[BILLNO]     [bigint] NULL ,"
            //  + "[CHEQUE]     [bit] DEFAULT 0 ,"
            //  + "[CHQNO]      [varchar] (30) NULL ,"
            //  + "[CHQPASSDATE][datetime] NULL ,"
            //  + "[CHQDATE]    [datetime] NULL ,"
            //  + "[CHQPASSED]  [bit] DEFAULT 0 ,"
            //  + "[USERID]     [varchar] (50) NULL ,"
            //  + "[MODIFIED]   [bigint]  NULL ,"
            //  + "[MODIUSER]   [varchar](15) NULL,"
            //  + "[RECNO]      [bigint] IDENTITY (1, 1) NOT NULL "
            //  + ") ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();

            sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACCOUNTS" + FrmCode + "]')"
            + "and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[ACCOUNTS" + FrmCode + "] "
            + "CREATE TABLE ACCOUNTS" + FrmCode + "("
            + "[CODE]       [varchar](10) NOT NULL ,"
            + "[HEAD]       [varchar](40) NULL ,"
            + "[BALANCE]    [NUMERIC](15, 2) NOT NULL,"
            + "[TYPE]       [varchar](1) NULL ,"
            + "[LEVEL1]     [varchar](2) NULL ,"
            + "[GROUPCODE]  [varchar](10) NULL ,"
            + "[GROUPLEVEL] [varchar](2) NULL ,"
            + "[OPBALANCE]  [NUMERIC](15, 2) NOT NULL,"
            + "[CRLIMIT]    [NUMERIC](15, 2) NOT NULL ,"
            + "[CRDAYS]     [INT] NULL ,"
            + "[INTRATE]    [NUMERIC](15, 2)NULL,"
            + "[ADDRESS]    [varchar](40) NULL ,"
            + "[STREET]     [varchar](40) NULL ,"
            + "[CITY]       [varchar](40) NULL ,"
            + "[STATE]      [varchar](40) NULL ,"
            + "[PIN]        [varchar](20) NULL ,"
            + "[TIN]        [varchar](20) NULL ,"
            + "[CST]        [varchar](20) NULL ,"
            + "[PHONE1]     [varchar](40) NULL ,"
            + "[PHONE2]     [varchar](40) NULL ,"
            + "[FAX_MAIL]   [varchar](40) NULL ,"
            + "[REMARKS]    [varchar](1000) NULL ,"
            + "[LEGAL]      [varchar](1) NULL ,"
            + "[USERID]     [varchar](15) NULL ,"
            + "[BILL]       [varchar] (1) NULL"
            + " CONSTRAINT [PK_ACCOUNTS" + FrmCode + "] PRIMARY KEY  CLUSTERED "
            + " ([CODE]) ON [PRIMARY]  "
            + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            // sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GROUPS" + FrmCode + "]')"
            // + "and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[GROUPS" + FrmCode + "]"
            // + " CREATE TABLE GROUPS" + FrmCode + "("
            // + "[CODE]       [varchar] (10) NOT NULL ,"
            // + "[HEAD]       [varchar] (40) NULL ,"
            // + "[LEVEL1]     [varchar] (2) NULL ,"
            // + "[GROUPLEVEL] [varchar] (2) NULL ,"
            // + "[GROUPCODE]  [varchar] (10) NULL ,"
            // + "[BALANCE]    [NUMERIC](15, 2) NOT NULL ,"
            // + "[OPBALANCE]  [NUMERIC](15, 2) NOT NULL ,"
            // + "[LEGAL]      [varchar] (1) NULL ,"
            // + "[USERID]     [varchar] (15) NULL "
            // + " CONSTRAINT [PK_GROUPS" + FrmCode + "] PRIMARY KEY  CLUSTERED "
            // + " ([CODE]) ON [PRIMARY]  "
            // + ") ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //creating table for bill to bill
            // //...................................pendingbill...................................................... 
            // sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[pendingbill" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            //    + " DROP TABLE [DBO].[PENDINGBILL" + FrmCode + "]"
            //    + " CREATE TABLE [DBO].[PENDINGBILL" + FrmCode + "] ("
            //    + "[TRANSTYPE] [VARCHAR] (6)   NULL ,"
            //    + "[SERIES] [VARCHAR] (1)   NULL ,"
            //    + "[ORGINALSERIES] [VARCHAR] (1)   NULL ,"
            //    + "[ORDERNO] [BIGINT] NULL DEFAULT (0) ,"
            //    + "[DATE1] [DATETIME] NOT NULL ,"
            //    + "[CUSTCODE] [VARCHAR] (10)   NULL ,"
            //    + "[CRDAYS] [FLOAT] NULL ,"
            //    + "[BILLAMT] [NUMERIC](17, 2) NULL DEFAULT (0.0),"
            //    + "[BILL] [VARCHAR] (1)   NULL ,"
            //    + "[RECEIVED] [NUMERIC](17, 2) NULL DEFAULT (0.0) ,"
            //    + "[BALANCE] [NUMERIC](17, 2) NULL DEFAULT (0.0) ,"
            //    + "[STATUS] [VARCHAR] (5)   NULL ,"
            //    + "[FLAG] [VARCHAR] (1)   NULL ,"
            //    + "[PAID] [NUMERIC](17, 2) NULL DEFAULT (0.0) ,"
            //    + "INVNO [VARCHAR] (20)   NULL ,INVDATE DATETIME,"
            //    + "REMARKS[VARCHAR](40)   NULL,"
            //    + "[MODIFIED] [BIGINT],"
            //    + "[MODIUSER] [VARCHAR] (20),"
            //    + "[USERID] [VARCHAR] (20)   NULL, "
            //    + "[RECNO] [BIGINT] IDENTITY (1, 1) NOT NULL) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();

            // //...................................pbillsub...................................................... 
            // sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PBillSub" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            //         + " drop table [dbo].[PBillSub" + FrmCode + "]"
            //         + " CREATE TABLE [dbo].[PBILLSUB" + FrmCode + "] ("
            //         + " [CUSTCODE] [VARCHAR] (10)   NULL ,"
            //         + " [SERIES] [varchar] (1)   NULL ,"
            //         + " [ORGINALSERIES] [VARCHAR] (1)   NULL ,"
            //         + " [BILL_NO] [BIGINT] NOT NULL DEFAULT (0) ,"
            //         + " [BILL_FROM] [varchar] (1)   NULL ,"
            //         + " [BILL_AMOUNT] [NUMERIC](17, 2) NOT NULL DEFAULT (0.0) ,"
            //         + " [AMOUNT_PAID] [NUMERIC](17, 2) NOT NULL DEFAULT (0.0),"
            //         + " [AMOUNT_GIVEN] [NUMERIC](17, 2) NOT NULL DEFAULT(0.0)  ,"
            //         + " [BALANCE] [NUMERIC](17, 2) NOT NULL DEFAULT(0.0) ,"
            //         + " [STATUS] [VARCHAR] (5)   NULL ,"
            //         + " [DATE_OF_PAY] [SMALLDATETIME] NULL ,"
            //         + " [TRANS_NO] [BIGINT] NOT NULL ,"
            //         + " [TRANS_TYPE] [VARCHAR] (10)   NULL ,"
            //         + " [TRANS_SERIES] [VARCHAR](1) NULL, "
            //         + " [TRANS_FLAG]  [VARCHAR](5) NULL,"
            //         + " [FLAG] [VARCHAR] (10)   NULL ,"
            //         + " [PAID] [NUMERIC](17, 2) NOT NULL DEFAULT(0.0),"
            //         + " INVNO [VARCHAR] (1)   NULL ,INVDATE DATETIME,"
            //         + " [MODIFIED] [BIGINT],"
            //         + " [MODIUSER] [VARCHAR] (20),"
            //         + " [USERID] [VARCHAR] (20)   NULL, "
            //         + " [RECNO] [BIGINT] IDENTITY (1, 1) NOT NULL ) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //   -------  Creating Table SALES   --------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[SALES" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[SALES" + FrmCode + "]"
            // + "CREATE TABLE SALES" + FrmCode + "("
            // + "[DATE1]          [datetime] NULL ,"
            // + "[ORDERNO]        [bigint] NULL ,"
            // + "[FORM]           [varchar](1) NULL ,"
            // + "[BILLTYPE]       [varchar](1) NULL ,"
            // + "[PRICETYPE]      [varchar](1) NULL ,"
            // + "[CUSTCODE]       [varchar](10) NULL ,"
            // + "[CUSTNAME]       [varchar](40) NULL ,"
            // + "[PCUSTNAME]      [varchar](40) NULL ,"
            // + "[PADDRESS]       [varchar](40) NULL ,"
            // + "[PCONTACTNO]     [varchar](20) NULL ,"
            // + "[SALESPERSON]    [varchar](100) NULL ,"
            // + "[EXPENSE]        [numeric](17, 2) NULL ,"
            // + "[BILLAMT]        [numeric](17, 2) NULL ,"
            // + "[SPDISCAMT]      [numeric](17, 2) NULL ,"
            // + "[BILDISCPER]     [numeric](17,2) NULL ,"
            // + "[BILDISCAMT]     [numeric](17,2) NULL ,"
            // + "[ITEMTOTAL]      [numeric](17,2) NULL ,"
            // + "[TAXAMT]         [numeric](17,2) NULL ,"
            // + "[CESSAMT]        [numeric](17,2) NULL ,"
            // + "[ROUNDOFF]       [numeric](17,2) NULL ,"
            // + "[SALESORDERNO]   [bigint] NULL ,"
            // + "[REMARKS1]       [varchar](200) NULL ,"
            // + "[USERID]         [varchar](15) NULL ,"
            // + "[MODIFIED]       [numeric](3) NULL ,"
            // + "[MODIUSER]       [varchar](15) NULL ,"
            // + "[RECNO]          [BIGINT] IDENTITY(1,1) NOT NULL ,"
            // + "[STATUS]         [varchar](1) NULL) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //   -------  Creating Table SITEM   --------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[SITEM" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[SITEM" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[SITEM" + FrmCode + "] ("
            // + "[DATE1]      [datetime] NULL ,"
            // + "[ORDERNO]    [bigint] NULL ,"
            // + "[RATE]       [numeric](17, 2) NULL ,"
            // + "[BILLTYPE]   [varchar] (1)   NULL ,"
            // + "[ITEMCODE]   [varchar] (20)  NULL ,"
            // + "[ITEMNAME]   [varchar] (40)  NULL ,"
            // + "[NETAMT]     [numeric](17,2) NULL ,"
            // + "[QTY]        [numeric](17,3) NULL ,"
            // + "[FOC]        [numeric](17,3) NULL ,"
            // + "[UnitValue]  [numeric](17,3) NULL ,"
            // + "[UNIT]       [varchar](20) NULL ,"
            // + "[PRICE]      [numeric](17,2) NULL ,"
            // + "[TAXPER]     [numeric](17,2) NULL ,"
            // + "[TAXAMT]     [numeric](17,2) NULL ,"
            // + "[CESSPER]    [numeric](17,2) NULL ,"
            // + "[CESSAMT]    [numeric](17,2) NULL ,"
            // + "[DISCPER]    [numeric](17,2) NULL ,"
            // + "[DISCAMT]    [numeric](17,2) NULL ,"
            // + "[ITEMTOTAL]  [numeric](17,2) NULL ,"
            // + "[LC]         [numeric](17,2) NULL ,"
            // + "[ACCCODE]    [varchar] (1)   NULL ,"
            // + "[PRICETYPE]  [varchar] (10)  NULL ,"
            // + "[REMARKS]    [varchar] (120) NULL ,"
            // + "[RECNO]      [bigint] IDENTITY (1, 1) NOT NULL ) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //   -------  Creating Table SALES RETURN   --------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[SRETURN" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[SRETURN" + FrmCode + "]"
            // + "CREATE TABLE SRETURN" + FrmCode + "("
            // + "[DATE1]          [datetime] NULL ,"
            // + "[ORDERNO]        [bigint] NULL ,"
            // + "[FORM]           [varchar](1) NULL ,"
            // + "[BILLTYPE]       [varchar](1) NULL ,"
            // + "[PRICETYPE]      [varchar](1) NULL ,"
            // + "[CUSTCODE]       [varchar](10) NULL ,"
            // + "[CUSTNAME]       [varchar](40) NULL ,"
            // + "[PCUSTNAME]      [varchar](40) NULL ,"
            // + "[PADDRESS]       [varchar](40) NULL ,"
            // + "[PCONTACTNO]     [varchar](20) NULL ,"
            // + "[SALESPERSON]    [varchar](100) NULL ,"
            // + "[EXPENSE]        [numeric](17, 2) NULL ,"
            // + "[BILLAMT]        [numeric](17, 2) NULL ,"
            // + "[SPDISCAMT]      [numeric](17, 2) NULL ,"
            // + "[BILDISCPER]     [numeric](17,2) NULL ,"
            // + "[BILDISCAMT]     [numeric](17,2) NULL ,"
            // + "[ITEMTOTAL]      [numeric](17,2) NULL ,"
            // + "[TAXAMT]         [numeric](17,2) NULL ,"
            // + "[CESSAMT]        [numeric](17,2) NULL ,"
            // + "[ROUNDOFF]       [numeric](17,2) NULL ,"
            // + "[SALESORDERNO]   [bigint] NULL ,"
            // + "[REMARKS1]       [varchar](200) NULL ,"
            // + "[USERID]         [varchar](15) NULL ,"
            // + "[MODIFIED]       [numeric](3) NULL ,"
            // + "[MODIUSER]       [varchar](15) NULL ,"
            // + "[RECNO]          [BIGINT] IDENTITY(1,1) NOT NULL ,"
            // + "[STATUS]         [varchar](1) NULL) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //   -------  Creating Table SRITEM   --------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[SRITEM" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[SRITEM" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[SRITEM" + FrmCode + "] ("
            // + "[DATE1]      [datetime] NULL ,"
            // + "[ORDERNO]    [bigint] NULL ,"
            // + "[RATE]       [numeric](17, 2) NULL ,"
            // + "[BILLTYPE]   [varchar] (1)   NULL ,"
            // + "[ITEMCODE]   [varchar] (20)  NULL ,"
            // + "[ITEMNAME]   [varchar] (40)  NULL ,"
            // + "[NETAMT]     [numeric](17,2) NULL ,"
            // + "[QTY]        [numeric](17,3) NULL ,"
            // + "[FOC]        [numeric](17,3) NULL ,"
            // + "[UNITVALUE]  [numeric](17,3) NULL ,"
            // + "[UNIT]       [varchar](20) NULL ,"
            // + "[PRICE]      [numeric](17,2) NULL ,"
            // + "[TAXPER]     [numeric](17,2) NULL ,"
            // + "[TAXAMT]     [numeric](17,2) NULL ,"
            // + "[CESSPER]    [numeric](17,2) NULL ,"
            // + "[CESSAMT]    [numeric](17,2) NULL ,"
            // + "[DISCPER]    [numeric](17,2) NULL ,"
            // + "[DISCAMT]    [numeric](17,2) NULL ,"
            // + "[ITEMTOTAL]  [numeric](17,2) NULL ,"
            // + "[LC]         [numeric](17,2) NULL ,"
            // + "[ACCCODE]    [varchar] (1)   NULL ,"
            // + "[PRICETYPE]  [varchar] (10)  NULL ,"
            // + "[REMARKS]    [varchar] (120) NULL ,"
            // + "[RECNO]      [bigint] IDENTITY (1, 1) NOT NULL ) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();



            // //   -------  Creating Table TRANSFER   --------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[TRANSFER" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[TRANSFER" + FrmCode + "]"
            // + "CREATE TABLE TRANSFER" + FrmCode + "("
            // + "[DATE1]          [datetime] NULL ,"
            // + "[ORDERNO]        [bigint] NULL ,"
            // + "[FORM]           [varchar](1) NULL ,"
            // + "[BILLTYPE]       [varchar](1) NULL ,"
            // + "[PRICETYPE]      [varchar](1) NULL ,"
            // + "[CUSTCODE]       [varchar](10) NULL ,"
            // + "[CUSTNAME]       [varchar](40) NULL ,"
            // + "[PCUSTNAME]      [varchar](40) NULL ,"
            // + "[PADDRESS]       [varchar](40) NULL ,"
            // + "[PCONTACTNO]     [varchar](20) NULL ,"
            // + "[SALESPERSON]    [varchar](100) NULL ,"
            // + "[EXPENSE]        [numeric](17, 2) NULL ,"
            // + "[BILLAMT]        [numeric](17, 2) NULL ,"
            // + "[SPDISCAMT]      [numeric](17, 2) NULL ,"
            // + "[BILDISCPER]     [numeric](17,2) NULL ,"
            // + "[BILDISCAMT]     [numeric](17,2) NULL ,"
            // + "[ITEMTOTAL]      [numeric](17,2) NULL ,"
            // + "[TAXAMT]         [numeric](17,2) NULL ,"
            // + "[CESSAMT]        [numeric](17,2) NULL ,"
            // + "[ROUNDOFF]       [numeric](17,2) NULL ,"
            // + "[SALESORDERNO]   [bigint] NULL ,"
            // + "[REMARKS1]       [varchar](200) NULL ,"
            // + "[USERID]         [varchar](15) NULL ,"
            // + "[MODIFIED]       [numeric](3) NULL ,"
            // + "[MODIUSER]       [varchar](15) NULL ,"
            // + "[RECNO]          [BIGINT] IDENTITY(1,1) NOT NULL ,"
            // + "[STATUS]         [varchar](1) NULL) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //   -------  Creating Table TRITEM   --------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[TRITEM" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[TRITEM" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[TRITEM" + FrmCode + "] ("
            // + "[DATE1]      [datetime] NULL ,"
            // + "[ORDERNO]    [bigint] NULL ,"
            // + "[RATE]       [numeric](17, 2) NULL ,"
            // + "[BILLTYPE]   [varchar] (1)   NULL ,"
            // + "[ITEMCODE]   [varchar] (20)  NULL ,"
            // + "[ITEMNAME]   [varchar] (40)  NULL ,"
            // + "[NETAMT]     [numeric](17,2) NULL ,"
            // + "[QTY]        [numeric](17,3) NULL ,"
            // + "[FOC]        [numeric](17,3) NULL ,"
            // + "[UnitValue]  [numeric](17,3) NULL ,"
            // + "[UNIT]       [varchar](20) NULL ,"
            // + "[PRICE]      [numeric](17,2) NULL ,"
            // + "[TAXPER]     [numeric](17,2) NULL ,"
            // + "[TAXAMT]     [numeric](17,2) NULL ,"
            // + "[CESSPER]    [numeric](17,2) NULL ,"
            // + "[CESSAMT]    [numeric](17,2) NULL ,"
            // + "[DISCPER]    [numeric](17,2) NULL ,"
            // + "[DISCAMT]    [numeric](17,2) NULL ,"
            // + "[ITEMTOTAL]  [numeric](17,2) NULL ,"
            // + "[LC]         [numeric](17,2) NULL ,"
            // + "[ACCCODE]    [varchar] (1)   NULL ,"
            // + "[PRICETYPE]  [varchar] (10)  NULL ,"
            // + "[REMARKS]    [varchar] (120) NULL ,"
            // + "[RECNO]      [bigint] IDENTITY (1, 1) NOT NULL ) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();



            // //   ------- Creating Table STOCKMST -------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[STOCKMST" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[STOCKMST" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[STOCKMST" + FrmCode + "] ("
            // + "[itemcode]   [varchar] (13) NOT NULL CONSTRAINT PK_itemcode" + FrmCode + " PRIMARY KEY, "
            // + "[itemname]   [varchar] (30) NULL ,"
            // + "[mrp]        [numeric](17, 2) NULL ,"
            // + "[rprofit1]   [numeric](17, 2) NULL ,"
            // + "[rprofit2]   [numeric](17, 2) NULL ,"
            // + "[rprofit3]   [numeric](17, 2) NULL ,"
            // + "[qty]        [numeric](17, 2) NULL ,"
            // + "[taxper]     [numeric](17,2) NULL ,"
            // + "[cessper]    [numeric](17,2) NULL ,"
            // + "[lc]         [numeric](17,2) NULL ,"
            // + "[opstock]    [numeric](17,2) NULL ,"
            // + "[suppcode]   [varchar] (10) NULL ,"
            // + "[unit]       [varchar] (10) NULL ,"
            // + "[multiunit]  [varchar] (10) NULL ,"
            // + "[groupcode]  [varchar] (10) NULL ,"
            // + "[Manufacturer]      [varchar] (100) NULL ,"
            // + "[product]    [varchar] (10) NULL ,"
            // + "[commodity]  [varchar] (15) NULL ,"
            // + "[schedule]   [varchar] (3) NULL ,"
            // + "[userid]     [varchar] (15) NULL ,"
            // + "[recno]      [bigint] IDENTITY (1, 1) NOT NULL) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[MULTIUNIT" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[MULTIUNIT" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[MULTIUNIT" + FrmCode + "]("
            // + "[itemcode] [varchar](13) NOT NULL,         "
            // + "[itemname] [varchar](30) NULL,             "
            // + "[rprofit1] [numeric](17, 2) NULL,          "
            // + "[rprofit3] [numeric](17, 2) NULL,          "
            // + "[lc] [numeric](17, 2) NULL,                "
            // + "[mrp] [numeric](17, 2) NULL,               "
            // + "[retailper] [numeric](17, 2) NULL,         "
            // + "[wholeper] [numeric](17, 2) NULL,          "
            // + "[basedmrp] [bit] NULL,                     "
            // + "[specprice] [numeric](17, 2) NULL,         "
            // + "[specdisc] [numeric](17, 2) NULL,          "
            // + "[projectprice] [numeric](17, 2) NULL,      "
            // + "[multiunit] [varchar](1) NULL,             "
            // + "[unitid] [varchar](20) NULL,               "
            // + "[unit] [varchar](40) NULL,                 "
            // + "[value] [numeric](17, 3) NULL,             "
            // + "[coolie] [numeric](17, 3) NULL,            "
            // + "[weight] [numeric](17, 3) NULL,            "
            // + "[PASS] [varchar](1) NULL,                  "
            // + "[recno] [bigint] IDENTITY(1,1) NOT NULL    "
            // + ") ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();

            // //   ------- Creating Table PURCHASE -------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[PURCHASE" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[PURCHASE" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[PURCHASE" + FrmCode + "] ("
            // + "[DATE1]      [datetime] NULL ,"
            // + "[ORDERNO]    [bigint] NOT NULL ,"
            // + "[PORDERNO]   [bigint]  NULL ,"
            // + "[SUPPCODE]   [varchar] (10) NULL ,"
            // + "[SUPPNAME]   [varchar] (40) NULL ,"
            // + "[INVNO]      [varchar] (20) NULL ,"
            // + "[INVDATE]    [datetime] NULL ,"
            // + "[INVAMT]     [numeric](17, 4) NULL ,"
            // + "[TYPE]       [varchar] (1) NULL ,"
            // + "[REMARKS]    [varchar] (150) NULL ,"
            // + "[EXPENSE]    [numeric](17, 4) NULL ,"
            // + "[ROUNDOFF]   [numeric](17, 4) NULL ,"
            // + "[DISCAMT]    [numeric](17, 4) NULL ,"
            // + "[CSTBILL]    [varchar] (1) NULL ,"
            // + "[TAXAMT]     [numeric](17, 4) NULL ,"
            // + "[CESSAMT]    [numeric](17,4) NULL ,"
            // + "[ADISCAMT]   [numeric](17, 4) NULL ,"
            // + "[USERID]     [varchar] (15) NULL ,"
            // + "[CFORM]      [varchar] (1) NULL ,"
            // + "[CFORMNO]    [varchar] (20) NULL ,"
            // + "[CFORMDATE]  [datetime] NULL ,"
            // + "[CFORMBOOKNO][varchar] (20) NULL ,"
            // + "[STATUS]     [varchar](1) "
            // + " CONSTRAINT [PK_PURCHASE" + FrmCode + "] PRIMARY KEY  CLUSTERED "
            // + " ([ORDERNO]) ON [PRIMARY]  "
            // + ") ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //   ------- Creating Table PITEM -------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[PITEM" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[PITEM" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[PITEM" + FrmCode + "] ("
            // + "[ORDERNO]    [bigint] NULL ,"
            // + "[DATE1]      [datetime] NULL ,"
            // + "[ITEMCODE]   [varchar] (20) NULL ,"
            // + "[ITEMNAME]   [varchar] (40) NULL ,"
            // + "[QTY]        [numeric](17, 3) NULL ,"
            // + "[FOC]        [numeric](17, 3) NULL ,"
            // + "[UNITVALUE]  [numeric](17,4) NULL ,"
            // + "[UNIT]       [varchar](20) NULL ,"
            // + "[PRICE]      [numeric](17, 4) NULL ,"
            // + "[NETAMT]     [numeric](17, 4) NULL ,"
            // + "[DISCPER]    [numeric](17, 2) NULL ,"
            // + "[DISCAMT]    [numeric](17, 4) NULL ,"
            // + "[TAXPER]     [numeric](17, 2) NULL ,"
            // + "[TAXAMT]     [numeric](17, 4) NULL ,"
            // + "[LC]         [numeric](17, 4) NULL ,"
            // + "[TDISCOUNT]  [numeric](17, 4) NULL ,"
            // + "[ACCCODE]    [varchar] (10) NULL ,"
            // + "[POSTAMOUNT] [numeric](17, 4) NULL ,"
            // + "[VATAMT]     [numeric](17, 4) NULL ,"
            // + "[CESSPER]    [numeric](17, 2) NULL ,"
            // + "[CESSAMT]    [numeric](17, 4) NULL ,"
            // + "[CESSPOSTAMT][numeric](17, 4) NULL ,"
            // + "[BARCODE]    [varchar] (1) NULL ,"
            // + "[BAR]        [numeric](17, 4) NULL ,"
            // + "[MRP]        [numeric](17, 4) NULL ,"
            // + "[RECNO]      [bigint] IDENTITY (1, 1) NOT NULL "
            // + " ) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //   ------- Creating Table PRETURN -------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[PRETURN" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[PRETURN" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[PRETURN" + FrmCode + "] ("
            // + "[DATE1]      [datetime] NULL ,"
            // + "[ORDERNO]    [bigint] NOT NULL ,"
            // + "[PORDERNO]   [bigint]  NULL ,"
            // + "[SUPPCODE]   [varchar] (10) NULL ,"
            // + "[SUPPNAME]   [varchar] (40) NULL ,"
            // + "[INVNO]      [varchar] (20) NULL ,"
            // + "[INVDATE]    [datetime] NULL ,"
            // + "[INVAMT]     [numeric](17, 4) NULL ,"
            // + "[TYPE]       [varchar] (1) NULL ,"
            // + "[REMARKS]    [varchar] (150) NULL ,"
            // + "[EXPENSE]    [numeric](17, 4) NULL ,"
            // + "[ROUNDOFF]   [numeric](17, 4) NULL ,"
            // + "[DISCAMT]    [numeric](17, 4) NULL ,"
            // + "[CSTBILL]    [varchar] (1) NULL ,"
            // + "[TAXAMT]     [numeric](17, 4) NULL ,"
            // + "[CESSAMT]    [numeric](17,4) NULL ,"
            // + "[ADISCAMT]   [numeric](17, 4) NULL ,"
            // + "[USERID]     [varchar] (15) NULL ,"
            // + "[CFORM]      [varchar] (1) NULL ,"
            // + "[CFORMNO]    [varchar] (20) NULL ,"
            // + "[CFORMDATE]  [datetime] NULL ,"
            // + "[CFORMBOOKNO][varchar] (20) NULL ,"
            // + "[STATUS]     [varchar](1) "
            // + " CONSTRAINT [PK_PRETURN" + FrmCode + "] PRIMARY KEY  CLUSTERED "
            // + " ([ORDERNO]) ON [PRIMARY]  "
            // + ") ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //   ------- Creating Table PRITEM -------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[PRITEM" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[PRITEM" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[PRITEM" + FrmCode + "] ("
            // + "[ORDERNO]    [bigint] NULL ,"
            // + "[DATE1]      [datetime] NULL ,"
            // + "[ITEMCODE]   [varchar] (20) NULL ,"
            // + "[ITEMNAME]   [varchar] (40) NULL ,"
            // + "[QTY]        [numeric](17, 3) NULL ,"
            // + "[FOC]        [numeric](17, 3) NULL ,"
            // + "[UNITVALUE]  [numeric](17,4) NULL ,"
            // + "[UNIT]       [varchar](20) NULL ,"
            // + "[PRICE]      [numeric](17, 4) NULL ,"
            // + "[NETAMT]     [numeric](17, 4) NULL ,"
            // + "[DISCPER]    [numeric](17, 2) NULL ,"
            // + "[DISCAMT]    [numeric](17, 4) NULL ,"
            // + "[TAXPER]     [numeric](17, 2) NULL ,"
            // + "[TAXAMT]     [numeric](17, 4) NULL ,"
            // + "[LC]         [numeric](17, 4) NULL ,"
            // + "[TDISCOUNT]  [numeric](17, 4) NULL ,"
            // + "[ACCCODE]    [varchar] (10) NULL ,"
            // + "[POSTAMOUNT] [numeric](17, 4) NULL ,"
            // + "[VATAMT]     [numeric](17, 4) NULL ,"
            // + "[CESSPER]    [numeric](17, 2) NULL ,"
            // + "[CESSAMT]    [numeric](17, 4) NULL ,"
            // + "[CESSPOSTAMT][numeric](17, 4) NULL ,"
            // + "[BARCODE]    [varchar] (1) NULL ,"
            // + "[BAR]        [numeric](17, 4) NULL ,"
            // + "[MRP]        [numeric](17, 4) NULL ,"
            // + "[RECNO]      [bigint] IDENTITY (1, 1) NOT NULL "
            // + " ) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();

            // sql = "if exists (select * from dbo.sysobjects where id = "
            //+ " object_id(N'[dbo].[Profit" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            //+ " drop table [dbo].[Profit" + FrmCode + "]"
            //+ " CREATE TABLE [dbo].[Profit] "
            //+ "  ( "
            //+ "   [Profit] numeric(17,4) NULL "
            //+ "  ) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();



            // //   ------- Creating Table TRANSIN -------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[TRANSIN" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[TRANSIN" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[TRANSIN" + FrmCode + "] ("
            // + "[DATE1]      [datetime] NULL ,"
            // + "[ORDERNO]    [bigint] NOT NULL ,"
            // + "[PORDERNO]   [bigint]  NULL ,"
            // + "[SUPPCODE]   [varchar] (10) NULL ,"
            // + "[SUPPNAME]   [varchar] (40) NULL ,"
            // + "[INVNO]      [varchar] (20) NULL ,"
            // + "[INVDATE]    [datetime] NULL ,"
            // + "[INVAMT]     [numeric](17, 4) NULL ,"
            // + "[TYPE]       [varchar] (1) NULL ,"
            // + "[REMARKS]    [varchar] (150) NULL ,"
            // + "[EXPENSE]    [numeric](17, 4) NULL ,"
            // + "[ROUNDOFF]   [numeric](17, 4) NULL ,"
            // + "[DISCAMT]    [numeric](17, 4) NULL ,"
            // + "[CSTBILL]    [varchar] (1) NULL ,"
            // + "[TAXAMT]     [numeric](17, 4) NULL ,"
            // + "[CESSAMT]    [numeric](17,4) NULL ,"
            // + "[ADISCAMT]   [numeric](17, 4) NULL ,"
            // + "[USERID]     [varchar] (15) NULL ,"
            // + "[CFORM]      [varchar] (1) NULL ,"
            // + "[CFORMNO]    [varchar] (20) NULL ,"
            // + "[CFORMDATE]  [datetime] NULL ,"
            // + "[CFORMBOOKNO][varchar] (20) NULL ,"
            // + "[STATUS]     [varchar](1) "
            // + " CONSTRAINT [PK_TRANSIN" + FrmCode + "] PRIMARY KEY  CLUSTERED "
            // + " ([ORDERNO]) ON [PRIMARY]  "
            // + ") ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //   ------- Creating Table TRITEMIN -------

            // sql = "if exists (select * from dbo.sysobjects where id = "
            // + "object_id(N'[dbo].[TRITEMIN" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            // + "drop table [dbo].[TRITEMIN" + FrmCode + "]"
            // + "CREATE TABLE [dbo].[TRITEMIN" + FrmCode + "] ("
            // + "[ORDERNO]    [bigint] NULL ,"
            // + "[DATE1]      [datetime] NULL ,"
            // + "[ITEMCODE]   [varchar] (20) NULL ,"
            // + "[ITEMNAME]   [varchar] (40) NULL ,"
            // + "[QTY]        [numeric](17, 3) NULL ,"
            // + "[FOC]        [numeric](17, 3) NULL ,"
            // + "[UNITVALUE]  [numeric](17,4) NULL ,"
            // + "[UNIT]       [varchar](20) NULL ,"
            // + "[PRICE]      [numeric](17, 4) NULL ,"
            // + "[NETAMT]     [numeric](17, 4) NULL ,"
            // + "[DISCPER]    [numeric](17, 2) NULL ,"
            // + "[DISCAMT]    [numeric](17, 4) NULL ,"
            // + "[TAXPER]     [numeric](17, 2) NULL ,"
            // + "[TAXAMT]     [numeric](17, 4) NULL ,"
            // + "[LC]         [numeric](17, 4) NULL ,"
            // + "[TDISCOUNT]  [numeric](17, 4) NULL ,"
            // + "[ACCCODE]    [varchar] (10) NULL ,"
            // + "[POSTAMOUNT] [numeric](17, 4) NULL ,"
            // + "[VATAMT]     [numeric](17, 4) NULL ,"
            // + "[CESSPER]    [numeric](17, 2) NULL ,"
            // + "[CESSAMT]    [numeric](17, 4) NULL ,"
            // + "[CESSPOSTAMT][numeric](17, 4) NULL ,"
            // + "[BARCODE]    [varchar] (1) NULL ,"
            // + "[BAR]        [numeric](17, 4) NULL ,"
            // + "[MRP]        [numeric](17, 4) NULL ,"
            // + "[RECNO]      [bigint] IDENTITY (1, 1) NOT NULL "
            // + " ) ON [PRIMARY]";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();



            // // MANFACT,MITEMIN,MITEMOUT
            // sql = "if exists (select * from dbo.sysobjects where id = "
            //    + " object_id(N'[dbo].[MANFACT" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            //    + " drop table [dbo].[MANFACT" + FrmCode + "]"
            // + "   CREATE TABLE [MANFACT" + FrmCode + "] (\n"
            // + "   [DATE1] [datetime] NULL ,\n"
            // + "   [ORDERNO] [bigint] NOT NULL ,\n"
            // + "   [EXPENSE] [numeric](17, 2) NULL ,\n"
            // + "   [REMARKS] [varchar] (40) NULL ,\n"
            // + "   [NETAMT] [numeric](17, 2) NULL ,\n"
            // + "   [USERID] [varchar] (50) NULL \n"
            // + " ) \n";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();

            // //MITEMIN
            // sql = "if exists (select * from dbo.sysobjects where id = "
            //    + " object_id(N'[dbo].[MITEMIN" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            //    + " drop table [dbo].[MITEMIN" + FrmCode + "]"
            //    + "  CREATE TABLE [MITEMIN" + FrmCode + "] (       \n"
            //    + "  [DATE1] [datetime] NULL ,                     \n"
            //    + "  [ORDERNO] [bigint] NOT NULL ,                 \n"
            //    + "  [ITEMCODE] [varchar] (13)  NULL ,             \n"
            //    + "  [ITEMNAME] [varchar] (20)  NULL ,             \n"
            //    + "  [QTY] [numeric](17, 3) NULL ,                 \n"
            //    + "  [PRICE] [numeric](17, 2) NULL ,               \n"
            //    + "  [AMOUNT] [numeric](17, 2) NULL ,              \n"
            //    + "  [MRP] [numeric](17, 2) NULL ,                 \n"
            //    + "  [WASTAGE] [numeric](17, 3) NULL ,             \n"
            //    + "  [RECNO] [bigint] IDENTITY (1, 1) NOT NULL ,   \n"
            //     + " [ROLLQTY] [numeric](17, 3) NULL ,             \n"
            //     + " [UNIT] [varchar] (3) NULL,                    \n"
            //     + " [UNITID] [varchar] (1) NULL,                  \n"
            //     + " [UNITVALUE] [numeric](17, 3) NULL             \n"
            //     + " )                  \n";

            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();


            // //MITEMOUT

            // sql = "if exists (select * from dbo.sysobjects where id = "
            //    + " object_id(N'[dbo].[MITEMOUT" + FrmCode + "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            //    + " drop table [dbo].[MITEMOUT" + FrmCode + "]       \n"
            //    + " CREATE TABLE [MITEMOUT" + FrmCode + "] (          \n"
            //    + " [DATE1] [datetime] NULL ,                        \n"
            //    + " [ORDERNO] [bigint] NOT NULL ,                    \n"
            //    + " [ITEMCODE] [varchar] (13)  NULL ,                \n"
            //    + " [ITEMNAME] [varchar] (20)  NULL ,                \n"
            //    + " [QTY] [numeric](17, 3) NULL ,                    \n"
            //    + " [PRICE] [numeric](17, 2) NULL ,                  \n"
            //    + " [AMOUNT] [numeric](17, 2) NULL ,                 \n"
            //    + " [PACKDATE] [datetime] NULL ,                     \n"
            //    + " [EXPDATE] [datetime] NULL ,                      \n"
            //    + " [MRP] [numeric](17, 2) NULL ,                    \n"
            //    + " [RECNO] [bigint] IDENTITY (1, 1) NOT NULL ,      \n"
            //    + " [ROLLQTY] [numeric](17, 3) NULL ,                \n"
            //    + " [UNIT] [varchar] (3) NULL,                      \n"
            //    + " [UNITID] [varchar] (1) NULL,                   \n"
            //    + " [UNITVALUE] [numeric](17, 3) NULL            \n"
            //    + " )                    \n";
            // comm = new SqlCommand(sql, cPublic.Connection);
            // comm.ExecuteNonQuery();

            //job(FrmCode);
            sPS(FrmCode);
            Function(FrmCode);
        }

        private void ExecuteQuery(string QueryStatement)
        {
            SqlCommand SqlCommand = new SqlCommand(QueryStatement, cPublic.Connection);
            SqlCommand.ExecuteNonQuery();
        }


        private void CreateFunctions(string FirmCode)
        {


        }

       

        public void sPS(string FrmCode)
        {



            string sql = string.Empty;
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);



            sql = "CREATE PROCEDURE [dbo].[SP_DELETE_LOOKUP" + FrmCode + "] \n"
           + " (\n"
           + " @field1     [varchar](10)\n"
           + " ,@code        [varchar](10)\n"
           + " )\n"
           + " AS \n"
           + " DELETE FROM LOOKUP where [Field1] = @field1 and [Code] = @code ";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE procedure [dbo].[SP_GET_BALLEAVE" + FrmCode + "] \n"
           + "	( @staffid varchar(50))				  \n"
           + "	as									  \n"
           + "	declare @DOJ datetime				  \n"
           + "	declare @DOR date					  \n"
           + "	declare @DOA date					  \n"
           + "	declare @DOAP date					  \n"
           + "	declare @rejid varchar				  \n"
           + "	declare @balleave numeric(18,0)		  \n"
           + "	declare @currleave numeric(18,0)	  \n"
           + "	declare @staffdorcount numeric(18,0)  \n"
           + "	declare @totalleave numeric(18,0)	  \n"
           + "	declare @appid varchar 				  \n"
           + "	set @staffdorcount= (select count (*) from  [dbo].[LeaveApproval" + FrmCode + "] b join [dbo].[LeaveApplication" + FrmCode + "] c on c.id=b.[applicationid] join 			\n"
           + "[dbo].[Employee" + FrmCode + "] d on d.StaffID=c.Staffid where c.staffid=@staffid and b.id not in (select approvalid from [dbo].[REJOINING" + FrmCode + "])) 					\n"
           + "	set @DOJ= (select top 1 d.DateOfJoining from [dbo].[REJOINING" + FrmCode + "] a join [dbo].[LeaveApproval" + FrmCode + "] b on a.[approvalid]=b.id join 					\n"
           + "[dbo].[LeaveApplication" + FrmCode + "] c on c.id=b.[applicationid] join [dbo].[Employee" + FrmCode + "] d on d.StaffID=c.Staffid where c.staffid=@staffid order by a.id desc)\n"
           + "	Set @DOAP=(select top 1 CONVERT(datetime, CONVERT(varchar(10), [To], 101))  from  [dbo].[LeaveApplication" + FrmCode + "]  where staffid=@staffid 				\n"
           + "	order by id desc)																																	\n"
           + "	set  @DOR= (select top 1 CONVERT(datetime, CONVERT(varchar(10), a.[DOR], 101))  from [dbo].[REJOINING" + FrmCode + "] a join [dbo].[LeaveApproval" + FrmCode + "] b 		\n"
           + "	on a.[approvalid]=b.id join [dbo].[LeaveApplication" + FrmCode + "] c on c.id=b.[applicationid]  where c.staffid=@staffid order by a.id desc)					\n"
           + "	set @DOA=(select top 1  CONVERT(datetime, CONVERT(varchar(10), a.[To], 101)) from [dbo].[LeaveApproval" + FrmCode + "] a join [dbo].[LeaveApplication" + FrmCode + "] b 	\n"
           + "	on b.id=a.[applicationid]  where b.staffid=@staffid order by a.id desc)																				\n"
           + "	set @appid=(select top 1  a.id from [dbo].[LeaveApproval" + FrmCode + "] a join [dbo].[LeaveApplication" + FrmCode + "] b on b.id=a.[applicationid]  						\n"
           + "	where b.staffid=@staffid order by a.id desc)																										\n"
           + "	set @currleave=((select top 1     DATEDIFF(MONTH,a.[DOR], GETDATE()) from [dbo].[REJOINING" + FrmCode + "] a join [dbo].[LeaveApproval" + FrmCode + "] b 					\n"
           + "	on a.[approvalid]=b.id join [dbo].[LeaveApplication" + FrmCode + "] c on c.id=b.[applicationid]  where c.staffid=@staffid order by a.id desc ) * 2.5)			\n"
           + "	set @totalleave= (select  DATEDIFF(MONTH,[DateOfJoining], @DOA) from  [dbo].[Employee" + FrmCode + "] where StaffID=@staffid) * 2.5							\n"
      																														
           + "	if ISNULL(@DOAP, '')='' or ISNULL(@DOA, '')=''																										\n"
           + "	Begin																																				\n"
           + "	set @balleave= ((select DATEDIFF(MONTH,[DateOfJoining], GETDATE()) from  [dbo].[Employee" + FrmCode + "] where StaffID=@staffid) * 2.5)						\n"
           + "	select ISNULL(@balleave, 0)																															\n"
           + "	End																																					\n"
           + "	if ISNULL(@DOR, '')='' and ISNULL(@DOA, '')<>''																										\n"
           + "	Begin																																				\n"
           + "	set @balleave= ((select DATEDIFF(MONTH,[DateOfJoining], @DOA) from  [dbo].[Employee" + FrmCode + "] where StaffID=@staffid) * 2.5)								\n"
           + "	select ISNULL(@balleave, 0)																															\n"
           + "	End																																					\n"
           + "	if(@DOR>@DOA)																																		\n"
           + "	Begin																																				\n"
           + "	set @balleave= (select top 1 (a.[balleave]- DATEDIFF(DAY,@DOA, @DOR)) from [dbo].[LeaveApproval" + FrmCode + "] a join [dbo].[LeaveApplication" + FrmCode + "] b on 		\n"
           + "	b.id=a.[applicationid]  where b.staffid=@staffid order by a.id desc)																				\n"
           + "	select ISNULL(@balleave, 0)																															\n"
           + "	End																																					\n"
           + "	if(@DOR=@DOA)																																		\n"
           + "	Begin																																				\n"
           + "		if(@DOR<GETDATE()) 																																\n"
           + "		Begin																																			\n"
           + "			set @balleave= (select top 1 (select top 1 a.[balleave] from [dbo].[LeaveApproval" + FrmCode + "] a join [dbo].[LeaveApplication" + FrmCode + "] b on 			\n"
           + "	b.id=a.[applicationid]  where b.staffid=@staffid and a.[balleave] <> 0 order by a.id desc)+ (DATEDIFF(MONTH,@DOR, GETDATE())*2.5) 					\n"
           + "	from [dbo].[LeaveApproval" + FrmCode + "] a join [dbo].[LeaveApplication" + FrmCode + "] b on b.id=a.[applicationid]  where b.staffid=@staffid order by a.id desc)		\n"
           + "			select ISNULL(@balleave, 0)																													\n"
           + "		End																																				\n"
           + "		else																																			\n"
           + "		Begin																																			\n"
           + "			set @balleave= (select top 1 a.[balleave] from [dbo].[LeaveApproval" + FrmCode + "] a join [dbo].[LeaveApplication" + FrmCode + "] b on b.id=a.[applicationid]  	\n"
           + "where b.staffid=@staffid order by a.id desc)																											\n"
           + "			select ISNULL(@balleave, 0)																													\n"
           + "		End																																				\n"
           + "	END																																					\n"
           + "	if(@DOR<@DOA and @staffdorcount > 0) 																												\n"
           + "	Begin																																				\n"
           + "	set @balleave= (select top 1 (select top 1 a.[balleave] from [dbo].[LeaveApproval" + FrmCode + "] a join [dbo].[LeaveApplication" + FrmCode + "] b on 					\n"
           + "	b.id=a.[applicationid]  where b.staffid=@staffid and a.[balleave] <> 0 order by a.id desc)+ (DATEDIFF(MONTH,@DOR, @DOA)*2.5) from 					\n"
           + "	[dbo].[LeaveApproval" + FrmCode + "] a join [dbo].[LeaveApplication" + FrmCode + "] b on b.id=a.[applicationid]  where b.staffid=@staffid order by a.id desc)				\n"
           + "	select ISNULL(@balleave, 0)																															\n"
           + "	End																																					\n"
           + "	if(@DOR<@DOA and @staffdorcount = 0) 																												\n"
           + "	Begin																																				\n"
           + "	set @balleave= (select top 1 (a.[balleave]+ DATEDIFF(DAY,@DOR, @DOA)) from [dbo].[LeaveApproval" + FrmCode + "] a join [dbo].[LeaveApplication" + FrmCode + "] b 			\n"
            + "	on b.id = a.[applicationid]  where b.staffid = @staffid order by a.id desc)		\n"                                                                         
            + "	select ISNULL(@balleave, 0) \n"
            + "	End";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE [dbo].[SP_INSERT_DETAILS" + FrmCode + "] \n"
           + "		(@id 	    [varchar](13)='', 				   \n"
           + "		@name 	    [varchar](max)='', 				   \n"
           + "		@issuedate 	        datetime , 				   \n"
           + "		@expirydate        datetime, 				   \n"
           + "		@docno        [varchar](max)='', 			   \n"
           + "		@doctype      [varchar](13)='', 			   \n"
           + "		@Attachment  [varchar](max)='', 			   \n"
           + "		@PlaceofIss [varchar](max)='',				   \n"
           + "		@email [varchar](max)='', 					   \n"
           + "		@userid          [varchar](15) = '', 		   \n"
           + "		@code            [varchar](13)='' output) 	   \n"
           + "		AS 											   \n"
           + "		if @id='' 									   \n"
           + "		begin 													  \n"
           + "		set @id = (select isnull(MAX(CAST(id AS Int)) ,0)+1 from Details" + FrmCode + ")   \n"
           + "		end  													  \n"
           + "		set @code=@id 											  \n"
           + "		if @id <> '' 											  \n"
           + "		INSERT INTO Details" + FrmCode + "( [id],[name], [IssDate],[ExpDate],[DocumentType],[DocumentNo],[PlaceofIss],[Email],[Attachment], [userid]) \n"
           + "		VALUES( @id, @name, @issuedate,@expirydate,@doctype,@docno,@PlaceofIss, @email, @Attachment, @userid) 								\n"
           + "		INSERT [dbo].[Prior0001] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'1', N'0', @id, NULL,'0') 							\n"
           + "		INSERT [dbo].[Prior0001] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'2', N'1', @id, NULL,'0') 							\n"
           + "		INSERT [dbo].[Prior0001] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'3', N'7', @id, NULL,'0') 							\n"
           + "		INSERT [dbo].[Prior0001] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'4', N'15', @id, NULL,'0') 							\n"
           + "		INSERT [dbo].[Prior0001] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'5', N'30', @id, NULL,'0') 							\n"
           + "		select @code";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE[dbo].[SP_INSERT_DOCREF0001] \n"
                  + "       (@id[varchar](13)='', \n"
                  + "       @docid[varchar](max)='', \n"
                  + "       @empid[varchar](max)='' ) \n"
                  + "       AS \n"
                  + "       if @id='' \n"
                  + "       begin \n"
                  + "       set @id = (select isnull(MAX(CAST(id AS Int)) ,0)+1 from Docref0001) \n"
                  + "       end  \n"
              
                  + "       if @id<> '' \n"
                  + "       INSERT INTO Docref0001([id], [docid], [empid])\n"
                  + "       VALUES(@id, @docid, @empid)";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "Create PROCEDURE[dbo].[SP_DELETE_Details" + FrmCode + "](\n"
                    + "@id[varchar](10))\n"
                    + "AS\n"
                    + "DELETE FROM[dbo].[Details" + FrmCode + "] where id = @id \n"
                    + "DELETE FROM[dbo].[Prior" + FrmCode + "]  where detid = @id";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "Create PROCEDURE[dbo].[SP_DELETE_EmployeeDetails" + FrmCode + "](\n"
                   + "@id[varchar](10))\n"
                   + "AS\n"
                   + "DELETE FROM [EmployeeDocument" + FrmCode + "] where id = @id ";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE [dbo].[SP_INSERT_EMPLOYEE" + FrmCode + "] \n"
           + "	(@id 	     [varchar](13)='', 							\n"
           + "	@dateofjoining 	        datetime , 						\n"
           + "	@dateofarrival 	        datetime , 						\n"
           + "	@staffid	    [varchar](max)='', 						\n"
           + "	@staffname 	    [varchar](max)='', 						\n"
            + "	@grade 	    [varchar](max)='', 						\n"
           + "	@department 	    [varchar](max)='', 					\n"
           + "	@qualification        [varchar](max)='', 				\n"
           + "	@designation [varchar](max)='', 						\n"
           + "	@basic  numeric(18,0), 									\n"
           + "	@allowance numeric(18,0), 								\n"
           + "	@drivinglicense	    [varchar](max)='', 					\n"
            + "	@drivinglicenseno	    [varchar](max)='', 					\n"
           + "	@prevemp	    [varchar](max)='', 						\n"
           + "	@yearofexperience  numeric(18,0), 						\n"
           + "	@phoneno 	    [varchar](max)='', 						\n"
           + "	@mobileno 	    [varchar](max)='', 						\n"
           + "	@email          [varchar](15) = '', 					\n"
           + "	@dob       datetime, 									\n"
           + "	@gender 	    [varchar](max)='', 						\n"
           + "	@marital 	    [varchar](max)='', 						\n"
           + "	@spouse 	    [varchar](max)='', 						\n"
           + "	@nationality  [varchar](max)='', 						\n"
           + "	@religion  [varchar](max)='', 							\n"
           + "	@dependent numeric(18,0)=0, 							\n"
           + "	@FatherName 	    [varchar](max)='', 					\n"
           + "	@MotherName 	    [varchar](max)='', 					\n"
           + "	@NName 	    [varchar](max)='',							\n"
           + "	@NRelation 	    [varchar](max)='',  					\n"
           + "	@NPhoneNo 	    [varchar](max)='', 						\n"
           + "	@NMobileNo 	    [varchar](max)='', 						\n"
           + "	@NEmail          [varchar](15) = '',					\n"
           + "	@NPermanentAddress1 	    [varchar](max)='', 				\n"
            + "	@NPermanentAddress2 	    [varchar](max)='', 				\n"
             + "	@NPermanentAddress3 	    [varchar](max)='', 				\n"
              + "	@NPermanentAddress4 	    [varchar](max)='', 				\n"
           + "	@NTemporaryAddress1 	    [varchar](max)='', 				\n"
             + "	@NTemporaryAddress2 	    [varchar](max)='', 				\n"
               + "	@NTemporaryAddress3 	    [varchar](max)='', 				\n"
                 + "	@NTemporaryAddress4 	    [varchar](max)='', 				\n"
           + "	@DName 	    [varchar](max)='',							\n"
           + "	@DRelation 	    [varchar](max)='',  					\n"
           + "	@DPhoneNo 	    [varchar](max)='', 						\n"
           + "	@DMobileNo 	    [varchar](max)='', 						\n"
           + "	@DEmail          [varchar](15) = '',					\n"
           + "	@DPermanentAddress1 	    [varchar](max)='', 				\n"
            + "	@DPermanentAddress2 	    [varchar](max)='', 				\n"
             + "	@DPermanentAddress3 	    [varchar](max)='', 				\n"
              + "	@DPermanentAddress4 	    [varchar](max)='', 				\n"
           + "	@DTemporaryAddress1 	    [varchar](max)='', 				\n"
            + "	@DTemporaryAddress2 	    [varchar](max)='', 				\n"
             + "	@DTemporaryAddress3 	    [varchar](max)='', 				\n"
              + "	@DTemporaryAddress4 	    [varchar](max)='', 				\n"
           + "	@Picture [Image]=null,									\n"
            + "	@cancel bit,									\n"
            +"@remarks[varchar](max) = '', \n"
             + "@abbr [varchar](max)='', \n"
           + "	@code             [varchar](13)='' output) 				\n"
           + "	AS 														\n"

           + "	if @id='' \n"
           + "	begin \n"
           
            + "	set @id = (select isnull(max(id), 0) + 1 from Employee" + FrmCode + ") \n"
           + "	end  \n"
           + "	set @code=@id \n"
           + "	if @id <> '' \n"
           + "	INSERT INTO Employee" + FrmCode + " \n"
           + "   ( \n"
           + "	[id] ,						 \n"
           + "		[DateOfJoining] ,		 \n"
           + "		[DateOfArrival],		 \n"
           + "		[StaffID] ,				 \n"
           + "		[StaffName] ,			 \n"
            + "		[Grade] ,			 \n"
           + "		[Department] ,			 \n"
           + "		[Qualification] ,		 \n"
           + "		[Designation] ,			 \n"
           + "		[Basic] ,				 \n"
           + "		[Allowance],			 \n"
           + "		[DrivingLicence] ,		 \n"
            + "		[DrivingLicenceNo] ,		 \n"
           + "		[PrevEmployer] ,		 \n"
           + "		[YearOfExperience] ,	 \n"
           + "		[PhoneNo] ,				 \n"
           + "		[MobileNo] ,			 \n"
           + "		[Email] ,				 \n"
           + "		[DOB] ,					 \n"
           + "		[Gender] ,				 \n"
           + "		[Marital] ,				 \n"
           + "		[Spouse] ,				 \n"
           + "		[Nationality] ,			 \n"
           + "		[Religion] ,			 \n"
           + "		[Dependent] ,			 \n"
           + "		[FatherName] ,			 \n"
           + "		[MotherName],			 \n"
           + "		[NName] ,				 \n"
           + "		[NRelation] ,			 \n"
           + "		[NPhoneNo],				 \n"
           + "		[NMobileNo] ,			 \n"
           + "		[NEmail] ,				 \n"
           + "		[NPermanentAddress1] ,	 \n"
            + "		[NPermanentAddress2] ,	 \n"
             + "		[NPermanentAddress3] ,	 \n"
              + "		[NPermanentAddress4] ,	 \n"
           + "		[NTemporaryAddress1] ,	 \n"
           + "		[NTemporaryAddress2] ,	 \n"
           + "		[NTemporaryAddress3] ,	 \n"
           + "		[NTemporaryAddress4] ,	 \n"
           + "		[DName],				 \n"
           + "		[DRelation],			 \n"
           + "		[DPhoneNo] ,			 \n"
           + "		[DMobileNo],			 \n"
           + "		[DEmail] ,				 \n"
           + "		[DPermanentAddress1] ,	 \n"
            + "		[DPermanentAddress2] ,	 \n"
             + "		[DPermanentAddress3] ,	 \n"
              + "		[DPermanentAddress4] ,	 \n"
           + "		[DTemporaryAddress1] ,	 \n"
            + "		[DTemporaryAddress2] ,	 \n"
             + "		[DTemporaryAddress3] ,	 \n"
              + "		[DTemporaryAddress4] ,	 \n"
           + "		[Picture], 				 \n"
            + "[Cancel],  \n"
             + "[Remarks])  \n"
           + "	VALUES( 					 \n"
           + "	@id 	     , 				 \n"
           + "	@dateofjoining 	         , 	 \n"
           + "	@dateofarrival 	         , 	 \n"
           + "	@staffid	    , 			 \n"
           + "	@staffname 	    , 			 \n"
            + "	@grade 	    , 			 \n"
           + "	@department 	    , 		 \n"
           + "	@qualification        , 	 \n"
           + "	@designation , 				 \n"
           + "	@basic  , 					 \n"
           + "	@allowance,					 \n"
           + "	@drivinglicense	    , 		 \n"
             + "	@drivinglicenseno	    , 		 \n"
           + "	@prevemp	    , 			 \n"
           + "	@yearofexperience  , 		 \n"
           + "	@phoneno 	    , 			 \n"
           + "	@mobileno 	    , 			 \n"
           + "	@email  , 					 \n"
           + "	@dob       , 				 \n"
           + "	@gender 	    , 			 \n"
           + "	@marital 	    , 			 \n"
           + "	@spouse 	    , 			 \n"
           + "	@nationality  , 			 \n"
           + "	@religion  , 				 \n"
           + "	@dependent, 				 \n"
           + "	@FatherName 	    , 		 \n"
           + "	@MotherName 	    , 		 \n"
           + "	@NName 	    ,				 \n"
           + "	@NRelation 	    ,  			 \n"
           + "	@NPhoneNo 	    , 			 \n"
           + "	@NMobileNo 	    , 			 \n"
           + "	@NEmail         ,			 \n"
           + "	@NPermanentAddress1 	    , 	 \n"
           + "	@NPermanentAddress2 	    , 	 \n"
           + "	@NPermanentAddress3 	    , 	 \n"
           + "	@NPermanentAddress4 	    , 	 \n"
           + "	@NTemporaryAddress1 	    , 	 \n"
            + "	@NTemporaryAddress2 	    , 	 \n"
             + "	@NTemporaryAddress3 	    , 	 \n"
              + "	@NTemporaryAddress4 	    , 	 \n"
           + "	@DName 	    ,				 \n"
           + "	@DRelation 	    ,  			 \n"
           + "	@DPhoneNo 	    , 			 \n"
           + "	@DMobileNo 	    , 			 \n"
           + "	@DEmail       ,				 \n"
           + "	@DPermanentAddress1 	    , 	 \n"
            + "	@DPermanentAddress2 	    , 	 \n"
             + "	@DPermanentAddress3 	    , 	 \n"
              + "	@DPermanentAddress4 	    , 	 \n"
           + "	@DTemporaryAddress1 	    , 	 \n"
           + "	@DTemporaryAddress2 	    , 	 \n"
           + "	@DTemporaryAddress3 	    , 	 \n"
           + "	@DTemporaryAddress4 	    , 	 \n"
           + "	@Picture ,					 \n"
           + "	@cancel,					 \n"
            + "	@remarks )					 \n"
           + "	select @code";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE PROCEDURE [dbo].[SP_UPDATE_EMPLOYEE" + FrmCode + "] \n"
    + "(@id 	     [varchar](13)='', 		   \n"
    + "@dateofjoining 	        datetime , 	   \n"
    + "@dateofarrival 	        datetime , 	   \n"
    + "@staffid	    [varchar](max)='', 		   \n"
    + "@staffname 	    [varchar](max)='', 	   \n"
     + "@grade	    [varchar](max)='', 	   \n"
    + "@department 	    [varchar](max)='', 	   \n"
    + "@qualification        [varchar](max)='',\n"
    + "@designation [varchar](max)='', 		   \n"
    + "@basic  numeric(18,0), 				   \n"
    + "@allowance numeric(18,0), 			   \n"
    + "@drivinglicense	    [varchar](max)='', \n"
      + "@drivinglicenseno	    [varchar](max)='', \n"
    + "@prevemp	    [varchar](max)='', 		   \n"
    + "@yearofexperience  numeric(18,0), 	   \n"
    + "@phoneno 	    [varchar](max)='', 	   \n"
    + "@mobileno 	    [varchar](max)='', 	   \n"
    + "@email          [varchar](15) = '', 	   \n"
    + "@dob       datetime, 				   \n"
    + "@gender 	    [varchar](max)='', 		   \n"
    + "@marital 	    [varchar](max)='', 	   \n"
    + "@spouse 	    [varchar](max)='', 		   \n"
    + "@nationality  [varchar](max)='', 	   \n"
    + "@religion  [varchar](max)='', 		   \n"
    + "@dependent numeric(18,0), 			   \n"
    + "@FatherName 	    [varchar](max)='', 	   \n"
    + "@MotherName 	    [varchar](max)='', 	   \n"
    + "@NName 	    [varchar](max)='',		   \n"
    + "@NRelation 	    [varchar](max)='',     \n"
    + "@NPhoneNo 	    [varchar](max)='', 	   \n"
    + "@NMobileNo 	    [varchar](max)='', 	   \n"
    + "@NEmail          [varchar](15) = '',	   \n"
      + "	@NPermanentAddress1 	    [varchar](max)='', 				\n"
            + "	@NPermanentAddress2 	    [varchar](max)='', 				\n"
             + "	@NPermanentAddress3 	    [varchar](max)='', 				\n"
              + "	@NPermanentAddress4 	    [varchar](max)='', 				\n"
           + "	@NTemporaryAddress1 	    [varchar](max)='', 				\n"
             + "	@NTemporaryAddress2 	    [varchar](max)='', 				\n"
               + "	@NTemporaryAddress3 	    [varchar](max)='', 				\n"
                 + "	@NTemporaryAddress4 	    [varchar](max)='', 				\n"
    + "@DName 	    [varchar](max)='',				\n"
    + "@DRelation 	    [varchar](max)='',  		\n"
    + "@DPhoneNo 	    [varchar](max)='', 			\n"
    + "@DMobileNo 	    [varchar](max)='', 			\n"
    + "@DEmail          [varchar](15) = '',			\n"
   + "	@DPermanentAddress1 	    [varchar](max)='', 				\n"
            + "	@DPermanentAddress2 	    [varchar](max)='', 				\n"
             + "	@DPermanentAddress3 	    [varchar](max)='', 				\n"
              + "	@DPermanentAddress4 	    [varchar](max)='', 				\n"
           + "	@DTemporaryAddress1 	    [varchar](max)='', 				\n"
            + "	@DTemporaryAddress2 	    [varchar](max)='', 				\n"
             + "	@DTemporaryAddress3 	    [varchar](max)='', 				\n"
              + "	@DTemporaryAddress4 	    [varchar](max)='', 				\n"
    + "@Picture [Image]=null,						\n"
      + "@cancel bit=0,						\n"
         + "@remarks [varchar](max)='',						\n"
         + "@abbr [varchar](max)='') 						\n"
    + "AS 											\n"
    + "begin 										\n"
    + "\n"
    + "update Employee" + FrmCode + " set 				 \n"
    + "										 \n"
    + "[DateOfJoining] =@dateofjoining, 	 \n"
    + "[DateOfArrival]=@dateofarrival, 		 \n"
    + "[StaffID]=@staffid,					 \n"
    + "[StaffName]=@staffname, 				 \n"
     + "[Grade]=@grade, 				 \n"
    + "[Department]=@department, 			 \n"
    + "[Qualification] =@qualification,      \n"
    + "[Designation]=@designation, 			 \n"
    + "[Basic]=@basic, 						 \n"
    + "[allowance]=@allowance,				 \n"
    + "[DrivingLicence] =@drivinglicense,	 \n"
      + "[DrivingLicenceNo] =@drivinglicenseno,	 \n"
    + "[PrevEmployer] =		@prevemp,		 \n"
    + "[YearOfExperience] =		@yearofexperience,  \n"
    + "[PhoneNo] =		@phoneno ,					\n"
    + "[MobileNo] =		@mobileno ,					\n"
    + "[Email] =		@email , 					\n"
    + "[DOB] =		@dob    ,  						\n"
    + "[Gender] =		@gender ,					\n"
    + "[Marital] =		@marital ,					\n"
    + "[Spouse] =		@spouse ,					\n"
    + "[Nationality] =		@nationality ,			\n"
    + "[Religion] =		@religion  , 				\n"
    + "[Dependent] =		@dependent,				\n"
    + "[FatherName] =		@FatherName, 			\n"
    + "[MotherName]=		@MotherName, 			\n"
    + "[NName] =		@NName ,					\n"
    + "[NRelation] =		@NRelation, 			\n"
    + "[NPhoneNo]=		@NPhoneNo ,					\n"
    + "[NMobileNo] =		@NMobileNo ,			\n"
    + "[NEmail] =		@NEmail       ,  			\n"
+ "[NPermanentAddress1] = @NPermanentAddress1 ,       \n"
+ "[NPermanentAddress2] =		@NPermanentAddress2 , \n"
+ "[NPermanentAddress3] =		@NPermanentAddress3 , \n"
+ "[NPermanentAddress4] =		@NPermanentAddress4 , \n"
+ "[NTemporaryAddress1] =		@NTemporaryAddress1 , \n"
+ "[NTemporaryAddress2] =		@NTemporaryAddress2 , \n"
+ "[NTemporaryAddress3] =		@NTemporaryAddress3 , \n"
+ "[NTemporaryAddress4] =		@NTemporaryAddress4 , \n"
    + "[DName]=		@DName ,							 \n"
    + "[DRelation]=		@DRelation ,					 \n"
    + "[DPhoneNo] =		@DPhoneNo ,						 \n"
    + "[DMobileNo]=		@DMobileNo ,					 \n"
    + "[DEmail] =		@DEmail       ,					 \n"
+ "[DPermanentAddress1] =		@DPermanentAddress1 ,   \n"
+ "[DPermanentAddress2] =		@DPermanentAddress2 ,   \n"
+ "[DPermanentAddress3] =		@DPermanentAddress3 ,   \n"
+ "[DPermanentAddress4] =		@DPermanentAddress4 ,   \n"
+ "[DTemporaryAddress1] =		@DTemporaryAddress1 ,   \n"
+ "[DTemporaryAddress2] =		@DTemporaryAddress2 ,   \n"
+ "[DTemporaryAddress3] =		@DTemporaryAddress3 ,   \n"
+ "[DTemporaryAddress4] =		@DTemporaryAddress4 ,   \n"
    + "[Picture]	=	@Picture,[Remarks]=@remarks  where id=@id			 \n"

    + "END";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE PROCEDURE [dbo].[SP_INSERT_EmployeeDocument" + FrmCode + "] \n"
           + "	(@id 	    [varchar](13)='', 		  \n"
           + "	@staffid [varchar](max)='', 		  \n"
           + "	@name 	    [varchar](max)='', 		  \n"
           + "	@issuedate 	        datetime , 		  \n"
           + "	@expirydate        datetime, 		  \n"
           + "	@docno        [varchar](max)='', 	  \n"
           + "	@doctype      [varchar](13)='',		  \n"
           + "	@PlaceofIss [varchar](max)='', 		  \n"
           + "	@filename     [varchar](max)='', 	  \n"
           + "	@Attachment   [varbinary](max) NULL,  \n"
           + "	@userid          [varchar](15) = '',  \n"
           + "	@code[varchar](13) = '' output )  \n"
           + "	AS 									  \n"
           + "	if @id='' 							  \n"
           + "	begin 								  \n"
           + "	set @id = (select isnull(MAX(CAST(id AS Int)) ,0)+1 from EmployeeDocument" + FrmCode + ") \n"
           + "	end  \n"
            + "	set @code = @id \n"
           + "	if @id <> '' \n"
           + "	INSERT INTO EmployeeDocument" + FrmCode + " \n"
           + "	( 				 \n"
           + "	[id],			 \n"
           + "	[staffid],		 \n"
           + "	[name], 		 \n"
           + "	[IssDate],		 \n"
           + "	[ExpDate],		 \n"
           + "	[DocumentType],	 \n"
           + "	[DocumentNo],	 \n"
           + "	[PlaceofIss],	 \n"
           + "	[FileName],		 \n"
           + "	[Attachment],\n"
           + " [userid]) \n"
           + "	VALUES( @id,@staffid, @name, @issuedate,@expirydate,@doctype,@docno,@PlaceofIss,@filename,  @Attachment, @userid) \n"
            + "select @code";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();



            sql = "CREATE PROCEDURE [dbo].[sp_insert_firms]\n"
           + "	 (  							 \n"
           + "	 @clddate 	[datetime],			 \n"
           + "	 @addr1 	[varchar](40)='',	 \n"
           + "	 @addr2  [varchar](40)='',		 \n"
           + "	 @addr3 	[varchar](40)='',	 \n"
           + "	 @addr4 	[varchar](40)='',	 \n"
           + "	 @code 	[varchar](20)='',		 \n"
           + "	 @name	[varchar](50)='',		 \n"
           + "	 @dispname 	[varchar](50)='',	 \n"
           + "	 @cstno 	[varchar](30)='',	 \n"
           + "	 @cformat 	[varchar](10)='',	 \n"
           + "	 @csymbol 	[varchar](10)='',	 \n"
           + "	 @email 	[varchar](50)='',	 \n"
           + "	 @jurid 	[varchar](40)='',	 \n"
           + "	 @kgst 	[varchar](30)='',		 \n"
           + "	 @opdate 	[datetime],			 \n"
           + "	 @panno 	[varchar](20)='',	 \n"
           + "	 @phone1	[varchar](40)='',	 \n"
           + "	 @phone2 	[varchar](40)='',	 \n"
           + "	 @Path 	[varchar](4)=''			 \n"
           + "	 ,@SecondPath 	[varchar](4)='', \n"
           + "	 @Status 	[varchar](4)=''		 \n"
           + "	 )								 \n"
           + "	 AS 							 \n"
           + "	 INSERT INTO [Firms] 			 \n"
           + "	 ( 								 \n"
           + "	 [clddate],						 \n"
           + "	 [addr1],						 \n"
           + "	 [addr2],						 \n"
           + "	 [addr3],						 \n"
           + "	 [addr4],						 \n"
           + "	 [code],						 \n"
           + "	 [name],						 \n"
           + "	 [dispname],					 \n"
           + "	 [cstno],						 \n"
           + "	 [cformat],						 \n"
           + "	 [csymbol],						 \n"
           + "	 [email],						 \n"
           + "	 [jurid],						 \n"
           + "	 [kgst],						 \n"
           + "	 [opdate],						 \n"
           + "	 [panno],						 \n"
           + "	 [phone1],						 \n"
           + "	 [phone2],						 \n"
           + "	 [Path],						 \n"
           + "	 [SecondPath],					 \n"
           + "	 [Status]						 \n"
           + "	 )								 \n"
           + "	 VALUES							 \n"
           + "	 ( 								 \n"
           + "	 @clddate,						 \n"
           + "	 @addr1,						 \n"
           + "	 @addr2,						 \n"
           + "	 @addr3,						 \n"
           + "	 @addr4,						 \n"
           + "	 @code,							 \n"
           + "	 @name,							 \n"
           + "	 @dispname,						 \n"
           + "	 @cstno,						 \n"
           + "	 @cformat,						 \n"
           + "	 @csymbol,						 \n"
           + "	 @email,						 \n"
           + "	 @jurid,						 \n"
           + "	 @kgst,							 \n"
           + "	 @opdate,						 \n"
           + "	 @panno,						 \n"
           + "	 @phone1,						 \n"
           + "	 @phone2,						 \n"
           + "	 @Path,							 \n"
           + "	 @SecondPath,					 \n"
           + "	 @Status						 \n"
           + "	 )";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE PROCEDURE [dbo].[SP_INSERT_LEAVEAPPLICATION" + FrmCode + "] \n"
           + "	(@id 	     [varchar](13)='', 	   \n"
           + "	@StaffID 	    [varchar](max)='', \n"
           + "	@LeaveType 	    [varchar](max)='', \n"
           + "	@DOA	    datetime, 			   \n"
           + "	@From datetime,					   \n"
           + "	@To datetime,					   \n"
           + "	@cov [varchar](max)='', 		   \n"
           + "@remarks [varchar](max)='',\n"
           + "	@code             [varchar](13)='' output) \n"
           + "	AS 										   \n"
           + "	if @id='' 								   \n"
           + "	begin 									   \n"
           + "	set @id = (select isnull(max(id),0)+1 from LeaveApplication" + FrmCode + ") \n"
           + "	end  							  \n"
           + "	set @code=@id 					  \n"
           + "	if @id <> '' 					  \n"
           + "	INSERT INTO LeaveApplication" + FrmCode + " \n"
           + "(\n"
           + "	[id],							  \n"
           + "	[StaffID], 						  \n"
           + "	[DOA],							  \n"
           + "	[LeaveType],					  \n"
           + "	[From],							  \n"
           + "	[To],							  \n"
           + "	[COV], 							  \n"
            + "	[Remarks]) 							  \n"
           + "	VALUES( 						  \n"
           + "	@id, 							  \n"
           + "	@StaffID, 						  \n"
           + "	@DOA,							  \n"
           + "	@LeaveType,						  \n"
           + "	@From,							  \n"
           + "	@To,							  \n"
           + "	@cov, \n"
           +"@remarks ) \n"
           + "	select @code";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();



            sql = "CREATE PROCEDURE [dbo].[SP_INSERT_LEAVEAPPROVAL" + FrmCode + "] \n"
           + "(@id 	     [varchar](13)='', 			 \n"
           + "@applicationid	    [varchar](max)='', 	 \n"
           + "@From datetime,							 \n"
           + "@To datetime,							 \n"
           + "@balleave numeric(18,0),				 \n"
           + "@code             [varchar](13)='' output) 	\n"
           + "AS 											\n"
           + "if @id='' 									\n"
           + "begin 										\n"
           + "set @id = (select isnull(max(id),0)+1 from LeaveApproval" + FrmCode + ") \n"
           + "end  							 \n"
           + "set @code=@id 					 \n"
           + "if @id <> '' 					 \n"
           + "INSERT INTO LeaveApproval" + FrmCode + "(  \n"
           + "[id],							 \n"
           + "[applicationid], 				 \n"
           + "[From],							 \n"
           + "[To],							 \n"
           + "[balleave]) 					 \n"
           + "VALUES( 						 \n"
           + "@id, 							 \n"
           + "@applicationid, 				 \n"
           + "@From,							 \n"
           + "@To,							 \n"
           + "@balleave) 						 \n"

           + "select @code";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE PROCEDURE [dbo].[SP_INSERT_LOOKUP" + FrmCode + "] \n"
            + "(\n"
            + "@field1     [varchar](10),\n"
            + "@code        [varchar](10),\n"
            + "@details     [varchar](100)\n"
            + ")\n"
            + "AS \n"
            + "INSERT INTO LOOKUP ([Field1], [Code], [Details]) VALUES (@field1, @code, @details)";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE PROCEDURE [dbo].[SP_INSERT_REJOINING" + FrmCode + "] \n"
            + "(@id 	     [varchar](13)='', 	   \n"
            + "@approvalid	    [varchar](max)='', \n"
            + "@DOJ datetime,					   \n"
            + "@DOR datetime,					   \n"
            + "@DOARR datetime,					   \n"
            + "@balleave numeric(18,0),			   \n"
             + "@remarks [varchar](max)='',\n"
            + "@code             [varchar](13)='' output) \n"
            + "AS 										  \n"
            + "if @id='' 								  \n"
            + "begin 									  \n"
            + "set @id = (select isnull(max(id),0)+1 from REJOINING" + FrmCode + ")\n"
            + "end  						\n"
            + "set @code=@id 				\n"
            + "if @id <> '' 				\n"
            + "INSERT INTO REJOINING" + FrmCode + " ( 	\n"
            + "[id],						\n"
            + "[approvalid], 				\n"
            + "[DOR],						\n"
            + "[DOJ],						\n"
            + "[DOARR],						\n"
            + "[balleave], 					\n"
             + "	[Remarks]) 							  \n"
            + "VALUES( 						\n"
            + "@id, 						\n"
            + "@approvalid, 				\n"
            + "@DOR, 						\n"
            + "@DOJ, 						\n"
            + "@DOARR,						\n"
            + "@balleave,					\n"
             + "@remarks ) \n"
            + "select @code";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "CREATE PROCEDURE [dbo].[SP_UPDATE_DETAILS" + FrmCode + "] \n"
           + "(@id 	    [varchar](13)='', 	 \n"
           + "@name 	    [varchar](max)='', 	 \n"
           + "@issuedate 	        datetime , 	 \n"
           + "@expirydate        datetime, 	 \n"
           + "@docno        [varchar](max)='', \n"
           + "@doctype      [varchar](13)='',  \n"
           + "@Attachment  [varchar](max)='',  \n"
           + "@PlaceofIss [varchar](max)='', 	 \n"
           + "@email [varchar](max)='', 		 \n"
           + "@userid          [varchar](15) = '') \n"
           + "AS  \n"
           + "UPDATE Details" + FrmCode + "  SET  [name]=@name, [IssDate]=@issuedate,[ExpDate]=@expirydate,[DocumentType]=@doctype,[DocumentNo]=@docno,\n"
           + "[PlaceofIss]=@PlaceofIss,[Email]=@email,[Attachment]=@Attachment, [userid]=@userid \n"
           + "where id= @id ";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

     

            sql = "CREATE PROCEDURE [dbo].[sp_update_firms] \n"
           + " (								\n"
           + " @code 	[varchar](6),			\n"
           + " @clddate 	[datetime],			\n"
           + " @addr1 	[varchar](40)='',		\n"
           + " @addr2 	[varchar](40)='',		\n"
           + " @addr3 	[varchar](40)='',		\n"
           + " @addr4 	[varchar](40)='',		\n"
           + " @name 	    [varchar](50)='',	\n"
           + " @dispname 	[varchar](50)='',	\n"
           + " @cstno 	[varchar](30)='',		\n"
           + " @cformat 	[varchar](10)='',	\n"
           + " @csymbol 	[varchar](10)='',	\n"
           + " @email 	[varchar](50)='',		\n"
           + " @jurid	 	[varchar](40)='',	\n"
           + " @kgst	 	[varchar](30)='',	\n"
           + " @opdate 	[datetime],			\n"
           + " @panno 	[varchar](20)='',		\n"
           + " @phone1 	[varchar](40)='',	\n"
           + " @phone2 	[varchar](40)='',	\n"
           + " @Path	 	[varchar](4)='',	\n"
           + " @SecondPath	 	[varchar](4)=''	\n"
           + " )								\n"
           + " AS								\n"
           + " UPDATE [Firms] SET  			\n"
           + " [clddate]	 = @clddate,		\n"
           + " [addr1]= @addr1,				\n"
           + " [addr2]= @addr2,				\n"
           + " [addr3]= @addr3,				\n"
           + " [addr4]= @addr4,				\n"
           + " [code]= @code,					\n"
           + " [name]= @name,					\n"
           + " [dispname]= @dispname,			\n"
           + " [cstno]= @cstno,				\n"
           + " [cformat]= @cformat,			\n"
           + " [csymbol]= @csymbol,			\n"
           + " [email]= @email,				\n"
           + " [jurid]= @jurid,				\n"
           + " [kgst]= @kgst,					\n"
           + " [opdate]= @opdate,				\n"
           + " [panno]= @panno,				\n"
           + " [phone1]= @phone1,				\n"
           + " [phone2]= @phone2,				\n"
           + " [Path]= @Path,					\n"
           + " [SecondPath]= @SecondPath 		\n"
           + " WHERE([code]= @code)";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE [dbo].[SP_UPDATE_LEAVEAPPLICATION" + FrmCode + "] \n"
           + "(@id 	     [varchar](13)='', 		\n"
           + "@StaffID 	    [varchar](max)='', 	\n"
           + "@LeaveType 	    [varchar](max)='', 	\n"
           + "@DOA	    datetime, 					\n"
           + "@From datetime,						\n"
           + "@To datetime,						\n"
           + "@cov [varchar](max)='') 				\n"
           + "AS 									\n"
           + "begin 								\n"

           + "update [dbo].[LeaveApplication" + FrmCode + "] set \n"

           + "[LeaveType]=@LeaveType, \n"
           + "[DOA]=@DOA,			   \n"
           + "[From]=@From,		   \n"
           + "[To]=@To,			   \n"
           + "[COV]=@cov where id=@id \n"
           + "END";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE [dbo].[SP_UPDATE_LOOKUP" + FrmCode + "] \n"
           + " (							  \n"
           + " @field1     [varchar](10),	  \n"
           + " @code        [varchar](10),  \n"
           + " @details [varchar](100))	  \n"
           + " AS 						  \n"
           + " UPDATE LOOKUP SET 			  \n"
           + " [Details] = @details 		  \n"
           + " WHERE [Field1] = @field1 and [Code]=@code";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();















          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //      + " object_id(N'SP_UPDATE_BALANCE" + FrmCode + "') ) "
          //      + " drop PROCEDURE SP_UPDATE_BALANCE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "Create PROCEDURE [dbo].[SP_INSERT_DETAILS" + FrmCode + "] \n"
          //      + "(@id 	    [varchar](13)='', \n"
          //      + "@name 	    [varchar](max)='', \n"
          //      + "@issuedate 	        datetime , \n"
          //      + "@expirydate        datetime, \n"
          //      + "@docno        [varchar](max)='', \n"
          //      + "@doctype      [varchar](13)='', \n"
          //      + "@Attachment  [varchar](max)='', \n"
          //      + "@email [varchar](max)='', \n"
          //      + "@userid          [varchar](15) = '', \n"
          //      + "@code            [varchar](13)='' output) \n"
          //      + "AS \n"
          //      + "if @id='' \n"
          //          + "begin \n"
          //              + "set @id = (select isnull(max(id),0)+1 from Details" + FrmCode + ") \n"
          //              + "end  \n"
          //      + "set @code=@id \n"
          //      + "if @id <> '' \n"
          //          + "INSERT INTO Details" + FrmCode + " ( [id],[name], [IssDate],[ExpDate],[DocumentType],[DocumentNo],[Email],[Attachment], [userid]) \n"
          //      + "VALUES( @id, @name, @issuedate,@expirydate,@doctype,@docno, @email, @Attachment, @userid) \n"


          //      + "INSERT [dbo].[Prior" + FrmCode + "] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'1', N'30', @id, NULL,'0') \n"

          //      + "INSERT [dbo].[Prior" + FrmCode + "] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'2', N'15', @id, NULL,'0') \n"

          //      + "INSERT [dbo].[Prior" + FrmCode + "] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'3', N'0', @id, NULL,'0') \n"

          //      + "INSERT [dbo].[Prior" + FrmCode + "] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'6', N'1', @id, NULL,'0') \n"

          //      + "INSERT [dbo].[Prior" + FrmCode + "] ([id], [Priorday], [detid], [userid], [sent]) VALUES (N'7', N'7', @id, NULL,'0') \n"

          //      + "select @code";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "Create PROCEDURE [dbo].[SP_UPDATE_DETAILS" + FrmCode + "] \n"
          //      + "(@id 	    [varchar](13)='', \n"
          //      + "@name 	    [varchar](max)='', \n"
          //      + "@issuedate 	        datetime , \n"
          //      + "@expirydate        datetime, \n"
          //      + "@docno        [varchar](max)='', \n"
          //      + "@doctype      [varchar](13)='', \n"
          //      + "@Attachment  [varchar](max)='', \n"
          //      + "@email [varchar](max)='', \n"
          //      + "@userid          [varchar](15) = '') \n"
          //      + "AS  \n"


          //      + "UPDATE Details0001 SET  [name]=@name, [IssDate]=@issuedate,[ExpDate]=@expirydate,[DocumentType]=@doctype,[DocumentNo]=@docno,[Email]=@email,[Attachment]=@Attachment, [userid]=@userid \n"
          //      + "where id= @id ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE  procedure SP_UPDATE_BALANCE" + FrmCode + " \n"
          //      + "( \n"
          //      + " @code varchar(10), \n"
          //      + " @bookcode varchar(10), \n"
          //      + " @amount numeric(17,2) \n"
          //      + ") as  \n"
          //      + "begin  \n"
          //      + "UPDATE ACCOUNTS" + FrmCode + " SET balance=balance + (CASE WHEN  code=@code THEN @amount else -(@amount) END)  \n"
          //      + "WHERE code=@code OR code=@bookcode  \n"
          //      + "declare  \n"
          //      + "@grouplevel bigint,  \n"
          //      + "@count tinyint  \n"
          //      + "set @count =0  \n"
          //      + "abc:  \n"
          //      + "    select @code=groupcode,@grouplevel=grouplevel from accounts" + FrmCode + " where code in (@code)  \n"
          //      + "    while (@grouplevel>0) \n"
          //      + "      begin \n"
          //      + "          update groups" + FrmCode + " set balance=balance + @amount where code=@code \n"
          //      + "          select @code=groupcode,@grouplevel=grouplevel from groups" + FrmCode + " where code in (@code) \n"
          //      + "      end  \n"
          //      + "if @count =0 \n"
          //      + "      begin \n"
          //      + "          set @count=@count+1 \n"
          //      + "          set @code = @bookcode \n"
          //      + "          set @amount = @amount * (-1) \n"
          //      + "          goto abc \n"
          //      + "      end \n"
          //      + "end";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //        + " object_id(N'SP_UPDATE_OPBALANCE" + FrmCode + "') ) "
          //        + " drop PROCEDURE SP_UPDATE_OPBALANCE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE  procedure SP_UPDATE_OPBALANCE" + FrmCode + " \n"
          //          + "( \n"
          //          + " @code varchar(10), \n"
          //          + " @bookcode varchar(10), \n"
          //          + " @amount numeric(17,2) \n"
          //          + ") as  \n"
          //          + "begin  \n"
          //          + "UPDATE ACCOUNTS" + FrmCode + " SET opbalance=opbalance + (CASE WHEN  code=@code THEN @amount else -(@amount) END)  \n"
          //          + "WHERE code=@code OR code=@bookcode  \n"
          //          + "declare  \n"
          //          + "@grouplevel bigint,  \n"
          //          + "@count tinyint  \n"
          //          + "set @count =0  \n"
          //          + "abc:  \n"
          //          + "    select @code=groupcode,@grouplevel=grouplevel from accounts" + FrmCode + " where code in (@code)  \n"
          //          + "    while (@grouplevel>0) \n"
          //          + "      begin \n"
          //          + "          update groups" + FrmCode + " set opbalance=opbalance + @amount where code=@code \n"
          //          + "          select @code=groupcode,@grouplevel=grouplevel from groups" + FrmCode + " where code in (@code) \n"
          //          + "      end  \n"
          //          + "if @count =0 \n"
          //          + "      begin \n"
          //          + "          set @count=@count+1 \n"
          //          + "          set @code = @bookcode \n"
          //          + "          set @amount = @amount * (-1) \n"
          //          + "          goto abc \n"
          //          + "      end \n"
          //          + "end";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_GET_OPBAL" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_GET_OPBAL" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_GET_OPBAL" + FrmCode + "  " + Environment.NewLine
          //              + " ( @AMT AS NUMERIC(15,2)=0 OUTPUT,@CODE AS VARCHAR(6),@DATE AS DATETIME,@PASS INT=0 )   " + Environment.NewLine
          //              + " AS   " + Environment.NewLine
          //              + " SET @AMT=0   " + Environment.NewLine
          //              + " IF (@PASS=0)  " + Environment.NewLine
          //              + " BEGIN  " + Environment.NewLine
          //              + " SELECT @AMT=(OPBALANCE +   " + Environment.NewLine
          //              + " (SELECT ISNULL(SUM(AMOUNT),0) FROM VOUCHER" + FrmCode + " WHERE CODE =A.CODE AND DATE1< @DATE)+  " + Environment.NewLine
          //              + " (SELECT ISNULL(SUM(AMOUNT*-1),0) FROM VOUCHER" + FrmCode + " WHERE BOOKCODE =A.CODE AND DATE1< @DATE)  " + Environment.NewLine
          //              + " ) FROM ACCOUNTS" + FrmCode + " A WHERE CODE =@CODE   " + Environment.NewLine
          //              + " END  " + Environment.NewLine
          //              + " ELSE IF (@PASS=1)  " + Environment.NewLine
          //              + " BEGIN  " + Environment.NewLine
          //              + " SELECT @AMT=(OPBALANCE +   " + Environment.NewLine
          //              + " (SELECT ISNULL(SUM(AMOUNT),0) FROM VOUCHER" + FrmCode + " WHERE CODE =A.CODE   " + Environment.NewLine
          //              + " AND ( (BOOKCODE<='200002' AND DATE1<@DATE) OR (BOOKCODE>'200002' AND CHQPASSDATE< @DATE AND CHQPASSED=1 )))+  " + Environment.NewLine
          //              + " (SELECT ISNULL(SUM(AMOUNT*-1),0) FROM VOUCHER" + FrmCode + " WHERE BOOKCODE =A.CODE   " + Environment.NewLine
          //              + " AND ( (BOOKCODE<='200002' AND DATE1<@DATE) OR (BOOKCODE>'200002' AND CHQPASSDATE< @DATE AND CHQPASSED=1 )))  " + Environment.NewLine
          //              + " ) FROM ACCOUNTS" + FrmCode + " A WHERE CODE =@CODE   " + Environment.NewLine
          //              + " END  " + Environment.NewLine
          //              + " SELECT @AMT ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  sql = "if exists (select * from dbo.sysobjects where id ="
          // + " object_id(N'SP_VOUCHER_DELETE" + FrmCode + "') ) "
          // + " drop PROCEDURE SP_VOUCHER_DELETE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = " CREATE   PROCEDURE SP_VOUCHER_DELETE" + FrmCode + " \n"
          //  + " ( \n"
          //  + " @TRANSTYPE VARCHAR, \n"
          //  + " @TRANSNO  BIGINT \n"
          //  + " ) \n"
          //  + " AS \n"
          //  + " DECLARE @CODE VARCHAR(10), @BOOKCODE VARCHAR(10), @AMOUNT NUMERIC(17,2) \n"

          //  + " WHILE(SELECT COUNT(*) FROM VOUCHER" + FrmCode + " WHERE [TRANSTYPE] = @TRANSTYPE  AND [TRANSNO]= @TRANSNO) <>0 \n"
          //  + " BEGIN  \n"
          //  + " SELECT @CODE=CODE,@AMOUNT=(-AMOUNT),@BOOKCODE=BOOKCODE FROM VOUCHER" + FrmCode + "  \n"
          //  + " WHERE [TRANSTYPE] = @TRANSTYPE  AND [TRANSNO]= @TRANSNO AND RECNO=(SELECT ISNULL(MAX(RECNO),0) FROM VOUCHER" + FrmCode + " WHERE [TRANSTYPE] = @TRANSTYPE  AND [TRANSNO]= @TRANSNO) \n"

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " \n"
          //  + " 	@CODE , \n"
          //  + "  	@BOOKCODE, \n"
          //  + " 	@AMOUNT \n"

          //  + " DELETE FROM VOUCHER" + FrmCode + " WHERE  [TRANSTYPE] = @TRANSTYPE  AND [TRANSNO]= @TRANSNO AND RECNO=(SELECT ISNULL(MAX(RECNO),0) FROM VOUCHER" + FrmCode + " WHERE [TRANSTYPE] = @TRANSTYPE  AND [TRANSNO]= @TRANSNO) \n"
          //  + " END";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_VOUCHER_DELETE_REC" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_VOUCHER_DELETE_REC" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_VOUCHER_DELETE_REC" + FrmCode
          //  + "(@TRANSTYPE[varchar],@RECNO  [bigint])"
          //  + "AS DELETE FROM VOUCHER" + FrmCode + " WHERE "
          //  + "( [TRANSTYPE]	 = @TRANSTYPE  AND [RECNO]= @RECNO)";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();   

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_VOUCHER_INSERT" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_VOUCHER_INSERT" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_VOUCHER_INSERT" + FrmCode
          //  + "(@DATE1 	        [datetime],"
          //  + "@BOOKCODE 	    [varchar](10),"
          //  + "@CODE 	        [varchar](10),"
          //  + "@TRANSTYPE 	    [varchar](6),"
          //  + "@TRANSNO	        [bigint],"
          //  + "@NARRATION	    [varchar](200)='',"
          //  + "@AMOUNT 	        [numeric](15,2),"
          //  + "@BILLNO 	        [bigint]='',"
          //  + "@CHEQUE 	        [bit]=0,"
          //  + "@CHQNO 	        [varchar](30)='',"
          //  + "@CHQPASSDATE 	[datetime]= NULL,"
          //  + "@CHQDATE	        [datetime]=NULL,"
          //  + "@CHQPASSED 	    [bit]=0,"
          //  + "@MODIFIED        [bigint]=0 ,"
          //  + "@MODIUSER        [varchar](15)='',"
          //  + "@USERID 	        [varchar](50)='')"
          //  + "AS INSERT INTO VOUCHER" + FrmCode
          //  + "( [DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT],[BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID],[MODIFIED],[MODIUSER])"
          //  + " VALUES (@DATE1, @BOOKCODE, @CODE, @TRANSTYPE, @TRANSNO, @NARRATION, @AMOUNT, @BILLNO, @CHEQUE, @CHQNO, @CHQPASSDATE, @CHQDATE, @CHQPASSED, @USERID,@MODIFIED,@MODIUSER)"
          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode
          //  + " @CODE, @BOOKCODE,@AMOUNT ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          
          //  //PENDINGBILL PROCEEDURE////////////INSERT//////

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //             + " object_id(N'SP_PendingBill_INSERT" + FrmCode + "') ) "
          //             + " drop PROCEDURE  SP_PendingBill_INSERT" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_PendingBill_INSERT" + FrmCode
          //          + "(@DATE1 	[datetime]=null,"
          //          + "@custcode 	[varchar](10)='',"
          //          + "@TRANSTYPE 	[varchar](6)='',"
          //          + "@orderno	[bigint],"
          //          + "@billamt 	[numeric](17,2)=0,"
          //          + "@SERIES 	[varchar](1)='',"
          //          + "@orginalseries 	[varchar](1)='',"
          //          + "@crdays [float]='',"
          //          + "@bill [varchar](1)='',"
          //          + "@status 	[varchar](5)='',"
          //          + "@received [numeric](17,2)= 0.0,"
          //          + "@balance [numeric](17,2)= 0.0,"
          //          + "@flag	[varchar](1)= '',"
          //          + "@paid 	[numeric](17,2)=0.0,"
          //          + "@invno 	[varchar](20)='',"
          //          + "@invdate [datetime]=null,"
          //          + "@remarks [varchar](40)='',"
          //          + "@modified    [bigint]=0,"
          //          + "@modiuser    [varchar](20)='',"
          //          + "@userid      [varchar](20)='')"
          //          + " AS \n"
          //          + "if exists(select * from pendingbill" + FrmCode + " where custcode=@custcode and  \n"
          //          + "   transtype=@transtype and orderno=@orderno and series=@series and flag=@flag) \n"
          //          + "    begin \n"
          //          + "        delete from pendingbill" + FrmCode + " where custcode=@custcode and transtype=@transtype  \n"
          //          + "        and orderno=@orderno and series=@series and flag=@flag \n"
          //          + "    end \n"
          //          + "INSERT INTO PendingBill" + FrmCode
          //          + "( [DATE1], [custcode], [TRANSTYPE], [orderno],[billamt], [SERIES],[orginalseries], [crdays], [bill], [status], [received], [balance], [flag], [paid],[invno],[invdate],[remarks],[USERID],[MODIFIED],[MODIUSER])"
          //          + "VALUES (@DATE1, @custcode, @TRANSTYPE, @orderno, @billamt, @SERIES, @orginalseries ,@crdays, @bill, @status, @received,@balance, @flag, @paid,@invno,@invdate,@remarks,@userid,@modified,@modiuser)";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  // PENDINGBILL PROCEEDURE////////////UPDATE//////

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //            + " object_id(N'SP_PendingBill_UPDATE" + FrmCode + "') ) "
          //            + " drop PROCEDURE  SP_PendingBill_UPDATE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_PendingBill_UPDATE" + FrmCode
          //          + "(@DATE1 	    [datetime],"
          //          + "@TRANSTYPE 	[varchar](6),"
          //          + "@orderno	    [bigint],"
          //          + "@billamt 	[numeric](17,2),"
          //          + "@custcode    [varchar](10),"
          //          + "@SERIES 	    [varchar](1)='',"
          //          + "@orginalseries   [varchar](1)='',"
          //          + "@crdays      [float]=0,"
          //          + "@bill        [varchar](1)='',"
          //          + "@status 	    [varchar](5)='',"
          //          + "@received    [numeric](17,2)= 0.0,"
          //          + "@balance     [numeric](17,2)= 0.0,"
          //          + "@flag	    [varchar](1)= '',"
          //          + "@paid 	    [numeric](17,2)=0.0,"
          //          + "@invno 	    [varchar](20)='',"
          //          + "@invdate     [datetime]=null,"
          //          + "@remarks     [varchar](40)='',"
          //          + "@modified    [bigint]=0,"
          //          + "@modiuser    [varchar](20)='',"
          //          + "@userid      [varchar](20)='',"
          //          + "@recno       [bigint])"
          //          + "AS UPDATE PendingBill" + FrmCode + " SET [DATE1] = @DATE1, "
          //          + "[TRANSTYPE]=@TRANSTYPE, [orderno]=@orderno,[billamt]=@billamt,[custcode]=@custcode,"
          //          + "[SERIES]=@SERIES, [orginalseries]=@orginalseries ,[crdays]=@crdays,[bill]= @bill,[status]=@status,"
          //          + "[received]= @received,[balance]=@balance,[flag]=@flag,[paid]=@paid,"
          //          + "[invno]=@invno,[invdate]=@invdate,[remarks]=@remarks,"
          //          + "[userid]=@userid,[modiuser]=@modiuser,[modified]=(Select [modified] from PendingBill" + FrmCode + " where [recno]=@recno)+1 "
          //          + " WHERE  [recno]=@recno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  //PENDINGBILL PROCEEDURE////////////delete//////

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //            + " object_id(N'SP_PendingBill_DELETE" + FrmCode + "') ) "
          //            + " drop PROCEDURE  SP_PendingBill_DELETE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_PendingBill_DELETE" + FrmCode + "("
          //         + " @RECNO  [bigint])"
          //         + "AS DELETE FROM PENDINGBILL" + FrmCode + " WHERE "
          //         + "([RECNO]= @RECNO)";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //Pbill sub  PROCEEDURE/////////////////
          //  //Procedure for pbillsub insertion
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_PBILLSUB_INSERT" + FrmCode + "') ) "
          //  + " drop PROCEDURE  SP_PBILLSUB_INSERT" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "create procedure SP_PBILLSUB_INSERT" + FrmCode + " \n"
          //  + "(  \n"
          //  + " @CustCode        [varchar](10)='',  \n"
          //  + " @series          [varchar](1)='' ,  \n"
          //  + " @orginalseries   [varchar](1)='',  \n"
          //  + " @Bill_No         [bigint]=null,  \n"
          //  + " @Bill_From       [varchar](1)='',  \n"
          //  + " @Bill_Amount     [numeric](17,2)=0, \n"
          //  + " @Amount_Paid     [numeric](17,2)=0,  \n"
          //  + " @Amount_Given    [numeric](17,2)=0,  \n"
          //  + " @Balance         [numeric](17,2)=0,  \n"
          //  + " @status          [varchar](5)='',  \n"
          //  + " @Date_Of_Pay     [smalldatetime]=null, \n"
          //  + " @Trans_No        [bigint]=0,  \n"
          //  + " @Trans_Type      [varchar](10)='',  \n"
          //  + " @Trans_Series    [varchar](1)='', \n"
          //  + " @Trans_Flag      [varchar](5)='', \n"
          //  + " @flag            [varchar](10)='' ,  \n"
          //  + " @paid            [numeric](17, 2)=0,  \n"
          //  + " @invno           [varchar](1)='',  \n"
          //  + " @invdate         [datetime]= null,  \n"
          //  + " @modiuser        [varchar](20)='',  \n"
          //  + " @modified        [bigint]=0,  \n"
          //  + " @userid          [varchar](20)='', \n"
          //  + " @Type            [varchar](2)='' \n"
          //  + " ) as  \n"
          //  + " \n"
          //  + "if @orginalseries='P' or @orginalseries='R' or @Type='S' \n"
          //  + "    begin \n"
          //  + "        select @Balance=[balance]-(case when [balance]<0 then @Amount_Given*-1 else @Amount_Paid end)  \n"
          //  + "        from pendingbill" + FrmCode + " where [custcode]=@custcode and [orderno]=@Bill_No and  \n"
          //  + "        [orginalseries]=@orginalseries and [transtype]=@Bill_From and [status]=@status and [flag]=@flag 		 \n"
          //  + " \n"
          //  + "    if @Type='S'   \n"
          //  + "         begin  \n"
          //  + "             update pendingbill" + FrmCode + " set billamt=[billamt]-(case when [billamt]<0 then @paid*-1 else @paid end)   ,  \n"
          //  + "             balance=[balance]-(case when [balance]<0 then @paid*-1 else @paid end)   where  \n"
          //  + "             transtype=@trans_type and series=case when series in ('H','U','S','N') then  @trans_series else series end  \n"
          //  + "             and orderno=@trans_no and flag=@trans_flag    \n"
          //  + "         end   \n"
          //  + " \n"
          //  + "    end \n"
          //  + "else \n"
          //  + "    begin \n"
          //  + "        select @Balance=[balance]-(case when [balance]<0 then @Amount_Given*-1 else @Amount_Paid end)  \n"
          //  + "        from pendingbill" + FrmCode + " where [custcode]=@custcode and [orderno]=@Bill_No and [series]=@series  \n"
          //  + "        and [orginalseries]=@orginalseries and [transtype]=@Bill_From and [status]=@status and [flag]=@flag  \n"
          //  + "    end \n"
          //  + " \n"
          //  + "insert into pbillsub" + FrmCode + "([custcode],[series],[bill_no],[bill_from],[bill_amount],  \n"
          //  + "[amount_paid],[amount_given],[balance],[status],[date_of_pay],[trans_no],[trans_type],[Trans_Series],[Trans_Flag], \n"
          //  + "[flag],[paid],[invno],[invdate],[userid],[modiuser],[modified],orginalseries)  \n"
          //  + "values(@CustCode,@series,@Bill_No,@Bill_From,@Bill_Amount,@Amount_Paid ,@Amount_Given,  \n"
          //  + "@Balance,@status,@Date_Of_Pay, @Trans_No,@Trans_Type,@Trans_Series,@Trans_Flag,@flag,@paid ,@invno,@invdate,@userid,  \n"
          //  + "@modiuser,@modified,@orginalseries)  \n"
          //  + "  \n"
          //  + "declare  \n"
          //  + "  @amount [numeric](17,2)  \n"
          //  + "  \n"
          //  + "if @type='S'   \n"
          //  + "begin  \n"
          //  + "  \n"
          //  + "select @amount=isnull(sum(paid),0) from pbillsub" + FrmCode + " where custcode=@custcode and    \n"
          //  + "bill_no=@Bill_No and (series=@series or series='H')  and  \n"
          //  + "bill_from=@Bill_From  and status=@status and flag=@flag  \n"
          //  + "end  \n"
          //  + "else  \n"
          //  + "begin  \n"
          //  + "select @amount=isnull(sum(paid),0) from pbillsub" + FrmCode + " where custcode=@custcode and  \n"
          //  + "bill_no=@Bill_No and series=@series and orginalseries=@orginalseries and  \n"
          //  + "bill_from=@Bill_From  and status=@status and flag=@flag  \n"
          //  + " \n"
          //  + " end \n"
          //  + " \n"
          //  + "if @orginalseries='P' or @orginalseries='R' or @type='S'  \n"
          //  + "    begin \n"
          //  + "        update pendingbill" + FrmCode + " set received=@amount,  \n"
          //  + "        balance=billamt-(case when billamt<0 then @amount*-1 else @amount end),paid=@paid,  \n"
          //  + "        modified=modified+1,modiuser=@modiuser, \n"
          //  + "        series= case when (billamt-(case when billamt<0 then @amount*-1 else @amount end))=0 then \n"
          //  + "        @series else series end \n"
          //  + "        where custcode=@custcode and orderno=@Bill_No and transtype=@Bill_From and status=@status and flag=@flag 		 \n"
          //  + "    end \n"
          //  + "else \n"
          //  + "    begin \n"
          //  + "        update pendingbill" + FrmCode + " set received=@amount,  \n"
          //  + "        balance=billamt-(case when billamt<0 then @amount*-1 else @amount end),paid=@paid,  \n"
          //  + "        modified=modified+1,modiuser=@modiuser  \n"
          //  + "        where custcode=@custcode and orderno=@Bill_No and (series=@series OR SERIES='*') and orginalseries=@orginalseries  \n"
          //  + "        and transtype=@Bill_From and status=@status and flag=@flag \n"
          //  + "    end";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //                       + " object_id(N'SP_DELETE_PBILLSUB" + FrmCode + "') ) "
          //                       + " drop PROCEDURE  SP_DELETE_PBILLSUB" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "create procedure SP_DELETE_PBILLSUB" + FrmCode + " \n"
          //  + "( \n"
          //  + "    @Custcode 	   [varchar](10)='', \n"
          //  + "    @Trans_No 	   [bigint], \n"
          //  + "    @Trans_Type     [varchar](10)='', \n"
          //  + "    @Trans_Series   [varchar](1)='', \n "
          //  + "    @Trans_Flag     [varchar](5)='', \n "
          //  + "    @orginalseries  [varchar](1)='', \n"
          //  + "    @modiuser  	   [varchar](20)='', \n"
          //  + "    @type    	   [varchar](2) ='' \n"
          //  + ") as \n"
          //  + " \n"
          //  + "begin \n"
          //  + "    update a set a.balance = a.billamt-(case when a.billamt<0 then b.amount*-1 else b.amount end), \n"
          //  + "    a.received=b.amount,a.paid=0,a.modified=a.modified+1,a.modiuser=@modiuser \n"
          //  + "    from pendingbill" + FrmCode + "  a join    \n"
          //  + "    ( \n"
          //  + "         select bill_no,series,orginalseries,custcode,status,bill_from,flag,    \n"
          //  + "         (select isnull(sum(paid),0) from pbillsub" + FrmCode + "  where bill_no=a.bill_no and  \n"
          //  + "         series=a.series and orginalseries=a.orginalseries and custcode=a.custcode and   \n"
          //  + "         status=a.status and bill_from=a.bill_from and flag=a.flag)-paid[amount]    \n"
          //  + "         from pbillsub" + FrmCode + "  a where trans_no=@Trans_No and trans_type=@Trans_Type \n"
          //  + "         and custcode=case when @Custcode='' then custcode else @Custcode end \n"
          //  + "         and trans_series=CASE WHEN @trans_series='*' THEN trans_series ELSE @trans_series END and trans_flag=@trans_flag \n"
          //  + "    )b    \n"
          //  + "    on a.custcode=b.custcode and a.orderno=b.bill_no and a.transtype=b.bill_from and  \n"
          //  + "    a.series=case when (b.orginalseries='R' or b.orginalseries='P') then a.series else b.series end \n"
          //  + "    and a.orginalseries=b.orginalseries and a.status=b.status and a.flag=b.flag   \n"
          //  + "  \n"
          //  + "    delete from pbillsub" + FrmCode + "  where trans_no=@Trans_No and trans_type=@Trans_Type \n"
          //  + "    and custcode=case when @Custcode='' then custcode else @Custcode end \n"
          //  + "    and trans_series=CASE WHEN @trans_series='*' THEN trans_series ELSE @trans_series END and trans_flag=@trans_flag \n"
          //  + " \n"
          //  + "    if (@type<>'S')\n"
          //  + "         begin \n"
          //  + "             delete from pendingbill" + FrmCode + " where transtype=@Trans_Type and \n"
          //  + "             orderno=@Trans_No and custcode=case when @Custcode='' then custcode else @Custcode end \n"
          //  + "         end \n"
          //  + " \n"
          //  + "end";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //                                   + " object_id(N'SP_PENDINGBILL_PAY_UPDATE" + FrmCode + "') ) "
          //                                   + " drop PROCEDURE  SP_PENDINGBILL_PAY_UPDATE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROC SP_PENDINGBILL_PAY_UPDATE" + FrmCode + " ("
          //  + " @CustCode [varchar] (10) ,  "
          //  + " @series [varchar] (1) , "
          //  + " @Bill_No [bigint] ,  "
          //  + " @transtype [varchar](1),  "
          //  + " @Amount_Paid [numeric](17, 2)=0,  "
          //  + " @modiuser [varchar] (20)='',  "
          //  + " @modified [bigint]=0 ,   "
          //  + " @status [varchar](5)='',"
          //  + " @orginalseries [varchar] (1)='' )   "
          //  + " AS   "
          //  + " declare @Balance [numeric](17, 2) "
          //  + " declare @Received [numeric](17, 2) "
          //  + " IF((SELECT BALANCE FROM PENDINGBILL" + FrmCode + "  WHERE   "
          //  + " [CustCode]=@CustCode AND  [series]=@series AND[ORDERNO]=@Bill_No and "
          //  + " [transtype]=@transtype and  orginalseries=@orginalseries and [status]=@status)<0 )   "
          //  + " SET  @Balance=isnull(((select [Balance] from PENDINGBILL" + FrmCode + "  "
          //  + " where [CustCode]=@CustCode  AND [series]=@series AND   [orderno]=@Bill_No and "
          //  + " [transtype] = @transtype and  orginalseries=@orginalseries and [status]=@status)  + @Amount_Paid)  ,0) "
          //  + " ELSE   "
          //  + " SET  @BALANCE=isnull(((select [Balance] from PENDINGBILL" + FrmCode + "  where "
          //  + " [CustCode]=@CustCode  AND [series]=@series AND   [orderno]=@Bill_No and "
          //  + " [transtype] = @transtype  and orginalseries=@orginalseries and [status]=@status)  - @Amount_Paid)  ,0) "

          //  + " SET  @Received=isnull(((select [Received] from PENDINGBILL" + FrmCode + "  where "
          //  + " [CustCode]=@CustCode  AND [series]=@series AND   [orderno]=@Bill_No and "
          //  + " [transtype] = @transtype  and orginalseries=@orginalseries and [status]=@status))  ,0) "

          //  + " UPDATE PENDINGBILL" + FrmCode + " SET [Balance] =@Balance ,[Paid]=@Amount_Paid,   "
          //  + " [Received]=@Received+@Amount_Paid,  [modiuser]=@modiuser,[modified]=[modified]+1 "
          //  + " WHERE [CustCode]=@CustCode AND   "
          //  + " [series]=@series AND [orderno]=@Bill_No  and [transtype] = @transtype and "
          //  + " orginalseries=@orginalseries and [status]=@status ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //     + " object_id(N'SP_DELETE_TRAN_PBILLSUB" + FrmCode + "') ) "
          //     + " drop PROCEDURE SP_DELETE_TRAN_PBILLSUB" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE  PROCEDURE SP_DELETE_TRAN_PBILLSUB" + FrmCode + " \n"
          //  + " ( \n"
          //  + "  @Trans_No 	   [bigint], \n"
          //  + "  @Trans_Type    [varchar](10)='', \n"
          //  + "  @modiuser	varchar (15)='' \n"
          //  + " ) as \n"
          //  + "     \n"
          //  + " update a set a.balance = a.billamt-(case when a.billamt<0 then b.amount*-1 else b.amount end), \n"
          //  + " a.received=b.amount,a.paid=0,a.modified=a.modified+1,a.modiuser=@modiuser \n"
          //  + " from pendingbill" + FrmCode + "  a join    \n"
          //  + " (select bill_no,series,orginalseries,custcode,status,bill_from,flag,    \n"
          //  + " (select isnull(sum(paid),0) from pbillsub" + FrmCode + "  where bill_no=a.bill_no and    \n"
          //  + " series=a.series and orginalseries=a.orginalseries and custcode=a.custcode and \n"
          //  + " status=a.status and bill_from=a.bill_from and flag=a.flag)-paid[amount]    \n"
          //  + " from pbillsub" + FrmCode + "  a where trans_no=@Trans_No and trans_type=@Trans_Type) b    \n"
          //  + " on a.custcode=b.custcode and a.orderno=b.bill_no and a.transtype=b.bill_from and \n"
          //  + " a.series=b.series and a.orginalseries=b.orginalseries and a.status=b.status and a.flag=b.flag \n"
          //  + " \n"
          //  + " delete from pbillsub" + FrmCode + "  where trans_no=@Trans_No and trans_type=@Trans_Type";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


            

          //  /////////////////////////////////////////////////////////////////////////////////////////

          //  //   -------------  Procedure for Insert Items to STOCKMST  -----------------   

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_STOCKMST" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_STOCKMST" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_STOCKMST" + FrmCode + Constants.vbCrLf
          //  + "(@itemcode 	    [varchar](13)=''," + Constants.vbCrLf
          //  + "@itemname 	    [varchar](30)=''," + Constants.vbCrLf
          //  + "@mrp 	        [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@rprofit1        [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@rprofit2        [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@rprofit3        [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@qty             [numeric](17, 3) = 0," + Constants.vbCrLf
          //  + "@taxper          [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@cessper         [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@lc              [numeric](17, 3) = 0," + Constants.vbCrLf           
          //  + "@opstock         [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@suppcode        [varchar](10) = ''," + Constants.vbCrLf
          //  + "@groupcode       [varchar](10) = ''," + Constants.vbCrLf
          //  + "@unit            [varchar](10) = ''," + Constants.vbCrLf
          //  + "@multiunit       [varchar](10) = ''," + Constants.vbCrLf
          //  + "@Manufacturer    [varchar](100) = ''," + Constants.vbCrLf
          //  + "@product         [varchar](10) = ''," + Constants.vbCrLf
          //  + "@commodity       [varchar](15) = ''," + Constants.vbCrLf
          //  + "@schedule        [varchar](3) = ''," + Constants.vbCrLf
          //  + "@userid          [varchar](15) = ''," + Constants.vbCrLf
          //  + "@code            [varchar](13)='' output)" + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + "if @itemcode=''" + Constants.vbCrLf
          //  + "    begin " + Constants.vbCrLf
          //  + "       set @itemcode = (select isnull(max(itemcode),0)+1 from (select itemcode from stockmst" + FrmCode + "  )[t]  where len(itemcode)=7 and isnumeric(itemcode)<>0)  " + Constants.vbCrLf
          //  + "       if @itemcode <= 9999999 " + Constants.vbCrLf
          //  + "           begin " + Constants.vbCrLf
          //  + "               set @itemcode = (select substring('0000000',1,7-datalength(@itemcode))+(@itemcode)) " + Constants.vbCrLf
          //  + "           end " + Constants.vbCrLf
          //  + "       else " + Constants.vbCrLf
          //  + "           set @itemcode = '' " + Constants.vbCrLf
          //  + "    end " + Constants.vbCrLf
          //  + "set @code=@itemcode " + Constants.vbCrLf
          //  + "if @itemcode <> '' " + Constants.vbCrLf
          //  + " INSERT INTO STOCKMST" + FrmCode + " ( [itemcode], [itemname], [mrp],[rprofit1],[rprofit2],[rprofit3], " + Constants.vbCrLf
          //  + "[qty], [taxper],[cessper], [lc],  [opstock],[unit],[multiunit],  [suppcode], " + Constants.vbCrLf
          //  + "[groupcode], [Manufacturer], [product],[commodity], [schedule], [userid])" + Constants.vbCrLf
          //  + "VALUES( @itemcode, @itemname, @mrp,@rprofit1,@rprofit2,@rprofit3, @qty, @taxper,@cessper, @lc, " + Constants.vbCrLf
          //  + "@opstock, @suppcode,@unit,@multiunit, @groupcode, @Manufacturer, @product, " + Constants.vbCrLf
          //  + "@commodity, @schedule, @userid)" + Constants.vbCrLf
          //  + "select @code";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();         

          //  //   -------------  Procedure for Update Items in STOCKMST  -----------------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_UPDATE_STOCKMST" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_UPDATE_STOCKMST" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_UPDATE_STOCKMST" + FrmCode + Constants.vbCrLf
          //  + "(@tag		[varchar](1)='', " + Constants.vbCrLf
          //  + "@itemcode 	[varchar](13)," + Constants.vbCrLf
          //  + "@itemname 	[varchar](30)," + Constants.vbCrLf
          //  + "@mrp 	    [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@rprofit1 	[numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@rprofit2 	[numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@rprofit3 	[numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@qty         [numeric](17, 3) = 0," + Constants.vbCrLf
          //  + "@taxper      [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@cessper     [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@lc          [numeric](17, 3) = 0," + Constants.vbCrLf
          //  + "@opstock     [numeric](17, 2) = 0," + Constants.vbCrLf
          //  + "@suppcode    [varchar](10) = ''," + Constants.vbCrLf
          //  + "@unit        [varchar](10) = ''," + Constants.vbCrLf
          //  + "@multiunit   [varchar](10) = ''," + Constants.vbCrLf
          //  + "@groupcode   [varchar](10) = ''," + Constants.vbCrLf
          //  + "@Manufacturer    [varchar](100) = ''," + Constants.vbCrLf
          //  + "@product     [varchar](10) = ''," + Constants.vbCrLf
          //  + "@commodity   [varchar](15) = ''," + Constants.vbCrLf
          //  + "@schedule    [varchar](3) = ''," + Constants.vbCrLf
          //  + "@userid      [varchar](15) = '' )" + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + "if @tag=1      --- Tag 1 Will Update All Fields." + Constants.vbCrLf
          //  + "    begin" + Constants.vbCrLf
          //  + "         UPDATE STOCKMST" + FrmCode + " SET [itemname] = @itemname, " + Constants.vbCrLf
          //  + "         [mrp] = @mrp,[rprofit1]=@rprofit1,[rprofit2]=@rprofit2,[rprofit3]=@rprofit3, [qty] = @qty, [taxper] = @taxper,[cessper]=@cessper, " + Constants.vbCrLf
          //  + "         [lc] = @lc,  " + Constants.vbCrLf
          //  + "         [opstock] =  @opstock, [suppcode] = @suppcode, " + Constants.vbCrLf
          //  + "         [groupcode] = @groupcode, [Manufacturer] = @Manufacturer, [product] = @product,[unit]=@unit,[multiunit]=@multiunit,  " + Constants.vbCrLf
          //  + "         [commodity] = @commodity, [schedule] = @schedule, [userid] = @userid where [itemcode] = @itemcode " + Constants.vbCrLf
          //  + "    end" + Constants.vbCrLf
          //  + "else " + Constants.vbCrLf
          //  + "    begin" + Constants.vbCrLf
          //  + "         UPDATE STOCKMST" + FrmCode + " SET [itemname] = @itemname,  " + Constants.vbCrLf
          //  + "         [mrp] = @mrp,[rprofit1]=@rprofit1,[rprofit2]=@rprofit2,[rprofit3]=@rprofit3, [taxper] = @taxper,[cessper]=@cessper,  " + Constants.vbCrLf
          //  + "         [suppcode] = @suppcode, [groupcode] = @groupcode, " + Constants.vbCrLf
          //  + "         [Manufacturer] = @Manufacturer, [product] = @product, [unit]=@unit,[multiunit]=@multiunit, " + Constants.vbCrLf
          //  + "         [commodity] = @commodity, [schedule] = @schedule, [userid] = @userid " + Constants.vbCrLf
          //  + "         where [itemcode] = @itemcode " + Constants.vbCrLf
          //  + "   end";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -------  Procedure for Delete Records form STOCKMST1 --------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_STOCKMST" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_STOCKMST" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_STOCKMST" + FrmCode + Constants.vbCrLf
          //  + "(@itemcode 	[varchar](13))" + Constants.vbCrLf
          //  + "as delete from STOCKMST" + FrmCode + " where [itemcode] = @itemcode";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();
      
          //  //  -----  Procedure for Insert SALES -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_SALES" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_SALES" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_SALES" + FrmCode + Constants.vbCrLf
          //      + "(@orderno    [bigint]=0, " + Constants.vbCrLf
          //      + "@date1       [datetime]," + Constants.vbCrLf
          //      + "@form        [varchar] (1)=''," + Constants.vbCrLf
          //      + "@billtype    [varchar] (1)=''," + Constants.vbCrLf
          //      + "@pricetype   [varchar] (1)=''," + Constants.vbCrLf
          //      + "@custcode    [varchar] (10) =''," + Constants.vbCrLf
          //      + "@custname    [varchar] (40)=''," + Constants.vbCrLf
          //      + "@pcustname   [varchar](40)=''," + Constants.vbCrLf
          //      + "@paddress    [varchar](40)=''," + Constants.vbCrLf
          //      + "@pcontactno  [varchar](20)=''," + Constants.vbCrLf
          //      + "@salesperson [varchar] (100)=''," + Constants.vbCrLf
          //      + "@billamt     [numeric](17, 2)=0," + Constants.vbCrLf
          //      + "@expense     [numeric](17, 2)=0," + Constants.vbCrLf
          //      + "@spdiscamt   [numeric](17, 2) =0 ," + Constants.vbCrLf
          //      + "@bildiscper  [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@bildiscamt  [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@itemtotal   [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@taxamt      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@cessAmt     [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@roundoff    [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@SALESORDERNO    [bigint]=0," + Constants.vbCrLf
          //      + "@remarks1    [varchar] (200)=''," + Constants.vbCrLf
          //      + "@userid      [varchar] (15) =''," + Constants.vbCrLf
          //      + "@modified      [int]  =0," + Constants.vbCrLf
          //      + "@STATUS 	    [varchar](1)='0'," + Constants.vbCrLf
          //      + "@modiuser    [varchar] (15)='')" + Constants.vbCrLf
          //      + "AS " + Constants.vbCrLf
          //      + "    begin  " + Constants.vbCrLf
          //      + "         SET @orderno=(SELECT isnull(MAX(ORDERNO),0)+1 from SALES" + FrmCode + " with(XLOCK) )" + Constants.vbCrLf
          //      + "    end  " + Constants.vbCrLf
          //      + " INSERT INTO SALES" + FrmCode + " with(XLOCK) ([DATE1],[ORDERNO],[FORM],[BILLTYPE],[PRICETYPE],[CUSTCODE]," + Constants.vbCrLf
          //      + " [CUSTNAME],[PCUSTNAME] ,[PADDRESS],[PCONTACTNO], [SALESPERSON],[BILLAMT],[EXPENSE],[SPDISCAMT],[BILDISCPER]," + Constants.vbCrLf
          //      + " [BILDISCAMT],[ITEMTOTAL],[TAXAMT],[CESSAMT],[ROUNDOFF],[SALESORDERNO],[REMARKS1],[USERID]," + Constants.vbCrLf
          //      + " [MODIFIED],[MODIUSER],[STATUS])" + Constants.vbCrLf
          //      + " VALUES(@date1,@orderno,@form,@billtype,@pricetype,@custcode,@custname,@pcustname  ,@paddress ,@pcontactno,@salesperson," + Constants.vbCrLf
          //      + " @billamt,@expense,@spdiscamt,@bildiscper,@bildiscamt,@itemtotal,@taxamt,@cessAmt," + Constants.vbCrLf
          //      + " @roundoff,@SALESORDERNO,@remarks1,@userid,@modified,@userid,@STATUS)" + Constants.vbCrLf
          //      + " SELECT @orderno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  //  -----  Procedure for Insert SITEM -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_SITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_SITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_SITEM" + FrmCode + Constants.vbCrLf
          //     + "(@DATE1		[datetime]," + Constants.vbCrLf
          //     + "@ORDERNO 	    [bigint]," + Constants.vbCrLf
          //     + "@RATE 		[numeric](17,2)=0," + Constants.vbCrLf
          //     + "@BILLTYPE 	[varchar](1)=''," + Constants.vbCrLf
          //     + "@ITEMCODE 	[varchar](20)=''," + Constants.vbCrLf
          //     + "@ITEMNAME 	[varchar](40)=''," + Constants.vbCrLf
          //     + "@NETAMT 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@QTY	 	    [numeric](18,3)=0," + Constants.vbCrLf
          //     + "@FOC	 	    [numeric](18,3)=0," + Constants.vbCrLf
          //     + "@UNIT	 	    [varchar](20)=''," + Constants.vbCrLf
          //     + "@UNITVALUE    [numeric](18,3)=0," + Constants.vbCrLf
          //     + "@PRICE 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@TAXPER 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@TAXAMT 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@CESSPER      [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@CESSAMT      [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@DISCPER 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@DISCAMT 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@ITEMTOTAL 	[numeric](17,2)=0," + Constants.vbCrLf
          //     + "@LC	 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@ACCCODE 	[varchar](1)=''," + Constants.vbCrLf
          //     + "@REMARKS 	[varchar](120)=''," + Constants.vbCrLf
          //     + "@PRICETYPE 	[varchar](10)='')" + Constants.vbCrLf
          //     + " AS " + Constants.vbCrLf
          //     + "  " + Constants.vbCrLf
          //     + " INSERT INTO SITEM" + FrmCode + "( [DATE1],[ORDERNO],[RATE],[BILLTYPE],[ITEMCODE],[ITEMNAME]," + Constants.vbCrLf
          //     + " [NETAMT],[QTY],[FOC],[UNIT],[UNITVALUE],[PRICE],[TAXPER],[TAXAMT],[CESSPER],CESSAMT,[DISCPER],[DISCAMT],[ITEMTOTAL]," + Constants.vbCrLf
          //     + " [LC],[ACCCODE],[REMARKS])" + Constants.vbCrLf
          //     + " VALUES (@DATE1,@ORDERNO,@RATE,@BILLTYPE,@ITEMCODE,@ITEMNAME,@NETAMT,@QTY,@FOC,@UNIT,@UNITVALUE,@PRICE," + Constants.vbCrLf
          //     + " @TAXPER,@TAXAMT,@CESSPER,@CESSAMT,@DISCPER,@DISCAMT,@ITEMTOTAL,@LC, " + Constants.vbCrLf
          //     + " @ACCCODE,@REMARKS) " + Constants.vbCrLf
          //     + "  " + Constants.vbCrLf
          //     + "  UPDATE a set qty=qty-((@qty+@foc)*@unitvalue) from stockmst" + FrmCode + " A where itemcode=@itemcode  " + Constants.vbCrLf
          //     + "   " + Constants.vbCrLf
          //     + " ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for UPDATE SALES -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_UPDATE_SALES" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_UPDATE_SALES" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_UPDATE_SALES" + FrmCode
          //      + " (@date1         [datetime]," + Constants.vbCrLf
          //      + " @orderno        [bigint]," + Constants.vbCrLf
          //      + " @form           [varchar] (1)=''," + Constants.vbCrLf
          //      + " @billtype       [varchar] (1)=''," + Constants.vbCrLf
          //      + " @pricetype      [varchar] (1)=''," + Constants.vbCrLf
          //      + " @custcode       [varchar] (10)='' ," + Constants.vbCrLf
          //      + " @custname       [varchar] (40)=''," + Constants.vbCrLf
          //      + " @pcustname      [varchar](40)=''," + Constants.vbCrLf
          //      + " @paddress       [varchar](40)=''," + Constants.vbCrLf
          //      + " @pcontactno     [varchar](20)=''," + Constants.vbCrLf
          //      + " @salesperson    [varchar] (100)=''," + Constants.vbCrLf
          //      + " @billamt        [numeric](17,2)=0," + Constants.vbCrLf
          //      + " @expense        [numeric](17,2)=0," + Constants.vbCrLf
          //      + " @spdiscamt      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @bildiscper     [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @bildiscamt     [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @itemtotal      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @taxamt         [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @cessAmt        [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @roundoff       [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @SALESORDERNO   [bigint]=0," + Constants.vbCrLf
          //      + " @remarks1       [varchar] (200)=''," + Constants.vbCrLf
          //      + " @STATUS 	    [varchar](1)='0'," + Constants.vbCrLf
          //      + " @modiuser       [varchar] (15)='')" + Constants.vbCrLf
          //      + " AS UPDATE SALES" + FrmCode + " SET [DATE1]=@date1,[FORM]=@form,[BILLTYPE]=@billtype,[PRICETYPE]=@pricetype,[CUSTCODE]=@custcode," + Constants.vbCrLf
          //      + " [CUSTNAME]=@custname,[PCUSTNAME]=@pcustname ,[PADDRESS]=@paddress,[PCONTACTNO]=@pcontactno,[SALESPERSON]=@salesperson, " + Constants.vbCrLf
          //      + " " + Constants.vbCrLf
          //      + " [BILLAMT]=@billamt,[EXPENSE]=@expense," + Constants.vbCrLf
          //      + " [SPDISCAMT]=@spdiscamt,[BILDISCPER]=@bildiscper,[BILDISCAMT]=@bildiscamt,[ITEMTOTAL]=@itemtotal," + Constants.vbCrLf
          //      + " [TAXAMT]=@taxamt,[CESSAMT]=@cessAmt,[ROUNDOFF]=@roundoff,[SALESORDERNO]=@SALESORDERNO,[REMARKS1]=@remarks1,  " + Constants.vbCrLf
          //      + " [MODIFIED]=[MODIFIED]+1,[MODIUSER]=@modiuser,[STATUS]=@STATUS  WHERE [ORDERNO]=@orderno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  //  -----  Procedure for CANCEL SALES -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_CANCEL_SALES" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_CANCEL_SALES" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_CANCEL_SALES" + FrmCode + Constants.vbCrLf
          //  + " (@orderno    [bigint]," + Constants.vbCrLf
          //  + " @custcode   [varchar] (10)," + Constants.vbCrLf
          //  + " @billamt    [numeric](17,2)," + Constants.vbCrLf
          //  + " @modiuser   [varchar] (15)) " + Constants.vbCrLf
          //  + " AS UPDATE SALES" + FrmCode + " SET [CUSTCODE]=@custcode,[BILLAMT]=@billamt, " + Constants.vbCrLf
          //  + " [MODIFIED]=[MODIFIED]+1,[MODIUSER]=@modiuser WHERE [ORDERNO]=@orderno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for DELETE SITEM -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_SITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_SITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_SITEM" + FrmCode
          //  + " (@orderno BIGINT)" + Constants.vbCrLf
          //  + " AS  " + Environment.NewLine
          //  + " UPDATE A SET A.QTY=A.QTY+B.QTY FROM STOCKMST" + FrmCode + " A JOIN  " + Constants.vbCrLf
          //  + " (SELECT ITEMCODE,SUM((QTY+FOC)*unitvalue) QTY FROM SITEM" + FrmCode + " WHERE [ORDERNO]=@orderno    " + Constants.vbCrLf
          //  + "  GROUP BY ITEMCODE) B ON A.ITEMCODE=B.ITEMCODE " + Constants.vbCrLf
          //  + " DELETE FROM SITEM" + FrmCode + " WHERE [ORDERNO]=@orderno ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();




          //  //  -----  Procedure for Insert SALES RETURN -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_SRETURN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_SRETURN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_SRETURN" + FrmCode + Constants.vbCrLf
          //      + "(@orderno        [bigint]=0, " + Constants.vbCrLf
          //      + "@date1           [datetime]," + Constants.vbCrLf
          //      + "@form            [varchar] (1)=''," + Constants.vbCrLf
          //      + "@billtype        [varchar] (1)=''," + Constants.vbCrLf
          //      + "@pricetype       [varchar] (1)=''," + Constants.vbCrLf
          //      + "@custcode        [varchar] (10) =''," + Constants.vbCrLf
          //      + "@custname        [varchar] (40)=''," + Constants.vbCrLf
          //      + "@pcustname       [varchar](40)=''," + Constants.vbCrLf
          //      + "@paddress        [varchar](40)=''," + Constants.vbCrLf
          //      + "@pcontactno      [varchar](20)=''," + Constants.vbCrLf
          //      + "@salesperson     [varchar] (100)=''," + Constants.vbCrLf
          //      + "@billamt         [numeric](17, 2)=0," + Constants.vbCrLf
          //      + "@expense         [numeric](17, 2)=0," + Constants.vbCrLf
          //      + "@spdiscamt       [numeric](17, 2) =0 ," + Constants.vbCrLf
          //      + "@bildiscper      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@bildiscamt      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@itemtotal       [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@taxamt          [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@cessAmt         [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@roundoff        [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@SALESORDERNO    [bigint]=0," + Constants.vbCrLf
          //      + "@remarks1        [varchar] (200)=''," + Constants.vbCrLf
          //      + "@userid          [varchar] (15) =''," + Constants.vbCrLf
          //      + "@modified        [int]  =0," + Constants.vbCrLf
          //      + "@STATUS 	        [varchar](1)='0'," + Constants.vbCrLf
          //      + "@modiuser        [varchar] (15)='')" + Constants.vbCrLf
          //      + "AS " + Constants.vbCrLf
          //      + "    begin  " + Constants.vbCrLf
          //      + "         SET @orderno=(SELECT isnull(MAX(ORDERNO),0)+1 from SRETURN" + FrmCode + " with(XLOCK) )" + Constants.vbCrLf
          //      + "    end  " + Constants.vbCrLf
          //      + " INSERT INTO SRETURN" + FrmCode + " with(XLOCK) ([DATE1],[ORDERNO],[FORM],[BILLTYPE],[PRICETYPE],[CUSTCODE]," + Constants.vbCrLf
          //      + " [CUSTNAME],[PCUSTNAME] ,[PADDRESS],[PCONTACTNO],[SALESPERSON],[BILLAMT],[EXPENSE],[SPDISCAMT],[BILDISCPER]," + Constants.vbCrLf
          //      + " [BILDISCAMT],[ITEMTOTAL],[TAXAMT],[CESSAMT],[ROUNDOFF],[SALESORDERNO],[REMARKS1],[USERID]," + Constants.vbCrLf
          //      + " [MODIFIED],[MODIUSER],[STATUS])" + Constants.vbCrLf
          //      + " VALUES(@date1,@orderno,@form,@billtype,@pricetype,@custcode,@custname,@pcustname  ,@paddress ,@pcontactno,@salesperson," + Constants.vbCrLf
          //      + " @billamt,@expense,@spdiscamt,@bildiscper,@bildiscamt,@itemtotal,@taxamt,@cessAmt," + Constants.vbCrLf
          //      + " @roundoff,@SALESORDERNO,@remarks1,@userid,@modified,@userid,@STATUS)" + Constants.vbCrLf
          //      + " SELECT @orderno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  //  -----  Procedure for Insert SITEM -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_SRITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_SRITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_SRITEM" + FrmCode + Constants.vbCrLf
          //     + "(@DATE1		[datetime]," + Constants.vbCrLf
          //     + "@ORDERNO 	    [bigint]," + Constants.vbCrLf
          //     + "@RATE 		[numeric](17,2)=0," + Constants.vbCrLf
          //     + "@BILLTYPE 	[varchar](1)=''," + Constants.vbCrLf
          //     + "@ITEMCODE 	[varchar](20)=''," + Constants.vbCrLf
          //     + "@ITEMNAME 	[varchar](40)=''," + Constants.vbCrLf
          //     + "@NETAMT 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@QTY	 	    [numeric](18,3)=0," + Constants.vbCrLf
          //     + "@FOC	 	    [numeric](18,3)=0," + Constants.vbCrLf
          //     + "@UNIT	 	    [varchar](20)=''," + Constants.vbCrLf
          //     + "@UNITVALUE	[numeric](18,3)=0," + Constants.vbCrLf
          //     + "@PRICE 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@TAXPER 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@TAXAMT 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@CESSPER      [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@CESSAMT      [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@DISCPER 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@DISCAMT 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@ITEMTOTAL 	[numeric](17,2)=0," + Constants.vbCrLf
          //     + "@LC	 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@ACCCODE 	    [varchar](1)=''," + Constants.vbCrLf
          //     + "@REMARKS 	    [varchar](120)=''," + Constants.vbCrLf
          //     + "@PRICETYPE 	[varchar](10)='')" + Constants.vbCrLf
          //     + " AS " + Constants.vbCrLf
          //     + "  " + Constants.vbCrLf
          //     + " INSERT INTO SRITEM" + FrmCode + "( [DATE1],[ORDERNO],[RATE],[BILLTYPE],[ITEMCODE],[ITEMNAME]," + Constants.vbCrLf
          //     + " [NETAMT],[QTY],[FOC],[UNIT],[UNITVALUE],[PRICE],[TAXPER],[TAXAMT],[CESSPER],CESSAMT,[DISCPER],[DISCAMT],[ITEMTOTAL]," + Constants.vbCrLf
          //     + " [LC],[ACCCODE],[REMARKS])" + Constants.vbCrLf
          //     + " VALUES (@DATE1,@ORDERNO,@RATE,@BILLTYPE,@ITEMCODE,@ITEMNAME,@NETAMT,@QTY,@FOC,@UNIT,@UNITVALUE,@PRICE," + Constants.vbCrLf
          //     + " @TAXPER,@TAXAMT,@CESSPER,@CESSAMT,@DISCPER,@DISCAMT,@ITEMTOTAL,@LC, " + Constants.vbCrLf
          //     + " @ACCCODE,@REMARKS) " + Constants.vbCrLf
          //     + "  " + Constants.vbCrLf
          //     + "  UPDATE a set qty=qty+((@qty+@foc)*@unitvalue) from stockmst" + FrmCode + " A where itemcode=@itemcode  " + Constants.vbCrLf
          //     + "   " + Constants.vbCrLf
          //     + " ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for UPDATE SALES RETURN -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_UPDATE_SRETURN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_UPDATE_SRETURN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_UPDATE_SRETURN" + FrmCode
          //      + " (@date1         [datetime]," + Constants.vbCrLf
          //      + " @orderno        [bigint]," + Constants.vbCrLf
          //      + " @form           [varchar] (1)=''," + Constants.vbCrLf
          //      + " @billtype       [varchar] (1)=''," + Constants.vbCrLf
          //      + " @pricetype      [varchar] (1)=''," + Constants.vbCrLf
          //      + " @custcode       [varchar] (10)='' ," + Constants.vbCrLf
          //      + " @custname       [varchar] (40)=''," + Constants.vbCrLf
          //      + " @pcustname      [varchar](40)=''," + Constants.vbCrLf
          //      + " @paddress       [varchar](40)=''," + Constants.vbCrLf
          //      + " @pcontactno     [varchar](20)=''," + Constants.vbCrLf
          //      + " @salesperson    [varchar] (100)=''," + Constants.vbCrLf
          //      + " @billamt        [numeric](17,2)=0," + Constants.vbCrLf
          //      + " @expense        [numeric](17,2)=0," + Constants.vbCrLf
          //      + " @spdiscamt      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @bildiscper     [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @bildiscamt     [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @itemtotal      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @taxamt         [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @cessAmt        [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @roundoff       [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @SALESORDERNO   [bigint]=0," + Constants.vbCrLf
          //      + " @remarks1       [varchar] (200)=''," + Constants.vbCrLf
          //      + " @STATUS 	    [varchar](1)='0'," + Constants.vbCrLf
          //      + " @modiuser       [varchar] (15)='')" + Constants.vbCrLf
          //      + " AS UPDATE SRETURN" + FrmCode + " SET [DATE1]=@date1,[FORM]=@form,[BILLTYPE]=@billtype,[PRICETYPE]=@pricetype,[CUSTCODE]=@custcode," + Constants.vbCrLf
          //      + " [CUSTNAME]=@custname,[PCUSTNAME]=@pcustname ,[PADDRESS]=@paddress,[PCONTACTNO]=@pcontactno,[SALESPERSON]=@salesperson, " + Constants.vbCrLf
          //      + " " + Constants.vbCrLf
          //      + " [BILLAMT]=@billamt,[EXPENSE]=@expense," + Constants.vbCrLf
          //      + " [SPDISCAMT]=@spdiscamt,[BILDISCPER]=@bildiscper,[BILDISCAMT]=@bildiscamt,[ITEMTOTAL]=@itemtotal," + Constants.vbCrLf
          //      + " [TAXAMT]=@taxamt,[CESSAMT]=@cessAmt,[ROUNDOFF]=@roundoff,[SALESORDERNO]=@SALESORDERNO,[REMARKS1]=@remarks1,  " + Constants.vbCrLf
          //      + " [MODIFIED]=[MODIFIED]+1,[MODIUSER]=@modiuser,[STATUS]=@STATUS  WHERE [ORDERNO]=@orderno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  //  -----  Procedure for CANCEL SALES RETURN -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_CANCEL_SRETURN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_CANCEL_SRETURN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_CANCEL_SRETURN" + FrmCode + Constants.vbCrLf
          //  + " (@orderno    [bigint]," + Constants.vbCrLf
          //  + " @custcode   [varchar] (10)," + Constants.vbCrLf
          //  + " @billamt    [numeric](17,2)," + Constants.vbCrLf
          //  + " @modiuser   [varchar] (15)) " + Constants.vbCrLf
          //  + " AS UPDATE SRETURN" + FrmCode + " SET [CUSTCODE]=@custcode,[BILLAMT]=@billamt, " + Constants.vbCrLf
          //  + " [MODIFIED]=[MODIFIED]+1,[MODIUSER]=@modiuser WHERE [ORDERNO]=@orderno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for DELETE SITEM -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_SRITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_SRITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_SRITEM" + FrmCode
          //  + " (@orderno BIGINT)" + Constants.vbCrLf
          //  + " AS  " + Environment.NewLine
          //  + " UPDATE A SET A.QTY=A.QTY-B.QTY FROM STOCKMST" + FrmCode + " A JOIN  " + Constants.vbCrLf
          //  + " (SELECT ITEMCODE,SUM((QTY+foc)*unitvalue) QTY FROM SRITEM" + FrmCode + " WHERE [ORDERNO]=@orderno    " + Constants.vbCrLf
          //  + "  GROUP BY ITEMCODE) B ON A.ITEMCODE=B.ITEMCODE " + Constants.vbCrLf
          //  + " DELETE FROM SRITEM" + FrmCode + " WHERE [ORDERNO]=@orderno ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  #region Transfer Out 

          //  //  -----  Procedure for Insert TRANSFER -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_TRANSFER" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_TRANSFER" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_TRANSFER" + FrmCode + Constants.vbCrLf
          //      + "(@orderno    [bigint]=0, " + Constants.vbCrLf
          //      + "@date1       [datetime]," + Constants.vbCrLf
          //      + "@form        [varchar] (1)=''," + Constants.vbCrLf
          //      + "@billtype    [varchar] (1)=''," + Constants.vbCrLf
          //      + "@pricetype   [varchar] (1)=''," + Constants.vbCrLf
          //      + "@custcode    [varchar] (10) =''," + Constants.vbCrLf
          //      + "@custname    [varchar] (40)=''," + Constants.vbCrLf
          //      + "@pcustname   [varchar](40)=''," + Constants.vbCrLf
          //      + "@paddress    [varchar](40)=''," + Constants.vbCrLf
          //      + "@pcontactno  [varchar](20)=''," + Constants.vbCrLf
          //      + "@salesperson [varchar] (100)=''," + Constants.vbCrLf
          //      + "@billamt     [numeric](17, 2)=0," + Constants.vbCrLf
          //      + "@expense     [numeric](17, 2)=0," + Constants.vbCrLf
          //      + "@spdiscamt   [numeric](17, 2) =0 ," + Constants.vbCrLf
          //      + "@bildiscper  [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@bildiscamt  [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@itemtotal   [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@taxamt      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@cessAmt     [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@roundoff    [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + "@SALESORDERNO    [bigint]=0," + Constants.vbCrLf
          //      + "@remarks1    [varchar] (200)=''," + Constants.vbCrLf
          //      + "@userid      [varchar] (15) =''," + Constants.vbCrLf
          //      + "@modified      [int]  =0," + Constants.vbCrLf
          //      + "@STATUS 	    [varchar](1)='0'," + Constants.vbCrLf
          //      + "@modiuser    [varchar] (15)='')" + Constants.vbCrLf
          //      + "AS " + Constants.vbCrLf
          //      + "    begin  " + Constants.vbCrLf
          //      + "         SET @orderno=(SELECT isnull(MAX(ORDERNO),0)+1 from TRANSFER" + FrmCode + " with(XLOCK) )" + Constants.vbCrLf
          //      + "    end  " + Constants.vbCrLf
          //      + " INSERT INTO TRANSFER" + FrmCode + " with(XLOCK) ([DATE1],[ORDERNO],[FORM],[BILLTYPE],[PRICETYPE],[CUSTCODE]," + Constants.vbCrLf
          //      + " [CUSTNAME],[PCUSTNAME] ,[PADDRESS],[PCONTACTNO], [SALESPERSON],[BILLAMT],[EXPENSE],[SPDISCAMT],[BILDISCPER]," + Constants.vbCrLf
          //      + " [BILDISCAMT],[ITEMTOTAL],[TAXAMT],[CESSAMT],[ROUNDOFF],[SALESORDERNO],[REMARKS1],[USERID]," + Constants.vbCrLf
          //      + " [MODIFIED],[MODIUSER],[STATUS])" + Constants.vbCrLf
          //      + " VALUES(@date1,@orderno,@form,@billtype,@pricetype,@custcode,@custname,@pcustname  ,@paddress ,@pcontactno,@salesperson," + Constants.vbCrLf
          //      + " @billamt,@expense,@spdiscamt,@bildiscper,@bildiscamt,@itemtotal,@taxamt,@cessAmt," + Constants.vbCrLf
          //      + " @roundoff,@SALESORDERNO,@remarks1,@userid,@modified,@userid,@STATUS)" + Constants.vbCrLf
          //      + " SELECT @orderno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  //  -----  Procedure for Insert TRITEM -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_TRITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_TRITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_TRITEM" + FrmCode + Constants.vbCrLf
          //     + "(@DATE1		[datetime]," + Constants.vbCrLf
          //     + "@ORDERNO 	    [bigint]," + Constants.vbCrLf
          //     + "@RATE 		[numeric](17,2)=0," + Constants.vbCrLf
          //     + "@BILLTYPE 	[varchar](1)=''," + Constants.vbCrLf
          //     + "@ITEMCODE 	[varchar](20)=''," + Constants.vbCrLf
          //     + "@ITEMNAME 	[varchar](40)=''," + Constants.vbCrLf
          //     + "@NETAMT 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@QTY	 	    [numeric](18,3)=0," + Constants.vbCrLf
          //     + "@FOC	 	    [numeric](18,3)=0," + Constants.vbCrLf
          //     + "@UNIT	 	    [varchar](20)=''," + Constants.vbCrLf
          //     + "@UNITVALUE    [numeric](18,3)=0," + Constants.vbCrLf
          //     + "@PRICE 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@TAXPER 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@TAXAMT 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@CESSPER      [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@CESSAMT      [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@DISCPER 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@DISCAMT 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@ITEMTOTAL 	[numeric](17,2)=0," + Constants.vbCrLf
          //     + "@LC	 	    [numeric](17,2)=0," + Constants.vbCrLf
          //     + "@ACCCODE 	[varchar](1)=''," + Constants.vbCrLf
          //     + "@REMARKS 	[varchar](120)=''," + Constants.vbCrLf
          //     + "@PRICETYPE 	[varchar](10)='')" + Constants.vbCrLf
          //     + " AS " + Constants.vbCrLf
          //     + "  " + Constants.vbCrLf
          //     + " INSERT INTO TRITEM" + FrmCode + "( [DATE1],[ORDERNO],[RATE],[BILLTYPE],[ITEMCODE],[ITEMNAME]," + Constants.vbCrLf
          //     + " [NETAMT],[QTY],[FOC],[UNIT],[UNITVALUE],[PRICE],[TAXPER],[TAXAMT],[CESSPER],CESSAMT,[DISCPER],[DISCAMT],[ITEMTOTAL]," + Constants.vbCrLf
          //     + " [LC],[ACCCODE],[REMARKS])" + Constants.vbCrLf
          //     + " VALUES (@DATE1,@ORDERNO,@RATE,@BILLTYPE,@ITEMCODE,@ITEMNAME,@NETAMT,@QTY,@FOC,@UNIT,@UNITVALUE,@PRICE," + Constants.vbCrLf
          //     + " @TAXPER,@TAXAMT,@CESSPER,@CESSAMT,@DISCPER,@DISCAMT,@ITEMTOTAL,@LC, " + Constants.vbCrLf
          //     + " @ACCCODE,@REMARKS) " + Constants.vbCrLf
          //     + "  " + Constants.vbCrLf
          //     + "  UPDATE a set qty=qty-((@qty+@foc)*@unitvalue) from stockmst" + FrmCode + " A where itemcode=@itemcode  " + Constants.vbCrLf
          //     + "   " + Constants.vbCrLf
          //     + " ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for UPDATE TRANSFER -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_UPDATE_TRANSFER" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_UPDATE_TRANSFER" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_UPDATE_TRANSFER" + FrmCode
          //      + " (@date1         [datetime]," + Constants.vbCrLf
          //      + " @orderno        [bigint]," + Constants.vbCrLf
          //      + " @form           [varchar] (1)=''," + Constants.vbCrLf
          //      + " @billtype       [varchar] (1)=''," + Constants.vbCrLf
          //      + " @pricetype      [varchar] (1)=''," + Constants.vbCrLf
          //      + " @custcode       [varchar] (10)='' ," + Constants.vbCrLf
          //      + " @custname       [varchar] (40)=''," + Constants.vbCrLf
          //      + " @pcustname      [varchar](40)=''," + Constants.vbCrLf
          //      + " @paddress       [varchar](40)=''," + Constants.vbCrLf
          //      + " @pcontactno     [varchar](20)=''," + Constants.vbCrLf
          //      + " @salesperson    [varchar] (100)=''," + Constants.vbCrLf
          //      + " @billamt        [numeric](17,2)=0," + Constants.vbCrLf
          //      + " @expense        [numeric](17,2)=0," + Constants.vbCrLf
          //      + " @spdiscamt      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @bildiscper     [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @bildiscamt     [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @itemtotal      [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @taxamt         [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @cessAmt        [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @roundoff       [numeric](17,2) =0 ," + Constants.vbCrLf
          //      + " @SALESORDERNO   [bigint]=0," + Constants.vbCrLf
          //      + " @remarks1       [varchar] (200)=''," + Constants.vbCrLf
          //      + " @STATUS 	    [varchar](1)='0'," + Constants.vbCrLf
          //      + " @modiuser       [varchar] (15)='')" + Constants.vbCrLf
          //      + " AS UPDATE TRANSFER" + FrmCode + " SET [DATE1]=@date1,[FORM]=@form,[BILLTYPE]=@billtype,[PRICETYPE]=@pricetype,[CUSTCODE]=@custcode," + Constants.vbCrLf
          //      + " [CUSTNAME]=@custname,[PCUSTNAME]=@pcustname ,[PADDRESS]=@paddress,[PCONTACTNO]=@pcontactno,[SALESPERSON]=@salesperson, " + Constants.vbCrLf
          //      + " " + Constants.vbCrLf
          //      + " [BILLAMT]=@billamt,[EXPENSE]=@expense," + Constants.vbCrLf
          //      + " [SPDISCAMT]=@spdiscamt,[BILDISCPER]=@bildiscper,[BILDISCAMT]=@bildiscamt,[ITEMTOTAL]=@itemtotal," + Constants.vbCrLf
          //      + " [TAXAMT]=@taxamt,[CESSAMT]=@cessAmt,[ROUNDOFF]=@roundoff,[SALESORDERNO]=@SALESORDERNO,[REMARKS1]=@remarks1,  " + Constants.vbCrLf
          //      + " [MODIFIED]=[MODIFIED]+1,[MODIUSER]=@modiuser,[STATUS]=@STATUS  WHERE [ORDERNO]=@orderno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  //  -----  Procedure for CANCEL TRANSFER -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_CANCEL_TRANSFER" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_CANCEL_TRANSFER" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_CANCEL_TRANSFER" + FrmCode + Constants.vbCrLf
          //  + " (@orderno    [bigint]," + Constants.vbCrLf
          //  + " @custcode   [varchar] (10)," + Constants.vbCrLf
          //  + " @billamt    [numeric](17,2)," + Constants.vbCrLf
          //  + " @modiuser   [varchar] (15)) " + Constants.vbCrLf
          //  + " AS UPDATE TRANSFER" + FrmCode + " SET [CUSTCODE]=@custcode,[BILLAMT]=@billamt, " + Constants.vbCrLf
          //  + " [MODIFIED]=[MODIFIED]+1,[MODIUSER]=@modiuser WHERE [ORDERNO]=@orderno";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for DELETE TRITEM -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_TRITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_TRITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_TRITEM" + FrmCode
          //  + " (@orderno BIGINT)" + Constants.vbCrLf
          //  + " AS  " + Environment.NewLine
          //  + " UPDATE A SET A.QTY=A.QTY+B.QTY FROM STOCKMST" + FrmCode + " A JOIN  " + Constants.vbCrLf
          //  + " (SELECT ITEMCODE,SUM((QTY+FOC)*unitvalue) QTY FROM TRITEM" + FrmCode + " WHERE [ORDERNO]=@orderno    " + Constants.vbCrLf
          //  + "  GROUP BY ITEMCODE) B ON A.ITEMCODE=B.ITEMCODE " + Constants.vbCrLf
          //  + " DELETE FROM TRITEM" + FrmCode + " WHERE [ORDERNO]=@orderno ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  #endregion

          //  #region Purchase

          //  //  -----  Procedure for Insert PURCHASE -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_PURCHASE" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_PURCHASE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_PURCHASE" + FrmCode + Constants.vbCrLf
          //  + "(@date1      [datetime], " + Constants.vbCrLf
          //  + "@PORDERNO    [bigint] = null, " + Constants.vbCrLf
          //  + "@suppCode    [varchar] (10) = '', " + Constants.vbCrLf
          //  + "@suppName    [varchar] (40) = '', " + Constants.vbCrLf
          //  + "@invNo       [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@invDate     [datetime], " + Constants.vbCrLf
          //  + "@invAmt      [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@type        [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@remarks     [varchar] (50) = '', " + Constants.vbCrLf
          //  + "@expense     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@roundOff    [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@disAmt      [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@cstBill     [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@taxAmt      [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@cessAmt     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@AdisAmt     [numeric] (17,2) = 0, " + Constants.vbCrLf         
          //  + "@userID      [varchar] (15) = '', " + Constants.vbCrLf
          //  + "@cForm       [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@cFromNo     [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@cFormDate   [datetime], " + Constants.vbCrLf
          //  + "@cFormBookNO [varchar] (20) = '') " + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + "BEGIN TRAN T1 " + Constants.vbCrLf
          //  + "declare @orderNo  [bigint] " + Constants.vbCrLf
          //  + "set @orderNo = (select isnull(max(orderno),0)+1 from purchase" + FrmCode + " with(xlock)) " + Constants.vbCrLf
          //  + "INSERT INTO PURCHASE" + FrmCode + "([date1], [ORDERNO],[PORDERNO], [SUPPCODE], [SUPPNAME], [INVNO], [INVDATE], " + Constants.vbCrLf
          //  + "[INVAMT], [type], [REMARKS], [ADISCAMT], [EXPENSE],[ROUNDOFF], [DISCAMT], " + Constants.vbCrLf
          //  + "[CSTBILL], [TAXAMT], [CESSAMT], [USERID], [CFORM], [CFORMNO], [CFORMDATE], [CFORMBOOKNO])  " + Constants.vbCrLf
          //  + " VALUES (@date1, @orderNo,@PORDERNO, @suppCode, @suppName, @invNo, @invDate, @invAmt, @type, " + Constants.vbCrLf
          //  + "@remarks, @AdisAmt, @expense,@roundOff, @disAmt, @cstBill, @taxAmt, @cessAmt, @userID, " + Constants.vbCrLf
          //  + "@cForm, @cFromNo, @cFormDate, @cFormBookNO ) " + Constants.vbCrLf
          //  + "select @orderNo " + Constants.vbCrLf
          //  + "COMMIT TRAN T1";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for Insert PITEM -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_PITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_PITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_PITEM" + FrmCode + Constants.vbCrLf
          //  + "(@orderNo    [bigint],  " + Constants.vbCrLf
          //  + " @date1       [datetime], " + Constants.vbCrLf
          //  + " @itemCode    [varchar] (20) = '', " + Constants.vbCrLf
          //  + " @ITEMNAME    [varchar] (40) = '', " + Constants.vbCrLf
          //  + " @qty         [numeric] (17,3) = 0, " + Constants.vbCrLf
          //  + " @foc         [numeric] (17,3) = 0, " + Constants.vbCrLf
          //  + " @UNIT	 	[varchar](20)=''," + Constants.vbCrLf
          //  + " @UNITVALUE	[numeric](18,3)=0," + Constants.vbCrLf
          //  + " @price       [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @netAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @discPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + " @discAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @taxPer      [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + " @taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @lc          [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @tdiscount   [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @accCode     [varchar] (10) = '', " + Constants.vbCrLf
          //  + " @postAmount  [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @vatAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @cessPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + " @cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @cessPostAmt [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @barCode     [varchar] (1) = '', " + Constants.vbCrLf
          //  + " @mrp         [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + " @priceupdation      bit = 0 " + Constants.vbCrLf
          //  + ")" + Constants.vbCrLf
          //  + "AS  " + Constants.vbCrLf
          //  + "   " + Constants.vbCrLf
          //  + " INSERT INTO PITEM" + FrmCode + "([ORDERNO], [DATE1], [ITEMCODE],[ITEMNAME], [QTY],[FOC],[UNIT],[UNITVALUE], [PRICE], [NETAMT], " + Constants.vbCrLf
          //  + "[DISCPER], [DISCAMT], [TAXPER], [TAXAMT], [LC], [TDISCOUNT], [ACCCODE],  [POSTAMOUNT], " + Constants.vbCrLf
          //  + "[VATAMT],[CESSPER],[CESSAMT],[CESSPOSTAMT],[BARCODE], [MRP]) " + Constants.vbCrLf
          //  + "values(@orderNo, @date1, @itemCode,@ITEMNAME, @qty,@foc,@unit,@unitvalue, @price, @netAmt, @discPer,@discAmt, @taxPer, @taxAmt, " + Constants.vbCrLf
          //  + "@lc, @tdiscount,  @accCode,@postAmount, @vatAmt,@cessPer,@cessAmt,@cessPostAmt,@barCode,  @mrp)  " + Constants.vbCrLf
          //  + " update a set QTY=QTY+((@QTY+@FOC)*@unitvalue),LC=@LC  " + Environment.NewLine 
          //  + " ,rprofit2= " +Environment.NewLine 
          //  + "  case when @priceupdation=0 then rprofit2 " +Environment.NewLine 
          //  + "  else  " +Environment.NewLine 
          //  + " ( " +Environment.NewLine 
          //  + " ( " +Environment.NewLine 
          //  + " @price +  " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 then @price*(taxper/100 ) " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " + " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " ) " +Environment.NewLine 
          //  + " + " +Environment.NewLine 
          //  + " ( " +Environment.NewLine 
          //  + " @price +  " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 then @price*(taxper/100 ) " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " + " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine
          //  + " ) * (select profit/100 from profit" + FrmCode + ") " + Environment.NewLine 
          //  + " ) end " +Environment.NewLine 
          //  + " , " +Environment.NewLine 
          //  + " rprofit1= " +Environment.NewLine 
          //  + "  case when @priceupdation=0 then rprofit1 " +Environment.NewLine 
          //  + "  else  " +Environment.NewLine 
          //  + " ( " +Environment.NewLine 
          //  + " ( " +Environment.NewLine 
          //  + " @price +  " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 then @price*(taxper/100 ) " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " + " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " ) " +Environment.NewLine 
          //  + " + " +Environment.NewLine 
          //  + " ( " +Environment.NewLine 
          //  + " @price +  " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 then @price*(taxper/100 ) " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " + " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine
          //  + " ) * (select profit/100 from profit" + FrmCode + ") " + Environment.NewLine 
          //  + " ) end " +Environment.NewLine 
          //  + " , " +Environment.NewLine 
          //  + " rprofit3= " +Environment.NewLine 
          //  + "  case when @priceupdation=0 then rprofit3 " +Environment.NewLine 
          //  + "  else  " +Environment.NewLine 
          //  + " ( " +Environment.NewLine 
          //  + " ( " +Environment.NewLine 
          //  + " @price +  " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 then @price*(taxper/100 ) " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " + " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " ) " +Environment.NewLine 
          //  + " + " +Environment.NewLine 
          //  + " ( " +Environment.NewLine 
          //  + " @price +  " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 then @price*(taxper/100 ) " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " + " +Environment.NewLine 
          //  + " (  " +Environment.NewLine 
          //  + " case when taxper>0 and cessper>0 then (@price*(taxper/100 ))*cessper/100 " +Environment.NewLine 
          //  + " else 0 end) " +Environment.NewLine 
          //  + " ) * (select profit/100 from profit" + FrmCode + ") " +Environment.NewLine 
          //  + " ) end  from stockmst" + FrmCode + " a where itemcode=@itemcode " + Constants.vbCrLf
          //  + "  " + Constants.vbCrLf
          //  + " ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();



          //  //  -----  Procedure for Update PURCHASE -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_UPDATE_PURCHASE" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_UPDATE_PURCHASE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_UPDATE_PURCHASE" + FrmCode + Constants.vbCrLf
          //  + "(@date1      [datetime], " + Constants.vbCrLf
          //  + "@orderNo     [bigint], " + Constants.vbCrLf
          //  + "@PORDERNO    [bigint]=null, " + Constants.vbCrLf
          //  + "@suppCode    [varchar] (10) = '', " + Constants.vbCrLf
          //  + "@suppName    [varchar] (40) = '', " + Constants.vbCrLf
          //  + "@invNo       [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@invDate     [datetime], " + Constants.vbCrLf
          //  + "@invAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@type        [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@remarks     [varchar] (50) = '', " + Constants.vbCrLf
          //  + "@aDisAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@expense     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@roundOff    [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@disAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cstBill     [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@userID      [varchar] (15) = '', " + Constants.vbCrLf
          //  + "@cForm       [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@cFromNo     [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@cFormDate   [datetime], " + Constants.vbCrLf
          //  + "@cFormBookNO [varchar] (20) = '') " + Constants.vbCrLf
          //  + "AS UPDATE PURCHASE" + FrmCode + " set [date1] = @date1,[PORDERNO]=@PORDERNO, [SUPPCODE] = @suppCode, [SUPPNAME] = @suppName, " + Constants.vbCrLf
          //  + "[INVNO] = @invNo, [INVDATE] = @invDate, [INVAMT] = @invAmt, [type] = @type, [REMARKS] = @remarks, " + Constants.vbCrLf
          //  + "[ADISCAMT] = @aDisAmt, [EXPENSE] = @expense, " + Constants.vbCrLf
          //  + "[ROUNDOFF] = @roundOff, [DISCAMT] = @disAmt, [CSTBILL] = @cstBill, [TAXAMT] = @taxAmt, [CESSAMT]=@cessAmt, " + Constants.vbCrLf
          //  + "[USERID] = @userID, [CFORM] = @cForm, [CFORMNO] = @cFromNo, [CFORMDATE] = @cFormDate, " + Constants.vbCrLf
          //  + "[CFORMBOOKNO] = @cFormBookNO  " + Constants.vbCrLf
          //  + " " + Constants.vbCrLf
          //  + "WHERE [ORDERNO] = @orderNo";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for Delete PURCHASE -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_PURCHASE" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_PURCHASE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_PURCHASE" + FrmCode
          // + "(@orderNo     [bigint])"
          // + "AS DELETE FROM PURCHASE" + FrmCode + " where [ORDERNO] = @orderNo";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for Delete PITEM -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_PITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_PITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_PITEM" + FrmCode
          //  + "(@orderno    [bigint],@porderno bigint=null)" + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + " UPDATE A SET A.QTY=A.QTY-B.QTY FROM STOCKMST" + FrmCode + " A JOIN  " + Constants.vbCrLf
          //  + " (SELECT ITEMCODE,SUM((QTY+FOC)*unitvalue) QTY FROM PITEM" + FrmCode + " WHERE [ORDERNO]=@orderno    " + Constants.vbCrLf
          //  + "  GROUP BY ITEMCODE) B ON A.ITEMCODE=B.ITEMCODE " + Constants.vbCrLf
          //  + " DELETE FROM PITEM" + FrmCode + " WHERE [ORDERNO]=@orderno ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  #endregion

          //  #region TransferIn

          //  //  -----  Procedure for Insert TransferIn -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_TRANSIN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_TRANSIN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_TRANSIN" + FrmCode + Constants.vbCrLf
          //  + "(@date1      [datetime], " + Constants.vbCrLf
          //  + "@PORDERNO    [bigint] = null, " + Constants.vbCrLf
          //  + "@suppCode    [varchar] (10) = '', " + Constants.vbCrLf
          //  + "@suppName    [varchar] (40) = '', " + Constants.vbCrLf
          //  + "@invNo       [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@invDate     [datetime], " + Constants.vbCrLf
          //  + "@invAmt      [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@type        [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@remarks     [varchar] (50) = '', " + Constants.vbCrLf
          //  + "@expense     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@roundOff    [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@disAmt      [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@cstBill     [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@taxAmt      [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@cessAmt     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@AdisAmt     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@userID      [varchar] (15) = '', " + Constants.vbCrLf
          //  + "@cForm       [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@cFromNo     [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@cFormDate   [datetime], " + Constants.vbCrLf
          //  + "@cFormBookNO [varchar] (20) = '') " + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + "BEGIN TRAN T1 " + Constants.vbCrLf
          //  + "declare @orderNo  [bigint] " + Constants.vbCrLf
          //  + "set @orderNo = (select isnull(max(orderno),0)+1 from TRANSIN" + FrmCode + " with(xlock)) " + Constants.vbCrLf
          //  + "INSERT INTO TRANSIN" + FrmCode + "([date1], [ORDERNO],[PORDERNO], [SUPPCODE], [SUPPNAME], [INVNO], [INVDATE], " + Constants.vbCrLf
          //  + "[INVAMT], [type], [REMARKS], [ADISCAMT], [EXPENSE],[ROUNDOFF], [DISCAMT], " + Constants.vbCrLf
          //  + "[CSTBILL], [TAXAMT], [CESSAMT], [USERID], [CFORM], [CFORMNO], [CFORMDATE], [CFORMBOOKNO])  " + Constants.vbCrLf
          //  + " VALUES (@date1, @orderNo,@PORDERNO, @suppCode, @suppName, @invNo, @invDate, @invAmt, @type, " + Constants.vbCrLf
          //  + "@remarks, @AdisAmt, @expense,@roundOff, @disAmt, @cstBill, @taxAmt, @cessAmt, @userID, " + Constants.vbCrLf
          //  + "@cForm, @cFromNo, @cFormDate, @cFormBookNO ) " + Constants.vbCrLf
          //  + "select @orderNo " + Constants.vbCrLf
          //  + "COMMIT TRAN T1";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for Insert TRITEMIN -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_TRITEMIN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_TRITEMIN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_TRITEMIN" + FrmCode + Constants.vbCrLf
          //  + "(@orderNo    [bigint],  " + Constants.vbCrLf
          //  + "@date1       [datetime], " + Constants.vbCrLf
          //  + "@itemCode    [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@ITEMNAME    [varchar] (40) = '', " + Constants.vbCrLf
          //  + "@qty         [numeric] (17,3) = 0, " + Constants.vbCrLf
          //  + "@foc         [numeric] (17,3) = 0, " + Constants.vbCrLf
          //  + "@UNIT	 	[varchar](20)=''," + Constants.vbCrLf
          //  + "@UNITVALUE	[numeric](18,3)=0," + Constants.vbCrLf
          //  + "@price       [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@netAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@discPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@discAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@taxPer      [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@lc          [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@tdiscount   [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@accCode     [varchar] (10) = '', " + Constants.vbCrLf
          //  + "@postAmount  [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@vatAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cessPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cessPostAmt [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@barCode     [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@mrp         [numeric] (17,4) = 0 " + Constants.vbCrLf
          //  + ")" + Constants.vbCrLf
          //  + "AS  " + Constants.vbCrLf
          //  + "   " + Constants.vbCrLf
          //  + " INSERT INTO TRITEMIN" + FrmCode + "([ORDERNO], [DATE1], [ITEMCODE],[ITEMNAME], [QTY],[FOC],[UNIT],[UNITVALUE], [PRICE], [NETAMT], " + Constants.vbCrLf
          //  + "[DISCPER], [DISCAMT], [TAXPER], [TAXAMT], [LC], [TDISCOUNT], [ACCCODE],  [POSTAMOUNT], " + Constants.vbCrLf
          //  + "[VATAMT],[CESSPER],[CESSAMT],[CESSPOSTAMT],[BARCODE], [MRP]) " + Constants.vbCrLf
          //  + "values(@orderNo, @date1, @itemCode,@ITEMNAME, @qty,@foc,@unit,@unitvalue, @price, @netAmt, @discPer,@discAmt, @taxPer, @taxAmt, " + Constants.vbCrLf
          //  + "@lc, @tdiscount,  @accCode,@postAmount, @vatAmt,@cessPer,@cessAmt,@cessPostAmt,@barCode,  @mrp)  " + Constants.vbCrLf
          //  + " update a set QTY=QTY+((@QTY+@FOC)*@unitvalue) ,LC=@LC from stockmst" + FrmCode + " a where itemcode=@itemcode " + Constants.vbCrLf
          //  + "  " + Constants.vbCrLf
          //  + " ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();



          //  //  -----  Procedure for Update TRANSIN -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_UPDATE_TRANSIN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_UPDATE_TRANSIN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_UPDATE_TRANSIN" + FrmCode + Constants.vbCrLf
          //  + "(@date1      [datetime], " + Constants.vbCrLf
          //  + "@orderNo     [bigint], " + Constants.vbCrLf
          //  + "@PORDERNO    [bigint]=null, " + Constants.vbCrLf
          //  + "@suppCode    [varchar] (10) = '', " + Constants.vbCrLf
          //  + "@suppName    [varchar] (40) = '', " + Constants.vbCrLf
          //  + "@invNo       [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@invDate     [datetime], " + Constants.vbCrLf
          //  + "@invAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@type        [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@remarks     [varchar] (50) = '', " + Constants.vbCrLf
          //  + "@aDisAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@expense     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@roundOff    [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@disAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cstBill     [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@userID      [varchar] (15) = '', " + Constants.vbCrLf
          //  + "@cForm       [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@cFromNo     [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@cFormDate   [datetime], " + Constants.vbCrLf
          //  + "@cFormBookNO [varchar] (20) = '') " + Constants.vbCrLf
          //  + "AS UPDATE TRANSIN" + FrmCode + " set [date1] = @date1,[PORDERNO]=@PORDERNO, [SUPPCODE] = @suppCode, [SUPPNAME] = @suppName, " + Constants.vbCrLf
          //  + "[INVNO] = @invNo, [INVDATE] = @invDate, [INVAMT] = @invAmt, [type] = @type, [REMARKS] = @remarks, " + Constants.vbCrLf
          //  + "[ADISCAMT] = @aDisAmt, [EXPENSE] = @expense, " + Constants.vbCrLf
          //  + "[ROUNDOFF] = @roundOff, [DISCAMT] = @disAmt, [CSTBILL] = @cstBill, [TAXAMT] = @taxAmt, [CESSAMT]=@cessAmt, " + Constants.vbCrLf
          //  + "[USERID] = @userID, [CFORM] = @cForm, [CFORMNO] = @cFromNo, [CFORMDATE] = @cFormDate, " + Constants.vbCrLf
          //  + "[CFORMBOOKNO] = @cFormBookNO  " + Constants.vbCrLf
          //  + " " + Constants.vbCrLf
          //  + "WHERE [ORDERNO] = @orderNo";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for Delete TRANSIN -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_TRANSIN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_TRANSIN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_TRANSIN" + FrmCode
          // + "(@orderNo     [bigint])"
          // + "AS DELETE FROM TRANSIN" + FrmCode + " where [ORDERNO] = @orderNo";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for Delete TRITEMIN -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_TRITEMIN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_TRITEMIN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_TRITEMIN" + FrmCode
          //  + "(@orderno    [bigint],@porderno bigint=null)" + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + " UPDATE A SET A.QTY=A.QTY-B.QTY FROM STOCKMST" + FrmCode + " A JOIN  " + Constants.vbCrLf
          //  + " (SELECT ITEMCODE,SUM((QTY+FOC)*unitvalue) QTY FROM TRITEMIN" + FrmCode + " WHERE [ORDERNO]=@orderno    " + Constants.vbCrLf
          //  + "  GROUP BY ITEMCODE) B ON A.ITEMCODE=B.ITEMCODE " + Constants.vbCrLf
          //  + " DELETE FROM TRITEMIN" + FrmCode + " WHERE [ORDERNO]=@orderno ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  #endregion

          //  #region Purchase Return

          //  //  -----  Procedure for Insert PURCHASE RETURN-------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_PRETURN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_PRETURN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_PRETURN" + FrmCode + Constants.vbCrLf
          //  + "(@date1      [datetime], " + Constants.vbCrLf
          //  + "@PORDERNO    [bigint] = null, " + Constants.vbCrLf
          //  + "@suppCode    [varchar] (10) = '', " + Constants.vbCrLf
          //  + "@suppName    [varchar] (40) = '', " + Constants.vbCrLf
          //  + "@invNo       [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@invDate     [datetime], " + Constants.vbCrLf
          //  + "@invAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@type        [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@remarks     [varchar] (50) = '', " + Constants.vbCrLf
          //  + "@expense     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@roundOff    [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@disAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cstBill     [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@AdisAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@userID      [varchar] (15) = '', " + Constants.vbCrLf
          //  + "@cForm       [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@cFromNo     [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@cFormDate   [datetime], " + Constants.vbCrLf
          //  + "@cFormBookNO [varchar] (20) = '') " + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + "BEGIN TRAN T1 " + Constants.vbCrLf
          //  + "declare @orderNo  [bigint] " + Constants.vbCrLf
          //  + "set @orderNo = (select isnull(max(orderno),0)+1 from PRETURN" + FrmCode + " with(xlock)) " + Constants.vbCrLf
          //  + "INSERT INTO PRETURN" + FrmCode + "([date1], [ORDERNO],[PORDERNO], [SUPPCODE], [SUPPNAME], [INVNO], [INVDATE], " + Constants.vbCrLf
          //  + "[INVAMT], [type], [REMARKS], [ADISCAMT], [EXPENSE],[ROUNDOFF], [DISCAMT], " + Constants.vbCrLf
          //  + "[CSTBILL], [TAXAMT], [CESSAMT], [USERID], [CFORM], [CFORMNO], [CFORMDATE], [CFORMBOOKNO])  " + Constants.vbCrLf
          //  + " VALUES (@date1, @orderNo,@PORDERNO, @suppCode, @suppName, @invNo, @invDate, @invAmt, @type, " + Constants.vbCrLf
          //  + "@remarks, @AdisAmt, @expense,@roundOff, @disAmt, @cstBill, @taxAmt, @cessAmt, @userID, " + Constants.vbCrLf
          //  + "@cForm, @cFromNo, @cFormDate, @cFormBookNO ) " + Constants.vbCrLf
          //  + "select @orderNo " + Constants.vbCrLf
          //  + "COMMIT TRAN T1";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for Insert PRITEM -------

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_INSERT_PRITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_INSERT_PRITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_PRITEM" + FrmCode + Constants.vbCrLf
          //  + "(@orderNo    [bigint],  " + Constants.vbCrLf
          //  + "@date1       [datetime], " + Constants.vbCrLf
          //  + "@itemCode    [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@ITEMNAME    [varchar] (40) = '', " + Constants.vbCrLf
          //  + "@qty         [numeric] (17,3) = 0, " + Constants.vbCrLf
          //  + "@foc         [numeric] (17,3) = 0, " + Constants.vbCrLf
          //  + "@UNIT	 	[varchar](20)=''," + Constants.vbCrLf
          //  + "@UNITVALUE	[numeric](18,3)=0," + Constants.vbCrLf
          //  + "@price       [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@netAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@discPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@discAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@taxPer      [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@lc          [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@tdiscount   [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@accCode     [varchar] (10) = '', " + Constants.vbCrLf
          //  + "@postAmount  [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@vatAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cessPer     [numeric] (17,2) = 0, " + Constants.vbCrLf
          //  + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cessPostAmt [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@barCode     [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@mrp         [numeric] (17,2) = 0 " + Constants.vbCrLf
          //  + ")" + Constants.vbCrLf
          //  + "AS  " + Constants.vbCrLf
          //  + "   " + Constants.vbCrLf
          //  + " INSERT INTO PRITEM" + FrmCode + "([ORDERNO], [DATE1], [ITEMCODE],[ITEMNAME], [QTY],[FOC],[UNIT],[UNITVALUE], [PRICE], [NETAMT], " + Constants.vbCrLf
          //  + "[DISCPER], [DISCAMT], [TAXPER], [TAXAMT], [LC], [TDISCOUNT], [ACCCODE],  [POSTAMOUNT], " + Constants.vbCrLf
          //  + "[VATAMT],[CESSPER],[CESSAMT],[CESSPOSTAMT],[BARCODE], [MRP]) " + Constants.vbCrLf
          //  + "values(@orderNo, @date1, @itemCode,@ITEMNAME, @qty,@foc,@unit,@unitvalue, @price, @netAmt, @discPer,@discAmt, @taxPer, @taxAmt, " + Constants.vbCrLf
          //  + "@lc, @tdiscount,  @accCode,@postAmount, @vatAmt,@cessPer,@cessAmt,@cessPostAmt,@barCode,  @mrp)  " + Constants.vbCrLf
          //  + " update a set QTY=QTY-((@QTY+@FOC)*@unitvalue)  from stockmst" + FrmCode + " a where itemcode=@itemcode " + Constants.vbCrLf
          //  + "  " + Constants.vbCrLf
          //  + " ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();



          //  //  -----  Procedure for Update PURCHASE RETURN-------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_UPDATE_PRETURN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_UPDATE_PRETURN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_UPDATE_PRETURN" + FrmCode + Constants.vbCrLf
          //  + "(@date1      [datetime], " + Constants.vbCrLf
          //  + "@orderNo     [bigint], " + Constants.vbCrLf
          //  + "@PORDERNO    [bigint]=null, " + Constants.vbCrLf
          //  + "@suppCode    [varchar] (10) = '', " + Constants.vbCrLf
          //  + "@suppName    [varchar] (40) = '', " + Constants.vbCrLf
          //  + "@invNo       [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@invDate     [datetime], " + Constants.vbCrLf
          //  + "@invAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@type        [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@remarks     [varchar] (50) = '', " + Constants.vbCrLf
          //  + "@aDisAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@expense     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@roundOff    [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@disAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cstBill     [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@taxAmt      [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@cessAmt     [numeric] (17,4) = 0, " + Constants.vbCrLf
          //  + "@userID      [varchar] (15) = '', " + Constants.vbCrLf
          //  + "@cForm       [varchar] (1) = '', " + Constants.vbCrLf
          //  + "@cFromNo     [varchar] (20) = '', " + Constants.vbCrLf
          //  + "@cFormDate   [datetime], " + Constants.vbCrLf
          //  + "@cFormBookNO [varchar] (20) = '') " + Constants.vbCrLf
          //  + "AS UPDATE PRETURN" + FrmCode + " set [date1] = @date1,[PORDERNO]=@PORDERNO, [SUPPCODE] = @suppCode, [SUPPNAME] = @suppName, " + Constants.vbCrLf
          //  + "[INVNO] = @invNo, [INVDATE] = @invDate, [INVAMT] = @invAmt, [type] = @type, [REMARKS] = @remarks, " + Constants.vbCrLf
          //  + "[ADISCAMT] = @aDisAmt, [EXPENSE] = @expense, " + Constants.vbCrLf
          //  + "[ROUNDOFF] = @roundOff, [DISCAMT] = @disAmt, [CSTBILL] = @cstBill, [TAXAMT] = @taxAmt, [CESSAMT]=@cessAmt, " + Constants.vbCrLf
          //  + "[USERID] = @userID, [CFORM] = @cForm, [CFORMNO] = @cFromNo, [CFORMDATE] = @cFormDate, " + Constants.vbCrLf
          //  + "[CFORMBOOKNO] = @cFormBookNO  " + Constants.vbCrLf
          //  + " " + Constants.vbCrLf
          //  + "WHERE [ORDERNO] = @orderNo";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for Delete PURCHASE -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_PRETURN" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_PRETURN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_PRETURN" + FrmCode
          // + "(@orderNo     [bigint])"
          // + "AS DELETE FROM PRETURN" + FrmCode + " where [ORDERNO] = @orderNo";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  //  -----  Procedure for Delete PITEM -------
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_DELETE_PRITEM" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_DELETE_PRITEM" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_PRITEM" + FrmCode
          //  + "(@orderno    [bigint],@porderno bigint=null)" + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + " UPDATE A SET A.QTY=A.QTY+B.QTY FROM STOCKMST" + FrmCode + " A JOIN  " + Constants.vbCrLf
          //  + " (SELECT ITEMCODE,SUM((QTY+FOC)*unitvalue) QTY FROM PRITEM" + FrmCode + " WHERE [ORDERNO]=@orderno    " + Constants.vbCrLf
          //  + "  GROUP BY ITEMCODE) B ON A.ITEMCODE=B.ITEMCODE " + Constants.vbCrLf
          //  + " DELETE FROM PRITEM" + FrmCode + " WHERE [ORDERNO]=@orderno ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  #endregion

          //  #region Repacking
            
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          // + " object_id(N'SP_INSERT_MANFACT" + FrmCode + "') ) "
          // + " drop PROCEDURE SP_INSERT_MANFACT" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_INSERT_MANFACT" + FrmCode + "(                                                           \n"
          //           + "   @date1 	                                                                                            \n"
          //           + "   [datetime],                                                                                          \n"
          //           + "   @orderno 	[bigint]=0,                                                                             \n"
          //           + "   @expense 	[numeric](17,2) = 0,                                                                    \n"
          //           + "   @remarks 	[varchar](40) = '',                                                                     \n"
          //           + "   @netamt 	[numeric](17,2) = 0,                                                                        \n"
          //           + "   @userid 	[varchar](50) = ''                                                                         \n"
          //           + "                                                                                                        \n"
          //           + "   ) AS BEGIN TRAN T1                                                                                   \n"
          //           + "   set @orderno = (select isnull(max(orderno),0)+1 from manfact" + FrmCode + ")                         \n"
          //           + "   INSERT INTO MANFACT" + FrmCode + " ( [DATE1],[ORDERNO],[EXPENSE],[REMARKS],[NETAMT],[USERID])         \n"
          //           + "   VALUES ( @date1,@orderno,@expense,@remarks,@netamt,@userid)    select @orderno COMMIT TRAN T1\n";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //+ " object_id(N'SP_INSERT_MITEMIN" + FrmCode + "') ) "
          //+ " drop PROCEDURE SP_INSERT_MITEMIN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "  CREATE PROCEDURE SP_INSERT_MITEMIN" + FrmCode + "(                                               \n"
          //       + "   @date1	    [datetime],                                                                        \n"
          //       + "   @orderno 	[bigint],                                                                          \n"
          //       + "   @itemcode 	[varchar](13)='',                                                                  \n"
          //       + "   @itemname 	[varchar](20)='',                                                                  \n"
          //       + "   @qty 		[numeric](17,2)=0,                                                                 \n"
          //       + "   @price    	[numeric](17,2)=0,                                                                 \n"
          //       + "   @amount 	[numeric](17,2)=0,                                                                     \n"
          //       + "   @mrp 	 	[numeric](17,2)=0,                                                                 \n"
          //       + "   @wastage 	[numeric](17,2)=0,                                                                 \n"
          //       + "   @ROLLQTY	  [numeric](17, 3) =0 ,                                                                  \n"
          //       + "   @UNIT           [varchar] (3)='',                                                               \n"
          //       + "   @UNITID         [varchar] (1)='',                                                               \n"
          //       + "   @UNITVALUE 	 	[numeric](17,3)=0                                                                \n"
          //       + "   ) AS INSERT INTO MITEMIN" + FrmCode + " ([DATE1],[ORDERNO],[ITEMCODE],[ITEMNAME],[QTY],          \n"
          //       + " [PRICE],[AMOUNT],[MRP],[WASTAGE],[ROLLQTY],[UNIT],[UNITID],[UNITVALUE])                                \n"
          //       + "  VALUES (@date1,@orderno,@itemcode,@itemname,@qty,@price,@amount,                                  \n"
          //       + " @mrp,@wastage,@ROLLQTY,@UNIT,@UNITID,@UNITVALUE)                                                      \n"
          //       + "  update stockmst" + FrmCode + " set  qty=qty-(@qty*@unitvalue) where itemcode=@itemcode \n"
          //       + "  ";

          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //    + " object_id(N'SP_INSERT_MITEMOUT" + FrmCode + "') ) "
          //    + " drop PROCEDURE SP_INSERT_MITEMOUT" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "   CREATE   PROCEDURE SP_INSERT_MITEMOUT" + FrmCode + "                                     \n"
          //       + "   (@date1   	[datetime],@orderno 	[bigint],                                          \n"
          //       + "   @itemcode	[varchar](13)='',@itemname	[varchar](20)='',                              \n"
          //       + "   @qty 	 	[numeric](17,2)=0,                                                         \n"
          //       + "   @price    	[numeric](17,2)=0,                                                         \n"
          //       + "   @amount 	    [numeric](17,2)=0,                                                         \n"
          //       + "   @packdate 	[datetime]=null,                                                           \n"
          //       + "   @expdate 	[datetime]=null,                                                           \n"
          //       + "   @mrp 	 	[numeric](17,2)=0,                                                         \n"
          //       + "   @ROLLQTY	[numeric](17, 3) =0 ,                                                          \n"
          //       + "   @UNIT           [varchar] (3)='',                                                       \n"
          //       + "   @UNITID         [varchar] (1)='',                                                       \n"
          //       + "   @UNITVALUE 	 	[numeric](17,3)=0                                                     \n"
          //       + "                                                           \n"
          //       + "   )                                                                                       \n"
          //       + "   AS INSERT INTO MITEMOUT" + FrmCode + " ([DATE1],[ORDERNO],[ITEMCODE],[ITEMNAME],         \n"
          //       + "    [QTY],[PRICE],[AMOUNT],[PACKDATE],[EXPDATE],[MRP],[ROLLQTY],[UNIT],[UNITID],[UNITVALUE])    \n"
          //       + "   VALUES (@date1,@orderno,@itemcode,@itemname,@qty,@price,@amount,                         \n"
          //       + "   @packdate,@expdate,@mrp,@ROLLQTY,@UNIT,@UNITID,@UNITVALUE)update                            \n"
          //       + "   stockmst" + FrmCode + " set qty=qty+(@qty*@unitvalue) where itemcode=@itemcode \n"
          //       + "  ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //    + " object_id(N'SP_UPDATE_MANFACT" + FrmCode + "') ) "
          //    + " drop PROCEDURE SP_UPDATE_MANFACT" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  sql = "   CREATE PROCEDURE SP_UPDATE_MANFACT" + FrmCode + "(                   \n"
          //  + "   @date1 	[datetime],                                           \n"
          //  + "   @orderno 	[bigint],                                                     \n"
          //  + "   @expense 	[numeric](17,2) = 0,                                          \n"
          //  + "   @remarks 	[varchar](40) = '',                                           \n"
          //  + "   @netamt 	[numeric](17,2) = 0,                                          \n"
          //  + "   @userid 	[varchar](50) = ''                                      \n"
          //  + "                                          \n"
          //  + "                                                                           \n"
          //  + "   ) AS UPDATE MANFACT" + FrmCode + " SET                                  \n"
          //  + "   [DATE1]=@date1,[EXPENSE]=@expense,[REMARKS]=@remarks,[NETAMT]=@netamt,  \n"
          //  + " [USERID]=@userid                                         \n"
          //  + "   where [ORDERNO]=@orderno                                                \n";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  sql = "if exists (select * from dbo.sysobjects where id ="
          // + " object_id(N'SP_DELETE_MANFACT" + FrmCode + "') ) "
          // + " drop PROCEDURE SP_DELETE_MANFACT" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_MANFACT" + FrmCode + "(                                   \n"
          //   + " @orderno    [bigint]) AS DELETE FROM MANFACT" + FrmCode + " where [ORDERNO]=@orderno   \n";

          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //    + " object_id(N'SP_DELETE_MITEMIN" + FrmCode + "') ) "
          //    + " drop PROCEDURE SP_DELETE_MITEMIN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_MITEMIN" + FrmCode
          //  + "(@orderno    [bigint]=0)" + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + " Declare @itemcode varchar(13),@Qty numeric(17,3) " + Constants.vbCrLf
          //  + "while(select count(*) from MITEMIN" + FrmCode + " where orderno=@orderno) <>0 " + Constants.vbCrLf
          //  + "   begin " + Constants.vbCrLf
          //  + " " + Constants.vbCrLf
          //  + " Select @itemcode=itemcode,@qty=qty*unitvalue from MITEMIN" + FrmCode + " where recno = (select isnull(max(recno),0) from MITEMIN" + FrmCode + " where orderno=@orderno) " + Constants.vbCrLf
          //  + " set @Qty=@qty "
          //  + " update a set qty=qty+@qty  from  stockmst" + FrmCode + " a where itemcode= @itemcode   " + Constants.vbCrLf
          //  + "       delete from MITEMIN" + FrmCode + " where recno = (select isnull(max(recno),0) from MITEMIN" + FrmCode + " where orderno=@orderno)" + Constants.vbCrLf
          //  + "   end";

          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();



          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //    + " object_id(N'SP_DELETE_MITEMOUT" + FrmCode + "') ) "
          //    + " drop PROCEDURE SP_DELETE_MITEMOUT" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_DELETE_MITEMOUT" + FrmCode
          //  + "(@orderno    [bigint]=0)" + Constants.vbCrLf
          //  + "AS " + Constants.vbCrLf
          //  + " Declare @itemcode varchar(13),@Qty numeric(17,3) " + Constants.vbCrLf
          //  + "while(select count(*) from MITEMOUT" + FrmCode + " where orderno=@orderno) <>0 " + Constants.vbCrLf
          //  + "   begin " + Constants.vbCrLf
          //  + " " + Constants.vbCrLf
          //  + " Select @itemcode=itemcode,@qty=qty*unitvalue from MITEMOUT" + FrmCode + " where recno = (select isnull(max(recno),0) from MITEMOUT" + FrmCode + " where orderno=@orderno) " + Constants.vbCrLf
          //  + " set @Qty=@qty*-1 "
          //  + " update a set Qty=Qty+@Qty from  stockmst" + FrmCode + " a where itemcode= @itemcode   " + Constants.vbCrLf
          //  + "       delete from MITEMOUT" + FrmCode + " where recno = (select isnull(max(recno),0) from MITEMOUT" + FrmCode + " where orderno=@orderno)" + Constants.vbCrLf
          //  + "   end";//--------------------------------------
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();
            
          //  #endregion

          //  # region balancesheet
          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //  + " object_id(N'SP_BALANCE_SHEET" + FrmCode + "') ) "
          //  + " drop PROCEDURE SP_BALANCE_SHEET" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_BALANCE_SHEET" + FrmCode + "  " + Environment.NewLine
          //  + "  (     " + Environment.NewLine
          //  + "      @tag  	[varchar](2),     " + Environment.NewLine
          //  + "      @group  	[varchar](1)='',    " + Environment.NewLine
          //  + "      @fromDate	[datetime]=null,     " + Environment.NewLine
          //  + "      @toDate	[datetime]=null,	    " + Environment.NewLine
          //  + "      @checkpass	[bit]=0  " + Environment.NewLine
          //  + "  ) as     " + Environment.NewLine
          //  + "  DECLARE @TEMP table     " + Environment.NewLine
          //  + "  (     " + Environment.NewLine
          //  + "   [ID] bigint IDENTITY(1,1),     " + Environment.NewLine
          //  + "  Code varchar(10),     " + Environment.NewLine
          //  + "  Head varchar(50),     " + Environment.NewLine
          //  + "  Balance numeric(17,2),     " + Environment.NewLine
          //  + "  groupCode varchar(10)    " + Environment.NewLine
          //  + " )     " + Environment.NewLine
          //  + " DECLARE @Assets table     " + Environment.NewLine
          //  + " (     " + Environment.NewLine
          //  + "  [ID] bigint IDENTITY(1,1),     " + Environment.NewLine
          //  + "  Code varchar(10),     " + Environment.NewLine
          //  + "  Head varchar(50),     " + Environment.NewLine
          //  + "  Balance numeric(17,2),     " + Environment.NewLine
          //  + "  groupCode varchar(10)     " + Environment.NewLine
          //  + " )     " + Environment.NewLine
          //  + "      " + Environment.NewLine
          //  + " DECLARE @Liability table     " + Environment.NewLine
          //  + " (     " + Environment.NewLine
          //  + "  [ID] bigint IDENTITY(1,1),     " + Environment.NewLine
          //  + "  Code varchar(10),     " + Environment.NewLine
          //  + "  Head varchar(50),     " + Environment.NewLine
          //  + "  Balance numeric(17,2),     " + Environment.NewLine
          //  + "  groupCode varchar(10)     " + Environment.NewLine
          //  + " )     " + Environment.NewLine
          //  + "     " + Environment.NewLine
          //  + " if upper(@tag)='OP'  -- For Select The Opening Balance       " + Environment.NewLine
          //  + "  begin        " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode)   SELECT Code,Head,abs(OPbalance) Balance,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE (groupcode like '2%' or groupcode like '1%') AND code not in ('1111') and OPbalance<0 ORDER BY HEAD       " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode)   SELECT Code,Head,abs(opbalance) Balance,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE ( code in ('1111')) and opbalance>0 ORDER BY HEAD         " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode)   SELECT Code,Head,abs(opbalance) Balance,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE (code in ('3901'))  ORDER BY HEAD         " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "          INSERT INTO @Assets(code,head,balance)    " + Environment.NewLine
          //  + "	         Select code,head,balance from (    " + Environment.NewLine
          //  + "	         SELECT code,'   '+head head,balance,groupcode,2 lev   FROM @TEMP A    " + Environment.NewLine
          //  + "	         UNION ALL    " + Environment.NewLine
          //  + "	         SELECT code,head,null balance,code groupcode,1 lev from groups" + FrmCode + " where code in (select groupcode from @temp)    " + Environment.NewLine
          //  + "          ) Sn order by groupcode,lev,head     " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "	         Delete from @TEMP    " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode)   SELECT Code,Head,abs(opbalance) Balance,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE (groupcode like '2%' or groupcode like '1%') AND code not in ('1111') and opbalance>0 ORDER BY HEAD         " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode)   SELECT Code,Head,abs(opbalance) Balance,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE (code in ('1111')) and opbalance<0 ORDER BY HEAD          " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "	         INSERT INTO @Liability(code,head,balance)    " + Environment.NewLine
          //  + "	         Select code,head,balance from (    " + Environment.NewLine
          //  + "	         SELECT code,'   '+head head,balance,groupcode,2 lev   FROM @TEMP A    " + Environment.NewLine
          //  + "	         UNION ALL    " + Environment.NewLine
          //  + "	         SELECT code,head,null balance,code groupcode,1 lev  from groups" + FrmCode + " where code in (select groupcode from @temp)    " + Environment.NewLine
          //  + "          ) Sn order by groupcode,lev,head     " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "     end         " + Environment.NewLine
          //  + "  else if upper(@tag)='CA'  -- For Select The Current Balance As of Now        " + Environment.NewLine
          //  + "      begin        " + Environment.NewLine
          //  + "	 if @checkpass=0  " + Environment.NewLine
          //  + "	  begin  " + Environment.NewLine
          //  + "	          INSERT INTO @TEMP(code,head,balance,groupcode)         " + Environment.NewLine
          //  + "	          SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,OPBALANCE+ ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE ),0)+        " + Environment.NewLine
          //  + "	          ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "	          FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE<0 ORDER BY HEAD    " + Environment.NewLine
          //  + "          end  " + Environment.NewLine
          //  + "	 else  " + Environment.NewLine
          //  + "	  begin  " + Environment.NewLine
          //  + "	          INSERT INTO @TEMP(code,head,balance,groupcode)         " + Environment.NewLine
          //  + "	          SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,OPBALANCE+ ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE   " + Environment.NewLine
          //  + "			and ((bookcode<'200003' and cheque=0 ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  ) )   " + Environment.NewLine
          //  + "			),0)+        " + Environment.NewLine
          //  + "	          ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE    " + Environment.NewLine
          //  + "			and ((bookcode<'200003' and cheque=0 ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  ) )    " + Environment.NewLine
          //  + "			),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "	          FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE<0 ORDER BY HEAD        " + Environment.NewLine
          //  + "          end  " + Environment.NewLine
          //  + "  " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode) SELECT Code,Head,0,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE ( code in ('1111'))  ORDER BY HEAD         " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode) SELECT Code,Head,abs(balance),groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE (code in ('4901'))  ORDER BY HEAD           " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "	         INSERT INTO @Assets(code,head,balance)    " + Environment.NewLine
          //  + "	         Select code,head,balance from (    " + Environment.NewLine
          //  + "	         SELECT code,'   '+head head,balance,groupcode,2 lev   FROM @TEMP A    " + Environment.NewLine
          //  + "	         UNION ALL    " + Environment.NewLine
          //  + "	         SELECT code,head,null balance,code groupcode,1 lev from groups" + FrmCode + " where code in (select groupcode from @temp)    " + Environment.NewLine
          //  + "          ) Sn order by groupcode,lev,head     " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "	         Delete from @TEMP    " + Environment.NewLine
          //  + "              " + Environment.NewLine
          //  + "              " + Environment.NewLine
          //  + "          if @checkpass=0  " + Environment.NewLine
          //  + "           begin  " + Environment.NewLine
          //  + "	          INSERT INTO @TEMP(code,head,balance,groupcode)      " + Environment.NewLine
          //  + "        	  SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,OPBALANCE+ ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE ),0)+        " + Environment.NewLine
          //  + "	          ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "        	  FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE>0 ORDER BY HEAD    " + Environment.NewLine
          //  + "	   end  " + Environment.NewLine
          //  + "	  else  " + Environment.NewLine
          //  + "           begin  " + Environment.NewLine
          //  + "	          INSERT INTO @TEMP(code,head,balance,groupcode)      " + Environment.NewLine
          //  + "        	  SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,OPBALANCE+ ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE   " + Environment.NewLine
          //  + "			AND ((bookcode<'200003' and cheque=0 ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  ) )    " + Environment.NewLine
          //  + "			),0)+        " + Environment.NewLine
          //  + "	          ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE   " + Environment.NewLine
          //  + "			and ((bookcode<'200003' and cheque=0 ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  ) )   " + Environment.NewLine
          //  + "			),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "        	  FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE>0 ORDER BY HEAD    " + Environment.NewLine
          //  + "	   end  " + Environment.NewLine
          //  + "  " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode)  SELECT Code,Head,0,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + " 	     WHERE (code in ('1111'))  ORDER BY HEAD        " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "	         INSERT INTO @Liability(code,head,balance)    " + Environment.NewLine
          //  + "	         Select code,head,balance from (    " + Environment.NewLine
          //  + "	         SELECT code,'   '+head head,balance,groupcode,2 lev   FROM @TEMP A    " + Environment.NewLine
          //  + "	         UNION ALL    " + Environment.NewLine
          //  + "	         SELECT code,head,null balance,code groupcode,1 lev  from groups" + FrmCode + " where code in (select groupcode from @temp)    " + Environment.NewLine
          //  + "          ) Sn order by groupcode,lev,head    " + Environment.NewLine
          //  + "      end        " + Environment.NewLine
          //  + "  else if upper(@tag)='CS' -- For Select The Total Balance To a Specified Date        " + Environment.NewLine
          //  + "      begin        " + Environment.NewLine
          //  + "	 if @checkpass=0  " + Environment.NewLine
          //  + "	  begin  " + Environment.NewLine
          //  + "            INSERT INTO @TEMP(code,head,balance,groupcode)         " + Environment.NewLine
          //  + "            SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,OPBALANCE+ ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE AND  date1<= @toDate ),0)+        " + Environment.NewLine
          //  + "            ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE AND  date1<= @toDate ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "            FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE<0 ORDER BY HEAD        " + Environment.NewLine
          //  + "          End    " + Environment.NewLine
          //  + "	 else  " + Environment.NewLine
          //  + "	  begin  " + Environment.NewLine
          //  + "            INSERT INTO @TEMP(code,head,balance,groupcode)         " + Environment.NewLine
          //  + "            SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,OPBALANCE+ ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE AND  date1<= @toDate   " + Environment.NewLine
          //  + "	    and ((bookcode<'200003' and cheque=0 AND  date1<= @toDate ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  AND  date1<= @toDate ) )   " + Environment.NewLine
          //  + "	    ),0)+        " + Environment.NewLine
          //  + "            ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE AND  date1<= @toDate   " + Environment.NewLine
          //  + " 	    and ((bookcode<'200003' and cheque=0 AND  date1<= @toDate ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  AND  date1<= @toDate ) )   " + Environment.NewLine
          //  + "	    ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "            FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE<0 ORDER BY HEAD        " + Environment.NewLine
          //  + "          End    " + Environment.NewLine
          //  + "              " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode) SELECT Code,Head,0,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE ( code in ('1111'))  ORDER BY HEAD         " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode) SELECT Code,Head,abs(balance),groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE (code in ('4901'))  ORDER BY HEAD           " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "	         INSERT INTO @Assets(code,head,balance)    " + Environment.NewLine
          //  + "	         Select code,head,balance from (    " + Environment.NewLine
          //  + "	         SELECT code,'   '+head head,balance,groupcode,2 lev   FROM @TEMP A    " + Environment.NewLine
          //  + "	         UNION ALL    " + Environment.NewLine
          //  + "	         SELECT code,head,null balance,code groupcode,1 lev from groups" + FrmCode + " where code in (select groupcode from @temp)    " + Environment.NewLine
          //  + "          ) Sn order by groupcode,lev,head     " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "	         Delete from @TEMP    " + Environment.NewLine
          //  + "          if @checkpass=0  " + Environment.NewLine
          //  + "	   begin  " + Environment.NewLine
          //  + "            INSERT INTO @TEMP(code,head,balance,groupcode)      " + Environment.NewLine
          //  + "	         SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,OPBALANCE+ ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE AND  date1<= @toDate ),0)+        " + Environment.NewLine
          //  + "            ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE AND  date1<= @toDate ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "            FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE>0 ORDER BY HEAD    " + Environment.NewLine
          //  + "	   end            " + Environment.NewLine
          //  + "          else  " + Environment.NewLine
          //  + "             begin  " + Environment.NewLine
          //  + "            INSERT INTO @TEMP(code,head,balance,groupcode)      " + Environment.NewLine
          //  + "	         SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,OPBALANCE+ ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE AND  date1<= @toDate   " + Environment.NewLine
          //  + "            and ((bookcode<'200003' and cheque=0 AND  date1<= @toDate ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  AND  date1<= @toDate ) ) 	      " + Environment.NewLine
          //  + "            ),0)+        " + Environment.NewLine
          //  + "            ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE AND  date1<= @toDate   " + Environment.NewLine
          //  + "	    and ((bookcode<'200003' and cheque=0 AND  date1<= @toDate ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  AND  date1<= @toDate ) )   " + Environment.NewLine
          //  + "	    ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "            FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE>0 ORDER BY HEAD    " + Environment.NewLine
          //  + "	   end   " + Environment.NewLine
          //  + "  " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode)  SELECT Code,Head,0,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + " 	     WHERE (code in ('1111'))  ORDER BY HEAD        " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "	         INSERT INTO @Liability(code,head,balance)    " + Environment.NewLine
          //  + "	         Select code,head,balance from (    " + Environment.NewLine
          //  + "	         SELECT code,'   '+head head,balance,groupcode,2 lev   FROM @TEMP A    " + Environment.NewLine
          //  + "	         UNION ALL    " + Environment.NewLine
          //  + "	         SELECT code,head,null balance,code groupcode,1 lev  from groups" + FrmCode + " where code in (select groupcode from @temp)    " + Environment.NewLine
          //  + "          ) Sn order by groupcode,lev,head    " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "      end        " + Environment.NewLine
          //  + "  else if upper(@tag)='T' -- For Select the Transaction to a Perticular Period        " + Environment.NewLine
          //  + "      begin        " + Environment.NewLine
          //  + "	 if @checkpass=0  " + Environment.NewLine
          //  + "	   begin  " + Environment.NewLine
          //  + "            INSERT INTO @TEMP(code,head,balance,groupcode)         " + Environment.NewLine
          //  + "            SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE AND  date1 >= @fromDate and date1<=@toDate ),0)+        " + Environment.NewLine
          //  + "            ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE AND  date1 >= @fromDate and date1<=@toDate ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "            FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE<0 ORDER BY HEAD        " + Environment.NewLine
          //  + "           End  " + Environment.NewLine
          //  + "   	else  " + Environment.NewLine
          //  + "	   begin  " + Environment.NewLine
          //  + "            INSERT INTO @TEMP(code,head,balance,groupcode)         " + Environment.NewLine
          //  + "            SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE AND  date1 >= @fromDate and date1<=@toDate   " + Environment.NewLine
          //  + "	    and ((bookcode<'200003' and cheque=0 AND   date1 >= @fromDate and date1<=@toDate  ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  AND   date1 >= @fromDate and date1<=@toDate  ) ) 	      " + Environment.NewLine
          //  + "	    ),0)+        " + Environment.NewLine
          //  + "            ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE AND  date1 >= @fromDate and date1<=@toDate   " + Environment.NewLine
          //  + "	    and ((bookcode<'200003' and cheque=0 AND   date1 >= @fromDate and date1<=@toDate  ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  AND   date1 >= @fromDate and date1<=@toDate  ) ) 	      " + Environment.NewLine
          //  + "	    ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "            FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE<0 ORDER BY HEAD        " + Environment.NewLine
          //  + "           End  " + Environment.NewLine
          //  + "              " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode) SELECT Code,Head,0,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "  	     WHERE ( code in ('1111'))  ORDER BY HEAD         " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode) SELECT Code,Head,abs(balance),groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE (code in ('4901'))  ORDER BY HEAD         " + Environment.NewLine
          //  + "            " + Environment.NewLine
          //  + "	         INSERT INTO @Assets(code,head,balance)    " + Environment.NewLine
          //  + "	         Select code,head,balance from (    " + Environment.NewLine
          //  + "	         SELECT code,'   '+head head,balance,groupcode,2 lev   FROM @TEMP A    " + Environment.NewLine
          //  + "	         UNION ALL    " + Environment.NewLine
          //  + "	         SELECT code,head,null balance,code groupcode,1 lev from groups" + FrmCode + " where code in (select groupcode from @temp)    " + Environment.NewLine
          //  + "          ) Sn order by groupcode,lev,head     " + Environment.NewLine
          //  + "	           " + Environment.NewLine
          //  + "	         Delete from @TEMP    " + Environment.NewLine
          //  + "	           " + Environment.NewLine
          //  + "	         " + Environment.NewLine
          //  + "	 if @checkpass=0  " + Environment.NewLine
          //  + "	   begin         " + Environment.NewLine
          //  + "             INSERT INTO @TEMP(code,head,balance,groupcode) SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE AND  date1 >= @fromDate and date1<=@toDate ),0)+        " + Environment.NewLine
          //  + "             ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE AND  date1 >= @fromDate and date1<=@toDate ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "             FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE>0 ORDER BY HEAD	        " + Environment.NewLine
          //  + "           End  " + Environment.NewLine
          //  + " 	 else  " + Environment.NewLine
          //  + "	   begin         " + Environment.NewLine
          //  + "             INSERT INTO @TEMP(code,head,balance,groupcode) SELECT CODE,HEAD,ABS(BALANCE) BALANCE,groupcode FROM (SELECT CODE,HEAD,ISNULL((SELECT SUM(AMOUNT)  FROM VOUCHER" + FrmCode + "     WHERE CODE=A.CODE AND  date1 >= @fromDate and date1<=@toDate   " + Environment.NewLine
          //  + "	     and ((bookcode<'200003' and cheque=0 AND   date1 >= @fromDate and date1<=@toDate  ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  AND   date1 >= @fromDate and date1<=@toDate  ) ) 	      " + Environment.NewLine
          //  + "	     ),0)+        " + Environment.NewLine
          //  + "             ISNULL((SELECT SUM(AMOUNT)*-1 FROM VOUCHER" + FrmCode + "     WHERE BOOKCODE=A.CODE AND  date1 >= @fromDate and date1<=@toDate   " + Environment.NewLine
          //  + "             and ((bookcode<'200003' and cheque=0 AND   date1 >= @fromDate and date1<=@toDate  ) or (bookcode>='200003' and  chqpassed =1 and cheque=1  AND   date1 >= @fromDate and date1<=@toDate  ) ) 	      " + Environment.NewLine
          //  + "	     ),0) BALANCE,groupcode        " + Environment.NewLine
          //  + "             FROM ACCOUNTS" + FrmCode + "     A WHERE (CODE LIKE '1%' OR CODE LIKE '2%')  AND CODE NOT IN ('1111') ) SN WHERE BALANCE>0 ORDER BY HEAD	        " + Environment.NewLine
          //  + "           End  " + Environment.NewLine
          //  + "	           " + Environment.NewLine
          //  + "          INSERT INTO @TEMP(code,head,balance,groupcode) SELECT Code,Head,0,groupcode FROM accounts" + FrmCode + "             " + Environment.NewLine
          //  + "          WHERE (code in ('1111'))  ORDER BY HEAD         " + Environment.NewLine
          //  + "	           " + Environment.NewLine
          //  + "	         INSERT INTO @Liability(code,head,balance)    " + Environment.NewLine
          //  + "	         Select code,head,balance from (    " + Environment.NewLine
          //  + "	         SELECT code,'   '+head head,balance,groupcode,2 lev   FROM @TEMP A    " + Environment.NewLine
          //  + "	         UNION ALL    " + Environment.NewLine
          //  + "	         SELECT code,head,null balance,code groupcode,1 lev  from groups" + FrmCode + " where code in (select groupcode from @temp)    " + Environment.NewLine
          //  + "          ) Sn order by groupcode,lev,head     " + Environment.NewLine
          //  + "	           " + Environment.NewLine
          //  + "	         end        " + Environment.NewLine
          //  + "	           " + Environment.NewLine
          //  + "	         if (@group='Y')      " + Environment.NewLine
          //  + "	             begin                   " + Environment.NewLine
          //  + "	                 update A set a.balance=(select isnull(sum(balance),0.00) from @assets where rtrim(substring(head,1,2))='' and code in (select code from accounts" + FrmCode + " where groupcode=a.code)) from @Assets a where rtrim(substring(head,1,2))<>''   " + Environment.NewLine
          //  + "	                 update @assets set balance=0 where rtrim(substring(head,1,2))=''   " + Environment.NewLine
          //  + "	                            " + Environment.NewLine
          //  + "	                 update A set a.balance=(select isnull(sum(balance),0.00) from @Liability where rtrim(substring(head,1,2))='' and code in (select code from accounts" + FrmCode + " where groupcode=a.code)) from @Liability a where rtrim(substring(head,1,2))<>''   " + Environment.NewLine
          //  + "	                 update @Liability set balance=0 where rtrim(substring(head,1,2))=''   " + Environment.NewLine
          //  + "	            end   " + Environment.NewLine
          //  + "	                  " + Environment.NewLine
          //  + "	                  " + Environment.NewLine
          //  + "	                  " + Environment.NewLine
          //  + "  if(select count(*) from @Assets)<(select count(*) from @Liability)        " + Environment.NewLine
          //  + "           SELECT L.code,L.head Liability,L.Balance,a.code,a.head Assets,a.Balance FROM @Liability L left outer JOIN @Assets A        " + Environment.NewLine
          //  + "           ON L.ID=A.ID        " + Environment.NewLine
          //  + "   else        " + Environment.NewLine
          //  + "           SELECT L.code,L.head Liability,L.Balance,a.code,a.head Assets,a.Balance FROM @Liability L right outer JOIN @Assets A        " + Environment.NewLine
          //  + "           ON L.ID=A.ID   ";
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  #endregion balancesheet            

          //  # region purchase posting

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //       + " object_id(N'SP_POSTING_PURCHASE" + FrmCode + "') ) "
          //       + " drop PROCEDURE SP_POSTING_PURCHASE" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = " CREATE PROCEDURE SP_POSTING_PURCHASE" + FrmCode + " " + Constants.vbCrLf
          //  + " ( " + Constants.vbCrLf
          //   + " @ORDERNO [BIGINT] " + Constants.vbCrLf
          //  + " ) AS " + Constants.vbCrLf
          //  + " DECLARE  " + Constants.vbCrLf
          //  + " @TYPE [VARCHAR](1) " + Constants.vbCrLf
          //  + " SET @TYPE='H' " + Constants.vbCrLf

          //  //DELETE 


          //  + " DECLARE @RECNO BIGINT,@CODE VARCHAR(10),@BOOKCODE VARCHAR(10),@AMOUNT NUMERIC(17,2) " + Constants.vbCrLf

          //  + " DECLARE SA CURSOR FOR  " + Constants.vbCrLf
          //      + " SELECT RECNO FROM VOUCHER" + FrmCode + "  WHERE TRANSTYPE=@TYPE AND TRANSNO=@ORDERNO " + Constants.vbCrLf
          //  + " OPEN SA " + Constants.vbCrLf
          //  + "   FETCH NEXT FROM SA INTO @RECNO " + Constants.vbCrLf
          //  + "     WHILE @@FETCH_STATUS=0 " + Constants.vbCrLf
          //  + "         BEGIN " + Constants.vbCrLf
          //  + "             SET @CODE='' " + Constants.vbCrLf
          //  + "             SET @BOOKCODE='' " + Constants.vbCrLf
          //  + "             SET @AMOUNT=0 " + Constants.vbCrLf

          //  + "             SELECT @CODE=CODE,@BOOKCODE=BOOKCODE,@AMOUNT=AMOUNT FROM VOUCHER" + FrmCode + " WHERE TRANSTYPE=@TYPE AND TRANSNO=@ORDERNO AND RECNO=@RECNO " + Constants.vbCrLf
          //  + "             SET @AMOUNT=@AMOUNT*-1 " + Constants.vbCrLf
          //  + "                 EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + "                 DELETE FROM VOUCHER" + FrmCode + " WHERE TRANSTYPE=@TYPE AND TRANSNO=@ORDERNO AND RECNO=@RECNO " + Constants.vbCrLf

          //  + "                 FETCH NEXT FROM SA INTO @RECNO " + Constants.vbCrLf
          //  + "         END  " + Constants.vbCrLf
          //  + " CLOSE SA " + Constants.vbCrLf
          //  + " DEALLOCATE SA " + Constants.vbCrLf

          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT],  " + Constants.vbCrLf
          //  + " [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT DATE1,'000000',SUPPCODE, @TYPE,ORDERNO,convert(varchar(200), 'Inv.No:'+ INVNO + ',Inv.dt:'+ convert(varchar,invdate,103) +  ' ' + REMARKS) , " + Constants.vbCrLf
          //  + " INVAMT ,ORDERNO,0,'',NULL, NULL,0,USERID FROM PURCHASE" + FrmCode + " WHERE TYPE='2' AND INVAMT>0 AND ORDERNO=@ORDERNO " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='000000',@CODE=SUPPCODE, @AMOUNT=INVAMT FROM PURCHASE" + FrmCode + " WHERE TYPE='2' AND INVAMT>0 AND ORDERNO=@ORDERNO " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  //EXPENSE

          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "] " + Constants.vbCrLf
          //  + " ([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT],[BILLNO],  " + Constants.vbCrLf
          //  + " [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT DATE1, " + Constants.vbCrLf
          //  + " CASE RTRIM(TYPE) " + Constants.vbCrLf
          //  + " WHEN '1' THEN '200001' " + Constants.vbCrLf
          //  + " ELSE '000000'END AS SUPPCODE " + Constants.vbCrLf
          //  + " ,'300002', @TYPE,ORDERNO,  " + Constants.vbCrLf
          //  + " convert(varchar(200), suppname + 'Inv.No:'+ INVNO + ',Inv.dt:'+ convert(varchar,invdate,103) +  ' ' + REMARKS)   " + Constants.vbCrLf
          //  + " AS NARRATION , " + Constants.vbCrLf
          //  + " EXPENSE * -1,ORDERNO,0,'',NULL, NULL,0,USERID FROM PURCHASE" + FrmCode + " WHERE  EXPENSE>0 AND ORDERNO=@ORDERNO  " + Constants.vbCrLf

          //  + " SELECT  @BOOKCODE=CASE RTRIM(TYPE) WHEN '1' THEN '200001' ELSE '000000' END , " + Constants.vbCrLf
          //  + " @CODE='300002', @AMOUNT=EXPENSE * -1 FROM PURCHASE" + FrmCode + " WHERE  EXPENSE>0 AND ORDERNO=@ORDERNO  " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  //// 

          //  ////ROUNDOFF


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "] " + Constants.vbCrLf
          //  + " ([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT],[BILLNO],  " + Constants.vbCrLf
          //  + " [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT DATE1, " + Constants.vbCrLf
          //  + " CASE RTRIM(TYPE) " + Constants.vbCrLf
          //  + " WHEN '1' THEN '200001' " + Constants.vbCrLf
          //  + " ELSE '000000' END AS SUPPCODE " + Constants.vbCrLf
          //  + " ,'502', @TYPE,ORDERNO,  " + Constants.vbCrLf
          //  + " convert(varchar(200), suppname + 'Inv.No:'+ INVNO + ',Inv.dt:'+ convert(varchar,invdate,103) +  ' ' + REMARKS)  " + Constants.vbCrLf
          //  + " AS NARRATION , " + Constants.vbCrLf
          //  + " ROUNDOFF * -1,ORDERNO,0,'',NULL, NULL,0,USERID FROM PURCHASE" + FrmCode + " WHERE  ROUNDOFF<>0 AND ORDERNO=@ORDERNO  " + Constants.vbCrLf

          //  + " SELECT  @BOOKCODE=CASE RTRIM(TYPE) WHEN '1' THEN '200001' ELSE '000000' END , " + Constants.vbCrLf
          //  + " @CODE='502', @AMOUNT=ROUNDOFF * -1 FROM PURCHASE" + FrmCode + " WHERE  ROUNDOFF<>0 AND ORDERNO=@ORDERNO  " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  /////



          //  + " INSERT INTO ACCOUNTS" + FrmCode + " (CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT, " + Constants.vbCrLf
          //  + " CRDAYS,INTRATE,LEGAL,USERID,BILL,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS) " + Constants.vbCrLf
          //  + " SELECT A.ACCCODE CODE,'Purchase ' + ISNULL((SELECT DETAILS FROM LOOKUP WHERE FIELD1='TAXPER' AND CODE=MAX(TAXPER) ),'')   " + Constants.vbCrLf
          //  + " + '-' + ISNULL((SELECT DETAILS FROM LOOKUP WHERE FIELD1='ACCCODE' AND CODE=SUBSTRING(ACCCODE,LEN(ACCCODE),1)),'')  HEAD  " + Constants.vbCrLf
          //  + " , 0 BALANCE,'P' TYPE,2 LEVEL1,'31' GROUPCODE,2 GROUPLEVEL,0 OPBALANCE,0 CRLIMIT, " + Constants.vbCrLf
          //  + " 0 CRDAYS,0 INTRATE,1 LEGAL,'SYSTEM' USERID,'N' BILL,'' ADDRESS,'' STREET,'' CITY ,'' STATE, " + Constants.vbCrLf
          //  + " ''PIN,'' TIN,'' CST,'' PHONE1,'' PHONE2,'' FAX_MAIL,'' REMARKS  FROM PITEM" + FrmCode + " A  WHERE A.POSTAMOUNT>0  " + Constants.vbCrLf
          //  + " AND A.ORDERNO=@ORDERNO AND ACCCODE NOT IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ")  GROUP BY A.ACCCODE " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf

          //  + " DECLARE SA CURSOR FOR   " + Constants.vbCrLf
          //      + " SELECT A.ACCCODE FROM PITEM" + FrmCode + " A JOIN  PURCHASE" + FrmCode + " B ON A.ORDERNO=B.ORDERNO   " + Constants.vbCrLf
          //      + " WHERE A.POSTAMOUNT>0 AND B.ORDERNO=@ORDERNO   GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf
          //  + " OPEN SA " + Constants.vbCrLf
          //  + " FETCH NEXT FROM SA INTO @CODE " + Constants.vbCrLf
          //  + " WHILE @@FETCH_STATUS=0 " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf

          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "] " + Constants.vbCrLf
          //  + " ([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO],  " + Constants.vbCrLf
          //  + " [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT MAX(A.DATE1), " + Constants.vbCrLf
          //  + " CASE RTRIM(MAX(B.TYPE)) " + Constants.vbCrLf
          //  + " WHEN '1' THEN '200001' " + Constants.vbCrLf
          //  + " ELSE '000000' END AS SUPPCODE, " + Constants.vbCrLf
          //  + " A.ACCCODE, @TYPE,A.ORDERNO, " + Constants.vbCrLf
          //  + " convert(varchar(200), MAX(B.suppname) + ',Inv.No:'+ MAX(B.INVNO) + ',Inv.dt:'+ convert(varchar,MAX(B.invdate),103) +  ' ' + max(B.REMARKS))   AS NARRATION, " + Constants.vbCrLf
          //  + " SUM(A.POSTAMOUNT) * -1  ,A.ORDERNO,0,'',NULL, NULL,0,max(b.userid) FROM PITEM" + FrmCode + " A JOIN  PURCHASE" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO  WHERE A.POSTAMOUNT>0 AND B.ORDERNO=@ORDERNO AND A.ACCCODE=@CODE   " + Constants.vbCrLf
          //  + " GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf


          //  + " SELECT @BOOKCODE=CASE RTRIM(MAX(B.TYPE)) WHEN '1' THEN '200001'	ELSE '000000' END , " + Constants.vbCrLf
          //  + " @AMOUNT=SUM(A.POSTAMOUNT) * -1  FROM PITEM" + FrmCode + " A JOIN  PURCHASE" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " 	A.ORDERNO=B.ORDERNO  WHERE A.POSTAMOUNT>0 AND B.ORDERNO=@ORDERNO AND A.ACCCODE=@CODE   " + Constants.vbCrLf
          //  + " 	GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf

          //  + " 	EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " 	FETCH NEXT FROM SA INTO @CODE " + Constants.vbCrLf
          //  + "     END  " + Constants.vbCrLf
          //  + " CLOSE SA " + Constants.vbCrLf
          //  + " DEALLOCATE SA " + Constants.vbCrLf

          //  + " INSERT INTO ACCOUNTS" + FrmCode + "" + Constants.vbCrLf
          //  + " (CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,LEGAL,USERID, " + Constants.vbCrLf
          //  + " BILL,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS) " + Constants.vbCrLf
          //  + " SELECT '1'+A.ACCCODE CODE,'Vat Purchase ' + ISNULL((SELECT DETAILS FROM LOOKUP WHERE FIELD1='TAXPER'  " + Constants.vbCrLf
          //  + " AND CODE=MAX(TAXPER) ),'') + '-' + ISNULL((SELECT DETAILS FROM LOOKUP WHERE FIELD1='ACCCODE' AND  " + Constants.vbCrLf
          //  + " CODE=SUBSTRING(ACCCODE,LEN(ACCCODE),1)),'') HEAD, 0 BALANCE,'L' TYPE,2 LEVEL1,'17' GROUPCODE,2 GROUPLEVEL, " + Constants.vbCrLf
          //  + " 0 OPBALANCE,0 CRLIMIT,0 CRDAYS,0 INTRATE,1 LEGAL,'SYSTEM' USERID,'N' BILL,'' ADDRESS,'' STREET,'' CITY ,'' STATE, " + Constants.vbCrLf
          //  + " '' PIN,'' TIN,'' CST,'' PHONE1,'' PHONE2,'' FAX_MAIL,'' REMARKS  FROM PITEM" + FrmCode + " A  WHERE A.TAXPER>0 AND A.VATAMT>0 " + Constants.vbCrLf
          //  + " AND A.ORDERNO=@ORDERNO AND '1'+ACCCODE NOT IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ")  GROUP BY A.ACCCODE " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf

          //  + " DECLARE SA CURSOR FOR  " + Constants.vbCrLf
          //  + " SELECT '1'+A.ACCCODE  FROM PITEM" + FrmCode + " A JOIN  PURCHASE" + FrmCode + " B  " + Constants.vbCrLf
          //  + " ON A.ORDERNO=B.ORDERNO WHERE A.TAXPER>0 AND A.VATAMT>0  " + Constants.vbCrLf
          //  + " 	AND B.ORDERNO=@ORDERNO   GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf

          //  + " OPEN SA " + Constants.vbCrLf
          //  + " FETCH NEXT FROM SA INTO @CODE " + Constants.vbCrLf
          //  + " WHILE @@FETCH_STATUS=0 " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf

          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " 	SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "] " + Constants.vbCrLf
          //  + " ([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO],  " + Constants.vbCrLf
          //  + " [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT MAX(A.DATE1), " + Constants.vbCrLf
          //  + " CASE RTRIM(MAX(B.TYPE)) " + Constants.vbCrLf
          //  + " WHEN '1' THEN '200001' " + Constants.vbCrLf
          //  + " ELSE '000000' END AS SUPPCODE, " + Constants.vbCrLf
          //  + " '1'+A.ACCCODE, @TYPE,A.ORDERNO, " + Constants.vbCrLf
          //  + " convert(varchar(200), MAX(B.suppname) + ',Inv.No:'+ MAX(B.INVNO) + ',Inv.dt:'+ convert(varchar,MAX(b.invdate),103) +  ' ' + max(b.REMARKS))   AS NARRATION, " + Constants.vbCrLf
          //  + " SUM(A.VATAMT) * -1  ,A.ORDERNO,0,'',NULL, NULL,0,max(b.userid) FROM PITEM" + FrmCode + " A JOIN  PURCHASE" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO WHERE A.TAXPER>0 AND A.VATAMT>0 AND B.ORDERNO=@ORDERNO AND '1'+A.ACCCODE=@CODE " + Constants.vbCrLf
          //  + " GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf

          //  + " SELECT 	@BOOKCODE=CASE RTRIM(MAX(B.TYPE))	WHEN '1' THEN '200001'	ELSE '000000' END , " + Constants.vbCrLf
          //  + " @CODE='1'+A.ACCCODE,@AMOUNT=SUM(A.VATAMT) * -1   FROM PITEM" + FrmCode + " A JOIN  PURCHASE" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO WHERE A.TAXPER>0 AND A.VATAMT>0 AND B.ORDERNO=@ORDERNO AND '1'+A.ACCCODE=@CODE    " + Constants.vbCrLf
          //  + " GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " 	FETCH NEXT FROM SA INTO @CODE " + Constants.vbCrLf
          //  + "     END  " + Constants.vbCrLf
          //  + " CLOSE SA " + Constants.vbCrLf
          //  + " DEALLOCATE SA " + Constants.vbCrLf


          //  + " INSERT INTO ACCOUNTS" + FrmCode + "" + Constants.vbCrLf
          //  + " (CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,LEGAL,USERID,BILL, " + Constants.vbCrLf
          //  + " ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS) " + Constants.vbCrLf
          //  + " SELECT '1310' CODE,'Cess on Vat Purchase' HEAD, 0 BALANCE,'L' TYPE,2 LEVEL1,'17' GROUPCODE,2 GROUPLEVEL, " + Constants.vbCrLf
          //  + " 0 OPBALANCE,0 CRLIMIT,0 CRDAYS,0 INTRATE,1 LEGAL,'SYSTEM' USERID,'N' BILL,'' ADDRESS,'' STREET,'' CITY,'' STATE , " + Constants.vbCrLf
          //  + " '' PIN,'' TIN,'' CST,'' PHONE1,'' PHONE2,'' FAX_MAIL,'' REMARKS  FROM PITEM" + FrmCode + " A  WHERE A.CESSPER>0 AND A.CESSPOSTAMT>0   " + Constants.vbCrLf
          //  + " AND A.ORDERNO=@ORDERNO AND '1310' NOT IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ")  GROUP BY A.ORDERNO " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT MAX(A.DATE1), " + Constants.vbCrLf
          //  + " CASE RTRIM(MAX(B.TYPE)) WHEN '1' THEN '200001' ELSE '000000' END AS SUPPCODE, " + Constants.vbCrLf
          //  + " '1310', @TYPE,A.ORDERNO, " + Constants.vbCrLf
          //  + " convert(varchar(200), MAX(B.suppname) + ',Inv.No:'+ MAX(B.INVNO) + ',Inv.dt:'+ convert(varchar,max(b.invdate),103)+  ' ' + max(B.REMARKS))   AS NARRATION, " + Constants.vbCrLf
          //  + " SUM(A.CESSPOSTAMT) * -1  ,A.ORDERNO,0,'',NULL, NULL,0,max(b.userid) FROM PITEM" + FrmCode + " A JOIN  PURCHASE" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO  WHERE  A.CESSPER>0 AND A.CESSPOSTAMT>0 AND B.ORDERNO=@ORDERNO   GROUP BY A.ORDERNO " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE=CASE RTRIM(MAX(B.TYPE)) WHEN '1' THEN '200001' ELSE '000000' END, " + Constants.vbCrLf
          //  + " @CODE='1310',@AMOUNT=SUM(A.CESSPOSTAMT) * -1   FROM PITEM" + FrmCode + " A JOIN  PURCHASE" + FrmCode + " B ON " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO  WHERE  A.CESSPER>0 AND A.CESSPOSTAMT>0 AND B.ORDERNO=@ORDERNO   GROUP BY A.ORDERNO " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT ";


          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  #endregion purchase posting

          //  # region purchasereturn posting

          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //       + " object_id(N'SP_POSTING_PRETURN" + FrmCode + "') ) "
          //       + " drop PROCEDURE SP_POSTING_PRETURN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = " CREATE PROCEDURE SP_POSTING_PRETURN" + FrmCode + " " + Constants.vbCrLf
          //  + " ( " + Constants.vbCrLf
          //   + " @ORDERNO [BIGINT] " + Constants.vbCrLf
          //  + " ) AS " + Constants.vbCrLf
          //  + " DECLARE  " + Constants.vbCrLf
          //  + " @TYPE [VARCHAR](1) " + Constants.vbCrLf
          //  + " SET @TYPE='U' " + Constants.vbCrLf

          //  //DELETE 


          //  + " DECLARE @RECNO BIGINT,@CODE VARCHAR(10),@BOOKCODE VARCHAR(10),@AMOUNT NUMERIC(17,2) " + Constants.vbCrLf

          //  + " DECLARE SA CURSOR FOR  " + Constants.vbCrLf
          //      + " SELECT RECNO FROM VOUCHER" + FrmCode + "  WHERE TRANSTYPE=@TYPE AND TRANSNO=@ORDERNO " + Constants.vbCrLf
          //  + " OPEN SA " + Constants.vbCrLf
          //  + "   FETCH NEXT FROM SA INTO @RECNO " + Constants.vbCrLf
          //  + "     WHILE @@FETCH_STATUS=0 " + Constants.vbCrLf
          //  + "         BEGIN " + Constants.vbCrLf
          //  + "             SET @CODE='' " + Constants.vbCrLf
          //  + "             SET @BOOKCODE='' " + Constants.vbCrLf
          //  + "             SET @AMOUNT=0 " + Constants.vbCrLf

          //  + "             SELECT @CODE=CODE,@BOOKCODE=BOOKCODE,@AMOUNT=AMOUNT FROM VOUCHER" + FrmCode + " WHERE TRANSTYPE=@TYPE AND TRANSNO=@ORDERNO AND RECNO=@RECNO " + Constants.vbCrLf
          //  + "             SET @AMOUNT=@AMOUNT " + Constants.vbCrLf
          //  + "                 EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + "                 DELETE FROM VOUCHER" + FrmCode + " WHERE TRANSTYPE=@TYPE AND TRANSNO=@ORDERNO AND RECNO=@RECNO " + Constants.vbCrLf

          //  + "                 FETCH NEXT FROM SA INTO @RECNO " + Constants.vbCrLf
          //  + "         END  " + Constants.vbCrLf
          //  + " CLOSE SA " + Constants.vbCrLf
          //  + " DEALLOCATE SA " + Constants.vbCrLf

          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT],  " + Constants.vbCrLf
          //  + " [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT DATE1,'000000',SUPPCODE, @TYPE,ORDERNO,convert(varchar(200), 'Inv.No:'+ INVNO + ',Inv.dt:'+ convert(varchar,invdate,103) +  ' ' + REMARKS) , " + Constants.vbCrLf
          //  + " INVAMT ,ORDERNO,0,'',NULL, NULL,0,USERID FROM PRETURN" + FrmCode + " WHERE TYPE='2' AND INVAMT>0 AND ORDERNO=@ORDERNO " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='000000',@CODE=SUPPCODE, @AMOUNT=INVAMT*-1 FROM PRETURN" + FrmCode + " WHERE TYPE='2' AND INVAMT>0 AND ORDERNO=@ORDERNO " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  //EXPENSE

          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "] " + Constants.vbCrLf
          //  + " ([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT],[BILLNO],  " + Constants.vbCrLf
          //  + " [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT DATE1, " + Constants.vbCrLf
          //  + " CASE RTRIM(TYPE) " + Constants.vbCrLf
          //  + " WHEN '1' THEN '200001' " + Constants.vbCrLf
          //  + " ELSE '000000'END AS SUPPCODE " + Constants.vbCrLf
          //  + " ,'300002', @TYPE,ORDERNO,  " + Constants.vbCrLf
          //  + " convert(varchar(200), suppname + 'Inv.No:'+ INVNO + ',Inv.dt:'+ convert(varchar,invdate,103) +  ' ' + REMARKS)   " + Constants.vbCrLf
          //  + " AS NARRATION , " + Constants.vbCrLf
          //  + " EXPENSE * -1,ORDERNO,0,'',NULL, NULL,0,USERID FROM PRETURN" + FrmCode + " WHERE  EXPENSE>0 AND ORDERNO=@ORDERNO  " + Constants.vbCrLf

          //  + " SELECT  @BOOKCODE=CASE RTRIM(TYPE) WHEN '1' THEN '200001' ELSE '000000' END , " + Constants.vbCrLf
          //  + " @CODE='300002', @AMOUNT=EXPENSE FROM PRETURN" + FrmCode + " WHERE  EXPENSE>0 AND ORDERNO=@ORDERNO  " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  //// 

          //  ////ROUNDOFF


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "] " + Constants.vbCrLf
          //  + " ([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT],[BILLNO],  " + Constants.vbCrLf
          //  + " [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT DATE1, " + Constants.vbCrLf
          //  + " CASE RTRIM(TYPE) " + Constants.vbCrLf
          //  + " WHEN '1' THEN '200001' " + Constants.vbCrLf
          //  + " ELSE '000000' END AS SUPPCODE " + Constants.vbCrLf
          //  + " ,'502', @TYPE,ORDERNO,  " + Constants.vbCrLf
          //  + " convert(varchar(200), suppname + 'Inv.No:'+ INVNO + ',Inv.dt:'+ convert(varchar,invdate,103) +  ' ' + REMARKS)  " + Constants.vbCrLf
          //  + " AS NARRATION , " + Constants.vbCrLf
          //  + " ROUNDOFF * -1,ORDERNO,0,'',NULL, NULL,0,USERID FROM PRETURN" + FrmCode + " WHERE  ROUNDOFF<>0 AND ORDERNO=@ORDERNO  " + Constants.vbCrLf

          //  + " SELECT  @BOOKCODE=CASE RTRIM(TYPE) WHEN '1' THEN '200001' ELSE '000000' END , " + Constants.vbCrLf
          //  + " @CODE='502', @AMOUNT=ROUNDOFF FROM PRETURN" + FrmCode + " WHERE  ROUNDOFF<>0 AND ORDERNO=@ORDERNO  " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  /////



          //  + " INSERT INTO ACCOUNTS" + FrmCode + " (CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT, " + Constants.vbCrLf
          //  + " CRDAYS,INTRATE,LEGAL,USERID,BILL,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS) " + Constants.vbCrLf
          //  + " SELECT A.ACCCODE CODE,'Purchase Return ' + ISNULL((SELECT DETAILS FROM LOOKUP WHERE FIELD1='TAXPER' AND CODE=MAX(TAXPER) ),'')   " + Constants.vbCrLf
          //  + " + '-' + ISNULL((SELECT DETAILS FROM LOOKUP WHERE FIELD1='ACCCODE' AND CODE=SUBSTRING(ACCCODE,LEN(ACCCODE),1)),'')  HEAD  " + Constants.vbCrLf
          //  + " , 0 BALANCE,'P' TYPE,2 LEVEL1,'32' GROUPCODE,2 GROUPLEVEL,0 OPBALANCE,0 CRLIMIT, " + Constants.vbCrLf
          //  + " 0 CRDAYS,0 INTRATE,1 LEGAL,'SYSTEM' USERID,'N' BILL,'' ADDRESS,'' STREET,'' CITY ,'' STATE, " + Constants.vbCrLf
          //  + " ''PIN,'' TIN,'' CST,'' PHONE1,'' PHONE2,'' FAX_MAIL,'' REMARKS  FROM PRITEM" + FrmCode + " A  WHERE A.POSTAMOUNT>0  " + Constants.vbCrLf
          //  + " AND A.ORDERNO=@ORDERNO AND ACCCODE NOT IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ")  GROUP BY A.ACCCODE " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf

          //  + " DECLARE SA CURSOR FOR   " + Constants.vbCrLf
          //      + " SELECT A.ACCCODE FROM PRITEM" + FrmCode + " A JOIN  PRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO   " + Constants.vbCrLf
          //      + " WHERE A.POSTAMOUNT>0 AND B.ORDERNO=@ORDERNO   GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf
          //  + " OPEN SA " + Constants.vbCrLf
          //  + " FETCH NEXT FROM SA INTO @CODE " + Constants.vbCrLf
          //  + " WHILE @@FETCH_STATUS=0 " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf

          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "] " + Constants.vbCrLf
          //  + " ([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO],  " + Constants.vbCrLf
          //  + " [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT MAX(A.DATE1), " + Constants.vbCrLf
          //  + " CASE RTRIM(MAX(B.TYPE)) " + Constants.vbCrLf
          //  + " WHEN '1' THEN '200001' " + Constants.vbCrLf
          //  + " ELSE '000000' END AS SUPPCODE, " + Constants.vbCrLf
          //  + " A.ACCCODE, @TYPE,A.ORDERNO, " + Constants.vbCrLf
          //  + " convert(varchar(200), MAX(B.suppname) + ',Inv.No:'+ MAX(B.INVNO) + ',Inv.dt:'+ convert(varchar,MAX(B.invdate),103) +  ' ' + max(B.REMARKS))   AS NARRATION, " + Constants.vbCrLf
          //  + " SUM(A.POSTAMOUNT)  ,A.ORDERNO,0,'',NULL, NULL,0,max(b.userid) FROM PRITEM" + FrmCode + " A JOIN  PRETURN" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO  WHERE A.POSTAMOUNT>0 AND B.ORDERNO=@ORDERNO AND A.ACCCODE=@CODE   " + Constants.vbCrLf
          //  + " GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf


          //  + " SELECT @BOOKCODE=CASE RTRIM(MAX(B.TYPE)) WHEN '1' THEN '200001'	ELSE '000000' END , " + Constants.vbCrLf
          //  + " @AMOUNT=SUM(A.POSTAMOUNT)  FROM PRITEM" + FrmCode + " A JOIN  PRETURN" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " 	A.ORDERNO=B.ORDERNO  WHERE A.POSTAMOUNT>0 AND B.ORDERNO=@ORDERNO AND A.ACCCODE=@CODE   " + Constants.vbCrLf
          //  + " 	GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf

          //  + " 	EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " 	FETCH NEXT FROM SA INTO @CODE " + Constants.vbCrLf
          //  + "     END  " + Constants.vbCrLf
          //  + " CLOSE SA " + Constants.vbCrLf
          //  + " DEALLOCATE SA " + Constants.vbCrLf

          //  + " INSERT INTO ACCOUNTS" + FrmCode + "" + Constants.vbCrLf
          //  + " (CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,LEGAL,USERID, " + Constants.vbCrLf
          //  + " BILL,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS) " + Constants.vbCrLf
          //  + " SELECT '1'+A.ACCCODE CODE,'Vat Purchase Return ' + ISNULL((SELECT DETAILS FROM LOOKUP WHERE FIELD1='TAXPER'  " + Constants.vbCrLf
          //  + " AND CODE=MAX(TAXPER) ),'') + '-' + ISNULL((SELECT DETAILS FROM LOOKUP WHERE FIELD1='ACCCODE' AND  " + Constants.vbCrLf
          //  + " CODE=SUBSTRING(ACCCODE,LEN(ACCCODE),1)),'') HEAD, 0 BALANCE,'L' TYPE,2 LEVEL1,'17' GROUPCODE,2 GROUPLEVEL, " + Constants.vbCrLf
          //  + " 0 OPBALANCE,0 CRLIMIT,0 CRDAYS,0 INTRATE,1 LEGAL,'SYSTEM' USERID,'N' BILL,'' ADDRESS,'' STREET,'' CITY ,'' STATE, " + Constants.vbCrLf
          //  + " '' PIN,'' TIN,'' CST,'' PHONE1,'' PHONE2,'' FAX_MAIL,'' REMARKS  FROM PRITEM" + FrmCode + " A  WHERE A.TAXPER>0 AND A.VATAMT>0 " + Constants.vbCrLf
          //  + " AND A.ORDERNO=@ORDERNO AND '1'+ACCCODE NOT IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ")  GROUP BY A.ACCCODE " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf

          //  + " DECLARE SA CURSOR FOR  " + Constants.vbCrLf
          //  + " SELECT '1'+A.ACCCODE  FROM PRITEM" + FrmCode + " A JOIN  PRETURN" + FrmCode + " B  " + Constants.vbCrLf
          //  + " ON A.ORDERNO=B.ORDERNO WHERE A.TAXPER>0 AND A.VATAMT>0  " + Constants.vbCrLf
          //  + " 	AND B.ORDERNO=@ORDERNO   GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf

          //  + " OPEN SA " + Constants.vbCrLf
          //  + " FETCH NEXT FROM SA INTO @CODE " + Constants.vbCrLf
          //  + " WHILE @@FETCH_STATUS=0 " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf

          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " 	SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "] " + Constants.vbCrLf
          //  + " ([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO],  " + Constants.vbCrLf
          //  + " [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT MAX(A.DATE1), " + Constants.vbCrLf
          //  + " CASE RTRIM(MAX(B.TYPE)) " + Constants.vbCrLf
          //  + " WHEN '1' THEN '200001' " + Constants.vbCrLf
          //  + " ELSE '000000' END AS SUPPCODE, " + Constants.vbCrLf
          //  + " '1'+A.ACCCODE, @TYPE,A.ORDERNO, " + Constants.vbCrLf
          //  + " convert(varchar(200), MAX(B.suppname) + ',Inv.No:'+ MAX(B.INVNO) + ',Inv.dt:'+ convert(varchar,MAX(b.invdate),103) +  ' ' + max(b.REMARKS))   AS NARRATION, " + Constants.vbCrLf
          //  + " SUM(A.VATAMT) * -1  ,A.ORDERNO,0,'',NULL, NULL,0,max(b.userid) FROM PRITEM" + FrmCode + " A JOIN  PRETURN" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO WHERE A.TAXPER>0 AND A.VATAMT>0 AND B.ORDERNO=@ORDERNO AND '1'+A.ACCCODE=@CODE " + Constants.vbCrLf
          //  + " GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf

          //  + " SELECT 	@BOOKCODE=CASE RTRIM(MAX(B.TYPE))	WHEN '1' THEN '200001'	ELSE '000000' END , " + Constants.vbCrLf
          //  + " @CODE='1'+A.ACCCODE,@AMOUNT=SUM(A.VATAMT)   FROM PRITEM" + FrmCode + " A JOIN  PRETURN" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO WHERE A.TAXPER>0 AND A.VATAMT>0 AND B.ORDERNO=@ORDERNO AND '1'+A.ACCCODE=@CODE    " + Constants.vbCrLf
          //  + " GROUP BY A.ACCCODE,A.ORDERNO " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " 	FETCH NEXT FROM SA INTO @CODE " + Constants.vbCrLf
          //  + "     END  " + Constants.vbCrLf
          //  + " CLOSE SA " + Constants.vbCrLf
          //  + " DEALLOCATE SA " + Constants.vbCrLf


          //  + " INSERT INTO ACCOUNTS" + FrmCode + "" + Constants.vbCrLf
          //  + " (CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,LEGAL,USERID,BILL, " + Constants.vbCrLf
          //  + " ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS) " + Constants.vbCrLf
          //  + " SELECT '1320' CODE,'Cess on Vat Purchase Return' HEAD, 0 BALANCE,'L' TYPE,2 LEVEL1,'17' GROUPCODE,2 GROUPLEVEL, " + Constants.vbCrLf
          //  + " 0 OPBALANCE,0 CRLIMIT,0 CRDAYS,0 INTRATE,1 LEGAL,'SYSTEM' USERID,'N' BILL,'' ADDRESS,'' STREET,'' CITY,'' STATE , " + Constants.vbCrLf
          //  + " '' PIN,'' TIN,'' CST,'' PHONE1,'' PHONE2,'' FAX_MAIL,'' REMARKS  FROM PRITEM" + FrmCode + " A  WHERE A.CESSPER>0 AND A.CESSPOSTAMT>0   " + Constants.vbCrLf
          //  + " AND A.ORDERNO=@ORDERNO AND '1320' NOT IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ")  GROUP BY A.ORDERNO " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID]) " + Constants.vbCrLf
          //  + " SELECT MAX(A.DATE1), " + Constants.vbCrLf
          //  + " CASE RTRIM(MAX(B.TYPE)) WHEN '1' THEN '200001' ELSE '000000' END AS SUPPCODE, " + Constants.vbCrLf
          //  + " '1320', @TYPE,A.ORDERNO, " + Constants.vbCrLf
          //  + " convert(varchar(200), MAX(B.suppname) + ',Inv.No:'+ MAX(B.INVNO) + ',Inv.dt:'+ convert(varchar,max(b.invdate),103)+  ' ' + max(B.REMARKS))   AS NARRATION, " + Constants.vbCrLf
          //  + " SUM(A.CESSPOSTAMT) * -1  ,A.ORDERNO,0,'',NULL, NULL,0,max(b.userid) FROM PRITEM" + FrmCode + " A JOIN  PRETURN" + FrmCode + " B ON  " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO  WHERE  A.CESSPER>0 AND A.CESSPOSTAMT>0 AND B.ORDERNO=@ORDERNO   GROUP BY A.ORDERNO " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE=CASE RTRIM(MAX(B.TYPE)) WHEN '1' THEN '200001' ELSE '000000' END, " + Constants.vbCrLf
          //  + " @CODE='1320',@AMOUNT=SUM(A.CESSPOSTAMT)  FROM PRITEM" + FrmCode + " A JOIN  PRETURN" + FrmCode + " B ON " + Constants.vbCrLf
          //  + " A.ORDERNO=B.ORDERNO  WHERE  A.CESSPER>0 AND A.CESSPOSTAMT>0 AND B.ORDERNO=@ORDERNO   GROUP BY A.ORDERNO " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT ";


          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();


          //  #endregion purchase return posting
                        
          //  # region sales posting


          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //          + " object_id(N'SP_POSTING_SALES" + FrmCode + "') ) "
          //          + " drop PROCEDURE SP_POSTING_SALES" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_POSTING_SALES" + FrmCode + "" + Constants.vbCrLf
          //  + " @DATE DATETIME=NULL,@ORDERNO BIGINT=0,@TYPE VARCHAR(1) " + Constants.vbCrLf
          //  + " AS " + Constants.vbCrLf
          //  + " DECLARE @CODE VARCHAR(10),@VALUE DECIMAL(15,2),@BOOKCODE VARCHAR(10),@AMOUNT NUMERIC(17,2)  " + Constants.vbCrLf
          //  + " IF (@TYPE='+') SET @VALUE=1 " + Constants.vbCrLf
          //  + " ELSE IF (@TYPE='-') SET @VALUE=-1 " + Constants.vbCrLf

          //  //CREDIT

          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='000000',@CODE=CUSTCODE,@AMOUNT=BILLAMT*-1 * @VALUE FROM SALES" + FrmCode + " WHERE BILLTYPE='2'  " + Constants.vbCrLf
          //  + " AND CUSTCODE<>'CANCEL' AND BILLAMT>0 AND ORDERNO=@ORDERNO " + Constants.vbCrLf

          //  + " IF (@TYPE='+') " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'000000',CUSTCODE,'S',ORDERNO,'Sales Bill No.'+ convert(varchar,orderno)+ ' - ' + convert(varchar(150),Remarks1)  , " + Constants.vbCrLf
          //  + " BILLAMT*-1 * @VALUE,ORDERNO,0,'',NULL, NULL,0,USERID FROM SALES" + FrmCode + " WHERE BILLTYPE='2'  " + Constants.vbCrLf
          //  + " AND CUSTCODE<>'CANCEL' AND BILLAMT>0 AND ORDERNO=@ORDERNO " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT  " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE IF (@TYPE='-')  " + Constants.vbCrLf
          //  + " BEGIN  " + Constants.vbCrLf

          //  + " DECLARE @RECNO BIGINT  " + Constants.vbCrLf
          //  + " DECLARE SA CURSOR FOR   " + Constants.vbCrLf
          //  + " SELECT RECNO FROM VOUCHER" + FrmCode + "  WHERE [DATE1]=@DATE AND [BOOKCODE]='000000' AND [TRANSTYPE]='S'  " + Constants.vbCrLf
          //  + " AND BILLNO=@ORDERNO   " + Constants.vbCrLf
          //  + " OPEN SA  " + Constants.vbCrLf
          //  + " FETCH NEXT FROM SA INTO @RECNO  " + Constants.vbCrLf
          //  + " WHILE @@FETCH_STATUS=0  " + Constants.vbCrLf
          //  + " BEGIN  " + Constants.vbCrLf
          //  + " SET @CODE=''  " + Constants.vbCrLf
          //  + " SET @BOOKCODE=''  " + Constants.vbCrLf
          //  + " SET @AMOUNT=0  " + Constants.vbCrLf
          //  + " SELECT @CODE=CODE,@BOOKCODE=BOOKCODE,@AMOUNT=AMOUNT FROM VOUCHER" + FrmCode + " WHERE [DATE1]=@DATE AND [BOOKCODE]='000000' AND [TRANSTYPE]='S'  " + Constants.vbCrLf
          //  + " AND BILLNO=@ORDERNO  AND RECNO=@RECNO  " + Constants.vbCrLf
          //  + " SET @AMOUNT=@AMOUNT*-1  " + Constants.vbCrLf
          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT  " + Constants.vbCrLf
          //  + " DELETE FROM VOUCHER" + FrmCode + " WHERE [DATE1]=@DATE AND [BOOKCODE]='000000' AND [TRANSTYPE]='S'  " + Constants.vbCrLf
          //  + " AND BILLNO=@ORDERNO AND RECNO=@RECNO  " + Constants.vbCrLf
          //  + " FETCH NEXT FROM SA INTO @RECNO  " + Constants.vbCrLf
          //  + " END   " + Constants.vbCrLf
          //  + " CLOSE SA  " + Constants.vbCrLf
          //  + " DEALLOCATE SA  " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //      //ROUNDOFF


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='000000' ,@CODE='600001',@AMOUNT=SUM(ROUNDOFF) * @VALUE  " + Constants.vbCrLf
          //  + " FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND ROUNDOFF<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE='600001' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'000000' ,'600001','Z',0,'Credit',SUM(ROUNDOFF) * @VALUE ,0,0,'',NULL, NULL,0,MAX(USERID)  " + Constants.vbCrLf
          //  + " FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND ROUNDOFF<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " UPDATE [VOUCHER" + FrmCode + "] SET AMOUNT=AMOUNT+ " + Constants.vbCrLf
          //  + " ISNULL((SELECT SUM(ROUNDOFF) * @VALUE FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND ROUNDOFF<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 ),0) WHERE BOOKCODE='000000' AND CODE='600001' AND TRANSTYPE='Z' AND DATE1=@DATE  " + Constants.vbCrLf
          //  + " END " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='200001' ,@CODE='600001',@AMOUNT=SUM(ROUNDOFF) * @VALUE  " + Constants.vbCrLf
          //  + " FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND ROUNDOFF<>0   " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE='600001' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'200001' ,'600001','Z',0,'Cash',SUM(ROUNDOFF) * @VALUE ,0,0,'',NULL, NULL,0,MAX(USERID)  " + Constants.vbCrLf
          //  + " FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND ROUNDOFF<>0   " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " UPDATE [VOUCHER" + FrmCode + "] SET AMOUNT=AMOUNT+ " + Constants.vbCrLf
          //  + " ISNULL((SELECT SUM(ROUNDOFF) * @VALUE FROM SALES" + FrmCode + "  WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND ROUNDOFF<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 ),0) WHERE BOOKCODE='200001' AND CODE='600001' AND TRANSTYPE='Z' AND DATE1=@DATE  " + Constants.vbCrLf
          //  + " END " + Constants.vbCrLf

          //   //EXPENSE


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='000000' ,@CODE='603',@AMOUNT=SUM(EXPENSE) * @VALUE  " + Constants.vbCrLf
          //  + " FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND EXPENSE<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE='603' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'000000' ,'603','Z',0,'Credit',SUM(EXPENSE) * @VALUE ,0,0,'',NULL, NULL,0,MAX(USERID)  " + Constants.vbCrLf
          //  + " FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND EXPENSE<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " UPDATE [VOUCHER" + FrmCode + "] SET AMOUNT=AMOUNT+ " + Constants.vbCrLf
          //  + " ISNULL((SELECT SUM(EXPENSE) * @VALUE FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND EXPENSE<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 ),0) WHERE BOOKCODE='000000' AND CODE='603' AND TRANSTYPE='Z' AND DATE1=@DATE  " + Constants.vbCrLf
          //  + " END " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='200001' ,@CODE='603',@AMOUNT=SUM(EXPENSE) * @VALUE  " + Constants.vbCrLf
          //  + " FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND EXPENSE<>0   " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE='603' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'200001' ,'603','Z',0,'Cash',SUM(EXPENSE) * @VALUE ,0,0,'',NULL, NULL,0,MAX(USERID)  " + Constants.vbCrLf
          //  + " FROM SALES" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND EXPENSE<>0   " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " UPDATE [VOUCHER" + FrmCode + "] SET AMOUNT=AMOUNT+ " + Constants.vbCrLf
          //  + " ISNULL((SELECT SUM(EXPENSE) * @VALUE FROM SALES" + FrmCode + "  WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND EXPENSE<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 ),0) WHERE BOOKCODE='200001' AND CODE='603' AND TRANSTYPE='Z' AND DATE1=@DATE  " + Constants.vbCrLf
          //  + " END " + Constants.vbCrLf

          
          //  //AMOUNT


          //  + " INSERT INTO ACCOUNTS" + FrmCode + "(CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS,LEGAL,USERID,BILL) " + Constants.vbCrLf
          //  + " SELECT '41' + convert(varchar, CONVERT(int,A.TAXPER)) CODE ,'Sales '+ " + Constants.vbCrLf
          //  + " + (SELECT DETAILS FROM LOOKUP WHERE FIELD1='TAXPER' AND CODE=MAX(A.TAXPER)) HEAD " + Constants.vbCrLf
          //  + " ,0.0,'S','2','41',2,0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N' " + Constants.vbCrLf
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO " + Constants.vbCrLf
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.ORDERNO=@ORDERNO  "
          //  + " AND ('41' + convert(varchar, CONVERT(int,A.TAXPER))) NOT  IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ") "
          //  + " GROUP BY A.DATE1,A.TAXPER "


          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DECLARE SA CURSOR FOR  "
          //  + " SELECT '41' + convert(varchar, CONVERT(int,A.TAXPER)) CODE  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND B.ORDERNO=@ORDERNO  "
          //  + " GROUP BY A.DATE1,A.TAXPER "
          //  + " OPEN SA "
          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " WHILE @@FETCH_STATUS=0 "
          //  + " BEGIN "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'000000','41' + convert(varchar, CONVERT(int,A.TAXPER)),'Z',0,'Credit', "
          //  + " (SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE ,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '41' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER "
          //  + " END  "
          //  + " ELSE "
          //  + " BEGIN "

          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT (SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '41' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER),0) "
          //  + " WHERE BOOKCODE='000000' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE "

          //  + " END "

          //  + " SELECT @BOOKCODE='000000',@CODE='41' + convert(varchar, CONVERT(int,A.TAXPER)), "
          //  + " @AMOUNT=(SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '41' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " END  "
          //  + " CLOSE SA "
          //  + " DEALLOCATE SA "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DECLARE SA CURSOR FOR  "
          //  + " SELECT '41' + convert(varchar, CONVERT(int,A.TAXPER)) CODE  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND B.ORDERNO=@ORDERNO  "
          //  + " GROUP BY A.DATE1,A.TAXPER "
          //  + " OPEN SA "
          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " WHILE @@FETCH_STATUS=0 "
          //  + " BEGIN "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'200001','41' + convert(varchar, CONVERT(int,A.TAXPER)),'Z',0,'Cash', "
          //  + " (SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE ,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '41' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER "
          //  + " END  "
          //  + " ELSE "
          //  + " BEGIN "

          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT (SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1'  AND B.ORDERNO=@ORDERNO  "
          //  + " AND '41' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER),0) "
          //  + " WHERE BOOKCODE='200001' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE "

          //  + " END "

          //  + " SELECT @BOOKCODE='200001',@CODE='41' + convert(varchar, CONVERT(int,A.TAXPER)), "
          //  + " @AMOUNT=(SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '41' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "


          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " END  "
          //  + " CLOSE SA "
          //  + " DEALLOCATE SA "


          //  //TAX

          //  + " INSERT INTO ACCOUNTS" + FrmCode + "(CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS,LEGAL,USERID,BILL) "
          //  + " SELECT '141' + convert(varchar, CONVERT(int,A.TAXPER)) CODE ,'Vat Sales '+ "
          //  + " + (SELECT DETAILS FROM LOOKUP WHERE FIELD1='TAXPER' AND CODE=MAX(A.TAXPER)) "
          //  + " ,0.0,'L','2','17',2,0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N' "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + "  B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND  B.ORDERNO=@ORDERNO  "
          //  + " AND ('141' + convert(varchar, CONVERT(int,A.TAXPER))) NOT  IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ") "
          //  + " AND A.TAXPER<>0 GROUP BY A.DATE1,A.TAXPER "


          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DECLARE SA CURSOR FOR  "
          //  + " SELECT '141' + convert(varchar, CONVERT(int,A.TAXPER)) CODE "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1,A.TAXPER "
          //  + " OPEN SA "
          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " WHILE @@FETCH_STATUS=0 "
          //  + " BEGIN "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'000000','141' + convert(varchar, CONVERT(int,A.TAXPER)),'Z',0,'Credit', "
          //  + " SUM(A.TAXAMT)  * @VALUE,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '141' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER "
          //  + " END  "
          //  + " ELSE "
          //  + " BEGIN "

          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT SUM(A.TAXAMT)  * @VALUE "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '141' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER),0) "
          //  + " WHERE BOOKCODE='000000' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE "

          //  + " END "

          //  + " SELECT @BOOKCODE='000000',@CODE='141' + convert(varchar, CONVERT(int,A.TAXPER)), "
          //  + " @AMOUNT=SUM(A.TAXAMT)  * @VALUE FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '141' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " END  "
          //  + " CLOSE SA "
          //  + " DEALLOCATE SA "



          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DECLARE SA CURSOR FOR  "
          //  + " SELECT '141' + convert(varchar, CONVERT(int,A.TAXPER)) CODE "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1,A.TAXPER "
          //  + " OPEN SA "
          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " WHILE @@FETCH_STATUS=0 "
          //  + " BEGIN "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'200001','141' + convert(varchar, CONVERT(int,A.TAXPER)),'Z',0,'Cash', "
          //  + " SUM(A.TAXAMT)  * @VALUE,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '141' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER "
          //  + " END  "
          //  + " ELSE "
          //  + " BEGIN "

          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT SUM(A.TAXAMT)  * @VALUE "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '141' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER),0) "
          //  + " WHERE BOOKCODE='200001' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE "

          //  + " END "

          //  + " SELECT @BOOKCODE='200001',@CODE='141' + convert(varchar, CONVERT(int,A.TAXPER)), "
          //  + " @AMOUNT=SUM(A.TAXAMT)  * @VALUE "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '141' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " END  "
          //  + " CLOSE SA "
          //  + " DEALLOCATE SA "

          //  //CESS

          //  + " INSERT INTO ACCOUNTS" + FrmCode + "(CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS,LEGAL,USERID,BILL) "
          //  + " SELECT  TOP 1 '1410' CODE ,'Cess on Vat - Sales' HEAD "
          //  + " ,0.0,'L','2','17',2,0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N' "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + "  B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.ORDERNO=@ORDERNO  "
          //  + " AND ('1410') NOT  IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ") "
          //  + " GROUP BY A.DATE1 "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE='1410' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'000000','1410' ,'Z',0,'Credit',SUM(A.CESSAMT)  * @VALUE,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SITEM" + FrmCode + "  A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 "
          //  + " END "
          //  + " ELSE "
          //  + " BEGIN "
          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT SUM(A.CESSAMT)  * @VALUE  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO   "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 ),0) "
          //  + " WHERE BOOKCODE='000000' AND CODE='1410' AND TRANSTYPE='Z' AND DATE1=@DATE "
          //  + " END "

          //  + " SELECT @BOOKCODE='000000',@CODE='1410' ,@AMOUNT=SUM(A.CESSAMT)  * @VALUE "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE='1410' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'200001','1410' ,'Z',0,'Cash',SUM(A.CESSAMT)  * @VALUE,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 "
          //  + " END "
          //  + " ELSE "
          //  + " BEGIN "
          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT SUM(A.CESSAMT)  * @VALUE  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 ),0) "
          //  + " WHERE BOOKCODE='200001' AND CODE='1410' AND TRANSTYPE='Z' AND DATE1=@DATE "
          //  + " END "

          //  + " SELECT @BOOKCODE='200001',@CODE='1410' ,@AMOUNT=SUM(A.CESSAMT)  * @VALUE  "
          //  + " FROM SITEM" + FrmCode + " A JOIN  SALES" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DELETE FROM VOUCHER" + FrmCode + " WHERE AMOUNT=0 ";

          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();



          //  # endregion sales posting

          //  # region salesreturn posting


          //  sql = "if exists (select * from dbo.sysobjects where id ="
          //          + " object_id(N'SP_POSTING_SRETURN" + FrmCode + "') ) "
          //          + " drop PROCEDURE SP_POSTING_SRETURN" + FrmCode;
          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();

          //  sql = "CREATE PROCEDURE SP_POSTING_SRETURN" + FrmCode + "" + Constants.vbCrLf
          //  + " @DATE DATETIME=NULL,@ORDERNO BIGINT=0,@TYPE VARCHAR(1) " + Constants.vbCrLf
          //  + " AS " + Constants.vbCrLf
          //  + " DECLARE @CODE VARCHAR(10),@VALUE DECIMAL(15,2),@BOOKCODE VARCHAR(10),@AMOUNT NUMERIC(17,2)  " + Constants.vbCrLf
          //  + " IF (@TYPE='+') SET @VALUE=-1 " + Constants.vbCrLf
          //  + " ELSE IF (@TYPE='-') SET @VALUE=1 " + Constants.vbCrLf

          //  //CREDIT

          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='000000',@CODE=CUSTCODE,@AMOUNT=BILLAMT*-1 * @VALUE FROM SRETURN" + FrmCode + " WHERE BILLTYPE='2'  " + Constants.vbCrLf
          //  + " AND CUSTCODE<>'CANCEL' AND BILLAMT>0 AND ORDERNO=@ORDERNO " + Constants.vbCrLf

          //  + " IF (@TYPE='+') " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'000000',CUSTCODE,'S',ORDERNO,'Sales Return Bill No.'+ convert(varchar,orderno)+ ' - ' + convert(varchar(150),Remarks1)  , " + Constants.vbCrLf
          //  + " BILLAMT*-1 * @VALUE,ORDERNO,0,'',NULL, NULL,0,USERID FROM SRETURN" + FrmCode + " WHERE BILLTYPE='2'  " + Constants.vbCrLf
          //  + " AND CUSTCODE<>'CANCEL' AND BILLAMT>0 AND ORDERNO=@ORDERNO " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT  " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE IF (@TYPE='-')  " + Constants.vbCrLf
          //  + " BEGIN  " + Constants.vbCrLf

          //  + " DECLARE @RECNO BIGINT  " + Constants.vbCrLf
          //  + " DECLARE SA CURSOR FOR   " + Constants.vbCrLf
          //  + " SELECT RECNO FROM VOUCHER" + FrmCode + "  WHERE [DATE1]=@DATE AND [BOOKCODE]='000000' AND [TRANSTYPE]='S'  " + Constants.vbCrLf
          //  + " AND BILLNO=@ORDERNO   " + Constants.vbCrLf
          //  + " OPEN SA  " + Constants.vbCrLf
          //  + " FETCH NEXT FROM SA INTO @RECNO  " + Constants.vbCrLf
          //  + " WHILE @@FETCH_STATUS=0  " + Constants.vbCrLf
          //  + " BEGIN  " + Constants.vbCrLf
          //  + " SET @CODE=''  " + Constants.vbCrLf
          //  + " SET @BOOKCODE=''  " + Constants.vbCrLf
          //  + " SET @AMOUNT=0  " + Constants.vbCrLf
          //  + " SELECT @CODE=CODE,@BOOKCODE=BOOKCODE,@AMOUNT=AMOUNT FROM VOUCHER" + FrmCode + " WHERE [DATE1]=@DATE AND [BOOKCODE]='000000' AND [TRANSTYPE]='S'  " + Constants.vbCrLf
          //  + " AND BILLNO=@ORDERNO  AND RECNO=@RECNO  " + Constants.vbCrLf
          //  + " SET @AMOUNT=@AMOUNT*-1  " + Constants.vbCrLf
          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT  " + Constants.vbCrLf
          //  + " DELETE FROM VOUCHER" + FrmCode + " WHERE [DATE1]=@DATE AND [BOOKCODE]='000000' AND [TRANSTYPE]='S'  " + Constants.vbCrLf
          //  + " AND BILLNO=@ORDERNO AND RECNO=@RECNO  " + Constants.vbCrLf
          //  + " FETCH NEXT FROM SA INTO @RECNO  " + Constants.vbCrLf
          //  + " END   " + Constants.vbCrLf
          //  + " CLOSE SA  " + Constants.vbCrLf
          //  + " DEALLOCATE SA  " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //      //ROUNDOFF


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='000000' ,@CODE='600001',@AMOUNT=SUM(ROUNDOFF) * @VALUE  " + Constants.vbCrLf
          //  + " FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND ROUNDOFF<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE='600001' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'000000' ,'600001','Z',0,'Credit',SUM(ROUNDOFF) * @VALUE ,0,0,'',NULL, NULL,0,MAX(USERID)  " + Constants.vbCrLf
          //  + " FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND ROUNDOFF<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " UPDATE [VOUCHER" + FrmCode + "] SET AMOUNT=AMOUNT+ " + Constants.vbCrLf
          //  + " ISNULL((SELECT SUM(ROUNDOFF) * @VALUE FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND ROUNDOFF<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 ),0) WHERE BOOKCODE='000000' AND CODE='600001' AND TRANSTYPE='Z' AND DATE1=@DATE  " + Constants.vbCrLf
          //  + " END " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='200001' ,@CODE='600001',@AMOUNT=SUM(ROUNDOFF) * @VALUE  " + Constants.vbCrLf
          //  + " FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND ROUNDOFF<>0   " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE='600001' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'200001' ,'600001','Z',0,'Cash',SUM(ROUNDOFF) * @VALUE ,0,0,'',NULL, NULL,0,MAX(USERID)  " + Constants.vbCrLf
          //  + " FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND ROUNDOFF<>0   " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " UPDATE [VOUCHER" + FrmCode + "] SET AMOUNT=AMOUNT+ " + Constants.vbCrLf
          //  + " ISNULL((SELECT SUM(ROUNDOFF) * @VALUE FROM SRETURN" + FrmCode + "  WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND ROUNDOFF<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 ),0) WHERE BOOKCODE='200001' AND CODE='600001' AND TRANSTYPE='Z' AND DATE1=@DATE  " + Constants.vbCrLf
          //  + " END " + Constants.vbCrLf

          //   //EXPENSE


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='000000' ,@CODE='603',@AMOUNT=SUM(EXPENSE) * @VALUE  " + Constants.vbCrLf
          //  + " FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND EXPENSE<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE='603' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'000000' ,'603','Z',0,'Credit',SUM(EXPENSE) * @VALUE ,0,0,'',NULL, NULL,0,MAX(USERID)  " + Constants.vbCrLf
          //  + " FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND EXPENSE<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " UPDATE [VOUCHER" + FrmCode + "] SET AMOUNT=AMOUNT+ " + Constants.vbCrLf
          //  + " ISNULL((SELECT SUM(EXPENSE) * @VALUE FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='2' AND EXPENSE<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 ),0) WHERE BOOKCODE='000000' AND CODE='603' AND TRANSTYPE='Z' AND DATE1=@DATE  " + Constants.vbCrLf
          //  + " END " + Constants.vbCrLf


          //  + " SET @CODE='' " + Constants.vbCrLf
          //  + " SET @BOOKCODE='' " + Constants.vbCrLf
          //  + " SET @AMOUNT=0 " + Constants.vbCrLf

          //  + " SELECT @BOOKCODE='200001' ,@CODE='603',@AMOUNT=SUM(EXPENSE) * @VALUE  " + Constants.vbCrLf
          //  + " FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND EXPENSE<>0   " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT " + Constants.vbCrLf

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE='603' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   " + Constants.vbCrLf
          //  + " SELECT DATE1,'200001' ,'603','Z',0,'Cash',SUM(EXPENSE) * @VALUE ,0,0,'',NULL, NULL,0,MAX(USERID)  " + Constants.vbCrLf
          //  + " FROM SRETURN" + FrmCode + " WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND EXPENSE<>0   " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 " + Constants.vbCrLf
          //  + " END  " + Constants.vbCrLf
          //  + " ELSE " + Constants.vbCrLf
          //  + " BEGIN " + Constants.vbCrLf
          //  + " UPDATE [VOUCHER" + FrmCode + "] SET AMOUNT=AMOUNT+ " + Constants.vbCrLf
          //  + " ISNULL((SELECT SUM(EXPENSE) * @VALUE FROM SRETURN" + FrmCode + "  WHERE CUSTCODE<>'CANCEL' AND BILLTYPE='1' AND EXPENSE<>0  " + Constants.vbCrLf
          //  + " AND ORDERNO=@ORDERNO GROUP BY DATE1 ),0) WHERE BOOKCODE='200001' AND CODE='603' AND TRANSTYPE='Z' AND DATE1=@DATE  " + Constants.vbCrLf
          //  + " END " + Constants.vbCrLf


          //  //AMOUNT


          //  + " INSERT INTO ACCOUNTS" + FrmCode + "(CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS,LEGAL,USERID,BILL) " + Constants.vbCrLf
          //  + " SELECT '42' + convert(varchar, CONVERT(int,A.TAXPER)) CODE ,'Sales Return '+ " + Constants.vbCrLf
          //  + " + (SELECT DETAILS FROM LOOKUP WHERE FIELD1='TAXPER' AND CODE=MAX(A.TAXPER)) HEAD " + Constants.vbCrLf
          //  + " ,0.0,'S','2','42',2,0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N' " + Constants.vbCrLf
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO " + Constants.vbCrLf
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.ORDERNO=@ORDERNO  "
          //  + " AND ('42' + convert(varchar, CONVERT(int,A.TAXPER))) NOT  IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ") "
          //  + " GROUP BY A.DATE1,A.TAXPER "


          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DECLARE SA CURSOR FOR  "
          //  + " SELECT '42' + convert(varchar, CONVERT(int,A.TAXPER)) CODE  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND B.ORDERNO=@ORDERNO  "
          //  + " GROUP BY A.DATE1,A.TAXPER "
          //  + " OPEN SA "
          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " WHILE @@FETCH_STATUS=0 "
          //  + " BEGIN "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'000000','42' + convert(varchar, CONVERT(int,A.TAXPER)),'Z',0,'Credit', "
          //  + " (SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE ,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '42' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER "
          //  + " END  "
          //  + " ELSE "
          //  + " BEGIN "

          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT (SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '42' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER),0) "
          //  + " WHERE BOOKCODE='000000' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE "

          //  + " END "

          //  + " SELECT @BOOKCODE='000000',@CODE='42' + convert(varchar, CONVERT(int,A.TAXPER)), "
          //  + " @AMOUNT=(SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '42' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " END  "
          //  + " CLOSE SA "
          //  + " DEALLOCATE SA "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DECLARE SA CURSOR FOR  "
          //  + " SELECT '42' + convert(varchar, CONVERT(int,A.TAXPER)) CODE  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND B.ORDERNO=@ORDERNO  "
          //  + " GROUP BY A.DATE1,A.TAXPER "
          //  + " OPEN SA "
          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " WHILE @@FETCH_STATUS=0 "
          //  + " BEGIN "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'200001','42' + convert(varchar, CONVERT(int,A.TAXPER)),'Z',0,'Cash', "
          //  + " (SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE ,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '42' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER "
          //  + " END  "
          //  + " ELSE "
          //  + " BEGIN "

          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT (SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1'  AND B.ORDERNO=@ORDERNO  "
          //  + " AND '42' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER),0) "
          //  + " WHERE BOOKCODE='200001' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE "

          //  + " END "

          //  + " SELECT @BOOKCODE='200001',@CODE='42' + convert(varchar, CONVERT(int,A.TAXPER)), "
          //  + " @AMOUNT=(SUM(A.NETAMT) -SUM(A.TAXAMT) -SUM(A.CESSAMT)) * @VALUE  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND B.ORDERNO=@ORDERNO  "
          //  + " AND '42' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE "
          //  + " GROUP BY A.DATE1,A.TAXPER "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "


          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " END  "
          //  + " CLOSE SA "
          //  + " DEALLOCATE SA "


          //  //TAX

          //  + " INSERT INTO ACCOUNTS" + FrmCode + "(CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS,LEGAL,USERID,BILL) "
          //  + " SELECT '142' + convert(varchar, CONVERT(int,A.TAXPER)) CODE ,'Vat Sales Return '+ "
          //  + " + (SELECT DETAILS FROM LOOKUP WHERE FIELD1='TAXPER' AND CODE=MAX(A.TAXPER)) "
          //  + " ,0.0,'L','2','17',2,0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N' "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + "  B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND  B.ORDERNO=@ORDERNO  "
          //  + " AND ('142' + convert(varchar, CONVERT(int,A.TAXPER))) NOT  IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ") "
          //  + " AND A.TAXPER<>0 GROUP BY A.DATE1,A.TAXPER "


          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DECLARE SA CURSOR FOR  "
          //  + " SELECT '142' + convert(varchar, CONVERT(int,A.TAXPER)) CODE "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1,A.TAXPER "
          //  + " OPEN SA "
          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " WHILE @@FETCH_STATUS=0 "
          //  + " BEGIN "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'000000','142' + convert(varchar, CONVERT(int,A.TAXPER)),'Z',0,'Credit', "
          //  + " SUM(A.TAXAMT)  * @VALUE,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '142' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER "
          //  + " END  "
          //  + " ELSE "
          //  + " BEGIN "

          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT SUM(A.TAXAMT)  * @VALUE "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '142' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER),0) "
          //  + " WHERE BOOKCODE='000000' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE "

          //  + " END "

          //  + " SELECT @BOOKCODE='000000',@CODE='142' + convert(varchar, CONVERT(int,A.TAXPER)), "
          //  + " @AMOUNT=SUM(A.TAXAMT)  * @VALUE FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '142' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " END  "
          //  + " CLOSE SA "
          //  + " DEALLOCATE SA "



          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DECLARE SA CURSOR FOR  "
          //  + " SELECT '142' + convert(varchar, CONVERT(int,A.TAXPER)) CODE "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1,A.TAXPER "
          //  + " OPEN SA "
          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " WHILE @@FETCH_STATUS=0 "
          //  + " BEGIN "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'200001','142' + convert(varchar, CONVERT(int,A.TAXPER)),'Z',0,'Cash', "
          //  + " SUM(A.TAXAMT)  * @VALUE,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '142' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER "
          //  + " END  "
          //  + " ELSE "
          //  + " BEGIN "

          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT SUM(A.TAXAMT)  * @VALUE "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '142' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER),0) "
          //  + " WHERE BOOKCODE='200001' AND CODE=@CODE AND TRANSTYPE='Z' AND DATE1=@DATE "

          //  + " END "

          //  + " SELECT @BOOKCODE='200001',@CODE='142' + convert(varchar, CONVERT(int,A.TAXPER)), "
          //  + " @AMOUNT=SUM(A.TAXAMT)  * @VALUE "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.TAXPER>0 AND A.TAXAMT<>0  "
          //  + " AND B.ORDERNO=@ORDERNO  "
          //  + " AND '142' + convert(varchar, CONVERT(int,A.TAXPER))=@CODE GROUP BY A.DATE1,A.TAXPER "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " FETCH NEXT FROM SA INTO @CODE "
          //  + " END  "
          //  + " CLOSE SA "
          //  + " DEALLOCATE SA "

          //  //CESS

          //  + " INSERT INTO ACCOUNTS" + FrmCode + "(CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS,LEGAL,USERID,BILL) "
          //  + " SELECT  TOP 1 '1420' CODE ,'Cess on Vat - Sales Return' HEAD "
          //  + " ,0.0,'L','2','17',2,0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N' "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + "  B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.ORDERNO=@ORDERNO  "
          //  + " AND ('1420') NOT  IN (SELECT CODE FROM ACCOUNTS" + FrmCode + ") "
          //  + " GROUP BY A.DATE1 "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='000000' AND CODE='1420' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'000000','1420' ,'Z',0,'Credit',SUM(A.CESSAMT)  * @VALUE,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SRITEM" + FrmCode + "  A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 "
          //  + " END "
          //  + " ELSE "
          //  + " BEGIN "
          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT SUM(A.CESSAMT)  * @VALUE  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO   "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 ),0) "
          //  + " WHERE BOOKCODE='000000' AND CODE='1420' AND TRANSTYPE='Z' AND DATE1=@DATE "
          //  + " END "

          //  + " SELECT @BOOKCODE='000000',@CODE='1420' ,@AMOUNT=SUM(A.CESSAMT)  * @VALUE "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='2' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " IF ((SELECT COUNT(*) cc FROM VOUCHER" + FrmCode + " WHERE BOOKCODE='200001' AND CODE='1420' AND TRANSTYPE='Z' AND DATE1=@DATE)=0) "
          //  + " BEGIN "
          //  + " INSERT INTO [VOUCHER" + FrmCode + "]([DATE1], [BOOKCODE], [CODE], [TRANSTYPE], [TRANSNO], [NARRATION], [AMOUNT], [BILLNO], [CHEQUE], [CHQNO], [CHQPASSDATE], [CHQDATE], [CHQPASSED], [USERID])   "
          //  + " SELECT A.DATE1,'200001','1420' ,'Z',0,'Cash',SUM(A.CESSAMT)  * @VALUE,0,0,'',NULL, NULL,0,max(b.userid)  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 "
          //  + " END "
          //  + " ELSE "
          //  + " BEGIN "
          //  + " UPDATE VOUCHER" + FrmCode + " SET AMOUNT=AMOUNT + ISNULL((SELECT SUM(A.CESSAMT)  * @VALUE  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 ),0) "
          //  + " WHERE BOOKCODE='200001' AND CODE='1420' AND TRANSTYPE='Z' AND DATE1=@DATE "
          //  + " END "

          //  + " SELECT @BOOKCODE='200001',@CODE='1420' ,@AMOUNT=SUM(A.CESSAMT)  * @VALUE  "
          //  + " FROM SRITEM" + FrmCode + " A JOIN  SRETURN" + FrmCode + " B ON A.ORDERNO=B.ORDERNO  "
          //  + " WHERE B.CUSTCODE<>'CANCEL' AND B.BILLTYPE='1' AND A.CESSPER>0  AND A.CESSAMT>0  "
          //  + " AND B.ORDERNO=@ORDERNO GROUP BY A.DATE1 "

          //  + " EXEC SP_UPDATE_BALANCE" + FrmCode + " @CODE,@BOOKCODE,@AMOUNT "

          //  + " SET @CODE ='' "
          //  + " SET @BOOKCODE='' "
          //  + " SET @AMOUNT=0 "

          //  + " DELETE FROM VOUCHER" + FrmCode + " WHERE AMOUNT=0 ";

          //  comm = new SqlCommand(sql, cPublic.Connection);
          //  comm.ExecuteNonQuery();



          //  # endregion sales posting          


            Function(FrmCode);
        }

        public void Triggers(string FrmCode)
        {

        }

        public void OtherTables(string FrmCode)
        {

            OtherSPs(FrmCode);
        }
        public void OtherTables()
        {
            OtherSPs();
        }

        public void OtherSPs()
        {
            string sql = string.Empty;
            SqlCommand comm;
            //  -------  Procedure for Insert Records to LOOKUP  ---------

            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_INSERT_LOOKUP') ) "
            + " drop PROCEDURE SP_INSERT_LOOKUP";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE SP_INSERT_LOOKUP"
            + "(@field1     [varchar](10),"
            + "@code        [varchar](10),"
            + "@details     [varchar](100))"
            + "AS INSERT INTO LOOKUP ([Field1], [Code], [Details]) VALUES (@field1, @code, @details)";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            //  ------  Procedure for Update LOOKUP  --------
            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_UPDATE_LOOKUP') ) "
            + " drop PROCEDURE SP_UPDATE_LOOKUP";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE SP_UPDATE_LOOKUP"
            + "(@field1     [varchar](10),"
            + "@code        [varchar](10),"
            + "@details [varchar](100))"
            + "AS UPDATE LOOKUP SET [Details] = @details WHERE [Field1] = @field1 and [Code]=@code";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            //  ------  Procedure for Delete Records from LOOKUP  ------
            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_DELETE_LOOKUP') ) "
            + " drop PROCEDURE SP_DELETE_LOOKUP";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE SP_DELETE_LOOKUP"
            + "(@field1     [varchar](10),"
            + "@code        [varchar](10))"
            + "AS DELETE FROM LOOKUP where [Field1] = @field1 and [Code] = @code ";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

        }

        public void OtherSPs(string FrmCode)
        {
            string sql = string.Empty;
            SqlCommand comm;
            //  -------  Procedure for Insert Records to LOOKUP  ---------

            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_INSERT_LOOKUP" + FrmCode + "') ) "
            + " drop PROCEDURE SP_INSERT_LOOKUP" + FrmCode;
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE SP_INSERT_LOOKUP" + FrmCode
            + "(@field1     [varchar](10),"
            + "@code        [varchar](10),"
            + "@details     [varchar](100))"
            + "AS INSERT INTO LOOKUP ([Field1], [Code], [Details]) VALUES (@field1, @code, @details)";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            //  ------  Procedure for Update LOOKUP  --------
            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_UPDATE_LOOKUP" + FrmCode + "') ) "
            + " drop PROCEDURE SP_UPDATE_LOOKUP" + FrmCode;
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE SP_UPDATE_LOOKUP" + FrmCode
            + "(@field1     [varchar](10),"
            + "@code        [varchar](10),"
            + "@details [varchar](100))"
            + "AS UPDATE LOOKUP SET [Details] = @details WHERE [Field1] = @field1 and [Code]=@code";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            //  ------  Procedure for Delete Records from LOOKUP  ------
            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_DELETE_LOOKUP" + FrmCode + "') ) "
            + " drop PROCEDURE SP_DELETE_LOOKUP" + FrmCode;
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE SP_DELETE_LOOKUP" + FrmCode
            + "(@field1     [varchar](10),"
            + "@code        [varchar](10))"
            + "AS DELETE FROM LOOKUP where [Field1] = @field1 and [Code] = @code ";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
        }

        public void firmcreation(string FrmCode)
        {   //firm creation

            string sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Firms]')"
                   + " and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[Firms]"
                   + " CREATE TABLE [dbo].[Firms] ("
                   + "[slno] [bigint] IDENTITY (1, 1) NOT NULL ,"
                   + "[clddate] [datetime] NULL ,"
                   + "[addr1] [varchar] (40) NULL ,"
                   + "[addr2] [varchar] (40) NULL ,"
                   + "[addr3] [varchar] (40) NULL ,"
                   + "[addr4] [varchar] (40) NULL ,"
                   + "[code] [varchar] (20) NOT NULL ,"
                   + "[name] [varchar] (50) NULL ,"
                   + "[dispname] [varchar] (50) NULL ,"
                   + "[cstno] [varchar] (30) NULL ,"
                   + "[cformat] [varchar] (10) NULL ,"
                   + "[csymbol] [varchar] (10) NULL ,"
                   + "[email] [varchar] (50) NULL ,"
                   + "[jurid] [varchar] (40) NULL ,"
                   + "[kgst] [varchar] (30) NULL ,"
                   + "[opdate] [datetime] NULL ,"
                   + "[panno] [varchar] (20) NULL ,"
                   + "[phone1] [varchar] (40) NULL ,"
                   + "[phone2] [varchar] (40) NULL ,"
                   + "[Path] [varchar] (4) NULL ,"
                   + "[SecondPath] [varchar] (4) NULL ,"
                   + "[Status] [varchar] (4) NULL "
                   + ") ON [PRIMARY]";
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            firmsp();
        }

        public void firmsp()
        {

            //SP FOR INSERTION
            string sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'sp_insert_firms') ) "
            + " drop PROCEDURE sp_insert_firms";
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE sp_insert_firms"
                + "(  @clddate 	[datetime],"
                + "@addr1 	[varchar](40)='',"
                + "@addr2  [varchar](40)='',"
                + "@addr3 	[varchar](40)='',"
                + "@addr4 	[varchar](40)='',"
                + "@code 	[varchar](20)='',"
                + "@name	[varchar](50)='',"
                + "@dispname 	[varchar](50)='',"
                + "@cstno 	[varchar](30)='',"
                + "@cformat 	[varchar](10)='',"
                + "@csymbol 	[varchar](10)='',"
                + "@email 	[varchar](50)='',"
                + "@jurid 	[varchar](40)='',"
                + "@kgst 	[varchar](30)='',"
                + "@opdate 	[datetime],"
                + "@panno 	[varchar](20)='',"
                + "@phone1	[varchar](40)='',"
                + "@phone2 	[varchar](40)='',"
                + "@Path 	[varchar](4)='',"
                + "@SecondPath 	[varchar](4)='',"
                + "@Status 	[varchar](4)='')"
            + "AS INSERT INTO [Firms] "
            + "( [clddate],[addr1],[addr2],[addr3],[addr4],[code],[name],[dispname],[cstno],[cformat],[csymbol],[email],[jurid],[kgst],[opdate],[panno],[phone1],[phone2],[Path],[SecondPath],[Status])"
            + "VALUES( @clddate,@addr1,@addr2,@addr3,@addr4,@code,@name,@dispname,@cstno,@cformat,@csymbol,@email,@jurid,@kgst,@opdate,@panno,@phone1,@phone2,@Path,@SecondPath,@Status)";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            //SP FOR UPDATION
            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'sp_update_firms') ) "
            + " drop PROCEDURE sp_update_firms";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE sp_update_firms"
         + "(@code 	[varchar](6),"
         + "@clddate 	[datetime],"
         + "@addr1 	[varchar](40)='',"
         + "@addr2 	[varchar](40)='',"
         + "@addr3 	[varchar](40)='',"
         + "@addr4 	[varchar](40)='',"
         + "@name 	    [varchar](50)='',"
         + "@dispname 	[varchar](50)='',"
         + "@cstno 	[varchar](30)='',"
         + "@cformat 	[varchar](10)='',"
         + "@csymbol 	[varchar](10)='',"
         + "@email 	[varchar](50)='',"
         + "@jurid	 	[varchar](40)='',"
         + "@kgst	 	[varchar](30)='',"
         + "@opdate 	[datetime],"
         + "@panno 	[varchar](20)='',"
         + "@phone1 	[varchar](40)='',"
         + "@phone2 	[varchar](40)='',"
         + "@Path	 	[varchar](4)='',"
         + "@SecondPath	 	[varchar](4)='')"
         + "AS UPDATE [Firms] "
         + "SET  [clddate]	 = @clddate,[addr1]= @addr1,[addr2]= @addr2,[addr3]= @addr3,[addr4]= @addr4,[code]= @code,[name]= @name,[dispname]= @dispname,[cstno]= @cstno,[cformat]= @cformat,[csymbol]= @csymbol,[email]= @email,[jurid]= @jurid,[kgst]= @kgst,[opdate]= @opdate,[panno]= @panno,[phone1]= @phone1,[phone2]= @phone2,[Path]= @Path,[SecondPath]= @SecondPath "
         + "WHERE([code]= @code)";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            //SP FOR DELETION
            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'sp_delete_firms') ) "
            + " drop PROCEDURE sp_delete_firms";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE sp_delete_firms"
                 + "(@code_6 	[varchar])"
                 + "AS DELETE  from  [Firms]"
                 + "WHERE ( [code] = @code_6)";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();
        }

        public void companyloginsps()
        {
            if (cPublic.Connection.State == ConnectionState.Closed)
            { cPublic.Connection.Open(); }

            string sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'sp_insert_company') ) "
            + " drop PROCEDURE sp_insert_company";
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE sp_insert_company"
                    + "(  @clddate 	[datetime],"
                    + " @addr1 	[varchar](40)='',"
                    + " @addr2   	[varchar](40)='',"
                    + " @addr3 	[varchar](40)='',"
                    + " @addr4 	[varchar](40)='',"
                    + "@code 	[varchar](20)='',"
                    + "@name	[varchar](50)='',"
                    + "@dispname 	[varchar](50)='',"
                      + "@abbr 	[varchar](50)='',"
                    + "@cstno 	[varchar](30)='',"
                    + "@cformat 	[varchar](10)='',"
                    + "@csymbol 	[varchar](10)='',"
                    + "@email 	[varchar](50)='',"
                    + "@jurid 	[varchar](40)='',"
                    + "@kgst 	[varchar](30)='',"
                    + "@opdate 	[datetime],"
                    + "@panno 	[varchar](20)='',"
                    + "@phone1	[varchar](40)='',"
                    + "@phone2 	[varchar](40)='',"
                    + "@Path 	[varchar](4)='',"
                    + "@SecondPath 	[varchar](4)='',"
                    + "@acctype 	[varchar](10)='',"
                     + "@picture image null)"
                + "AS INSERT INTO [Company] "
                + "( [clddate],[addr1],[addr2],[addr3],[addr4],[code],[name],[dispname],[abbr],[cstno],[cformat],[csymbol],[email],[jurid],[kgst],[opdate],[panno],[phone1],[phone2],[Path],[SecondPath],[acctype],[picture])"
                + "VALUES( @clddate,@addr1,@addr2,@addr3,@addr4,@code,@name,@dispname,@abbr,@cstno,@cformat,@csymbol,@email,@jurid,@kgst,@opdate,@panno,@phone1,@phone2,@Path,@SecondPath,@acctype,@picture)";
            SqlCommand COMM = new SqlCommand(sql, cPublic.Connection);
            COMM.ExecuteNonQuery();

            //SP FOR UPDATION
            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'sp_update_company') ) "
            + " drop PROCEDURE sp_update_company";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE sp_update_company"
                 + "(@code 	[varchar](6),"
                 + "@clddate 	[datetime],"
                 + "@addr1 	[varchar](40)='',"
                 + "@addr2 	[varchar](40)='',"
                 + "@addr3 	[varchar](40)='',"
                 + "@addr4 	[varchar](40)='',"
                 + "@name 	    [varchar](50)='',"
                 + "@dispname 	[varchar](50)='',"
                 + "@cstno 	[varchar](30)='',"
                 + "@cformat 	[varchar](10)='',"
                 + "@csymbol 	[varchar](10)='',"
                 + "@email 	[varchar](50)='',"
                 + "@jurid	 	[varchar](40)='',"
                 + "@kgst	 	[varchar](30)='',"
                 + "@opdate 	[datetime],"
                 + "@panno 	[varchar](20)='',"
                 + "@phone1 	[varchar](40)='',"
                 + "@phone2 	[varchar](40)='',"
                 + "@Path	 	[varchar](4)='',"
                  + "@SecondPath	 	[varchar](4)='',"
                 + "@acctype 	[varchar](10)='')"
                 + "AS UPDATE [Company] "
                 + "SET  [clddate]	 = @clddate,[addr1]= @addr1,[addr2]= @addr2,[addr3]= @addr3,[addr4]= @addr4,[code]= @code,[name]= @name,[dispname]= @dispname,[cstno]= @cstno,[cformat]= @cformat,[csymbol]= @csymbol,[email]= @email,[jurid]= @jurid,[kgst]= @kgst,[opdate]= @opdate,[panno]= @panno,[phone1]= @phone1,[phone2]= @phone2,[Path]= @Path,[SecondPath]= @SecondPath,[acctype]= @acctype "
                 + "WHERE([code]= @code)";
            COMM = new SqlCommand(sql, cPublic.Connection);
            COMM.ExecuteNonQuery();

            //SP FOR DELETION
            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'sp_delete_company') ) "
            + " drop PROCEDURE sp_delete_company";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE sp_delete_company"
                 + "(@code_6 	[varchar])"
                 + "AS DELETE  from  [Company]"
                 + "WHERE ( [code] = @code_6)";
            COMM = new SqlCommand(sql, cPublic.Connection);
            COMM.ExecuteNonQuery();

            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_INSERT_LOGIN') ) "
            + " drop PROCEDURE SP_INSERT_LOGIN";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE SP_INSERT_LOGIN"
              + "(@UserLevel 	[int],"
              + "@UserName 	[varchar](40),"
              + "@userid 	[varchar](6),"
              + "@Password	[varchar](20))"
              + "AS INSERT INTO LOGIN( [UserLevel], [UserName],[userid],[Password])"
              + "VALUES( @UserLevel, @UserName, @userid,@Password)";
            COMM = new SqlCommand(sql, cPublic.Connection);
            COMM.ExecuteNonQuery();

            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_UPDATE_LOGIN') ) "
            + " drop PROCEDURE SP_UPDATE_LOGIN";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE  SP_UPDATE_LOGIN"
           + "(@userid 	[varchar](6),"
           + "@UserLevel 	[int],"
           + "@UserName 	[varchar](40))"
           + "AS UPDATE LOGIN  SET  [UserLevel]= @UserLevel, [UserName]= @UserName "
           + "WHERE(  [userid] = @userid )";
            COMM = new SqlCommand(sql, cPublic.Connection);
            COMM.ExecuteNonQuery();

            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_DELETE_LOGIN') ) "
            + " drop PROCEDURE SP_DELETE_LOGIN";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE SP_DELETE_LOGIN"
           + "( @userid_3 	[varchar](6))"
           + "AS DELETE FROM LOGIN WHERE (  [userid]	 = @userid_3 )";
            COMM = new SqlCommand(sql, cPublic.Connection);
            COMM.ExecuteNonQuery();

            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_UPDATE_LOGIN_PASSWORD') ) "
            + " drop PROCEDURE SP_UPDATE_LOGIN_PASSWORD";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE  SP_UPDATE_LOGIN_PASSWORD "
           + " (@userid 	[varchar](6),@UserLevel 	[int],"
           + " @UserName 	[varchar](40),@Password 	[varchar](20)) "
           + " AS UPDATE LOGIN  SET  [UserLevel]= @UserLevel, "
           + " [UserName]= @UserName,[Password]= @Password WHERE(  [userid] = @userid ) ";
            COMM = new SqlCommand(sql, cPublic.Connection);
            COMM.ExecuteNonQuery();

            //proceedures fro uercomp

            sql = "if exists (select * from dbo.sysobjects where id ="
            + " object_id(N'SP_INSERT_USERCOMP') ) "
            + " drop PROCEDURE SP_INSERT_USERCOMP";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "CREATE PROCEDURE SP_INSERT_USERCOMP"
              + "(@userid 	[varchar](6),"
              + "@code	[varchar](3))"
              + "AS INSERT INTO USERCOMPANIES "
              + "VALUES(@userid,@code)";

            COMM = new SqlCommand(sql, cPublic.Connection);
            COMM.ExecuteNonQuery();
            //triger for usrecomp
            sql = "create trigger del_usercomps  on login after delete,update as delete from usercompanies  from deleted d where usercompanies.userid=d.userid ";
            COMM = new SqlCommand(sql, cPublic.Connection);
            COMM.ExecuteNonQuery();
        }

        DateTime dtop, dtcl;

        public void companydemoandlogin()
        {

            if (cPublic.g_Rdate.Month >= 4)
            {
                dtop = new DateTime(DateTime.Now.Year, 04, 1);
                dtcl = new DateTime((DateTime.Now.Year + 1), 03, 31);

            }
            else
            {

                dtop = new DateTime((DateTime.Now.Year - 1), 04, 1);
                dtcl = new DateTime((DateTime.Now.Year), 03, 31);
            }
            //demo insert company
            SqlCommand sqlcmd = new SqlCommand("SP_INSERT_COMPANY", cPublic.Connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@code", "001");
            sqlcmd.Parameters.AddWithValue("@name", "DEMO");
            sqlcmd.Parameters.AddWithValue("@dispname", "DEMO COMPANY");
            sqlcmd.Parameters.AddWithValue("@acctype", "SINGLE");
            sqlcmd.Parameters.AddWithValue("@Path", cPublic.drive);
            sqlcmd.Parameters.AddWithValue("@clddate", dtcl);
            sqlcmd.Parameters.AddWithValue("@opdate", dtop);
            sqlcmd.Parameters.AddWithValue("@abbr ", "DM");
            sqlcmd.Parameters.AddWithValue("@Picture ", null);
            sqlcmd.ExecuteNonQuery();
            //demo login
            sqlcmd = new SqlCommand("SP_INSERT_LOGIN", cPublic.Connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@UserLevel ", "9");
            sqlcmd.Parameters.AddWithValue("@UserName", "ADMINISTRATOR");
            sqlcmd.Parameters.AddWithValue("@userid", "ADMIN");
            sqlcmd.Parameters.AddWithValue("@Password", "admin");
            sqlcmd.ExecuteNonQuery();
        }

        public void firminsert(string code, string dispname)
        {
            if (cPublic.g_Rdate.Month >= 4)
            {
                dtop = new DateTime(DateTime.Now.Year, 04, 1);
                dtcl = new DateTime((DateTime.Now.Year + 1), 03, 31);
            }
            else
            {
                dtop = new DateTime((DateTime.Now.Year - 1), 04, 1);
                dtcl = new DateTime((DateTime.Now.Year), 03, 31);
            }
            con.RefreshConn();

            SqlCommand sqlcmd = new SqlCommand("SP_INSERT_FIRMS", cPublic.Connection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@code", code);
            sqlcmd.Parameters.AddWithValue("@name", "DEMO");
            if (dispname == string.Empty)
            { sqlcmd.Parameters.AddWithValue("@dispname", "DEMO COMPANY"); }
            else { sqlcmd.Parameters.AddWithValue("@dispname", dispname); }
            sqlcmd.Parameters.AddWithValue("@clddate", dtcl);
            sqlcmd.Parameters.AddWithValue("@opdate", dtop);
            sqlcmd.Parameters.AddWithValue("@status", "D");
            sqlcmd.ExecuteNonQuery();
        }

        public Boolean CreateTableCompany(string tablename)
        {
            if (cPublic.Connection.State == System.Data.ConnectionState.Closed)
            { cPublic.Connection.Open(); }

            string sql = "if exists (select * from dbo.sysobjects where id = "
                + " object_id(N'[dbo].[Company]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
                + " drop table [dbo].[Company]"
                + " CREATE TABLE [dbo].[Company] ("
                + "[clddate] [datetime] NULL ,"
                + "[addr1] [varchar] (40) NULL ,"
                + "[addr2] [varchar] (40) NULL ,"
                + "[addr3] [varchar] (40) NULL ,"
                + "[addr4] [varchar] (40) NULL ,"
                + "[code] [varchar] (20) NOT NULL ,"
                + "[name] [varchar] (50) NULL ,"
                + "[dispname] [varchar] (50) NULL ,"
                 + "[abbr] [varchar](50) NULL,"
                + "[cstno] [varchar] (30) NULL ,"
                + "[cformat] [varchar] (10) NULL ,"
                + "[csymbol] [varchar] (10) NULL ,"
                + "[email] [varchar] (50) NULL ,"
                + "[jurid] [varchar] (40) NULL ,"
                + "[kgst] [varchar] (30) NULL ,"
                + "[opdate] [datetime] NULL ,"
                + "[panno] [varchar] (20) NULL ,"
                + "[phone1] [varchar] (40) NULL ,"
                + "[phone2] [varchar] (40) NULL ,"
                + "[Path] [varchar] (4) NULL ,"
                + "[SecondPath] [varchar] (4) NULL ,"
                + "[acctype] [varchar](10) NOT NULL,"
                 + "[Picture] [image] NULL"
                + ") ON [PRIMARY]";
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "if exists (select * from dbo.sysobjects where id = " + Environment.NewLine
            + "object_id(N'[dbo].[SETRIGHTS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" + Environment.NewLine
            + "drop table [dbo].[SETRIGHTS]" + Environment.NewLine
            + " CREATE TABLE [SETRIGHTS] (" + Environment.NewLine
            + "[Level0] [bit] NULL , " + Environment.NewLine
            + "[Level1] [bit] NULL , " + Environment.NewLine
            + "[Level2] [bit] NULL , " + Environment.NewLine
            + "[Level3] [bit] NULL , " + Environment.NewLine
            + "[Level4] [bit] NULL , " + Environment.NewLine
            + "[Level5] [bit] NULL , " + Environment.NewLine
            + "[Level6] [bit] NULL , " + Environment.NewLine
            + "[Level7] [bit] NULL , " + Environment.NewLine
            + "[Level8] [bit] NULL , " + Environment.NewLine
            + "[Level9] [bit] NULL , " + Environment.NewLine
            + "[Menulevel] [bigint] NULL , " + Environment.NewLine
            + "[MenuDesc] [varchar] (40) NULL , " + Environment.NewLine
            + "[MnuName] [varchar] (50) NULL , " + Environment.NewLine
            + "[Parent] [varchar] (10) NULL , " + Environment.NewLine
            + "[Slno] [smallint] NULL " + Environment.NewLine
            + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();


            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, N'Purchase', N'Purchase', N'0', 1)				   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Purchase', N'Purchase', N'1', 20)				   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Purchase Return', N'Purchase Return', N'1', 30)  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, N'Payment', N'Payment', N'100', 101)			   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Cash Payment', N'Cash Payment', N'101', 120)	   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Bank Payment', N'Bank Payment', N'101', 130)	   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, N'Sales', N'Sales', N'200', 201)				   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Sales', N'Sales', N'201', 230)				   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Sales Return', N'Sales Return', N'201', 240)	   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Bank Payment', N'Bank Payment', N'501', 510)	   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Cash Reciepts', N'Cash Reciepts', N'501', 520)   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, N'Reports', N'Reports', N'801', 601)			   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, N'Tool', N'Tool', N'700', 701)					   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Backup', N'Backup', N'701', 702)				   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Restore', N'Restore', N'701', 703)			   ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Recalculate Stock', N'Recalculate Stock', N'701', 720)  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Updates', N'Updates', N'701', 730)					  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Check for Updates', N'Check for Updates', N'701', 740)  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Initialize', N'Initialize', N'701', 750)				  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, N'Master', N'Master', N'800', 801)						  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'A/c Master', N'A/c Master', N'806', 810)				  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Stock Master', N'Stock Master', N'806', 820)			  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Common Masters', N'Common Master', N'801', 830)		  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Set Parameters', N'Set Parameters', N'806', 840)		  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, N'Reciept', N'Reciept', N'900', 901)					  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Cash Reciept', N'Cash Reciept', N'901', 920)			  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Bank Reciept', N'Bank Reciept', N'901', 930)			  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, N'Journal Voucher', N'Journal Voucher', N'1000', 1001)	  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Transaction', N'Transaction', N'1001', 1010)			  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Journal', N'Journal', N'1001', 1040)					  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Debit Note', N'Debit Note', N'1001', 1050)			  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Credit Note', N'Credit Note', N'1001', 1060)			  ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Document Registration', N'Document Registration', N'801', 860) ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Renewal', N'Renewal', N'801', 861) ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Employee Registration', N'Employee Registration', N'801', 862) ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Leave Application', N'Leave Application', N'2000', 863)		 ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Leave Approval', N'Leave Approval', N'2000', 864)				 ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Rejoining', N'Rejoining', N'2000', 865)						 ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Leave', N'Leave', N'801', 2000)								 ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Employee Details', N'Employee Details', N'601', 650)			 ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();
            sql = "INSERT [dbo].[SETRIGHTS] ([Level0], [Level1], [Level2], [Level3], [Level4], [Level5], [Level6], [Level7], [Level8], [Level9], [Menulevel], [MenuDesc], [MnuName], [Parent], [Slno]) VALUES (1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, N'Sales Performance', N'Sales Performance', N'601', 695)		 ";
            comm = new SqlCommand(sql, cPublic.Connection); comm.ExecuteNonQuery();



            sql = "if exists (select * from dbo.sysobjects where id = "
            + "object_id(N'[dbo].[LOGIN]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            + "drop table [dbo].[LOGIN]"
            + " CREATE TABLE LOGIN ("
               + "[UserLevel] [int] NULL ,"
               + "[UserName] [varchar] (40) NULL ,"
               + "[userid] [varchar] (6) NULL ,"
               + "[Password] [varchar] (20) NULL "
               + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            sql = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USERCOMPANIES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
            + "drop table [dbo].[USERCOMPANIES]"
            + "CREATE TABLE [dbo].[USERCOMPANIES] ("
            + "[userid] [varchar] (6) NULL ,"
            + "[compcode] [varchar] (3) NULL "
            + ") ON [PRIMARY]";
            comm = new SqlCommand(sql, cPublic.Connection);
            comm.ExecuteNonQuery();

            companyloginsps();
            companydemoandlogin();
            return true;
        }

        public void headinsert(string FrmCode)
        {

            //inserting primary heads and values in to groups table

            fssdbfun.fssdbfun dbfun = new fssdbfun.fssdbfun();
            string fields = "CODE,HEAD,LEVEL1,GROUPLEVEL,GROUPCODE,BALANCE,OPBALANCE,LEGAL,USERID";
            string values = "'1','Liability','0','0','0',0,0,'1','SYSTEM'";
            SqlConnection con = new SqlConnection();
            con = cPublic.Connection;

            //default heads
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'2','Assets','0','0','0',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'21','Current Assets','1','1','2',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'22','Fixed Assets','1','1','2',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'200000','Cash & Banks','2','2','21',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'3','Trading Expense','0','0','0',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'4','Trading Income','0','0','0',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'5','Expense','0','0','0',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'6','Income','0','0','0',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);

            //FOR INSERTING DEFAULTS GROUPS AND ITZ VALUES
            values = "'17','VAT Collected & Paid','1','1','1',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'31','Purchase A/c','1','1','3',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'32','Purchase Return A/c','1','1','3',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'41','Sales A/c','1','1','4',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'42','Sales Return A/c','1','1','4',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);
            values = "'43','Transfer A/c','1','1','4',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);

            values = "'100012','Sundry Creditors','1','1','1',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);

            values = "'200021','Sundry Debtors','1','1','2',0.0,0.0,'1','SYSTEM'";
            dbfun.insert("groups" + FrmCode, fields, values, con);

            #region Basic Heads
#if false  //if (cPublic.ledgerhead != "Basic Heads")
            {
                //liablity

                values = "'100000','Capital','1','1','1',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'100001','Reserves & Surplus','1','1','1',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'100002','Secured Loans','1','1','1',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'100003','Vehicle Loans','2','2','100002',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'100004','Business Loans','2','2','100002',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'100005','Land & Building Loans ','2','2','100002',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'100006','Unsecured Loans','1','1','1',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'100007','Current Liabilities & Provision ','1','1','1',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'100008','Overdrafts & Short Terms Loans','2','2','100007',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'100009','Salary & Wages Payable','2','2','100007',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'200015','Investments','1','1','2',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                //groups under current assets
                values = "'200016','Loans & Advances','2','2','21',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                //groups under loan & advances
                values = "'200017','Staff Advances','3','3','200016',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                //groups under fixed assets
                values = "'200010','Land & Buildings','2','2','22',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'200011','Vehicles','2','2','22',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'200012','Plant & Machinery','2','2','22',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'200013','Furniture & Fittings','2','2','22',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'200014','Equipment & Tools','2','2','22',0.0,0.0,'1','SYSTEM'";

                //trading expense

                //dbfun.insert("groups" + FrmCode, fields, values, con);
                //values = "'300002','Purchase A/c','1','1','3',0.0,0.0,'1','SYSTEM'";
                //dbfun.insert("groups" + FrmCode, fields, values, con);
                //values = "'300003','Purchase Return A/c','1','1','3',0.0,0.0,'1','SYSTEM'";
                //dbfun.insert("groups" + FrmCode, fields, values, con);
                
                values = "'300004','Eqpmnt Rental expense','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300005','Transportation Expense','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);


                values = "'300015','Fuel Charges','2','2','300005',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300006','Site Support Expense','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300007','Labour Wages & Salary','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300008','Sub Contract Expenses','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'300009','Employee welfare','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'300010','Direct Rental/Hiring Charges','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                
                values = "'300019','Labour Camp Rental','2','2','300010',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300020','Warehousing Rental','2','2','300010',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                
                values = "'300021','Equipment Rental Expense','2','2','300010',0.0,0.0,'1','SYSTEM'";                
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300011','Direct Repairs/Maintainence','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'300022','Transport Vehicle R&M','2','2','300011',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300023','Labour camps R&M','2','2','300011',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300024','Plant & machinery R&M','2','2','300011',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300025','Equipments R&M','2','2','300011',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'300012','Direct Electrcity & Water','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'300013','Direct Insurance Expenses','1','1','3',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'300026','Transport Vehicle Insurance','2','2','300013',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'300027','Employee Insurance','2','2','300013',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'500000','Rental Expenses','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500010','Partners/Directors Rental','2','2','500000',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500011','Executive Staff Rental','2','2','500000',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500012','Office  Rental','2','2','500000',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'50001','Repair & Maintainence','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500014','Office R & M','2','2','50001',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500015','Vehicle R & M','2','2','50001',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'500002','Depreciation','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500003','Administrative Expenses','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500019','Staff Salary Expense','2','2','500003',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500020','Office Expense','2','2','500003',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500021','Travelling Expenses','2','2','500003',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500030','Fuel Expense - Office Vehicles','3','3','500021',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'500004','Electricity & Water Charges','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500032','Partner`s/ Directors E & W','2','2','500004',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500033','Executive Staff E & W','2','2','500004',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500034','Office E & W','2','2','500004',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'500005','Goverment Fees & Charges','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'500035','Labour Department','2','2','500005',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500036','Immigration Department','2','2','500005',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500037','Department Of Economic Development','2','2','500005',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500038','Health Department','2','2','500005',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500039','Muncipality Fee & Charges','2','2','500005',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500040','Road Transport Authority','2','2','500005',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500041','Consular Services','2','2','500005',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'500006','insurance expenses','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500042','Vehicle Insurance','2','2','500006',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'500007','Financial Expenses','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'500044','interest on loans','2','2','500007',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500045','Interest On Loan - Business Loans','3','3','500044',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500046','Interest On Loan - Outside Parties','3','3','500044',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500047','Interest On Loan - Vehicles','3','3','500044',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

                values = "'500008','Telephone Expenses','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);
                values = "'500009','Staff Welfare','1','1','5',0.0,0.0,'1','SYSTEM'";
                dbfun.insert("groups" + FrmCode, fields, values, con);

            }
#endif
            #endregion

            //inserting primary heads and values to accounts table
            fields = "CODE,HEAD,BALANCE,TYPE,LEVEL1,GROUPCODE,GROUPLEVEL,OPBALANCE,CRLIMIT,CRDAYS,INTRATE,ADDRESS,STREET,CITY,STATE,PIN,TIN,CST,PHONE1,PHONE2,FAX_MAIL,REMARKS,LEGAL,USERID,BILL";
            values = "'200001','Cash A/C',0.0,'A','3','200000',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
            values = "'200002','Petty Cash A/C',0.0,'A','3','200000',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
            values = "'200003','Bank Account1',0.0,'A','3','200000',3,0,0.0,0.0,0.0,'','','','','','','','','','','','0','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
            values = "'300001','Freight',0.0,'S','2','31',2,0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
            values = "'300002','Other Expense',0.0,'S','2','31',2,0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
            values = "'600001','Miscellaneous Income',0.0,'I','1','6',1,0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);

            values = "'3901','Opening Stock',0.0,'P','1','3',1,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);

            values = "'4901','Closing Stock',0.0,'S','1','4',1,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);

            values = "'1111','Trading Profit & Loss A/c',0.0,'L','1','1',1,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);


            values = "'502','Round Off',0.0,'E','1','5',1,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
            values = "'603','Forwarding Charge',0.0,'I','1','6',1,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
            dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);

            #region Basic Heads
#if false
            //if (cPublic.ledgerhead != "Basic Heads")
            {
                //added as per shivan on liabality
                values = "'100010','Staff Salary Payable',0.0,'L','3','100009',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'100011','Workers Salary  & Wages Payable',0.0,'L','3','100009',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'100013','Profit & Loss A/c',0.0,'L','3','1',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);

                values = "'200018','Staff Advance- Administratives',0.0,'A','4','200017',4,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'200019','Staff Advance-Site Staff',0.0,'A','4','200017',4,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                // on groups of loans & buldings								
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'200020','Worker Advance',0.0,'A','3','200016',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";

                //added on trading expense
                // on groups of site support expenses
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'300016','Site Pass Expenses',0.0,'P','2','300006',2,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                //
                // on groups of labour wages salary
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'300017','Salary Site Staff',0.0,'P','2','300007',2,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                //
                // on groups of employee management
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'300018','Employee Medical Expenses',0.0,'P','2','300009',2,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                //
                // on groups of transport vehicle insurance
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'300028','Insurance-worker Transport Vehicle',0.0,'P','3','300026',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                //
                // on groups of transport vehicle insurance
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'300029','Insurance--employee Group Policy',0.0,'P','3','300027',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                //
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'400003','Contract Invoice',0.0,'S','1','4',1,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";

                //added on  expense
                // on groups of executive staff rental
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500013','Accomodation Allowance',0.0,'E','3','500011',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                // on groups of office R & M
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500016','R & M Office',0.0,'E','3','500014',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                // on groups of vehicle R & M
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500017','R & M Vehicles General',0.0,'E','3','500015',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500018','Vehicle Service & Oil Changes',0.0,'E','3','500015',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                // on groups of staff salary expense
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500022','Salary- Administrative Staff',0.0,'E','3','500019',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                // on groups of office expense
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500023','Audit Fees & Charges',0.0,'E','3','500020',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500024','Computer Consumpable Expenses',0.0,'E','3','500020',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500025','Miscelaneous Expenses',0.0,'E','3','500020',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500026','Postal Expenses',0.0,'E','3','500020',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500027','Printing Expenses',0.0,'E','3','500020',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500028','Stationary Expenses',0.0,'E','3','500020',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                values = "'500029','Typing Expenses',0.0,'E','3','500020',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                // on groups of travelling expense
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);

                values = "'500031','Travelling Expense',0.0,'E','3','500021',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                // on groups of travelling expense
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);

                values = "'500043','Insurance- Office Vehicles',0.0,'E','3','500042',3,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                // on groups of financial expense
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);

                values = "'500048','Bank Charges',0.0,'E','2','500007',2,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                // on groups of staff welfare
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);

                values = "'500049','Staff Welfare..',0.0,'E','2','500009',2,0.0,0.0,0.0,0.0,'','','','','','','','','','','','1','SYSTEM','N'";
                dbfun.insert("ACCOUNTS" + FrmCode, fields, values, con);
                //added on income
                // on group income
            }

#endif
            #endregion

        }

        public void Function(string FrmCode)
        {

            
        }
    }
}
