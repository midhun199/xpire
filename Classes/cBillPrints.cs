using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace Xpire.Classes
{
    public class cBillPrints
    {
        SqlCommand cmd = new SqlCommand(string.Empty , cPublic.Connection);
        SqlDataReader dr=null ;
        cGeneral cgen = new cGeneral();

        private int i;
        private double prtQty;
        private double prtGv;
        private double prtTaxamt;
        private double prtCessamt;
        private double prtNetamt;
        private double prtNetVlu;
        private double prtDisc;
        private int skiplines;

        private int coun;
        private double slNo;

        private int maxline;
        private int lineno;

        DataTable dataTable = new DataTable();

        public void BillPlains(StreamWriter tstream, string rptmaster1, string rptsub, int  RptOrderno)
        {
            int rptWidth = 136;

            string curr = "";
            slNo = 1;
            skiplines = 0;
            coun = 0;
            prtQty = 0;
            prtGv = 0;
            prtTaxamt = 0;
            prtCessamt = 0;
            prtNetamt = 0;
            prtNetVlu = 0;
            prtDisc = 0;

            string shead = "";
            string custname = "";
            string custcode = "";
            string custad1 = "";
            string custad2 = "";
            string custad3 = "";
            string custph = ""; 
            double cessamt = 0;
            double billdiscamt = 0;
            DateTime date1 = new DateTime();
            string orderno = "";
            string userid = "";
            string custtin = "";
            string custcst = "";
            double roundoff = 0;
            double billamt = 0;
            string remarks1 = "";
            string billtype = "";

            string tincst = "";
            string form = "";

            if (RptOrderno == 0 )
            {
                MessageBox.Show("Bill Number Not Found", cPublic.messagename);
                return;
            }
            switch (Strings.Left(rptmaster1, rptmaster1.Trim().Length - 4).ToUpper())
            {
                case "SALES":
                    shead = "TAX INVOICE";
                    break;
                case "SRETURN":
                    shead = "TAX INVOICE";
                    break;
                case "QTN":
                    shead = "QUOTATION ";
                    break;
                case "SORDER":
                    shead = "SALES ORDER ";
                    break;
                case "TRANSFER":
                    shead = "TRANSFER OUT ";
                    break;
                case "TRANSIN":
                    shead = "TRANSFER IN ";
                    break;
            }

            cmd.Parameters.Clear();
            cmd.CommandText = "select *,a.tin,a.cst,a.phone1,a.ADDRESS, a.STREET, a.CITY from " + rptmaster1 + " m,accounts" + cPublic.g_firmcode + " a where m.custcode=a.code and orderno = " + RptOrderno + "  ";
            cmd.CommandType = CommandType.Text;  
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                custname = dr["custname"].ToString().ToUpper();
                custcode = dr["custcode"].ToString().ToUpper();
                custad1 = dr["ADDRESS"].ToString().ToUpper();
                custad2 = dr["STREET"].ToString().ToUpper();
                custad3 = dr["CITY"].ToString().ToUpper();
                custph = dr["phone1"].ToString().ToUpper();
                custtin = dr["tin"].ToString().ToUpper();

                if (custtin.StartsWith("T") == true) { custtin = "Tin:" + custtin.Substring(2); }
                else if (custtin.StartsWith("P") == true) { custtin = "Pin:" + custtin.Substring(2); }
                else { custtin = "Tin:" + custtin; }

                custcst = dr["cst"].ToString();
                cessamt = Convert.ToDouble(dr["cessamt"].ToString());
                date1 = Convert.ToDateTime(dr["date1"].ToString());
                orderno = dr["orderno"].ToString();
                userid = dr["userid"].ToString();
                roundoff = Convert.ToDouble(dr["roundoff"].ToString());
                billamt = Convert.ToDouble(dr["billamt"].ToString());
                billdiscamt = Convert.ToDouble(dr["bildiscamt"].ToString());
                remarks1 = dr["remarks1"].ToString();
                billtype = dr["billtype"].ToString();
                orderno = dr["orderno"].ToString();
                form = dr["form"].ToString();
            }

            dr.Close();


            cmd.Parameters.Clear();
            cmd.CommandText ="select * from " + rptmaster1 + " m where orderno = " + RptOrderno + "  ";
            cmd.CommandType = CommandType.Text;  
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (custad1.Trim() == string.Empty)
                {
                    custad1 = dr["custname"].ToString();
                }
                cessamt = Convert.ToDouble(dr["cessamt"].ToString());
                date1 = Convert.ToDateTime(dr["date1"].ToString());
                orderno = dr["orderno"].ToString();
                userid = dr["userid"].ToString();
                roundoff = Convert.ToDouble(dr["roundoff"].ToString());
                billamt = Convert.ToDouble(dr["billamt"].ToString());
                billdiscamt = Convert.ToDouble(dr["bildiscamt"].ToString());
                remarks1 = dr["remarks1"].ToString();
                billtype = dr["billtype"].ToString();
                orderno = dr["orderno"].ToString();
                form = dr["form"].ToString();
            }

            dr.Close();

            if (custcode == "CANCEL")
            {
                MessageBox.Show("Bill " + orderno + " Already Cancelled", cPublic.messagename);
            }

            if ((Strings.Left(rptmaster1, rptmaster1.Length - 4).Trim().ToUpper()) != "QTN")
            {
                shead = shead + " CASH / CREDIT";
            }
            else
            {
                shead = "Quotation";
            }

            if (Strings.Left(rptmaster1, rptmaster1.Trim().Length - 4) == "SRETURN")
            {
                if (billtype.ToUpper() == "1")
                {
                    shead = "SALES RETURN-CASH";
                }
                else
                {
                    shead = "SALES RETURN-CREDIT";
                }
            }

            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text; 
            cmd.CommandText = "select *,price*qty as netvalue,(select top 1 unit from stockmst"+cPublic.g_firmcode +" where itemcode=i.itemcode) Unitt from " + rptsub + " i where orderno = " + RptOrderno + " order by i.recno"; 
            SqlDataAdapter dad = new SqlDataAdapter(cmd);
            dataTable.Reset();
            dad.Fill(dataTable);

            string strdata = "";

            if (cPublic.BillSize.ToUpper() == "FULLPAGE")
            {
                if (form == "A")
                {
                    maxline = 37;
                }
                else
                {
                    maxline = 40;
                }
            }
            else
            {
                if (form == "A")
                {
                    maxline = 8;
                }
                else
                {
                    maxline = 11;
                }
            }


            if (dataTable.Rows.Count > 0)
            {
                for (i = 1; i < Convert.ToInt32(cPublic.LinestoReverse); i++)
                {
                    tstream.Write("j$");
                }

                tstream.Write(cPublic.Ch10 + cPublic.ChN);
                tstream.WriteLine(cPublic.ChC);

                    tstream.Write(cPublic.Ch10 + cPublic.ChN + cPublic.BchDW);
                    tstream.Write(cgen.FnAlignStringlarge(cPublic.g_firmname, 80));
                    tstream.WriteLine(cPublic.BchNDW);

                    skiplines = skiplines + 1;
                    tstream.Write(cPublic.ChC);

                    tstream.WriteLine(cgen.AlignWord(cPublic.g_add1.ToUpper() + "," + cPublic.g_add2.ToUpper(), "C", rptWidth));
                    tstream.WriteLine(cgen.AlignWord(cPublic.g_add3.ToUpper() + "," + cPublic.g_add4.ToUpper(), "C", rptWidth));
                    tstream.WriteLine(cgen.AlignWord("Phone No : " + cPublic.g_Phone1 + "," + cPublic.g_Phone2, "C", rptWidth));
                    if (form.Trim() == "A")
                    {
                        if ((Strings.Left(rptmaster1, rptmaster1.Length - 4).Trim().ToUpper()) != "QTN")
                        {
                            tstream.Write(cgen.AlignWord("THE KERALA VALUE ADDED TAX RULES,2005 FORM NO. 8[See Rule 58(10)]", "L", 111));
                        }
                        else
                        {
                            tstream.Write(cgen.AlignWord(" ", "L", 111));
                        }
                        if (cPublic.g_TinNo.StartsWith("T") == true) { tstream.WriteLine(cgen.AlignWord("TIN : " + cPublic.g_TinNo.Substring(2), "R", 25)); }
                        else if (cPublic.g_TinNo.StartsWith("P") == true) { tstream.WriteLine(cgen.AlignWord("PIN : " + cPublic.g_TinNo.Substring(2), "R", 25)); }
                        else { tstream.WriteLine(cgen.AlignWord("TIN : " + cPublic.g_TinNo, "R", 25)); }
                    }
                    else
                    {
                        if ((Strings.Left(rptmaster1, rptmaster1.Length - 4).Trim().ToUpper()) != "QTN")
                        {
                            tstream.Write(cgen.AlignWord("THE KERALA VALUE ADDED TAX RULES,2005 FORM NO. 8" + form .Trim() + "[See Rule 58(10)]", "L", 111));
                        }
                        else
                        {
                            tstream.Write(cgen.AlignWord(" ", "L", 111));
                        }
                        if (cPublic.g_TinNo.StartsWith("T") == true) { tstream.WriteLine(cgen.AlignWord("TIN : " + cPublic.g_TinNo.Substring(2), "R", 25)); }
                        else if (cPublic.g_TinNo.StartsWith("P") == true) { tstream.WriteLine(cgen.AlignWord("PIN : " + cPublic.g_TinNo.Substring(2), "R", 25)); }
                        else { tstream.WriteLine(cgen.AlignWord("TIN : " + cPublic.g_TinNo, "R", 25)); }
                    }

                    tstream.WriteLine(cgen.AlignWord("CST :  " + cPublic.g_CstNo, "R", rptWidth));

                    skiplines = skiplines + 4;

                    tstream.Write(cgen.AlignWord(" ", "L", 58));
                    if ((Strings.Left(rptmaster1, rptmaster1.Length - 4).Trim().ToUpper()) != "QTN")
                    {
                        tstream.WriteLine(cgen.AlignWord(shead, "L", 71));
                    }
                    else
                    {
                        tstream.Write(cPublic.Ch10 + cPublic.ChN + cPublic.BchDW);
                        tstream.WriteLine(cgen.AlignWord(shead, "L", 71));
                        tstream.Write(cPublic.BchNDW);
                        tstream.Write(cPublic.ChC);
                    }
                
                tstream.Write(cgen.AlignWord("To  :" + custad1.ToUpper(), "L", 116));
                tstream.Write(cPublic.ChE);

                    tstream.Write(cgen.AlignWord("Bill No. :", "L", 10));
               
                tstream.Write(cgen.AlignWord(" ", "L", 1));
                strdata = Strings.Format(RptOrderno, "#0");
                tstream.WriteLine(cgen.AlignWord(strdata, "R", 9));
                tstream.Write(cPublic.ChNE);

                tstream.Write(cgen.AlignWord(custad2.ToUpper() + "," + custad3.ToUpper(), "L", 116));
                tstream.WriteLine(cgen.AlignWord("Date     :" + date1.ToString("dd/MM/yyyy"), "R", 20));
                skiplines = skiplines + 2;//   ' "Date:" + FnAlignString(CStr(Format(date1, "dd/MM/yyyy")), "R", 10)

                if (custtin.Trim().Length != 0)
                    //tincst = "TIN No: " + custtin.Trim();
                    tincst = custtin.Trim();
                if (custcst.Trim().Length != 0)
                    tincst = tincst + ",  CST No: " + custcst.Trim();

                    tstream.Write(cgen.AlignWord(tincst, "L", tincst.Length + 3));

                if (custph.Trim().Length > 0)
                    tstream.Write(cgen.AlignWord("Ph:" + custph.Trim(), "L", 35));
                skiplines = skiplines + 1;
                tstream.WriteLine();
                tstream.Write(cgen.AlignWord(" ", "L", 55));
                tstream.Write(cgen.AlignWord(" ", "L", 10));
                tstream.WriteLine(cgen.AlignWord(" ", "L", 55));
                tstream.Write(cPublic.Ch10 + cPublic.ChN);
                tstream.Write(cPublic.ChC);
                tstream.Write(cgen.Replicate("-", rptWidth)); tstream.WriteLine();
              
                    tstream.WriteLine(cgen.AlignWord("|Sl.| Schedule  / Commodity Item    |Code   | Tax%|Unit|Quantity|   Price|GrossValue|Discount| Net Value| Tax.Amt.|Cess.Amt.|     Total|", "L", rptWidth));
               
                tstream.Write(cgen.Replicate("=", rptWidth));
                skiplines = skiplines + 4;
                lineno = lineno + 7;

                    PlainLine(tstream);
               

                    for (int pri = coun; pri < maxline; pri++)
                    {
                        tstream.WriteLine();
                        skiplines = skiplines + 1;
                        tstream.Write("|");
                        tstream.Write(Strings.Space(3));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(31));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(7));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(5));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(4));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(8));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(8));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(10));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(8));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(10));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(9));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(9));
                        tstream.Write("|");
                        tstream.Write(Strings.Space(10));
                        tstream.Write("|");
                    }

                tstream.WriteLine();
                skiplines = skiplines + 1;
                tstream.WriteLine(cgen.Replicate("-", rptWidth));
                skiplines = skiplines + 1;
                //'*******************totaling
                tstream.Write("|");
                tstream.Write(Strings.Space(3));
                tstream.Write("|");
                //'sch
             
                    tstream.Write(Strings.Space(22));
                
                tstream.Write("TOTAL");


                tstream.Write(Strings.Space(4));

                tstream.Write("|");
                //code

             
                    tstream.Write(Strings.Space(7));
               
                tstream.Write("|");
                //taxper


                    tstream.Write(Strings.Space(5));
                    tstream.Write("|");
                //unit

                tstream.Write(Strings.Space(4));

                tstream.Write("|");
                //quantity

              
                strdata = Strings.Format(prtQty, "#0.000");
                tstream.Write(cgen.AlignWord(strdata, "R", 8));
                tstream.Write("|");


                tstream.Write(Strings.Space(8));
               
                //price
               
                    tstream.Write("|");

                //grossvalue
               
                    strdata = Strings.Format(prtGv, "#0.00");
                    tstream.Write(cgen.AlignWord(strdata, "R", 10));
                    tstream.Write("|");
                    //disc
                    strdata = Strings.Format(prtDisc, "#0.00");
                    tstream.Write(cgen.AlignWord(strdata, "R", 8));
                    tstream.Write("|");
                    //netvalue
                    strdata = Strings.Format(prtNetVlu, "#0.00");
                    tstream.Write(cgen.AlignWord(strdata, "R", 10));
                    tstream.Write("|");
                    // taxamt
                    strdata = Strings.Format(prtTaxamt, "#0.00");
                    tstream.Write(cgen.AlignWord(strdata, "R", 9));
                    tstream.Write("|");
                    // cessamt
                    strdata = Strings.Format(prtCessamt, "#0.00");
                    tstream.Write(cgen.AlignWord(strdata, "R", 9));
                    tstream.Write("|");
               
                // Total
                strdata = Strings.Format(prtNetamt, "#0.00");
                tstream.Write(cgen.AlignWord(strdata, "R", 10));
                tstream.Write("|");
                // '**********************
                tstream.WriteLine();
                tstream.WriteLine(cgen.Replicate("=", rptWidth));
                skiplines = skiplines + 2;

                if (dataTable.Rows.Count == 0)
                {
                    goto g;
                }
                skiplines = skiplines + 1;

                //'tstream.WriteLine (" " + chr(12))
                skiplines = skiplines + 1;
                skiplines = 0;

            }


        g:
                      
           
            if (roundoff != 0)
            {
                tstream.Write(Strings.Space(101));
                tstream.Write("Round off      : ");
                strdata = Strings.Format(roundoff, "#0.00");
                tstream.WriteLine(cgen.AlignWord(strdata, "R", 17));
            }
          


            tstream.Write(Strings.Space(90));
            tstream.Write(cPublic.ChE);
            tstream.Write(cPublic.Ch10 + cPublic.ChN);
            tstream.Write("** Grand Total : ");
            strdata = Strings.Format(billamt, "#0.00");
            tstream.Write(cgen.AlignWord(strdata, "R", 10));
            tstream.WriteLine(cPublic.ChNE);
            skiplines = skiplines + 1;

            tstream.Write(cPublic.ChC);

            fssgen.fssgen gen = new fssgen.fssgen();
            double lng = Convert.ToInt32(billamt);
            curr = gen.NumToChars(ref lng);
            curr = curr + "".Trim();

            tstream.WriteLine("Amount In Words : Rs. " + curr + " Only");
            skiplines = skiplines + 1;

            // 'declaration
            if (form.Trim() == "A" )
            {
                if ((Strings.Left(rptmaster1, rptmaster1.Length - 4).Trim().ToUpper()) != "QTN")
                {
                    tstream.WriteLine(cgen.Replicate("=", rptWidth));
                    tstream.Write(cPublic.BchDW); tstream.Write(cgen.AlignWord("DECLARATION", "L", 12)); tstream.WriteLine(cPublic.BchNDW);
                    tstream.WriteLine("Certified that all the particulars shown in the above Tax Invoice are true and correct in all respects and the  goods on which the tax");
                    tstream.WriteLine("charged and collected are in accordance with the provisions of the KVAT Act 2003 and the rules made thereunder.  It is also  certified");
                    tstream.WriteLine("that my/our Registration under KVAT Act 2003 is not subject to any suspension/cancellation and it is valid as on the date of this bill");
                    skiplines = skiplines + 4;
                }
            }


            // 'end of declaration
            tstream.WriteLine(cgen.Replicate("=", rptWidth));
            skiplines = skiplines + 1;

            tstream.Write(cPublic.ChN + cPublic.ChE);

           
                tstream.WriteLine(cgen.AlignWord("For " + cPublic.g_firmname + "   ", "R", rptWidth / 2 + 15));
            

            tstream.Write(cPublic.ChN + cPublic.ChNE);

            skiplines = skiplines + 1;

            tstream.WriteLine();
            if (roundoff == 0)
            { tstream.WriteLine(); }


            if (cPublic.BillSize.Trim().ToUpper() == "FULLPAGE")
            {                    
                tstream.WriteLine(Convert.ToChar(12));                
            }
            else
            {
                for (int i = 1; i < Convert.ToInt32(10); i++)
                {
                    tstream.WriteLine();
                }
            }
        }

        public void PlainLine(StreamWriter tstream)
        {
            coun = 1;
            string itname = "";
            string schedl = "";
            string strdata = "";

            foreach (DataRow dataRow in dataTable.Rows)
            {
                tstream.WriteLine();
                skiplines = skiplines + 1;
                tstream.Write("|");
                tstream.Write(cgen.AlignWord(Convert.ToString(slNo), "R", 3));
                slNo = slNo + 1;
                tstream.Write("|");

                cmd.Parameters.Clear();
                cmd.CommandText = "select itemname ,schedule from stockmst" + cPublic.g_firmcode + " where itemcode='" + dataRow["Itemcode"].ToString() + "'";
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    if (dataRow["itemcode"].ToString().ToUpper().Contains("SL") == false)
                    { itname = dr["itemname"].ToString(); }
                    else { itname = dataRow["itemname"].ToString(); }
                    schedl = dr["schedule"].ToString();
                }

                dr.Close();

                //sch
                //tstream.Write(cgen.FnAlignString(schedl + " ", "L", 4));
                //tstream.Write("|");

                //itemname
                tstream.Write(cgen.AlignWord(itname + " ", "L", 31));
                tstream.Write("|");

                //code
                strdata = Strings.Format(dataRow["qty"], "#0.000");
                tstream.Write(cgen.AlignWord(" ", "L", 7));
                tstream.Write("|");

                strdata = Strings.Format(dataRow["taxper"], "#0.00");
                tstream.Write(cgen.AlignWord(strdata, "R", 5));
                tstream.Write("|");

                //unit
                tstream.Write(cgen.AlignWord(dataRow["unitt"].ToString(), "L", 4));
                tstream.Write("|");

                //quantity
                strdata = Strings.Format(Convert.ToDouble(dataRow["QTY"]), "#0.000");
                tstream.Write(cgen.AlignWord(strdata, "R", 8));
                tstream.Write("|");

                strdata = Strings.Format(Convert.ToDouble(dataRow["Price"]) + Convert.ToDouble(dataRow["DISCAMT"]) / Convert.ToDouble(dataRow["QTY"]), "#0.00");
                tstream.Write(cgen.AlignWord(strdata, "R", 8));
                tstream.Write("|");

                //GrossValue
                strdata = Strings.Format(Convert.ToDouble(dataRow["netvalue"]) + Convert.ToDouble(dataRow["DISCAMT"]), "#0.00");
                tstream.Write(cgen.AlignWord(strdata, "R", 10));
                tstream.Write("|");

                //discamt
                strdata = Strings.Format(dataRow["discamt"], "#0.00");
                tstream.Write(cgen.AlignWord(strdata, "R", 8));
                tstream.Write("|");

                //NetValue
                strdata = Strings.Format(Convert.ToDouble(dataRow["netvalue"]), "#0.00");
                tstream.Write(cgen.AlignWord(strdata, "R", 10));
                tstream.Write("|");

                strdata = Strings.Format(dataRow["taxamt"], "#0.00");
                tstream.Write(cgen.AlignWord(strdata, "R", 9));
                tstream.Write("|");

                strdata = Strings.Format(dataRow["cessamt"], "#0.00");
                tstream.Write(cgen.AlignWord(strdata, "R", 9));
                tstream.Write("|");

                //Total
                strdata = Strings.Format(dataRow["NetAmt"], "#0.00");
                tstream.Write(cgen.AlignWord(strdata, "R", 10));
                tstream.Write("|");

                prtQty = prtQty + Convert.ToDouble(dataRow["qty"]);
                prtNetVlu = prtNetVlu + Convert.ToDouble(dataRow["netvalue"]);
                prtGv = prtGv + Convert.ToDouble(dataRow["netvalue"]) + Convert.ToDouble(dataRow["DISCAMT"]);

                prtTaxamt = prtTaxamt + Convert.ToDouble(dataRow["taxamt"]);
                prtCessamt = prtCessamt + Convert.ToDouble(dataRow["cessamt"]);
                prtNetamt = prtNetamt + Convert.ToDouble(dataRow["NetAmt"]);
                prtDisc = prtDisc + Convert.ToDouble(dataRow["discamt"]);

                coun = coun + 1;
                //' If rsprintsub.EOF = True Then
                if (dataTable.Rows.Count == 0)
                {
                    // goto r;
                }
                //' End While
                if (coun > maxline)
                {
                    //  goto r;
                }
            }
            // r:
        }


        public void PBill(int vOrderNo,string vMaster, string vSub, StreamWriter tstream, string vPrintType)
        {

            for (i = 1; i < Convert.ToInt32(cPublic.LinestoReverse); i++)
            {
                tstream.Write("j$");
            }
            rPagehead(tstream, vOrderNo, vMaster, vSub, vPrintType);

            for (int i = 1; i < Convert.ToInt32(cPublic.LinestoSkip ); i++)
            {
                tstream.WriteLine();
            }
        }


        private void rPagehead(StreamWriter tstream, int Orderno,string Master, string Sub, string vPrintType)
        {

            // PageNo = 1;

            string sql = "SELECT * ,isnull((select balance from Accounts" + Master.Substring(Master.Length - 4) + " where code=a.custcode),0) balance FROM " + Master + " a Where ORDERNO=@ORDERNO ";


            cmd.Parameters.Clear();
            cmd.CommandText = sql; 
                cmd.CommandType = CommandType.Text;  

            cmd.Parameters.AddWithValue("@ORDERNO", Orderno);
            SqlDataAdapter ap = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            ap.Fill(dt);

            if (dt.Rows.Count == 0)
            { return; }

            if (vPrintType.ToUpper() == "PLAIN")
            {
                tstream.Write(cPublic.Ch10 + cPublic.ChN + cPublic.BchDW);
                tstream.Write(cgen.FnAlignStringlarge(cPublic.g_firmname, 66));
                tstream.WriteLine(cPublic.BchDW);
                tstream.Write(cPublic.Ch10 + cPublic.ChC);
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add1 + " " + cPublic.g_add2, "C", 66));
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add3 + " " + cPublic.g_add4, "C", 66));
            }
            else if (vPrintType.ToUpper() == "PRINTED")
            {
                tstream.WriteLine();
                tstream.WriteLine();
            }
            else if (vPrintType.ToUpper() == "PACKING SLIP")
            {
                tstream.Write(cPublic.Ch10 + cPublic.Ch12 + cPublic.BchDW);
              
                    tstream.Write(cgen.FnAlignStringlarge(cPublic.g_firmname, 66));
                    tstream.WriteLine(cPublic.BchNDW);
                    tstream.Write(cPublic.Ch12 + cPublic.ChN);
                    tstream.WriteLine(cgen.FnAlignString(cPublic.g_add1 + " " + cPublic.g_add2, "C", 66));
                    tstream.WriteLine(cgen.FnAlignString(cPublic.g_add3 + " " + cPublic.g_add4, "C", 66));
            }

            if (vPrintType.ToUpper() == "PLAIN")
            {
                tstream.Write(cgen.FnAlignString("No : " + Orderno, "L", 15));
                tstream.WriteLine(cgen.FnAlignString("Date.:" + Convert.ToDateTime(dt.Rows[0]["date1"] + "").ToString("dd/MM/yyyy"), "R", 55 - 4));

                tstream.Write("To: ");
                tstream.Write(cgen.FnAlignString(dt.Rows[0]["CUSTNAME"] + "" + "(" + dt.Rows[0]["CUSTCODE"] + "" + ")", "L", 46));

                tstream.WriteLine(cgen.FnAlignString("Time.:" + System.DateTime.Now.ToString("hh:mm:ss"), "L", 16));

                tstream.Write(cgen.Replicate("-", 66));
                tstream.WriteLine(cPublic.ChC);

                tstream.WriteLine(" Sl Item Name                             Qty     Rate      Amount");
                tstream.Write(cgen.Replicate("=", 66));
            }
            else if (vPrintType.ToUpper() == "PRINTED")
            {
                tstream.Write(cgen.FnAlignString("     " +  Orderno, "L", 15));
                tstream.WriteLine(cgen.FnAlignString("      " + Convert.ToDateTime(dt.Rows[0]["date1"] + "").ToString("dd/MM/yyyy"), "R", 55 - 4));

                tstream.Write("    ");
                tstream.Write(cgen.FnAlignString(dt.Rows[0]["CUSTNAME"] + "" + "(" + dt.Rows[0]["CUSTCODE"] + "" + ")", "L", 46));

                tstream.WriteLine(cgen.FnAlignString("      " + System.DateTime.Now.ToString("hh:mm:ss"), "L", 16));

                tstream.WriteLine(cPublic.ChC);

                tstream.WriteLine();
            }
            else if (vPrintType.ToUpper() == "PACKING SLIP")
            {
                tstream.Write(cgen.FnAlignString("No : " +  Orderno, "L", 15));
                tstream.WriteLine(cgen.FnAlignString("Date.:" + Convert.ToDateTime(dt.Rows[0]["date1"] + "").ToString("dd/MM/yyyy"), "R", 55 - 4));

                tstream.Write("To: ");
                tstream.Write(cgen.FnAlignString(dt.Rows[0]["CUSTNAME"] + "", "L", 46));

                tstream.WriteLine(cgen.FnAlignString("Time.:" + System.DateTime.Now.ToString("hh:mm:ss"), "L", 16));

                tstream.WriteLine(cgen.Replicate("-", 66));
                tstream.WriteLine("     Rate Particulars                        Qty Unit       Amount");
                tstream.Write(cgen.Replicate("=", 66));
            }
            else
            {

            }
            Double disc = Convert.ToDouble(dt.Rows[0]["bildiscamt"]);
            String custcode = dt.Rows[0]["custcode"] + "";
            Double taxamt = Convert.ToDouble(dt.Rows[0]["taxamt"]);
            Double cessamt = Convert.ToDouble(dt.Rows[0]["cessamt"]);
            String uId = dt.Rows[0]["userid"] + "";
            Double rndoff = Convert.ToDouble(dt.Rows[0]["roundoff"]);
            String Remarks = dt.Rows[0]["remarks1"] + "";
            Double BillAmt = Convert.ToDouble(dt.Rows[0]["billamt"]);
            Double oldBal = Convert.ToDouble(dt.Rows[0]["balance"]);
            string billtype = dt.Rows[0]["billtype"] + "";


            cmd.Parameters.Clear();
            cmd.CommandText ="SELECT itemcode, itemname,sum(qty) as qty,sum(rate) as rate,sum(netamt) as netamt,(select unit from stockmst"+cPublic.g_firmcode +" where itemcode=a.itemcode ) unit FROM " + Sub + " A  where orderno=" + Orderno + " group by itemcode,ITEMNAME,recno order by recno";
            cmd.CommandType = CommandType.Text;  
            ap = new SqlDataAdapter(cmd);
            dt = new DataTable();
            ap.Fill(dt);
            sliplines(tstream, dt);
            tstream.WriteLine();
            if (vPrintType.ToUpper() == "PLAIN")
            {
                tstream.WriteLine(cgen.Replicate("-", 66));
                tstream.Write(Strings.Space(46));
                tstream.Write("Total    :");
                String strdata = Strings.Format(prtNetamt, "#0.00") + "";
                tstream.WriteLine(cgen.FnAlignString(strdata, "R", 10));

                tstream.Write("Round Off   :");
                strdata = Strings.Format(rndoff, "#0.00");
                tstream.WriteLine(cgen.FnAlignString(strdata, "R", 13));

                strdata = Strings.Format(taxamt, "#0.00");
                tstream.Write("Tax Amt     :");
                tstream.WriteLine(cgen.FnAlignString(strdata, "R", 13));

                tstream.Write(cPublic.ChE);
                tstream.Write("Grand Total :");
                strdata = Strings.Format(BillAmt, "#0.00");
                tstream.Write(cPublic.BchDW);
                tstream.Write(cgen.FnAlignString(strdata, "R", 13));
                tstream.Write(cPublic.BchNDW);

                tstream.Write(Strings.Left(uId, 6));
                tstream.Write(Strings.Right(uId, 2));
                tstream.WriteLine(cgen.Replicate("=", 58));
            }
            else if (vPrintType.ToUpper() == "PACKING SLIP")
            {
                tstream.WriteLine("                                                ------------------");
                tstream.Write(Strings.Space(40));
                tstream.Write("     Amt    :");
                String strdata = Strings.Format(prtNetamt, "#0.00") + "";
                tstream.WriteLine(cgen.FnAlignString(strdata, "R", 13));

               
                    tstream.Write("Round Off   :");
                    strdata = Strings.Format(rndoff, "#0.00");
                    tstream.Write(cgen.FnAlignString(strdata, "R", 13));

                    tstream.Write("  ");
                    strdata = Strings.Format(taxamt, "#0.00");
                    tstream.Write("Tax Amt     :");
                    tstream.WriteLine(cgen.FnAlignString(strdata, "R", 13));

                    strdata = Strings.Format(cessamt, "#0.00");
                    tstream.Write("Cess Amt    :");
                    tstream.Write(cgen.FnAlignString(strdata, "R", 13));

                    tstream.Write("  ");
                    tstream.Write("Grand Total :");
                    strdata = Strings.Format(BillAmt, "#0.00");
                    tstream.WriteLine(cgen.FnAlignString(strdata, "R", 13));
                
                tstream.WriteLine(cgen.Replicate("-", 66));
                tstream.Write("       ");
                strdata = " ";
                tstream.Write(cgen.FnAlignString(strdata, "R", 10));

                tstream.Write("        Disc  :");

                strdata = Strings.Format(disc + rndoff, "#0.00");
                tstream.Write(cgen.FnAlignString(strdata, "R", 10));

                tstream.Write(cPublic.ChE);
                tstream.Write("   Net Amt :");
                strdata = Strings.Format(BillAmt, "#0.00");

                tstream.WriteLine(cgen.FnAlignString(strdata, "R", 12));
                tstream.Write(cPublic.ChNE);

                tstream.Write(Strings.Left(uId, 6));
                tstream.Write(Strings.Right(uId, 2));
                tstream.WriteLine(cgen.Replicate("=", 58));               
            }
            else if (vPrintType.ToUpper() == "PRINTED")
            {
                tstream.WriteLine();
                tstream.Write(Strings.Space(46));
                tstream.Write("          ");
                String strdata = Strings.Format(prtNetamt, "#0.00") + "";
                tstream.WriteLine(cgen.FnAlignString(strdata, "R", 10));

                tstream.Write("             ");
                strdata = Strings.Format(rndoff, "#0.00");
                tstream.WriteLine(cgen.FnAlignString(strdata, "R", 13));

                strdata = Strings.Format(taxamt, "#0.00");
                tstream.Write("             ");
                tstream.WriteLine(cgen.FnAlignString(strdata, "R", 13));

                tstream.Write(cPublic.ChE);
                tstream.Write("             ");
                strdata = Strings.Format(BillAmt, "#0.00");
                tstream.Write(cPublic.BchDW);
                tstream.Write(cgen.FnAlignString(strdata, "R", 13));
                tstream.Write(cPublic.BchNDW);
                tstream.WriteLine(cPublic.ChNE);
                tstream.Write(cPublic.ChC);
            }
        }

        private void sliplines(StreamWriter tstream, DataTable dt)
        {
            prtNetamt = 0;
            for (Int32 incr = 0; incr < dt.Rows.Count; incr++)
            {
                String itName = dt.Rows[incr]["itemname"] + "";
                String strdata = "";

                tstream.WriteLine();

                strdata = Strings.Format(Convert.ToDouble(dt.Rows[incr]["RATE"]), "#0.00");
                tstream.Write(cgen.FnAlignString(strdata, "R", 9));
                tstream.Write(" ");

                tstream.Write(cgen.FnAlignString(itName, "L", 28));
                tstream.Write(" ");


                strdata = Strings.Format(Convert.ToDouble(dt.Rows[incr]["QTY"]), "#0.000");
                tstream.Write(cgen.FnAlignString(strdata, "R", 9));
                tstream.Write(" ");

                strdata = dt.Rows[incr]["unit"].ToString();
                tstream.Write(cgen.FnAlignString(strdata, "L", 3));
                tstream.Write(" ");
                strdata = (Strings.Format(0, "#0") == "0") ? " " : (Strings.Format(0, "#0"));
                tstream.Write(cgen.FnAlignString(strdata, "R", 3));

                strdata = Strings.Format(Convert.ToDouble(dt.Rows[incr]["NETAMT"]), "#0.00");
                tstream.Write(cgen.FnAlignString(strdata, "R", 10));
                prtNetamt = prtNetamt + Convert.ToDouble(dt.Rows[incr]["NETAMT"]);
            }
        }


    }
}
