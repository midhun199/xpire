﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Xpire.BL;

namespace Xpire.DL
{
    class ClsCustDl
    {
        ClsConnect objCon = new ClsConnect();
        internal void save(ClsCustomerBL objCustomer)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("Pro_Customer", objCon.Connection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Prm_Flag", 'N');
            cmd.Parameters.AddWithValue("@Prm_Id", 0);
            cmd.Parameters.AddWithValue("@Prm_Name", objCustomer.Name);
            cmd.Parameters.AddWithValue("@Prm_Address", objCustomer.Address);
            cmd.Parameters.AddWithValue("@Prm_Phone", objCustomer.Phone);
            cmd.Parameters.AddWithValue("@Prm_Mobile", objCustomer.Mobile);
            cmd.Parameters.AddWithValue("@Prm_EmailId", objCustomer.EmailId);
            cmd.Parameters.AddWithValue("@Prm_Active", objCustomer.Active);
            cmd.Parameters.AddWithValue("@Prm_IsDelete", null);


            cmd.ExecuteNonQuery();
            objCon.Connection().Close();

        }

        internal BL.ClsCustomer Select()
        {
            ClsCustomer objCol = new ClsCustomer();
            SqlCommand cmd = null;
            cmd = new SqlCommand("Pro_Customer", objCon.Connection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Prm_Flag", 'S');
            cmd.Parameters.AddWithValue("@Prm_Id", 0);
            cmd.Parameters.AddWithValue("@Prm_Name", null);
            cmd.Parameters.AddWithValue("@Prm_Address",null );
            cmd.Parameters.AddWithValue("@Prm_Phone",null);
            cmd.Parameters.AddWithValue("@Prm_Mobile",null );
            cmd.Parameters.AddWithValue("@Prm_EmailId",null );
            cmd.Parameters.AddWithValue("@Prm_Active", null);
            cmd.Parameters.AddWithValue("@Prm_IsDelete", null);
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                ClsCustomerBL objbl = new ClsCustomerBL();
                objbl.Id = Convert.ToInt32(sdr.GetDecimal(0));
                objbl.Name = sdr.GetString(1);
                objbl.Address = sdr.GetString(2);
                objbl.Phone = sdr.GetString(3);
                objbl.Mobile = sdr.GetString(4);
                objbl.EmailId = sdr.GetString(5);
                objbl.Active = sdr.GetBoolean(6);
                objCol.Add(objbl);
            }
            return objCol;
        }

        internal void Update(ClsCustomerBL objCustomer)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("Pro_Customer", objCon.Connection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Prm_Flag", 'M');
            cmd.Parameters.AddWithValue("@Prm_Id", objCustomer.Id);
            cmd.Parameters.AddWithValue("@Prm_Name", objCustomer.Name);
            cmd.Parameters.AddWithValue("@Prm_Address", objCustomer.Address);
            cmd.Parameters.AddWithValue("@Prm_Phone", objCustomer.Phone);
            cmd.Parameters.AddWithValue("@Prm_Mobile", objCustomer.Mobile);
            cmd.Parameters.AddWithValue("@Prm_EmailId", objCustomer.EmailId);
            cmd.Parameters.AddWithValue("@Prm_Active", objCustomer.Active);
            cmd.Parameters.AddWithValue("@Prm_IsDelete", null);


            cmd.ExecuteNonQuery();
            objCon.Connection().Close();
        }

        internal void Delete(ClsCustomerBL objCustomer)
        {
            SqlCommand cmd = null;
            cmd = new SqlCommand("Pro_Customer", objCon.Connection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Prm_Flag", 'D');
            cmd.Parameters.AddWithValue("@Prm_Id", objCustomer.Id);
            cmd.Parameters.AddWithValue("@Prm_Name", null);
            cmd.Parameters.AddWithValue("@Prm_Address", null);
            cmd.Parameters.AddWithValue("@Prm_Phone", null);
            cmd.Parameters.AddWithValue("@Prm_Mobile", null);
            cmd.Parameters.AddWithValue("@Prm_EmailId", null);
            cmd.Parameters.AddWithValue("@Prm_Active", null);
            cmd.Parameters.AddWithValue("@Prm_IsDelete", null);


            cmd.ExecuteNonQuery();
            objCon.Connection().Close();
        }
    }
}
