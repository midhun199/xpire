namespace Xpire.Reports
{
    partial class ucSalesBillWise
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucSalesBillWise));
            this.btnClose = new System.Windows.Forms.Button();
            this.DtpTo = new System.Windows.Forms.DateTimePicker();
            this.DtpFrom = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Chkmonthwise = new System.Windows.Forms.RadioButton();
            this.chkDaywise = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.cmbBillSrs = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbpricetype = new System.Windows.Forms.ComboBox();
            this.cmbBilltype = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chksummary = new System.Windows.Forms.CheckBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.flxStockPrv = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flxStockPrv)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnClose.Location = new System.Drawing.Point(723, 570);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // DtpTo
            // 
            this.DtpTo.CustomFormat = "dd/MM/yyyy";
            this.DtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpTo.Location = new System.Drawing.Point(276, 12);
            this.DtpTo.Name = "DtpTo";
            this.DtpTo.Size = new System.Drawing.Size(144, 20);
            this.DtpTo.TabIndex = 1;
            // 
            // DtpFrom
            // 
            this.DtpFrom.CustomFormat = "dd/MM/yyyy";
            this.DtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrom.Location = new System.Drawing.Point(68, 12);
            this.DtpFrom.Name = "DtpFrom";
            this.DtpFrom.Size = new System.Drawing.Size(144, 20);
            this.DtpFrom.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(228, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "To";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "From";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Chkmonthwise);
            this.groupBox1.Controls.Add(this.chkDaywise);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(29, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(393, 44);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bills";
            // 
            // Chkmonthwise
            // 
            this.Chkmonthwise.AutoSize = true;
            this.Chkmonthwise.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.Chkmonthwise.Location = new System.Drawing.Point(274, 19);
            this.Chkmonthwise.Name = "Chkmonthwise";
            this.Chkmonthwise.Size = new System.Drawing.Size(100, 19);
            this.Chkmonthwise.TabIndex = 2;
            this.Chkmonthwise.Text = "Month Wise";
            this.Chkmonthwise.UseVisualStyleBackColor = true;
            // 
            // chkDaywise
            // 
            this.chkDaywise.AutoSize = true;
            this.chkDaywise.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.chkDaywise.Location = new System.Drawing.Point(149, 19);
            this.chkDaywise.Name = "chkDaywise";
            this.chkDaywise.Size = new System.Drawing.Size(84, 19);
            this.chkDaywise.TabIndex = 1;
            this.chkDaywise.Text = "Day Wise";
            this.chkDaywise.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.radioButton1.Location = new System.Drawing.Point(17, 21);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(81, 19);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Bill Wise";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // cmbBillSrs
            // 
            this.cmbBillSrs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBillSrs.FormattingEnabled = true;
            this.cmbBillSrs.Items.AddRange(new object[] {
            "All",
            "A",
            "B"});
            this.cmbBillSrs.Location = new System.Drawing.Point(472, 11);
            this.cmbBillSrs.Name = "cmbBillSrs";
            this.cmbBillSrs.Size = new System.Drawing.Size(73, 21);
            this.cmbBillSrs.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(581, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 15);
            this.label5.TabIndex = 13;
            this.label5.Text = "Price Type";
            // 
            // cmbpricetype
            // 
            this.cmbpricetype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbpricetype.FormattingEnabled = true;
            this.cmbpricetype.Items.AddRange(new object[] {
            "All",
            "Retail Price",
            "Wholesale Price",
            "Special Price"});
            this.cmbpricetype.Location = new System.Drawing.Point(667, 39);
            this.cmbpricetype.Name = "cmbpricetype";
            this.cmbpricetype.Size = new System.Drawing.Size(120, 21);
            this.cmbpricetype.TabIndex = 4;
            // 
            // cmbBilltype
            // 
            this.cmbBilltype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBilltype.FormattingEnabled = true;
            this.cmbBilltype.Items.AddRange(new object[] {
            "All",
            "Cash",
            "Credit"});
            this.cmbBilltype.Location = new System.Drawing.Point(667, 12);
            this.cmbBilltype.Name = "cmbBilltype";
            this.cmbBilltype.Size = new System.Drawing.Size(120, 21);
            this.cmbBilltype.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(582, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 15);
            this.label3.TabIndex = 12;
            this.label3.Text = "Bill Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(426, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 15);
            this.label4.TabIndex = 11;
            this.label4.Text = "Form";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(100, 101);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(320, 20);
            this.txtCustomer.TabIndex = 6;
            this.txtCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomer_KeyDown);
            this.txtCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCustomer_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(26, 102);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 15);
            this.label8.TabIndex = 19;
            this.label8.Text = "Customer";
            // 
            // chksummary
            // 
            this.chksummary.AutoSize = true;
            this.chksummary.Location = new System.Drawing.Point(429, 61);
            this.chksummary.Name = "chksummary";
            this.chksummary.Size = new System.Drawing.Size(69, 17);
            this.chksummary.TabIndex = 7;
            this.chksummary.Text = "&Summary";
            this.chksummary.UseVisualStyleBackColor = true;
            // 
            // btnSelect
            // 
            this.btnSelect.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnSelect.Location = new System.Drawing.Point(472, 96);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(89, 26);
            this.btnSelect.TabIndex = 8;
            this.btnSelect.Text = "&Load";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // flxStockPrv
            // 
            this.flxStockPrv.AllowEditing = false;
            this.flxStockPrv.AllowFreezing = C1.Win.C1FlexGrid.AllowFreezingEnum.Both;
            this.flxStockPrv.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.flxStockPrv.BackColor = System.Drawing.SystemColors.Window;
            this.flxStockPrv.ColumnInfo = resources.GetString("flxStockPrv.ColumnInfo");
            this.flxStockPrv.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.flxStockPrv.Font = new System.Drawing.Font("Verdana", 9F);
            this.flxStockPrv.Location = new System.Drawing.Point(3, 128);
            this.flxStockPrv.Name = "flxStockPrv";
            this.flxStockPrv.Rows.MinSize = 22;
            this.flxStockPrv.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.flxStockPrv.Size = new System.Drawing.Size(795, 436);
            this.flxStockPrv.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("flxStockPrv.Styles"));
            this.flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
            this.flxStockPrv.TabIndex = 20;
            this.flxStockPrv.TabStop = false;
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPrint.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPrint.Location = new System.Drawing.Point(409, 570);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 32);
            this.btnPrint.TabIndex = 23;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.White;
            this.btnView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnView.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnView.Location = new System.Drawing.Point(490, 570);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(88, 32);
            this.btnView.TabIndex = 22;
            this.btnView.Text = "&View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.White;
            this.btnExport.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnExport.Location = new System.Drawing.Point(584, 570);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(132, 32);
            this.btnExport.TabIndex = 21;
            this.btnExport.Text = "&Export - Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // ucSalesBillWise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.flxStockPrv);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.chksummary);
            this.Controls.Add(this.txtCustomer);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmbBillSrs);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbpricetype);
            this.Controls.Add(this.cmbBilltype);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DtpTo);
            this.Controls.Add(this.DtpFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Name = "ucSalesBillWise";
            this.Size = new System.Drawing.Size(807, 606);
            this.Load += new System.EventHandler(this.ucSalesBillWise_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flxStockPrv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.DateTimePicker DtpTo;
        public System.Windows.Forms.DateTimePicker DtpFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton Chkmonthwise;
        private System.Windows.Forms.RadioButton chkDaywise;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.ComboBox cmbBillSrs;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbpricetype;
        private System.Windows.Forms.ComboBox cmbBilltype;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtCustomer;
        public System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chksummary;
        private System.Windows.Forms.Button btnSelect;
        public C1.Win.C1FlexGrid.C1FlexGrid flxStockPrv;
        private System.Windows.Forms.Button btnPrint;
        public System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnExport;
    }
}
