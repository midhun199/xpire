using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using System.IO;
using C1.Win.C1FlexGrid;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Reports
{
    public partial class ucStockDetails : UserControl
    {
        public ucStockDetails()
        {
            InitializeComponent();
        }
        string tablename = string.Empty ;
        string fields = string.Empty;
        string displayname = string.Empty;
        string widthfld = string.Empty;
        string tblcondition = string.Empty;
        string caption = string.Empty;
        double gewidth = 0;

        cGeneral cgen = new cGeneral();
        fssgen.fssgen gen = new fssgen.fssgen();
        SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);

        int leng = 0;
        PointF foi = new PointF(100, 50);
        int le, lee, ii, jj;
        Boolean lin = false;
        StreamWriter tstream;
        int RptWidth;
        int lineno;
        long PageNo;
        bool line = false, day = false;
        int G_Lperpage = 69;
        decimal[] colSum = new decimal[100];

 
        private void loadLookup(TextBox txtboxname, EventArgs e)
        {
            frmlookup lookup = new frmlookup();
            if ((e.GetType().ToString() == typeof(KeyEventArgs).ToString()))
            {
                lookup.m_strat = "";
            }
            else
            {
                lookup.m_strat = ((KeyPressEventArgs)e).KeyChar.ToString();
                ((KeyPressEventArgs)e).KeyChar = Convert.ToChar(Keys.None);
            }
            lookup.m_con = cPublic.Connection;
            lookup.m_table = tablename;
            lookup.m_fields = fields;
            lookup.m_caption = caption;
            if (displayname + "" == string.Empty) { lookup.m_dispname = fields; }
            else { lookup.m_dispname = displayname; }
            lookup.m_fldwidth = widthfld;
            lookup.m_condition = tblcondition;
            lookup.ShowDialog();
            txtboxname.ForeColor = Color.Black;
            if (lookup.m_values.Count > 0)
            {
                if (txtboxname.Name.Trim().ToUpper() == "TXTITEMCODE")
                {
                    txtboxname.Text = lookup.m_values[1].ToString();
                    txtboxname.Tag = lookup.m_values[0].ToString();
                }
                else
                {
                    txtboxname.Text = lookup.m_values[0].ToString();
                    txtboxname.Tag = lookup.m_values[1].ToString();
                }
                SendKeys.Send("{Tab}");
            }
            else
            {
                txtboxname.ForeColor = Color.Black;
                txtboxname.Text = "";
                txtboxname.Tag = "";
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtItemcode_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.F5) || (e.KeyCode == Keys.Add))
            {
                tablename = "Stockmst" + cPublic.g_firmcode;
                fields = "itemname,itemcode,qty,rprofit3,rprofit1,GROUPcode";
                displayname = "Itemname,Itemcode,Qty,WPrice,RPrice,GROUP";
                widthfld = "270,100,80,80,80,60";
                tblcondition = "";
                caption = "Item Master";
                loadLookup((TextBox)txtItemcode, e);
            }
        }

        private void txtItemname_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.F5) || (e.KeyCode == Keys.Add))
            {
                tablename = "Stockmst" + cPublic.g_firmcode;
                fields = "itemname,itemcode,qty,rprofit1,rprofit1,Groupcode";
                displayname = "Itemname,Itemcode,Qty,RPrice,WPrice,Group";
                widthfld = "250,150,100,150,100,100";
                tblcondition = "";
                loadLookup((TextBox)txtItemname, e);
            }
        }

        private void txtManufacturer_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.F5) || (e.KeyCode == Keys.Add))
            {
                tablename = "lookup";
                fields = "code,Details";
                displayname = "code,Details";
                widthfld = "200,200";
                tblcondition = "Field1='Manufacturer'";
                loadLookup((TextBox)txtManufacturer, e);

            }
        }

        private void txtProduct_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.F5) || (e.KeyCode == Keys.Add))
            {
                tablename = "lookup";
                fields = "code,Details";
                displayname = "code,Details";
                widthfld = "200,200";
                tblcondition = "Field1='Product'";
                loadLookup((TextBox)txtProduct, e);
            }
        }

        private void txttax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5 || e.KeyCode == Keys.Add)
            {
                tablename = "lookup";
                fields = "code,Details";
                displayname = "code,Details";
                widthfld = "200,200";
                tblcondition = "Field1='taxper'";
                loadLookup((TextBox)txttax, e);
            }
        }

        private void txttax_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            gen.ValidDecimalNumber(e, ref tb, 10, 2);

            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtProduct.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
            if (e.KeyChar != Convert.ToChar(Keys.Back) && e.KeyChar != Convert.ToChar(Keys.Enter))
            {
                tablename = "lookup";
                fields = "code,Details";
                displayname = "code,Details";
                widthfld = "200,200";
                tblcondition = "Field1='taxper'";
                loadLookup((TextBox)txttax, e);
            }
            e.Handled = true;
        }

        private void txtProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtProduct.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
            if (e.KeyChar != Convert.ToChar(Keys.Back) && e.KeyChar != Convert.ToChar(Keys.Enter))
            {
                tablename = "lookup";
                fields = "code,Details";
                displayname = "code,Details";
                widthfld = "200,200";
                tblcondition = "Field1='Product'";
                loadLookup((TextBox)txtProduct, e);
            }
            e.Handled = true;
        }

        private void txtManufacturer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtManufacturer.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
            if (e.KeyChar != Convert.ToChar(Keys.Back) && e.KeyChar != Convert.ToChar(Keys.Enter))
            {
                tablename = "lookup";
                fields = "code,Details";
                displayname = "code,Details";
                widthfld = "200,200";
                tblcondition = "Field1='Manufacturer'";
                loadLookup((TextBox)txtManufacturer, e);
            }
            e.Handled = true;
        }

        private void chkApplyTax_CheckedChanged(object sender, EventArgs e)
        {
            if (chkApplyTax.Checked == true)
            {
                txttax.Enabled = true;
            }
            else
            {
                txttax.Enabled = false;
                txttax.Clear();
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            common();
        }

        public void common()
        {
            string cond1 = " where A.itemcode IS NOT NULL", query = string.Empty;

            if (txtItemcode.Text.Trim() != "")
            {
                cond1 = cond1 + " and A.itemcode like '" + gen.SQLFormat(txtItemcode.Text.Trim()) + "%' ";
            }
            if (txtItemname.Text.Trim() != "")
            {
                cond1 = cond1 + " and A.itemname like '" + gen.SQLFormat(txtItemname.Text.Trim()) + "%' ";
            }
            if (txtManufacturer.Text.Trim() != "")
            {
                cond1 = cond1 + " and Manufacturer='" + gen.SQLFormat(txtManufacturer.Text.Trim()) + "' ";
            }
            if (txttax.Text.Trim() != "" && chkApplyTax.Checked == true)
            {
                cond1 = cond1 + " and taxper='" + gen.SQLFormat(txttax.Text.Trim()) + "' ";
            }
            if (txtProduct.Text.Trim() != "")
            {
                cond1 = cond1 + " and product='" + gen.SQLFormat(txtProduct.Text.Trim()) + "' ";
            }

            if (optQtyitem.Checked == true)
            {
                if (dtpDate.ShowCheckBox == true && dtpDate.Checked == true)
                {
                    cond1 = cond1 + " and ([OPStock]>0 or  [Purchase]>0 or [PReturn]>0 or [Sales]>0 or [SReturn]>0 ) ";
                }
                else
                {
                    cond1 = cond1 + " and a.qty>0";
                }
            }
            if (optZeroqty.Checked == true)
            {
                if (dtpDate.ShowCheckBox == true && dtpDate.Checked == true)
                {
                    cond1 = cond1 + " and ([OPStock]+ [Purchase]- [PReturn]- [Sales]+[SReturn]=0 ) ";
                }
                else
                {
                    cond1 = cond1 + " and a.qty=0";
                }
            }
            if (optMinus.Checked == true)
            {
                if (dtpDate.ShowCheckBox == true && dtpDate.Checked == true)
                {
                    cond1 = cond1 + " and ([OPStock]+[Purchase]-[PReturn]-[Sales]+[SReturn]<0 ) ";
                }
                else
                {
                    cond1 = cond1 + " and a.qty<0";
                }
            }
            string vQty = string.Empty;
            if (this.Tag.ToString() == "PriceList")
            {
                query = "Select Itemcode,Itemname [ItemName],[Unit],Rprofit1 [Retail],Rprofit2 [Whole Sale],Rprofit3 [Special],Qty [Quantity] from Stockmst" + cPublic.g_firmcode + " a " + cond1 + "  order by Itemname ";
            }
            else
            {
                if (dtpDate.ShowCheckBox == true && dtpDate.Checked == true)
                {
                    query = "select itemcode,itemname, " + Environment.NewLine +
                    "  opstock + " + Environment.NewLine +
                    "  (select isnull(SUM(qty),0) from PITEM" + cPublic.g_firmcode + " where DATE1<'" + dtpDate.Value.ToString("yyyy/MM/dd") + "'  and itemcode=R.itemcode ) " + Environment.NewLine +
                    "  - " + Environment.NewLine +
                    "  (select isnull(SUM(qty),0) from PRITEM" + cPublic.g_firmcode + " where DATE1<'" + dtpDate.Value.ToString("yyyy/MM/dd") + "' and itemcode=R.itemcode  ) " + Environment.NewLine +
                    "  - " + Environment.NewLine +
                    "  (select isnull(SUM(qty),0) from sITEM" + cPublic.g_firmcode + " where DATE1<'" + dtpDate.Value.ToString("yyyy/MM/dd") + "'  and itemcode=R.itemcode ) " + Environment.NewLine +
                    "  + " + Environment.NewLine +
                    "  (select isnull(SUM(qty),0) from SRITEM" + cPublic.g_firmcode + " where DATE1<'" + dtpDate.Value.ToString("yyyy/MM/dd") + "'  and itemcode=R.itemcode ) " + Environment.NewLine +
                    "   [OPStock], " + Environment.NewLine +
                    "   (select isnull(SUM(qty),0) from PITEM" + cPublic.g_firmcode + " where DATE1='" + dtpDate.Value.ToString("yyyy/MM/dd") + "' and itemcode=R.itemcode  ) [Purchase], " + Environment.NewLine +
                    "   (select isnull(SUM(qty),0) from PRITEM" + cPublic.g_firmcode + " where DATE1='" + dtpDate.Value.ToString("yyyy/MM/dd") + "' and itemcode=R.itemcode ) [PReturn], " + Environment.NewLine +
                    "   (select isnull(SUM(qty),0) from sITEM" + cPublic.g_firmcode + " where DATE1='" + dtpDate.Value.ToString("yyyy/MM/dd") + "' and itemcode=R.itemcode ) [Sales], " + Environment.NewLine +
                    "   (select isnull(SUM(qty),0) from SRITEM" + cPublic.g_firmcode + " where DATE1='" + dtpDate.Value.ToString("yyyy/MM/dd") + "' and itemcode=R.itemcode ) [SReturn] , " + Environment.NewLine +
                    "   Manufacturer,taxper,product    " + Environment.NewLine +
                    "   into #table " + Environment.NewLine +
                    "     from dbo.STOCKMST" + cPublic.g_firmcode + " R " + Environment.NewLine +
                    "  " + Environment.NewLine +
                    "  select ROW_NUMBER() OVER (ORDER BY ItemName,itemcode) AS [SlNo],Itemcode,ItemName,[OPStock] ,[Purchase],[PReturn],[Sales],[SReturn] , " + Environment.NewLine +
                    "  [OPStock]+[Purchase]-[PReturn]-[Sales]+[SReturn] ClosingStock  from #table a " + cond1 + "  order by ItemName ";
                }
                else
                {
                    query = "Select ROW_NUMBER() OVER (ORDER BY ItemName,itemcode) AS [SlNo],Itemcode,Itemname [ItemName],[Unit],Qty [Quantity] from Stockmst" + cPublic.g_firmcode + " a " + cond1 + "    order by Itemname ";
                }
            }


            Display_Details(query);
        }

        private void Display_Details(string sqlQuery)
        {
            if (cPublic.Connection.State == ConnectionState.Open)
            { cPublic.Connection.Close(); cPublic.Connection.Open(); }
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, cPublic.Connection);
            DataTable dt = new DataTable();
            da.Fill(dt);

            flxStockPrv.DataSource = dt.DefaultView;


            flxStockPrv.Focus();

            for (int i = 0; i < flxStockPrv.Cols.Count; i++)
            {
                if (flxStockPrv.Cols[i].DataType == null) { continue; }
                string type = flxStockPrv.Cols[i].DataType.ToString();
                if (type.Equals("System.Decimal"))
                    if ((this.Tag + "".ToString() == "QtyList" || this.Tag + "".ToString() == "QtyList2") && dtpDate.Visible == true && dtpDate.ShowCheckBox == true && dtpDate.Checked ==true  )
                    {
                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                        if (this.Tag + "".ToString() == "QtyList" && flxStockPrv.Cols["Godown"] != null)
                        {
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["ITEMCODE"].Index, flxStockPrv.Cols["ClosingStock"].Index, "Sub Total");
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["ITEMCODE"].Index, flxStockPrv.Cols["SReturn"].Index, "Sub Total");
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["ITEMCODE"].Index, flxStockPrv.Cols["Sales"].Index, "Sub Total");
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["ITEMCODE"].Index, flxStockPrv.Cols["PReturn"].Index, "Sub Total");
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["ITEMCODE"].Index, flxStockPrv.Cols["Purchase"].Index, "Sub Total");
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["ITEMCODE"].Index, flxStockPrv.Cols["OPStock"].Index, "Sub Total");
                        }
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, -1, flxStockPrv.Cols["ClosingStock"].Index, "Grand Total");
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, -1, flxStockPrv.Cols["SReturn"].Index, "Grand Total");
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, -1, flxStockPrv.Cols["Sales"].Index, "Grand Total");
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, -1, flxStockPrv.Cols["PReturn"].Index, "Grand Total");
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, -1, flxStockPrv.Cols["Purchase"].Index, "Grand Total");
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, -1, flxStockPrv.Cols["OPStock"].Index, "Grand Total");
                    }
                    else if (this.Tag + "".ToString() == "QtyList" || this.Tag + "".ToString() == "QtyList2")
                    {
                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                        if (this.Tag + "".ToString() == "QtyList" && flxStockPrv.Cols["Godown"] != null)
                        {
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["ITEMCODE"].Index, flxStockPrv.Cols["Quantity"].Index, "Sub Total");
                        }
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, -1, flxStockPrv.Cols["Quantity"].Index, "Grand Total");
                    }

                if (this.Tag + "".ToString() == "Stkvalue")
                {
                    flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, -1, 3, "Grand Total");
                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, -1, 7, "Grand Total");
                }
                if (this.Tag + "".ToString() == "Stkvalue2")
                {
                    flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                    if (flxStockPrv.Cols["LC"] != null) { flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, -1, i, "Grand Total"); }

                    if (flxStockPrv[0, 2] + "" == "code")
                    {
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, -1, 5, "Grand Total");
                        flxStockPrv.Cols[2].Visible = false;
                    }
                }
                if (this.Tag + "".ToString() == "SKM")
                {
                    if (Convert.ToString(flxStockPrv[0, i] + "").ToUpper().Contains("ITEMNAME") == true)
                    {
                        flxStockPrv.Cols[i].Width = Convert.ToInt32(gewidth.ToString()) + 400;
                    }

                    flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, -1, flxStockPrv.Cols["Qty"].Index, "Grand Total");
                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, -1, flxStockPrv.Cols["MoveOut Qty"].Index, "Grand Total");
                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, -1, flxStockPrv.Cols["Rec. Qty"].Index, "Grand Total");
                }
            }
            for (int i = 0; i < flxStockPrv.Cols.Count; i++)
            {
                if (flxStockPrv.Cols[i].DataType == null) { continue; }
                string type = flxStockPrv.Cols[i].DataType.ToString();
                if (type.Equals("System.Decimal"))
                {
                    if (flxStockPrv[0, i] + "".ToString() == "QUANTITY")
                    {
                        flxStockPrv.Cols[i].Format = "#0.000";
                    }
                    else if (flxStockPrv[0, i] + "".ToString() == "OPENING QTY")
                    { flxStockPrv.Cols[i].Format = "#0.000"; }
                    else
                    {
                        flxStockPrv.Cols[i].Format = "##,###0.00";
                    }
                }
            }

            if (this.Tag + "".ToString() == "QtyList" || this.Tag + "".ToString() == "Stkvalue" || this.Tag + "".ToString() == "Stkvalue2" || this.Tag + "".ToString() == "QtyList2")
            {
                if (flxStockPrv.Rows.Count > 1)
                {
                    flxStockPrv.SetData(flxStockPrv.Rows.Count - 1, 1, "GRAND TOTAL");
                }
            }
            string Cust_Supp = "";
            if (this.Tag + "" == "Cust_Supp")
            {

                for (int i = 1; i < flxStockPrv.Rows.Count; i++)
                {

                    if (flxStockPrv[i, 1].ToString().ToUpper() != Cust_Supp.ToUpper())
                    {
                        Cust_Supp = flxStockPrv[i, 1].ToString();
                        // MessageBox.Show(flxStockPrv[i, 1].ToString());

                    }
                    else
                    {
                        flxStockPrv[i, 1] = "";
                    }



                }
            }
            setSlNo();

            gewidth = Convert.ToInt32(flxStockPrv.Width.ToString()) / flxStockPrv.Cols.Count;


            for (int wdth = 0; wdth < flxStockPrv.Cols.Count; wdth++)
            {
                if (flxStockPrv.Cols.Count <= 6)
                {
                    if (flxStockPrv.Cols[wdth].Visible == true)
                    {
                        if (this.Tag + "".ToString() == "QtyList" || this.Tag + "".ToString() == "QtyList2" || this.Tag + "".ToString() == "QtyList2")
                        {
                            if (Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEMNAME") == true)
                            {
                                flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) + 200;
                            }
                        }
                        if (this.Tag + "".ToString() == "Stkvalue")
                        {
                            if (Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEMNAME") == true)
                            {
                                flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) + 200;
                                label1.Visible = true;
                                if (flxStockPrv.Rows.Count > 1)
                                {
                                    label1.Text = Strings.FormatNumber((Convert.ToDouble(flxStockPrv[flxStockPrv.Rows.Count - 1, 5] + "".ToString()) / Convert.ToDouble(flxStockPrv[flxStockPrv.Rows.Count - 1, 3] + "".ToString())) + "", 2, TriState.True, TriState.True, TriState.True);
                                }
                            }
                        }

                        if (this.Tag + "".ToString() == "Stkvalue2")
                        {
                            if (Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEMNAME") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("Manufacturer") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("PRODUCT") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("GROUP NAME") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("Sub Group") == true)
                            {
                                flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) + 200;
                                //label1.Visible = true;
                                //if (flxStockPrv.Rows.Count > 1)
                                //{
                                //    label1.Text = Strings.FormatNumber((Convert.ToDouble(flxStockPrv[flxStockPrv.Rows.Count - 1, 3] + "".ToString()) / Convert.ToDouble(flxStockPrv[flxStockPrv.Rows.Count - 1, 3] + "".ToString())) + "", 2, TriState.True, TriState.True, TriState.True);
                                //}
                            }

                            if (Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("STOCK VALUE") == true)
                            {
                                flxStockPrv.Cols[wdth].Width = 210;
                            }
                        }

                        if (this.Tag + "".ToString() == "PriceList")
                        {
                            if (Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEMNAME") == true)
                            {
                                flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) + 200;
                            }
                        }
                        if (Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEMCODE") == true)
                        {
                            flxStockPrv.Cols[wdth].Width = 200;
                        }
                        if (this.Tag + "".ToString() == "SKM")
                        {
                            if (Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEMNAME") == true)
                            {
                                flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) + 200;
                            }
                        }
                    }
                }
                else
                {
                    string type = flxStockPrv.Cols[wdth].DataType+"".ToString();

                    if (type.Equals("System.Decimal"))
                    {
                        if (this.Tag + "".ToString() != "NULL")
                        {
                            if (gewidth > 30)
                            {
                                flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) - 20;
                            }
                        }
                    }
                    else
                    {
                        if (Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("CUSTOMER") == true || Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEMNAME") == true || Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("SUPPLIER") == true || Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("SUPPLIER NAME") == true || Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEM NAME") == true || Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEMNAME") == true || Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("CUSTOMER NAME") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("Manufacturer") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("Product") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("Group") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("Color/Size") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("Schedule") == true || Convert.ToString(flxStockPrv[0, wdth] + "".ToString()).Contains("Commodity") == true)
                        {
                            flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) + 130;
                        }
                        else if (Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEM CODE") == true || Convert.ToString(flxStockPrv[0, wdth] + "").ToUpper().Contains("ITEMCODE") == true)
                        {
                            flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) + 100;
                        }
                        else
                        {
                            flxStockPrv.Cols[wdth].Width = 50;
                        }
                    }
                }
            }
        }

        private void setSlNo()
        {
            int r = 1;
            flxStockPrv[0, 0] = "Nos";

            for (int i = 1; i < flxStockPrv.Rows.Count; i++)
            {
                if (flxStockPrv[i, 1] + "".ToString() != "")
                {

                    if (flxStockPrv[i, 1] + "".ToString() != "SUB TOTAL")
                    {
                        if (flxStockPrv[i, 1] + "".ToString() != "GRAND TOTAL")
                        {
                            flxStockPrv[i, 0] = r;
                            r++;
                        }
                    }
                }

            }
            r--;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            cPublic.g_rpthead = this.Text;
            cgen.RepPrint(PrintDos().ToString());
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            cPublic.g_rpthead = this.Text;
            Interaction.Shell("notepad.exe  " + cPublic.FilePath + "\\" + PrintDos() + ".txt", AppWinStyle.MaximizedFocus, true, 1);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataExport data = new DataExport();
            data.flex_Excel(flxStockPrv);
        }

        private int  PrintDos()
        {
            leng = 0;

            for (ii = 0; ii <= flxStockPrv.Cols.Count - 1; ii++)
            {

                if (flxStockPrv.Cols[ii].Visible == true)
                {

                    if (flxStockPrv[0, ii].ToString() == "Bill No" || flxStockPrv[0, ii].ToString() == "Inv.No." || flxStockPrv[0, ii].ToString() == "Tin.No." || flxStockPrv[0, ii].ToString() == "Order No")
                    {
                        lee = 18;
                    }
                    else if (flxStockPrv[0, ii].ToString() == "A/C Code" || flxStockPrv[0, ii].ToString().ToUpper () == "ITEMCODE")
                    {
                        lee = 18;
                    }

                    else if (flxStockPrv[0, ii].ToString() == "Date" || flxStockPrv[0, ii].ToString() == "Inv.Date")
                    {
                        lee = 20;
                    }
                    else if (flxStockPrv[0, ii].ToString() == "Unit")
                    {
                        lee = 1;
                    }
                    else if (flxStockPrv[0, ii].ToString() == "Customer" || flxStockPrv[0, ii].ToString() == "Item Name" || flxStockPrv[0, ii].ToString() == "ItemName" || flxStockPrv[0, ii].ToString() == "Supplier" || flxStockPrv[0, ii].ToString() == "Supplier Name" || flxStockPrv[0, ii].ToString() == "ITEMNAME")
                    {
                        lee = 40;
                    }
                    else if (Convert.ToString(flxStockPrv[0, ii] + "").Trim().ToUpper() == "NOS")
                    {
                        lee = 13;
                    }
                    else
                    {

                        lee = flxStockPrv.Cols[ii].ToString().Length;
                    }

                    if (flxStockPrv.Cols[ii].DataType == null) { continue; }
                    string type = flxStockPrv.Cols[ii].DataType.ToString();
                    if (type.Equals("System.Decimal"))
                    {
                        lee = lee - 13;
                        leng = leng + lee + 1;
                    }

                    else
                    {
                        lee = lee - 9;
                        leng = leng + lee + 1;
                    }

                }

            }

            RptWidth = leng - 2;

            PageNo = 0;
            lineno = 0;
            lee = 0;
            le = 0;

            int notepadpath = (Convert.ToInt32(1000 * VBMath.Rnd()) + 1);

            if (Directory.Exists(cPublic.FilePath + "\\" + notepadpath + ".txt"))
            {
                Directory.Delete(cPublic.FilePath + "\\" + notepadpath + ".txt");
            }

            string path = cPublic.FilePath + "\\" + notepadpath + ".txt";
            tstream = File.CreateText(path);

            rPagehead(path);
            oneline(path);

            tstream.WriteLine(cgen.Replicate("=", RptWidth));
            for (int j = 0; j < Convert.ToInt32(cPublic.LinestoSkip) + 4; j++)
            { tstream.WriteLine(); }
            tstream.Flush();
            tstream.Close();


            return notepadpath;
        }

        private void rPagehead(string path)
        {
            PageNo = PageNo + 1;           
            tstream.Write(cPublic.Ch10 + cPublic.ChN);
            tstream.WriteLine(cPublic.BchDW);
            lineno += 1;
            tstream.WriteLine(cgen.FnAlignString(cPublic.g_CompName, "L", RptWidth));
            lineno += 1;
            tstream.Write(cPublic.BchNDW);
            tstream.Write(cPublic.Ch10 + cPublic.ChN);
            tstream.Write(cPublic.ChC);
            tstream.WriteLine(cgen.FnAlignString(cPublic.g_add1, "C", RptWidth));
            tstream.WriteLine(cgen.FnAlignString(cPublic.g_add2, "C", RptWidth));
            tstream.WriteLine(cgen.FnAlignString(cPublic.g_add3, "C", RptWidth));
            tstream.WriteLine(cgen.FnAlignString("Tin No:" + cPublic.g_TinNo, "C", RptWidth));
            lineno += 4;
            if (cPublic.g_rpthead == "Stock Value 1")
            {
                tstream.WriteLine(cgen.FnAlignString("Stock Value Report", "C", RptWidth));
                lineno += 1;
            }
            else
            {
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_rpthead, "C", RptWidth));
                lineno += 1;
            }

            if (dtpDate.Checked == true)
            {
                tstream.Write(cgen.FnAlignString("As Of Date : " + dtpDate.Value .ToString("dd/MM/yyyy"), "L", RptWidth - 10));
            }
            else
            {
                tstream.Write(cgen.FnAlignString("As Of Date : " + cPublic.g_Rdate.ToString("dd/MM/yyyy"), "L", RptWidth - 10));
            }
            tstream.WriteLine(cgen.FnAlignString("PageNo. " + PageNo, "R", 10));


            tstream.WriteLine(cgen.Replicate("-", RptWidth));

            lineno += 2;
            for (int iii = 0; iii <= flxStockPrv.Cols.Count - 1; iii++)
            {

                if (this.Tag.ToString() == "PriceList" && iii == flxStockPrv.Cols["select"].Index) { continue; }
                if (flxStockPrv.Cols[iii].Visible == true)
                {

                    if (flxStockPrv.Cols[iii].DataType == null) { continue; }
                    lee = flxStockPrv.Cols[iii].ToString().Length;
                    string type = flxStockPrv.Cols[iii].DataType.ToString();
                    if (type.Equals("System.Decimal"))
                    {
                        tstream.Write(cgen.FnAlignString(flxStockPrv.GetData(0, iii) + "".ToString(), "R", lee - 13));

                    }
                    else
                    {
                        if (flxStockPrv[0, iii].ToString() == "Bill No" || flxStockPrv[0, iii].ToString() == "Inv.No." || flxStockPrv[0, iii].ToString() == "Tin.No." || flxStockPrv[0, iii].ToString() == "Order No")
                        {
                            lee = 18;
                        }
                        else if (flxStockPrv[0, iii].ToString() == "Date" || flxStockPrv[0, iii].ToString() == "Inv.Date")
                        {
                            lee = 20;
                        }
                        else if (flxStockPrv[0, iii].ToString() == "A/C Code" || flxStockPrv[0, iii].ToString().ToUpper() == "ITEMCODE")
                        {
                            lee = 18;
                        }
                        else if (flxStockPrv[0, iii].ToString() == "Unit")
                        {
                            lee = 14;
                        }
                        else if (flxStockPrv[0, iii].ToString() == "Customer" || flxStockPrv[0, iii].ToString() == "Item Name" || flxStockPrv[0, iii].ToString() == "ItemName" || flxStockPrv[0, iii].ToString() == "Supplier" || flxStockPrv[0, iii].ToString() == "Supplier Name" || flxStockPrv[0, iii].ToString() == "ITEMNAME")
                        {
                            lee = 39;
                        }
                        else if (Convert.ToString(flxStockPrv[0, iii] + "").Trim().ToUpper() == "NOS")
                        {
                            lee = 13;
                        }
                        tstream.Write(cgen.FnAlignString(flxStockPrv.GetData(0, iii) + "".ToString(), "L", lee - 9));

                    }
                    tstream.Write(" ");
                }

            }

            tstream.WriteLine();
            tstream.WriteLine(cgen.Replicate("-", RptWidth));
            lineno += 1;

            //lineno = 11;
        }

        private void oneline(string path)
        {
            initialize_ColSum();
            for (ii = 1; ii <= flxStockPrv.Rows.Count - 1; ii++)
            {
                if (this.Tag.ToString() == "PriceList" && flxStockPrv.GetCellCheck(ii, flxStockPrv.Cols["select"].Index) == CheckEnum.Unchecked)
                { continue; }

                for (jj = 0; jj <= flxStockPrv.Cols.Count - 1; jj++)
                {                    
                    if (this.Tag.ToString() == "PriceList" && jj == flxStockPrv.Cols["select"].Index) { continue; }

                    if (flxStockPrv.Cols[jj].Visible == true)
                    {
                        le = flxStockPrv.Cols[jj].ToString().Length;

                        if (line == false)
                        {
                            if (flxStockPrv[ii, 0] == null && this.Tag + "".ToString() != "SR")
                            {
                                if (day == false)
                                {
                                    for (int i = 0; i <= flxStockPrv.Cols.Count - 1; i++)
                                    {
                                        if (flxStockPrv.Cols[i].DataType == null) { continue; }

                                        if (flxStockPrv.Cols[i].Visible == true)
                                        {
                                            le = 25;

                                            if (flxStockPrv.Cols[i].DataType.ToString() == "System.Decimal")
                                            {

                                            }

                                            else
                                            {
                                                if (flxStockPrv[0, i].ToString() == "Bill No" || flxStockPrv[0, i].ToString() == "Inv.No." || flxStockPrv[0, i].ToString() == "Tin.No." || flxStockPrv[0, i].ToString() == "Order No" || flxStockPrv[0, i].ToString().ToUpper() == "ITEMCODE")
                                                {
                                                    le = 18;
                                                }

                                                if (flxStockPrv[0, i].ToString() == "Date" || flxStockPrv[0, i].ToString() == "Inv.Date")
                                                {

                                                    le = 20;
                                                }
                                                else if (flxStockPrv[0, i].ToString() == "Customer" || flxStockPrv[0, i].ToString() == "Item Name" || flxStockPrv[0, i].ToString() == "ItemName" || flxStockPrv[0, i].ToString() == "Supplier" || flxStockPrv[0, i].ToString() == "Supplier Name" || flxStockPrv[0, i].ToString() == "ITEMNAME")
                                                {

                                                    le = 40;
                                                }

                                                else if (Convert.ToString(flxStockPrv[0, i] + "").Trim().ToUpper() == "NOS")
                                                {
                                                    le = 13;
                                                }
                                            }
                                        }
                                    }

                                }
                                if (this.Tag + "" != "Cust_Supp")
                                {
                                    tstream.WriteLine(cgen.Replicate("-", RptWidth));
                                }

                                line = true;
                            }
                        }

                        if (flxStockPrv.Cols[jj].DataType == null) { continue; }
                        string type = flxStockPrv.Cols[jj].DataType.ToString();
                        if (type.Equals("System.Decimal"))
                        {
                            if (flxStockPrv.GetData(ii, jj) + "".ToString() != "" && flxStockPrv[ii, 0] != null)
                            {
                                if (flxStockPrv[0, jj] + "".ToString() == "LC" || flxStockPrv[0, jj] + "".ToString() == "TAX %" || flxStockPrv[0, jj] + "".ToString() == "RETAIL RATE")
                                {
                                    tstream.Write(cgen.FnAlignString(Convert.ToDecimal(flxStockPrv.GetData(ii, jj)).ToString("##,###0.00"), "R", le - 14));
                                    tstream.Write("  ");
                                }
                                else
                                {
                                    tstream.Write(cgen.FnAlignString(Convert.ToDecimal(flxStockPrv.GetData(ii, jj)).ToString("#0.000"), "R", le - 14));
                                    tstream.Write("  ");
                                }
                            }
                            else if (flxStockPrv[ii, 0] == null)
                            {
                                if (flxStockPrv[0, jj] + "".ToString() == "LC" || flxStockPrv[0, jj] + "".ToString() == "TAX %" || flxStockPrv[0, jj] + "".ToString() == "RETAIL RATE")
                                {
                                    tstream.Write(cgen.FnAlignString(Convert.ToDecimal(flxStockPrv.GetData(ii, jj)).ToString("##,###0.00"), "R", le - 14));
                                    tstream.Write("  ");
                                }
                                else
                                {
                                    tstream.Write(cgen.FnAlignString(Convert.ToDecimal(flxStockPrv.GetData(ii, jj)).ToString("#0.000"), "R", le - 14));
                                    tstream.Write("  ");
                                }
                            }

                        }
                        else if (flxStockPrv.GetData(ii, jj) + "".ToString() == "")
                        {

                            if (flxStockPrv[0, jj].ToString() == "Bill No" || flxStockPrv[0, jj].ToString() == "Inv.No." || flxStockPrv[0, jj].ToString() == "Tin.No." || flxStockPrv[0, jj].ToString() == "Order No")
                            {

                                le = 18;
                            }

                            else if (flxStockPrv[0, jj].ToString() == "A/C Code" || flxStockPrv[0, jj].ToString().ToUpper() == "ITEMCODE")
                            {

                                le = 18;
                            }

                            else if (flxStockPrv[0, jj].ToString() == "Date" || flxStockPrv[0, jj].ToString() == "Inv.Date")
                            {

                                le = 20;
                            }
                            else if (flxStockPrv[0, jj].ToString() == "Customer" || flxStockPrv[0, jj].ToString() == "Item Name" || flxStockPrv[0, jj].ToString() == "ItemName" || flxStockPrv[0, jj].ToString() == "Supplier" || flxStockPrv[0, jj].ToString() == "Supplier Name" || flxStockPrv[0, jj].ToString() == "ITEMNAME")
                            {

                                le = 40;
                            }
                            else if (Convert.ToString(flxStockPrv[0, jj] + "").Trim().ToUpper() == "NOS")
                            {
                                le = 13;
                            }

                            tstream.Write(cgen.FnAlignString(" ", "L", le - 9));
                            tstream.Write(" ");
                        }
                        else
                        {

                            if (flxStockPrv[0, jj].ToString() == "Bill No" || flxStockPrv[0, jj].ToString() == "Inv.No." || flxStockPrv[0, jj].ToString() == "Tin.No." || flxStockPrv[0, jj].ToString() == "Order No")
                            {

                                le = 18;
                            }

                            else if (flxStockPrv[0, jj].ToString() == "Date" || flxStockPrv[0, jj].ToString() == "Inv.Date")
                            {

                                le = 20;
                            }

                            else if (flxStockPrv[0, jj].ToString() == "A/C Code" || flxStockPrv[0, jj].ToString().ToUpper() == "ITEMCODE")
                            {

                                le = 18;
                            }
                            else if (flxStockPrv[0, jj].ToString() == "Customer" || flxStockPrv[0, jj].ToString() == "Item Name" || flxStockPrv[0, jj].ToString() == "ItemName" || flxStockPrv[0, jj].ToString() == "Supplier" || flxStockPrv[0, jj].ToString() == "Supplier Name" || flxStockPrv[0, jj].ToString() == "ITEMNAME")
                            {

                                le = 40;
                            }
                            else if (flxStockPrv[0, jj].ToString() == "Unit")
                            {

                                le = 14;
                            }

                            else if (Convert.ToString(flxStockPrv[0, jj] + "").Trim().ToUpper() == "NOS")
                            {
                                le = 13;
                            }
                            tstream.Write(cgen.FnAlignString(flxStockPrv.GetData(ii, jj) + " ".ToString(), "L", le - 9));
                            tstream.Write(" ");
                        }

                    }

                }
                tstream.WriteLine();
                line = false;
                lineno = lineno + 1;

                if (lineno == G_Lperpage)
                {
                    lin = false;

                }
                else
                {
                    lin = true;
                }


                if (lin == false)
                {
                    if (lineno == G_Lperpage)
                    {
                        if (day == false)
                        {
                            for (int i = 0; i <= flxStockPrv.Cols.Count - 1; i++)
                            {
                                if (flxStockPrv.Cols[i].Visible == true)
                                {
                                    if (flxStockPrv[i, 0] == null)
                                    {
                                        le = 20;
                                    }
                                    else
                                    {
                                        le = 25;
                                    }

                                    if (flxStockPrv.Cols[i].DataType+"".ToString() == "System.Decimal")
                                    {
                                        // tstream.Write(cgen.FnAlignString(colSum[i].ToString(), "R", le - 13));
                                    }

                                    else
                                    {
                                        if (flxStockPrv[0, i].ToString() == "Bill No" || flxStockPrv[0, i].ToString() == "Inv.No." || flxStockPrv[0, i].ToString() == "Tin.No." || flxStockPrv[0, i].ToString() == "Order No" || flxStockPrv[0, i].ToString().ToUpper() == "ITEMCODE")
                                        {

                                            le = 18;
                                        }

                                        if (flxStockPrv[0, i].ToString() == "Date" || flxStockPrv[0, i].ToString() == "Inv.Date")
                                        {

                                            le = 20;
                                        }
                                        else if (flxStockPrv[0, i].ToString() == "Customer" || flxStockPrv[0, i].ToString() == "Item Name" || flxStockPrv[0, i].ToString() == "ItemName")
                                        {
                                            le = 40;
                                        }

                                        else if (Convert.ToString(flxStockPrv[0, i] + "").Trim().ToUpper() == "NOS")
                                        {
                                            le = 13;
                                        }
                                        tstream.Write(cgen.FnAlignString(" ", "L", le - 9));
                                    }
                                }
                            }
                            tstream.WriteLine();
                            lineno += 1;
                        }

                        tstream.WriteLine(cgen.Replicate("=", RptWidth));
                        tstream.WriteLine(" " + Convert.ToChar(12));
                        lineno = 1;
                        rPagehead(path);
                        lin = true;
                    }
                    initialize_ColSum();
                }


                for (jj = 0; jj <= flxStockPrv.Cols.Count - 1; jj++)
                {
                    if (flxStockPrv.Cols[jj].Visible == true)
                    {
                        le = flxStockPrv.Cols[jj].ToString().Length;

                        if (flxStockPrv.GetData(ii, jj) + "".ToString() == "SUB TOTAL")
                        {

                            string type1 = flxStockPrv.Cols[jj].DataType.ToString();
                            if (type1.Equals("System.Decimal"))
                            {
                                //tstream.Write(cgen.FnAlignString(flxStockPrv.GetData(ii, jj) + "".ToString(), "R", le - 13));
                            }
                            else
                            {
                                tstream.WriteLine(cgen.Replicate("-", RptWidth));
                                lineno += 1;
                            }
                        }
                    }
                }
            }
            lin = false;
        }

        private void initialize_ColSum()
        {
            for (int i = 0; i < flxStockPrv.Cols.Count; i++)
            { colSum[i] = 0; }

        }

        private void ucStockDetails_Load(object sender, EventArgs e)
        {
            dtpDate.Checked = false;

            if (this.Tag.ToString() == "PriceList")
            {
                dtpDate.Visible = false;
                label5.Visible = false; 
            }
        }



    }
}
