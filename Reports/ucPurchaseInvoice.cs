using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Data.SqlClient;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Reports
{
    public partial class ucPurchaseInvoice : UserControl
    {
        public ucPurchaseInvoice()
        {
            InitializeComponent();
        }

        fssgen.fssgen fGen = new fssgen.fssgen();
        cGeneral cgen = new cGeneral();

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            string sql = "select convert(varchar,DATE1,103)[Date],orderno[Order No],invno[Inv.No],convert(varchar,invdate,103)[Inv.Date],isnull((select head from Accounts" + cPublic.g_firmcode + " where code=suppcode),suppname) [Supplier Name],invamt[Amount] "
                        + "from PURCHASE" + cPublic.g_firmcode + " where date1 >= '" + DtpFrom.Value.ToString("yyyy/MM/dd") + "' and  date1<= '"
                        + DtpTo.Value.ToString("yyyy/MM/dd") + "'";
            if (txtSupp.Text.Trim() != string.Empty)
            { sql += " and suppcode='" + txtSupp.Tag + "".ToString() + "'"; }
            if (Information.IsNumeric(txtTaxPer.Text) == true)
            { sql += " and ORDERNO in (SELECT ORDERNO FROM dbo.PITEM" + cPublic.g_firmcode + " WHERE TAXPER=" + Convert.ToDecimal(txtTaxPer.Text) + ")"; }

            Load_Details(sql);

        }
        private void Load_Details(string sqlQuery)
        {
            flxStockPrv.ExtendLastCol = true;
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, cPublic.Connection);
            DataTable dt = new DataTable();
            da.Fill(dt);

            flxStockPrv.DataSource = dt.DefaultView;

            for (int i = 1; i < flxStockPrv.Cols.Count; i++)
            {
                string type = flxStockPrv.Cols[i].DataType.ToString();
                if (type.Equals("System.Decimal"))
                {
                    C1.Win.C1FlexGrid.CellStyle cs;

                    //if (subtotal == true)
                    //{
                    //    flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;

                    //    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, 3, 3, i, "Subtotal");
                    //}
                    flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                    if (flxStockPrv.Rows.Count > 1)
                    {
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, -1, -1, i, "Grand Total");

                        flxStockPrv.SetData(flxStockPrv.Rows.Count - 1, 1, "GRAND TOTAL", true);
                    }
                    cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.GrandTotal];
                    cs.BackColor = Color.DarkGray;
                    cs.ForeColor = Color.White;
                }
            }
        }
        private void txtSupp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { loadSuppLookup(""); }
        }

        private void txtSupp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!e.KeyChar.Equals('\r') && !e.KeyChar.Equals('\b'))
            {
                e.Handled = true;
                loadSuppLookup(txtSupp.Text.Trim() + e.KeyChar.ToString());
            }
        }

        private void loadSuppLookup(string str)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;

            lookup.m_table = "ACCOUNTS" + cPublic.g_firmcode;
            lookup.m_fields = "HEAD,CODE,Address,Street,City,Balance";
            lookup.m_fldwidth = "350,0,0,150,150,100,0,0";
            lookup.m_strat = str;
            lookup.m_condition = "groupcode = '100012'";
            lookup.m_dispname = "Account Head, Code, Address,Street,Place,Balance";
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                txtSupp.Text = lookup.m_values[0].ToString();
                txtSupp.Tag = lookup.m_values[1].ToString();
                SendKeys.Send("{Enter}");
            }
            else
            {
                txtSupp.Text = string.Empty;
                txtSupp.Tag = string.Empty;
            }
        }

        private void txtTaxPer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { LoadLookup((TextBox)sender, "TAXPER", string.Empty); }
        }

        private void txtTaxPer_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            fGen.ValidDecimalNumber(e, ref tb, 17, 2);
        }

        private void LoadLookup(TextBox txtBx, string option, string start)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;

            lookup.m_condition = "Field1 = '" + option + "'";
            lookup.m_table = "LOOKUP";
            lookup.m_fields = "Code,Details";
            lookup.m_dispname = "Code,Details";
            lookup.m_fldwidth = "75,150";
            lookup.m_strat = start;
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                txtBx.Text = lookup.m_values[0].ToString();
                txtBx.Tag = lookup.m_values[1].ToString();

                SendKeys.Send("{TAB}");
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataExport data = new DataExport();
            data.flex_Excel(flxStockPrv);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //cgen.RepPrint(PrintDos().ToString());
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            //Interaction.Shell("notepad.exe  " + cPublic.FilePath + "\\" + PrintDos() + ".txt", AppWinStyle.MaximizedFocus, true, 1);
   
        }
    }
}
