namespace Xpire.Reports
{
    partial class ucLedgerView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.chkpasschq = new System.Windows.Forms.CheckBox();
            this.rddailybal = new System.Windows.Forms.RadioButton();
            this.rdmonthbal = new System.Windows.Forms.RadioButton();
            this.chkopbal = new System.Windows.Forms.CheckBox();
            this.rdyearbal = new System.Windows.Forms.RadioButton();
            this.chkprintled = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.chkNormal = new System.Windows.Forms.CheckBox();
            this.btnprint = new System.Windows.Forms.Button();
            this.btnreturn = new System.Windows.Forms.Button();
            this.btnview = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtfromdate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dttodate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtdate = new System.Windows.Forms.DateTimePicker();
            this.txtpageno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtgrouphead = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtacchead = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.chkpasschq);
            this.panel3.Controls.Add(this.rddailybal);
            this.panel3.Controls.Add(this.rdmonthbal);
            this.panel3.Controls.Add(this.chkopbal);
            this.panel3.Controls.Add(this.rdyearbal);
            this.panel3.Controls.Add(this.chkprintled);
            this.panel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(3, 146);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(569, 100);
            this.panel3.TabIndex = 6;
            // 
            // chkpasschq
            // 
            this.chkpasschq.AutoSize = true;
            this.chkpasschq.BackColor = System.Drawing.Color.Transparent;
            this.chkpasschq.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkpasschq.Location = new System.Drawing.Point(21, 12);
            this.chkpasschq.Name = "chkpasschq";
            this.chkpasschq.Size = new System.Drawing.Size(164, 18);
            this.chkpasschq.TabIndex = 0;
            this.chkpasschq.Text = "Passed Cheques Only";
            this.chkpasschq.UseVisualStyleBackColor = false;
            // 
            // rddailybal
            // 
            this.rddailybal.AutoSize = true;
            this.rddailybal.BackColor = System.Drawing.Color.Transparent;
            this.rddailybal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rddailybal.Location = new System.Drawing.Point(393, 12);
            this.rddailybal.Name = "rddailybal";
            this.rddailybal.Size = new System.Drawing.Size(108, 18);
            this.rddailybal.TabIndex = 3;
            this.rddailybal.TabStop = true;
            this.rddailybal.Text = "Daily Balance";
            this.rddailybal.UseVisualStyleBackColor = false;
            this.rddailybal.Visible = false;
            // 
            // rdmonthbal
            // 
            this.rdmonthbal.AutoSize = true;
            this.rdmonthbal.BackColor = System.Drawing.Color.Transparent;
            this.rdmonthbal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdmonthbal.Location = new System.Drawing.Point(393, 40);
            this.rdmonthbal.Name = "rdmonthbal";
            this.rdmonthbal.Size = new System.Drawing.Size(127, 18);
            this.rdmonthbal.TabIndex = 4;
            this.rdmonthbal.TabStop = true;
            this.rdmonthbal.Text = "Monthly Balance";
            this.rdmonthbal.UseVisualStyleBackColor = false;
            // 
            // chkopbal
            // 
            this.chkopbal.AutoSize = true;
            this.chkopbal.BackColor = System.Drawing.Color.Transparent;
            this.chkopbal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkopbal.Location = new System.Drawing.Point(21, 40);
            this.chkopbal.Name = "chkopbal";
            this.chkopbal.Size = new System.Drawing.Size(165, 18);
            this.chkopbal.TabIndex = 1;
            this.chkopbal.Text = "With Opening Balance";
            this.chkopbal.UseVisualStyleBackColor = false;
            // 
            // rdyearbal
            // 
            this.rdyearbal.AutoSize = true;
            this.rdyearbal.BackColor = System.Drawing.Color.Transparent;
            this.rdyearbal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdyearbal.Location = new System.Drawing.Point(393, 69);
            this.rdyearbal.Name = "rdyearbal";
            this.rdyearbal.Size = new System.Drawing.Size(116, 18);
            this.rdyearbal.TabIndex = 5;
            this.rdyearbal.TabStop = true;
            this.rdyearbal.Text = "Yearly Balance";
            this.rdyearbal.UseVisualStyleBackColor = false;
            // 
            // chkprintled
            // 
            this.chkprintled.AutoSize = true;
            this.chkprintled.BackColor = System.Drawing.Color.Transparent;
            this.chkprintled.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkprintled.Location = new System.Drawing.Point(21, 68);
            this.chkprintled.Name = "chkprintled";
            this.chkprintled.Size = new System.Drawing.Size(143, 18);
            this.chkprintled.TabIndex = 2;
            this.chkprintled.Text = "Print Ledger Index";
            this.chkprintled.UseVisualStyleBackColor = false;
            this.chkprintled.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.chkNormal);
            this.panel4.Controls.Add(this.btnprint);
            this.panel4.Controls.Add(this.btnreturn);
            this.panel4.Controls.Add(this.btnview);
            this.panel4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(3, 252);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(569, 58);
            this.panel4.TabIndex = 7;
            // 
            // chkNormal
            // 
            this.chkNormal.AutoSize = true;
            this.chkNormal.Checked = true;
            this.chkNormal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNormal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNormal.Location = new System.Drawing.Point(22, 20);
            this.chkNormal.Name = "chkNormal";
            this.chkNormal.Size = new System.Drawing.Size(70, 18);
            this.chkNormal.TabIndex = 4;
            this.chkNormal.Text = "Normal";
            this.chkNormal.UseVisualStyleBackColor = true;
            this.chkNormal.Visible = false;
            // 
            // btnprint
            // 
            this.btnprint.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnprint.Location = new System.Drawing.Point(236, 14);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(94, 29);
            this.btnprint.TabIndex = 2;
            this.btnprint.Text = "&Print";
            this.btnprint.UseVisualStyleBackColor = true;
            this.btnprint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // btnreturn
            // 
            this.btnreturn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnreturn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnreturn.Location = new System.Drawing.Point(336, 14);
            this.btnreturn.Name = "btnreturn";
            this.btnreturn.Size = new System.Drawing.Size(94, 29);
            this.btnreturn.TabIndex = 3;
            this.btnreturn.Text = "&Return";
            this.btnreturn.UseVisualStyleBackColor = true;
            this.btnreturn.Click += new System.EventHandler(this.btnreturn_Click);
            // 
            // btnview
            // 
            this.btnview.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnview.Location = new System.Drawing.Point(136, 14);
            this.btnview.Name = "btnview";
            this.btnview.Size = new System.Drawing.Size(94, 29);
            this.btnview.TabIndex = 1;
            this.btnview.Text = "&View";
            this.btnview.UseVisualStyleBackColor = true;
            this.btnview.Click += new System.EventHandler(this.btnview_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.dtfromdate);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.dttodate);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(3, 93);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(569, 47);
            this.panel2.TabIndex = 5;
            // 
            // dtfromdate
            // 
            this.dtfromdate.CustomFormat = "dd/MM/yyyy";
            this.dtfromdate.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtfromdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtfromdate.Location = new System.Drawing.Point(103, 11);
            this.dtfromdate.Name = "dtfromdate";
            this.dtfromdate.Size = new System.Drawing.Size(138, 22);
            this.dtfromdate.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "From Date";
            // 
            // dttodate
            // 
            this.dttodate.CustomFormat = "dd/MM/yyyy";
            this.dttodate.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dttodate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dttodate.Location = new System.Drawing.Point(416, 11);
            this.dttodate.Name = "dttodate";
            this.dttodate.Size = new System.Drawing.Size(138, 22);
            this.dttodate.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(346, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 14);
            this.label6.TabIndex = 1;
            this.label6.Text = "To Date";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.dtdate);
            this.panel1.Controls.Add(this.txtpageno);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtgrouphead);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtacchead);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(569, 84);
            this.panel1.TabIndex = 4;
            // 
            // dtdate
            // 
            this.dtdate.CustomFormat = "dd/MM/yyyy";
            this.dtdate.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtdate.Location = new System.Drawing.Point(417, 15);
            this.dtdate.Name = "dtdate";
            this.dtdate.Size = new System.Drawing.Size(138, 22);
            this.dtdate.TabIndex = 3;
            // 
            // txtpageno
            // 
            this.txtpageno.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpageno.Location = new System.Drawing.Point(417, 49);
            this.txtpageno.Name = "txtpageno";
            this.txtpageno.Size = new System.Drawing.Size(138, 22);
            this.txtpageno.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Account";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "Group Head";
            // 
            // txtgrouphead
            // 
            this.txtgrouphead.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrouphead.Location = new System.Drawing.Point(104, 49);
            this.txtgrouphead.Name = "txtgrouphead";
            this.txtgrouphead.Size = new System.Drawing.Size(238, 22);
            this.txtgrouphead.TabIndex = 1;
            this.txtgrouphead.Enter += new System.EventHandler(this.txtgrouphead_Enter);
            this.txtgrouphead.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtgrouphead_KeyDown);
            this.txtgrouphead.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtgrouphead_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(347, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 14);
            this.label1.TabIndex = 2;
            this.label1.Text = "Date";
            // 
            // txtacchead
            // 
            this.txtacchead.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtacchead.Location = new System.Drawing.Point(104, 15);
            this.txtacchead.Name = "txtacchead";
            this.txtacchead.Size = new System.Drawing.Size(238, 22);
            this.txtacchead.TabIndex = 0;
            this.txtacchead.Enter += new System.EventHandler(this.txtacchead_Enter);
            this.txtacchead.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtacchead_KeyDown);
            this.txtacchead.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtacchead_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(347, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "Page No.";
            // 
            // ucLedgerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ucLedgerView";
            this.Size = new System.Drawing.Size(577, 317);
            this.Load += new System.EventHandler(this.ucLedgerView_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox chkpasschq;
        private System.Windows.Forms.RadioButton rddailybal;
        private System.Windows.Forms.RadioButton rdmonthbal;
        private System.Windows.Forms.CheckBox chkopbal;
        private System.Windows.Forms.RadioButton rdyearbal;
        private System.Windows.Forms.CheckBox chkprintled;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnprint;
        private System.Windows.Forms.Button btnview;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker dtfromdate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dttodate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtdate;
        private System.Windows.Forms.TextBox txtpageno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtgrouphead;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtacchead;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkNormal;
        public System.Windows.Forms.Button btnreturn;
    }
}
