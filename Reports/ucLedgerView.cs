using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Data.SqlClient;
using System.IO;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Reports
{
    public partial class ucLedgerView : UserControl
    {
        public ucLedgerView()
        {
            InitializeComponent();
        }

        cGeneral cgen = new cGeneral();
        SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);

        StreamWriter tstream;

        double TotDebit=0,TotCredit=0;
        int lineno;
        long PageNo;
        string date1 = "";
        int RptWidth;
        double opbalance;
        string SQL = "";

        SqlDataAdapter ap = new SqlDataAdapter();
        string strdata = "";
        Int32 j = 0;
        string PARTI = "";

        double amtt;
        string prev = "";
        DateTime cudate;
        string li1, li2, li3;
        
        private void ucLedgerView_Load(object sender, EventArgs e)
        {
            dtdate.Value = cPublic.g_Rdate;
            dtfromdate.Value = cPublic.g_OpDate;
            dttodate.Value = cPublic.g_Rdate;
            chkopbal.Checked = true;
            rdyearbal.Checked = true;
        }

        private void btnview_Click(object sender, EventArgs e)
        {
            string path = WriteReport();

            if (path != string.Empty)
            {
                Interaction.Shell("notepad.exe " + cPublic.FilePath + "\\" + path + ".txt", AppWinStyle.MaximizedFocus, true, 1);
            }
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            string path = WriteReport();
            cgen.RepPrint(path);
        }

        private void btnreturn_Click(object sender, EventArgs e)
        {
                this.Dispose ();            
        }

        private void txtacchead_Enter(object sender, EventArgs e)
        {
            if (!txtgrouphead.Text.Length.Equals(0))
                SendKeys.Send("{tab}");
            ((TextBox)(sender)).SelectAll();
        }

        private void txtacchead_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                SendKeys.Send("{tab}");
            }
            if (e.KeyCode.Equals(Keys.F5))
            {
                Lookup((TextBox )sender, null );
            }
        }

        private void txtacchead_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!e.KeyChar.Equals(Convert.ToChar(Keys.Enter)))
            {
                if (txtacchead.Text.Length.Equals(0))
                {
                    Lookup((TextBox)sender, e);
                }
            }
        }

        private void txtgrouphead_Enter(object sender, EventArgs e)
        {
            if (!txtacchead.Text.Length.Equals(0))
                SendKeys.Send("{tab}");
            ((TextBox)(sender)).SelectAll();
        }

        private void txtgrouphead_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                SendKeys.Send("{tab}");
            }
            if (e.KeyCode.Equals(Keys.F5))
            {
                Lookup((TextBox)sender, null);
            }
        }

        private void txtgrouphead_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!e.KeyChar.Equals(Convert.ToChar(Keys.Enter)))
            {
                if (txtgrouphead.Text.Length.Equals(0))
                {
                    Lookup((TextBox)sender, e);
                }
            }
        }


        private string WriteReport()
        {
            int G_Lperpage = 64;
            string code = "";
            string ccode = "";
            TotDebit = 0;
            TotCredit = 0;
            DataTable  dt = new DataTable();
            Int32 cumonth = 0;
            if (txtpageno.Text == "") { txtpageno.Text = "0"; }
            PageNo = Convert.ToInt32(txtpageno.Text);
            date1 = cPublic.g_Rdate.ToString();
            opbalance = 0;
            RptWidth = 80;
            lineno = 1;
            if (txtacchead.Text.Trim() == string.Empty && txtgrouphead.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Select a Head ,Group or Agent !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtacchead.Focus();
            }

            if (txtacchead.Text != "")
            {
                code = txtacchead.Tag.ToString();
            }
            else if (txtgrouphead.Text != "")
            {
                code = txtgrouphead.Tag + "".ToString();
            }


            string notepadpath =Convert.ToString  (Convert.ToInt32(1000 * VBMath.Rnd()) + 1);

            if (File.Exists(cPublic.FilePath + "\\" + notepadpath + ".txt"))
            {
                File.Delete(cPublic.FilePath + "\\" + notepadpath + ".txt");
            }


            string path = cPublic.FilePath + "\\" + notepadpath + ".txt";
            tstream = File.CreateText(path);

            for (int i = 1; i < Convert.ToInt32(cPublic.LinestoReverse); i++)
            {//  lines to reverse
                tstream.Write(Strings.Chr(27) + "j" + Strings.Chr(36));
            }

            pageheadledger();

            cgen.FindHeads(code);
            bool sk = true;
            for (int i = 0; i < cPublic.AccNos; i++)
            {
                TotDebit = 0;
                TotCredit = 0;
                if (!chkopbal.Checked) { opbalance = 0; }
                else
                {
                    if (cPublic.AccArray[i + 1, 3] + "".ToString() != "")
                        opbalance = Convert.ToDouble(cPublic.AccArray[i + 1, 3].ToString());
                }

                ccode = cPublic.AccArray[i + 1, 1];
                opbalance = 0;

                cmd.Parameters.Clear();
                cmd.CommandText="SP_GET_OPBAL" + cPublic.g_firmcode;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@code", ccode);
                cmd.Parameters.AddWithValue("@date", dtfromdate.Value.ToString("MM/dd/yyyy"));
                SqlDataReader dr1 = cmd.ExecuteReader();
                while (dr1.Read())
                {
                    opbalance = Convert.ToDouble(dr1[0]);
                }
                dr1.Close();

                if (!chkpasschq.Checked)
                {
                    SQL = "Select DATE1,BOOKCODE,CODE,(select head from accounts" + cPublic.g_firmcode + " where code=case when a.bookcode='000000' then a.code else a.bookcode end ) HEAD, AMOUNT,CHEQUE,TRANSTYPE,TRANSNO,CHQNO,CHQDATE,"
                    + " CHQPASSED,NARRATION,BILLNO from VOUCHER" + cPublic.g_firmcode + " A "
                    + " where (Date1 >= '" + dtfromdate.Value.ToString("yyyy/MM/dd") + "' and  Date1 <= '" + dttodate.Value.ToString("yyyy/MM/dd") + "') and CODE = '"
                    + ccode + "' order by date1,transtype,transno";
                }
                else
                {
                    SQL = "Select DATE1,BOOKCODE,CODE,(select head from accounts" + cPublic.g_firmcode + " where code=case when a.bookcode='000000' then a.code else a.bookcode end ) HEAD, AMOUNT,CHEQUE,TRANSTYPE,TRANSNO,CHQNO,CHQDATE,CHQPASSDATE,"
                    + " CHQPASSED,NARRATION,BILLNO from VOUCHER" + cPublic.g_firmcode + " A "
                    + " where CODE='" + ccode + "' AND ((Date1 >= '" + dtfromdate.Value.ToString("yyyy/MM/dd") + "' and  date1  <= '" + dttodate.Value.ToString("yyyy/MM/dd") + "' AND bookcode <= '200002' ) OR "
                    + " (chqpassdate >= '" + dtfromdate.Value.ToString("yyyy/MM/dd") + "' and  chqpassdate <= '" + dttodate.Value.ToString("yyyy/MM/dd") + "'  AND bookcode > '200002' AND chqpassed= 1) ) "
                    + "   order by date1,transtype,transno";
                }

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text; 
                cmd.CommandText  = SQL;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count == 0)
                {
                    sk = true;
                }
                else
                {
                    sk = false;
                }
                j = 0;
                if (dt.Rows.Count == 0 && opbalance == 0)
                {
                    if (dt.Rows.Count == 0)
                    {
                        if (lineno >= G_Lperpage + 5)
                        {
                            printtot("");
                            tstream.WriteLine("  " + cPublic.Ch12);
                            lineno = 1;
                            for (int nn = 0; nn < 11; nn++)
                            {
                                tstream.WriteLine();
                            }
                            pageheadledger();
                            ledgerhead(i);
                        }
                        else
                        {
                            ledgerhead(i);
                        }
                    }
                    if (sk == true)
                    {
                        printtot("");
                        printbalance("");
                    }


                    goto aa;
                }
                if (cPublic.AccArray[i + 1, 5].ToString() == "200000") { goto aa; }


                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToDateTime(Convert.ToDateTime(dt.Rows[j]["date1"]).ToShortDateString()) >= Convert.ToDateTime(dtfromdate.Value.ToShortDateString()))
                    {
                        if (lineno >= G_Lperpage + 5)
                        {
                            printtot("");
                            tstream.WriteLine("  " + cPublic.Ch12);
                            lineno = 1;
                            for (int nn = 0; nn < 11; nn++)
                            {
                                tstream.WriteLine();
                            }
                            pageheadledger();
                            ledgerhead(i);
                        }
                        else
                        {
                            ledgerhead(i);
                        }
                        //break;
                    }
                    /////////////////opbalance += Convert.ToDouble(dt.Rows[j]["AMOUNT"].ToString());
                }
                if (j != dt.Rows.Count)
                {
                    cudate = Convert.ToDateTime(dt.Rows[j]["date1"].ToString());
                    cumonth = Convert.ToDateTime(dt.Rows[j]["date1"]).Month;
                }

                for (j = 0; j < dt.Rows.Count; j++)
                {
                    if (lineno >= G_Lperpage)
                    {
                        printtot("");
                        tstream.WriteLine("  " + cPublic.Ch12);
                        lineno = 1;
                        for (int nn = 0; nn < 11; nn++)
                        {
                            tstream.WriteLine();
                        }
                        pageheadledger();
                        ledgerhead(i);
                    }
                    if ((cudate.ToShortDateString() != Convert.ToDateTime(dt.Rows[j]["date1"]).ToShortDateString()) && rddailybal.Checked)
                    {
                        cudate = Convert.ToDateTime(dt.Rows[j]["date1"].ToString());
                        printtot("");
                        printbalance("");
                    }
                    if (cumonth != Convert.ToDateTime(dt.Rows[j]["date1"]).Month && rdmonthbal.Checked)
                    {
                        cumonth = Convert.ToDateTime(dt.Rows[j]["date1"]).Month;
                        printtot("");
                        printbalance("");
                    }
                    oneline(dt);
                }


                if (dt.Rows.Count == 0)
                {
                    ledgerhead(i);
                }

                if (sk == true)
                {
                    printtot("");
                    printbalance("");
                }




                if (sk == false)
                {
                    printtot("");
                    printbalance("");
                }
                lineno += 2;
            aa:




                TotDebit = 0;
                TotCredit = 0;
            }

            //if (!chkNormal.Checked)
            //{
            //for (j = 0; j < Convert.ToInt32(cPublic.LinestoSkip) + 5; j++)
            //    tstream.WriteLine();
            //} 
            for (int l = 0; l < 20; l++)
            {
                tstream.WriteLine();
            }

            tstream.Flush();
            tstream.Close();
            j = 0;
            dt.Rows.Clear();

            return notepadpath;
        }

        public void ledgerhead(int i)
        {
            if (lineno == 5)
            {
                tstream.WriteLine("|========|=======|=====================================|===========|===========|");
            }
            tstream.Write("|" + Strings.Left(cPublic.AccArray[i + 1, 2] + Strings.Space(100), 54));
            tstream.Write("|");
            if (opbalance < 0)
            {
                strdata = Strings.FormatNumber(Math.Abs(opbalance), 2, TriState.True, TriState.True, TriState.True);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                tstream.Write("|");
                strdata = Strings.Space(13);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
            }
            else
            {
                strdata = Strings.Space(13);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                tstream.Write("|");
                strdata = Strings.FormatNumber(Math.Abs(opbalance), 2, TriState.True, TriState.True, TriState.True);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
            }
            tstream.Write("|");
            // tstream.Write(cgen.FnAlignString(Strings.Space(14), "R", 14));
            //tstream.WriteLine ("|");
            tstream.WriteLine();

            tstream.WriteLine("|========|=======|=====================================|===========|===========|");
            tstream.WriteLine("|Date    |Reff.  |  Particulars                        |    Debit  |  Credit   |");
            tstream.WriteLine("|--------|-------|-------------------------------------|-----------|-----------|");
            lineno = lineno + 5;
        }

        public void oneline(DataTable dt)
        {

            PARTI = "";//dt.Rows[j]["date1"]
            amtt = Convert.ToDouble(dt.Rows[j]["Amount"]);
            prev = Microsoft.VisualBasic.Interaction.IIf(amtt > 0, "By", "To") + "";

            PARTI = dt.Rows[j]["head"] + "".ToString().Trim();


            if (dt.Rows[j]["chqno"] + "".ToString().Trim() != "")
            {
                if (dt.Rows[j]["chqdate"].ToString() == "")
                {
                    PARTI = dt.Rows[j]["Narration"] + "".ToString().Trim() + " Chqno: " + dt.Rows[j]["chqno"] + "".ToString().Trim() + Strings.Space(250);
                }
                else
                {
                    PARTI = dt.Rows[j]["Narration"] + "".ToString().Trim() + " Chqno: " + dt.Rows[j]["chqno"] + "".ToString().Trim() + " Chqdt:" + Convert.ToDateTime(dt.Rows[j]["chqdate"]).ToString("dd/MM/yyyy") + Strings.Space(250);
                }
            }
            else
            {
                PARTI = dt.Rows[j]["Narration"] + "".ToString().Trim();
            }

            li1 = Strings.Mid(PARTI, 1, 33);
            li2 = Strings.Mid(PARTI, 34, 33);
            li3 = Strings.Mid(PARTI, 67, 33);
            tstream.Write("|");
            if (chkpasschq.Checked == true && dt.Rows[j]["CHQPASSDATE"].Equals(DBNull.Value) == false)
            { strdata = Convert.ToDateTime(dt.Rows[j]["CHQPASSDATE"] + "").ToString("dd/MM/yy"); }
            else
            { strdata = Convert.ToDateTime(dt.Rows[j]["date1"] + "").ToString("dd/MM/yy"); }

            tstream.Write(cgen.FnAlignString(strdata, "L", 8));
            tstream.Write("|");
            strdata = dt.Rows[j]["transtype"].ToString().Trim() + Strings.Left(dt.Rows[j]["TransNo"].ToString().Trim(), 7);
            tstream.Write(cgen.FnAlignString(strdata, "L", 7));
            tstream.Write("|");
            tstream.Write(cgen.FnAlignString(prev, "L", 3));
            if (li1 != "")
            { tstream.Write(cgen.FnAlignString(li1, "L", 33)); }
            else
            { tstream.Write(cgen.FnAlignString(Strings.Space(1), "L", 33)); }
            if (Strings.Right(li1, 1) != " " && Strings.Left(li2, 1) != " ")
            { tstream.Write("-"); }
            else
            { tstream.Write(" "); }
            tstream.Write("|");
            if (amtt < 0)
            {
                strdata = Strings.FormatNumber(Math.Abs(amtt), 2, TriState.True, TriState.True, TriState.True);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                tstream.Write("|");
                strdata = Strings.Space(11);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                TotDebit = TotDebit + amtt;
            }
            else
            {
                strdata = Strings.Space(11);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                tstream.Write("|");
                strdata = Strings.FormatNumber(Math.Abs(amtt), 2, TriState.True, TriState.True, TriState.True);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                TotCredit = TotCredit + amtt;

            }
            tstream.Write("|");

            if (li2.Trim() != "")// ' && third line is needed
            {
                tstream.WriteLine();
                tstream.Write("|");
                lineno = lineno + 1;
                tstream.Write(Strings.Space(8));
                tstream.Write("|");
                tstream.Write(Strings.Space(7));
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString("   ", "L", 3));
                tstream.Write(cgen.FnAlignString(li2, "L", 33));
                if (Strings.Right(li2, 1) != " " && Strings.Left(li3, 1) != " ")
                {
                    tstream.Write("-");
                }
                else
                {
                    tstream.Write(" ");
                }

                tstream.Write("|");
                tstream.Write(cgen.FnAlignString(Strings.Space(11), "R", 11));
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString(Strings.Space(11), "R", 11));
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString(Strings.Space(13), "R", 14));
                //tstream.Write("|");
            }
            if (li3.Trim() != "")// ' && third line is needed
            {
                tstream.WriteLine();
                tstream.Write("|");
                lineno = lineno + 1;
                tstream.Write(Strings.Space(8));
                tstream.Write("|");
                tstream.Write(Strings.Space(7));
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString("   ", "L", 3));
                tstream.Write(cgen.FnAlignString(li3, "L", 33));
                if (Strings.Right(li3, 1) != " " )
                {
                    tstream.Write("-");
                }
                else
                {
                    tstream.Write(" ");
                }

                tstream.Write("|");
                tstream.Write(cgen.FnAlignString(Strings.Space(13), "R", 11));
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString(Strings.Space(13), "R", 11));
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString(Strings.Space(13), "R", 14));
                //tstream.Write("|");
            }
            tstream.WriteLine();
            lineno = lineno + 1;

        }
        private void printtot(string prev)
        {
            if (TotCredit != 0 || TotDebit != 0)
            {
                tstream.WriteLine("|--------|-------|-------------------------------------|-----------|-----------|");
            }
            tstream.Write("|        |       | ");
            strdata = prev + "Total";
            tstream.Write(cgen.FnAlignString(strdata, "L", 24));
            tstream.Write(Strings.Space(12));
            tstream.Write("|");
            strdata = Strings.FormatNumber(Math.Abs(TotDebit), 2, TriState.True, TriState.True, TriState.True);
            tstream.Write(cgen.FnAlignString(strdata, "R", 11));
            tstream.Write("|");
            strdata = Strings.FormatNumber(Math.Abs(TotCredit), 2, TriState.True, TriState.True, TriState.True);
            tstream.Write(cgen.FnAlignString(strdata, "R", 11));
            tstream.Write("|");
            // tstream.Write(cgen.FnAlignString(Strings. Space(14), "R", 14));
            tstream.WriteLine();
            //tstream.WriteLine("|");
            tstream.WriteLine("|========|=======|=====================================|===========|===========|");

            lineno = lineno + 3;
            opbalance = opbalance + TotDebit + TotCredit;
            TotDebit = 0;
            TotCredit = 0;
        }

        private void printbalance(string prev)
        {
            tstream.Write("|        |       | ");
            strdata = prev + "Closing Balance";
            tstream.Write(cgen.FnAlignString(strdata, "L", 30));
            tstream.Write(Strings.Space(6));
            tstream.Write("|");
            if (opbalance < 0)
            {
                strdata = Strings.FormatNumber((Math.Abs(opbalance)), 2, TriState.True, TriState.True, TriState.True);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                tstream.Write("|");
                strdata = Strings.FormatNumber((Math.Abs(0)), 2, TriState.True, TriState.True, TriState.True);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                tstream.Write("|");
            }
            else
            {
                strdata = Strings.FormatNumber((Math.Abs(0)), 2, TriState.True, TriState.True, TriState.True);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                tstream.Write("|");
                strdata = Strings.FormatNumber((Math.Abs(opbalance)), 2, TriState.True, TriState.True, TriState.True);
                tstream.Write(cgen.FnAlignString(strdata, "R", 11));
                tstream.Write("|");
            }
            // tstream.Write (cgen.FnAlignString(Strings. Space(14), "R", 14));
            //tstream.WriteLine ("|");
            tstream.WriteLine();
            lineno = lineno + 1;
            tstream.WriteLine("|========|=======|=====================================|===========|===========|");
            lineno = lineno + 1;
        }

        public void pageheadledger()
        {
            string X = "";
            tstream.WriteLine();
            lineno = lineno + 1;
            if (chkNormal.Checked == true)
            {
                tstream.Write(cPublic.ChN);
            }
            else
            {
                tstream.Write(cPublic.ChC);
            }

            if (Strings.Right(cPublic.g_DispName, 1) != "*")
            {
                tstream.Write(cPublic.BchDW);
                tstream.WriteLine(cgen.FnAlignStringlarge(cPublic.g_firmname, 80));
                tstream.Write(cPublic.BchNDW);
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add1 + ", " + cPublic.g_add2, "C", 75));
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add3 + ", " + cPublic.g_add4, "C", 75));

                if (cPublic.g_TinNo.StartsWith("T") == true) { tstream.WriteLine(cgen.FnAlignString("Tin No:" + cPublic.g_TinNo.Substring(2), "C", 75)); }
                else if (cPublic.g_TinNo.StartsWith("P") == true) { tstream.WriteLine(cgen.FnAlignString("Pin No:" + cPublic.g_TinNo.Substring(2), "C", 75)); }
                else { tstream.WriteLine(cgen.FnAlignString("Tin No:" + cPublic.g_TinNo, "C", 75)); }

                lineno = lineno + 1;

            }
            X = "Ledger   For   The   Year   Ended " + dtdate.Value.ToString("dd/MM/yyyy");

            tstream.Write(cgen.FnAlignString(X, "C", RptWidth));
            tstream.WriteLine();
            lineno = lineno + 1;
            tstream.Write("Date:");
            tstream.Write(dtdate.Value.ToString("dd/MM/yyyy"));
            tstream.Write(Strings.Space(49));
            tstream.Write("Page No.");
            strdata = Convert.ToString(PageNo);

            tstream.Write(cgen.FnAlignString(strdata, "R", 5));
            tstream.WriteLine();
            lineno = lineno + 1;
            PageNo = PageNo + 1;

        }

        private void Lookup(TextBox tbs, KeyPressEventArgs  e)
        {
            frmlookup lnew = new frmlookup();
            if (tbs == txtacchead)
            {
                lnew.m_table = "accounts" + cPublic.g_firmcode;
                lnew.m_fields = "head,ADDRESS, STREET, CITY,code,balance";
                lnew.m_dispname = "HEAD,ADDRESS, STREET, CITY,CODE,BALANCE";
                lnew.m_fldwidth = "300,100,100,100,0,100";
                lnew.m_condition = "groupcode<>'200000'";
            }
            else
            {
                lnew.m_fields = "head,code";
                lnew.m_table = "groups" + cPublic.g_firmcode;
                lnew.m_dispname = "HEAD,CODE";
                lnew.m_fldwidth = "500,0";
            }
            lnew.m_order = "";
            if (e != null) { lnew.m_strat = Convert.ToString(e.KeyChar); }
            lnew.ShowDialog();
            if (lnew.m_values.Count > 0)
            {
                if (tbs == txtacchead)
                {
                    txtacchead.Text = lnew.m_values[0].ToString();
                    txtacchead.Tag = lnew.m_values[4].ToString();

                    txtgrouphead.Clear();
                }
                else
                {
                    txtgrouphead.Text = lnew.m_values[0].ToString();
                    txtgrouphead.Tag = lnew.m_values[1].ToString();

                    txtacchead.Clear();
                }
                SendKeys.Send("{tab}");
            }
        }

    }
}
