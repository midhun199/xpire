namespace Xpire.Reports
{
    partial class ucSchedule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucSchedule));
            this.cmbbalancetype = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chqPass = new System.Windows.Forms.CheckBox();
            this.cmbbalance = new System.Windows.Forms.ComboBox();
            this.chkupto = new System.Windows.Forms.CheckBox();
            this.dtpupto = new System.Windows.Forms.DateTimePicker();
            this.dtpfrom = new System.Windows.Forms.DateTimePicker();
            this.dtpto = new System.Windows.Forms.DateTimePicker();
            this.lblto = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblfrom = new System.Windows.Forms.Label();
            this.radDatewise = new System.Windows.Forms.RadioButton();
            this.radCurrentBalance = new System.Windows.Forms.RadioButton();
            this.rdbopbalance = new System.Windows.Forms.RadioButton();
            this.txtGroup = new System.Windows.Forms.TextBox();
            this.txtAccounts = new System.Windows.Forms.TextBox();
            this.lblgrouphead = new System.Windows.Forms.Label();
            this.lblacchead = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.cfgday = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.cfgday)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbbalancetype
            // 
            this.cmbbalancetype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbbalancetype.FormattingEnabled = true;
            this.cmbbalancetype.ItemHeight = 13;
            this.cmbbalancetype.Items.AddRange(new object[] {
            "ALL",
            "DEBIT",
            "CREDIT"});
            this.cmbbalancetype.Location = new System.Drawing.Point(502, 68);
            this.cmbbalancetype.Name = "cmbbalancetype";
            this.cmbbalancetype.Size = new System.Drawing.Size(103, 21);
            this.cmbbalancetype.TabIndex = 37;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(441, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 39;
            this.label7.Text = "Balance";
            // 
            // chqPass
            // 
            this.chqPass.AutoSize = true;
            this.chqPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.chqPass.Location = new System.Drawing.Point(351, 100);
            this.chqPass.Name = "chqPass";
            this.chqPass.Size = new System.Drawing.Size(114, 17);
            this.chqPass.TabIndex = 38;
            this.chqPass.Text = "Cheque Passed";
            this.chqPass.UseVisualStyleBackColor = true;
            // 
            // cmbbalance
            // 
            this.cmbbalance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbbalance.FormattingEnabled = true;
            this.cmbbalance.ItemHeight = 13;
            this.cmbbalance.Items.AddRange(new object[] {
            "Y",
            "N"});
            this.cmbbalance.Location = new System.Drawing.Point(559, 36);
            this.cmbbalance.Name = "cmbbalance";
            this.cmbbalance.Size = new System.Drawing.Size(46, 21);
            this.cmbbalance.TabIndex = 36;
            // 
            // chkupto
            // 
            this.chkupto.AutoSize = true;
            this.chkupto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.chkupto.Location = new System.Drawing.Point(437, 13);
            this.chkupto.Name = "chkupto";
            this.chkupto.Size = new System.Drawing.Size(57, 17);
            this.chkupto.TabIndex = 32;
            this.chkupto.Text = "Up to";
            this.chkupto.UseVisualStyleBackColor = true;
            this.chkupto.CheckedChanged += new System.EventHandler(this.chkupto_CheckedChanged);
            // 
            // dtpupto
            // 
            this.dtpupto.CustomFormat = "dd/MM/yyyy";
            this.dtpupto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpupto.Location = new System.Drawing.Point(502, 10);
            this.dtpupto.Name = "dtpupto";
            this.dtpupto.Size = new System.Drawing.Size(103, 20);
            this.dtpupto.TabIndex = 35;
            // 
            // dtpfrom
            // 
            this.dtpfrom.CustomFormat = "dd/MM/yyyy";
            this.dtpfrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfrom.Location = new System.Drawing.Point(69, 98);
            this.dtpfrom.Name = "dtpfrom";
            this.dtpfrom.Size = new System.Drawing.Size(103, 20);
            this.dtpfrom.TabIndex = 33;
            // 
            // dtpto
            // 
            this.dtpto.CustomFormat = "dd/MM/yyyy";
            this.dtpto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpto.Location = new System.Drawing.Point(221, 98);
            this.dtpto.Name = "dtpto";
            this.dtpto.Size = new System.Drawing.Size(103, 20);
            this.dtpto.TabIndex = 34;
            // 
            // lblto
            // 
            this.lblto.AutoSize = true;
            this.lblto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblto.Location = new System.Drawing.Point(193, 102);
            this.lblto.Name = "lblto";
            this.lblto.Size = new System.Drawing.Size(22, 13);
            this.lblto.TabIndex = 31;
            this.lblto.Text = "To";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(410, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Head with Balance Only";
            // 
            // lblfrom
            // 
            this.lblfrom.AutoSize = true;
            this.lblfrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblfrom.Location = new System.Drawing.Point(6, 102);
            this.lblfrom.Name = "lblfrom";
            this.lblfrom.Size = new System.Drawing.Size(34, 13);
            this.lblfrom.TabIndex = 29;
            this.lblfrom.Text = "From";
            // 
            // radDatewise
            // 
            this.radDatewise.AutoSize = true;
            this.radDatewise.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.radDatewise.Location = new System.Drawing.Point(327, 69);
            this.radDatewise.Name = "radDatewise";
            this.radDatewise.Size = new System.Drawing.Size(77, 17);
            this.radDatewise.TabIndex = 30;
            this.radDatewise.Text = "Datewise";
            this.radDatewise.UseVisualStyleBackColor = true;
            this.radDatewise.CheckedChanged += new System.EventHandler(this.radDatewise_CheckedChanged);
            // 
            // radCurrentBalance
            // 
            this.radCurrentBalance.AutoSize = true;
            this.radCurrentBalance.Checked = true;
            this.radCurrentBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.radCurrentBalance.Location = new System.Drawing.Point(162, 69);
            this.radCurrentBalance.Name = "radCurrentBalance";
            this.radCurrentBalance.Size = new System.Drawing.Size(116, 17);
            this.radCurrentBalance.TabIndex = 27;
            this.radCurrentBalance.TabStop = true;
            this.radCurrentBalance.Text = "Current Balance";
            this.radCurrentBalance.UseVisualStyleBackColor = true;
            // 
            // rdbopbalance
            // 
            this.rdbopbalance.AutoSize = true;
            this.rdbopbalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.rdbopbalance.Location = new System.Drawing.Point(9, 69);
            this.rdbopbalance.Name = "rdbopbalance";
            this.rdbopbalance.Size = new System.Drawing.Size(122, 17);
            this.rdbopbalance.TabIndex = 26;
            this.rdbopbalance.Text = "Opening Balance";
            this.rdbopbalance.UseVisualStyleBackColor = true;
            // 
            // txtGroup
            // 
            this.txtGroup.BackColor = System.Drawing.Color.White;
            this.txtGroup.ForeColor = System.Drawing.Color.Black;
            this.txtGroup.Location = new System.Drawing.Point(92, 10);
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Size = new System.Drawing.Size(312, 20);
            this.txtGroup.TabIndex = 22;
            this.txtGroup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGroup_KeyDown);
            this.txtGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGroup_KeyPress);
            // 
            // txtAccounts
            // 
            this.txtAccounts.BackColor = System.Drawing.Color.White;
            this.txtAccounts.ForeColor = System.Drawing.Color.Black;
            this.txtAccounts.Location = new System.Drawing.Point(92, 36);
            this.txtAccounts.Name = "txtAccounts";
            this.txtAccounts.Size = new System.Drawing.Size(312, 20);
            this.txtAccounts.TabIndex = 25;
            this.txtAccounts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAccounts_KeyDown);
            this.txtAccounts.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAccounts_KeyPress);
            // 
            // lblgrouphead
            // 
            this.lblgrouphead.AutoSize = true;
            this.lblgrouphead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblgrouphead.Location = new System.Drawing.Point(4, 14);
            this.lblgrouphead.Name = "lblgrouphead";
            this.lblgrouphead.Size = new System.Drawing.Size(75, 13);
            this.lblgrouphead.TabIndex = 24;
            this.lblgrouphead.Text = "Group Head";
            // 
            // lblacchead
            // 
            this.lblacchead.AutoSize = true;
            this.lblacchead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblacchead.Location = new System.Drawing.Point(4, 39);
            this.lblacchead.Name = "lblacchead";
            this.lblacchead.Size = new System.Drawing.Size(60, 13);
            this.lblacchead.TabIndex = 23;
            this.lblacchead.Text = "Accounts";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(515, 95);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(90, 26);
            this.btnLoad.TabIndex = 40;
            this.btnLoad.Text = "&Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // cfgday
            // 
            this.cfgday.AllowEditing = false;
            this.cfgday.AllowFreezing = C1.Win.C1FlexGrid.AllowFreezingEnum.Both;
            this.cfgday.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.cfgday.BackColor = System.Drawing.SystemColors.Window;
            this.cfgday.ColumnInfo = resources.GetString("cfgday.ColumnInfo");
            this.cfgday.ExtendLastCol = true;
            this.cfgday.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.cfgday.Location = new System.Drawing.Point(3, 127);
            this.cfgday.Name = "cfgday";
            this.cfgday.Rows.MinSize = 22;
            this.cfgday.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.cfgday.Size = new System.Drawing.Size(795, 433);
            this.cfgday.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("cfgday.Styles"));
            this.cfgday.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
            this.cfgday.TabIndex = 41;
            this.cfgday.TabStop = false;
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPrint.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPrint.Location = new System.Drawing.Point(409, 566);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 32);
            this.btnPrint.TabIndex = 45;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.White;
            this.btnView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnView.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnView.Location = new System.Drawing.Point(490, 566);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(88, 32);
            this.btnView.TabIndex = 44;
            this.btnView.Text = "&View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.White;
            this.btnExport.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnExport.Location = new System.Drawing.Point(584, 566);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(132, 32);
            this.btnExport.TabIndex = 43;
            this.btnExport.Text = "&Export - Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnClose.Location = new System.Drawing.Point(723, 566);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 42;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ucSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.cfgday);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.cmbbalancetype);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.chqPass);
            this.Controls.Add(this.cmbbalance);
            this.Controls.Add(this.chkupto);
            this.Controls.Add(this.dtpupto);
            this.Controls.Add(this.dtpfrom);
            this.Controls.Add(this.dtpto);
            this.Controls.Add(this.lblto);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblfrom);
            this.Controls.Add(this.radDatewise);
            this.Controls.Add(this.radCurrentBalance);
            this.Controls.Add(this.rdbopbalance);
            this.Controls.Add(this.txtGroup);
            this.Controls.Add(this.txtAccounts);
            this.Controls.Add(this.lblgrouphead);
            this.Controls.Add(this.lblacchead);
            this.Name = "ucSchedule";
            this.Size = new System.Drawing.Size(807, 606);
            this.Load += new System.EventHandler(this.ucSchedule_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cfgday)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbbalancetype;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chqPass;
        private System.Windows.Forms.ComboBox cmbbalance;
        private System.Windows.Forms.CheckBox chkupto;
        private System.Windows.Forms.DateTimePicker dtpupto;
        private System.Windows.Forms.DateTimePicker dtpfrom;
        private System.Windows.Forms.DateTimePicker dtpto;
        private System.Windows.Forms.Label lblto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblfrom;
        private System.Windows.Forms.RadioButton radDatewise;
        private System.Windows.Forms.RadioButton radCurrentBalance;
        private System.Windows.Forms.RadioButton rdbopbalance;
        private System.Windows.Forms.TextBox txtGroup;
        private System.Windows.Forms.TextBox txtAccounts;
        private System.Windows.Forms.Label lblgrouphead;
        private System.Windows.Forms.Label lblacchead;
        private System.Windows.Forms.Button btnLoad;
        public C1.Win.C1FlexGrid.C1FlexGrid cfgday;
        private System.Windows.Forms.Button btnPrint;
        public System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnExport;
        public System.Windows.Forms.Button btnClose;
    }
}
