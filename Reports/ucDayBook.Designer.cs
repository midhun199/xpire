namespace Xpire.Reports
{
    partial class ucDayBook
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDayBook));
            this.btnView = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lblcrdr = new System.Windows.Forms.Label();
            this.pnlbalance = new System.Windows.Forms.Panel();
            this.cfgday = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.lblopbvalance = new System.Windows.Forms.Label();
            this.chkchqpass = new System.Windows.Forms.CheckBox();
            this.rddailybal = new System.Windows.Forms.RadioButton();
            this.rdmothbal = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.rdyearbal = new System.Windows.Forms.RadioButton();
            this.cmbBooktype = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtfrdate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dttodate = new System.Windows.Forms.DateTimePicker();
            this.btnLoad = new System.Windows.Forms.Button();
            this.pnlbalance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cfgday)).BeginInit();
            this.SuspendLayout();
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.White;
            this.btnView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnView.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnView.Location = new System.Drawing.Point(486, 564);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(98, 32);
            this.btnView.TabIndex = 2;
            this.btnView.Text = "&View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnClose.Location = new System.Drawing.Point(726, 564);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.White;
            this.btnExport.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnExport.Location = new System.Drawing.Point(590, 564);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(130, 32);
            this.btnExport.TabIndex = 3;
            this.btnExport.Text = "&Export - Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPrint.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPrint.Location = new System.Drawing.Point(405, 564);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 32);
            this.btnPrint.TabIndex = 1;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Opening Balance";
            // 
            // lblcrdr
            // 
            this.lblcrdr.AutoSize = true;
            this.lblcrdr.BackColor = System.Drawing.Color.White;
            this.lblcrdr.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcrdr.ForeColor = System.Drawing.Color.Maroon;
            this.lblcrdr.Location = new System.Drawing.Point(301, 8);
            this.lblcrdr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblcrdr.Name = "lblcrdr";
            this.lblcrdr.Size = new System.Drawing.Size(0, 14);
            this.lblcrdr.TabIndex = 1;
            this.lblcrdr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlbalance
            // 
            this.pnlbalance.BackColor = System.Drawing.Color.Transparent;
            this.pnlbalance.Controls.Add(this.label3);
            this.pnlbalance.Controls.Add(this.lblcrdr);
            this.pnlbalance.Location = new System.Drawing.Point(164, 108);
            this.pnlbalance.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pnlbalance.Name = "pnlbalance";
            this.pnlbalance.Size = new System.Drawing.Size(334, 29);
            this.pnlbalance.TabIndex = 7;
            // 
            // cfgday
            // 
            this.cfgday.AllowEditing = false;
            this.cfgday.AllowFreezing = C1.Win.C1FlexGrid.AllowFreezingEnum.Both;
            this.cfgday.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.cfgday.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.cfgday.BackColor = System.Drawing.SystemColors.Window;
            this.cfgday.ColumnInfo = resources.GetString("cfgday.ColumnInfo");
            this.cfgday.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.cfgday.Location = new System.Drawing.Point(3, 143);
            this.cfgday.Name = "cfgday";
            this.cfgday.Rows.MinSize = 22;
            this.cfgday.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.cfgday.Size = new System.Drawing.Size(798, 415);
            this.cfgday.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("cfgday.Styles"));
            this.cfgday.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
            this.cfgday.TabIndex = 5;
            this.cfgday.TabStop = false;
            // 
            // lblopbvalance
            // 
            this.lblopbvalance.BackColor = System.Drawing.Color.Transparent;
            this.lblopbvalance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblopbvalance.ForeColor = System.Drawing.Color.Maroon;
            this.lblopbvalance.Location = new System.Drawing.Point(646, 115);
            this.lblopbvalance.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblopbvalance.Name = "lblopbvalance";
            this.lblopbvalance.Size = new System.Drawing.Size(111, 17);
            this.lblopbvalance.TabIndex = 8;
            this.lblopbvalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkchqpass
            // 
            this.chkchqpass.AutoSize = true;
            this.chkchqpass.BackColor = System.Drawing.Color.Transparent;
            this.chkchqpass.Checked = true;
            this.chkchqpass.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkchqpass.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkchqpass.Location = new System.Drawing.Point(481, 13);
            this.chkchqpass.Name = "chkchqpass";
            this.chkchqpass.Size = new System.Drawing.Size(164, 18);
            this.chkchqpass.TabIndex = 23;
            this.chkchqpass.Text = "Passed Cheques Only";
            this.chkchqpass.UseVisualStyleBackColor = false;
            // 
            // rddailybal
            // 
            this.rddailybal.AutoSize = true;
            this.rddailybal.BackColor = System.Drawing.Color.Transparent;
            this.rddailybal.Checked = true;
            this.rddailybal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rddailybal.Location = new System.Drawing.Point(24, 72);
            this.rddailybal.Name = "rddailybal";
            this.rddailybal.Size = new System.Drawing.Size(108, 18);
            this.rddailybal.TabIndex = 24;
            this.rddailybal.TabStop = true;
            this.rddailybal.Text = "Daily Balance";
            this.rddailybal.UseVisualStyleBackColor = false;
            // 
            // rdmothbal
            // 
            this.rdmothbal.AutoSize = true;
            this.rdmothbal.BackColor = System.Drawing.Color.Transparent;
            this.rdmothbal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdmothbal.Location = new System.Drawing.Point(153, 72);
            this.rdmothbal.Name = "rdmothbal";
            this.rdmothbal.Size = new System.Drawing.Size(127, 18);
            this.rdmothbal.TabIndex = 25;
            this.rdmothbal.Text = "Monthly Balance";
            this.rdmothbal.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 14);
            this.label5.TabIndex = 27;
            this.label5.Text = "Book Type";
            // 
            // rdyearbal
            // 
            this.rdyearbal.AutoSize = true;
            this.rdyearbal.BackColor = System.Drawing.Color.Transparent;
            this.rdyearbal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdyearbal.Location = new System.Drawing.Point(311, 72);
            this.rdyearbal.Name = "rdyearbal";
            this.rdyearbal.Size = new System.Drawing.Size(116, 18);
            this.rdyearbal.TabIndex = 26;
            this.rdyearbal.Text = "Yearly Balance";
            this.rdyearbal.UseVisualStyleBackColor = false;
            // 
            // cmbBooktype
            // 
            this.cmbBooktype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBooktype.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBooktype.FormattingEnabled = true;
            this.cmbBooktype.Location = new System.Drawing.Point(110, 9);
            this.cmbBooktype.Name = "cmbBooktype";
            this.cmbBooktype.Size = new System.Drawing.Size(355, 22);
            this.cmbBooktype.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 14);
            this.label1.TabIndex = 28;
            this.label1.Text = "From Date";
            // 
            // dtfrdate
            // 
            this.dtfrdate.CustomFormat = "dd/MM/yyyy";
            this.dtfrdate.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtfrdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtfrdate.Location = new System.Drawing.Point(110, 37);
            this.dtfrdate.Name = "dtfrdate";
            this.dtfrdate.Size = new System.Drawing.Size(132, 22);
            this.dtfrdate.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(264, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 14);
            this.label4.TabIndex = 29;
            this.label4.Text = "To Date";
            // 
            // dttodate
            // 
            this.dttodate.CustomFormat = "dd/MM/yyyy";
            this.dttodate.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dttodate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dttodate.Location = new System.Drawing.Point(333, 37);
            this.dttodate.Name = "dttodate";
            this.dttodate.Size = new System.Drawing.Size(132, 22);
            this.dttodate.TabIndex = 31;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(481, 63);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(103, 30);
            this.btnLoad.TabIndex = 32;
            this.btnLoad.Text = "&Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // ucDayBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtfrdate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dttodate);
            this.Controls.Add(this.chkchqpass);
            this.Controls.Add(this.rddailybal);
            this.Controls.Add(this.rdmothbal);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rdyearbal);
            this.Controls.Add(this.cmbBooktype);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.pnlbalance);
            this.Controls.Add(this.cfgday);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.lblopbvalance);
            this.Controls.Add(this.btnPrint);
            this.Name = "ucDayBook";
            this.Size = new System.Drawing.Size(807, 606);
            this.Load += new System.EventHandler(this.ucDayBook_Load);
            this.pnlbalance.ResumeLayout(false);
            this.pnlbalance.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cfgday)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblcrdr;
        private System.Windows.Forms.Panel pnlbalance;
        public C1.Win.C1FlexGrid.C1FlexGrid cfgday;
        private System.Windows.Forms.Label lblopbvalance;
        private System.Windows.Forms.CheckBox chkchqpass;
        private System.Windows.Forms.RadioButton rddailybal;
        private System.Windows.Forms.RadioButton rdmothbal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rdyearbal;
        private System.Windows.Forms.ComboBox cmbBooktype;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtfrdate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dttodate;
        private System.Windows.Forms.Button btnLoad;
        public System.Windows.Forms.Button btnClose;
    }
}
