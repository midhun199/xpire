using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using System.IO;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Reports
{
    public partial class ucSalesBillWise : UserControl
    {
        string masterTable = string.Empty,heading=string.Empty ;
        SqlCommand cmd = new SqlCommand("", cPublic.Connection);
        cGeneral cgen = new cGeneral();
        fssgen.fssgen gen = new fssgen.fssgen();
        string vTag = string.Empty;

        int leng=0;
        PointF foi = new PointF(100, 50);
        int  le, lee,ii, jj;
        Boolean lin = false;
        StreamWriter tstream;
        int RptWidth;
        int lineno;
        long PageNo;
        bool line = false, day = false;
        int G_Lperpage = 59;


        public ucSalesBillWise()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            Common();
        }

        private void ucSalesBillWise_Load(object sender, EventArgs e)
        {
            DtpFrom.Value = cPublic.g_Rdate;
            DtpTo.Value = cPublic.g_Rdate;
            cmbBillSrs.SelectedIndex = 0;
            cmbBilltype.SelectedIndex = 0;
            cmbpricetype.SelectedIndex = 0;
            switch (this.Tag.ToString())
            {
                case "SA":  //  SalesReports
                    masterTable = "SALES" + cPublic.g_firmcode;
                    break;
                case "SR":  //  Sales Return
                    masterTable = "SRETURN" + cPublic.g_firmcode;
                    break;
                case "SO":  //  Sales Indend
                    masterTable = "SORDER0000";
                    break;
                case "DMG": //  Damage
                    masterTable = "DAMAGE" + cPublic.g_firmcode;
                    break;
                case "QTN": //  Quotation.
                    masterTable = "QTN0000";
                    break;
                case "TRO": //  Transfer Out.
                    masterTable = "TRANSFER" + cPublic.g_firmcode;
                    break;
            }
        }

        private void txtCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.F5) || (e.KeyCode == Keys.Add))
            {
                Cust_LookUp(sender, e);
            }
        }

        private void txtCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtCustomer.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
                    else if (txtCustomer.Text == "")
                        if (e.KeyChar != Convert.ToChar(Keys.Back))
                            Cust_LookUp(sender, e);
            e.Handled = true;
        }

        private void Cust_LookUp(object sender, EventArgs e)
        {
            frmlookup lnew1 = new frmlookup();
            if ((e.GetType().ToString() == typeof(KeyEventArgs).ToString()))
            {
                lnew1.m_strat = "";
            }
            else
            {
                lnew1.m_strat = ((KeyPressEventArgs)e).KeyChar.ToString();
                ((KeyPressEventArgs)e).KeyChar = Convert.ToChar(Keys.None);
            }
            if (sender == txtCustomer)
            {
                txtCustomer.ForeColor = Color.Black;

                lnew1.m_fields = "HEAD,CODE,ADDRESS, STREET, CITY,balance";
                lnew1.m_table = "ACCOUNTS" + cPublic.g_firmcode;
                lnew1.m_dispname = "HEAD,CODE,ADDRESS, STREET, CITY,BALANCE";
                lnew1.m_condition = "groupcode='200021' ";
                lnew1.m_order = "";
                lnew1.m_fldwidth = "150,0,100,100,100,100";
                lnew1.ShowDialog();
                if (lnew1.m_values.Count > 0)
                {
                    txtCustomer.Text = lnew1.m_values[0].ToString();
                    txtCustomer.Tag = lnew1.m_values[1].ToString();
                }
                else
                {
                    txtCustomer.ForeColor = Color.Black;
                    txtCustomer.Text = "";
                }

            }
        }

        public void Common()
        {
            string vgroupby = string.Empty;
            string Query = string.Empty, cond1 = string.Empty;
            DateTime x = new DateTime();
            DateTime y = new DateTime();

            if (chksummary.Checked == true)
            {
                Query = " [Bill Amount]=SUM(BILLAMT),[Item Total]= SUM(BILLAMT-ROUNDOFF+SPDISCAMT-TAXAMT-CESSAMT)-SUM(EXPENSE) ,"
                           + "Discount=SUM(SPDISCAMT) ,"
                           + "[Tax Amount]=SUM(TAXAMT) ,"
                           + "[Cess Amount]=SUM(CESSAMT) ,"
                           + "[Round Off]=SUM(ROUNDOFF) "
                           + "  from " + masterTable;

                if (radioButton1.Checked == true)
                {
                    Query = "select "
                            + "[BillType] =case when  billtype ='1' then  'Cash' else 'Credit' end,"
                           + Query;
                    vgroupby = " group by billtype  order by billtype ";
                }
                else if (chkDaywise.Checked == true)
                {
                    Query = "select convert(varchar,DATE1,103) as Date,"
                           + "[BillType] =case when  billtype ='1' then  'Cash' else 'Credit' end,"
                          + Query;
                    vgroupby = " group by date1,billtype  order by date1,billtype ";
                }
                else if (Chkmonthwise.Checked == true)
                {
                    Query = "select datename(month,max(date1)) Month, substring(datename(month,max(date1)),1,3) +' - '+ convert(varchar,year(max(date1))) Date,"
                           + "[BillType] =case when  billtype ='1' then  'Cash' else 'Credit' end,"
                          + Query;
                    vgroupby = " group by year(date1),month(date1),billtype order by year(date1),month(date1),billtype ";
                }
            }
            else if (Chkmonthwise.Checked == true)
            {
                if ((this.Tag.ToString() == "SM") | (this.Tag.ToString() == "SRM"))
                {
                    Query = "select substring(convert(varchar,DATE1,103),4,7) D,convert(varchar,DATE1,103) as Date,convert(varchar,orderno) as [Bill No],"
                        + "[BillType] =case when  billtype ='1' then  'Cash' else 'Credit' end,"
                        + "[Sales Person]=case when custcode='cancel' then '-- Cancelled --' else (select [name] from employeemaster where empid=salesman ) end,"
                        + "[Bill Amount]=case when custcode='cancel' then 0.00 else SUM(BILLAMT) end, " + Environment.NewLine
                        + "[Item Total]=case when custcode='cancel' then 0.00 else SUM(BILLAMT-ROUNDOFF+SPDISCAMT-TAXAMT-CESSAMT-EXPENSE) end," + Environment.NewLine
                        + "Discount=case when custcode='cancel' then 0.00 else  SUM(SPDISCAMT) end," + Environment.NewLine
                        + "[Tax Amount]=case when custcode='cancel' then 0.00 else SUM(TAXAMT) end," + Environment.NewLine
                        + "[Cess Amount]=case when custcode='cancel' then 0.00 else SUM(CESSAMT) end," + Environment.NewLine
                        + "[Round Off]=case when custcode='cancel' then 0.00 else SUM(ROUNDOFF) end  "
                        + " from " + masterTable;
                }
                else
                {
                    Query = "select substring(convert(varchar,DATE1,103),4,7) D,convert(varchar,DATE1,103) as Date,convert(varchar,orderno) as [Bill No]," + Environment.NewLine
                                           + "[BillType] =case when  billtype ='1' then  'Cash' else 'Credit' end," + Environment.NewLine
                                           + "[Customer]=case when custcode='cancel' then '-- Cancelled --' else custname end," + Environment.NewLine
                                           + "[Bill Amount]=case when custcode='cancel' then 0.00 else SUM(BILLAMT) end, " + Environment.NewLine
                                           + "[Item Total]=case when custcode='cancel' then 0.00 else SUM(BILLAMT-ROUNDOFF+SPDISCAMT-TAXAMT-CESSAMT-EXPENSE) end," + Environment.NewLine
                                           + "Discount=case when custcode='cancel' then 0.00 else  SUM(SPDISCAMT) end," + Environment.NewLine
                                           + "[Tax Amount]=case when custcode='cancel' then 0.00 else SUM(TAXAMT) end," + Environment.NewLine
                                           + "[Cess Amount]=case when custcode='cancel' then 0.00 else SUM(CESSAMT) end," + Environment.NewLine
                                           + "[Round Off]=case when custcode='cancel' then 0.00 else SUM(ROUNDOFF) end "
                                           + " from " + masterTable;

                }

            }
            else
            {
                if ((this.Tag.ToString() == "SM") | (this.Tag.ToString() == "SRM"))
                {
                    Query = "select convert(varchar,DATE1,103) as Date,convert(varchar,orderno) as [Bill No],"
                        + "[BillType] =case when  billtype ='1' then  'Cash' else 'Credit' end,"
                        + "[Sales Person]=case when custcode='cancel' then '-- Cancelled --' else (select [name] from employeemaster where empid=salesman ) end,"
                        + "[Bill Amount]=case when custcode='cancel' then 0.00 else SUM(BILLAMT) end, " + Environment.NewLine
                        + "[Item Total]=case when custcode='cancel' then 0.00 else SUM(BILLAMT-ROUNDOFF+SPDISCAMT-TAXAMT-CESSAMT-EXPENSE) end,"
                        + "[Discount]=case when custcode='cancel' then 0.00 else  SUM(SPDISCAMT) end,"
                        + "[Tax Amount]=case when custcode='cancel' then 0.00 else SUM(TAXAMT) end,"
                        + "[Cess]=case when custcode='cancel' then 0.00 else SUM(CESSAMT) end,"
                        + "[Round Off]=case when custcode='cancel' then 0.00 else SUM(ROUNDOFF) end "
                        + " from " + masterTable;
                }



                else
                {

                    if (this.Tag.ToString() == "TD")
                    {

                    }
                    else
                    {
                        string vcond = "";
                        if (this.Tag.ToString() == "SO" || this.Tag.ToString() == "QTN")
                        { vcond = "[Sales No]=case when a.custcode='cancel' then '-- Cancelled --' else (select top 1 convert(varchar,orderno) from sales0000 where qtnno=a.orderno) end,"; }


                        Query = "select convert(varchar,DATE1,103) as Date,convert(varchar,orderno) as [Bill No],"
                                               + "[BillType] =case when  billtype ='1' then  'Cash' else 'Credit' end,"
                                               + "[Customer]=case when custcode='cancel' then '-- Cancelled --' else custname end,"
                                               + vcond
                                               + "[Bill Amount]=case when custcode='cancel' then 0.00 else SUM(BILLAMT) end, "
                                               + "[Item Total]=case when custcode='cancel' then 0.00 else SUM(BILLAMT-ROUNDOFF+SPDISCAMT-TAXAMT-CESSAMT-EXPENSE) end,"
                                               + "Discount=case when custcode='cancel' then 0.00 else  SUM(SPDISCAMT) end,"
                                               + "[Tax Amount]=case when custcode='cancel' then 0.00 else SUM(TAXAMT) end,"
                                               + "[Cess]=case when custcode='cancel' then 0.00 else SUM(CESSAMT) end,"
                                               + "[Round Off]=case when custcode='cancel' then 0.00 else SUM(ROUNDOFF) end "
                                               + "  from " + masterTable + " a";

                    }
                }
            }
            x = DtpFrom.Value;
            y = DtpTo.Value;

            cond1 = "";


            if (txtCustomer.Text.Trim() != "")
            {
                if ((this.Tag.ToString() == "SM") | (this.Tag.ToString() == "SRM"))
                { cond1 += " salesman = '" + gen.SQLFormat(txtCustomer.Tag + "") + "' and "; }
                else
                {
                    if (this.Tag.ToString() == "TD" || this.Tag.ToString() == "PK")
                    { cond1 += " AND CODE='" + gen.SQLFormat(txtCustomer.Tag + "") + "'"; }
                    else
                    { cond1 += " Custname = '" + gen.SQLFormat(txtCustomer.Text.Trim()) + "' and "; }
                }
            }

            if (this.Tag.ToString() != "TD" && this.Tag.ToString() != "PK")
            {
                if (cmbBillSrs.SelectedItem.ToString() == "All")
                    cond1 = cond1 + "";
                else
                    cond1 += " form='" + cmbBillSrs.SelectedItem.ToString() + "' and ";



                switch (cmbpricetype.Text.ToString())
                {
                    case "All":
                        cond1 = cond1 + "";
                        break;
                    case "Retail Price":
                        cond1 = cond1 + "  pricetype='1' and ";
                        break;
                    case "Wholesale Price":
                        cond1 = cond1 + " pricetype='2' and ";
                        break;
                    case "Special Price":
                        cond1 = cond1 + " pricetype='3' and ";
                        break;
                }


                switch (cmbBilltype.Text.ToString())
                {
                    case "All":
                        if ((this.Tag.ToString() == "SM") | (this.Tag.ToString() == "SRM"))
                        {
                            cond1 = " where " + cond1 + " date1>='" + x.ToString("yyyy/MM/dd") + "' AND date1<='" + y.ToString("yyyy/MM/dd") + "'  ";
                            if (chksummary.Checked == false) { vgroupby = " group by date1,form ,orderno,billtype ,custname,custcode,userid  ORDER BY date1,orderno "; }

                        }
                        else
                        {
                            if (this.Tag.ToString() == "TD")
                                cond1 = " where " + cond1 + " date1>='" + x.ToString("yyyy/MM/dd") + "' AND date1<='" + y.ToString("yyyy/MM/dd") + "'";
                            else
                            {
                                cond1 = " where " + cond1 + " date1>='" + x.ToString("yyyy/MM/dd") + "' AND date1<='" + y.ToString("yyyy/MM/dd") + "'  ";
                                if (chksummary.Checked == false) { vgroupby = " group by date1,form ,orderno,billtype ,custname,custcode,userid  ORDER BY BILLTYPE,date1,orderno ,custname "; }
                            }
                        }
                        cond1 = cond1 + vgroupby;
                        break;
                    case "Cash":
                        if ((this.Tag.ToString() == "SM") | (this.Tag.ToString() == "SRM"))
                        {
                            cond1 = " where " + cond1 + " date1>='" + x.ToString("yyyy-MM-dd") + "' AND date1<='" + y.ToString("yyyy-MM-dd") + "' and billtype='1'  ";
                            if (chksummary.Checked == false) { vgroupby = " group by date1,form ,orderno ,custname,custcode,userid ORDER BY date1,orderno "; }
                        }
                        else
                        {
                            cond1 = " where " + cond1 + " date1>='" + x.ToString("yyyy-MM-dd") + "' AND date1<='" + y.ToString("yyyy-MM-dd") + "' and billtype='1' ";
                            if (chksummary.Checked == false) { vgroupby = " group by date1,form ,orderno,billtype ,custname,custcode,userid ORDER BY date1,orderno ,custname "; }
                        }
                        cond1 = cond1 + vgroupby;
                        break;
                    case "Credit":
                        if ((this.Tag.ToString() == "SM") | (this.Tag.ToString() == "SRM"))
                        {
                            cond1 = " where " + cond1 + " date1>='" + x.ToString("yyyy-MM-dd") + "' AND date1<='" + y.ToString("yyyy-MM-dd") + "' and billtype='2' ";
                            if (chksummary.Checked == false) { vgroupby = " group by date1,form ,orderno ,custname,custcode,userid ORDER BY date1,orderno "; }
                        }
                        else
                        {
                            cond1 = " where " + cond1 + " date1>='" + x.ToString("yyyy-MM-dd") + "' AND date1<='" + y.ToString("yyyy-MM-dd") + "' and billtype='2' ";
                            if (chksummary.Checked == false) { vgroupby = " group by date1,form ,orderno,billtype ,custname,custcode,userid ORDER BY date1,orderno ,custname "; }
                        }

                        cond1 = cond1 + vgroupby;
                        break;
                }
            }
            if (this.Tag.ToString() == "TD")
            {
                Query = "select CODE [CODE],DETAILS [Sales Person],[sales][Total Sales],[sreturn][Total SReturn]," + Environment.NewLine
                + "([sales]-[sreturn])[Actual Amount] from " + Environment.NewLine
                + "(select B.CODE,[DETAILS],(select isnull(sum(billamt),0) from sales" + cPublic.g_firmcode + " where salesman=B.CODE and date1 between '" + x.ToString("yyyy-MM-dd") + "' and '" + y.ToString("yyyy-MM-dd") + "')[sales]," + Environment.NewLine
                + "(select isnull(sum(billamt),0) from sreturn" + cPublic.g_firmcode + " where salesman=B.CODE and date1 between '" + x.ToString("yyyy-MM-dd") + "' and '" + y.ToString("yyyy-MM-dd") + "')[sreturn] from lookup B where field1='SALES PERSON' " + Environment.NewLine
                + cond1 + ") a";
            }
            if (this.Tag.ToString() == "PK")
            {
                Query = "select CODE [CODE],DETAILS [PACKER],[sales][Total Sales],[sreturn][Total SReturn]," + Environment.NewLine
                + "([sales]-[sreturn])[Actual Amount] from " + Environment.NewLine
                + "(select B.CODE,[DETAILS],(select isnull(sum(billamt),0) from sales" + cPublic.g_firmcode + " where packer=B.CODE and date1 between '" + x.ToString("yyyy-MM-dd") + "' and '" + y.ToString("yyyy-MM-dd") + "')[sales]," + Environment.NewLine
                + "(select isnull(sum(billamt),0) from sreturn" + cPublic.g_firmcode + " where packer=B.CODE and date1 between '" + x.ToString("yyyy-MM-dd") + "' and '" + y.ToString("yyyy-MM-dd") + "')[sreturn] from lookup B where field1='PACKER' " + Environment.NewLine
                + cond1 + ") a";
            }

            string sql = string.Empty;
            if (this.Tag.ToString() == "TD")
            { sql = Query; }
            else { sql = Query + cond1; }

            heading = this.Text + "  " + "Reports From : " + x.ToString("dd-MM-yyyy") + " To " + y.ToString("dd-MM-yyyy");

            if (chkDaywise.Checked == true)
            {
                vTag = "SRC";
            }
            else if (Chkmonthwise.Checked == true)
            {
                vTag = "SRP";
            }
            else
            {
                vTag = "SRU";
            }

            if (chkDaywise.Checked == true)
            {
                day = true;
            }
            else
            {
                day = false;
            }

            Load_Details(sql);
        }

        private void Load_Details(string sqlQuery)
        {
            flxStockPrv.ExtendLastCol = true;
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, cPublic.Connection);
            DataTable dt = new DataTable();
            da.Fill(dt);

            flxStockPrv.DataSource = dt.DefaultView;

            flxStockPrv.Focus();
            if (vTag == "SAG")
            {
                if (flxStockPrv.Cols["Customer"] != null)
                {
                    for (int vCount = 1; vCount < flxStockPrv.Rows.Count; vCount++)
                    {
                        if (flxStockPrv[vCount, "Customer"] + "" == string.Empty)
                        { flxStockPrv[vCount, "Customer"] = " "; }
                    }
                }
            }

            for (int i = 1; i < flxStockPrv.Cols.Count; i++)
            {
                string type = flxStockPrv.Cols[i].DataType.ToString();
                if (type.Equals("System.Decimal"))
                {
                    flxStockPrv.Cols[i].TextAlignFixed = C1.Win.C1FlexGrid.TextAlignEnum.RightTop;
                    if (vTag == "SAG" || vTag == "SRG")
                    {
                        C1.Win.C1FlexGrid.CellStyle cs;
                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;

                        if (flxStockPrv.Cols["Customer"] == null)
                        { flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, 1, i, "Subtotal"); }
                        else
                        {
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 3, flxStockPrv.Cols["Customer"].Index, flxStockPrv.Cols["Customer"].Index + 1, i, "");
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, flxStockPrv.Cols["Customer"].Index, i, "Cus.total");
                        }
                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                        if (flxStockPrv.Rows.Count > 1)
                        {
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, -1, i, "Grand Total");
                        }
                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.Subtotal1];
                        cs.BackColor = Color.DarkGray;
                        cs.ForeColor = Color.White;

                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.GrandTotal];
                        cs.BackColor = Color.SteelBlue;
                        cs.ForeColor = Color.White;
                    }
                    else if (vTag == "SRC")
                    {
                        C1.Win.C1FlexGrid.CellStyle cs;
                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;

                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, 1, i, "Subtotal");

                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                        if (flxStockPrv.Rows.Count > 1)
                        {
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, -1, i, "Grand Total");
                        }
                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.Subtotal1];
                        cs.BackColor = Color.DarkGray;
                        cs.ForeColor = Color.White;

                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.GrandTotal];
                        cs.BackColor = Color.SteelBlue;
                        cs.ForeColor = Color.White;
                    }
                    else if (vTag == "SRU")
                    {
                        C1.Win.C1FlexGrid.CellStyle cs;

                        //if (subtotal == true)
                        //{
                        //    flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;

                        //    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, 3, 3, i, "Subtotal");
                        //}
                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                        if (flxStockPrv.Rows.Count > 1)
                        {
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, -1, -1, i, "Grand Total");

                            flxStockPrv.SetData(flxStockPrv.Rows.Count - 1, 1, "GRAND TOTAL", true);
                        }
                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.GrandTotal];
                        cs.BackColor = Color.DarkGray;
                        cs.ForeColor = Color.White;
                    }
                    else if (vTag == "SD")
                    {
                        C1.Win.C1FlexGrid.CellStyle cs;


                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                        if (flxStockPrv.Rows.Count > 1)
                        {
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, -1, -1, i, "Grand Total");

                            flxStockPrv.SetData(flxStockPrv.Rows.Count - 1, 1, "GRAND TOTAL", true);
                        }
                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.GrandTotal];
                        cs.BackColor = Color.DarkGray;
                        cs.ForeColor = Color.White;
                    }
                    else if (vTag == "SRP")
                    {
                        C1.Win.C1FlexGrid.CellStyle cs;
                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, 1, i, "Subtotal");

                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                        if (flxStockPrv.Rows.Count > 1)
                        {
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, -1, -1, i, "Grand Total");
                        }
                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.Subtotal1];
                        cs.BackColor = Color.DarkGray;
                        cs.ForeColor = Color.White;

                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.GrandTotal];
                        cs.BackColor = Color.SteelBlue;
                        cs.ForeColor = Color.White;
                        flxStockPrv.Cols[1].Visible = false;
                        for (int j = 1; j < flxStockPrv.Rows.Count; j++)
                        {
                            if (flxStockPrv[j, 2] == null)
                            { flxStockPrv[j, 2] = "SUB TOTAL"; }
                        }
                        flxStockPrv.SetData(flxStockPrv.Rows.Count - 1, 2, "GRAND TOTAL");
                    }

                    else if (vTag == "SPB")
                    {
                        C1.Win.C1FlexGrid.CellStyle cs;
                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                        flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 2, 1, i, "Subtotal");

                        flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;

                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.Subtotal1];
                        cs.BackColor = Color.DarkGray;
                        cs.ForeColor = Color.White;

                        cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.GrandTotal];
                        cs.BackColor = Color.SteelBlue;
                        cs.ForeColor = Color.White;
                    }
                    else if (vTag == "POQ")
                    {
                        if (flxStockPrv.Cols["Qty"] != null)
                        {
                            C1.Win.C1FlexGrid.CellStyle cs;
                            flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 0, -1, flxStockPrv.Cols["Qty"].Index, "Grandtotal");
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 0, -1, flxStockPrv.Cols["PQty"].Index, "Grandtotal");
                            flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 0, -1, flxStockPrv.Cols["Bal.Qty"].Index, "Grandtotal");

                            if (flxStockPrv.Cols["Itemcode"] != null && flxStockPrv.Cols["Suppname"] != null)
                            {
                                if (flxStockPrv.Cols["Itemcode"].Index > flxStockPrv.Cols["Suppname"].Index)
                                {
                                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["Suppname"].Index, flxStockPrv.Cols["Qty"].Index, "Subtotal");
                                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["Suppname"].Index, flxStockPrv.Cols["PQty"].Index, "Subtotal");
                                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["Suppname"].Index, flxStockPrv.Cols["Bal.Qty"].Index, "Subtotal");
                                }
                                else
                                {
                                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["Itemcode"].Index, flxStockPrv.Cols["Qty"].Index, "Subtotal");
                                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["Itemcode"].Index, flxStockPrv.Cols["PQty"].Index, "Subtotal");
                                    flxStockPrv.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, flxStockPrv.Cols["Itemcode"].Index, flxStockPrv.Cols["Bal.Qty"].Index, "Subtotal");
                                }
                            }
                            //flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;

                            cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.Subtotal1];
                            cs.BackColor = Color.DarkGray;
                            cs.ForeColor = Color.White;

                            cs = flxStockPrv.Styles[C1.Win.C1FlexGrid.CellStyleEnum.GrandTotal];
                            cs.BackColor = Color.SteelBlue;
                            cs.ForeColor = Color.White;
                        }
                    }
                }

            }
            if (vTag == "SPB")
            {
                if (dt.Rows.Count > 0)
                {
                    if (flxStockPrv[1, 6] + "" != flxStockPrv[flxStockPrv.Rows.Count - 1, 4] + "")
                    {
                        if (Convert.ToDecimal(flxStockPrv[1, 6] + "") == 0)
                        {
                            dt.Rows.Add();
                            dt.Rows[dt.Rows.Count - 1][0] = cPublic.g_Rdate.ToString("dd/MM/yyyy");
                            dt.Rows[dt.Rows.Count - 1][1] = "Opening Balance";
                            dt.Rows[dt.Rows.Count - 1][2] = "0";
                            dt.Rows[dt.Rows.Count - 1][3] = Math.Abs(Convert.ToDecimal(flxStockPrv[1, 6] + "")) + Convert.ToDecimal(flxStockPrv[1, 7] + "");
                            dt.Rows[dt.Rows.Count - 1][4] = "0";
                            dt.Rows[dt.Rows.Count - 1][5] = "0";
                            dt.Rows[dt.Rows.Count - 1][6] = "0";
                            dt.Rows[dt.Rows.Count - 1][6] = "0";
                        }
                        else
                        {
                            dt.Rows.Add();
                            dt.Rows[dt.Rows.Count - 1][0] = cPublic.g_Rdate.ToString("dd/MM/yyyy");
                            dt.Rows[dt.Rows.Count - 1][1] = "Opening Balance";
                            dt.Rows[dt.Rows.Count - 1][2] = "0";
                            dt.Rows[dt.Rows.Count - 1][3] = flxStockPrv[1, 6] + "";
                            dt.Rows[dt.Rows.Count - 1][4] = "0";
                            dt.Rows[dt.Rows.Count - 1][5] = "0";
                            dt.Rows[dt.Rows.Count - 1][6] = "0";
                            dt.Rows[dt.Rows.Count - 1][6] = "0";
                        }
                    }
                    flxStockPrv.Cols[1].Width = 200;
                    flxStockPrv.Cols[2].Width = 200;
                    flxStockPrv.Cols[3].Width = 200;
                    flxStockPrv.Cols[4].Width = 200;

                    flxStockPrv.Cols[5].Visible = false;
                    flxStockPrv.Cols[6].Visible = false;
                    flxStockPrv.Cols[7].Visible = false;
                    flxStockPrv.Cols[8].Visible = false;
                    flxStockPrv.Cols[5].Width = 0;
                    flxStockPrv.Cols[6].Width = 0;
                    flxStockPrv.Cols[7].Width = 0;
                    flxStockPrv.Cols[8].Width = 0;
                }
            }

            for (int i = 1; i < flxStockPrv.Cols.Count; i++)
            {

                string type = flxStockPrv.Cols[i].DataType.ToString();
                if (type.Equals("System.Decimal"))
                    if (flxStockPrv[0, i] + "".ToString() == "Quantity" || flxStockPrv[0, i] + "".ToString() == "Qty")
                    {
                        flxStockPrv.Cols[i].Format = "#0.000";
                    }
                    else
                    { flxStockPrv.Cols[i].Format = "#0.00"; }
            }

            for (int i = 1; i < flxStockPrv.Rows.Count; i++)
            {
                if (flxStockPrv[i, 1] == null)
                { flxStockPrv[i, 1] = "SUB TOTAL"; }
            }

            if ((this.Tag + "".ToString() == "SRC" || this.Tag + "".ToString() == "SRU" || this.Tag + "".ToString() == "SRP") && flxStockPrv.Rows.Count > 1)
            {
                flxStockPrv.Cols[1].DataType = typeof(string);

                flxStockPrv.SetData(flxStockPrv.Rows.Count - 1, 1, "GRAND TOTAL");
            }
            setSlNo();

           
            int gewidth = Convert.ToInt32(flxStockPrv.Width.ToString()) / flxStockPrv.Cols.Count;

            for (int wdth = 1; wdth < flxStockPrv.Cols.Count; wdth++)
            {
                string type = flxStockPrv.Cols[wdth].DataType.ToString();

                if (type.Equals("System.Decimal"))
                {
                    if (flxStockPrv.Cols[wdth].Name.ToUpper() == "TAX AMOUNT")
                    {
                        flxStockPrv.Cols[wdth].Width = 100;
                    }
                    else if (flxStockPrv.Cols[wdth].Name.ToUpper() == "CREDIT" || flxStockPrv.Cols[wdth].Name.ToUpper() == "DEBIT")
                    {
                        flxStockPrv.Cols[wdth].Width = 100;
                    }
                    else if (flxStockPrv.Cols[wdth].Name.ToUpper() == "BILL AMOUNT" || flxStockPrv.Cols[wdth].Name.ToUpper() == "ITEM TOTAL")
                    { flxStockPrv.Cols[wdth].Width = 100; }
                    else if (flxStockPrv.Cols[wdth].Name.ToUpper() == "CESS AMOUNT")
                    { flxStockPrv.Cols[wdth].Width = 120; }
                    else if (flxStockPrv.Cols[wdth].Name.ToUpper() == "CESS")
                    { flxStockPrv.Cols[wdth].Width = 60; }
                    else if (flxStockPrv.Cols[wdth].Name.ToUpper() == "DISCOUNT")
                    { flxStockPrv.Cols[wdth].Width = 70; }

                    else if (flxStockPrv.Cols[wdth].Name.ToUpper() == "SALES")
                    {
                        flxStockPrv.Cols[wdth].Width = 150;
                    }
                    else if (flxStockPrv.Cols[wdth].Name.ToUpper() == "SALES RETURN")
                    {
                        flxStockPrv.Cols[wdth].Width = 250;
                    }
                    else
                    {
                        flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) - 14;
                    }
                }

                if (flxStockPrv.Cols[wdth].Name.ToUpper() == "CUSTOMER")
                {
                    flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) + 200;
                }
                if (flxStockPrv.Cols[wdth].Name.ToUpper() == "ITEM NAME")
                {
                    flxStockPrv.Cols[wdth].Width = Convert.ToInt32(gewidth.ToString()) + 150;
                }
                else if (flxStockPrv.Cols[wdth].Name.ToUpper() == "AGENT NAME")
                {
                    flxStockPrv.Cols[wdth].Width = 300;
                }
                if (flxStockPrv.Cols[wdth].Name.ToUpper() == "BILL NO")
                {
                    flxStockPrv.Cols[wdth].Width = 60;
                }
                if (flxStockPrv.Cols[wdth].Name.ToUpper() == "HEAD")
                {
                    flxStockPrv.Cols[wdth].Width = 320;
                }
            }
            int locati = flxStockPrv.Cols[0].Width - 50;
            for (int k = 1; k < flxStockPrv.Cols.Count; k++)
            {
                locati += flxStockPrv.Cols[k].Width;
                string type = flxStockPrv.Cols[k].DataType.ToString();
                
                if (flxStockPrv.Cols[1].Name == "a" && flxStockPrv.Rows.Count > 1)
                {
                    flxStockPrv.Cols[1].Visible = false;
                    flxStockPrv[flxStockPrv.Rows.Count - 1, 2] = "GRAND TOTAL";
                }
            }

            flxStockPrv.Row = flxStockPrv.Rows.Count - 1;

            if (flxStockPrv.Rows.Count == 1)
            {
                btnView.Visible = false;
                btnPrint.Visible = false;
                btnExport.Visible = false;
            }
            else
            {
                btnView.Visible = true;
                btnPrint.Visible = true;
                btnExport.Visible = true;
            }

            if (this.Tag + "".ToString() == "POQ")
            {
                if (flxStockPrv.Cols["Itemcode"] != null && flxStockPrv.Cols["Suppname"] != null)
                {
                    if (flxStockPrv.Cols["Itemcode"].Index < flxStockPrv.Cols["Suppname"].Index)
                    {
                        for (int j = flxStockPrv.Rows.Count - 1; j > 0; j--)
                        {
                            if (Convert.ToString(flxStockPrv[j, "Itemcode"] + "") == Convert.ToString(flxStockPrv[j - 1, "Itemcode"] + "") && Convert.ToString(flxStockPrv[j, "Itemname"] + "") == Convert.ToString(flxStockPrv[j - 1, "Itemname"] + "")
                                && flxStockPrv[j, "itemname"] != null)
                            {
                                flxStockPrv[j, "Itemcode"] = " "; flxStockPrv[j, "Itemname"] = " ";
                            }
                        }
                    }
                    else
                    {
                        for (int j = flxStockPrv.Rows.Count - 1; j > 0; j--)
                        {
                            if (Convert.ToString(flxStockPrv[j, "Suppname"] + "") == Convert.ToString(flxStockPrv[j - 1, "Suppname"] + "")
                                && flxStockPrv[j, "itemname"] != null)
                            {
                                flxStockPrv[j, "Suppname"] = " ";
                            }
                        }
                    }
                }
            }
        }

        private void setSlNo()
        {
            int r = 1;
            flxStockPrv[0, 0] = "Nos";

            for (int i = 1; i < flxStockPrv.Rows.Count; i++)
            {

                if (flxStockPrv[i, 1] + "".ToString() != "SUB TOTAL")
                {
                    if (flxStockPrv[i, 1] + "".ToString() != "GRAND TOTAL")
                    {
                        flxStockPrv[i, 0] = r;
                        r++;
                    }
                }
            }
            r--;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataExport data = new DataExport();
            data.flex_Excel(flxStockPrv);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            cPublic.g_rpthead = this.Text;
            cPublic.g_from = DtpFrom.Value;
            cPublic.g_to = DtpTo.Value;  
            cgen.RepPrint(PrintDos().ToString());
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            cPublic.g_rpthead = this.Text;
            cPublic.g_from = DtpFrom.Value;
            cPublic.g_to = DtpTo.Value;  
            Interaction.Shell("notepad.exe  " + cPublic.FilePath + "\\" + PrintDos() + ".txt", AppWinStyle.MaximizedFocus, true, 1);
        }

        private int PrintDos()
        {
            leng = 0;

            for (int  ii = 1; ii <= flxStockPrv.Cols.Count - 1; ii++)
            {

                if (flxStockPrv.Cols[ii].Visible == true)
                {

                    if (flxStockPrv[0, ii].ToString() == "Bill No")
                    {
                        lee = 18;
                    }
                    else if (flxStockPrv[0, ii].ToString() == "Date")
                    {
                        lee = 20;
                    }
                    else if (flxStockPrv[0, ii].ToString() == "Customer")
                    {
                        lee = 40;
                    }
                    else
                    {
                        lee = flxStockPrv.Cols[ii].ToString().Length;
                    }
                    string type = flxStockPrv.Cols[ii].DataType.ToString();
                    if (type.Equals("System.Decimal"))
                    {
                        lee = lee - 13;
                        leng = leng + lee + 1;
                    }
                    else
                    {
                        lee = lee - 9;
                        leng = leng + lee + 1;
                    }
                }
            }
            RptWidth = leng - 2;
            PageNo = 0;
            lineno = 0;
            lee = 0;
            le = 0;
            
            int notepadpath = (Convert.ToInt32(1000 * VBMath.Rnd()) + 1);

            if (Directory.Exists(cPublic.FilePath + "\\" + notepadpath + ".txt"))
            {
                Directory.Delete(cPublic.FilePath + "\\" + notepadpath + ".txt");
            }

            string path = cPublic.FilePath + "\\" + notepadpath + ".txt";
            tstream = File.CreateText(path);
            rPagehead(path);
            oneline(path);
            tstream.WriteLine(cgen.Replicate("=", RptWidth));
            tstream.WriteLine(Convert.ToChar(12));
            tstream.Flush();
            tstream.Close();

            return notepadpath;
        }

        private void rPagehead(string path)
        {
            PageNo = PageNo + 1;

            tstream.Write(cPublic.Ch10 + cPublic.Ch12);
           
                tstream.WriteLine(cPublic.BchDW);
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_CompName, "C", 40));
                tstream.Write(cPublic.BchNDW);
            
                    tstream.WriteLine(cgen.FnAlignString(cPublic.g_add1 + "," + cPublic.g_add2, "C", RptWidth - 80));
                    tstream.WriteLine(cgen.FnAlignString(cPublic.g_add3 + "," + cPublic.g_add4, "C", RptWidth - 80));
                    tstream.WriteLine(cgen.FnAlignString("Tin No:" + cPublic.g_TinNo, "C", RptWidth - 80));
                
            tstream.Write(cPublic.Ch10 + cPublic.Ch12);
            tstream.Write(cPublic.ChC);
            if (vTag  != "V")
            {
                tstream.WriteLine(cgen.FnAlignString(cPublic.ChE + cPublic.g_rpthead + cPublic.ChNE, "C", RptWidth - 60));
            }
            if (vTag == "V")
            {
                tstream.WriteLine(cgen.FnAlignString("VAT Register", "C", 40));
                tstream.WriteLine(cgen.FnAlignString("************", "C", 40));
            }
            if (RptWidth >= 55)
            {
                tstream.Write(cgen.FnAlignString("From : " + cPublic.g_from.ToString("dd/MM/yyyy") + "     " + "To : " + cPublic.g_to.ToString("dd/MM/yyyy"), "L", RptWidth - 20));
                tstream.WriteLine(cgen.FnAlignString("PageNo. " + PageNo, "R", 20));
            }


            else if (RptWidth >= 20)
            {
                tstream.WriteLine(cgen.FnAlignString("From : " + cPublic.g_from.ToString("dd/MM/yyyy"), "L", RptWidth - 10));
                tstream.Write(cgen.FnAlignString("To   : " + cPublic.g_to.ToString("dd/MM/yyyy"), "L", RptWidth - 10));
                tstream.WriteLine(cgen.FnAlignString("PageNo. " + PageNo, "R", 10));
            }
            else
            {
                tstream.WriteLine(cgen.FnAlignString("From : " + cPublic.g_from.ToString("dd/MM/yyyy"), "L", RptWidth));
                tstream.Write(cgen.FnAlignString("To   : " + cPublic.g_to.ToString("dd/MM/yyyy"), "L", RptWidth));
                tstream.WriteLine(cgen.FnAlignString("PageNo. " + PageNo, "R", 10));
            }
            tstream.WriteLine(cgen.Replicate("-", RptWidth));

            for (int iii = 1; iii <= flxStockPrv.Cols.Count - 1; iii++)
            {

                if (flxStockPrv.Cols[iii].Visible == true)
                {

                    lee = flxStockPrv.Cols[iii].ToString().Length;
                    string type = flxStockPrv.Cols[iii].DataType.ToString();
                    if (type.Equals("System.Decimal"))
                    {
                        if (flxStockPrv[0, iii].ToString() == "SALES RETURN")
                        {
                            lee = 29;
                        }
                        else if (flxStockPrv[0, iii].ToString() == "TOTAL COMMISSION")
                        {
                            lee = 29;
                        }
                        else if (flxStockPrv[0, iii].ToString() == "SALES")
                        {
                            lee = 20;
                        }
                        tstream.Write(cgen.FnAlignString(flxStockPrv.GetData(0, iii) + "".ToString(), "R", lee - 13));
                    }
                    else
                    {
                        if (flxStockPrv[0, iii].ToString() == "Bill No")
                        {
                            lee = 18;
                        }
                        else if (flxStockPrv[0, iii].ToString() == "Date" || flxStockPrv[0, iii].ToString() == "DATE")
                        {
                            lee = 20;
                        }
                        else if (flxStockPrv[0, iii].ToString() == "Customer")
                        {
                            lee = 39;
                        }
                        else if (flxStockPrv[0, iii].ToString() == "AGENT NAME")
                        {
                            lee = 41;
                        }
                        tstream.Write(cgen.FnAlignString(flxStockPrv.GetData(0, iii) + "".ToString(), "L", lee - 9));

                    }
                    tstream.Write(" ");
                }

            }

            tstream.WriteLine();
            tstream.WriteLine(cgen.Replicate("-", RptWidth));
            lineno = lineno + 11;
        }

        private void oneline(string path)
        {
            for (ii = 1; ii <= flxStockPrv.Rows.Count - 1; ii++)
            {
                for (jj = 1; jj <= flxStockPrv.Cols.Count - 1; jj++)
                {
                    if (flxStockPrv.Cols[jj].Visible == true)
                    {
                        le = flxStockPrv.Cols[jj].ToString().Length;

                        if (line == false)
                        {
                            if (flxStockPrv[ii, 0] == null && this.Tag + "".ToString() != "SR")
                            {
                                if (day == false)
                                {
                                    for (int i = 1; i <= flxStockPrv.Cols.Count - 1; i++)
                                    {
                                        if (flxStockPrv.Cols[i].Visible == true)
                                        {
                                            le = 25;

                                            if (flxStockPrv.Cols[i].DataType.ToString() == "System.Decimal")
                                            {

                                            }
                                            else
                                            {
                                                if (flxStockPrv[0, i].ToString() == "Bill No")
                                                {
                                                    le = 18;
                                                }

                                                if (flxStockPrv[0, i].ToString() == "Date")
                                                {

                                                    le = 15;
                                                }
                                                else if (flxStockPrv[0, i].ToString() == "Customer")
                                                {
                                                    le = 40;
                                                }
                                            }
                                        }
                                    }
                                }
                                tstream.WriteLine(cgen.Replicate("-", RptWidth));

                                line = true;
                            }
                        }
                        string type = flxStockPrv.Cols[jj].DataType.ToString();
                        if (type.Equals("System.Decimal"))
                        {
                            if (flxStockPrv.GetData(ii, jj) + "".ToString() != "" && flxStockPrv[ii, 0] != null)
                            {
                                if (flxStockPrv.GetData(0, jj) + "".ToString() == "SALES RETURN")
                                {
                                    le = 29;
                                }
                                if (flxStockPrv.GetData(0, jj) + "".ToString() == "SALES")
                                {
                                    le = 21;
                                }
                                if (flxStockPrv.GetData(0, jj) + "".ToString() == "TOTAL COMMISSION")
                                {
                                    le = 29;
                                }
                                tstream.Write(cgen.FnAlignString(Convert.ToDecimal(flxStockPrv.GetData(ii, jj)).ToString("#0.00") + "", "R", le - 14));
                                tstream.Write("  ");
                            }
                            else if (flxStockPrv[ii, 0] == null)
                            {
                                if (flxStockPrv[0, jj].ToString() == "Quantity")
                                {
                                    tstream.Write(cgen.FnAlignString(Convert.ToDecimal(flxStockPrv.GetData(ii, jj)).ToString("#0.000"), "R", le - 14));
                                    tstream.Write("  ");
                                }
                                else
                                {
                                    if (flxStockPrv[0, jj].ToString() == "SALES")
                                    {
                                        le = 21;
                                    }
                                    else if (flxStockPrv[0, jj].ToString() == "SALES RETURN")
                                    {
                                        le = 29;
                                    }
                                    else if (flxStockPrv[0, jj].ToString() == "TOTAL COMMISSION")
                                    {
                                        le = 29;
                                    }

                                    tstream.Write(cgen.FnAlignString(Convert.ToDecimal(flxStockPrv.GetData(ii, jj)).ToString("#0.00"), "R", le - 14));
                                    tstream.Write("  ");
                                }
                            }

                            if (flxStockPrv.GetData(ii, jj) + "".ToString() == "")
                            {
                                tstream.Write(cgen.FnAlignString(" ", "L", le - 13));
                                tstream.Write(" ");
                            }

                        }
                        else if (flxStockPrv.GetData(ii, jj) + "".ToString() == "")
                        {

                            if (flxStockPrv[0, jj].ToString() == "Bill No")
                            {

                                le = 18;
                            }

                            else if (flxStockPrv[0, jj].ToString() == "Date" || flxStockPrv[0, jj].ToString() == "DATE")
                            {

                                le = 20;
                            }
                            else if (flxStockPrv[0, jj].ToString() == "Customer")
                            {

                                le = 40;
                            }

                            else if (flxStockPrv[0, jj].ToString() == "AGENT NAME")
                            {

                                le = 41;
                            }
                            tstream.Write(cgen.FnAlignString(" ", "L", le - 9));
                            tstream.Write(" ");
                        }
                        else
                        {
                            if (flxStockPrv[0, jj].ToString() == "Bill No")
                            {
                                le = 18;
                            }
                            else if (flxStockPrv[0, jj].ToString() == "Date" || flxStockPrv[0, jj].ToString() == "DATE")
                            {
                                le = 20;
                            }
                            else if (flxStockPrv[0, jj].ToString() == "Customer")
                            {
                                le = 40;
                            }
                            else if (flxStockPrv[0, jj].ToString() == "AGENT NAME")
                            {
                                le = 41;
                            }

                            tstream.Write(cgen.FnAlignString(flxStockPrv.GetData(ii, jj) + " ".ToString(), "L", le - 9));
                            tstream.Write(" ");
                        }

                    }

                }
                tstream.WriteLine();
                line = false;
                lineno = lineno + 1;

                if (lineno == G_Lperpage)
                {
                    lin = false;

                }
                else
                {
                    lin = true;
                }


                if (lin == false)
                {
                    if (lineno == G_Lperpage)
                    {
                        if (day == false)
                        {
                            for (int i = 1; i <= flxStockPrv.Cols.Count - 1; i++)
                            {
                                if (flxStockPrv.Cols[i].Visible == true)
                                {

                                    if (flxStockPrv[i, 0] == null)
                                    {
                                        le = 20;
                                    }
                                    else
                                    {
                                        le = 25;
                                    }

                                    if (flxStockPrv.Cols[i].DataType.ToString() == "System.Decimal")
                                    {
                                        if (flxStockPrv[0, i].ToString() == "SALES RETURN")
                                        {
                                            le = 21;
                                        }
                                        if (flxStockPrv[0, i].ToString() == "SALES")
                                        {
                                            le = 19;
                                        }
                                        if (flxStockPrv[0, i].ToString() == "TOTAL COMMISSION")
                                        {
                                            le = 35;
                                        }
                                    }
                                    else
                                    {
                                        if (flxStockPrv[0, i].ToString() == "Bill No")
                                        {

                                            le = 18;
                                        }

                                        if (flxStockPrv[0, i].ToString() == "Date" || flxStockPrv[0, i].ToString() == "DATE")
                                        {

                                            le = 20;
                                        }
                                        else if (flxStockPrv[0, i].ToString() == "Customer")
                                        {

                                            le = 40;
                                        }
                                        else if (flxStockPrv[0, i].ToString() == "AGENT NAME")
                                        {

                                            le = 41;
                                        }

                                        tstream.Write(cgen.FnAlignString(" ", "L", le - 9));
                                        //tstream.Write("  ");
                                    }
                                }
                            }
                            tstream.WriteLine();
                        }

                        tstream.WriteLine(cgen.Replicate("=", RptWidth));
                        tstream.WriteLine(" " + Convert.ToChar(12));
                        lineno = 1;
                        rPagehead(path);
                        lin = true;
                    }
                    //initialize_ColSum();

                }


                for (jj = 1; jj <= flxStockPrv.Cols.Count - 1; jj++)
                {
                    if (flxStockPrv.Cols[jj].Visible == true)
                    {
                        le = flxStockPrv.Cols[jj].ToString().Length;

                        if (flxStockPrv.GetData(ii, jj) + "".ToString() == "SUB TOTAL")
                        {

                            string type1 = flxStockPrv.Cols[jj].DataType.ToString();
                            if (type1.Equals("System.Decimal"))
                            {

                                //tstream.Write(cgen.FnAlignString(flxStockPrv.GetData(ii, jj) + "".ToString(), "R", le - 13));
                            }
                            else
                            {
                                line = true;
                                // tstream.Write(cgen.FnAlignString("                              ", "L", le - 9));
                                tstream.WriteLine(cgen.Replicate("-", RptWidth));

                            }
                        }
                    }
                }

            }

            lin = false;
        }

    }
}
