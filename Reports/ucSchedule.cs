using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.VisualBasic;
using System.IO;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Reports
{
    public partial class ucSchedule : UserControl
    {
        fssgen.fssgen gen = new fssgen.fssgen();
        cGeneral cgen = new cGeneral();
        SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);

        StreamWriter tstream;
        int RptWidth;
        int lineno;
        long PageNo;
        int G_Lperpage = 60;
        Boolean lin = false; 
        int le;
        int lee;
        int hh;
        int ii;
        int jj;
        bool  line = false;
        bool  nu = false; 
        int leng;
        bool day = false;
 
        public ucSchedule()
        {
            InitializeComponent();
        }

        private void ucSchedule_Load(object sender, EventArgs e)
        {
            radCurrentBalance.Checked = true;
            datetime(false);
            cmbbalance.SelectedIndex = 0;
            cmbbalancetype.Text = "ALL";
        }

        private void txtGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { loadLookUp("Groups", string.Empty); }
        }

        private void txtAccounts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { loadLookUp("Accounts", string.Empty); }
        }

        private void txtGroup_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar.Equals('\b')))
            {
                e.Handled = true;
                txtGroup.Text = string.Empty;
                loadLookUp("Groups", txtGroup.Text.Trim());
            }

            if (e.KeyChar.Equals('+'))
            {
                e.Handled = true;
                txtGroup.Text = string.Empty;
                loadLookUp("Groups", txtGroup.Text.Trim());
                return;
            }
            if (!(e.KeyChar.Equals('\b')) && (!(e.KeyChar.Equals('\r'))))
            {
                e.Handled = true;
                txtGroup.Text = string.Empty;
                loadLookUp("Groups", txtGroup.Text.Trim() + e.KeyChar.ToString());
            }
        }

        private void txtAccounts_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar.Equals('\b')))
            {
                e.Handled = true;
                txtAccounts.Text = string.Empty;
                loadLookUp("Accounts", txtAccounts.Text.Trim());
            }

            if (e.KeyChar.Equals('+'))
            {
                e.Handled = true;
                txtAccounts.Text = string.Empty;
                loadLookUp("Accounts", txtAccounts.Text.Trim());
                return;
            }
            if (!(e.KeyChar.Equals('\b')) && (!(e.KeyChar.Equals('\r'))))
            {
                e.Handled = true;
                txtAccounts.Text = string.Empty;
                loadLookUp("Accounts", txtAccounts.Text.Trim() + e.KeyChar.ToString());
            }
        }

        private void chkupto_CheckedChanged(object sender, EventArgs e)
        {
            if (chkupto.Checked == true)
            {
                dtpfrom.Enabled = false;
                dtpto.Enabled = false;
            }
            else
            {
                dtpfrom.Enabled = true;
                dtpto.Enabled = true;
            }
        }

        private void loadLookUp(string TableName, string str)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;
            lookup.m_strat = str;
            if (TableName == "Accounts")
            {
                lookup.m_table = "ACCOUNTS" + cPublic.g_firmcode;
                lookup.m_fields = "HEAD,CODE,ADDRESS, STREET, CITY,Balance";
                lookup.m_dispname = "HEAD,CODE,ADDRESS, STREET, CITY,BALANCE";
                lookup.m_caption = "Customer";
                lookup.m_fldwidth = "300,0,100,100,100,100";
            }
            else
            {
                lookup.m_table = "GROUPS" + cPublic.g_firmcode;
                lookup.m_fields = "HEAD,CODE";

                lookup.m_caption = "Group Head";
                lookup.m_fldwidth = "300,0,100,100";
            }
            lookup.ShowDialog();

            if (lookup.m_values.Count > 0)
            {
                if (TableName == "Accounts")
                {
                    txtAccounts.Text = lookup.m_values[0].ToString();
                    txtAccounts.Tag = lookup.m_values[1].ToString();

                    txtGroup.Text = "";
                    txtGroup.Tag = "";
                }
                else
                {
                    txtGroup.Text = lookup.m_values[0].ToString();
                    txtGroup.Tag = lookup.m_values[1].ToString();

                    txtAccounts.Text = "";
                    txtAccounts.Tag = "";
                }
            }
            else
            {
                if (TableName == "Accounts")
                {
                    txtAccounts.Text = string.Empty;
                }
                else
                { txtGroup.Text = string.Empty; }
            }
            SendKeys.Send("{Tab}");

        }

        private void radDatewise_CheckedChanged(object sender, EventArgs e)
        {
            if (radDatewise.Checked == true)
            { datetime(true); }
            else
            { datetime(false); }
        }

        public void datetime(bool status)
        {
            dtpfrom.Enabled = status;
            dtpto.Enabled = status;
            dtpupto.Enabled = status;
            chkupto.Enabled = status;
        }

        ArrayList vGroups = new ArrayList();

        private string GroupDetails(string GroupCode)
        {
            vGroups = new ArrayList();
            vGroups.Add(GroupCode);

            string sk = Groups(GroupCode);
            sk = "";
            for (int j = 0; j < vGroups.Count; j++)
            {
                if (sk.Trim() != "")
                { sk = sk + ",'" + vGroups[j].ToString() + "'"; }
                else
                { sk = "'" + vGroups[j].ToString() + "'"; }
            }
            sk = " SELECT * FROM [ACCOUNTS" + cPublic.g_firmcode + "] WHERE GROUPCODE IN (" + sk + ")";
            return sk;
        }

        private string Groups(string GroupCode)
        {
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Clear();
            cmd.CommandText = "SELECT * FROM [GROUPS" + cPublic.g_firmcode + "] WHERE GROUPCODE='" + GroupCode + "'";
            SqlDataAdapter Rn = new SqlDataAdapter(cmd);
            DataTable Sn = new DataTable();
            Rn.Fill(Sn);
            if (Sn.Rows.Count > 0)
            {
                vGroups.Add(Sn.Rows[0]["code"].ToString());
                Groups(Sn.Rows[0]["code"].ToString());
            }
            else
            { return ""; }
            return "";
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            common();
        }

        private void common()
        {
            //if (this.txtAccounts.Text.Trim() == "" && this.txtGroup.Text.Trim() == "")
            //{
            //    MessageBox.Show("Account Code Or Group Code Connot Be Blank");
            //    txtAccounts.Focus();
            //    return;
            //}
            string sql = "";
            string vTe = "Schedule of ";
            string cond = string.Empty;
            string vFil = "";
            string balance = " ";
            string vdate = "", vchq=string.Empty ;

            if (txtAccounts.Text.Trim() != string.Empty)
            { cond += " and code in ('" + gen.SQLFormat(this.txtAccounts.Tag.ToString()) + "') "; }
            if (txtGroup.Text.Trim() != string.Empty)
            { cond += "  and code in (select code from ( " + GroupDetails(gen.SQLFormat(this.txtGroup.Tag + "".ToString()) + "") + " ) Sn) "; }
            else { cond += "  and head is not null"; }

            vdate = " ( A.CODE<='200002' and bookcode = A.CODE ) OR "
                   + " (A.CODE>'200002' and bookcode = A.CODE AND chqpassed = 1) OR "
                   + " (bookcode <= '200002' AND CODE = A.CODE) OR "
                   + " (bookcode > '200002' and code = A.CODE AND chqpassed= 1) ";

            if (rdbopbalance.Checked == true)
            {
                vFil = " CASE WHEN OPBALANCE<0 THEN ABS(OPBALANCE) ELSE 0.00 END  as [DEBIT],CASE WHEN OPBALANCE>0 THEN ABS(OPBALANCE) ELSE 0.00 END AS CREDIT ";
            }
            else
            {
                vFil = " CASE WHEN BALANCE<0 THEN ABS(BALANCE) ELSE 0.00 END  as [DEBIT],CASE WHEN BALANCE>0 THEN ABS(BALANCE) ELSE 0.00 END AS CREDIT ";
            }

            if (chqPass.Checked == false)
            {
                sql = " select * from (select code, head AS [HEAD]," + vFil + "  from Accounts" + cPublic.g_firmcode + " a Where  code is not null " + cond + " and HEAD<>''  " + balance + " )SNS  "
                    + "  ";
            }
            else if (chqPass.Checked == true)
            {
                sql = " select * from (select code,head, isnull((select CASE WHEN  (opbalance + isnull(SUM(amount),0))<0 THEN (opbalance + isnull(SUM(amount),0))*-1  ELSE 0.00 END from Voucher" + cPublic.g_firmcode + " S Where  code=a.code and  (" + vdate + " )),0) DEBIT "
                 + ",isnull((select CASE WHEN  (opbalance + isnull(SUM(amount),0))<=0 THEN 0.00 ELSE (opbalance + isnull(SUM(amount),0)) END AS CREDIT  from Voucher" + cPublic.g_firmcode + " S Where code=a.code and  (" + vdate + " )  ),0) CREDIT "
                 + "  from  Accounts" + cPublic.g_firmcode + " a Where  code is not null " + cond + " ) SNS    ";
            }

            if (chkupto.Checked == true)
            {
                if (chqPass.Checked == true)
                {
                    vdate = " (date1 <=  '" + this.dtpupto.Value.ToString("yyyy/MM/dd") + "'   AND  A.CODE<='200002' and bookcode = A.CODE ) OR "
                   + " (chqpassdate <=  '" + this.dtpupto.Value.ToString("yyyy/MM/dd") + "'  AND  A.CODE>'200002' and bookcode = A.CODE AND chqpassed = 1) OR "
                   + " (date1 <=  '" + this.dtpupto.Value.ToString("yyyy/MM/dd") + "'  AND bookcode <= '200002' AND CODE = A.CODE) OR "
                   + " (chqpassdate <=  '" + this.dtpupto.Value.ToString("yyyy/MM/dd") + "'  AND bookcode > '200002' and code = A.CODE AND chqpassed= 1) ";
                }
                else
                {
                    vdate = " date1<='" + this.dtpupto.Value.ToString("yyyy-MM-dd") + "'";
                }
            }

            if (chqPass.Checked == true)
            {
                vchq = " and chqpassed=1";
            }

            if (rdbopbalance.Checked == true)
            { vTe += "(Opening Balance)"; }
            else if (radCurrentBalance.Checked == true)
            { vTe += "(Current Balance)"; }
            else if (radDatewise.Checked == true)
            {
                if (cmbbalance.Text.Trim() == "Y") { balance = " and balance<>0 "; } else { balance = ""; }

                string vopb = " ";
                if (chkupto.Checked == false)
                {
                    if (chqPass.Checked == true)
                    {
                        vTe += "- Datewise ( From " + dtpfrom.Value.ToString("yyyy-MM-dd") + "  To " + dtpto.Value.ToString("yyyy-MM-dd") + " )";

                        vdate = " (date1 between '" + this.dtpfrom.Value.ToString("yyyy-MM-dd") + "' and '" + this.dtpto.Value.ToString("yyyy-MM-dd") + "'   AND  A.CODE<='200002' and bookcode = A.CODE ) OR "
                          + " (chqpassdate between '" + this.dtpfrom.Value.ToString("yyyy-MM-dd") + "' and '" + this.dtpto.Value.ToString("yyyy-MM-dd") + "'  AND  A.CODE>'200002' and bookcode = A.CODE AND chqpassed = 1) OR "
                          + " (date1 between '" + this.dtpfrom.Value.ToString("yyyy-MM-dd") + "' and '" + this.dtpto.Value.ToString("yyyy-MM-dd") + "' AND bookcode <= '200002' AND CODE = A.CODE) OR "
                          + " (chqpassdate between '" + this.dtpfrom.Value.ToString("yyyy-MM-dd") + "' and '" + this.dtpto.Value.ToString("yyyy-MM-dd") + "'  AND bookcode > '200002' and code = A.CODE AND chqpassed= 1) ";
                    }
                    else
                    {
                        vTe += "- Datewise ( From " + dtpfrom.Value.ToString("yyyy-MM-dd") + "  To " + dtpto.Value.ToString("yyyy-MM-dd") + " )";
                        vdate = " (date1 between '" + dtpfrom.Value.ToString("yyyy-MM-dd") + "'  and '" + dtpto.Value.ToString("yyyy-MM-dd") + "'  ) ";
                    }
                }
                else
                {
                    if (chqPass.Checked == true)
                    {
                        vTe += "- Upto  " + dtpupto.Value.ToString("yyyy-MM-dd") + " ";

                        vdate = " (date1 <=  '" + this.dtpupto.Value.ToString("yyyy/MM/dd") + "'   AND  A.CODE<='200002' and bookcode = A.CODE ) OR "
                           + " (chqpassdate <=  '" + this.dtpupto.Value.ToString("yyyy/MM/dd") + "'  AND  A.CODE>'200002' and bookcode = A.CODE AND chqpassed = 1) OR "
                           + " (date1 <=  '" + this.dtpupto.Value.ToString("yyyy/MM/dd") + "'  AND bookcode <= '200002' AND CODE = A.CODE) OR "
                           + " (chqpassdate <=  '" + this.dtpupto.Value.ToString("yyyy/MM/dd") + "'  AND bookcode > '200002' and code = A.CODE AND chqpassed= 1) ";
                        vopb = " opbalance + ";
                    }
                    else
                    {
                        vTe += "- Upto  " + dtpupto.Value.ToString("yyyy-MM-dd") + " ";
                        vdate = " date1<= '" + dtpupto.Value.ToString("yyyy-MM-dd") + "' ";
                        vopb = " opbalance + ";
                    }
                }
                sql = "select * from ( select code,head, isnull((select CASE WHEN  (" + vopb + " isnull(SUM(amount),0))<0 THEN (" + vopb + " isnull(SUM(amount),0))*-1  ELSE 0.00 END from Voucher" + cPublic.g_firmcode + " S Where  code=a.code and  (" + vdate + " )),0) DEBIT " + Environment.NewLine
                    + ",isnull((select CASE WHEN  (" + vopb + " isnull(SUM(amount),0))<=0 THEN 0.00 ELSE (" + vopb + " isnull(SUM(amount),0)) END AS CREDIT  from Voucher" + cPublic.g_firmcode + " S Where code=a.code and  (" + vdate + " )  ),0) CREDIT " + Environment.NewLine
                    + "  from  Accounts" + cPublic.g_firmcode + " a Where  code is not null " + cond + " ) SNS    " + Environment.NewLine;
            }

            balance = " Where [debit] is not null ";
            if (cmbbalance.Text == "Y")
            { balance += " and ([debit]<>0 or [credit]<>0) "; }

            if (cmbbalancetype.Text.Trim().ToUpper() == "CREDIT")
            { balance += " and [credit]<>0 "; }
            else if (cmbbalancetype.Text.Trim().ToUpper() == "DEBIT")
            { balance += " and [debit]<>0 "; }

            sql = "select CODE,HEAD,DEBIT,CREDIT from (" + sql + ")[t1] " + balance + " order by head";

            GridIntial();
            Display_Screen(sql);
        }

        private void Display_Screen(string  vSql)
        {
            this.cfgday.Clear();
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text; 
            cmd.CommandText =vSql;
            SqlDataAdapter ap = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            ap.Fill(dt);
            this.cfgday.DataSource = dt;

            cfgday.Cols[0].Visible = false;
            cfgday.Cols[1].Width = 500;
            cfgday.Cols[2].Width = 134;
            cfgday.Cols[3].Width = 131;

            cfgday.Cols[2].Format = "##,###0.00";
            cfgday.Cols[3].Format = "##,###0.00";
            cfgday.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
            cfgday.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, -1, -1, 2, "Subtotal");
            cfgday.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, -1, -1, 3, "Subtotal");


            if (dt.Rows.Count == 0)
            { MessageBox.Show("No Record to display", cPublic.messagename); }
            else
            { cfgday[cfgday.Rows.Count - 1, 1] = "Grand Total"; }
        }

        private void GridIntial()
        {
            cfgday.Clear();
            cfgday.Cols.Count = 3;
            cfgday.Cols.Fixed = 0;

            cfgday.Cols[0].Name = "head";
            cfgday.Cols["head"].Width = 300;
            cfgday[0, "head"] = "Head";


            cfgday.Cols[1].Name = "debit";
            cfgday.Cols["debit"].Width = 134;
            cfgday[0, "debit"] = "Debit";

            cfgday.Cols[2].Name = "credit";
            cfgday.Cols["credit"].Width = 131;
            cfgday[0, "credit"] = "Crebit";
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            cgen.RepPrint(PrintDos().ToString());
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Interaction.Shell("notepad.exe  " + cPublic.FilePath + "\\" + PrintDos() + ".txt", AppWinStyle.MaximizedFocus, true, 1);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataExport data = new DataExport();
            data.flex_Excel(cfgday);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private int PrintDos()
        {
            leng = 0;
            RptWidth = 78;

            PageNo = 0;
            lineno = 0;
            lee = 0;
            le = 0;

            int notepadpath = (Convert.ToInt32(1000 * VBMath.Rnd()) + 1);

            if (Directory.Exists(cPublic.FilePath + "\\" + notepadpath + ".txt"))
            {
                Directory.Delete(cPublic.FilePath + "\\" + notepadpath + ".txt");
            }

            string path = cPublic.FilePath + "\\" + notepadpath + ".txt";

            tstream = File.CreateText(path);
            for (int i = 1; i < Convert.ToInt32(cPublic.LinestoReverse); i++)
            {//  lines to reverse
                tstream.Write(Strings.Chr(27) + "j" + Strings.Chr(36));
            }
            rPagehead(path);
            oneline(path);
            ii = 0;
            tstream.WriteLine(cgen.Replicate("=", RptWidth));



            tstream.WriteLine(Convert.ToChar(12));
            tstream.Flush();
            tstream.Close();

            return notepadpath ;
        }

        private void rPagehead(string path)
        {
            PageNo = PageNo + 1;
            //cPublic.g_rpthead = "SCHEDULE";
            tstream.Write(cPublic.ChN);
            tstream.WriteLine(cPublic.BchDW);
            tstream.WriteLine(cgen.FnAlignString(cPublic.g_CompName, "C", 35)); 
            tstream.Write(cPublic.BchNDW);
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add1, "C", RptWidth));
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add2, "C", RptWidth));
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add3, "C", RptWidth));
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add4, "C", RptWidth));
                tstream.WriteLine(cgen.FnAlignString("Tin No:" + cPublic.g_TinNo, "C", RptWidth));
            tstream.Write(cPublic.ChN);
            tstream.WriteLine(cgen.FnAlignString(this.Text.Trim(), "C", RptWidth));
            tstream.Write(cgen.FnAlignString("Date : " + cPublic.g_from.ToString("dd/MM/yyyy"), "L", 20));
            tstream.WriteLine(cgen.FnAlignString("PageNo." + PageNo, "R", RptWidth - 20));
            tstream.WriteLine(cgen.Replicate("-", RptWidth));
            tstream.WriteLine(cgen.FnAlignString("HEAD                                                     DEBIT          CREDIT", "L", RptWidth));
            tstream.WriteLine(cgen.Replicate("-", RptWidth));
            lineno = lineno + 11;
        }

        private void oneline(string path)
        {

            for (ii = 1; ii <= cfgday.Rows.Count - 1; ii++)
            {
                for (jj = 1; jj <= cfgday.Cols.Count - 1; jj++)
                {
                    if (cfgday.Cols[jj].Visible == true)
                    {
                        le = cfgday.Cols[jj].ToString().Length;

                        if (line == false)
                        {
                            if (cfgday[ii, 0] == null && this.Tag + "".ToString() != "SR")
                            {
                                if (day == false)
                                {

                                    for (int i = 1; i <= cfgday.Cols.Count - 1; i++)
                                    {
                                        if (cfgday.Cols[i].Visible == true)
                                        {
                                            // 
                                            le = 25;

                                        }
                                    }

                                    //tstream.WriteLine();
                                }

                                tstream.WriteLine(cgen.Replicate("-", RptWidth));

                                line = true;
                            }

                        }
                        string type = cfgday.Cols[jj].DataType.ToString();
                        if (type.Equals("System.Decimal"))
                        {
                            if (cfgday.GetData(ii, jj) + "".ToString() != "" && cfgday[ii, 0] != null)
                            {

                                //  tstream.Write(cgen.FnAlignString(Convert.ToDecimal (flxStockPrv.GetData(ii, jj)) .ToString("##,###0.00"), "R", le - 14));
                                if (cfgday.GetData(0, jj) + "" == "CREDIT")
                                {
                                    tstream.Write(cgen.FnAlignString(cfgday.GetData(ii, jj) + "", "R", le - 9));
                                }
                                else
                                {
                                    tstream.Write(cgen.FnAlignString(cfgday.GetData(ii, jj) + "", "R", le - 2));
                                }
                                tstream.Write(" ");
                            }
                            else if (cfgday[ii, 0] == null)
                            {
                                if (cfgday.GetData(0, jj) + "" == "CREDIT")
                                {
                                    tstream.Write(cgen.FnAlignString(Convert.ToDecimal(cfgday.GetData(ii, jj)).ToString("##,###0.00"), "R", le - 10));
                                }
                                else if (cfgday.GetData(0, jj) + "" == "Diff")
                                {
                                    if (Information.IsNumeric(cfgday.GetData(ii, jj)) == true)
                                        tstream.Write(cgen.FnAlignString(Convert.ToDecimal(cfgday.GetData(ii, jj)).ToString("##,###0.00"), "R", le - 11));
                                }
                                else
                                {
                                    if (Information.IsNumeric(cfgday.GetData(ii, jj)) == true)
                                    { tstream.Write(cgen.FnAlignString(Convert.ToDecimal(cfgday.GetData(ii, jj)).ToString("##,###0.00"), "R", le - 3)); }
                                    else { tstream.Write(cgen.FnAlignString(cfgday.GetData(ii, jj) + "", "R", le - 3)); }
                                }
                                tstream.Write("  ");
                            }

                        }
                        else if (cfgday.GetData(ii, jj) + "".ToString() == "")
                        {

                            tstream.Write(cgen.FnAlignString(" ", "L", le + 15));
                            tstream.Write(" ");
                        }
                        else if (cfgday.GetData(0, jj) + "" == "BALANCE")
                        {

                            tstream.Write(cgen.FnAlignString(cfgday.GetData(ii, jj) + " ".ToString(), "R", le - 10));
                            tstream.Write(" ");
                        }
                        else if (cfgday.GetData(0, jj) + "" == "Balance1")
                        {
                            tstream.Write(cgen.FnAlignString(cfgday.GetData(ii, jj) + " ".ToString(), "R", le - 6));
                            tstream.Write(" ");
                        }

                        else
                        {

                            tstream.Write(cgen.FnAlignString(cfgday.GetData(ii, jj) + " ".ToString(), "L", le + 15));
                            tstream.Write(" ");
                        }

                    }

                }
                tstream.WriteLine();
                line = false;
                lineno = lineno + 1;

                if (lineno == G_Lperpage)
                {
                    lin = false;

                }
                else
                {
                    lin = true;
                }

                if (lin == false)
                {
                    if (lineno == G_Lperpage)
                    {
                        if (day == false)
                        {

                            for (int i = 1; i <= cfgday.Cols.Count - 1; i++)
                            {
                                if (cfgday.Cols[i].Visible == true)
                                {

                                    if (cfgday[i, 0] == null)
                                    {
                                        le = 20;
                                    }
                                    else
                                    {
                                        le = 25;
                                    }

                                    if (cfgday.Cols[i].DataType.ToString() == "System.Decimal")
                                    {
                                        // tstream.Write(cgen.FnAlignString(colSum[i].ToString(), "R", le - 13));
                                    }

                                    else
                                    {


                                        tstream.Write(cgen.FnAlignString(" ", "L", le - 9));
                                        //tstream.Write("  ");
                                    }
                                }
                            }
                            tstream.WriteLine();
                        }

                        tstream.WriteLine(cgen.Replicate("=", RptWidth));
                        tstream.WriteLine(" " + Convert.ToChar(12));
                        lineno = 1;
                        rPagehead(path);
                        lin = true;
                    }
                    //   initialize_ColSum();

                }

            }

            lin = false;
            nu = false;

        }
    }
}
