namespace Xpire.Reports
{
    partial class ucEmployeeReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucEmployeeReport));
            this.btnClose = new System.Windows.Forms.Button();
            this.txttax = new System.Windows.Forms.TextBox();
            this.txtItemname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtProduct = new System.Windows.Forms.TextBox();
            this.txtManufacturer = new System.Windows.Forms.TextBox();
            this.txtItemcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.flxStockPrv = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtGrade = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.flxStockPrv)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnClose.Location = new System.Drawing.Point(773, 589);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txttax
            // 
            this.txttax.BackColor = System.Drawing.Color.White;
            this.txttax.Location = new System.Drawing.Point(142, 63);
            this.txttax.Name = "txttax";
            this.txttax.Size = new System.Drawing.Size(252, 20);
            this.txttax.TabIndex = 16;
            this.txttax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txttax_KeyDown);
            this.txttax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttax_KeyPress);
            // 
            // txtItemname
            // 
            this.txtItemname.BackColor = System.Drawing.Color.White;
            this.txtItemname.Location = new System.Drawing.Point(531, 12);
            this.txtItemname.Name = "txtItemname";
            this.txtItemname.Size = new System.Drawing.Size(251, 20);
            this.txtItemname.TabIndex = 10;
            this.txtItemname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemname_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(412, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Staff Name";
            // 
            // txtProduct
            // 
            this.txtProduct.BackColor = System.Drawing.Color.White;
            this.txtProduct.Location = new System.Drawing.Point(531, 38);
            this.txtProduct.Name = "txtProduct";
            this.txtProduct.Size = new System.Drawing.Size(251, 20);
            this.txtProduct.TabIndex = 14;
            this.txtProduct.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProduct_KeyDown);
            this.txtProduct.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProduct_KeyPress);
            // 
            // txtManufacturer
            // 
            this.txtManufacturer.BackColor = System.Drawing.Color.White;
            this.txtManufacturer.Location = new System.Drawing.Point(142, 37);
            this.txtManufacturer.Name = "txtManufacturer";
            this.txtManufacturer.Size = new System.Drawing.Size(251, 20);
            this.txtManufacturer.TabIndex = 12;
            this.txtManufacturer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtManufacturer_KeyDown);
            this.txtManufacturer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtManufacturer_KeyPress);
            // 
            // txtItemcode
            // 
            this.txtItemcode.BackColor = System.Drawing.Color.White;
            this.txtItemcode.Location = new System.Drawing.Point(142, 11);
            this.txtItemcode.Name = "txtItemcode";
            this.txtItemcode.Size = new System.Drawing.Size(251, 20);
            this.txtItemcode.TabIndex = 8;
            this.txtItemcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemcode_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(412, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Qualification";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Designation";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Staff Id";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(15, 92);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(107, 23);
            this.btnLoad.TabIndex = 22;
            this.btnLoad.Text = "&Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // flxStockPrv
            // 
            this.flxStockPrv.AllowEditing = false;
            this.flxStockPrv.AllowFreezing = C1.Win.C1FlexGrid.AllowFreezingEnum.Both;
            this.flxStockPrv.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.flxStockPrv.ColumnInfo = resources.GetString("flxStockPrv.ColumnInfo");
            this.flxStockPrv.ExtendLastCol = true;
            this.flxStockPrv.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.flxStockPrv.Font = new System.Drawing.Font("Verdana", 9F);
            this.flxStockPrv.Location = new System.Drawing.Point(15, 121);
            this.flxStockPrv.Name = "flxStockPrv";
            this.flxStockPrv.Rows.DefaultSize = 21;
            this.flxStockPrv.Rows.MinSize = 22;
            this.flxStockPrv.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.flxStockPrv.Size = new System.Drawing.Size(835, 438);
            //this.flxStockPrv.StyleInfo = resources.GetString("flxStockPrv.StyleInfo");
            this.flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
            this.flxStockPrv.TabIndex = 23;
            this.flxStockPrv.TabStop = false;
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPrint.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPrint.Location = new System.Drawing.Point(470, 589);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 32);
            this.btnPrint.TabIndex = 26;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.White;
            this.btnView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnView.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnView.Location = new System.Drawing.Point(550, 589);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(82, 32);
            this.btnView.TabIndex = 25;
            this.btnView.Text = "&View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Visible = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.White;
            this.btnExport.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnExport.Location = new System.Drawing.Point(638, 589);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(129, 32);
            this.btnExport.TabIndex = 24;
            this.btnExport.Text = "&Export - Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Department";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(412, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Grade";
            // 
            // txtGrade
            // 
            this.txtGrade.BackColor = System.Drawing.Color.White;
            this.txtGrade.Location = new System.Drawing.Point(531, 63);
            this.txtGrade.Name = "txtGrade";
            this.txtGrade.Size = new System.Drawing.Size(251, 20);
            this.txtGrade.TabIndex = 14;
            this.txtGrade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGrade_KeyDown);
            this.txtGrade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGrade_KeyPress);
            // 
            // ucEmployeeReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.flxStockPrv);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.txttax);
            this.Controls.Add(this.txtItemname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtGrade);
            this.Controls.Add(this.txtProduct);
            this.Controls.Add(this.txtManufacturer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtItemcode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Name = "ucEmployeeReport";
            this.Size = new System.Drawing.Size(877, 648);
            this.Load += new System.EventHandler(this.ucStockDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flxStockPrv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.TextBox txttax;
        public System.Windows.Forms.TextBox txtItemname;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtProduct;
        public System.Windows.Forms.TextBox txtManufacturer;
        public System.Windows.Forms.TextBox txtItemcode;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLoad;
        public C1.Win.C1FlexGrid.C1FlexGrid flxStockPrv;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnExport;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtGrade;
    }
}
