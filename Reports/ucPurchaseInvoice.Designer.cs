namespace Xpire.Reports
{
    partial class ucPurchaseInvoice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucPurchaseInvoice));
            this.btnClose = new System.Windows.Forms.Button();
            this.DtpTo = new System.Windows.Forms.DateTimePicker();
            this.DtpFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.txtSupp = new System.Windows.Forms.TextBox();
            this.txtTaxPer = new System.Windows.Forms.TextBox();
            this.lblTax = new System.Windows.Forms.Label();
            this.lblSupp = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.flxStockPrv = new C1.Win.C1FlexGrid.C1FlexGrid();
            ((System.ComponentModel.ISupportInitialize)(this.flxStockPrv)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnClose.Location = new System.Drawing.Point(713, 562);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // DtpTo
            // 
            this.DtpTo.CustomFormat = "dd/MM/yyyy";
            this.DtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpTo.Location = new System.Drawing.Point(328, 13);
            this.DtpTo.Name = "DtpTo";
            this.DtpTo.Size = new System.Drawing.Size(144, 20);
            this.DtpTo.TabIndex = 9;
            // 
            // DtpFrom
            // 
            this.DtpFrom.CustomFormat = "dd/MM/yyyy";
            this.DtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFrom.Location = new System.Drawing.Point(120, 13);
            this.DtpFrom.Name = "DtpFrom";
            this.DtpFrom.Size = new System.Drawing.Size(144, 20);
            this.DtpFrom.TabIndex = 8;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTo.Location = new System.Drawing.Point(281, 15);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(23, 15);
            this.lblTo.TabIndex = 10;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrom.Location = new System.Drawing.Point(19, 15);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(40, 15);
            this.lblFrom.TabIndex = 7;
            this.lblFrom.Text = "From";
            // 
            // txtSupp
            // 
            this.txtSupp.Location = new System.Drawing.Point(120, 74);
            this.txtSupp.Name = "txtSupp";
            this.txtSupp.Size = new System.Drawing.Size(310, 20);
            this.txtSupp.TabIndex = 13;
            this.txtSupp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSupp_KeyDown);
            this.txtSupp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSupp_KeyPress);
            // 
            // txtTaxPer
            // 
            this.txtTaxPer.Location = new System.Drawing.Point(121, 46);
            this.txtTaxPer.Name = "txtTaxPer";
            this.txtTaxPer.Size = new System.Drawing.Size(100, 20);
            this.txtTaxPer.TabIndex = 12;
            this.txtTaxPer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTaxPer_KeyDown);
            this.txtTaxPer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTaxPer_KeyPress);
            // 
            // lblTax
            // 
            this.lblTax.AutoSize = true;
            this.lblTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTax.Location = new System.Drawing.Point(19, 47);
            this.lblTax.Name = "lblTax";
            this.lblTax.Size = new System.Drawing.Size(46, 15);
            this.lblTax.TabIndex = 16;
            this.lblTax.Text = "Tax %";
            // 
            // lblSupp
            // 
            this.lblSupp.AutoSize = true;
            this.lblSupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSupp.Location = new System.Drawing.Point(19, 78);
            this.lblSupp.Name = "lblSupp";
            this.lblSupp.Size = new System.Drawing.Size(61, 15);
            this.lblSupp.TabIndex = 14;
            this.lblSupp.Text = "Supplier";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(490, 74);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(97, 23);
            this.btnLoad.TabIndex = 17;
            this.btnLoad.Text = "&Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPrint.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPrint.Location = new System.Drawing.Point(400, 562);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 32);
            this.btnPrint.TabIndex = 27;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.White;
            this.btnView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnView.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnView.Location = new System.Drawing.Point(481, 562);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(88, 32);
            this.btnView.TabIndex = 26;
            this.btnView.Text = "&View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.White;
            this.btnExport.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnExport.Location = new System.Drawing.Point(575, 562);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(132, 32);
            this.btnExport.TabIndex = 25;
            this.btnExport.Text = "&Export - Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // flxStockPrv
            // 
            this.flxStockPrv.AllowEditing = false;
            this.flxStockPrv.AllowFreezing = C1.Win.C1FlexGrid.AllowFreezingEnum.Both;
            this.flxStockPrv.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.flxStockPrv.BackColor = System.Drawing.SystemColors.Window;
            this.flxStockPrv.ColumnInfo = resources.GetString("flxStockPrv.ColumnInfo");
            this.flxStockPrv.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.flxStockPrv.Font = new System.Drawing.Font("Verdana", 9F);
            this.flxStockPrv.Location = new System.Drawing.Point(9, 103);
            this.flxStockPrv.Name = "flxStockPrv";
            this.flxStockPrv.Rows.MinSize = 22;
            this.flxStockPrv.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.flxStockPrv.Size = new System.Drawing.Size(795, 453);
            this.flxStockPrv.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("flxStockPrv.Styles"));
            this.flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
            this.flxStockPrv.TabIndex = 28;
            this.flxStockPrv.TabStop = false;
            // 
            // ucPurchaseInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.Controls.Add(this.flxStockPrv);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.txtSupp);
            this.Controls.Add(this.txtTaxPer);
            this.Controls.Add(this.lblTax);
            this.Controls.Add(this.lblSupp);
            this.Controls.Add(this.DtpTo);
            this.Controls.Add(this.DtpFrom);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.lblFrom);
            this.Controls.Add(this.btnClose);
            this.Name = "ucPurchaseInvoice";
            this.Size = new System.Drawing.Size(807, 606);
            ((System.ComponentModel.ISupportInitialize)(this.flxStockPrv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DateTimePicker DtpTo;
        private System.Windows.Forms.DateTimePicker DtpFrom;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.TextBox txtSupp;
        public System.Windows.Forms.TextBox txtTaxPer;
        private System.Windows.Forms.Label lblTax;
        private System.Windows.Forms.Label lblSupp;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnPrint;
        public System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnExport;
        public C1.Win.C1FlexGrid.C1FlexGrid flxStockPrv;
    }
}
