namespace Xpire.Reports
{
    partial class ucDocumentReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDocumentReport));
            this.btnClose = new System.Windows.Forms.Button();
            this.txtDocName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDocNo = new System.Windows.Forms.TextBox();
            this.txtDocType = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.flxStockPrv = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpExpOn = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.chkExpOn = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.flxStockPrv)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnClose.Location = new System.Drawing.Point(773, 589);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtDocName
            // 
            this.txtDocName.BackColor = System.Drawing.Color.White;
            this.txtDocName.Location = new System.Drawing.Point(142, 63);
            this.txtDocName.Name = "txtDocName";
            this.txtDocName.Size = new System.Drawing.Size(278, 20);
            this.txtDocName.TabIndex = 10;
            this.txtDocName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemname_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Document Name";
            // 
            // txtDocNo
            // 
            this.txtDocNo.BackColor = System.Drawing.Color.White;
            this.txtDocNo.Location = new System.Drawing.Point(142, 37);
            this.txtDocNo.Name = "txtDocNo";
            this.txtDocNo.Size = new System.Drawing.Size(278, 20);
            this.txtDocNo.TabIndex = 12;
            this.txtDocNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtManufacturer_KeyDown);
            this.txtDocNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtManufacturer_KeyPress);
            // 
            // txtDocType
            // 
            this.txtDocType.BackColor = System.Drawing.Color.White;
            this.txtDocType.Location = new System.Drawing.Point(142, 11);
            this.txtDocType.Name = "txtDocType";
            this.txtDocType.Size = new System.Drawing.Size(278, 20);
            this.txtDocType.TabIndex = 8;
            this.txtDocType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemcode_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Document No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document Type";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(15, 92);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(107, 23);
            this.btnLoad.TabIndex = 22;
            this.btnLoad.Text = "&Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // flxStockPrv
            // 
            this.flxStockPrv.AllowEditing = false;
            this.flxStockPrv.AllowFreezing = C1.Win.C1FlexGrid.AllowFreezingEnum.Both;
            this.flxStockPrv.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.flxStockPrv.ColumnInfo = resources.GetString("flxStockPrv.ColumnInfo");
            this.flxStockPrv.ExtendLastCol = true;
            this.flxStockPrv.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.flxStockPrv.Font = new System.Drawing.Font("Verdana", 9F);
            this.flxStockPrv.Location = new System.Drawing.Point(15, 121);
            this.flxStockPrv.Name = "flxStockPrv";
            this.flxStockPrv.Rows.DefaultSize = 21;
            this.flxStockPrv.Rows.MinSize = 22;
            this.flxStockPrv.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.flxStockPrv.Size = new System.Drawing.Size(835, 438);
            //this.flxStockPrv.StyleInfo = resources.GetString("flxStockPrv.StyleInfo");
            this.flxStockPrv.SubtotalPosition = C1.Win.C1FlexGrid.SubtotalPositionEnum.BelowData;
            this.flxStockPrv.TabIndex = 23;
            this.flxStockPrv.TabStop = false;
            this.flxStockPrv.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.flxStockPrv_MouseDoubleClick);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPrint.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPrint.Location = new System.Drawing.Point(470, 589);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 32);
            this.btnPrint.TabIndex = 26;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.White;
            this.btnView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnView.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnView.Location = new System.Drawing.Point(550, 589);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(82, 32);
            this.btnView.TabIndex = 25;
            this.btnView.Text = "&View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Visible = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.White;
            this.btnExport.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnExport.Location = new System.Drawing.Point(638, 589);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(129, 32);
            this.btnExport.TabIndex = 24;
            this.btnExport.Text = "&Export - Excel";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(515, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "From";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(515, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "To";
            // 
            // dtpExpOn
            // 
            this.dtpExpOn.CustomFormat = "dd/MMM/yyyy";
            this.dtpExpOn.Enabled = false;
            this.dtpExpOn.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpExpOn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpOn.Location = new System.Drawing.Point(630, 6);
            this.dtpExpOn.Name = "dtpExpOn";
            this.dtpExpOn.Size = new System.Drawing.Size(115, 24);
            this.dtpExpOn.TabIndex = 27;
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd/MMM/yyyy";
            this.dtpTo.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(630, 59);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(115, 24);
            this.dtpTo.TabIndex = 27;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd/MMM/yyyy";
            this.dtpFrom.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(630, 33);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(115, 24);
            this.dtpFrom.TabIndex = 27;
            // 
            // chkExpOn
            // 
            this.chkExpOn.AutoSize = true;
            this.chkExpOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExpOn.Location = new System.Drawing.Point(499, 11);
            this.chkExpOn.Name = "chkExpOn";
            this.chkExpOn.Size = new System.Drawing.Size(112, 17);
            this.chkExpOn.TabIndex = 28;
            this.chkExpOn.Text = "Expiring Before";
            this.chkExpOn.UseVisualStyleBackColor = true;
            this.chkExpOn.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // ucDocumentReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            this.Controls.Add(this.chkExpOn);
            this.Controls.Add(this.dtpFrom);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.dtpExpOn);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.flxStockPrv);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.txtDocName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDocNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDocType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Name = "ucDocumentReport";
            this.Size = new System.Drawing.Size(877, 648);
            this.Load += new System.EventHandler(this.ucStockDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.flxStockPrv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.TextBox txtDocName;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtDocNo;
        public System.Windows.Forms.TextBox txtDocType;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLoad;
        public C1.Win.C1FlexGrid.C1FlexGrid flxStockPrv;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnExport;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.DateTimePicker dtpExpOn;
        public System.Windows.Forms.DateTimePicker dtpTo;
        public System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.CheckBox chkExpOn;
    }
}
