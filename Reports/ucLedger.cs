using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using C1.Win.C1FlexGrid;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Reports
{
    public partial class ucLedger : UserControl
    {
        SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);
        cGeneral cgen = new cGeneral();
        fssgen.fssgen gen = new fssgen.fssgen();
        SqlDataReader dr = null; 

        decimal sum = 0;
        decimal credit = 0, debit = 0; 
        string crosshead = "", transno = "", transtype = "", bookcode = "";
        string nar = string.Empty;

        public ucLedger()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void ucLedger_Load(object sender, EventArgs e)
        {
            searchload(txtHead.Tag + "");
        }

        private void Load_Screen()
        {
                    nar = " case when bookcode>'200002' then '' + ' Chqno:'+ chqno + ' Chqdt:'  " + Environment.NewLine
                   + "     + convert(varchar,chqdate,103) + ' ' + narration else narration end narration ";
                 

            c1FlexGrid2.Cols.Count = 15;
            c1FlexGrid2.Cols[5].Style.Margins.Right = 5;
            c1FlexGrid2.AllowEditing = false;
            c1FlexGrid2.ExtendLastCol = true;
            c1FlexGrid2.Focus();

            dtpfrom.MinDate = cPublic.g_OpDate;
            dtpfrom.MaxDate = cPublic.g_ClDate;
            dtpfrom.Value = cPublic.g_OpDate;

            dtpto.Value = DateTime.Today;
            dtpto.MinDate = cPublic.g_OpDate;
            dtpto.MaxDate = cPublic.g_ClDate;

            rdallvoy.Select();
            cmbamount.SelectedIndex = 0;
            cmbvoucher.SelectedIndex = 0;

            c1FlexGrid2.Cols[07].Name = "ACCOUNTTYPE";
            c1FlexGrid2.Cols[08].Name = "CHEQUE";
            c1FlexGrid2.Cols[09].Name = "CHQNO";
            c1FlexGrid2.Cols[10].Name = "CHQDATE";
            c1FlexGrid2.Cols[11].Name = "CHQPASSED";
            c1FlexGrid2.Cols[12].Name = "CHQPASSDATE";
            c1FlexGrid2.Cols[13].Name = "USERID";
            c1FlexGrid2.Cols[14].Name = "MODIUSER";

            c1FlexGrid2.Cols["ACCOUNTTYPE"].Caption = "TRANS TYPE";
            c1FlexGrid2.Cols["CHEQUE"].Caption = "CHEQUE";
            c1FlexGrid2.Cols["CHQNO"].Caption = "CHQNO";
            c1FlexGrid2.Cols["CHQPASSDATE"].Caption = "CHQPASSDATE";
            c1FlexGrid2.Cols["CHQDATE"].Caption = "CHQDATE";
            c1FlexGrid2.Cols["CHQPASSED"].Caption = "CHQPASSED";
            c1FlexGrid2.Cols["USERID"].Caption = "USERID";
            c1FlexGrid2.Cols["MODIUSER"].Caption = "MODIUSER";
            c1FlexGrid2.Cols["USERID"].Width = 150;
            c1FlexGrid2.Cols["MODIUSER"].Width = 150;
        }
      
        private void txtamount_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            gen.ValidDecimalNumber(e, ref tb, 10, 2);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog filesave = new SaveFileDialog();
            filesave.Filter = "Microsoft Office Excel WorkBook (*.xls)|*.xls";
            filesave.ShowDialog();
            if (filesave.FileName != "")
            {
                //c1FlexGrid2.SaveGrid(filesave.FileName, FileFormatEnum.TextCustom, true, UTF8Encoding.Unicode);
            }
        }

        private void cmbamount_KeyDown(object sender, KeyEventArgs e)
        {
            if (!(e.KeyValue == 38 || e.KeyValue == 40))
            {
                e.SuppressKeyPress = true;
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            searchload(txtHead.Tag +"");
            pnldiff.Visible = false;
            c1FlexGrid2.Enabled = true;
            gpserach.Visible = false;
            if (c1FlexGrid2.Rows.Count > 1)
            {
                c1FlexGrid2.Focus();
            }
        }

        private void btnShowAll_Click(object sender, EventArgs e)
        {
            load(txtHead.Tag +"");
            c1FlexGrid2.Enabled = true;
            gpserach.Visible = false;
            if (c1FlexGrid2.Rows.Count > 1)
            {
                c1FlexGrid2.Focus();
            }
        }

        public void searchload(string code)
        {
            Load_Screen();

            c1FlexGrid2.Cols[07].Name = "ACCOUNTTYPE";
            c1FlexGrid2.Cols[08].Name = "CHEQUE";
            c1FlexGrid2.Cols[09].Name = "CHQNO";
            c1FlexGrid2.Cols[10].Name = "CHQDATE";
            c1FlexGrid2.Cols[11].Name = "CHQPASSED";
            c1FlexGrid2.Cols[12].Name = "CHQPASSDATE";
            c1FlexGrid2.Cols[13].Name = "USERID";
            c1FlexGrid2.Cols[14].Name = "MODIUSER";

            c1FlexGrid2.Cols["ACCOUNTTYPE"].Caption = "TRANS TYPE";
            c1FlexGrid2.Cols["CHEQUE"].Caption = "CHEQUE";
            c1FlexGrid2.Cols["CHQNO"].Caption = "CHQNO";
            c1FlexGrid2.Cols["CHQPASSDATE"].Caption = "CHQPASSDATE";
            c1FlexGrid2.Cols["CHQDATE"].Caption = "CHQDATE";
            c1FlexGrid2.Cols["CHQPASSED"].Caption = "CHQPASSED";
            c1FlexGrid2.Cols["USERID"].Caption = "USERID";
            c1FlexGrid2.Cols["MODIUSER"].Caption = "MODIUSER";            

            credit = 0; debit = 0;
            c1FlexGrid2.Rows.Count = 1;

            string cond = "";
            if (chkchequepassed.Checked == true)
            {
                cond = " AND ((date1 between '" + this.dtpfrom.Value.ToString("yyyy-MM-dd") + "' and '" + this.dtpto.Value.ToString("yyyy-MM-dd") + "' AND bookcode <= '200002' ) OR "
                        + " (chqpassdate between '" + this.dtpfrom.Value.ToString("yyyy-MM-dd") + "' and '" + this.dtpto.Value.ToString("yyyy-MM-dd") + "'  AND bookcode > '200002' AND chqpassed= 1) ) ";

            }
            else
            {
                cond = " and date1 >= '" + (dtpfrom.Value.ToString("MM/dd/yyyy")) + "' and date1 <= '" + (dtpto.Value.ToString("MM/dd/yyyy")) + "' ";
            }

            if (rdbCredit.Checked == true)
            {
                cond = cond + " and (amount>0) ";
            }
            else if (rdbDebit.Checked == true)
            {
                cond = cond + " and (amount<0)";
            }
            if (txtamount.Text != "")
            {
                cond = cond + " and abs(amount)" + cmbamount.Text + txtamount.Text.Trim();
            }
            if (cmbvoucher.Text == "Purchase")
            {
                cond = cond + " and transtype='H'";
            }
            else if (cmbvoucher.Text == "Sales")
            {
                cond = cond + " and transtype='S'";
            }
            else if (cmbvoucher.Text == "Payment")
            {
                cond = cond + " and transtype='P'";
            }
            else if (cmbvoucher.Text == "Reciept")
            {
                cond = cond + " and transtype='R'";
            }
            else if (cmbvoucher.Text == "Journal")
            {
                cond = cond + " and transtype='J'";
            }
            else if (cmbvoucher.Text == "Credit")
            {
                cond = cond + " and transtype='C'";
            }
            else if (cmbvoucher.Text == "Debit")
            {
                cond = cond + " and transtype='D'";
            }
             int i = 0;

            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_GET_OPBAL"+ cPublic.g_firmcode ;
            cmd.Parameters.AddWithValue("@code", txtHead.Tag +"");
            cmd.Parameters.AddWithValue("@date", dtpfrom.Value.ToString("MM/dd/yyyy"));
            dr  = cmd.ExecuteReader();
            while (dr.Read())
            {
                lblopbvalance.Tag = dr.GetDecimal(0);
                lblopbvalance.Text = Convert.ToString(Math.Abs(Convert.ToDecimal(dr.GetValue(0))));
                lblopbvalance.Text = Convert.ToDecimal(lblopbvalance.Text).ToString("#,##,##0.00");
                if (Convert.ToDecimal(lblopbvalance.Tag) > 0)
                {
                    lblcrdr.Text = "Cr";
                }
                else
                {
                    lblcrdr.Text = "Dr";
                }
            }
            dr.Close();

            sum = Convert.ToDecimal(lblopbvalance.Tag);
            string fiel = " case chqpassed when 1 then CHQPASSDATE else DATE1 END [DATE1] ";

            if (chkchequepassed.Checked == false) { fiel = " [DATE1]"; }
            string sql = "select " + fiel + ",transtype,transno, " + nar + " ,Amount,bookcode, " + Environment.NewLine
            + " '' [S],ISNULL((SELECT HEAD FROM ACCOUNTS" + cPublic.g_firmcode + " WHERE CODE=A.BOOKCODE),'JOURNAL')  ACCOUNTTYPE,[CHEQUE], [CHQNO],CONVERT(VARCHAR,[CHQDATE],103) [CHQDATE] ,[CHQPASSED], CONVERT(VARCHAR,[CHQPASSDATE],103)  [CHQPASSDATE],  [USERID],MODIUSER from voucher" + cPublic.g_firmcode + " A where code='" + code + "' " + cond + " order by Date1,recno";
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql ;
            SqlDataAdapter ds = new SqlDataAdapter(cmd );
            DataSet dt = new DataSet();
            ds.Fill(dt);
            for (int j = 0; j <= dt.Tables[0].Rows.Count - 1; j++)
            {
                c1FlexGrid2.Rows.Add();
                i++;
                c1FlexGrid2[i, 0] = dt.Tables[0].Rows[j]["date1"];
                transno = dt.Tables[0].Rows[j][2].ToString();
                transtype = dt.Tables[0].Rows[j][1].ToString();
                c1FlexGrid2[i, 1] = (dt.Tables[0].Rows[j][1].ToString() + dt.Tables[0].Rows[j][2].ToString());
                c1FlexGrid2[i, 2] = dt.Tables[0].Rows[j][3];

                //c1FlexGrid2[i, "series"] = dt.Tables[0].Rows[j]["series"].ToString();

                if (Convert.ToDecimal(dt.Tables[0].Rows[j]["Amount"]) > 0)
                {
                    c1FlexGrid2[i, 4] = Convert.ToDecimal(Math.Abs(Convert.ToDecimal(dt.Tables[0].Rows[j][4])));
                    credit = credit + Convert.ToDecimal(Math.Abs(Convert.ToDecimal(dt.Tables[0].Rows[j][4])));
                    c1FlexGrid2[i, 3] = "0.00";
                }
                else
                {
                    c1FlexGrid2[i, 3] = Convert.ToDecimal(Math.Abs(Convert.ToDecimal(dt.Tables[0].Rows[j][4])));
                    debit = debit + Convert.ToDecimal(Math.Abs(Convert.ToDecimal(dt.Tables[0].Rows[j][4])));
                    c1FlexGrid2[i, 4] = "0.00";
                }
                sum = sum + Convert.ToDecimal(dt.Tables[0].Rows[j][4]);
                if (sum < 0)
                {
                    c1FlexGrid2[i, 5] = Convert.ToString(Math.Abs(sum)) + " Dr";
                    label1.Text = "Dr";
                }
                else
                {
                    c1FlexGrid2[i, 5] = Convert.ToString(Math.Abs(sum)) + " Cr";
                    label1.Text = "Cr";
                }

                bookcode = dt.Tables[0].Rows[j][5].ToString();


                c1FlexGrid2[i, "ACCOUNTTYPE"] = dt.Tables[0].Rows[j]["ACCOUNTTYPE"];
                c1FlexGrid2[i, "CHEQUE"] = dt.Tables[0].Rows[j]["CHEQUE"];
                c1FlexGrid2[i, "CHQNO"] = dt.Tables[0].Rows[j]["CHQNO"];
                c1FlexGrid2[i, "CHQPASSDATE"] = dt.Tables[0].Rows[j]["CHQPASSDATE"];
                c1FlexGrid2[i, "CHQDATE"] = dt.Tables[0].Rows[j]["CHQDATE"];
                c1FlexGrid2[i, "CHQPASSED"] = dt.Tables[0].Rows[j]["CHQPASSED"];
                c1FlexGrid2[i, "USERID"] = dt.Tables[0].Rows[j]["USERID"] + "";
                c1FlexGrid2[i, "MODIUSER"] = dt.Tables[0].Rows[j]["MODIUSER"] + "";
                if (bookcode == "000000")
                {
                    cmd.Parameters.Clear();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select code from voucher" + cPublic.g_firmcode + " where transno=@transno and transtype=@transtype and code=@code";
                    cmd.Parameters.AddWithValue("@transno", transno);
                    cmd.Parameters.AddWithValue("@transtype", transtype);
                    cmd.Parameters.AddWithValue("@code", code);
                    SqlDataAdapter da = new SqlDataAdapter(cmd );
                    DataSet dg = new DataSet();
                    da.Fill(dg);
                    if (dg.Tables[0].Rows.Count > 0)
                    {
                        crosshead = cgen.GetHead(dg.Tables[0].Rows[0]["code"].ToString());
                        c1FlexGrid2[i, 2] = crosshead + " - " + c1FlexGrid2[i, 2];
                    }
                }
                else
                {
                    crosshead = cgen.GetHead(bookcode);
                    c1FlexGrid2[i, 2] = crosshead + " - " + c1FlexGrid2[i, 2];
                }

                if ((c1FlexGrid2[i, 2].ToString().Length % 28) > 0)
                    c1FlexGrid2.Rows[i].Height = ((c1FlexGrid2[i, 2].ToString().Length / 28) + 1) * c1FlexGrid2.Rows.DefaultSize;
                else
                    c1FlexGrid2.Rows[i].Height = (c1FlexGrid2[i, 2].ToString().Length / 28) * c1FlexGrid2.Rows.DefaultSize;



            }
            lblcredit.Text = credit.ToString("#,##,##0.00");
            lbldebit.Text = debit.ToString("#,##,##0.00");
            lblcurrentbalance.Text = Convert.ToDecimal(Convert.ToString(Math.Abs(sum))).ToString("#,##,##0.00");

            c1FlexGrid2.Focus();
        }

        public void load(string code)
        {
            c1FlexGrid2.Cols.Count = 15;
            c1FlexGrid2.Cols[07].Name = "ACCOUNTTYPE";
            c1FlexGrid2.Cols[08].Name = "CHEQUE";
            c1FlexGrid2.Cols[09].Name = "CHQNO";
            c1FlexGrid2.Cols[10].Name = "CHQDATE";
            c1FlexGrid2.Cols[11].Name = "CHQPASSED";
            c1FlexGrid2.Cols[12].Name = "CHQPASSDATE";
            c1FlexGrid2.Cols[13].Name = "USERID";
            c1FlexGrid2.Cols[14].Name = "MODIUSER";

            c1FlexGrid2.Cols["ACCOUNTTYPE"].Caption = "TRANS TYPE";
            c1FlexGrid2.Cols["CHEQUE"].Caption = "CHEQUE";
            c1FlexGrid2.Cols["CHQNO"].Caption = "CHQNO";
            c1FlexGrid2.Cols["CHQPASSDATE"].Caption = "CHQPASSDATE";
            c1FlexGrid2.Cols["CHQDATE"].Caption = "CHQDATE";
            c1FlexGrid2.Cols["CHQPASSED"].Caption = "CHQPASSED";
            c1FlexGrid2.Cols["USERID"].Caption = "USERID";
            c1FlexGrid2.Cols["MODIUSER"].Caption = "MODIUSER";

            c1FlexGrid2.Cols[3].Format = "#,##,##0.00";
            c1FlexGrid2.Cols[4].Format = "#,##,##0.00";

                 nar = " case when bookcode>'200002' then '' + ' Chqno:'+ chqno + ' Chqdt:'  " + Environment.NewLine
                                + "     + convert(varchar,chqdate,103)+ ' ' + narration else narration end narration ";
           

            int i = 0;
            credit = 0; debit = 0;
            c1FlexGrid2.Rows.Count = 1;

            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select opbalance from accounts" + cPublic.g_firmcode + " where code='" + code + "'";
            dr  = cmd.ExecuteReader();
            while (dr.Read())
            {
                lblopbvalance.Tag = dr.GetDecimal(0);
                lblopbvalance.Text = Math.Abs(Convert.ToDecimal(dr.GetValue(0))).ToString("#,##,##0.00");
                if (Convert.ToDecimal(lblopbvalance.Tag) > 0)
                {
                    lblcrdr.Text = "Cr";
                }
                else
                {
                    lblcrdr.Text = "Dr";
                }
            }
            dr.Close();

            sum = Convert.ToDecimal(lblopbvalance.Tag);


            string sql = "select [DATE1],transtype,transno, " + Environment.NewLine
             + " " + nar + " " + Environment.NewLine
            + " ,Amount,bookcode,''S,ISNULL((SELECT HEAD FROM ACCOUNTS" + cPublic.g_firmcode + " WHERE CODE=A.BOOKCODE),'JOURNAL')   ACCOUNTTYPE,[CHEQUE], [CHQNO],CONVERT(VARCHAR,[CHQDATE],103) [CHQDATE] ,[CHQPASSED], CONVERT(VARCHAR,[CHQPASSDATE],103)  [CHQPASSDATE],  [USERID],MODIUSER from voucher" + cPublic.g_firmcode + " A where code='" + code + "'order by Date1,recno";
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql ;
            SqlDataAdapter ds = new SqlDataAdapter(cmd );
            DataSet dt = new DataSet();
            ds.Fill(dt);
            for (int j = 0; j <= dt.Tables[0].Rows.Count - 1; j++)
            {
                c1FlexGrid2.Rows.Add();
                i++;
                c1FlexGrid2[i, 0] = dt.Tables[0].Rows[j][0];
                transno = dt.Tables[0].Rows[j][2].ToString();
                transtype = dt.Tables[0].Rows[j][1].ToString();
                c1FlexGrid2[i, 1] = (dt.Tables[0].Rows[j][1].ToString() + dt.Tables[0].Rows[j][2].ToString());

                //c1FlexGrid2[i, "series"] = dt.Tables[0].Rows[j]["series"].ToString();

                if (Convert.ToDecimal(dt.Tables[0].Rows[j]["Amount"]) > 0)
                {
                    c1FlexGrid2[i, 4] = Math.Abs(Convert.ToDecimal(dt.Tables[0].Rows[j][4])).ToString("#,##,##0.00");
                    credit = credit + Convert.ToDecimal(Math.Abs(Convert.ToDecimal(dt.Tables[0].Rows[j][4])));
                    c1FlexGrid2[i, 3] = "0.00";
                    c1FlexGrid2[i, 2] = "By " + dt.Tables[0].Rows[j][3];
                }
                else
                {
                    c1FlexGrid2[i, 3] = Math.Abs(Convert.ToDecimal(dt.Tables[0].Rows[j][4])).ToString("#,##,##0.00");
                    debit = debit + Convert.ToDecimal(Math.Abs(Convert.ToDecimal(dt.Tables[0].Rows[j][4])));
                    c1FlexGrid2[i, 4] = "0.00";
                    c1FlexGrid2[i, 2] = "To " + dt.Tables[0].Rows[j][3];
                }
                sum = sum + Convert.ToDecimal(dt.Tables[0].Rows[j][4]);
                if (sum < 0)
                {
                    c1FlexGrid2[i, 5] = Math.Abs(sum).ToString("#,##,##0.00") + " Dr";
                    label1.Text = "Dr";
                }
                else
                {
                    c1FlexGrid2[i, 5] = Math.Abs(sum).ToString("#,##,##0.00") + " Cr";
                    label1.Text = "Cr";
                }

                bookcode = dt.Tables[0].Rows[j][5].ToString();
                c1FlexGrid2.Styles.Normal.WordWrap = true;

                c1FlexGrid2[i, "ACCOUNTTYPE"] = dt.Tables[0].Rows[j]["ACCOUNTTYPE"];
                c1FlexGrid2[i, "CHEQUE"] = dt.Tables[0].Rows[j]["CHEQUE"];
                c1FlexGrid2[i, "CHQNO"] = dt.Tables[0].Rows[j]["CHQNO"];
                c1FlexGrid2[i, "CHQPASSDATE"] = dt.Tables[0].Rows[j]["CHQPASSDATE"];
                c1FlexGrid2[i, "CHQDATE"] = dt.Tables[0].Rows[j]["CHQDATE"];
                c1FlexGrid2[i, "CHQPASSED"] = dt.Tables[0].Rows[j]["CHQPASSED"];
                c1FlexGrid2[i, "USERID"] = dt.Tables[0].Rows[j]["USERID"];
                c1FlexGrid2[i, "MODIUSER"] = dt.Tables[0].Rows[j]["MODIUSER"];
                if (bookcode == "000000")
                {
                    cmd.Parameters.Clear();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select code from voucher" + cPublic.g_firmcode + " where transno=@transno and transtype=@transtype and code<>@code and transtype in ('J','D','C')  ";
                    cmd.Parameters.AddWithValue("@transno", transno);
                    cmd.Parameters.AddWithValue("@transtype", transtype);
                    cmd.Parameters.AddWithValue("@code", code);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet dg = new DataSet();
                    da.Fill(dg);

                    if (dg.Tables[0].Rows.Count > 0)
                    {
                        crosshead = cgen.GetHead(dg.Tables[0].Rows[0]["code"].ToString());
                        c1FlexGrid2[i, 2] = crosshead + " - " + c1FlexGrid2[i, 2];
                    }
                }
                else
                {
                    crosshead = cgen.GetHead(bookcode);
                    c1FlexGrid2[i, 2] = crosshead + " - " + c1FlexGrid2[i, 2];
                }
                if ((c1FlexGrid2[i, 2].ToString().Length % 28) > 0)
                    c1FlexGrid2.Rows[i].Height = ((c1FlexGrid2[i, 2].ToString().Length / 28) + 1) * c1FlexGrid2.Rows.DefaultSize;
                else
                    c1FlexGrid2.Rows[i].Height = (c1FlexGrid2[i, 2].ToString().Length / 28) * c1FlexGrid2.Rows.DefaultSize;

            }
            lblcredit.Text = credit.ToString("#,##,##0.00");
            lbldebit.Text = debit.ToString("#,##,##0.00");
            lblcurrentbalance.Text = Math.Abs(sum).ToString("#,##,##0.00");



            decimal balance = 0;
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select balance from accounts" + cPublic.g_firmcode + " where code='" + code + "'";
            dr  = cmd.ExecuteReader();
            while (dr.Read())
            {
                balance = dr.GetDecimal(0);
            }
            dr.Close();

            decimal check = 0;
            check = (Math.Abs(balance) - Math.Abs(Convert.ToDecimal(lblcurrentbalance.Text.Trim())));
            if (check != 0)
            {
                pnldiff.Visible = true;
                lbldiff.Text = check.ToString("#,##,##0.00");
                if (check > 0)
                {
                    lbldiffcrdr.Text = "Cr";
                }
                else
                {
                    lbldiffcrdr.Text = "Dr";
                }
            }
            c1FlexGrid2.Focus();
            c1FlexGrid2.Select(c1FlexGrid2.Rows.Count - 1, 1, true);
        }

        private void txtHead_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { loadLookUp("Accounts", string.Empty); }
        }

        private void txtHead_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar.Equals('\b')))
            {
                e.Handled = true;
                txtHead .Text = string.Empty;
                loadLookUp("Accounts", txtHead.Text.Trim());
            }
            if (e.KeyChar.Equals('+'))
            {
                e.Handled = true;
                txtHead.Text = string.Empty;
                loadLookUp("Accounts", txtHead.Text.Trim());
                return;
            }
            if (!(e.KeyChar.Equals('\b')) && (!(e.KeyChar.Equals('\r'))))
            {
                e.Handled = true;
                txtHead.Text = string.Empty;
                loadLookUp("Accounts", txtHead.Text.Trim() + e.KeyChar.ToString());
            }
        }

        private void loadLookUp(string TableName, string str)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;
            lookup.m_strat = str;
            lookup.m_table = "ACCOUNTS" + cPublic.g_firmcode;
            lookup.m_fields = "HEAD,CODE,ADDRESS, STREET, CITY,Balance";
            lookup.m_dispname = "HEAD,CODE,ADDRESS, STREET, CITY,BALANCE";
            lookup.m_caption = "Customer";
            lookup.m_fldwidth = "300,0,100,100,100,100";
            lookup.m_condition = "groupcode<>'200000'";
            lookup.ShowDialog();

            if (lookup.m_values.Count > 0)
            {
                txtHead .Text = lookup.m_values[0].ToString();
                txtHead.Tag = lookup.m_values[1].ToString();

                SendKeys.Send("{TAB}");
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            searchload(txtHead.Tag + "");
        }

        private void c1FlexGrid2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                if (e.KeyCode == Keys.F)
                {
                    c1FlexGrid2.Enabled = false;
                    c1FlexGrid2.Select(0, 0);
                    gpserach.Visible = true;
                    dtpfrom.Focus();
                }
                else if (e.KeyCode == Keys.R)
                {
                    load(txtHead.Tag + "");
                }
            }
        }

    }
}
