using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using System.IO;
using Xpire.Classes;

namespace Xpire.Reports
{
    public partial class ucDayBook : UserControl
    {
        ArrayList code = new ArrayList();
        SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);
        cGeneral cgen = new cGeneral();

        string AcCode = "";
        Boolean ChkPassed = false;
        int BalType, j, i = 0, PageNo = 0;
        DateTime FrDate, TDate, CurDate;
        double TDebit, TCredit, ClBal = 0;

        StreamWriter tstream;
        string strdata;
        int lineno;

        public ucDayBook()
        {
            InitializeComponent();
        }

        public void GetOpbal(string code, DateTime FromDate, DateTime ToDate, Boolean chkpass, int bal)
        {
            AcCode = code;
            ChkPassed = chkpass;
            BalType = bal;
            FrDate = FromDate;
            TDate = ToDate;
            cmd.Parameters.Clear();
            cmd.CommandText="SP_GET_OPBAL" + cPublic.g_firmcode;
            cmd.CommandType = CommandType.StoredProcedure;
            if (!AcCode.Equals("999999"))
                cmd.Parameters.AddWithValue("@code", AcCode);
            else
                cmd.Parameters.AddWithValue("@code", "200001");
            cmd.Parameters.AddWithValue("@date", FrDate.ToString("MM/dd/yyyy"));
            if (chkpass == true)
            { cmd.Parameters.AddWithValue("@pass", 1); }
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lblopbvalance.Tag = dr.GetDecimal(0);
                lblopbvalance.Text = Convert.ToString(Math.Abs(Convert.ToDouble(dr.GetValue(0))));
                lblopbvalance.Text = Convert.ToDouble(lblopbvalance.Text).ToString("#,##,##0.00");
                ClBal = Convert.ToDouble(dr.GetValue(0));
                if (Convert.ToDouble(lblopbvalance.Tag) > 0)
                    lblcrdr.Text = "Cr";
                else
                    lblcrdr.Text = "Dr";
            }
            dr.Close();
        }

        private void PopulateComb()
        {
            string sql = "SELECT HEAD, CODE From Accounts" + cPublic.g_firmcode + " WHERE CODE not in ('200001','200002') AND GROUPCODE='200000'";
            cmd.CommandText = sql;
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                cmbBooktype.Items.Add(dr["head"]);
                code.Add(dr["code"]);
            }
            dr.Close();
        }

        private void ucDayBook_Load(object sender, EventArgs e)
        {
            rddailybal.Checked = true;
            dtfrdate.Value = cPublic.g_Rdate;
            dttodate.Value = cPublic.g_Rdate;
            cmbBooktype.Items.Add("Cash Book");
            code.Add("200001");
            cmbBooktype.Items.Add("PettyCash Book");
            code.Add("200002");
            cmbBooktype.Items.Add("Journal");
            code.Add("000000");
            PopulateComb();
            cmbBooktype.Items.Add("Day Book");
            code.Add("999999");
            cmbBooktype.SelectedIndex = 0;
        }

        private void Initialize_Grid()
        {
            cfgday.Cols.Count = 8;
            cfgday.Cols[1].Name = "DATE";
            cfgday.Cols[1].DataType = typeof(string);
            cfgday.Cols[2].Name = "ref";
            cfgday.Cols[3].Name = "particulars";
            cfgday.Cols[4].Name = "debit";
            cfgday.Cols["debit"].Caption = "Debit"; 
            cfgday.Cols[4].DataType = typeof(double);
            cfgday.Cols[4].Format = "##,###0.00";
            cfgday.Cols[5].Name = "credit";
            cfgday.Cols["credit"].Caption = "Credit"; 
            cfgday.Cols[5].DataType = typeof(double);
            cfgday.Cols[5].Format = "##,###0.00";
            cfgday.Cols[6].Name = "balance";
            cfgday.Cols[6].DataType = typeof(double);
            cfgday.Cols[6].Format = "##,###0.00";
            cfgday.Cols[7].Name = "series";
            cfgday.Cols["series"].DataType = typeof(string);
            cfgday.Cols["series"].Visible = false;
            cfgday.Cols[3].StyleNew.WordWrap = true;
            cfgday.Rows.Count = 1;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            int balval;
            if (rddailybal.Checked)
                balval = 1;
            else if (rdmothbal.Checked)
                balval = 2;
            else
                balval = 3;
            GetOpbal(code[cmbBooktype.SelectedIndex].ToString(), dtfrdate.Value, dttodate.Value, chkchqpass.Checked, balval);
            this.Text = cmbBooktype.Text;

            Initialize_Grid();
            Execute();
        }

        private void Execute()
        {
            string nar = " case when bookcode>'200002' then  'Chqno:'+ chqno + ' Chqdt:'  " + Environment.NewLine
            + "     + convert(varchar,chqdate,103) + ' ' + narration  else narration end narration ";
            string sql = "";
            switch (AcCode)
            {
                case "000000":  // journal
                    sql = "Select case chqpassed when 1 then CHQPASSDATE else DATE1 END [DATE1],BOOKCODE,CODE,(SELECT HEAD FROM ACCOUNTS" + cPublic.g_firmcode + " WHERE CODE=A.CODE) AS HEAD , AMOUNT,CHEQUE,TRANSTYPE,TRANSNO,CHQNO,CHQDATE,CHQPASSED," + nar + ",TRANSTYPE,BILLNO from VOUCHER" + cPublic.g_firmcode + " A where " + "( Date1 <= '" + TDate.ToString("yyyy/MM/dd") + "' and date1>='" + FrDate.ToString("yyyy/MM/dd") + "')"
                        // + "and bookcode = '000000' order by DATE1,billno,transtype,transno ";
                     + "and bookcode = '000000' order by DATE1,recno ";
                    break;
                case "999999": //day book
                    sql = "Select case chqpassed when 1 then CHQPASSDATE else DATE1 END [DATE1],BOOKCODE,CODE,(SELECT HEAD FROM ACCOUNTS" + cPublic.g_firmcode + " WHERE CODE=A.CODE) AS HEAD , AMOUNT,CHEQUE,TRANSTYPE,TRANSNO,CHQNO,CHQDATE,CHQPASSED," + nar + ",TRANSTYPE,BILLNO from VOUCHER" + cPublic.g_firmcode + " A where " + " ( Date1 <= '" + TDate.ToString("yyyy/MM/dd") + "' and date1>='" + FrDate.ToString("yyyy/MM/dd") + "')"
                    + "order by DATE1,recno ";
                    break;
                default:
                    if (ChkPassed.Equals(true))
                    {
                        sql = "Select (CASE WHEN bookcode<='200002' THEN DATE1 ELSE chqpassdate END) AS DATE1,BOOKCODE,CODE,(SELECT HEAD FROM ACCOUNTS" + cPublic.g_firmcode + " WHERE CODE=A.CODE) AS HEAD , AMOUNT,"
                        + "CHEQUE,TRANSTYPE,TRANSNO,CHQNO,CHQDATE,CHQPASSED,NARRATION,BILLNO from VOUCHER" + cPublic.g_firmcode + " A WHERE "
                        + " (date1 between  '" + FrDate.ToString("yyyy/MM/dd") + "' and  '" + TDate.ToString("yyyy/MM/dd") + "'   AND  '" + AcCode + "'<='200002' and bookcode = '" + AcCode + "' ) OR "
                        + " (chqpassdate between  '" + FrDate.ToString("yyyy/MM/dd") + "' and  '" + TDate.ToString("yyyy/MM/dd") + "'   AND  '" + AcCode + "'>'200002' and bookcode = '" + AcCode + "' AND chqpassed = 1) OR "
                        + " (date1 between  '" + FrDate.ToString("yyyy/MM/dd") + "' and  '" + TDate.ToString("yyyy/MM/dd") + "'  AND bookcode <= '200002' AND code = '" + AcCode + "') OR "
                        + " (chqpassdate between  '" + FrDate.ToString("yyyy/MM/dd") + "' and  '" + TDate.ToString("yyyy/MM/dd") + "'  AND bookcode > '200002' and code = '" + AcCode + "' AND chqpassed= 1) "
                        + " ORDER BY DATE1,RECNO";
                        cmd.CommandText = sql;
                        cmd.Parameters.Clear();
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = "Select DATE1 AS [DATE1],BOOKCODE,CODE,(SELECT HEAD FROM ACCOUNTS" + cPublic.g_firmcode + " WHERE CODE=A.CODE) AS HEAD , AMOUNT,CHEQUE,TRANSTYPE,TRANSNO,CHQNO,CHQDATE,CHQPASSED," + nar + ",TRANSTYPE,BILLNO from VOUCHER" + cPublic.g_firmcode + " A where (  Date1 <= '" + TDate.ToString("yyyy/MM/dd") + "' and date1>='" + FrDate.ToString("yyyy/MM/dd") + "' )"
                       + " and ( bookcode = '" + AcCode + "' or code = '" + AcCode + "') order by DATE1,recno ";

                        cmd.CommandText = sql;
                        cmd.Parameters.Clear();
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                    break;
            }

            SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
            DataSet ds = new DataSet();
            da.Fill(ds);
            cfgday.Styles.Normal.WordWrap = true;
            i = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (j = 0; j <= ds.Tables[0].Rows.Count - 1; j++)
                {
                    cfgday.Rows.Add();
                    i++;
                    CurDate = (DateTime)(ds.Tables[0].Rows[j]["date1"]);
                    cfgday[i, "DATE"] = ds.Tables[0].Rows[j]["date1"];
                    cfgday[i, "ref"] = ds.Tables[0].Rows[j]["transtype"] + ds.Tables[0].Rows[j]["transno"].ToString();
                    double amt = Convert.ToDouble(ds.Tables[0].Rows[j]["amount"]);
                    if (ds.Tables[0].Rows[j]["bookcode"].Equals(AcCode) | ds.Tables[0].Rows[j]["bookcode"].Equals("200001"))
                    {
                        if (Convert.ToDouble(AcCode) >= 200002 && AcCode != "999999" && ds.Tables[0].Rows[j]["bookcode"].Equals("200001"))
                        {
                            if (amt > 0)
                            {
                                cfgday[i, "particulars"] = "By " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = 0;
                                cfgday[i, "credit"] = Math.Abs(amt);
                                TCredit += Math.Abs(amt);
                                ClBal += amt;
                            }
                            else
                            {
                                cfgday[i, "particulars"] = "To " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = Math.Abs(amt);
                                cfgday[i, "credit"] = 0;
                                TDebit += Math.Abs(amt);
                                ClBal += amt;
                            }
                        }
                        else
                        {
                            if (amt > 0)
                            {
                                cfgday[i, "particulars"] = "To " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = Math.Abs(amt);
                                cfgday[i, "credit"] = 0;
                                TDebit += Math.Abs(amt);
                                ClBal -= amt;
                            }
                            else
                            {
                                cfgday[i, "particulars"] = "By " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = 0;
                                cfgday[i, "credit"] = Math.Abs(amt);
                                TCredit += Math.Abs(amt);
                                ClBal -= amt;
                            }
                        }
                    }
                    else if (Convert.ToDecimal(ds.Tables[0].Rows[j]["bookcode"]) > 200001 && cgen.groupcode(ds.Tables[0].Rows[j]["code"].ToString()).Equals("200000"))
                    {
                        if (Convert.ToDouble(AcCode) >= 200002 && AcCode != "999999" && ds.Tables[0].Rows[j]["bookcode"].Equals("200001"))
                        {
                            if (amt > 0)
                            {
                                cfgday[i, "particulars"] = "By " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = Math.Abs(amt);
                                cfgday[i, "credit"] = 0;
                                TDebit += Math.Abs(amt);
                                ClBal -= amt;
                            }
                            else
                            {
                                cfgday[i, "particulars"] = "To " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = 0;
                                cfgday[i, "credit"] = Math.Abs(amt);
                                TCredit += Math.Abs(amt);
                                ClBal -= amt;
                            }
                        }
                        else
                        {
                            if (amt > 0)
                            {
                                cfgday[i, "particulars"] = "To " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = 0;
                                cfgday[i, "credit"] = Math.Abs(amt);
                                TCredit += Math.Abs(amt);
                                ClBal += amt;
                            }
                            else
                            {
                                cfgday[i, "particulars"] = "By " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = Math.Abs(amt);
                                cfgday[i, "credit"] = 0;
                                TDebit += Math.Abs(amt);
                                ClBal += amt;
                            }
                        }
                    }
                    else
                    {
                        if (!(ds.Tables[0].Rows[j]["bookcode"].ToString().Equals("000000")))
                            cfgday[i, "particulars"] = ds.Tables[0].Rows[j]["HEAD"].ToString() + "   " + ds.Tables[0].Rows[j]["narration"];
                        else
                            cfgday[i, "particulars"] = ds.Tables[0].Rows[j]["HEAD"].ToString() + "   " + ds.Tables[0].Rows[j]["narration"];
                        if (amt < 0)
                        {
                            cfgday[i, "debit"] = 0;
                            cfgday[i, "credit"] = Math.Abs(amt);

                            if (AcCode.Equals("999999") && !ds.Tables[0].Rows[j]["code"].Equals("200001") && !ds.Tables[0].Rows[j]["bookcode"].Equals("000000"))
                            {
                                cfgday.Rows.Add();
                                i++;
                                cfgday[i, "DATE"] = ds.Tables[0].Rows[j]["date1"];
                                cfgday[i, "ref"] = ds.Tables[0].Rows[j]["transtype"] + ds.Tables[0].Rows[j]["transno"].ToString();
                                cfgday[i, "particulars"] = "To " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = Math.Abs(amt);
                                cfgday[i, "credit"] = 0;
                                TDebit += Math.Abs(amt);
                            }
                            else
                                ClBal += amt;
                            TCredit += Math.Abs(amt);
                        }
                        else
                        {
                            cfgday[i, "debit"] = Math.Abs(amt);
                            cfgday[i, "credit"] = 0;
                            if (AcCode.Equals("999999") && !ds.Tables[0].Rows[j]["code"].Equals("200001") && !ds.Tables[0].Rows[j]["bookcode"].Equals("000000"))
                            {
                                cfgday.Rows.Add();
                                i++;
                                cfgday[i, "DATE"] = ds.Tables[0].Rows[j]["date1"];
                                cfgday[i, "ref"] = ds.Tables[0].Rows[j]["transtype"] + ds.Tables[0].Rows[j]["transno"].ToString();
                                cfgday[i, "particulars"] = "By " + ds.Tables[0].Rows[j]["HEAD"].ToString() + " ~  " + ds.Tables[0].Rows[j]["narration"].ToString();
                                cfgday[i, "debit"] = 0;
                                cfgday[i, "credit"] = Math.Abs(amt);
                                TCredit += Math.Abs(amt);
                            }
                            else
                                ClBal += amt;
                            TDebit += Math.Abs(amt);
                        }
                    }
                    int s = cfgday.Rows.Count;
                    switch (BalType)
                    {
                        case 1:
                            if (!j.Equals(ds.Tables[0].Rows.Count - 1))
                                if (!ds.Tables[0].Rows[j + 1]["date1"].Equals(CurDate))
                                {
                                    WriteBal(Convert.ToDateTime(ds.Tables[0].Rows[j + 1]["date1"]));
                                }
                            break;
                        case 2:
                            if (!j.Equals(ds.Tables[0].Rows.Count - 1))
                                if (!ds.Tables[0].Rows[j + 1]["date1"].ToString().Substring(3, 2).Equals(CurDate.Month.ToString("00")))
                                {
                                    WriteBal(Convert.ToDateTime(ds.Tables[0].Rows[j + 1]["date1"]));
                                }
                            break;
                        default:
                            if (!j.Equals(ds.Tables[0].Rows.Count - 1))
                                if (!ds.Tables[0].Rows[j + 1]["date1"].ToString().Substring(6, 4).Equals(CurDate.Year.ToString()))
                                {
                                    WriteBal(Convert.ToDateTime(ds.Tables[0].Rows[j + 1]["date1"]));
                                }
                            break;
                    }
                    if ((cfgday[1, "particulars"].ToString().Length % 28) > 0)
                        cfgday.Rows[i].Height = ((cfgday[i, "particulars"].ToString().Length / 28) + 1) * cfgday.Rows.DefaultSize;
                    else
                        cfgday.Rows[i].Height = (cfgday[i, "particulars"].ToString().Length / 28) * cfgday.Rows.DefaultSize;
                }

                WriteBal(TDate);
                ds.Dispose();

            }
        }

        private void WriteBal(DateTime ds)
        {
            cfgday.Rows.Add();
            i++;
            string suff = "";
            if (ClBal > 0)
                suff = "Cr.";
            else
                suff = "Dr.";

            C1.Win.C1FlexGrid.CellStyle cd = cfgday.Styles.Add("MyThemes");
            cd.Font = new Font(cfgday.Font.Name, 10, FontStyle.Bold);
            cfgday[i, "particulars"] = "Total ";
            cfgday[i, "credit"] = TCredit.ToString("##,###0.00");
            cfgday[i, "debit"] = TDebit.ToString("##,###0.00");
            cfgday.Rows[i].Style = cfgday.Styles["MyThemes"];

            //closing balance

            C1.Win.C1FlexGrid.CellStyle cs = cfgday.Styles.Add("MyTheme");
            cs.Font = new Font(cfgday.Font.Name, 10, FontStyle.Bold);
            cfgday.Rows.Add();
            i++;
            cfgday[i, "particulars"] = "Closing Balance ";
            if (ClBal > 0)
            {
                cfgday[i, "credit"] = Math.Abs(ClBal).ToString("##,###0.00");
            }
            else
            {
                cfgday[i, "debit"] = Math.Abs(ClBal).ToString("##,###0.00");
            }
            cfgday[i, "balance"] = Math.Abs(ClBal).ToString("##,###0.00") + " " + suff;
            cfgday.Rows[i].Style = cfgday.Styles["MyTheme"];

            //opening balance

            if (ds < TDate)
            {
                C1.Win.C1FlexGrid.CellStyle cq = cfgday.Styles.Add("MyThemer");
                cq.Font = new Font(cfgday.Font.Name, 10, FontStyle.Bold);
                cfgday.Rows.Add();
                i++;
                cfgday[i, "DATE"] = ds.ToString("dd/MM/yyyy");
                cfgday[i, "particulars"] = "Opening Balance ";
                if (ClBal > 0)
                {
                    cfgday[i, "credit"] = Math.Abs(ClBal).ToString("##,###0.00");
                }
                else
                {
                    cfgday[i, "debit"] = Math.Abs(ClBal).ToString("##,###0.00");
                }
                cfgday[i, "balance"] = Math.Abs(ClBal).ToString("##,###0.00") + " " + suff;
                cfgday.Rows[i].Style = cfgday.Styles["MyThemer"];

            }
            TCredit = 0;
            TDebit = 0;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            cgen.RepPrint(PrintDos().ToString());
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Interaction.Shell("notepad.exe  "+cPublic.FilePath +"\\" + PrintDos() + ".txt", AppWinStyle.MaximizedFocus, true, 1);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataExport data = new DataExport();
            data.flex_Excel(cfgday);
        }

        private int  PrintDos()
        {
            PageNo = 1;
            lineno = 0;

            int notepadpath = (Convert.ToInt32(1000 * VBMath.Rnd()) + 1);

            if (Directory.Exists(cPublic.FilePath + "\\" + notepadpath + ".txt"))
            {
                Directory.Delete(cPublic.FilePath + "\\" + notepadpath + ".txt");
            }

            string path = cPublic.FilePath + "\\" + notepadpath + ".txt";
            tstream = File.CreateText(path);
            for (int i = 1; i < Convert.ToInt32(cPublic.LinestoReverse); i++)
            {//  lines to reverse
                tstream.Write(Strings.Chr(27) + "j" + Strings.Chr(36));
            }

            PageheadCash(path);
            OpbalDate(path);
            oneline(path);

            tstream.WriteLine(Convert.ToChar(12));
            tstream.Flush();
            tstream.Close();

            return notepadpath;
        }

        public void PageheadCash(string path)
        {
            tstream.WriteLine();
            lineno = lineno + 1;

            if (Strings.Right(cPublic.g_DispName, 1) != "*")
            {
                tstream.Write(cPublic.BchDW);
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_firmname, "C", 40));
                tstream.Write(cPublic.BchNDW);
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add1, "C", 70));
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add2, "C", 70));
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add3, "C", 70));
                tstream.WriteLine(cgen.FnAlignString(cPublic.g_add4, "C", 70));

                if (cPublic.g_TinNo.StartsWith("T") == true) { tstream.WriteLine(cgen.FnAlignString("Tin No:" + cPublic.g_TinNo.Substring(2), "C", 70)); }
                else if (cPublic.g_TinNo.StartsWith("P") == true) { tstream.WriteLine(cgen.FnAlignString("Pin No:" + cPublic.g_TinNo.Substring(2), "C", 70)); }
                else { tstream.WriteLine(cgen.FnAlignString("Tin No:" + cPublic.g_TinNo, "C", 70)); }

                tstream.WriteLine(cgen.FnAlignString(this.Text.Trim(), "C", 70));
                lineno = lineno + 1;

            }

            lineno = lineno + 1;
            tstream.Write("Date:");
            tstream.Write(cPublic.g_from.ToString("dd/MM/yyyy"));
            tstream.Write(Strings.Space(49));
            strdata = Convert.ToString(PageNo);

            tstream.Write(cgen.FnAlignString("Page No." + strdata, "R", 13));
            //
            tstream.WriteLine();


            tstream.WriteLine("|==========|======|====================================|===========|===========|");

            tstream.WriteLine("|Date      |Reff. |          Particulars               | Debit(Rec)|Credit(Pay)|");
            tstream.WriteLine("|----------|------|------------------------------------|-----------|-----------|");
            ////


            lineno = lineno + 5;
            lineno = lineno + 1;
            PageNo = PageNo + 1;
        }

        private void OpbalDate(string path)
        {
            tstream.Write("|");
            tstream.Write(cgen.FnAlignString("Date :" + cfgday[1, 1] + " ".ToString() + "|", "L", 24));
            tstream.Write(cgen.FnAlignString("Opening Balance", "C", 29));

            if (lblcrdr.Text == "Dr")
            {
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString(lblopbvalance.Text, "R", 11));
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString("  ", "R", 11));
                tstream.WriteLine("|");
            }
            else
            {
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString("  ", "R", 11));
                tstream.Write("|");
                tstream.Write(cgen.FnAlignString(lblopbvalance.Text, "R", 11));
                tstream.WriteLine("|");
            }

            tstream.WriteLine("|----------|------|------------------------------------|-----------|-----------|");

        }
        private void oneline(string path)
        {
            string data = string.Empty;
            Int32 part = 0;
            string part1 = string.Empty;
            string part2 = string.Empty;
            string part3 = string.Empty;
            string part4 = string.Empty;
            Boolean datepart = true;
            string rep = "";
            string[] s;
            string partorg = string.Empty;
            string partname = string.Empty;
            string partname1 = string.Empty;

            for (int ii = 1; ii < cfgday.Rows.Count; ii++)
            {
                if (lineno == 39)
                {
                    lineno = 1;
                    // tstream.WriteLine("----------------------------------------------------------------------------------------");

                    //res
                    tstream.WriteLine("-------------------------------------------------------------------------------");
                    //
                    PageheadCash(path);
                }


                for (int jj = 1; jj < cfgday.Cols.Count; jj++)
                {
                    if (cfgday.Cols[jj].Visible == true)
                    {

                        if (cfgday.Cols[jj].Name == "DATE")
                        {
                            if (cfgday[ii, jj] + "".ToString() != "" && datepart == true)
                            {

                                // tstream.Write("|");
                                if (data != cfgday[ii, jj] + "".ToString())
                                {
                                    // tstream.WriteLine(cgen.FnAlignString(cfgday[ii, jj] + "".ToString() + "|      |                                    |           |           |", "L", 80));
                                    datepart = false;
                                }
                                //else
                                //{
                                //    if (cfgday[ii, jj] + "".ToString() != "")
                                //    {
                                //       // MessageBox.Show("");
                                //        tstream.Write(Strings.Space(10));
                                //    }
                                //}
                                data = cfgday[ii, jj] + "".ToString();
                            }

                        }

                        if (cfgday.Cols[jj].Name == "ref")
                        {
                            if (cfgday[ii, jj] + "".ToString() != "")
                            {
                                if (datepart == false && rep == "")
                                // if ( rep == "")
                                {
                                    tstream.Write("|");
                                    tstream.Write(Strings.Space(10));
                                }

                                tstream.Write("|");
                                tstream.Write(cgen.FnAlignString(cfgday[ii, jj] + " ".ToString(), "L", 6));
                                tstream.Write("|");
                            }

                        }
                        if (cfgday.Cols[jj].Name == "particulars")
                        {
                            if (cfgday[ii, jj] + "".ToString() == "Total ")
                            {
                                //  tstream.WriteLine("|---------|----------------------------------------------------|-----------|-----------|");
                                //res
                                // tstream.WriteLine("|---------|------------------------------------------|-----------|-----------|");
                                tstream.WriteLine("|----------|------|------------------------------------|-----------|-----------|");
                                //
                            }

                            if (cfgday[ii, jj] + "".ToString() == "Opening Balance ")
                            {
                                //res
                                //tstream.WriteLine();
                                tstream.WriteLine("-------------------------------------------------------------------------------|");
                                //
                                // tstream.WriteLine("----------------------------------------------------------------------------------------");

                            }
                            if (cfgday[ii, jj] + "".ToString() == "Total ")
                            {

                                tstream.Write("|          |      |");
                                tstream.Write(Strings.Space(21));
                                //  tstream.Write(cgen.FnAlignString(cfgday[ii, jj] + " ".ToString(), "L", 52));
                                //res
                                tstream.Write(cgen.FnAlignString(cfgday[ii, jj] + " ".ToString(), "L", 15));
                                //
                                tstream.Write("|");
                            }
                            else if (cfgday[ii, jj] + "".ToString() == "Closing Balance ")
                            {
                                tstream.Write("|          |      |");
                                tstream.Write(Strings.Space(16));
                                // tstream.Write(cgen.FnAlignString(cfgday[ii, jj] + " ".ToString(), "L", 52));
                                //res
                                tstream.Write(cgen.FnAlignString(cfgday[ii, jj] + " ".ToString(), "L", 20));
                                //
                                tstream.Write("|");
                            }

                            else if (cfgday[ii, jj] + "".ToString() == "Opening Balance ")
                            {

                                tstream.Write(cgen.FnAlignString("|Date  : " + cfgday[ii, 1] + "".ToString(), "L", 39));
                                //res
                                // tstream.Write(cgen.FnAlignString("| Date  : " + cfgday[ii, 1] + "".ToString(), "L", 27));
                                tstream.Write(cgen.FnAlignString(cfgday[ii, jj] + "".ToString(), "L", 16));
                                rep = cfgday[ii, 1] + "".ToString();
                                //
                                //tstream.Write(cgen.FnAlignString(cfgday[ii, jj] + "".ToString(), "L", 26));
                                tstream.Write("|");





                            }
                            else
                            {

                                part = cfgday[ii, jj].ToString().Length;

                                //if (part >= 35)
                                //{
                                //    part1 = cfgday[ii, jj].ToString().Substring(0, 35);
                                //    if (part >= 70)
                                //    {
                                //        part2 = cfgday[ii, jj].ToString().Substring(35, 35);
                                //        if (part >= 105)
                                //        {
                                //            part3 = cfgday[ii, jj].ToString().Substring(70, 35);
                                //            if (part >= 140)
                                //            {
                                //                part4 = cfgday[ii, jj].ToString().Substring(105, 35);
                                //            }
                                //            else { part4 = cfgday[ii, jj].ToString().Substring(105, cfgday[ii, jj].ToString().Length - 105); }
                                //        }
                                //        else { part2 = cfgday[ii, jj].ToString().Substring(70, cfgday[ii, jj].ToString().Length - 70); }
                                //    }
                                //    else { part2 = cfgday[ii, jj].ToString().Substring(35, cfgday[ii, jj].ToString().Length - 35); }
                                //}
                                //else
                                //{
                                //    part1 = cfgday[ii, jj].ToString();
                                //}

                                ////tstream.Write(cgen.FnAlignString(cfgday[ii, jj] + " ".ToString(), "L", 52));
                                //tstream.Write(cgen.FnAlignString(part1, "L", 36));

                                # region splitup

                                string s1;
                                s1 = cfgday[ii, jj].ToString();
                                s = s1.Split('~');
                                partname = s[0];
                                if (s.Length > 1) { partorg = s[1].Trim(); }
                                part = partorg.Length;
                                if (part > 0)
                                {
                                    if (part >= 35)
                                    {
                                        part1 = partorg.Substring(0, 35);
                                        if (part >= 70)
                                        {
                                            part2 = partorg.Substring(35, 35);
                                            if (part >= 105)
                                            {
                                                part3 = partorg.Substring(70, 35);
                                                if (part >= 140)
                                                {
                                                    part4 = partorg.Substring(105, 35);
                                                }
                                                else { part4 = partorg.Substring(105, partorg.Length - 105); }
                                            }
                                            else { part2 = partorg.Substring(70, partorg.Length - 70); }
                                        }
                                        else { part2 = partorg.Substring(35, partorg.Length - 35); }
                                    }
                                    else
                                    {
                                        part1 = partorg;
                                    }
                                }

                                int partlength = 0;
                                partlength = partname.Length;
                                if (partlength >= 35)
                                {
                                    partname1 = partname.Substring(35, partname.Length - 35);
                                }

                                tstream.Write(cgen.FnAlignString(partname, "L", 36));

                                #endregion splitup


                                tstream.Write("|");
                            }
                        }

                        if (cfgday.Cols[jj].Name == "debit")
                        {
                            if (cfgday[ii, jj] + "" != "")
                            {
                                tstream.Write(cgen.FnAlignString(Convert.ToDecimal(cfgday[ii, jj] + "").ToString("##,###0.00"), "R", 11));
                                //res
                                //tstream.Write(cgen.FnAlignString(Convert.ToDecimal(cfgday[ii, jj] + "").ToString("##,###0.00"), "R", 1));

                                tstream.Write("|");
                            }
                            else
                            {
                                tstream.Write(Strings.Space(11));
                                tstream.Write("|");
                            }
                        }

                        if (cfgday.Cols[jj].Name == "credit")
                        {
                            if (cfgday[ii, jj] + "" != "")
                            {
                                tstream.Write(cgen.FnAlignString(Convert.ToDecimal(cfgday[ii, jj] + " ").ToString("##,###0.00"), "R", 11));
                                tstream.Write("|");
                            }
                            else
                            {
                                tstream.Write(Strings.Space(11));
                                tstream.Write("|");
                            }
                            if (cfgday[ii, 3] + "".ToString() == "Opening Balance ")
                            {

                                tstream.WriteLine();
                                // tstream.WriteLine("----------------------------------------------------------------------------------------");
                                //res
                                tstream.WriteLine("-------------------------------------------------------------------------------|");
                                //res
                                // tstream.WriteLine(cgen.FnAlignString("|" + rep + "|      |                                    |           |           |", "L", 80));
                                rep = "";
                            }

                        }


                    }


                }
                if (cfgday[ii, 3] + "".ToString() != "Opening Balance ")
                {

                    if (part2 != "")
                    {
                        tstream.WriteLine();
                        tstream.Write("|          |      |");
                        tstream.Write(cgen.FnAlignString(part2, "L", 36));
                        tstream.Write("|           |           |");
                        part2 = "";
                    }
                    if (part3 != "")
                    {
                        tstream.WriteLine();
                        tstream.Write("|          |      |");
                        tstream.Write(cgen.FnAlignString(part3, "L", 36));
                        tstream.Write("|           |           |");
                        part3 = "";
                    }
                    if (part4 != "")
                    {
                        tstream.WriteLine();
                        tstream.Write("|          |      |");
                        tstream.Write(cgen.FnAlignString(part4, "L", 36));
                        tstream.Write("|           |           |");
                        part4 = "";
                    }

                    tstream.WriteLine();

                }



                lineno += 1;
            }
            //  tstream.WriteLine("----------------------------------------------------------------------------------------");
            //res
            tstream.WriteLine("--------------------------------------------------------------------------------");
            //
        }


    }
}
