namespace Xpire.Reports
{
    partial class ucLedger
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucLedger));
            this.btnClose = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.lblcredit = new System.Windows.Forms.Label();
            this.lbldebit = new System.Windows.Forms.Label();
            this.pnldiff = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.lbldiffcrdr = new System.Windows.Forms.Label();
            this.lbldiff = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblcurrentbalance = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlbalance = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblcrdr = new System.Windows.Forms.Label();
            this.lblopbvalance = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.gpserach = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkchequepassed = new System.Windows.Forms.CheckBox();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.lblvouchertype = new System.Windows.Forms.Label();
            this.txtamount = new System.Windows.Forms.TextBox();
            this.cmbvoucher = new System.Windows.Forms.ComboBox();
            this.cmbamount = new System.Windows.Forms.ComboBox();
            this.lblAmount = new System.Windows.Forms.Label();
            this.rdbCredit = new System.Windows.Forms.RadioButton();
            this.rdbDebit = new System.Windows.Forms.RadioButton();
            this.rdallvoy = new System.Windows.Forms.RadioButton();
            this.dtpto = new System.Windows.Forms.DateTimePicker();
            this.dtpfrom = new System.Windows.Forms.DateTimePicker();
            this.lbltodate = new System.Windows.Forms.Label();
            this.lblfrmdate = new System.Windows.Forms.Label();
            this.c1FlexGrid2 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.txtHead = new System.Windows.Forms.TextBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5.SuspendLayout();
            this.pnldiff.SuspendLayout();
            this.panel6.SuspendLayout();
            this.pnlbalance.SuspendLayout();
            this.gpserach.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnClose.Location = new System.Drawing.Point(716, 536);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.lblcredit);
            this.panel5.Controls.Add(this.lbldebit);
            this.panel5.Location = new System.Drawing.Point(355, 450);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(436, 34);
            this.panel5.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "Total Balance";
            // 
            // lblcredit
            // 
            this.lblcredit.BackColor = System.Drawing.Color.LightBlue;
            this.lblcredit.Location = new System.Drawing.Point(283, 7);
            this.lblcredit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblcredit.Name = "lblcredit";
            this.lblcredit.Size = new System.Drawing.Size(149, 20);
            this.lblcredit.TabIndex = 1;
            this.lblcredit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbldebit
            // 
            this.lbldebit.BackColor = System.Drawing.Color.LightBlue;
            this.lbldebit.Location = new System.Drawing.Point(120, 7);
            this.lbldebit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbldebit.Name = "lbldebit";
            this.lbldebit.Size = new System.Drawing.Size(149, 20);
            this.lbldebit.TabIndex = 1;
            this.lbldebit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnldiff
            // 
            this.pnldiff.BackColor = System.Drawing.Color.Transparent;
            this.pnldiff.Controls.Add(this.label10);
            this.pnldiff.Controls.Add(this.lbldiffcrdr);
            this.pnldiff.Controls.Add(this.lbldiff);
            this.pnldiff.Location = new System.Drawing.Point(15, 450);
            this.pnldiff.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pnldiff.Name = "pnldiff";
            this.pnldiff.Size = new System.Drawing.Size(332, 34);
            this.pnldiff.TabIndex = 13;
            this.pnldiff.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 8);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(150, 14);
            this.label10.TabIndex = 1;
            this.label10.Text = "Difference In Balance";
            // 
            // lbldiffcrdr
            // 
            this.lbldiffcrdr.BackColor = System.Drawing.Color.Crimson;
            this.lbldiffcrdr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbldiffcrdr.Location = new System.Drawing.Point(293, 6);
            this.lbldiffcrdr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbldiffcrdr.Name = "lbldiffcrdr";
            this.lbldiffcrdr.Size = new System.Drawing.Size(25, 22);
            this.lbldiffcrdr.TabIndex = 1;
            // 
            // lbldiff
            // 
            this.lbldiff.BackColor = System.Drawing.Color.Crimson;
            this.lbldiff.Location = new System.Drawing.Point(168, 7);
            this.lbldiff.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbldiff.Name = "lbldiff";
            this.lbldiff.Size = new System.Drawing.Size(119, 21);
            this.lbldiff.TabIndex = 1;
            this.lbldiff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.lblcurrentbalance);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Location = new System.Drawing.Point(457, 496);
            this.panel6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(334, 34);
            this.panel6.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Thistle;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(298, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 21);
            this.label1.TabIndex = 4;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblcurrentbalance
            // 
            this.lblcurrentbalance.BackColor = System.Drawing.Color.Thistle;
            this.lblcurrentbalance.Location = new System.Drawing.Point(127, 7);
            this.lblcurrentbalance.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblcurrentbalance.Name = "lblcurrentbalance";
            this.lblcurrentbalance.Size = new System.Drawing.Size(166, 21);
            this.lblcurrentbalance.TabIndex = 3;
            this.lblcurrentbalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 14);
            this.label7.TabIndex = 1;
            this.label7.Text = "Closing Balance";
            // 
            // pnlbalance
            // 
            this.pnlbalance.BackColor = System.Drawing.Color.Transparent;
            this.pnlbalance.Controls.Add(this.label3);
            this.pnlbalance.Controls.Add(this.lblcrdr);
            this.pnlbalance.Controls.Add(this.lblopbvalance);
            this.pnlbalance.Location = new System.Drawing.Point(443, 21);
            this.pnlbalance.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pnlbalance.Name = "pnlbalance";
            this.pnlbalance.Size = new System.Drawing.Size(334, 29);
            this.pnlbalance.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "Opening Balance";
            // 
            // lblcrdr
            // 
            this.lblcrdr.BackColor = System.Drawing.Color.Ivory;
            this.lblcrdr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblcrdr.Location = new System.Drawing.Point(301, 6);
            this.lblcrdr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblcrdr.Name = "lblcrdr";
            this.lblcrdr.Size = new System.Drawing.Size(23, 16);
            this.lblcrdr.TabIndex = 1;
            this.lblcrdr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblopbvalance
            // 
            this.lblopbvalance.BackColor = System.Drawing.SystemColors.Info;
            this.lblopbvalance.Location = new System.Drawing.Point(127, 7);
            this.lblopbvalance.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblopbvalance.Name = "lblopbvalance";
            this.lblopbvalance.Size = new System.Drawing.Size(166, 14);
            this.lblopbvalance.TabIndex = 1;
            this.lblopbvalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.Silver;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnExport.Location = new System.Drawing.Point(635, 536);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 32);
            this.btnExport.TabIndex = 8;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // gpserach
            // 
            this.gpserach.BackColor = System.Drawing.Color.Gainsboro;
            this.gpserach.Controls.Add(this.panel1);
            this.gpserach.Location = new System.Drawing.Point(208, 171);
            this.gpserach.Name = "gpserach";
            this.gpserach.Size = new System.Drawing.Size(379, 187);
            this.gpserach.TabIndex = 9;
            this.gpserach.TabStop = false;
            this.gpserach.Text = "Search";
            this.gpserach.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.chkchequepassed);
            this.panel1.Controls.Add(this.btnShowAll);
            this.panel1.Controls.Add(this.btnView);
            this.panel1.Controls.Add(this.lblvouchertype);
            this.panel1.Controls.Add(this.txtamount);
            this.panel1.Controls.Add(this.cmbvoucher);
            this.panel1.Controls.Add(this.cmbamount);
            this.panel1.Controls.Add(this.lblAmount);
            this.panel1.Controls.Add(this.rdbCredit);
            this.panel1.Controls.Add(this.rdbDebit);
            this.panel1.Controls.Add(this.rdallvoy);
            this.panel1.Controls.Add(this.dtpto);
            this.panel1.Controls.Add(this.dtpfrom);
            this.panel1.Controls.Add(this.lbltodate);
            this.panel1.Controls.Add(this.lblfrmdate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(373, 168);
            this.panel1.TabIndex = 0;
            // 
            // chkchequepassed
            // 
            this.chkchequepassed.AutoSize = true;
            this.chkchequepassed.Location = new System.Drawing.Point(17, 134);
            this.chkchequepassed.Name = "chkchequepassed";
            this.chkchequepassed.Size = new System.Drawing.Size(101, 17);
            this.chkchequepassed.TabIndex = 14;
            this.chkchequepassed.Text = "&Cheque Passed";
            this.chkchequepassed.UseVisualStyleBackColor = true;
            // 
            // btnShowAll
            // 
            this.btnShowAll.BackColor = System.Drawing.Color.Silver;
            this.btnShowAll.Location = new System.Drawing.Point(276, 134);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(69, 23);
            this.btnShowAll.TabIndex = 9;
            this.btnShowAll.Text = "Show All";
            this.btnShowAll.UseVisualStyleBackColor = false;
            this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.Silver;
            this.btnView.Location = new System.Drawing.Point(201, 134);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(69, 23);
            this.btnView.TabIndex = 8;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblvouchertype
            // 
            this.lblvouchertype.AutoSize = true;
            this.lblvouchertype.Location = new System.Drawing.Point(25, 106);
            this.lblvouchertype.Name = "lblvouchertype";
            this.lblvouchertype.Size = new System.Drawing.Size(74, 13);
            this.lblvouchertype.TabIndex = 12;
            this.lblvouchertype.Text = "Voucher Type";
            // 
            // txtamount
            // 
            this.txtamount.Location = new System.Drawing.Point(159, 72);
            this.txtamount.MaxLength = 12;
            this.txtamount.Name = "txtamount";
            this.txtamount.Size = new System.Drawing.Size(160, 20);
            this.txtamount.TabIndex = 6;
            this.txtamount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtamount_KeyPress);
            // 
            // cmbvoucher
            // 
            this.cmbvoucher.FormattingEnabled = true;
            this.cmbvoucher.Items.AddRange(new object[] {
            "All",
            "Purchase",
            "Sales",
            "Payment",
            "Reciept",
            "Journal",
            "Credit",
            "Debit"});
            this.cmbvoucher.Location = new System.Drawing.Point(123, 103);
            this.cmbvoucher.Name = "cmbvoucher";
            this.cmbvoucher.Size = new System.Drawing.Size(196, 21);
            this.cmbvoucher.TabIndex = 7;
            this.cmbvoucher.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbamount_KeyDown);
            // 
            // cmbamount
            // 
            this.cmbamount.FormattingEnabled = true;
            this.cmbamount.Items.AddRange(new object[] {
            "=",
            "<",
            ">",
            "<>",
            "<=",
            ">="});
            this.cmbamount.Location = new System.Drawing.Point(87, 71);
            this.cmbamount.Name = "cmbamount";
            this.cmbamount.Size = new System.Drawing.Size(49, 21);
            this.cmbamount.TabIndex = 5;
            this.cmbamount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbamount_KeyDown);
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(20, 74);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(43, 13);
            this.lblAmount.TabIndex = 11;
            this.lblAmount.Text = "Amount";
            // 
            // rdbCredit
            // 
            this.rdbCredit.AutoSize = true;
            this.rdbCredit.Location = new System.Drawing.Point(240, 43);
            this.rdbCredit.Name = "rdbCredit";
            this.rdbCredit.Size = new System.Drawing.Size(95, 17);
            this.rdbCredit.TabIndex = 4;
            this.rdbCredit.Text = "Credit Voucher";
            this.rdbCredit.UseVisualStyleBackColor = true;
            // 
            // rdbDebit
            // 
            this.rdbDebit.AutoSize = true;
            this.rdbDebit.Location = new System.Drawing.Point(118, 43);
            this.rdbDebit.Name = "rdbDebit";
            this.rdbDebit.Size = new System.Drawing.Size(93, 17);
            this.rdbDebit.TabIndex = 3;
            this.rdbDebit.Text = "Debit Voucher";
            this.rdbDebit.UseVisualStyleBackColor = true;
            // 
            // rdallvoy
            // 
            this.rdallvoy.AutoSize = true;
            this.rdallvoy.Checked = true;
            this.rdallvoy.Location = new System.Drawing.Point(17, 43);
            this.rdallvoy.Name = "rdallvoy";
            this.rdallvoy.Size = new System.Drawing.Size(79, 17);
            this.rdallvoy.TabIndex = 2;
            this.rdallvoy.TabStop = true;
            this.rdallvoy.Text = "All Voucher";
            this.rdallvoy.UseVisualStyleBackColor = true;
            // 
            // dtpto
            // 
            this.dtpto.CustomFormat = "dd/MM/yyyy";
            this.dtpto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpto.Location = new System.Drawing.Point(252, 7);
            this.dtpto.Name = "dtpto";
            this.dtpto.Size = new System.Drawing.Size(100, 20);
            this.dtpto.TabIndex = 1;
            // 
            // dtpfrom
            // 
            this.dtpfrom.CustomFormat = "dd/MM/yyyy";
            this.dtpfrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpfrom.Location = new System.Drawing.Point(63, 7);
            this.dtpfrom.Name = "dtpfrom";
            this.dtpfrom.Size = new System.Drawing.Size(100, 20);
            this.dtpfrom.TabIndex = 0;
            // 
            // lbltodate
            // 
            this.lbltodate.AutoSize = true;
            this.lbltodate.Location = new System.Drawing.Point(216, 11);
            this.lbltodate.Name = "lbltodate";
            this.lbltodate.Size = new System.Drawing.Size(26, 13);
            this.lbltodate.TabIndex = 13;
            this.lbltodate.Text = "To :";
            // 
            // lblfrmdate
            // 
            this.lblfrmdate.AutoSize = true;
            this.lblfrmdate.Location = new System.Drawing.Point(10, 11);
            this.lblfrmdate.Name = "lblfrmdate";
            this.lblfrmdate.Size = new System.Drawing.Size(36, 13);
            this.lblfrmdate.TabIndex = 10;
            this.lblfrmdate.Text = "From :";
            // 
            // c1FlexGrid2
            // 
            this.c1FlexGrid2.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.c1FlexGrid2.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.c1FlexGrid2.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.c1FlexGrid2.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.c1FlexGrid2.BackColor = System.Drawing.SystemColors.Window;
            this.c1FlexGrid2.ColumnInfo = resources.GetString("c1FlexGrid2.ColumnInfo");
            this.c1FlexGrid2.ExtendLastCol = true;
            this.c1FlexGrid2.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.c1FlexGrid2.Location = new System.Drawing.Point(14, 56);
            this.c1FlexGrid2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.c1FlexGrid2.Name = "c1FlexGrid2";
            this.c1FlexGrid2.Rows.Count = 1;
            this.c1FlexGrid2.Rows.MinSize = 25;
            this.c1FlexGrid2.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.c1FlexGrid2.Size = new System.Drawing.Size(777, 387);
            this.c1FlexGrid2.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("c1FlexGrid2.Styles"));
            this.c1FlexGrid2.TabIndex = 7;
            this.c1FlexGrid2.Tree.LineColor = System.Drawing.Color.Gray;
            this.c1FlexGrid2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.c1FlexGrid2_KeyDown);
            // 
            // txtHead
            // 
            this.txtHead.Location = new System.Drawing.Point(99, 21);
            this.txtHead.Name = "txtHead";
            this.txtHead.Size = new System.Drawing.Size(230, 20);
            this.txtHead.TabIndex = 14;
            this.txtHead.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHead_KeyDown);
            this.txtHead.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHead_KeyPress);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(347, 21);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 15;
            this.btnLoad.Text = "&Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Ledger Head";
            // 
            // ucLedger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.txtHead);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.pnldiff);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pnlbalance);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.gpserach);
            this.Controls.Add(this.c1FlexGrid2);
            this.Controls.Add(this.btnClose);
            this.Name = "ucLedger";
            this.Size = new System.Drawing.Size(801, 574);
            this.Load += new System.EventHandler(this.ucLedger_Load);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.pnldiff.ResumeLayout(false);
            this.pnldiff.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.pnlbalance.ResumeLayout(false);
            this.pnlbalance.PerformLayout();
            this.gpserach.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblcredit;
        private System.Windows.Forms.Label lbldebit;
        private System.Windows.Forms.Panel pnldiff;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbldiffcrdr;
        private System.Windows.Forms.Label lbldiff;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblcurrentbalance;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlbalance;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblcrdr;
        private System.Windows.Forms.Label lblopbvalance;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.GroupBox gpserach;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.CheckBox chkchequepassed;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Label lblvouchertype;
        private System.Windows.Forms.TextBox txtamount;
        private System.Windows.Forms.ComboBox cmbvoucher;
        private System.Windows.Forms.ComboBox cmbamount;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.RadioButton rdbCredit;
        private System.Windows.Forms.RadioButton rdbDebit;
        private System.Windows.Forms.RadioButton rdallvoy;
        public System.Windows.Forms.DateTimePicker dtpto;
        public System.Windows.Forms.DateTimePicker dtpfrom;
        private System.Windows.Forms.Label lbltodate;
        private System.Windows.Forms.Label lblfrmdate;
        private C1.Win.C1FlexGrid.C1FlexGrid c1FlexGrid2;
        private System.Windows.Forms.TextBox txtHead;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label label2;
    }
}
