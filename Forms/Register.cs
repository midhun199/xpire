﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Net.NetworkInformation;
using CodeBlock_Demo_Author;
using Xpire.Classes;

namespace Xpire.Forms
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        cGeneral gen = new cGeneral();

        static byte[] bytes = ASCIIEncoding.ASCII.GetBytes("ZeroCool");

        string MacAddress;

        private void btnRegister_Click(object sender, EventArgs e)
        {
            CConvert CConvert = new CodeBlock_Demo_Author.CConvert();

            Serials Serials = new CodeBlock_Demo_Author.Serials();

            string serial = Serials.SerialFromKey(Convert.ToInt32(CConvert.FromBase36(gen.GetMacAddress().Substring(0, 5)))).ToString();

            //MacAddress = Decrypt(txtRegister.Text);



            if (txtRegister.Text == serial)
            {
                if (gen.RegistryKey(cPublic.LoginDate, gen.GetMacAddress(), true))
                {
                    MessageBox.Show("Registered Successfully", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    this.Close();
                }
            }
        }

       

        /// <summary>
        /// Decrypt a crypted string.
        /// </summary>
        /// <param name="cryptedString">The crypted string.</param>
        /// <returns>The decrypted string.</returns>
        /// <exception cref="ArgumentNullException">This exception will be thrown 
        /// when the crypted string is null or empty.</exception>
        public static string Decrypt(string cryptedString)
        {
            if (String.IsNullOrEmpty(cryptedString))
            {
                throw new ArgumentNullException
                   ("The string which needs to be decrypted can not be null.");
            }
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream
                    (Convert.FromBase64String(cryptedString));
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtRegister.Clear();
        }
    }
}
