﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Xpire.BL;

namespace Xpire.Forms
{
    public partial class frmOtherDetails : Form
    {
        CustomerBO customerbo=null ;
        public frmOtherDetails(CustomerBO customerBO)
        {
            InitializeComponent();

            this.customerbo = customerBO;

            this.txtCustomerName.Text = this.customerbo.CustomerName;
            this.txtAddress .Text = this.customerbo.Address;
            this.txtContactNo.Text = this.customerbo.ContactNo;   
        }

        private void frmOtherDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            { SendKeys.Send("{TAB}"); }
            else if (e.KeyCode == Keys.Escape)
            { this.Close(); }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.customerbo.CustomerName = this.txtCustomerName.Text;
            this.customerbo.Address = this.txtAddress.Text;
            this.customerbo.ContactNo = this.txtContactNo.Text;

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
