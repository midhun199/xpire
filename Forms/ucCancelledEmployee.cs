﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using System.IO;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using C1.Win.C1FlexGrid;
using Xpire.BL;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Forms
{
    public partial class ucCancelledEmployee : UserControl
    {
        SqlDataReader rdr = null;
        DataTable dtable = new DataTable();
        cConnection con = new cConnection();
        DataSet ds = new DataSet();
        SqlCommand cmd = null;
        DataTable dt = new DataTable();
        string id = string.Empty;
        string sqlQuery = string.Empty, nxtPrv = string.Empty;
        Boolean searchClick = false, autoCode = false, EscKey = false;
        cGeneral gen = new cGeneral();
        string filename = string.Empty;
        string email = string.Empty;

        public ucCancelledEmployee()
        {
            InitializeComponent();
        }


        private void loadFirstRecord()
        {
           
                sqlQuery = "select top 1  a.id,a.[DateOfJoining],a.[DateOfArrival],a.[StaffID],a.[StaffName],f.Code GradeCode, f.Details GradeDetails,b.Code DepCode, b.Details DepDetails, d.Code QlfCode, d.Details QlfDetails," +
                           "c.Code DesCode, c.Details DesDetails,a.[basic],a.[allowance],a.[DrivingLicence],a.[DrivingLicenceNo],a.[PrevEmployer], a.YearOfExperience,a.PhoneNo,a.MobileNo, a.Email,a.DOB,a.Gender,a.[Marital]," +
                           "a.[Spouse],e.Code NatCode, e.Details NatDetails,a.[Religion],a.[Dependent],a.FatherName,a.[MotherName],a.[NName],a.[NRelation],a.[NPhoneNo],a.[NMobileNo]," +
                           "a.[NEmail],a.NPermanentAddress1,a.NPermanentAddress2,a.NPermanentAddress3,a.NPermanentAddress4,a.NTemporaryAddress1,a.NTemporaryAddress2,a.NTemporaryAddress3,a.NTemporaryAddress4," +
                           "a.[DName],a.[DRelation],a.[DPhoneNo],a.[DMobileNo],a.[DEmail],a.DPermanentAddress1,a.DPermanentAddress2,a.DPermanentAddress3,a.DPermanentAddress4," +
                           "a.DTemporaryAddress1,a.DTemporaryAddress2,a.DTemporaryAddress3,a.DTemporaryAddress4, " +
                           "a.Picture,a.remarks from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code " +
                           "join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code join LOOKUP  f on a.[grade]= f.code where a.cancel=1 ORDER BY CONVERT(INT, Replace(a.staffid, '" + cPublic.Abbr + "',''))";

                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
          
            LoadDetails(cmd);

        }


        private int getItemCount()
        {
            sqlQuery = "select  isnull(count(*),0) from Employee" + cPublic.g_firmcode +" where cancel=1";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            return Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }

        private Boolean LoadDetails(SqlCommand cmd)
        {
            //CSP s = new CSP();
            //s.job();


            Boolean status = true;
            SqlDataReader dr;
            cmd.Connection = cPublic.Connection;
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                id = dr["id"].ToString();
                dtpDOJ.Text = dr["DateOfJoining"].ToString();
                dtpDOA.Text = dr["DateOfArrival"].ToString();
                StaffID= dr["StaffID"].ToString();
                txtStaffID.Text = dr["StaffID"].ToString();
                txtStaffName.Text = dr["StaffName"].ToString();
                txtGrade.Tag = dr["GradeCode"].ToString();
                txtGrade.Text = dr["GradeDetails"].ToString();
                txtDepartment.Tag = dr["DepCode"].ToString();
                txtDepartment.Text = dr["DepDetails"].ToString();
                txtQualifications.Tag = dr["QlfCode"].ToString();
                txtQualifications.Text = dr["QlfDetails"].ToString();
                txtDesignation.Tag = dr["DesCode"].ToString();
                txtDesignation.Text = dr["DesDetails"].ToString();
                txtBasic.Text= dr["basic"].ToString();
                txtAllowance.Text = dr["allowance"].ToString();
                cmbDrvLicense.Text = dr["DrivingLicence"].ToString();
                txtLicenseNo.Text = dr["DrivingLicenceNo"].ToString();
                txtPrevEmp.Text = dr["PrevEmployer"].ToString();
                txtYOP.Text = dr["YearOfExperience"].ToString();
                txtPhoneNo.Text = dr["PhoneNo"].ToString();
                txtMobileNo.Text = dr["MobileNo"].ToString();
                txtEmail.Text = dr["Email"].ToString();

                dtpDOB.Text = dr["DOB"].ToString();
                cmbGender.Text = dr["Gender"].ToString();
                cmbMarital.Text = dr["Marital"].ToString();
                txtSpouse.Text = dr["Spouse"].ToString();
                txtNationality.Tag = dr["NatCode"].ToString();
                txtNationality.Text = dr["NatDetails"].ToString();
                txtReligion.Text = dr["Religion"].ToString();
                txtDependant.Text = dr["Dependent"].ToString();
                txtFatherName.Text = dr["FatherName"].ToString();
                txtMotherName.Text = dr["MotherName"].ToString();

                txtNName.Text = dr["NName"].ToString();
                txtNRelation.Text = dr["NRelation"].ToString();
                txtNPhoneNo.Text = dr["NPhoneNo"].ToString();
                txtNMobileNo.Text = dr["NMobileNo"].ToString();
                txtNEmail.Text = dr["NEmail"].ToString();
                txtNPAddress1.Text = dr["NPermanentAddress1"].ToString();
                txtNPAddress2.Text = dr["NPermanentAddress2"].ToString();
                txtNPAddress3.Text = dr["NPermanentAddress3"].ToString();
                txtNPAddress4.Text = dr["NPermanentAddress4"].ToString();
                txtNTAddress1.Text = dr["NTemporaryAddress1"].ToString();
                txtNTAddress2.Text = dr["NTemporaryAddress2"].ToString();
                txtNTAddress3.Text = dr["NTemporaryAddress3"].ToString();
                txtNTAddress4.Text = dr["NTemporaryAddress4"].ToString();
                txtDName.Text = dr["DName"].ToString();
                txtDRelation.Text = dr["DRelation"].ToString();
                txtDPhoneNo.Text = dr["DPhoneNo"].ToString();
                txtDMobileNo.Text = dr["DMobileNo"].ToString();
                txtDEmail.Text = dr["DEmail"].ToString();
                txtDPAddress1.Text = dr["DPermanentAddress1"].ToString();
                txtDPAddress2.Text = dr["DPermanentAddress2"].ToString();
                txtDPAddress3.Text = dr["DPermanentAddress3"].ToString();
                txtDPAddress4.Text = dr["DPermanentAddress4"].ToString();
                txtDTAddress1.Text = dr["DTemporaryAddress1"].ToString();
                txtDTAddress2.Text = dr["DTemporaryAddress2"].ToString();
                txtDTAddress3.Text = dr["DTemporaryAddress3"].ToString();
                txtDTAddress4.Text = dr["DTemporaryAddress4"].ToString();
                txtRemarks.Text = dr["remarks"].ToString();

                object theValue = dr["Picture"];

                if (DBNull.Value != theValue)
                {
                    byte[] data = (byte[])dr["Picture"];
                    MemoryStream ms = new MemoryStream(data);
                    pictureBox1.Image = Image.FromStream(ms);
                }
                else
                    pictureBox1.Image = null;

                //chkRenewed.Checked = Convert.ToBoolean(dr["Renewal"].ToString());                this.txtDocumentType.Text = dr["details"].ToString();


                this.Tag = "Data Loaded";

            }
            else { status = false; }
            dr.Close();

            FILL_LISTVIEW();

            gen.EDControls(groupBox1, false);
            //gen.EDControls(groupBox2, false);
            gen.EDControls(groupBox3, false);
            gen.EDControls(groupBox4, false);
            gen.EDControls(groupBox5, false);
      
            gen.EDControls(groupBox7, false);
   
            gen.EDControls(groupBox9, false);

            int count = getItemCount();
            if (nxtPrv.Equals("next") && !status)
            {
                nxtPrv = string.Empty;
            }
            else if (nxtPrv.Equals("previous") && !status)
            {
                nxtPrv = string.Empty;
            }
            if (count == 0)
            { btnEdit.Enabled = false; btnDelete.Enabled = false; }
            btnCancelEmp.Enabled = false;

            return status;
        }

        private void ucEmployeeRegistration_Load(object sender, EventArgs e)
        {
           
                btnAdd.Enabled = false;
                btnCancelEmp.Enabled = false;
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;

                loadFirstRecord();
            lblItemCount.Text = "No of Employees : " + getItemCount().ToString();
        }

        private void FILL_LISTVIEW()
        {
           
            this.lisList.Items.Clear();
           

            fill_Listview();
        }

        private void fill_Listview()
        {
           
            try
            {
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select a.[id] Docid,a.[staffid],a.[name] DocName, a.[IssDate] IssDate,a.[ExpDate] ExpDate,b.[code] DocTypeCode,b.[details] DocTypeDetails,a.[DocumentNo] DocNo,a.filename,a.[Attachment] Attch from EmployeeDocument" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code where a.staffid = @staffid";
                cmd.Parameters.AddWithValue("@staffid", id);

                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    
                    ListViewItem item1 = new ListViewItem();
                    item1.SubItems[0].Text=rdr["Docid"].ToString();
                    item1.SubItems.Add(rdr["DocTypeCode"].ToString());
                    item1.SubItems.Add(rdr["DocTypeDetails"].ToString());
                    item1.SubItems.Add(rdr["DocName"].ToString());
                    item1.SubItems.Add(rdr["IssDate"].ToString());
                    item1.SubItems.Add(rdr["ExpDate"].ToString());
                    item1.SubItems.Add(rdr["DocNo"].ToString());
                    item1.SubItems.Add(rdr["filename"].ToString());

                    object theValue = rdr["Attch"];

                    if (DBNull.Value != theValue)
                    {
                        byte[] data = (byte[])rdr["Attch"];
                        //MemoryStream ms = new MemoryStream(data);
                        //pictureBox1.Image = Image.FromStream(ms);
                        item1.SubItems.Add(data.ToString());
                        string temfilename = Application.StartupPath + @"\" + @rdr["filename"].ToString();

                       if(! File.Exists(temfilename))
                            {
                           

                            using (System.IO.FileStream fs = new System.IO.FileStream(temfilename, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite))

                            {
                                fs.Write(data, 0, data.Length);
                                fs.Flush();
                                fs.Close();


                            }
                        }
                    }
                    item1.SubItems.Add("0");

                    this.lisList.Items.Add(item1);
    

                }
                rdr.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), cPublic.messagename);
            }
            finally
            {

            }
        }

        public bool varification(string verify)
        {
            return Regex.IsMatch(verify, "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
        }

        private void EnableButtons(Boolean status)
        {
            btnSearch.Enabled = status;
            btnNext.Enabled = status;
            btnPrevious.Enabled = status;
            btnEdit.Enabled = status;
            btnDelete.Enabled = status;
            btnAdd.Enabled = status;
            btnExit.Enabled = status;
            btnCancelEmp.Enabled = status;

        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            nxtPrv = "previous";
           
                sqlQuery = "select top 1   a.id,a.[DateOfJoining],a.[DateOfArrival],a.[StaffID],a.[StaffName],f.Code GradeCode, f.Details GradeDetails,b.Code DepCode, b.Details DepDetails, d.Code QlfCode, d.Details QlfDetails," +
                       "c.Code DesCode, c.Details DesDetails,a.[basic],a.[allowance],a.[DrivingLicence],a.[DrivingLicenceNo],a.[PrevEmployer], a.YearOfExperience,a.PhoneNo,a.MobileNo, a.Email,a.DOB,a.Gender,a.[Marital]," +
                       "a.[Spouse],e.Code NatCode, e.Details NatDetails,a.[Religion],a.[Dependent],a.FatherName,a.[MotherName],a.[NName],a.[NRelation],a.[NPhoneNo],a.[NMobileNo]," +
                       "a.[NEmail],a.NPermanentAddress1,a.NPermanentAddress2,a.NPermanentAddress3,a.NPermanentAddress4,a.NTemporaryAddress1,a.NTemporaryAddress2,a.NTemporaryAddress3,a.NTemporaryAddress4," +
                       "a.[DName],a.[DRelation],a.[DPhoneNo],a.[DMobileNo],a.[DEmail],a.DPermanentAddress1,a.DPermanentAddress2,a.DPermanentAddress3,a.DPermanentAddress4," +
                       "a.DTemporaryAddress1,a.DTemporaryAddress2,a.DTemporaryAddress3,a.DTemporaryAddress4, " +
                       "a.Picture,a.remarks from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code " +
                       "join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code join LOOKUP  f on a.[grade]= f.code where CONVERT(INT, Replace(a.staffid, '" + cPublic.Abbr + "','')) < CONVERT(INT, Replace(@id, '" + cPublic.Abbr + "',''))  and a.cancel=1 ORDER BY CONVERT(INT, Replace(a.staffid, '" + cPublic.Abbr + "','')) desc";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                string ID = string.IsNullOrEmpty(StaffID.Trim()) ? "0" : StaffID.Trim();
                cmd.Parameters.AddWithValue("@id", ID);
           
                LoadDetails(cmd);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            nxtPrv = "next";

          
            
                sqlQuery = "select top 1   a.id,a.[DateOfJoining],a.[DateOfArrival],a.[StaffID],a.[StaffName],f.Code GradeCode, f.Details GradeDetails,b.Code DepCode, b.Details DepDetails, d.Code QlfCode, d.Details QlfDetails," +
                       "c.Code DesCode, c.Details DesDetails,a.[basic],a.[allowance],a.[DrivingLicence],a.[DrivingLicenceNo],a.[PrevEmployer], a.YearOfExperience,a.PhoneNo,a.MobileNo, a.Email,a.DOB,a.Gender,a.[Marital]," +
                       "a.[Spouse],e.Code NatCode, e.Details NatDetails,a.[Religion],a.[Dependent],a.FatherName,a.[MotherName],a.[NName],a.[NRelation],a.[NPhoneNo],a.[NMobileNo]," +
                       "a.[NEmail],a.NPermanentAddress1,a.NPermanentAddress2,a.NPermanentAddress3,a.NPermanentAddress4,a.NTemporaryAddress1,a.NTemporaryAddress2,a.NTemporaryAddress3,a.NTemporaryAddress4," +
                       "a.[DName],a.[DRelation],a.[DPhoneNo],a.[DMobileNo],a.[DEmail],a.DPermanentAddress1,a.DPermanentAddress2,a.DPermanentAddress3,a.DPermanentAddress4," +
                       "a.DTemporaryAddress1,a.DTemporaryAddress2,a.DTemporaryAddress3,a.DTemporaryAddress4, " +
                       "a.Picture,a.remarks from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code " +
                       "join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code join LOOKUP  f on a.[grade]= f.code where CONVERT(INT, Replace(a.staffid, '" + cPublic.Abbr + "','')) > CONVERT(INT, Replace(@id, '" + cPublic.Abbr + "',''))  and a.cancel=1 ORDER BY CONVERT(INT, Replace(a.staffid, '" + cPublic.Abbr + "','')) ";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);

                string ID = string.IsNullOrEmpty(StaffID.Trim()) ? "0" : StaffID.Trim();
                cmd.Parameters.AddWithValue("@id", ID);
           
                LoadDetails(cmd);
        }
        public static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "123456789".ToCharArray();
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[maxSize];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
        private void auto()
        {
            txtStaffID.Text = "E-" + GetUniqueKey(6);
        }

        private void txtDepartment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'DEPARTMENT'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtDepartment.Text = lookup.m_values[1].ToString();
                    txtDepartment.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                switch (btnAdd.Tag.ToString())
                {
                    case "&Add":

                        btnAdd.Tag = "&Save";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.save1;
                        btnExit.Tag = "&Cancel";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

                        gen.EDControls(this, true);


                        EnableButtons(false);
                        btnAdd.Enabled = true;
                        btnExit.Enabled = true;


                        loadDefultValues();



                        break;
                    case "&Save":
                        if (Validation())
                        {
                            autoCode = true;
                            if (txtStaffID.Text == string.Empty)
                            { auto();
                            }
                            //SqlTransaction sqlTran = cPublic.Connection.BeginTransaction();

                            // Enlist a command in the current transaction.


                            try
                            {
                                SqlCommand cmd = new SqlCommand("SP_INSERT_EMPLOYEE" + cPublic.g_firmcode, cPublic.Connection);

                                //cmd.Transaction = sqlTran;

                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Connection = cPublic.Connection;


                                cmd.Parameters.Clear();


                                Items(cmd);



                                id = cmd.ExecuteScalar().ToString();

                                //sqlTran.Commit();


                                cmd = new SqlCommand("SP_INSERT_EmployeeDocument" + cPublic.g_firmcode, cPublic.Connection);
                                cmd.CommandType = CommandType.StoredProcedure;



                                cmd.Connection = cPublic.Connection;




                                foreach (ListViewItem item in lisList.Items)
                                {


                                    cmd.Parameters.Clear();


                                    FileStream fStream = File.OpenRead(item.SubItems[8].Text);

                                    byte[] contents = new byte[fStream.Length];

                                    fStream.Read(contents, 0, (int)fStream.Length);

                                    fStream.Close();


                                    cmd.Parameters.AddWithValue("@id", item.SubItems[0].Text);
                                    cmd.Parameters.AddWithValue("@staffid", id.ToString());
                                    cmd.Parameters.AddWithValue("@doctype", item.SubItems[1].Text);
                                    cmd.Parameters.AddWithValue("@name", item.SubItems[3].Text);
                                    cmd.Parameters.AddWithValue("@issuedate", item.SubItems[4].Text);

                                    cmd.Parameters.AddWithValue("@expirydate", item.SubItems[5].Text);
                                    cmd.Parameters.AddWithValue("@docno", item.SubItems[6].Text);
                                    cmd.Parameters.AddWithValue("@filename", item.SubItems[7].Text);
                                    cmd.Parameters.AddWithValue("@attachment", contents);

                                    cmd.Parameters.AddWithValue("@userid", gen.GetUserDet());
                                    cmd.Parameters.AddWithValue("@PlaceofIss", item.SubItems[13].Text);

                                    cmd.ExecuteNonQuery();

                                    if (item.SubItems[10].Text == "checked")
                                    {

                                        cmd = new SqlCommand("SP_INSERT_DETAILS" + cPublic.g_firmcode, cPublic.Connection);
                                        cmd.CommandType = CommandType.StoredProcedure;



                                        cmd.Connection = cPublic.Connection;


                                        cmd.Parameters.Clear();

                                        cmd.Parameters.AddWithValue("@id", "");
                                    
                                        cmd.Parameters.AddWithValue("@doctype", item.SubItems[1].Text);
                                        cmd.Parameters.AddWithValue("@name", item.SubItems[3].Text);
                                        cmd.Parameters.AddWithValue("@issuedate", item.SubItems[4].Text);

                                        cmd.Parameters.AddWithValue("@expirydate", item.SubItems[5].Text);
                                        cmd.Parameters.AddWithValue("@docno", item.SubItems[6].Text);
                                        cmd.Parameters.AddWithValue("@filename", item.SubItems[7].Text);
                                        cmd.Parameters.AddWithValue("@attachment", contents);

                                        cmd.Parameters.AddWithValue("@userid", gen.GetUserDet());
                                        cmd.Parameters.AddWithValue("@PlaceofIss", item.SubItems[13].Text);

                                        cmd.ExecuteNonQuery();
                                    }
                                }


                                //if (btnEdit.Tag == "&Update")
                                //{ cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(txtCurStock.Text) - opQty + Convert.ToDecimal(txtOPStock.Text)); }
                                //else { cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(txtOPStock.Text.Trim())); }


                                sqlQuery = "select  a.id,a.[DateOfJoining],a.[DateOfArrival],a.[StaffID],a.[StaffName],f.Code GradeCode, f.Details GradeDetails,b.Code DepCode, b.Details DepDetails, d.Code QlfCode, d.Details QlfDetails," +
                                            "c.Code DesCode, c.Details DesDetails,a.[basic],a.[allowance],a.[DrivingLicence],a.[DrivingLicenceNo],a.[PrevEmployer], a.YearOfExperience,a.PhoneNo,a.MobileNo, a.Email,a.DOB,a.Gender,a.[Marital]," +
                                            "a.[Spouse],e.Code NatCode, e.Details NatDetails,a.[Religion],a.[Dependent],a.FatherName,a.[MotherName],a.[NName],a.[NRelation],a.[NPhoneNo],a.[NMobileNo]," +
                                            "a.[NEmail],a.NPermanentAddress1,a.NPermanentAddress2,a.NPermanentAddress3,a.NPermanentAddress4,a.NTemporaryAddress1,a.NTemporaryAddress2,a.NTemporaryAddress3,a.NTemporaryAddress4," +
                                            "a.[DName],a.[DRelation],a.[DPhoneNo],a.[DMobileNo],a.[DEmail],a.DPermanentAddress1,a.DPermanentAddress2,a.DPermanentAddress3,a.DPermanentAddress4," +
                                            "a.DTemporaryAddress1,a.DTemporaryAddress2,a.DTemporaryAddress3,a.DTemporaryAddress4, " +
                                            "a.Picture from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code " +
                                            "join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code join LOOKUP  f on a.[grade]= f.code  where a.id = @id";
                                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                                cmd.Parameters.AddWithValue("@id", id.Trim());
                                LoadDetails(cmd);
                                autoCode = false;
                                lblItemCount.Text = "No of Items : " + getItemCount().ToString();
                                MessageBox.Show("New Item Saved Successfully......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                EnableButtons(true);

                                autoCode = false;
                                btnAdd.Tag = "&Add";
                                btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                                btnExit.Tag = "E&xit";
                                btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                            }
                            catch (Exception ex)
                            {
                              
                                    //sqlTran.Rollback();
                           
                                    MessageBox.Show(ex.Message);

                               
                            }


                        }
                        break;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DocumentItems(SqlCommand cmd)
        {
            cmd.Parameters.Clear();


            FileStream fStream = File.OpenRead(txtAttachment.Text);

            byte[] contents = new byte[fStream.Length];

            fStream.Read(contents, 0, (int)fStream.Length);

            fStream.Close();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id.Trim());
            cmd.Parameters.AddWithValue("@name", txtDocumentName.Text.Trim());
            cmd.Parameters.AddWithValue("@issuedate", Convert.ToDateTime(dtpIssue.Value));

            cmd.Parameters.AddWithValue("@expirydate", Convert.ToDateTime(dtpExpiry.Value));
            cmd.Parameters.AddWithValue("@docno", txtDocumentNo.Text.Trim());
            cmd.Parameters.AddWithValue("@attachment", contents);
            cmd.Parameters.AddWithValue("@doctype", txtDocumentType.Tag);
            cmd.Parameters.AddWithValue("@userid", gen.GetUserDet());


        }

        private void Browse_Click(object sender, EventArgs e)
        {
            var _with1 = openFileDialog1;

            _with1.Filter = ("Images |*.png; *.bmp; *.jpg;*.jpeg; *.gif; *.ico");
            _with1.FilterIndex = 4;

            //Clear the file name
            openFileDialog1.FileName = "";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
            }
        }

        public Boolean UpdateItem()
        {
            //SqlTransaction sqlTran = cPublic.Connection.BeginTransaction();

            // Enlist a command in the current transaction.
            
            //cmd.Transaction = sqlTran;

            Boolean status = true;

            try
            {
                
                cmd.Connection = cPublic.Connection;


                cmd.Parameters.Clear();
                cmd.CommandText = "SP_UPDATE_EMPLOYEE" + cPublic.g_firmcode;

                Items(cmd);

                cmd.ExecuteNonQuery();

                //sqlTran.Commit();


                foreach (ListViewItem item in lisList.Items)
                {
                    if (item.SubItems[9].Text == "1")
                    {
                        cmd = new SqlCommand("SP_INSERT_EmployeeDocument" + cPublic.g_firmcode, cPublic.Connection);
                        cmd.CommandType = CommandType.StoredProcedure;



                        cmd.Connection = cPublic.Connection;

                        cmd.Parameters.Clear();


                        FileStream fStream = File.OpenRead(item.SubItems[8].Text);

                        byte[] contents = new byte[fStream.Length];

                        fStream.Read(contents, 0, (int)fStream.Length);

                        fStream.Close();


                        cmd.Parameters.AddWithValue("@id", item.SubItems[0].Text);
                        cmd.Parameters.AddWithValue("@staffid", id.ToString());
                        cmd.Parameters.AddWithValue("@doctype", item.SubItems[1].Text);
                        cmd.Parameters.AddWithValue("@name", item.SubItems[3].Text);
                        cmd.Parameters.AddWithValue("@issuedate", item.SubItems[4].Text);

                        cmd.Parameters.AddWithValue("@expirydate", item.SubItems[5].Text);
                        cmd.Parameters.AddWithValue("@docno", item.SubItems[6].Text);
                        cmd.Parameters.AddWithValue("@filename", item.SubItems[7].Text);
                        cmd.Parameters.AddWithValue("@attachment", contents);

                        cmd.Parameters.AddWithValue("@userid", gen.GetUserDet());
                        cmd.Parameters.AddWithValue("@PlaceofIss", txtPlace.Text.Trim());

                        cmd.ExecuteNonQuery();

                        if (item.SubItems[10].Text == "Checked")
                        {

                            cmd = new SqlCommand("SP_INSERT_DETAILS" + cPublic.g_firmcode, cPublic.Connection);
                            cmd.CommandType = CommandType.StoredProcedure;



                            cmd.Connection = cPublic.Connection;


                            cmd.Parameters.Clear();


                            cmd.Parameters.Clear();

                            ucRegister docreg = new ucRegister();

                            docreg.CopyFile(item.SubItems[8].Text);

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@id", "");
                            cmd.Parameters.AddWithValue("@name", item.SubItems[3].Text);
                            cmd.Parameters.AddWithValue("@issuedate", item.SubItems[4].Text);
                            
                            cmd.Parameters.AddWithValue("@expirydate", item.SubItems[5].Text);
                            cmd.Parameters.AddWithValue("@docno", item.SubItems[6].Text);
                            cmd.Parameters.AddWithValue("@attachment", docreg.destFile);
                            cmd.Parameters.AddWithValue("@doctype", item.SubItems[1].Text);
                            cmd.Parameters.AddWithValue("@userid", gen.GetUserDet());
                            cmd.Parameters.AddWithValue("@PlaceofIss", item.SubItems[13].Text);

                            if (!string.IsNullOrEmpty(item.SubItems[11].Text))
                            {
                                email = item.SubItems[11].Text;
                            }
                            if (!string.IsNullOrEmpty(item.SubItems[12].Text))
                            {
                                email = email + ";" + item.SubItems[12].Text;
                            }

                            cmd.Parameters.AddWithValue("@email", email);

                            cmd.ExecuteNonQuery();
                        }
                    }
                }


                sqlQuery = "select  a.id,a.[DateOfJoining],a.[DateOfArrival],a.[StaffID],a.[StaffName],f.Code GradeCode, f.Details GradeDetails,b.Code DepCode, b.Details DepDetails, d.Code QlfCode, d.Details QlfDetails," +
                        "c.Code DesCode, c.Details DesDetails,a.[basic],a.[allowance],a.[DrivingLicence],a.[DrivingLicenceNo],a.[PrevEmployer], a.YearOfExperience,a.PhoneNo,a.MobileNo, a.Email,a.DOB,a.Gender,a.[Marital]," +
                        "a.[Spouse],e.Code NatCode, e.Details NatDetails,a.[Religion],a.[Dependent],a.FatherName,a.[MotherName],a.[NName],a.[NRelation],a.[NPhoneNo],a.[NMobileNo]," +
                        "a.[NEmail],a.NPermanentAddress1,a.NPermanentAddress2,a.NPermanentAddress3,a.NPermanentAddress4,a.NTemporaryAddress1,a.NTemporaryAddress2,a.NTemporaryAddress3,a.NTemporaryAddress4," +
                        "a.[DName],a.[DRelation],a.[DPhoneNo],a.[DMobileNo],a.[DEmail],a.DPermanentAddress1,a.DPermanentAddress2,a.DPermanentAddress3,a.DPermanentAddress4," +
                        "a.DTemporaryAddress1,a.DTemporaryAddress2,a.DTemporaryAddress3,a.DTemporaryAddress4, " +
                        "a.Picture from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code " +
                        "join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code join LOOKUP  f on a.[grade]= f.code where a.id = @id";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@id", id.Trim());
                LoadDetails(cmd);
            }
            catch (Exception ex)
            {
                //sqlTran.Rollback();
                MessageBox.Show(ex.Message);
                status = false;
            }
            return status;
        }

        private void DocItems(SqlCommand cmd)
        {
            cmd.Parameters.Clear();

            ucRegister docreg = new ucRegister();

            docreg.CopyFile(txtAttachment.Text);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id.Trim());
            cmd.Parameters.AddWithValue("@name", txtDocumentName.Text.Trim());
            cmd.Parameters.AddWithValue("@issuedate", Convert.ToDateTime(dtpIssue.Value));

            cmd.Parameters.AddWithValue("@expirydate", Convert.ToDateTime(dtpExpiry.Value));
            cmd.Parameters.AddWithValue("@docno", txtDocumentNo.Text.Trim());
            cmd.Parameters.AddWithValue("@PlaceofIss", txtPlace.Text.Trim());
            cmd.Parameters.AddWithValue("@attachment", docreg.destFile);
            cmd.Parameters.AddWithValue("@doctype", txtDocumentType.Tag);
            cmd.Parameters.AddWithValue("@userid", gen.GetUserDet());


            if (!string.IsNullOrEmpty(txtEmail1.Text.Trim()))
            {
                email = txtEmail1.Text.Trim();
            }
            if (!string.IsNullOrEmpty(txtEmail2.Text.Trim()))
            {
                email = email + ";" + txtEmail2.Text.Trim();
            }
         
            cmd.Parameters.AddWithValue("@email", email);

           
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            switch (btnEdit.Tag.ToString())
            {
                case "&Edit":
                    if (id.Trim() != string.Empty && txtStaffID.Text.Trim() == string.Empty)
                    { MessageBox.Show("You are not Allowed to Edit this Item..."); return; }
                    btnEdit.Tag = "&Update";
                    btnEdit.BackgroundImage = global::Xpire.Properties.Resources.save1;
                    btnExit.Tag = "&Cancel";
                    btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

                    gen.EDControls(this, true);

                    EnableButtons(false);
                    btnEdit.Enabled = true;
                    btnExit.Enabled = true;
                    btnCancelEmp.Enabled = true;

                    break;
                case "&Update":
                    if (Validation())
                    {
                        if (UpdateItem())
                        {
                            MessageBox.Show("Item Details Updated Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            EnableButtons(true);

                            btnEdit.Tag = "&Edit";
                            btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;


                        }
                        else { MessageBox.Show("Invalid Data.. Please Check...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    break;

            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchClick = true;
            //btnExit.Tag = "&Cancel";
            //btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
            gen.ClearFieldsInMe(this);
            txtStaffID.Enabled = true;
            txtStaffID.Focus();
            //btnAdd.Enabled = false;
            //btnEdit.Enabled = false;
            //btnDelete.Enabled = false;
            //btnNext.Enabled = false;
            //btnPrevious.Enabled = false;
        }

        public Boolean DocValidation()
        {
            Boolean status = true;
            if (txtDocumentType.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Document Type cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDocumentType.Focus();
            }
            else if (txtDocumentName.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Document Name cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDocumentName.Focus();
            }
            else if (DocNameExists("EmployeeDocument" + cPublic.g_firmcode, txtDocumentName.Text.Trim()))
            {
                status = false;
                MessageBox.Show("Document Name Exists.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDocumentName.Focus();
            }
            else if (txtDocumentNo.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Document Number cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDocumentNo.Focus();
            }
            else if(txtPlace.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Place of Issue cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPlace.Focus();
            }
            else if (txtAttachment.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Attachment cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtAttachment.Focus();
            }

            else if (chkRenewal.Checked)
            {
                if (txtEmail1.Text.Trim().Equals(string.Empty))
                {
                    status = false;
                    MessageBox.Show("Email 1 cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail1.Focus();
                }
            }
            if (!(txtEmail1.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtEmail1.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail1.Focus();
                }
            }
            return status;
        }

        int shown = 0;

        public string StaffID { get; private set; }

        private void btnAddDoc_Click(object sender, EventArgs e)
        {


            if (DocValidation())
            {
                if (shown == 0)
                {
                    if (chkRenewal.Checked == false)
                    {
                        DialogResult dialogResult = MessageBox.Show("Do you want to transfer thie item to Renewal", "Renewal", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            chkRenewal.Checked = true;
                            shown = 1;
                            return;

                        }
                    }
                }

                //else if (chkRenewal.Checked)
                //{
                //    if (txtEmail1.Text.Trim().Equals(string.Empty))
                //    {

                //        MessageBox.Show("Email 1 cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        txtEmail1.Focus();
                //    }
                //}
                //else if (!(txtEmail1.Text.Trim().Equals(string.Empty)))
                //{
                //    if (!varification(txtEmail1.Text.Trim()))
                //    {

                //        MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        txtEmail1.Focus();
                //    }
                //}
                //else
                //{
                ListViewItem item1 = new ListViewItem();
                item1.SubItems[0].Text = "";
                item1.SubItems.Add(txtDocumentType.Tag.ToString());
                item1.SubItems.Add(txtDocumentType.Text);
                item1.SubItems.Add(txtDocumentName.Text);

                item1.SubItems.Add(dtpIssue.Value.ToString("yyyy/MM/dd"));
                item1.SubItems.Add(dtpExpiry.Value.ToString("yyyy/MM/dd"));
                item1.SubItems.Add(txtDocumentNo.Text);
                item1.SubItems.Add(filename);
                item1.SubItems.Add(txtAttachment.Text);
                item1.SubItems.Add("1");
                item1.SubItems.Add(chkRenewal.CheckState.ToString());
                item1.SubItems.Add(txtEmail1.Text);
                item1.SubItems.Add(txtEmail2.Text);
                item1.SubItems.Add(txtPlace.Text);
                this.lisList.Items.Add(item1);

                txtDocumentNo.Text = "";
                txtDocumentType.Text = "";
                txtDocumentName.Text = "";
                txtAttachment.Text = "";
                shown = 0;
                chkRenewal.Checked = false;
                //}
            }
        }



        private void txtDocumentType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'DOCUMENT TYPE'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtDocumentType.Text = lookup.m_values[1].ToString();
                    txtDocumentType.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.CheckFileExists = true;
            openFileDialog.AddExtension = true;
            //openFileDialog.Multiselect = true;
            openFileDialog.Filter = "All files (*.*)|*.*|PDF files (*.pdf)|*.pdf|JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filename = openFileDialog.SafeFileName;

                txtAttachment.Text = openFileDialog.FileName;
            }
        }

        private void txtQualifications_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'QUALIFICATION'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtQualifications.Text = lookup.m_values[1].ToString();
                    txtQualifications.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void txtDesignation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'DESIGNATION'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtDesignation.Text = lookup.m_values[1].ToString();
                    txtDesignation.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void txtNationality_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'NATIONALITY'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "0,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtNationality.Text = lookup.m_values[1].ToString();
                    txtNationality.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void txtReligion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'RELIGION'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "0,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtReligion.Text = lookup.m_values[1].ToString();
                    txtReligion.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void txtYOP_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(Char.IsNumber(e.KeyChar) || e.KeyChar == 8);
        }

        private void txtPhoneNo_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(Char.IsNumber(e.KeyChar) || e.KeyChar == 8);
        }

        private void txtDependant_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(Char.IsNumber(e.KeyChar) || e.KeyChar == 8);
        }

        private void lisList_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            //lisList.FullRowSelect = true;

            //if (lisList.Items.Count > 0)
            //{
            //    if (lisList.SelectedItems.Count > 0)
            //    {
            //        ListViewItem item = lisList.SelectedItems[0];

            //        filename = item.SubItems[7].Text;


            //        string temfilename = Application.StartupPath + @"\" + @item.SubItems[7].Text;


            //        if (File.Exists(temfilename))
            //        {

            //            System.Diagnostics.Process.Start(temfilename);

            //        }

            //        else
            //        {

            //            System.Diagnostics.Process.Start(item.SubItems[8].Text);
            //        }

            //    }
            //}
      

    }

        public static byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }

        private void btnView_Click(object sender, EventArgs e)
        {




            //System.Diagnostics.Process.Start(txtAttachment.Text);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            switch (btnExit.Tag.ToString())
            {
                case "E&xit":
                    this.Dispose();
                    break;
                case "&Cancel":
                    if ((MessageBox.Show("Are You Sure to Cancel...?  ", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question)).Equals(DialogResult.Yes))
                    {
                        //if (btnAdd.Tag.Equals("&Save"))
                        //{
                        //    SqlCommand cmd = new SqlCommand("SP_DELETE_STOCKMST" + cPublic.g_firmcode, cPublic.Connection);
                        //    cmd.CommandType = CommandType.StoredProcedure;
                        //    cmd.Parameters.AddWithValue("@itemcode", id.Trim());
                        //    cmd.ExecuteNonQuery();
                        //}

                        gen.ClearFieldsInMe(this);
                        EnableButtons(true);
                        btnCancelEmp.Enabled = false;
                        btnAdd.Tag = "&Add";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                        btnEdit.Tag = "&Edit";
                        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        loadDefultValues();
                        loadFirstRecord();
                        searchClick = false;
                        autoCode = false;

                    }
                    EscKey = false;

                    break;
            }
        }

        private void txtStaffID_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchClick)
            {
                if (e.KeyCode.Equals(Keys.F5))
                {
                    string fields = string.Empty;
                    string dispName = string.Empty;
                    string width = string.Empty;
                    //if (cPublic.LookupDefault == "Itemcode")
                    //{
                    fields = "StaffName,StaffID,id";
                    dispName = "Name,Staff ID,id";
                    width = "150,100,0";

                    //}
                    //else
                    //{
                    //    fields = "itemname,itemcode,qty,Rate,taxper,cessper";
                    //    dispName = "Item Name, Item Code, Qty, Rate, Tax %, Cess %";
                    //    width = "350,80,80,100,50,60";
                    //}
                    frmlookup lookup = new frmlookup();
                    lookup.m_table = "Employee" + cPublic.g_firmcode;
                    lookup.m_fields = fields;
                    lookup.m_dispname = dispName;
                    lookup.m_fldwidth = width;
                    lookup.q_type = "Cancel";
                    lookup.m_con = cPublic.Connection;
                    lookup.ShowDialog();
                    if (lookup.m_values.Count > 0)
                    {
                        //if (cPublic.LookupDefault == "Itemcode")
                        //{
                        id = lookup.m_values[2].ToString();
                        txtStaffID.Text = lookup.m_values[1].ToString();
                        txtStaffName.Text = lookup.m_values[0].ToString();
                        //}
                        //else
                        //{
                        //    id = lookup.m_values[1].ToString();
                        //    txtItmName.Text = lookup.m_values[0].ToString();
                        //}
                        sqlQuery = "select  a.id,a.[DateOfJoining],a.[DateOfArrival],a.[StaffID],a.[StaffName],f.Code GradeCode, f.Details GradeDetails,b.Code DepCode, b.Details DepDetails, d.Code QlfCode, d.Details QlfDetails," +
                        "c.Code DesCode, c.Details DesDetails,a.[basic],a.[allowance],a.[DrivingLicence],a.[DrivingLicenceNo],a.[PrevEmployer], a.YearOfExperience,a.PhoneNo,a.MobileNo, a.Email,a.DOB,a.Gender,a.[Marital]," +
                        "a.[Spouse],e.Code NatCode, e.Details NatDetails,a.[Religion],a.[Dependent],a.FatherName,a.[MotherName],a.[NName],a.[NRelation],a.[NPhoneNo],a.[NMobileNo]," +
                        "a.[NEmail],a.NPermanentAddress1,a.NPermanentAddress2,a.NPermanentAddress3,a.NPermanentAddress4,a.NTemporaryAddress1,a.NTemporaryAddress2,a.NTemporaryAddress3,a.NTemporaryAddress4," +
                        "a.[DName],a.[DRelation],a.[DPhoneNo],a.[DMobileNo],a.[DEmail],a.DPermanentAddress1,a.DPermanentAddress2,a.DPermanentAddress3,a.DPermanentAddress4," +
                        "a.DTemporaryAddress1,a.DTemporaryAddress2,a.DTemporaryAddress3,a.DTemporaryAddress4, " +
                        "a.Picture,a.remarks from [dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code " +
                        "join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code join LOOKUP  f on a.[grade]= f.code  where a.id = @id";
                        SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                        cmd.Parameters.AddWithValue("@id", id.Trim());
                        LoadDetails(cmd);
                        //btnAdd.Enabled = true;
                        //btnEdit.Enabled = true;
                        //btnDelete.Enabled = true;
                        //btnNext.Enabled = true;
                        //btnPrevious.Enabled = true;
                        //btnExit.Tag = "E&xit";
                        //btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        searchClick = false;
                    }
                }

            }
        }

        private void cmbDrvLicense_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void chkRenewal_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRenewal.Checked)
            {
                lblEmail1.Visible = true;
                lblEmail2.Visible = true;
                txtEmail1.Visible = true;
                txtEmail2.Visible = true;
            }

            else
            {
                lblEmail1.Visible = false;
                lblEmail2.Visible = false;
                txtEmail1.Visible = false;
                txtEmail2.Visible = false;
            }
        }

        private void txtEmail1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            string PS = string.Empty;

            PS = "PRINT";
            cPublic.PrintMode = "WINDOWS";

            if (PS == "PRINT")
            {
                if (cPublic.PrintMode == "WINDOWS")
                {
                    new cWindowsPrint("[dbo].[Employee" + cPublic.g_firmcode + "]", "", id, "Employee");
                }
            }
        }

        private void btnCancelEmp_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty( txtRemarks.Text) )
            { 
                DialogResult dialogResult = MessageBox.Show("Do you want to cancel this employee", "Cancellation", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    dialogResult = MessageBox.Show("Do you want to transfer thie employee to new company", "Transfer Employee", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {

                        frmCompanySelection frmCompanySelection = new frmCompanySelection();
                        frmCompanySelection.ShowDialog();


                        SqlCommand cmd = new SqlCommand("SP_INSERT_EMPLOYEE" + cPublic.g_firmcode, cPublic.Connection);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Connection = cPublic.Connection;

                        cmd.Parameters.Clear();

                        TransferItems(cmd, frmCompanySelection.Abbr);

                        string cancelid = cmd.ExecuteScalar().ToString();

                        string todatab = cPublic.g_PrjCode + frmCompanySelection.G_compcode;

                        string fromdatab = cPublic.g_PrjCode + cPublic.g_compcode;

                        InsertLeave(todatab, fromdatab);

                        cmd = new SqlCommand(sqlQuery, cPublic.Connection);

                        cmd.ExecuteNonQuery();

                        con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + cPublic.g_compcode, cPublic.Sqlservername);

                        sqlQuery = "update [dbo].[Employee" + cPublic.g_firmcode + "] set cancel=1 where id = @id";
                        cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                        cmd.Parameters.AddWithValue("@id", id.Trim());
                        cmd.ExecuteNonQuery();

                        loadFirstRecord();

                        MessageBox.Show("Employee Transfered Successfully......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        btnEdit.Tag = "&Edit";
                        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                    }
                    else
                    {
                        sqlQuery = "update [dbo].[Employee" + cPublic.g_firmcode + "] set cancel=1 where id = @id";
                        cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                        cmd.Parameters.AddWithValue("@id", id.Trim());
                        cmd.ExecuteNonQuery();

                        loadFirstRecord();

                        MessageBox.Show("Employee Cancelled Successfully......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnEdit.Tag = "&Edit";
                        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                    }
                }
             
            }
            else
                {
                    MessageBox.Show("Enter Remarks", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRemarks.Focus();
                }
        }

        private void InsertLeave(string todatab, string fromdatab)
        {
            sqlQuery = "DECLARE  @applicationcnt int, @approvalcnt int, @rejoiningcnt int,@staffid [varchar](max) \n"

            + "select @staffid = '" + txtStaffID.Text + "' \n"

            + "SELECT @applicationcnt = (IsNull((select MAX(id) from " + todatab + ".[dbo].[LeaveApplication0001]),0)) \n"

            + "SELECT @approvalcnt = (IsNull((select MAX(id) from " + todatab + ".[dbo].[LeaveApproval0001]),0)) \n"

            + "SELECT @rejoiningcnt = (IsNull((select MAX(id) from " + todatab + ".[dbo].[REJOINING0001]),0)) \n"


            + "SELECT @applicationcnt+ROW_NUMBER() OVER(ORDER BY id asc) as id,id as oldid INTO #newapplicationid from " + fromdatab + " .[dbo].[LeaveApplication0001]  where staffid=@staffid \n"


            + "SELECT @applicationcnt+ROW_NUMBER() OVER(ORDER BY id asc) as id,id as oldid,[Staffid],[DOA],[LeaveType],[From],[To],[COV]\n"
                    + "INTO #newapplication from " + fromdatab + ".[dbo].[LeaveApplication0001]  where staffid=@staffid\n"


            + "SELECT @approvalcnt+ROW_NUMBER() OVER(ORDER BY id asc) as id,[applicationid],id oldapprovalid, [From],[To],[balleave] INTO #oldapproval from " + fromdatab + ".[dbo].[LeaveApproval0001]  where applicationid in\n"
             + "(select id from " + fromdatab + ".[dbo].[LeaveApplication0001] where staffid = @staffid)\n"

            + "SELECT @rejoiningcnt+ROW_NUMBER() OVER(ORDER BY id asc) as id,approvalid,DOR,DOARR,DOJ,balleave INTO #oldrejoining from " + fromdatab + " .[dbo].[REJOINING0001]  where [approvalid] in (select applicationid from #oldapproval)\n"


            + "select* into #newapproval from #oldapproval\n"


            + "select* into #newrejoining from #oldrejoining\n"


            + "update a set a.applicationid=b.id from #newapproval a join #newapplicationid b on a.applicationid=b.oldid\n"


            + "update a set a.approvalid= b.id from #newrejoining a join #newapproval b on a.approvalid=b.oldapprovalid\n"

            + "INSERT INTO " + todatab + ".dbo.LeaveApplication0001 ( \n"
            + "[id],        \n"
            + "[StaffID],   \n"
            + "[DOA],       \n"
            + "[LeaveType], \n"
            + "[From],      \n"
            + "[To],        \n"
            + "[COV])   select id, staffid, DOA, LeaveType,[From],[to],[COV] from #newapplication\n"


            + "INSERT INTO " + todatab + ".dbo.LeaveApproval0001 ( \n"
            + "[id],               \n"
            + "[applicationid],    \n"
            + "[From],             \n"
            + "[To],               \n"
            + "[balleave]) select id, applicationid,[From],[to], balleave from #newapproval\n"

            + "INSERT INTO " + todatab + ".dbo.REJOINING0001 ( \n"
            + "[id],             \n"
            + "[approvalid],     \n"
            + "[DOR],            \n"
            + "[DOJ],            \n"
            + "[DOARR],          \n"
            + "[balleave]) select id, approvalid, DOR, DOARR, DOJ, balleave from #newrejoining";
        }

        private void loadDefultValues()
        {

            id = string.Empty;
            dtpDOJ.Value = System.DateTime.Now;
            dtpDOA.Value = System.DateTime.Now;
            txtStaffID.Text = string.Empty;
            txtStaffName.Text = string.Empty;
            txtGrade.Text = string.Empty;
            txtDepartment.Text = string.Empty;
            txtDesignation.Text = string.Empty;
            txtQualifications.Text = string.Empty;
            txtBasic.Text = "0";
            txtAllowance.Text = "0";
            cmbDrvLicense.Text = string.Empty;
            txtLicenseNo.Text = string.Empty;
            txtPrevEmp.Text = string.Empty;
            txtYOP.Text = "0";
            txtPhoneNo.Text = string.Empty;
            txtMobileNo.Text = string.Empty;
            txtEmail.Text = string.Empty;


            dtpDOB.Value = System.DateTime.Now;
            cmbGender.Text = string.Empty;
            cmbMarital.Text = string.Empty;
            txtSpouse.Text = string.Empty;
            txtNationality.Text = string.Empty;
            txtReligion.Text = string.Empty;
            txtDependant.Text = "0";
            txtFatherName.Text = string.Empty;
            txtMotherName.Text = string.Empty;

            txtNName.Text = string.Empty;
            txtNRelation.Text = string.Empty;
            txtNPhoneNo.Text = string.Empty;
            txtNMobileNo.Text = string.Empty;
            txtNEmail.Text = string.Empty;
            txtNPAddress1.Text = string.Empty;
            txtNPAddress2.Text = string.Empty;
            txtNPAddress3.Text = string.Empty;
            txtNPAddress4.Text = string.Empty;
            txtNTAddress1.Text = string.Empty;
            txtNTAddress2.Text = string.Empty;
            txtNTAddress3.Text = string.Empty;
            txtNTAddress4.Text = string.Empty;

            txtDName.Text = string.Empty;
            txtDRelation.Text = string.Empty;
            txtDPhoneNo.Text = string.Empty;
            txtDMobileNo.Text = string.Empty;
            txtDEmail.Text = string.Empty;
            txtDPAddress1.Text = string.Empty;
            txtDPAddress2.Text = string.Empty;
            txtDPAddress3.Text = string.Empty;
            txtDPAddress4.Text = string.Empty;
            txtDTAddress1.Text = string.Empty;
            txtDTAddress2.Text = string.Empty;
            txtDTAddress3.Text = string.Empty;
            txtDTAddress4.Text = string.Empty;

            pictureBox1.Image = null;

            lisList.Items.Clear();
        }

        private void txtGrade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'GRADE'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtGrade.Text = lookup.m_values[1].ToString();
                    txtGrade.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void txtEmail1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'EMAIL'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtEmail1.Text = lookup.m_values[1].ToString();
                    txtEmail1.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void txtEmail2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'EMAIL'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtEmail2.Text = lookup.m_values[1].ToString();
                    txtEmail2.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void cmbMarital_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbMarital.Text=="Single")
            {
                panel1.Visible = false;
            }
            else
                panel1.Visible = true;
        }

        private void cmbDrvLicense_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDrvLicense.Text == "No")
            {
                panel2.Visible = false;
            }
            else
                panel2.Visible = true;
        }

        private void lisList_DoubleClick(object sender, EventArgs e)
        {
            lisList.FullRowSelect = true;

            //{
            if (lisList.Items.Count > 0)
            {
                if (lisList.SelectedItems.Count > 0)
                {
                    ListViewItem item = lisList.SelectedItems[0];

                    filename = item.SubItems[7].Text;


                    string temfilename = Application.StartupPath + @"\" + @item.SubItems[7].Text;


                    if (File.Exists(temfilename))
                    {

                        System.Diagnostics.Process.Start(temfilename);

                    }

                    else
                    {

                        System.Diagnostics.Process.Start(item.SubItems[8].Text);
                    }

                }
            }
        }

        private void lisList_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.Delete == e.KeyCode)
            {
                if (btnAdd.Tag.ToString() != "&Add" || btnEdit.Tag.ToString() != "&Edit")
                {

                    foreach (ListViewItem listViewItem in ((ListView)sender).SelectedItems)
                    {
                        if (MessageBox.Show("Are you sure to Delete this ITEM...", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(DialogResult.Yes))
                        {

                            if (listViewItem.Text != string.Empty)
                            {
                                if (isItmPossibleForDelete(listViewItem.SubItems[1].Text, listViewItem.SubItems[3].Text, listViewItem.SubItems[6].Text))
                                {
                                    SqlCommand cmd = new SqlCommand("SP_DELETE_EmployeeDocument" + cPublic.g_firmcode, cPublic.Connection);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@id", listViewItem.Text);
                                    cmd.ExecuteNonQuery();
                                    listViewItem.Remove();
                                }
                                else
                                {
                                    MessageBox.Show("Item present in renewal, Cannot delete item", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                listViewItem.Remove();
                            }


                        }
                    }
                }
            }

        }

        private Boolean DocNameExists(string tableName, string docname)
        {
            Boolean status = false;
            sqlQuery = "select id from " + tableName + " where name=@docname ";
            SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@docname", docname);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
                status = true;
            dr.Close();
            return status;
        }

        private Boolean isItmPossibleForDelete(string doctype,string docname,string docno)
        {
            Boolean status = true;

            string itmCode = id.Trim();
            if (isItemPresent("DETAILS" + cPublic.g_firmcode, doctype, docname, docno))
            { status = false; }
           
            return status;
        }

        private Boolean isItemPresent(string tableName, string doctype, string docname, string docno)
        {
            Boolean status = false;
            sqlQuery = "select id from " + tableName + " where DocumentType = @doctype and name=@docname and DocumentNo=@docno  ";
            SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@doctype", doctype);
            cmd.Parameters.AddWithValue("@docname", docname);
            cmd.Parameters.AddWithValue("@docno", docno);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
                status = true;
            dr.Close();
            return status;
        }
        private void Items(SqlCommand cmd)
        {

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id.Trim());
            cmd.Parameters.AddWithValue("@dateofjoining", Convert.ToDateTime(dtpDOJ.Value));
            cmd.Parameters.AddWithValue("@dateofarrival", Convert.ToDateTime(dtpDOA.Value));
            cmd.Parameters.AddWithValue("@staffid", txtStaffID.Text.Trim());
            cmd.Parameters.AddWithValue("@staffname", txtStaffName.Text.Trim());
            cmd.Parameters.AddWithValue("@grade", txtGrade.Tag);
            cmd.Parameters.AddWithValue("@department", txtDepartment.Tag);
            cmd.Parameters.AddWithValue("@qualification", txtQualifications.Tag);
            cmd.Parameters.AddWithValue("@designation", txtDesignation.Tag);
            cmd.Parameters.AddWithValue("@basic", Convert.ToInt32(txtBasic.Text.Trim()));
            cmd.Parameters.AddWithValue("@allowance", Convert.ToInt32(txtAllowance.Text.Trim()));
            cmd.Parameters.AddWithValue("@drivinglicense", cmbDrvLicense.Text);
            cmd.Parameters.AddWithValue("@drivinglicenseno", txtLicenseNo.Text);
            cmd.Parameters.AddWithValue("@prevemp", txtPrevEmp.Text);
            cmd.Parameters.AddWithValue("@yearofexperience", Convert.ToInt32(txtYOP.Text.Trim()));
            cmd.Parameters.AddWithValue("@phoneno", txtPhoneNo.Text.Trim());
            cmd.Parameters.AddWithValue("@mobileno", txtMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@dob", Convert.ToDateTime(dtpDOB.Value));
            cmd.Parameters.AddWithValue("@gender", cmbGender.Text.Trim());
            cmd.Parameters.AddWithValue("@marital", cmbMarital.Text.Trim());
            cmd.Parameters.AddWithValue("@spouse", txtSpouse.Text.Trim());
            cmd.Parameters.AddWithValue("@nationality", txtNationality.Tag);
            cmd.Parameters.AddWithValue("@religion", txtReligion.Text.Trim());
            cmd.Parameters.AddWithValue("@dependent", Convert.ToInt32(txtDependant.Text.Trim()));
            cmd.Parameters.AddWithValue("@FatherName", txtFatherName.Text.Trim());
            cmd.Parameters.AddWithValue("@MotherName", txtMotherName.Text.Trim());
            cmd.Parameters.AddWithValue("@NName", txtNName.Text.Trim());
            cmd.Parameters.AddWithValue("@NRelation", txtNRelation.Text.Trim());
            cmd.Parameters.AddWithValue("@NPhoneNo", txtNPhoneNo.Text.Trim());
            cmd.Parameters.AddWithValue("@NMobileNo", txtNMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@NEmail", txtNEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@NPermanentAddress1", txtNPAddress1.Text.Trim());
            cmd.Parameters.AddWithValue("@NPermanentAddress2", txtNPAddress2.Text.Trim());
            cmd.Parameters.AddWithValue("@NPermanentAddress3", txtNPAddress3.Text.Trim());
            cmd.Parameters.AddWithValue("@NPermanentAddress4", txtNPAddress4.Text.Trim());
            cmd.Parameters.AddWithValue("@NTemporaryAddress1", txtNPAddress1.Text.Trim());
            cmd.Parameters.AddWithValue("@NTemporaryAddress2", txtNPAddress2.Text.Trim());
            cmd.Parameters.AddWithValue("@NTemporaryAddress3", txtNPAddress3.Text.Trim());
            cmd.Parameters.AddWithValue("@NTemporaryAddress4", txtNPAddress4.Text.Trim());
            cmd.Parameters.AddWithValue("@DName", txtDName.Text.Trim());
            cmd.Parameters.AddWithValue("@DRelation", txtDRelation.Text.Trim());
            cmd.Parameters.AddWithValue("@DPhoneNo", txtDPhoneNo.Text.Trim());
            cmd.Parameters.AddWithValue("@DMobileNo", txtDMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@DEmail", txtDEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@DPermanentAddress1", txtDPAddress1.Text.Trim());
            cmd.Parameters.AddWithValue("@DPermanentAddress2", txtDPAddress2.Text.Trim());
            cmd.Parameters.AddWithValue("@DPermanentAddress3", txtDPAddress3.Text.Trim());
            cmd.Parameters.AddWithValue("@DPermanentAddress4", txtDPAddress4.Text.Trim());
            cmd.Parameters.AddWithValue("@DTemporaryAddress1", txtDPAddress1.Text.Trim());
            cmd.Parameters.AddWithValue("@DTemporaryAddress2", txtDPAddress2.Text.Trim());
            cmd.Parameters.AddWithValue("@DTemporaryAddress3", txtDPAddress3.Text.Trim());
            cmd.Parameters.AddWithValue("@DTemporaryAddress4", txtDPAddress4.Text.Trim());
            cmd.Parameters.AddWithValue("@cancel", false);

            cmd.Parameters.AddWithValue("@abbr", cPublic.Abbr);
            MemoryStream ms = new MemoryStream();
            Bitmap bmpImage = new Bitmap(pictureBox1.Image);

            bmpImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            byte[] data = ms.GetBuffer();
            SqlParameter p = new SqlParameter("@Picture", SqlDbType.Image);
            p.Value = data;
            cmd.Parameters.Add(p);

          

        }

        private void TransferItems(SqlCommand cmd,string abbr)
        {

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", "");
            cmd.Parameters.AddWithValue("@dateofjoining", Convert.ToDateTime(dtpDOJ.Value));
            cmd.Parameters.AddWithValue("@dateofarrival", Convert.ToDateTime(dtpDOA.Value));
            cmd.Parameters.AddWithValue("@staffid", "");
            cmd.Parameters.AddWithValue("@staffname", txtStaffName.Text.Trim());
            cmd.Parameters.AddWithValue("@grade", txtGrade.Tag);
            cmd.Parameters.AddWithValue("@department", txtDepartment.Tag);
            cmd.Parameters.AddWithValue("@qualification", txtQualifications.Tag);
            cmd.Parameters.AddWithValue("@designation", txtDesignation.Tag);
            cmd.Parameters.AddWithValue("@basic", Convert.ToInt32(txtBasic.Text.Trim()));
            cmd.Parameters.AddWithValue("@allowance", Convert.ToInt32(txtAllowance.Text.Trim()));
            cmd.Parameters.AddWithValue("@drivinglicense", cmbDrvLicense.Text);
            cmd.Parameters.AddWithValue("@drivinglicenseno", txtLicenseNo.Text);
            cmd.Parameters.AddWithValue("@prevemp", txtPrevEmp.Text);
            cmd.Parameters.AddWithValue("@yearofexperience", Convert.ToInt32(txtYOP.Text.Trim()));
            cmd.Parameters.AddWithValue("@phoneno", txtPhoneNo.Text.Trim());
            cmd.Parameters.AddWithValue("@mobileno", txtMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@dob", Convert.ToDateTime(dtpDOB.Value));
            cmd.Parameters.AddWithValue("@gender", cmbGender.Text.Trim());
            cmd.Parameters.AddWithValue("@marital", cmbMarital.Text.Trim());
            cmd.Parameters.AddWithValue("@spouse", txtSpouse.Text.Trim());
            cmd.Parameters.AddWithValue("@nationality", txtNationality.Tag);
            cmd.Parameters.AddWithValue("@religion", txtReligion.Text.Trim());
            cmd.Parameters.AddWithValue("@dependent", Convert.ToInt32(txtDependant.Text.Trim()));
            cmd.Parameters.AddWithValue("@FatherName", txtFatherName.Text.Trim());
            cmd.Parameters.AddWithValue("@MotherName", txtMotherName.Text.Trim());
            cmd.Parameters.AddWithValue("@NName", txtNName.Text.Trim());
            cmd.Parameters.AddWithValue("@NRelation", txtNRelation.Text.Trim());
            cmd.Parameters.AddWithValue("@NPhoneNo", txtNPhoneNo.Text.Trim());
            cmd.Parameters.AddWithValue("@NMobileNo", txtNMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@NEmail", txtNEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@NPermanentAddress1", txtNPAddress1.Text.Trim());
            cmd.Parameters.AddWithValue("@NPermanentAddress2", txtNPAddress2.Text.Trim());
            cmd.Parameters.AddWithValue("@NPermanentAddress3", txtNPAddress3.Text.Trim());
            cmd.Parameters.AddWithValue("@NPermanentAddress4", txtNPAddress4.Text.Trim());
            cmd.Parameters.AddWithValue("@NTemporaryAddress1", txtNPAddress1.Text.Trim());
            cmd.Parameters.AddWithValue("@NTemporaryAddress2", txtNPAddress2.Text.Trim());
            cmd.Parameters.AddWithValue("@NTemporaryAddress3", txtNPAddress3.Text.Trim());
            cmd.Parameters.AddWithValue("@NTemporaryAddress4", txtNPAddress4.Text.Trim());
            cmd.Parameters.AddWithValue("@DName", txtDName.Text.Trim());
            cmd.Parameters.AddWithValue("@DRelation", txtDRelation.Text.Trim());
            cmd.Parameters.AddWithValue("@DPhoneNo", txtDPhoneNo.Text.Trim());
            cmd.Parameters.AddWithValue("@DMobileNo", txtDMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@DEmail", txtDEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@DPermanentAddress1", txtDPAddress1.Text.Trim());
            cmd.Parameters.AddWithValue("@DPermanentAddress2", txtDPAddress2.Text.Trim());
            cmd.Parameters.AddWithValue("@DPermanentAddress3", txtDPAddress3.Text.Trim());
            cmd.Parameters.AddWithValue("@DPermanentAddress4", txtDPAddress4.Text.Trim());
            cmd.Parameters.AddWithValue("@DTemporaryAddress1", txtDPAddress1.Text.Trim());
            cmd.Parameters.AddWithValue("@DTemporaryAddress2", txtDPAddress2.Text.Trim());
            cmd.Parameters.AddWithValue("@DTemporaryAddress3", txtDPAddress3.Text.Trim());
            cmd.Parameters.AddWithValue("@DTemporaryAddress4", txtDPAddress4.Text.Trim());
            cmd.Parameters.AddWithValue("@cancel", false);
            cmd.Parameters.AddWithValue("@remarks", txtRemarks.Text);
            cmd.Parameters.AddWithValue("@abbr", abbr);
            MemoryStream ms = new MemoryStream();
            Bitmap bmpImage = new Bitmap(pictureBox1.Image);

            bmpImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            byte[] data = ms.GetBuffer();
            SqlParameter p = new SqlParameter("@Picture", SqlDbType.Image);
            p.Value = data;
            cmd.Parameters.Add(p);



        }



        public Boolean Validation()
        {
            Boolean status = true;
            if (txtStaffName.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Stafe Name cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtStaffName.Focus();
            }
            else if (txtGrade.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Grade cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtGrade.Focus();
            }
            else if (txtDepartment.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Department cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDepartment.Focus();
            }
            else if (txtDesignation.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Designation cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDesignation.Focus();
            }
            else if (txtQualifications.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Qualifications cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtQualifications.Focus();
            }
            else if (txtBasic.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Salary cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtBasic.Focus();
            }
           
            else if (cmbDrvLicense.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Driving License cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbDrvLicense.Focus();
            }
            else if (cmbDrvLicense.Text == "Yes")
            {
                if (txtLicenseNo.Text.Equals(string.Empty))
                {
                    status = false;
                    MessageBox.Show("License No cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtLicenseNo.Focus();
                }

            }
            else if (cmbGender.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Gender cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbGender.Focus();
            }
           
            else if (cmbMarital.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Marital Status cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbMarital.Focus();
            }
            else if (cmbMarital.Text == "Married")
            {
                if (txtSpouse.Text.Equals(string.Empty))
                {
                    status = false;
                    MessageBox.Show("Spouse cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSpouse.Focus();
                }
            }
            else if (txtNationality.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Nationality Status cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNationality.Focus();
            }
            else if (txtReligion.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Religion Status cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtReligion.Focus();
            }
 
            else if (!(txtEmail.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtEmail.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail.Focus();
                }
            }
            else if (!(txtNEmail.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtNEmail.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNEmail.Focus();
                }
            }
            else if (!(txtDEmail.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtDEmail.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDEmail.Focus();
                }
            }


            else if (pictureBox1.Image==null)
            {
                status = false;
                MessageBox.Show("Select Image.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Browse.Focus();
            }
            else if (txtYOP.Text.Trim().Equals(string.Empty))
            {
                txtYOP.Text = "0";
            }
            if (txtDependant.Text.Trim().Equals(string.Empty))
            {
                txtDependant.Text = "0";
            }
            //else if (txtNPAddress.Text.Trim().Equals(string.Empty))
            //{
            //    status = false;
            //    MessageBox.Show("Permenant Adress cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtNPAddress.Focus();
            //}
            //else if (txtNTAddress.Text.Trim().Equals(string.Empty))
            //{
            //    status = false;
            //    MessageBox.Show("Temperoray Adress cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtNTAddress.Focus();
            //}
            //else if (txtNPhoneNo.Text.Trim().Equals(string.Empty))
            //{
            //    status = false;
            //    MessageBox.Show("Phone Number cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtPhoneNo.Focus();
            //}
            //else if (txtNMobileNo.Text.Trim().Equals(string.Empty))
            //{
            //    status = false;
            //    MessageBox.Show("Mobile Number cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtMobileNo.Focus();
            //}

            return status;
        }
    }
}
