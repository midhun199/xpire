using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Xpire.Classes;

namespace Xpire.Forms
{
    public partial class frmCompanySelection : Form
    {
        public frmCompanySelection()
        {
            
            InitializeComponent();
        }
        cConnection con = new cConnection();
        cGeneral gen = new cGeneral();
        string currentcompcode = cPublic.g_compcode;

        public string comCode = "";
        Boolean frmclose = true;

        public string G_compcode { get; private set; }
        public DateTime G_ClDate { get; private set; }
        public DateTime G_OpDate { get; private set; }
        public string Drive { get; private set; }
        public string G_firmcode { get; private set; }
        public string Abbr { get; private set; }

        private void frmFirmSelection_Load(object sender, EventArgs e)
        {

            string str = string.Empty; 
           
            if (this.Tag == null)
            {
                this.Tag = "C";
            }
           
            string sql = "";
            if (this.Tag.ToString() == "C" || this.Tag.ToString() == "CS"||this.Tag.ToString().Substring(0,1)=="C")
            {
                
                gen.ServerDet();
                con.ConnectMe(cPublic.password,cPublic.Sqluserid,  cPublic.g_PrjCode + "000", cPublic.Sqlservername);

                
                if ((cPublic.g_UserLevel != "9" && cPublic.g_UserLevel != "8"))
                {
                    if (cPublic.g_UserLevel != "1")
                    { sql = "select  a.dispname,a.code from company a, usercompanies b where b.userid='" + cPublic.g_UserId + "' and a.code=b.compcode order by a.dispname"; }
                    else
                    {
                        sql = "select dispname [ NAME OF COMPANY],code from company  " + str + " order by dispname";
                    }
                }
                else
                {
                    sql = "select dispname [ NAME OF COMPANY],code from company "+str + " where code not in ('" + cPublic.g_compcode + "') order by dispname";
                }
            }           
            SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
            DataSet ds = new DataSet();
            da.Fill(ds);
            cfgcompany.DataSource = ds.Tables[0];
            InitializeFlex();

            //if ((this.Tag.ToString() == "CE" || this.Tag.ToString() == "FE"))
            //{
            //    btnok.Text = "Edit";
            //}
            //else if (this.Tag.ToString() == "CD" || this.Tag.ToString() == "FD")
            //{
            //    btnok.Text = "Delete";
            //}
            //else if (this.Tag.ToString() == "FS")
            //{
                btnok.Text = "Select";
            //}

           
        }

      
        private void InitializeFlex()
        {
            cfgcompany.Cols[0].AllowEditing = false;
            cfgcompany.Cols[0].Width = 390;
            cfgcompany.Cols[1].Visible = false;
            cfgcompany.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            cfgcompany.Styles.Highlight.BackColor = Color.LavenderBlush;
            cfgcompany.Styles.Highlight.ForeColor = Color.Purple ;            
            cfgcompany.Cols[0].Style.Margins.Left  = 10;

        }

        private void btncancel_Click(object sender, EventArgs e)
        {

           //this.Close();
            //cPublic.closing = true;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
  
            if (cfgcompany.Rows.Count > 1)
            {
                LoadCompdetails(cfgcompany[cfgcompany.RowSel, 1].ToString());
                this.Close();
            }
       }

        public void LoadCompdetails(String code)
        {
            con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
            string sql = "select code,name,dispname,acctype,opdate,clddate,path,picture,abbr from company where code=" + code;
            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            SqlDataReader dr = comm.ExecuteReader();
            if (dr.Read())
            {
                G_compcode = dr["code"].ToString();
                G_ClDate = Convert.ToDateTime(dr["clddate"].ToString());
                G_OpDate = Convert.ToDateTime(dr["opdate"].ToString());
                Drive = dr["path"].ToString();
                Abbr = dr["abbr"].ToString();
                dr.Close();
                con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + G_compcode, cPublic.Sqlservername);
               
            }
            dr.Close();

            //Firmselection("");
        }

        public void Firmselection(string vCode)
        {
            string sql;
            con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + G_compcode, cPublic.Sqlservername);

           sql = "select code,dispname,name,addr1, addr2, addr3, addr4, phone1, phone2,clddate,opdate,kgst,cstno from firms  where code='" + vCode + "' order  by code";


            SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
            SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                cPublic.g_FirmArray.Add(dr["code"].ToString());
            }

            G_firmcode = ds.Tables[0].Rows[0]["code"].ToString();
           
        }

        private void cfgcompany_DoubleClick(object sender, EventArgs e)
        {
            btnok_Click(sender, e);
        }

        private void frmFirmSelection_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }
       
       

        private bool check(string code)
        {
            string sql = "";
            con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + code, cPublic.Sqlservername);
            sql = "select code from firms";
            SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
            DataSet ds = new DataSet();
            da.Fill(ds);
            for (int i = 0; i < (ds.Tables[0].Rows.Count); i++)
            {
                sql = "select * from voucher" + ds.Tables[0].Rows[i]["code"].ToString();
                SqlCommand comm = new SqlCommand(sql, cPublic.Connection);
                SqlDataReader dr = comm.ExecuteReader();
                if (dr.Read())
                {
                    dr.Close();
                    return true;
                }
                dr.Close();
            }
            return false;
        }

        private void frmFirmSelection_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (!frmclose)
            //{
            //    if (this.Tag.ToString().Substring(0, 1) != "F" && this.Tag.ToString() != "CE" && this.Tag.ToString() != "CD" && this.Tag.ToString() != "BK" && this.Tag.ToString() != "RT")
            //    {
            //        Hide();
            //        string sql = "select getdate()";
            //        SqlCommand cmd = new SqlCommand(sql, cPublic.Connection);
            //        cPublic.g_Rdate = Convert.ToDateTime(cmd.ExecuteScalar());
            //    }
            //}          
        }

        private void frmFirmSelection_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Escape)
            //{ this.Close(); }

            if (e.KeyCode == Keys.Enter)
            {
                
                    LoadCompdetails(cfgcompany[cfgcompany.RowSel, 1].ToString());
                    this.Close();
               
            }
        }
    }
}



