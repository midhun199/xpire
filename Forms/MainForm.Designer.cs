namespace Xpire.Forms
{
    partial class frmMainNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Xpire.Forms.frmMainNew));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.debitNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashPaymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankPaymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creditNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receiptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashReceiptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankReceiptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.journalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankPaymentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashWithdrawalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trialBalanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profitAndLossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.balanceSheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountBooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashBooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankBooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ledgersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.compselection = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.compadd = new System.Windows.Forms.ToolStripMenuItem();
            this.compedit = new System.Windows.Forms.ToolStripMenuItem();
            this.compdelete = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.setRightsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Administration = new System.Windows.Forms.ToolStripMenuItem();
            this.ServerSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.MyNavigateBar = new MT.WindowsUI.NavigationPane.NavigateBar();
            this.nvbTools = new MT.WindowsUI.NavigationPane.NavigateBarButton();
            this.nvbCreate = new MT.WindowsUI.NavigationPane.NavigateBarButton();
            this.faTabStrip1 = new FarsiLibrary.Win.FATabStrip();
            this.faTabStripItem1 = new FarsiLibrary.Win.FATabStripItem();
            this.permissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.MyNavigateBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.faTabStrip1)).BeginInit();
            this.faTabStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.transactionsToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.companyToolStripMenuItem1,
            this.usersToolStripMenuItem,
            this.toolStripMenuItem1,
            this.Administration,
            this.Exit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(934, 24);
            this.menuStrip1.TabIndex = 345;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.newToolStripMenuItem.Text = "&New";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "&Open";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(143, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printToolStripMenuItem.Image")));
            this.printToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.printToolStripMenuItem.Text = "&Print";
            // 
            // printPreviewToolStripMenuItem
            // 
            this.printPreviewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("printPreviewToolStripMenuItem.Image")));
            this.printPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
            this.printPreviewToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.printPreviewToolStripMenuItem.Text = "Print Pre&view";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(143, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator4,
            this.selectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.undoToolStripMenuItem.Text = "&Undo";
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.redoToolStripMenuItem.Text = "&Redo";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(141, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.cutToolStripMenuItem.Text = "Cu&t";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.copyToolStripMenuItem.Text = "&Copy";
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.pasteToolStripMenuItem.Text = "&Paste";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(141, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            // 
            // transactionsToolStripMenuItem
            // 
            this.transactionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseToolStripMenuItem,
            this.paymentsToolStripMenuItem,
            this.salesToolStripMenuItem,
            this.receiptsToolStripMenuItem,
            this.journalToolStripMenuItem,
            this.contraToolStripMenuItem,
            this.reportsToolStripMenuItem});
            this.transactionsToolStripMenuItem.Name = "transactionsToolStripMenuItem";
            this.transactionsToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.transactionsToolStripMenuItem.Text = "Transactions";
            // 
            // purchaseToolStripMenuItem
            // 
            this.purchaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseOrderToolStripMenuItem,
            this.purchaseToolStripMenuItem1,
            this.purchaseOrderToolStripMenuItem1,
            this.debitNoteToolStripMenuItem});
            this.purchaseToolStripMenuItem.Name = "purchaseToolStripMenuItem";
            this.purchaseToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.purchaseToolStripMenuItem.Text = "Purchase";
            // 
            // purchaseOrderToolStripMenuItem
            // 
            this.purchaseOrderToolStripMenuItem.Name = "purchaseOrderToolStripMenuItem";
            this.purchaseOrderToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.purchaseOrderToolStripMenuItem.Text = "Purchase Order";
            // 
            // purchaseToolStripMenuItem1
            // 
            this.purchaseToolStripMenuItem1.Name = "purchaseToolStripMenuItem1";
            this.purchaseToolStripMenuItem1.Size = new System.Drawing.Size(155, 22);
            this.purchaseToolStripMenuItem1.Text = "Purchase";
            // 
            // purchaseOrderToolStripMenuItem1
            // 
            this.purchaseOrderToolStripMenuItem1.Name = "purchaseOrderToolStripMenuItem1";
            this.purchaseOrderToolStripMenuItem1.Size = new System.Drawing.Size(155, 22);
            this.purchaseOrderToolStripMenuItem1.Text = "Purchase Order";
            // 
            // debitNoteToolStripMenuItem
            // 
            this.debitNoteToolStripMenuItem.Name = "debitNoteToolStripMenuItem";
            this.debitNoteToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.debitNoteToolStripMenuItem.Text = "Debit Note";
            // 
            // paymentsToolStripMenuItem
            // 
            this.paymentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cashPaymentsToolStripMenuItem,
            this.bankPaymentsToolStripMenuItem});
            this.paymentsToolStripMenuItem.Name = "paymentsToolStripMenuItem";
            this.paymentsToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.paymentsToolStripMenuItem.Text = "Payments";
            // 
            // cashPaymentsToolStripMenuItem
            // 
            this.cashPaymentsToolStripMenuItem.Name = "cashPaymentsToolStripMenuItem";
            this.cashPaymentsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.cashPaymentsToolStripMenuItem.Text = "Cash Payments";
            // 
            // bankPaymentsToolStripMenuItem
            // 
            this.bankPaymentsToolStripMenuItem.Name = "bankPaymentsToolStripMenuItem";
            this.bankPaymentsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.bankPaymentsToolStripMenuItem.Text = "Bank Payments";
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesOrderToolStripMenuItem,
            this.salesToolStripMenuItem1,
            this.salesReturnToolStripMenuItem,
            this.creditNoteToolStripMenuItem});
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.salesToolStripMenuItem.Text = "Sales";
            // 
            // salesOrderToolStripMenuItem
            // 
            this.salesOrderToolStripMenuItem.Name = "salesOrderToolStripMenuItem";
            this.salesOrderToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.salesOrderToolStripMenuItem.Text = "Sales Order";
            // 
            // salesToolStripMenuItem1
            // 
            this.salesToolStripMenuItem1.Name = "salesToolStripMenuItem1";
            this.salesToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.salesToolStripMenuItem1.Text = "Sales";
            // 
            // salesReturnToolStripMenuItem
            // 
            this.salesReturnToolStripMenuItem.Name = "salesReturnToolStripMenuItem";
            this.salesReturnToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.salesReturnToolStripMenuItem.Text = "Sales Return";
            // 
            // creditNoteToolStripMenuItem
            // 
            this.creditNoteToolStripMenuItem.Name = "creditNoteToolStripMenuItem";
            this.creditNoteToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.creditNoteToolStripMenuItem.Text = "Credit Note";
            // 
            // receiptsToolStripMenuItem
            // 
            this.receiptsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cashReceiptsToolStripMenuItem,
            this.bankReceiptsToolStripMenuItem});
            this.receiptsToolStripMenuItem.Name = "receiptsToolStripMenuItem";
            this.receiptsToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.receiptsToolStripMenuItem.Text = "Receipts";
            // 
            // cashReceiptsToolStripMenuItem
            // 
            this.cashReceiptsToolStripMenuItem.Name = "cashReceiptsToolStripMenuItem";
            this.cashReceiptsToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.cashReceiptsToolStripMenuItem.Text = "Cash Receipts";
            // 
            // bankReceiptsToolStripMenuItem
            // 
            this.bankReceiptsToolStripMenuItem.Name = "bankReceiptsToolStripMenuItem";
            this.bankReceiptsToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.bankReceiptsToolStripMenuItem.Text = "Bank Receipts";
            // 
            // journalToolStripMenuItem
            // 
            this.journalToolStripMenuItem.Name = "journalToolStripMenuItem";
            this.journalToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.journalToolStripMenuItem.Text = "Journal";
            // 
            // contraToolStripMenuItem
            // 
            this.contraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bankPaymentsToolStripMenuItem1,
            this.cashWithdrawalsToolStripMenuItem});
            this.contraToolStripMenuItem.Name = "contraToolStripMenuItem";
            this.contraToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.contraToolStripMenuItem.Text = "Contra";
            // 
            // bankPaymentsToolStripMenuItem1
            // 
            this.bankPaymentsToolStripMenuItem1.Name = "bankPaymentsToolStripMenuItem1";
            this.bankPaymentsToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.bankPaymentsToolStripMenuItem1.Text = "Bank Payments";
            // 
            // cashWithdrawalsToolStripMenuItem
            // 
            this.cashWithdrawalsToolStripMenuItem.Name = "cashWithdrawalsToolStripMenuItem";
            this.cashWithdrawalsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.cashWithdrawalsToolStripMenuItem.Text = "Cash Withdrawals";
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trialBalanceToolStripMenuItem,
            this.profitAndLossToolStripMenuItem,
            this.balanceSheetToolStripMenuItem,
            this.accountBooksToolStripMenuItem,
            this.salesRegisterToolStripMenuItem,
            this.purchaseRegisterToolStripMenuItem,
            this.chequeRegisterToolStripMenuItem,
            this.ledgersToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // trialBalanceToolStripMenuItem
            // 
            this.trialBalanceToolStripMenuItem.Name = "trialBalanceToolStripMenuItem";
            this.trialBalanceToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.trialBalanceToolStripMenuItem.Text = "Trial Balance";
            // 
            // profitAndLossToolStripMenuItem
            // 
            this.profitAndLossToolStripMenuItem.Name = "profitAndLossToolStripMenuItem";
            this.profitAndLossToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.profitAndLossToolStripMenuItem.Text = "Profit And Loss";
            // 
            // balanceSheetToolStripMenuItem
            // 
            this.balanceSheetToolStripMenuItem.Name = "balanceSheetToolStripMenuItem";
            this.balanceSheetToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.balanceSheetToolStripMenuItem.Text = "Balance Sheet";
            // 
            // accountBooksToolStripMenuItem
            // 
            this.accountBooksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cashBooksToolStripMenuItem,
            this.bankBooksToolStripMenuItem});
            this.accountBooksToolStripMenuItem.Name = "accountBooksToolStripMenuItem";
            this.accountBooksToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.accountBooksToolStripMenuItem.Text = "Account Books";
            // 
            // cashBooksToolStripMenuItem
            // 
            this.cashBooksToolStripMenuItem.Name = "cashBooksToolStripMenuItem";
            this.cashBooksToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.cashBooksToolStripMenuItem.Text = "Cash Books";
            // 
            // bankBooksToolStripMenuItem
            // 
            this.bankBooksToolStripMenuItem.Name = "bankBooksToolStripMenuItem";
            this.bankBooksToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.bankBooksToolStripMenuItem.Text = "Bank Books";
            // 
            // salesRegisterToolStripMenuItem
            // 
            this.salesRegisterToolStripMenuItem.Name = "salesRegisterToolStripMenuItem";
            this.salesRegisterToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.salesRegisterToolStripMenuItem.Text = "Sales Register";
            // 
            // purchaseRegisterToolStripMenuItem
            // 
            this.purchaseRegisterToolStripMenuItem.Name = "purchaseRegisterToolStripMenuItem";
            this.purchaseRegisterToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.purchaseRegisterToolStripMenuItem.Text = "Purchase Register";
            // 
            // chequeRegisterToolStripMenuItem
            // 
            this.chequeRegisterToolStripMenuItem.Name = "chequeRegisterToolStripMenuItem";
            this.chequeRegisterToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.chequeRegisterToolStripMenuItem.Text = "Cheque Register";
            // 
            // ledgersToolStripMenuItem
            // 
            this.ledgersToolStripMenuItem.Name = "ledgersToolStripMenuItem";
            this.ledgersToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.ledgersToolStripMenuItem.Text = "Ledgers";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customizeToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.backUpToolStripMenuItem,
            this.restoreToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // customizeToolStripMenuItem
            // 
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.customizeToolStripMenuItem.Text = "&Customize";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.optionsToolStripMenuItem.Text = "&Options";
            // 
            // backUpToolStripMenuItem
            // 
            this.backUpToolStripMenuItem.Name = "backUpToolStripMenuItem";
            this.backUpToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.backUpToolStripMenuItem.Text = "BackUp";
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.restoreToolStripMenuItem.Text = "Restore";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.updatesToolStripMenuItem,
            this.toolStripSeparator5,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // updatesToolStripMenuItem
            // 
            this.updatesToolStripMenuItem.Name = "updatesToolStripMenuItem";
            this.updatesToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.updatesToolStripMenuItem.Text = "Updates";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(119, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            // 
            // companyToolStripMenuItem1
            // 
            this.companyToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compselection,
            this.toolStripSeparator24,
            this.compadd,
            this.compedit,
            this.compdelete});
            this.companyToolStripMenuItem1.Name = "companyToolStripMenuItem1";
            this.companyToolStripMenuItem1.Size = new System.Drawing.Size(71, 20);
            this.companyToolStripMenuItem1.Text = "&Company";
            // 
            // compselection
            // 
            this.compselection.Name = "compselection";
            this.compselection.Size = new System.Drawing.Size(122, 22);
            this.compselection.Text = "Selection";
            this.compselection.Click += new System.EventHandler(this.compselection_Click);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(119, 6);
            // 
            // compadd
            // 
            this.compadd.Name = "compadd";
            this.compadd.Size = new System.Drawing.Size(122, 22);
            this.compadd.Text = "Add";
            this.compadd.Click += new System.EventHandler(this.compadd_Click);
            // 
            // compedit
            // 
            this.compedit.Name = "compedit";
            this.compedit.Size = new System.Drawing.Size(122, 22);
            this.compedit.Text = "Edit";
            this.compedit.Click += new System.EventHandler(this.compedit_Click);
            // 
            // compdelete
            // 
            this.compdelete.Name = "compdelete";
            this.compdelete.Size = new System.Drawing.Size(122, 22);
            this.compdelete.Text = "Delete";
            this.compdelete.Click += new System.EventHandler(this.compdelete_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem1,
            this.editToolStripMenuItem1,
            this.deleteToolStripMenuItem2,
            this.ChangeStripMenuItem,
            this.toolStripSeparator25,
            this.setRightsToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.permissionToolStripMenuItem});
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.usersToolStripMenuItem.Text = "Us&ers";
            // 
            // addToolStripMenuItem1
            // 
            this.addToolStripMenuItem1.Name = "addToolStripMenuItem1";
            this.addToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.addToolStripMenuItem1.Text = "Add";
            this.addToolStripMenuItem1.Click += new System.EventHandler(this.addToolStripMenuItem1_Click);
            // 
            // editToolStripMenuItem1
            // 
            this.editToolStripMenuItem1.Name = "editToolStripMenuItem1";
            this.editToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.editToolStripMenuItem1.Text = "Edit";
            this.editToolStripMenuItem1.Click += new System.EventHandler(this.editToolStripMenuItem1_Click);
            // 
            // deleteToolStripMenuItem2
            // 
            this.deleteToolStripMenuItem2.Name = "deleteToolStripMenuItem2";
            this.deleteToolStripMenuItem2.Size = new System.Drawing.Size(168, 22);
            this.deleteToolStripMenuItem2.Text = "Delete";
            this.deleteToolStripMenuItem2.Click += new System.EventHandler(this.deleteToolStripMenuItem2_Click);
            // 
            // ChangeStripMenuItem
            // 
            this.ChangeStripMenuItem.Name = "ChangeStripMenuItem";
            this.ChangeStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.ChangeStripMenuItem.Text = "Change Password";
            this.ChangeStripMenuItem.Click += new System.EventHandler(this.ChangeStripMenuItem_Click);
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(165, 6);
            // 
            // setRightsToolStripMenuItem
            // 
            this.setRightsToolStripMenuItem.Name = "setRightsToolStripMenuItem";
            this.setRightsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.setRightsToolStripMenuItem.Text = "Set Rights";
            this.setRightsToolStripMenuItem.Click += new System.EventHandler(this.setRightsToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.logOutToolStripMenuItem.Text = "Log &Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registerToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem1.Text = "Register";
            // 
            // registerToolStripMenuItem
            // 
            this.registerToolStripMenuItem.Name = "registerToolStripMenuItem";
            this.registerToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.registerToolStripMenuItem.Text = "Register";
            this.registerToolStripMenuItem.Click += new System.EventHandler(this.registerToolStripMenuItem_Click);
            // 
            // Administration
            // 
            this.Administration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ServerSettings});
            this.Administration.Name = "Administration";
            this.Administration.Size = new System.Drawing.Size(47, 20);
            this.Administration.Text = "&Tools";
            this.Administration.ToolTipText = "Tools";
            // 
            // ServerSettings
            // 
            this.ServerSettings.Name = "ServerSettings";
            this.ServerSettings.Size = new System.Drawing.Size(151, 22);
            this.ServerSettings.Text = "&Server Settings";
            this.ServerSettings.ToolTipText = "Server Settings";
            this.ServerSettings.Click += new System.EventHandler(this.ServerSettings_Click);
            // 
            // Exit
            // 
            this.Exit.Name = "Exit";
            this.Exit.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.L)));
            this.Exit.Size = new System.Drawing.Size(37, 20);
            this.Exit.Text = "E&xit";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 597);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(934, 22);
            this.statusStrip1.TabIndex = 345;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.MyNavigateBar);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.faTabStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(934, 573);
            this.splitContainer1.SplitterDistance = 170;
            this.splitContainer1.TabIndex = 4;
            // 
            // MyNavigateBar
            // 
            this.MyNavigateBar.AlwaysUseSystemColors = false;
            this.MyNavigateBar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.MyNavigateBar.CollapsibleWidth = 31;
            this.MyNavigateBar.Controls.Add(this.nvbTools);
            this.MyNavigateBar.Controls.Add(this.nvbCreate);
            this.MyNavigateBar.DisplayedButtonCount = 2;
            this.MyNavigateBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.MyNavigateBar.Location = new System.Drawing.Point(0, 0);
            this.MyNavigateBar.MinimumSize = new System.Drawing.Size(22, 100);
            this.MyNavigateBar.Name = "MyNavigateBar";
            this.MyNavigateBar.NavigateBarButtons.AddRange(new MT.WindowsUI.NavigationPane.NavigateBarButton[] {
            this.nvbTools,
            this.nvbCreate});
            this.MyNavigateBar.NavigateBarColorTable = ((MT.WindowsUI.NavigationPane.NavigateBarColorTable)(resources.GetObject("MyNavigateBar.NavigateBarColorTable")));
            this.MyNavigateBar.NavigateBarDisplayedButtonCount = 2;
            this.MyNavigateBar.SelectedButton = this.nvbTools;
            this.MyNavigateBar.Size = new System.Drawing.Size(169, 573);
            this.MyNavigateBar.TabIndex = 345;
            this.MyNavigateBar.TabStop = false;
            this.MyNavigateBar.OnNavigateBarCollapseModeChanged += new MT.WindowsUI.NavigationPane.NavigateBar.OnNavigateBarCollapseModeChangedEventHandler(this.MyNavigateBar_OnNavigateBarCollapseModeChanged);
            this.MyNavigateBar.DockChanged += new System.EventHandler(this.MyNavigateBar_DockChanged);
            // 
            // nvbTools
            // 
            this.nvbTools.Caption = "Tools";
            this.nvbTools.CaptionDescription = "Tools";
            this.nvbTools.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nvbTools.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nvbTools.Image = global::Xpire.Properties.Resources.tasks;
            this.nvbTools.IsSelected = true;
            this.nvbTools.IsShowCollapseScreenCaption = false;
            this.nvbTools.Key = "9ADCEFFDF2E44F22BA4CE53FAD4B3047";
            this.nvbTools.Location = new System.Drawing.Point(0, 477);
            this.nvbTools.MinimumSize = new System.Drawing.Size(22, 20);
            this.nvbTools.MouseOverImage = global::Xpire.Properties.Resources.tasks;
            this.nvbTools.Name = "nvbTools";
            this.nvbTools.SelectedImage = global::Xpire.Properties.Resources.tasks;
            this.nvbTools.Size = new System.Drawing.Size(169, 32);
            this.nvbTools.TabIndex = 454;
            this.nvbTools.TabStop = false;
            this.nvbTools.ToolTipText = "Tools";
            this.nvbTools.OnNavigateBarButtonSelected += new MT.WindowsUI.NavigationPane.NavigateBarButton.OnNavigateBarButtonSelectedEventHandler(this.nvbTools_OnNavigateBarButtonSelected);
            // 
            // nvbCreate
            // 
            this.nvbCreate.Caption = "Create";
            this.nvbCreate.CaptionDescription = "Create";
            this.nvbCreate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nvbCreate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nvbCreate.Image = global::Xpire.Properties.Resources._276;
            this.nvbCreate.IsSelected = false;
            this.nvbCreate.IsShowCollapseScreenCaption = false;
            this.nvbCreate.Key = "2AF37763888844C8A038963DDBFDB345";
            this.nvbCreate.Location = new System.Drawing.Point(0, 509);
            this.nvbCreate.MinimumSize = new System.Drawing.Size(22, 20);
            this.nvbCreate.MouseOverImage = global::Xpire.Properties.Resources._276;
            this.nvbCreate.Name = "nvbCreate";
            this.nvbCreate.SelectedImage = global::Xpire.Properties.Resources._276;
            this.nvbCreate.Size = new System.Drawing.Size(169, 32);
            this.nvbCreate.TabIndex = 455;
            this.nvbCreate.TabStop = false;
            this.nvbCreate.ToolTipText = "Create";
            this.nvbCreate.OnNavigateBarButtonSelected += new MT.WindowsUI.NavigationPane.NavigateBarButton.OnNavigateBarButtonSelectedEventHandler(this.nvbCreate_OnNavigateBarButtonSelected);
            // 
            // faTabStrip1
            // 
            this.faTabStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.faTabStrip1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.faTabStrip1.Items.AddRange(new FarsiLibrary.Win.FATabStripItem[] {
            this.faTabStripItem1});
            this.faTabStrip1.Location = new System.Drawing.Point(0, 0);
            this.faTabStrip1.Name = "faTabStrip1";
            this.faTabStrip1.SelectedItem = this.faTabStripItem1;
            this.faTabStrip1.Size = new System.Drawing.Size(760, 573);
            this.faTabStrip1.TabIndex = 0;
            this.faTabStrip1.Text = "faTabStrip1";
            this.faTabStrip1.TabStripItemClosing += new FarsiLibrary.Win.TabStripItemClosingHandler(this.faTabStrip1_TabStripItemClosing);
            // 
            // faTabStripItem1
            // 
            this.faTabStripItem1.IsDrawn = true;
            this.faTabStripItem1.Name = "faTabStripItem1";
            this.faTabStripItem1.Selected = true;
            this.faTabStripItem1.Size = new System.Drawing.Size(758, 552);
            this.faTabStripItem1.TabIndex = 45;
            this.faTabStripItem1.Title = "TabStrip Page 1";
            // 
            // permissionToolStripMenuItem
            // 
            this.permissionToolStripMenuItem.Name = "permissionToolStripMenuItem";
            this.permissionToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.permissionToolStripMenuItem.Text = "Permission";
            this.permissionToolStripMenuItem.Click += new System.EventHandler(this.permissionToolStripMenuItem_Click);
            // 
            // frmMainNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 619);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMainNew";
            this.Text = "LOTZ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.MyNavigateBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.faTabStrip1)).EndInit();
            this.faTabStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private MT.WindowsUI.NavigationPane.NavigateBar MyNavigateBar;
        private MT.WindowsUI.NavigationPane.NavigateBarButton nvbTools;
        private MT.WindowsUI.NavigationPane.NavigateBarButton nvbCreate;
        private System.Windows.Forms.ToolStripMenuItem transactionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem purchaseOrderToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem debitNoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashPaymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankPaymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salesReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creditNoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receiptsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashReceiptsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankReceiptsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem journalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankPaymentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cashWithdrawalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trialBalanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profitAndLossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem balanceSheetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountBooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashBooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankBooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ledgersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updatesToolStripMenuItem;
        private System.Windows.Forms.TreeView trvMain;
        private FarsiLibrary.Win.FATabStrip faTabStrip1;
        private FarsiLibrary.Win.FATabStripItem faTabStripItem1;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem compselection;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripMenuItem compadd;
        private System.Windows.Forms.ToolStripMenuItem compedit;
        private System.Windows.Forms.ToolStripMenuItem compdelete;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ChangeStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ToolStripMenuItem setRightsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Administration;
        private System.Windows.Forms.ToolStripMenuItem ServerSettings;
        internal System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem permissionToolStripMenuItem;
    }
}

