﻿using System.Windows.Forms;
namespace Xpire.Forms
{
    partial class frmInitialize
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlDetails = new Panel();
            this.Panel2 = new Panel();
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox3 = new GroupBox();
            this.rdbLookClear = new System.Windows.Forms.RadioButton();
            this.rdbLookKeep = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new GroupBox();
            this.grpStk = new GroupBox();
            this.lblStk = new System.Windows.Forms.Label();
            this.rdbStkOb = new System.Windows.Forms.RadioButton();
            this.rdbStkZero = new System.Windows.Forms.RadioButton();
            this.rdbStkNc = new System.Windows.Forms.RadioButton();
            this.chkStock = new CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rdbStkClr = new System.Windows.Forms.RadioButton();
            this.rdbStkKp = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new GroupBox();
            this.grpAcc = new GroupBox();
            this.lblAcc = new System.Windows.Forms.Label();
            this.rdbAccOb = new System.Windows.Forms.RadioButton();
            this.rdbAccZero = new System.Windows.Forms.RadioButton();
            this.rdbAccNc = new System.Windows.Forms.RadioButton();
            this.chkOpbalance = new CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rdbAccClr = new System.Windows.Forms.RadioButton();
            this.rdbAccKp = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new GroupBox();
            this.txtPwd = new TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Panel1 = new Panel();
            this.btnInit = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            //((System.ComponentModel.ISupportInitialize)(this.pnlDetails)).BeginInit();
            //this.pnlDetails.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel2)).BeginInit();
            //this.Panel2.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.groupBox3)).BeginInit();
            //this.groupBox3.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.groupBox4)).BeginInit();
            //this.groupBox4.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.grpStk)).BeginInit();
            //this.grpStk.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.chkStock)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.groupBox2)).BeginInit();
            //this.groupBox2.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.grpAcc)).BeginInit();
            //this.grpAcc.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.chkOpbalance)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.groupBox1)).BeginInit();
            //this.groupBox1.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtPwd)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel1)).BeginInit();
            //this.Panel1.SuspendLayout();
            //this.SuspendLayout();
            // 
            // pnlDetails
            // 
            this.pnlDetails.Controls.Add(this.Panel2);
            this.pnlDetails.Location = new System.Drawing.Point(359, 68);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(395, 491);
            this.pnlDetails.TabIndex = 5;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
        
            this.Panel2.Controls.Add(this.ProgressBar1);
            this.Panel2.Controls.Add(this.groupBox3);
            this.Panel2.Controls.Add(this.groupBox4);
            this.Panel2.Controls.Add(this.groupBox2);
            this.Panel2.Controls.Add(this.groupBox1);
            this.Panel2.Location = new System.Drawing.Point(4, 91);

            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(386, 394);
            this.Panel2.TabIndex = 5;
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(9, 361);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(369, 23);
            this.ProgressBar1.TabIndex = 4;
            // 
            // groupBox3
            // 
        
            this.groupBox3.Controls.Add(this.rdbLookClear);
            this.groupBox3.Controls.Add(this.rdbLookKeep);
            this.groupBox3.Location = new System.Drawing.Point(9, 295);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(370, 55);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.Text = "Lookup";
            // 
            // rdbLookClear
            // 
            this.rdbLookClear.AutoSize = true;
            this.rdbLookClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbLookClear.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbLookClear.Location = new System.Drawing.Point(107, 30);
            this.rdbLookClear.Name = "rdbLookClear";
            this.rdbLookClear.Size = new System.Drawing.Size(59, 19);
            this.rdbLookClear.TabIndex = 14;
            this.rdbLookClear.Text = "Clear";
            this.rdbLookClear.UseVisualStyleBackColor = true;
            // 
            // rdbLookKeep
            // 
            this.rdbLookKeep.AutoSize = true;
            this.rdbLookKeep.Checked = true;
            this.rdbLookKeep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbLookKeep.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbLookKeep.Location = new System.Drawing.Point(22, 30);
            this.rdbLookKeep.Name = "rdbLookKeep";
            this.rdbLookKeep.Size = new System.Drawing.Size(58, 19);
            this.rdbLookKeep.TabIndex = 13;
            this.rdbLookKeep.TabStop = true;
            this.rdbLookKeep.Text = "Keep";
            this.rdbLookKeep.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
         
            this.groupBox4.Controls.Add(this.grpStk);
            this.groupBox4.Controls.Add(this.chkStock);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.rdbStkClr);
            this.groupBox4.Controls.Add(this.rdbStkKp);
            this.groupBox4.Location = new System.Drawing.Point(8, 184);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(371, 104);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.Text = "Stock Items";
            // 
            // grpStk
            // 

            this.grpStk.Controls.Add(this.lblStk);
            this.grpStk.Controls.Add(this.rdbStkOb);
            this.grpStk.Controls.Add(this.rdbStkZero);
            this.grpStk.Controls.Add(this.rdbStkNc);
            this.grpStk.Location = new System.Drawing.Point(10, 60);
            this.grpStk.Name = "grpStk";

            this.grpStk.Size = new System.Drawing.Size(351, 35);
            this.grpStk.TabIndex = 19;
            this.grpStk.Text = "GroupBox6";
            // 
            // lblStk
            // 
            this.lblStk.AutoSize = true;
            this.lblStk.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblStk.Location = new System.Drawing.Point(9, 11);
            this.lblStk.Name = "lblStk";
            this.lblStk.Size = new System.Drawing.Size(0, 13);
            this.lblStk.TabIndex = 11;
            // 
            // rdbStkOb
            // 
            this.rdbStkOb.AutoSize = true;
            this.rdbStkOb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbStkOb.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbStkOb.Location = new System.Drawing.Point(98, 8);
            this.rdbStkOb.Name = "rdbStkOb";
            this.rdbStkOb.Size = new System.Drawing.Size(100, 19);
            this.rdbStkOb.TabIndex = 11;
            this.rdbStkOb.Text = "Op-Balance";
            this.rdbStkOb.UseVisualStyleBackColor = true;
            // 
            // rdbStkZero
            // 
            this.rdbStkZero.AutoSize = true;
            this.rdbStkZero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbStkZero.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbStkZero.Location = new System.Drawing.Point(13, 8);
            this.rdbStkZero.Name = "rdbStkZero";
            this.rdbStkZero.Size = new System.Drawing.Size(54, 19);
            this.rdbStkZero.TabIndex = 10;
            this.rdbStkZero.Text = "Zero";
            this.rdbStkZero.UseVisualStyleBackColor = true;
            // 
            // rdbStkNc
            // 
            this.rdbStkNc.AutoSize = true;
            this.rdbStkNc.Checked = true;
            this.rdbStkNc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbStkNc.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbStkNc.Location = new System.Drawing.Point(234, 8);
            this.rdbStkNc.Name = "rdbStkNc";
            this.rdbStkNc.Size = new System.Drawing.Size(96, 19);
            this.rdbStkNc.TabIndex = 12;
            this.rdbStkNc.TabStop = true;
            this.rdbStkNc.Text = "No Change";
            this.rdbStkNc.UseVisualStyleBackColor = true;
            // 
            // chkStock
            // 

            this.chkStock.Location = new System.Drawing.Point(195, 34);
            this.chkStock.Name = "chkStock";

            this.chkStock.Size = new System.Drawing.Size(154, 19);
            this.chkStock.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.SaddleBrown;
            this.label3.Location = new System.Drawing.Point(20, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 16;
            // 
            // rdbStkClr
            // 
            this.rdbStkClr.AutoSize = true;
            this.rdbStkClr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbStkClr.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbStkClr.Location = new System.Drawing.Point(108, 33);
            this.rdbStkClr.Name = "rdbStkClr";
            this.rdbStkClr.Size = new System.Drawing.Size(59, 19);
            this.rdbStkClr.TabIndex = 8;
            this.rdbStkClr.Text = "Clear";
            this.rdbStkClr.UseVisualStyleBackColor = true;
            this.rdbStkClr.CheckedChanged += new System.EventHandler(this.rdbStkClr_CheckedChanged);
            // 
            // rdbStkKp
            // 
            this.rdbStkKp.AutoSize = true;
            this.rdbStkKp.Checked = true;
            this.rdbStkKp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbStkKp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbStkKp.Location = new System.Drawing.Point(23, 33);
            this.rdbStkKp.Name = "rdbStkKp";
            this.rdbStkKp.Size = new System.Drawing.Size(58, 19);
            this.rdbStkKp.TabIndex = 7;
            this.rdbStkKp.TabStop = true;
            this.rdbStkKp.Text = "Keep";
            this.rdbStkKp.UseVisualStyleBackColor = true;
            this.rdbStkKp.CheckedChanged += new System.EventHandler(this.rdbStkKp_CheckedChanged);
            // 
            // groupBox2
            // 

            this.groupBox2.Controls.Add(this.grpAcc);
            this.groupBox2.Controls.Add(this.chkOpbalance);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.rdbAccClr);
            this.groupBox2.Controls.Add(this.rdbAccKp);
            this.groupBox2.Location = new System.Drawing.Point(8, 74);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(371, 104);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.Text = "Account Heads";
            // 
            // grpAcc
            // 

            this.grpAcc.Controls.Add(this.lblAcc);
            this.grpAcc.Controls.Add(this.rdbAccOb);
            this.grpAcc.Controls.Add(this.rdbAccZero);
            this.grpAcc.Controls.Add(this.rdbAccNc);
            this.grpAcc.Location = new System.Drawing.Point(10, 59);
            this.grpAcc.Name = "grpAcc";

            this.grpAcc.Size = new System.Drawing.Size(351, 35);
            this.grpAcc.TabIndex = 14;
            this.grpAcc.Text = "GroupBox1";
            // 
            // lblAcc
            // 
            this.lblAcc.AutoSize = true;
            this.lblAcc.ForeColor = System.Drawing.Color.Red;
            this.lblAcc.Location = new System.Drawing.Point(9, 11);
            this.lblAcc.Name = "lblAcc";
            this.lblAcc.Size = new System.Drawing.Size(0, 13);
            this.lblAcc.TabIndex = 6;
            // 
            // rdbAccOb
            // 
            this.rdbAccOb.AutoSize = true;
            this.rdbAccOb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbAccOb.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbAccOb.Location = new System.Drawing.Point(98, 8);
            this.rdbAccOb.Name = "rdbAccOb";
            this.rdbAccOb.Size = new System.Drawing.Size(100, 19);
            this.rdbAccOb.TabIndex = 5;
            this.rdbAccOb.Text = "Op-Balance";
            this.rdbAccOb.UseVisualStyleBackColor = true;
            // 
            // rdbAccZero
            // 
            this.rdbAccZero.AutoSize = true;
            this.rdbAccZero.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbAccZero.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbAccZero.Location = new System.Drawing.Point(13, 8);
            this.rdbAccZero.Name = "rdbAccZero";
            this.rdbAccZero.Size = new System.Drawing.Size(54, 19);
            this.rdbAccZero.TabIndex = 4;
            this.rdbAccZero.Text = "Zero";
            this.rdbAccZero.UseVisualStyleBackColor = true;
            // 
            // rdbAccNc
            // 
            this.rdbAccNc.AutoSize = true;
            this.rdbAccNc.Checked = true;
            this.rdbAccNc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbAccNc.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbAccNc.Location = new System.Drawing.Point(234, 8);
            this.rdbAccNc.Name = "rdbAccNc";
            this.rdbAccNc.Size = new System.Drawing.Size(96, 19);
            this.rdbAccNc.TabIndex = 6;
            this.rdbAccNc.TabStop = true;
            this.rdbAccNc.Text = "No Change";
            this.rdbAccNc.UseVisualStyleBackColor = true;
            // 
            // chkOpbalance
            // 

            this.chkOpbalance.Location = new System.Drawing.Point(195, 34);
            this.chkOpbalance.Name = "chkOpbalance";

            this.chkOpbalance.Size = new System.Drawing.Size(154, 19);
            this.chkOpbalance.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(19, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 11;
            // 
            // rdbAccClr
            // 
            this.rdbAccClr.AutoSize = true;
            this.rdbAccClr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbAccClr.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbAccClr.Location = new System.Drawing.Point(108, 32);
            this.rdbAccClr.Name = "rdbAccClr";
            this.rdbAccClr.Size = new System.Drawing.Size(59, 19);
            this.rdbAccClr.TabIndex = 2;
            this.rdbAccClr.Text = "Clear";
            this.rdbAccClr.UseVisualStyleBackColor = true;
            this.rdbAccClr.CheckedChanged += new System.EventHandler(this.rdbAccClr_CheckedChanged);
            // 
            // rdbAccKp
            // 
            this.rdbAccKp.AutoSize = true;
            this.rdbAccKp.Checked = true;
            this.rdbAccKp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbAccKp.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rdbAccKp.Location = new System.Drawing.Point(23, 32);
            this.rdbAccKp.Name = "rdbAccKp";
            this.rdbAccKp.Size = new System.Drawing.Size(58, 19);
            this.rdbAccKp.TabIndex = 1;
            this.rdbAccKp.TabStop = true;
            this.rdbAccKp.Text = "Keep";
            this.rdbAccKp.UseVisualStyleBackColor = true;
            this.rdbAccKp.CheckedChanged += new System.EventHandler(this.rdbAccKp_CheckedChanged);
            // 
            // groupBox1
            // 

            this.groupBox1.Controls.Add(this.txtPwd);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(370, 55);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.Text = "Process Confirmation";
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(197, 27);
            this.txtPwd.Name = "txtPwd";
      
            this.txtPwd.Size = new System.Drawing.Size(152, 22);
            this.txtPwd.TabIndex = 0;
            this.txtPwd.TextChanged += new System.EventHandler(this.txtPwd_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(122, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Password";
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));

            this.Panel1.Controls.Add(this.btnInit);
            this.Panel1.Controls.Add(this.btnExit);
            this.Panel1.Location = new System.Drawing.Point(363, 75);

            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(386, 82);
            this.Panel1.TabIndex = 15;
            // 
            // btnInit
            // 
            this.btnInit.BackColor = System.Drawing.Color.White;
            this.btnInit.BackgroundImage = global::Xpire.Properties.Resources.initialize;
            this.btnInit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnInit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnInit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInit.ForeColor = System.Drawing.Color.Maroon;
            this.btnInit.Location = new System.Drawing.Point(6, 7);
            this.btnInit.Margin = new System.Windows.Forms.Padding(4);
            this.btnInit.Name = "btnInit";
            this.btnInit.Size = new System.Drawing.Size(76, 71);
            this.btnInit.TabIndex = 15;
            this.btnInit.TabStop = false;
            this.btnInit.Tag = "search";
            this.btnInit.UseVisualStyleBackColor = false;
            this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Maroon;
            this.btnExit.Location = new System.Drawing.Point(81, 7);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 71);
            this.btnExit.TabIndex = 16;
            this.btnExit.TabStop = false;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmInitialize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.pnlDetails);
            this.Name = "frmInitialize";
            this.Size = new System.Drawing.Size(1087, 640);
            //((System.ComponentModel.ISupportInitialize)(this.pnlDetails)).EndInit();
            //this.pnlDetails.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.Panel2)).EndInit();
            //this.Panel2.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.groupBox3)).EndInit();
            //this.groupBox3.ResumeLayout(false);
            //this.groupBox3.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.groupBox4)).EndInit();
            //this.groupBox4.ResumeLayout(false);
            //this.groupBox4.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.grpStk)).EndInit();
            //this.grpStk.ResumeLayout(false);
            //this.grpStk.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.chkStock)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.groupBox2)).EndInit();
            //this.groupBox2.ResumeLayout(false);
            //this.groupBox2.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.grpAcc)).EndInit();
            //this.grpAcc.ResumeLayout(false);
            //this.grpAcc.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.chkOpbalance)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.groupBox1)).EndInit();
            //this.groupBox1.ResumeLayout(false);
            //this.groupBox1.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtPwd)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel1)).EndInit();
            //this.Panel1.ResumeLayout(false);
            //this.ResumeLayout(false);

        }

        #endregion

        private Panel pnlDetails;
        private Panel Panel2;
        private GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdbLookClear;
        private System.Windows.Forms.RadioButton rdbLookKeep;
        private GroupBox groupBox4;
        private GroupBox grpStk;
        private System.Windows.Forms.RadioButton rdbStkOb;
        private System.Windows.Forms.RadioButton rdbStkZero;
        private System.Windows.Forms.RadioButton rdbStkNc;
        private CheckBox chkStock;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rdbStkClr;
        private System.Windows.Forms.RadioButton rdbStkKp;
        private GroupBox groupBox2;
        private GroupBox grpAcc;
        private System.Windows.Forms.RadioButton rdbAccOb;
        private System.Windows.Forms.RadioButton rdbAccZero;
        private System.Windows.Forms.RadioButton rdbAccNc;
        private CheckBox chkOpbalance;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdbAccClr;
        private System.Windows.Forms.RadioButton rdbAccKp;
        private GroupBox groupBox1;
        private TextBox txtPwd;
        private System.Windows.Forms.Label label1;
        private Panel Panel1;
        internal System.Windows.Forms.Button btnInit;
        public System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ProgressBar ProgressBar1;
        private System.Windows.Forms.Label lblStk;
        private System.Windows.Forms.Label lblAcc;

    }
}
