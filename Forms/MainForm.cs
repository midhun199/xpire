using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;


using Xpire.Classes;
using Xpire.Settings;
using Xpire.Reports;
using Xpire.BL;
//using ControlBasedSecurity;

namespace Xpire.Forms
{
   
    public partial class frmMainNew : Form
    {

        FarsiLibrary.Win.FATabStripItem registration_Strip= new FarsiLibrary.Win.FATabStripItem();
        ucRegister cRegister = new ucRegister();

        FarsiLibrary.Win.FATabStripItem empregistration_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucEmployeeRegistration cEmployeeDetails = new ucEmployeeRegistration();

        FarsiLibrary.Win.FATabStripItem empcancel_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucCancelledEmployee cEmployeeCancel = new ucCancelledEmployee();

        FarsiLibrary.Win.FATabStripItem leaveapplication_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucLeaveApplication cLeaveApplication = new ucLeaveApplication();

        FarsiLibrary.Win.FATabStripItem leaveapproval_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucLeaveApproval cLeaveApproval = new ucLeaveApproval();

        FarsiLibrary.Win.FATabStripItem rejoining_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucRejoining cRejoining = new ucRejoining();


        FarsiLibrary.Win.FATabStripItem renewal_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucRenewal cRenewal = new ucRenewal();

        FarsiLibrary.Win.FATabStripItem purchase_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucPurchase cPurchase = new ucPurchase();

        FarsiLibrary.Win.FATabStripItem purchasereturn_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucPurchase cPurchasereturn = new ucPurchase();

        FarsiLibrary.Win.FATabStripItem frmInitialize_Strip = new FarsiLibrary.Win.FATabStripItem();
        frmInitialize cInitialize = new frmInitialize();

        FarsiLibrary.Win.FATabStripItem Sales_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucSales sale_ui = new ucSales();

         //FarsiLibrary.Win.FATabStripItem Labour_Strip = new FarsiLibrary.Win.FATabStripItem();
         //ucLabour labour_ui = new ucLabour();



        FarsiLibrary.Win.FATabStripItem SalesReturn_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucSales saleReturn_ui = new ucSales();

        //FarsiLibrary.Win.FATabStripItem SpareParts_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucLabour spareparts_ui = new ucLabour();

       
        FarsiLibrary.Win.FATabStripItem stockmaster_Strip = new FarsiLibrary.Win.FATabStripItem();


        //FarsiLibrary.Win.FATabStripItem accounts_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucAccounts accounts_ui = new ucAccounts();


        FarsiLibrary.Win.FATabStripItem commonmaster_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucCommonMaster CommonMaster_ui = new ucCommonMaster();

        FarsiLibrary.Win.FATabStripItem payment_Strip = new FarsiLibrary.Win.FATabStripItem();
      

        FarsiLibrary.Win.FATabStripItem reciept_Strip = new FarsiLibrary.Win.FATabStripItem();
      

        //FarsiLibrary.Win.FATabStripItem cashreciept_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucCashPayment cashreciept_ui = new ucCashPayment();

        //FarsiLibrary.Win.FATabStripItem cashpayment_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucCashPayment cashpayment_ui = new ucCashPayment();

        FarsiLibrary.Win.FATabStripItem transaction_Strip = new FarsiLibrary.Win.FATabStripItem();
  

        //FarsiLibrary.Win.FATabStripItem debitnote_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucJournal debitnote_ui = new ucJournal();

        //FarsiLibrary.Win.FATabStripItem creditnote_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucJournal creditnote_ui = new ucJournal();

        //FarsiLibrary.Win.FATabStripItem journal_Strip = new FarsiLibrary.Win.FATabStripItem();
        //ucJournal journal_ui = new ucJournal();

        FarsiLibrary.Win.FATabStripItem ledger_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucLedgerView ledger_ui = new ucLedgerView();

        FarsiLibrary.Win.FATabStripItem setparameters_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucParameters setparameters_ui = new ucParameters();

        FarsiLibrary.Win.FATabStripItem daybook_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucDayBook daybook_ui = new ucDayBook();


        FarsiLibrary.Win.FATabStripItem ledgerSingle_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucLedger ledgerSingle_ui = new ucLedger();

        FarsiLibrary.Win.FATabStripItem salesbillwise_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucSalesBillWise salesbillwise_ui = new ucSalesBillWise();

        FarsiLibrary.Win.FATabStripItem purchaseinvoice_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucPurchaseInvoice purchaseinvoice_ui = new ucPurchaseInvoice();

        FarsiLibrary.Win.FATabStripItem stockdetails_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucStockDetails stockdetails_ui = new ucStockDetails();

        FarsiLibrary.Win.FATabStripItem EmployeeReport_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucEmployeeReport EmployeeReport_ui = new ucEmployeeReport();

        FarsiLibrary.Win.FATabStripItem DocumentReport_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucDocumentReport DocumentReport_ui = new ucDocumentReport();


        FarsiLibrary.Win.FATabStripItem salesPerformance_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucSalesPerformance salesPerformance_ui = new ucSalesPerformance();


        FarsiLibrary.Win.FATabStripItem schedule_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucSchedule schedule_ui = new ucSchedule();


        FarsiLibrary.Win.FATabStripItem TrialBalance_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucTrialBalance TrialBalance_ui = new ucTrialBalance();

        FarsiLibrary.Win.FATabStripItem TBL_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucTBL TBL_ui = new ucTBL();

        FarsiLibrary.Win.FATabStripItem BalanceSheet_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucBalanceSheet BalanceSheet_ui = new ucBalanceSheet();

        FarsiLibrary.Win.FATabStripItem ChequeRegister_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucChequeRegister ChequeRegister_ui = new ucChequeRegister();

        FarsiLibrary.Win.FATabStripItem VatComputation_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucVatComputation VatComputation_ui = new ucVatComputation();

        FarsiLibrary.Win.FATabStripItem Form10_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucForm10 Form10_ui = new ucForm10();

        FarsiLibrary.Win.FATabStripItem AnnualReturn_Strip = new FarsiLibrary.Win.FATabStripItem();
        ucAnnualReturn AnnualReturn_ui = new ucAnnualReturn();

        public frmMainNew()
        {
            this.trvMain = new System.Windows.Forms.TreeView();
            this.trvMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trvMain.Location = new System.Drawing.Point(0, 0);
            this.trvMain.Name = "trvMain";
            this.trvMain.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.trvMain.Size = new System.Drawing.Size(162, 176);
            this.trvMain.TabIndex = 0;

            

            InitializeComponent();
            
            nvbTools.RelatedControl = trvMain;
            
            nvbCreate.RelatedControl = trvMain;
            //toolStrip1.Visible = false;
            this.trvMain.NodeMouseClick += new TreeNodeMouseClickEventHandler(trvMain_NodeMouseClick);
            this.trvMain.KeyDown += new KeyEventHandler(trvMain_KeyDown);
            this.trvMain.AfterSelect += trvMain_AfterSelect;
        }

        void trvMain_AfterSelect(object sender, TreeViewEventArgs e)
        {
           
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            new SoftBO().Updates(false);

            MyNavigateBar.Width = splitContainer1.Panel1.Width;
            faTabStrip1.Items.Clear();
            Load_screen(801);       
            Load_Menu();

            Display_MainButtons();

            this.Text = cPublic.g_firmname; 
        }

        private void Display_MainButtons()
        {
            if (Convert.ToInt32(cPublic.g_UserLevel) >= 9) { return; }
            //nvbCreate
            if (Load_screen(801).Rows.Count == 0)
            {
                nvbCreate.Enabled = false;
                //this.MyNavigateBar.NavigateBarButtons.Remove(nvbCreate); 
            }
            //nvbTools
            if (Load_screen(701).Rows.Count == 0)
            {
                nvbTools.Enabled = false;
                //this.MyNavigateBar.NavigateBarButtons.Remove(nvbTools);
            }
            //nvbReport
            if (Load_screen(601).Rows.Count == 0)
            {
                //nvbReport.Enabled = false;
                //this.MyNavigateBar.NavigateBarButtons.Remove(nvbReport);
            }
            //nvbContra
            if (Load_screen(501).Rows.Count == 0)
            {
                //nvbContra.Enabled = false;
                //this.MyNavigateBar.NavigateBarButtons.Remove(nvbContra); 
            }
            //nvbJournalVoucher
        }

        private void Load_Menu()
        {
            //this.menuStrip1.Visible = false;
            
            this.menuStrip1.Items.Clear();
            if (cPublic.g_compcode != "000" && cPublic.g_compcode != string.Empty)
            {
                this.splitContainer1.Visible = true;
                this.menuStrip1.Visible = false;
                //this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                //this.fileToolStripMenuItem,
                //this.editToolStripMenuItem, 
                //this.transactionsToolStripMenuItem , 
                //this.toolsToolStripMenuItem ,
                //this.helpToolStripMenuItem
                //        });
            }
            else
            {
                this.splitContainer1.Visible = false;
                this.menuStrip1.Visible = true;
                this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                        this.companyToolStripMenuItem1,
                        this.usersToolStripMenuItem,
                        this.Administration, 
                        this.toolStripMenuItem1,
                        this.Exit });

                if (cPublic.g_UserLevel == "9" || cPublic.g_UserLevel == "8")
                {
                    toolStripSeparator24.Visible = true;
                    compadd.Visible = true;
                    compedit.Visible = true;
                    compdelete.Visible = true;
                    addToolStripMenuItem1.Visible = true;
                    editToolStripMenuItem1.Visible = true;
                    deleteToolStripMenuItem2.Visible = true;
                    setRightsToolStripMenuItem.Visible = true;
                    toolStripSeparator25.Visible = true;
                    ServerSettings.Visible = true;
                    //registerToolStripMenuItem.Visible = true;
                }
                else
                {
                    toolStripSeparator24.Visible = false;
                    compadd.Visible = false;
                    compedit.Visible = false;
                    compdelete.Visible = false;
                    addToolStripMenuItem1.Visible = false;
                    editToolStripMenuItem1.Visible = false;
                    deleteToolStripMenuItem2.Visible = false;
                    setRightsToolStripMenuItem.Visible = false;
                    toolStripSeparator25.Visible = false;
                    ServerSettings.Visible = false;
                    //registerToolStripMenuItem.Visible = true;
                }
            }
        }

        private DataTable Load_screen(int id)
        {
            trvMain.Nodes.Clear();

            SqlCommand cmd = new SqlCommand("Select * from " + cPublic.g_PrjCode + "000..Setrights  where  level" + cPublic.g_UserLevel + "=1  and menulevel=0", cPublic.Connection);
            SqlDataAdapter ap = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            ap.Fill(dt);
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                TreeNode tre = trvMain.Nodes.Add(dt.Rows[j]["slno"].ToString(), dt.Rows[j]["menudesc"].ToString());
                Getnode(tre, Convert.ToInt32( dt.Rows[j]["slno"].ToString()));
            }
            if (trvMain.Nodes.Count > 0) { trvMain.Nodes[0].Expand(); }
          

          //  this.trvMain.SelectedNode = this.trvMain.Nodes[0].Nodes[1]; 
            
           
          //Display_Forms(trvMain.SelectedNode.Name);

         
            return dt;  
        }

        private void Getnode(TreeNode tre, int parent)
        {
            SqlCommand cmd = new SqlCommand("Select * from " + cPublic.g_PrjCode + "000..Setrights where Parent=" + parent + " and level" + cPublic.g_UserLevel + "=1 ", cPublic.Connection);
            SqlDataAdapter ap = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            ap.Fill(dt);
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                TreeNode tr = tre.Nodes.Add(dt.Rows[j]["slno"].ToString(), dt.Rows[j]["menudesc"].ToString());
                Getnode(tr, Convert.ToInt32(dt.Rows[j]["slno"].ToString()));
            }
        }

        private void trvMain_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Display_Forms(e.Node.Name);


        }        

        void trvMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && trvMain.SelectedNode != null)
            {
                Display_Forms(trvMain.SelectedNode.Name);
            }
        }

        private void Display_Forms(string NodeName)
        {
            switch (NodeName)
            {
                case "10": //Purchase Order
                    break;
                case "860": //Purchase
                    if (registration_Strip.IsDisposed == true)
                    { registration_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(registration_Strip);
                    registration_Strip.Title = "Registration";

                    if (cRegister.IsDisposed == true)
                    { cRegister = new ucRegister(); }
                    cRegister.Tag = "RR";
                    registration_Strip.Controls.Add(cRegister);
                    cRegister.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = registration_Strip;
                    registration_Strip.Focus();


                    cRegister.btnExit.Click += ucRegister_Click;
                    break;
                case "861": //Renewal
                    if (faTabStrip1.Items.Contains(renewal_Strip))
                    {
                        faTabStrip1.Items.Remove(renewal_Strip);
                        renewal_Strip = new FarsiLibrary.Win.FATabStripItem();
                        cRenewal = new ucRenewal();
                    }
                    if (renewal_Strip.IsDisposed == true)
                    { renewal_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(renewal_Strip);
                    renewal_Strip.Title = "Renewal";

                    if (cRenewal.IsDisposed == true)
                    {
                        cRenewal = new ucRenewal();
                    }
                    //cRenewal.Tag = "RN";
                    renewal_Strip.Controls.Add(cRenewal);
                    cRenewal.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = renewal_Strip;
                    renewal_Strip.Focus();
                    cRenewal.Focus();
                    //cRenewal.btnExit.Click += new EventHandler(PurchaseExit_Click);

                    break;
                case "862": //Employee Registration
                    if (empregistration_Strip.IsDisposed == true)
                    { empregistration_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(empregistration_Strip);
                    empregistration_Strip.Title = "Employee Registration";

                    if (cEmployeeDetails.IsDisposed == true)
                    { cEmployeeDetails = new ucEmployeeRegistration(); }
                    cEmployeeDetails.Tag = "ER";
                    empregistration_Strip.Controls.Add(cEmployeeDetails);
                    cEmployeeDetails.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = empregistration_Strip;
                    empregistration_Strip.Focus();
                    cEmployeeDetails.btnExit.Click += cEmployeeDetailsExit_Click;
                    break;

                case "866": //Rejoining
                    if (empcancel_Strip.IsDisposed == true)
                    { empcancel_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(empcancel_Strip);
                    empcancel_Strip.Title = "Cancelled Employees";

                    if (cEmployeeCancel.IsDisposed == true)
                    { cEmployeeCancel = new ucCancelledEmployee(); }
                    cEmployeeCancel.Tag = "CE";
                    empcancel_Strip.Controls.Add(cEmployeeCancel);
                    cEmployeeCancel.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = empcancel_Strip;
                    empcancel_Strip.Focus();
                    cEmployeeCancel.btnExit.Click += cEmployeeDetailsExit_Click;
                    break;

                case "863": //Leave Application
                    if (leaveapplication_Strip.IsDisposed == true)
                    { leaveapplication_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(leaveapplication_Strip);
                    leaveapplication_Strip.Title = "Leave Application";

                    if (cLeaveApplication.IsDisposed == true)
                    { cLeaveApplication = new ucLeaveApplication(); }
                    cLeaveApplication.Tag = "LA";
                    leaveapplication_Strip.Controls.Add(cLeaveApplication);
                    cLeaveApplication.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = leaveapplication_Strip;
                    leaveapplication_Strip.Focus();
                    cLeaveApplication.btnExit.Click += cLeaveApplicationExit_Click;
                    break;

                case "864": //Leave Approval
                    if (leaveapproval_Strip.IsDisposed == true)
                    { leaveapproval_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(leaveapproval_Strip);
                    leaveapproval_Strip.Title = "Leave Approval";

                    if (cLeaveApproval.IsDisposed == true)
                    { cLeaveApproval = new ucLeaveApproval(); }
                    cLeaveApproval.Tag = "LA";
                    leaveapproval_Strip.Controls.Add(cLeaveApproval);
                    cLeaveApproval.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = leaveapproval_Strip;
                    leaveapproval_Strip.Focus();
                    cLeaveApproval.btnExit.Click += cLeaveApprovalExit_Click;
                    break;

                case "865": //Rejoining
                    if (rejoining_Strip.IsDisposed == true)
                    { rejoining_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(rejoining_Strip);
                    rejoining_Strip.Title = "Rejoining";

                    if (cRejoining.IsDisposed == true)
                    { cRejoining = new ucRejoining(); }
                    cRejoining.Tag = "LA";
                    rejoining_Strip.Controls.Add(cRejoining);
                    cRejoining.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = rejoining_Strip;
                    rejoining_Strip.Focus();
                    cRejoining.btnExit.Click += cRejoiningExit_Click;
                    break;

                case "830": //Common Masters	
                    if (commonmaster_Strip.IsDisposed == true)
                    { commonmaster_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(commonmaster_Strip);
                    commonmaster_Strip.Title = "Common Master";

                    if (CommonMaster_ui.IsDisposed == true)
                    { CommonMaster_ui = new ucCommonMaster(); }

                    commonmaster_Strip.Controls.Add(CommonMaster_ui);
                    CommonMaster_ui.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = commonmaster_Strip;
                    commonmaster_Strip.Focus();
                    CommonMaster_ui.btnClose.Click += new EventHandler(CommonMaster_Click);
                    break;

                case "840": //Set Parameters
                    if (setparameters_Strip.IsDisposed == true)
                    { setparameters_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(setparameters_Strip);
                    setparameters_Strip.Title = "Set Parameters";

                    if (setparameters_ui.IsDisposed == true)
                    { setparameters_ui = new ucParameters(); }

                    setparameters_Strip.Controls.Add(setparameters_ui);
                    setparameters_ui.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = setparameters_Strip;
                    setparameters_Strip.Focus();
                    setparameters_ui.btnClose.Click += new EventHandler(setparameters_Click);
                    break;

                case "650": //Employee Report
                    if (EmployeeReport_Strip.IsDisposed == true)
                    { EmployeeReport_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(EmployeeReport_Strip);
                    EmployeeReport_Strip.Title = "Employee Reports";

                    if (EmployeeReport_ui.IsDisposed == true)
                    { EmployeeReport_ui = new ucEmployeeReport(); }

                    EmployeeReport_Strip.Controls.Add(EmployeeReport_ui);
                    EmployeeReport_ui.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = EmployeeReport_Strip;
                    EmployeeReport_Strip.Focus();
                    EmployeeReport_ui.btnClose.Click += EmployeeReport_uiClose_Click;
                    break;
                case "695": //Document Report
                    if (DocumentReport_Strip.IsDisposed == true)
                    { DocumentReport_Strip = new FarsiLibrary.Win.FATabStripItem(); }

                    faTabStrip1.Items.Add(DocumentReport_Strip);
                    DocumentReport_Strip.Title = "Document Reports";

                    if (DocumentReport_ui.IsDisposed == true)
                    { DocumentReport_ui = new ucDocumentReport(); }

                    DocumentReport_Strip.Controls.Add(DocumentReport_ui);
                    DocumentReport_ui.Dock = DockStyle.Fill;
                    faTabStrip1.SelectedItem = DocumentReport_Strip;
                    DocumentReport_Strip.Focus();
                    DocumentReport_ui.btnClose.Click += new EventHandler(DocumentReport_ui_Click);
                    break;
                case "702": //Backup	
                    new cBackupRestore().Backup();
                    break;
                case "703": //Restore	 
                    new cBackupRestore().Restore();
                    break;
                default:
                    break;
            }
        }

        private void EmployeeReport_uiClose_Click(object sender, EventArgs e)
        {
            if (EmployeeReport_ui.IsDisposed == true)
            {
                if (faTabStrip1.Items.Contains(EmployeeReport_Strip) == true)
                {
                    faTabStrip1.Items.Remove(EmployeeReport_Strip);
                    TabTripSelection();
                }
            }
        }

        private void cRejoiningExit_Click(object sender, EventArgs e)
        {
            if (cRejoining.IsDisposed == true)
            {
                if (faTabStrip1.Items.Contains(rejoining_Strip) == true)
                {
                    faTabStrip1.Items.Remove(rejoining_Strip);
                    TabTripSelection();
                }
            }
        }

        private void cLeaveApprovalExit_Click(object sender, EventArgs e)
        {
            if (cLeaveApproval.IsDisposed == true)
            {
                if (faTabStrip1.Items.Contains(leaveapproval_Strip) == true)
                {
                    faTabStrip1.Items.Remove(leaveapproval_Strip);
                    TabTripSelection();
                }
            }
        }

        private void cLeaveApplicationExit_Click(object sender, EventArgs e)
        {
            if (cLeaveApplication.IsDisposed == true)
            {
                if (faTabStrip1.Items.Contains(leaveapplication_Strip) == true)
                {
                    faTabStrip1.Items.Remove(leaveapplication_Strip);
                    TabTripSelection();
                }
            }
        }

        private void cEmployeeDetailsExit_Click(object sender, EventArgs e)
        {
            if (cEmployeeDetails.IsDisposed == true)
            {
                if (faTabStrip1.Items.Contains(empregistration_Strip) == true)
                {
                    faTabStrip1.Items.Remove(empregistration_Strip);
                    
                    TabTripSelection();
                }
            }
        }

        private void ucRegister_Click(object sender, EventArgs e)
        {
            if (cRegister.IsDisposed == true)
            {
                if (faTabStrip1.Items.Contains(registration_Strip) == true)
                {
                    faTabStrip1.Items.Remove(registration_Strip);
                    TabTripSelection();
                }
            }
        }

        private void DocumentReport_ui_Click(object sender, EventArgs e)
        {
            if (DocumentReport_ui.IsDisposed == true)
            {
                if (faTabStrip1.Items.Contains(DocumentReport_Strip) == true)
                {
                    faTabStrip1.Items.Remove(DocumentReport_Strip);
                    TabTripSelection();
                }
            }
        }

        void debitnotecancel_Click(object sender, EventArgs e)
        {
            //if (debitnote_ui.IsDisposed == true)
            //{
            //    faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
            //    TabTripSelection();
            //}
        }

        void creditnotecancel_Click(object sender, EventArgs e)
        {
            //if (creditnote_ui.IsDisposed == true)
            //{
            //    faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
            //    TabTripSelection();
            //}
        }

        void journalcancel_Click(object sender, EventArgs e)
        {
            //if (journal_ui.IsDisposed == true)
            //{
            //    faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
            //    TabTripSelection();
            //}
        }

        void payment_Click(object sender, EventArgs e)
        {
           
        }

        void cashpayment_Click(object sender, EventArgs e)
        {
            //if (cashpayment_ui.IsDisposed == true)
            //{
            //    faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
            //    TabTripSelection();
            //}
        }

        void reciept_Click(object sender, EventArgs e)
        {
            
        }

        void cashreciept_Click(object sender, EventArgs e)
        {
            //if (cashreciept_ui.IsDisposed == true)
            //{
            //    faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
            //    TabTripSelection();
            //}
        }

        void transaction_Click(object sender, EventArgs e)
        {
           
        }

        void CommonMaster_Click(object sender, EventArgs e)
        {
            if (CommonMaster_ui.IsDisposed == true)
            {
                if (faTabStrip1.Items.Contains(commonmaster_Strip) == true)
                {
                    faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                    TabTripSelection();
                }
            }
        }

        void setparameters_Click(object sender, EventArgs e)
        {
            if (setparameters_ui.IsDisposed == true )
            {
                if (faTabStrip1.Items.Contains(setparameters_Strip) == true)
                {
                    faTabStrip1.Items.Remove(setparameters_Strip);
                    TabTripSelection();
                }
            }
        }

        void btnExit_Click(object sender, EventArgs e)
        {
           
        }

        void PurchaseExit_Click(object sender, EventArgs e)
        {
            if (cRegister.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void PurchaseReturnExit_Click(object sender, EventArgs e)
        {
            //if (cPurchasereturn.IsDisposed == true)
            //{
            //    faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
            //    TabTripSelection();
            //}
        }

        void SalesExit_Click(object sender, EventArgs e)
        {
            //if (sale_ui.IsDisposed == true)
            //{
            //    faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
            //    TabTripSelection();
            //}
        }

        void SalesReturnExit_Click(object sender, EventArgs e)
        {
            //if (saleReturn_ui.IsDisposed == true)
            //{
            //    faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
            //    TabTripSelection();
            //}
        }

        void ledgerClose_Click(object sender, EventArgs e)
        {
            if (ledger_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void scheduleClose_Click(object sender, EventArgs e)
        {
            if (schedule_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void TrialBalanceClose_Click(object sender, EventArgs e)
        {
            if (TrialBalance_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void TBLClose_Click(object sender, EventArgs e)
        {
            if (TBL_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void BalanceSheetClose_Click(object sender, EventArgs e)
        {
            if (BalanceSheet_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void AnnualReturnClose_Click(object sender, EventArgs e)
        {
            if (AnnualReturn_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void Form10Close_Click(object sender, EventArgs e)
        {
            if (Form10_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void VatComputationClose_Click(object sender, EventArgs e)
        {
            if (VatComputation_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void ChequeRegisterClose_Click(object sender, EventArgs e)
        {
            if (ChequeRegister_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void daybookClose_Click(object sender, EventArgs e)
        {
            if (daybook_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void ledgerSingleClose_Click(object sender, EventArgs e)
        {
            if (ledgerSingle_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void salesbillwiseClose_Click(object sender, EventArgs e)
        {
            if (salesbillwise_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void purchaseinvoiceClose_Click(object sender, EventArgs e)
        {
            if (purchaseinvoice_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void stockdetailsClose_Click(object sender, EventArgs e)
        {
            if (stockdetails_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        void salesPerformanceClose_Click(object sender, EventArgs e)
        {
            if (salesPerformance_ui.IsDisposed == true)
            {
                faTabStrip1.Items.Remove(faTabStrip1.SelectedItem);
                TabTripSelection();
            }
        }

        private void nvbPurchase_OnNavigateBarButtonSelected(MT.WindowsUI.NavigationPane.NavigateBarButtonEventArgs e)
        {
            Load_screen(1);
        }

        private void nvbTransaction_OnNavigateBarButtonSelected(MT.WindowsUI.NavigationPane.NavigateBarButtonEventArgs e)
        {
            Load_screen(101);
        }

        private void nvbRecieptVoucher_OnNavigateBarButtonSelected(MT.WindowsUI.NavigationPane.NavigateBarButtonEventArgs e)
        {
            Load_screen(901);
        }

        private void nvbJournalVoucher_OnNavigateBarButtonSelected(MT.WindowsUI.NavigationPane.NavigateBarButtonEventArgs e)
        {
            Load_screen(1001);
        }

        private void nvbSales_OnNavigateBarButtonSelected(MT.WindowsUI.NavigationPane.NavigateBarButtonEventArgs e)
        {
            Load_screen(201);
        }

        private void nvbContra_OnNavigateBarButtonSelected(MT.WindowsUI.NavigationPane.NavigateBarButtonEventArgs e)
        {
            Load_screen(501);
        }

        private void nvbReport_OnNavigateBarButtonSelected(MT.WindowsUI.NavigationPane.NavigateBarButtonEventArgs e)
        {
            Load_screen(601);
        }

        private void nvbTools_OnNavigateBarButtonSelected(MT.WindowsUI.NavigationPane.NavigateBarButtonEventArgs e)
        {
            Load_screen(701);
        }

        private void nvbCreate_OnNavigateBarButtonSelected(MT.WindowsUI.NavigationPane.NavigateBarButtonEventArgs e)
        {
            Load_screen(801);
        }

        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter) { SendKeys.Send("{TAB}"); }
        }

        private void compselection_Click(object sender, EventArgs e)
        {
            frmFirmSelection sel = new frmFirmSelection();
            sel.ShowDialog();
        }

        private void compadd_Click(object sender, EventArgs e)
        {
            frmcompany comp = new frmcompany();
            comp.Tag = "CA";
            comp.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            comp.ShowDialog();
        }

        private void compedit_Click(object sender, EventArgs e)
        {
            frmFirmSelection sel = new frmFirmSelection();
            sel.Tag = "CE";
            sel.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            sel.ShowDialog();
        }

        private void compdelete_Click(object sender, EventArgs e)
        {
            frmFirmSelection sel = new frmFirmSelection();
            sel.Tag = "CD";
            sel.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            sel.MdiParent = this;
            sel.TopMost = true;
            sel.Show();
        }

        private void addToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmUser usr = new frmUser();
            usr.Tag = "Add";
            usr.MdiParent = this;
            usr.Text = "Add User";
            usr.Show();
            usr.txtUserID.Focus();
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmUser usr = new frmUser();
            usr.Tag = "Edit";
            usr.MdiParent = this;
            usr.Text = "Edit User";
            usr.Show();
        }

        private void deleteToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmUser usr = new frmUser();
            usr.Tag = "Delete";
            usr.MdiParent = this;
            usr.Text = "Delete User";
            usr.Show();
        }

        private void ChangeStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUser usr = new frmUser();
            usr.Tag = "Change";
            usr.MdiParent = this;
            usr.Text = "Change Password";
            usr.Show();
        }

        private void setRightsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSetRights set = new frmSetRights();
            set.MdiParent = this;
            set.Show();

        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Sure to Logout", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question).Equals(DialogResult.Yes))
            {
                logout();
            }
        }

        public void logout()
        {
            int count = Application.OpenForms.Count;
            menuStrip1.Items.Clear();
            int i = 0;
            while (Application.OpenForms.Count > 1)
            {
                if (Application.OpenForms[i].Equals(this))
                {
                    i++;
                    continue;
                }
                Application.OpenForms[i].Close();
                i = 0;
            }
            cPublic.g_firmcode = "";
            cPublic.g_firmname = "";
            cPublic.g_UserId = "";
            cPublic.g_DispName = "";

            FrmLogin log = new FrmLogin();
            log.ShowDialog();
            if (cPublic.Login == false)
            {
                MessageBox.Show("No Privellage existing Please contact administrator", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Application.Exit();
            }

        }

        private void ServerSettings_Click(object sender, EventArgs e)
        {
            cGeneral cgen = new cGeneral();
            frmServerSetting serverseetting = new frmServerSetting();
            serverseetting.Tag = "SE";
            serverseetting.ShowDialog();
            if (cgen.ServerDet())
            {
                cConnection con = new cConnection();
                if (con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername))
                {

                }
                else
                {
                    MessageBox.Show("Database Connection Failed ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    Application.Exit();
                }
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to Exit !", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Dispose();
            }
        }

        private void faTabStrip1_TabStripItemClosing(FarsiLibrary.Win.TabStripItemClosingEventArgs e)
        {
            faTabStrip1.SelectedItem.Dispose();
            TabTripSelection();
        }


        private void TabTripSelection()
        {
            if (faTabStrip1.Items.Count > 0)
            { faTabStrip1.SelectedItem = faTabStrip1.Items[faTabStrip1.Items.Count - 1]; }
        }

        private void MyNavigateBar_DockChanged(object sender, EventArgs e)
        {

        }

        private void MyNavigateBar_OnNavigateBarCollapseModeChanged(bool tIsCollaped)
        {
            if (tIsCollaped == true) { splitContainer1.SplitterDistance = 1; }
            else { splitContainer1.SplitterDistance = 186; }
        }



        private void registerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Register r = new Register();

            r.ShowDialog();
        }

        private void permissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ManagePermissions mn = new ManagePermissions();
            //mn.ShowDialog();
        }
    }
}