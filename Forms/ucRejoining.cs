﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using Xpire.BL;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Forms
{
    public partial class ucRejoining : UserControl
    {

        SqlDataReader rdr = null;
        DataTable dtable = new DataTable();

        DataSet ds = new DataSet();
        SqlCommand cmd = null;
        DataTable dt = new DataTable();
        string id = string.Empty;
        string appid = string.Empty;
        string sqlQuery = string.Empty, nxtPrv = string.Empty;
        Boolean searchClick = false, autoCode = false, EscKey = false;
        cGeneral gen = new cGeneral();
        private string PS = string.Empty;

        public ucRejoining()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchClick = true;
            btnExit.Tag = "&Cancel";
            btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
            gen.ClearFieldsInMe(this);
            txtStaffID.Enabled = true;
            txtStaffID.Focus();
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnNext.Enabled = false;
            btnPrevious.Enabled = false;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            nxtPrv = "previous";
            //msgLabel.Text = string.Empty;

            sqlQuery = "select top 1 e.id,b.StaffID,b.StaffName,c.Details,d.[from] ,d.[to] ,e.DOR,e.DOARR,e.DOJ,e.remarks from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code join [dbo].[LeaveApproval" + cPublic.g_firmcode + "] d on d.[applicationid] = a.id join [dbo].[REJOINING" + cPublic.g_firmcode + "] e on e.approvalid=d.id where e.id < @id"
            + "  order by e.id desc";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@id", id.Trim());
            LoadDetails(cmd, "");
        }

        private void ucRejoining_Load(object sender, EventArgs e)
        {
            lblItemCount.BorderStyle = BorderStyle.None;


            this.Tag = "No Records";

            loadFirstRecord();
            lblItemCount.Text = "No of Application : " + getItemCount().ToString();

            if (this.Tag.Equals("No Records"))
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
            }
        }

        private void txtStaffID_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchClick)
            {
                if (e.KeyCode.Equals(Keys.F5))
                {
                    string fields = string.Empty;
                    string dispName = string.Empty;
                    string width = string.Empty;
                    //if (cPublic.LookupDefault == "Itemcode")
                    //{
                    fields = "id,StaffID,StaffName,DOR";
                    dispName = "id,Staff ID,Name,DOR";
                    width = "0,100,150,75";
                    //}
                    //else
                    //{
                    //    fields = "itemname,itemcode,qty,Rate,taxper,cessper";
                    //    dispName = "Item Name, Item Code, Qty, Rate, Tax %, Cess %";
                    //    width = "350,80,80,100,50,60";
                    //}
                    frmlookup lookup = new frmlookup();
                    lookup.m_table = "LeaveApplication" + cPublic.g_firmcode;
                    lookup.m_fields = fields;
                    lookup.m_dispname = dispName;
                    lookup.m_fldwidth = width;
                    lookup.q_type = "SearchRejoining";
                    lookup.m_con = cPublic.Connection;
                    lookup.ShowDialog();
                    if (lookup.m_values.Count > 0)
                    {
                        //if (cPublic.LookupDefault == "Itemcode")
                        //{
                        id = lookup.m_values[0].ToString();
                        txtStaffID.Text = lookup.m_values[1].ToString();
                        txtStaffName.Text = lookup.m_values[2].ToString();
                        //}
                        //else
                        //{
                        //    id = lookup.m_values[1].ToString();
                        //    txtItmName.Text = lookup.m_values[0].ToString();
                        //}
                        sqlQuery = "select e.id,b.[StaffID],b.[StaffName],d.[from],d.[to],e.[DOR],e.[DOJ],e.[DOARR],c.[details],e.remarks from [dbo].[LeaveApplication0001] a join [Employee0001] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code  join [dbo].[LeaveApproval0001] d on d.[applicationid] = a.id join  [dbo].[REJOINING0001] e on e.approvalid=d.id where e.id  = @id";
                        SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                        cmd.Parameters.AddWithValue("@id", id.Trim());
                        LoadDetails(cmd, "");
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnDelete.Enabled = true;
                        btnNext.Enabled = true;
                        btnPrevious.Enabled = true;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        searchClick = false;
                    }
                }

            }

            else
            {
                if (e.KeyCode.Equals(Keys.F5))
                {
                    string fields = string.Empty;
                    string dispName = string.Empty;
                    string width = string.Empty;
                    //if (cPublic.LookupDefault == "Itemcode")
                    //{
                    fields = "id,StaffID,StaffName";
                    dispName = "id,Staff ID,Name";
                    width = "0,100,150";

                    //}
                    //else
                    //{
                    //    fields = "itemname,itemcode,qty,Rate,taxper,cessper";
                    //    dispName = "Item Name, Item Code, Qty, Rate, Tax %, Cess %";
                    //    width = "350,80,80,100,50,60";
                    //}
                    frmlookup lookup = new frmlookup();
                    lookup.m_table = "Employee" + cPublic.g_firmcode;
                    lookup.m_fields = fields;
                    lookup.m_dispname = dispName;
                    lookup.m_fldwidth = width;
                    lookup.q_type = "Rejoining";
                    lookup.m_con = cPublic.Connection;
                    lookup.ShowDialog();
                    if (lookup.m_values.Count > 0)
                    {

                        appid = lookup.m_values[0].ToString();

                        txtStaffID.Text = lookup.m_values[1].ToString();

                        txtStaffName.Text = lookup.m_values[2].ToString();

                        sqlQuery = "select b.[StaffID],b.[StaffName],d.[from],d.[to],c.[details] from [dbo].[LeaveApplication0001] a join [Employee0001] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code  join [dbo].[LeaveApproval0001] d on d.[applicationid] = a.id  where d.id = @id";
                        SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                        cmd.Parameters.AddWithValue("@id", appid.Trim());


                        LoadDetails(cmd, "new");
                        //lblBalLeave.Text = "Balance Leave : " + getBalLeave().ToString();
                        searchClick = false;

                    }
                }
            }
        }

        private void Items(SqlCommand cmd)
        {

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id.Trim());
            cmd.Parameters.AddWithValue("@approvalid", appid);
            cmd.Parameters.AddWithValue("@DOR", Convert.ToDateTime(dtpDOR.Value));
            cmd.Parameters.AddWithValue("@DOJ", Convert.ToDateTime(dtpDOJ.Value));
            cmd.Parameters.AddWithValue("@DOARR", Convert.ToDateTime(dtpDOARR.Value));

            int balday = 0;


            cmd.Parameters.AddWithValue("@balleave", balday);
            cmd.Parameters.AddWithValue("@remarks", txtRemarks.Text);
            //cmd.Parameters.AddWithValue("@DOA", Convert.ToDateTime(dtpDOA.Value));

        }
        public Boolean UpdateItem()
        {
            SqlTransaction sqlTrans = null;
            Boolean status = true;

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;


                cmd.Parameters.Clear();
                cmd.CommandText = "SP_UPDATE_REJOINING" + cPublic.g_firmcode;

                Items(cmd);

                cmd.ExecuteNonQuery();

                sqlQuery = "select a.id,b.[StaffID],b.[StaffName],a.DOA,a.[From]  ,a.[To] ,c.Code LeaveTypeCode, c.Details LeaveTypeDetails from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID]= b.[StaffID] join LOOKUP  c on a.[LeaveType]= c.code where a.id = @id";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@id", id.Trim());
                LoadDetails(cmd, "");
            }
            catch (Exception ex)
            {
                sqlTrans.Rollback();
                MessageBox.Show(ex.Message);
                status = false;
            }
            return status;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                switch (btnAdd.Tag.ToString())
                {
                    case "&Add":

                        btnAdd.Tag = "&Save";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.save1;
                        btnExit.Tag = "&Cancel";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

                        gen.EDControls(this, true);


                        EnableButtons(false);
                        btnAdd.Enabled = true;
                        btnExit.Enabled = true;


                        loadDefultValues();



                        break;
                    case "&Save":
                        if (Validation())
                        {
                            autoCode = true;

                            SqlCommand cmd = new SqlCommand("SP_INSERT_REJOINING" + cPublic.g_firmcode, cPublic.Connection);

                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Connection = cPublic.Connection;


                            cmd.Parameters.Clear();


                            Items(cmd);

                            id = cmd.ExecuteScalar().ToString();


                            //DateTime startdate;
                            //DateTime enddate;
                            //TimeSpan remaindate;

                           

                            //startdate = Convert.ToDateTime(dtpAppFrom.Value).Date;
                            //enddate = Convert.ToDateTime(dtpAppTo.Value).Date;

                            //remaindate = enddate - startdate;

                            int bal = getBalLeave();

                            //sqlQuery = "UPDATE b SET b.[balleave] =  "+ bal + " FROM [dbo].[LeaveApproval" + cPublic.g_firmcode + "] b  join [dbo].[LeaveApplication" + cPublic.g_firmcode + "] c on c.id=b.[applicationid]  join [dbo].[REJOINING0001] e on e.approvalid=b.id where c.staffid=@staffid and b.id=@id ";
                            //cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                            //cmd.Parameters.AddWithValue("@id", appid.Trim());
                            ////cmd.Parameters.AddWithValue("@balleave", getBalLeave());
                            //cmd.Parameters.AddWithValue("@staffid", txtStaffID.Text);
                            //cmd.ExecuteNonQuery();

                            //sqlQuery = "UPDATE b SET e.[balleave] =  " + bal + " FROM [dbo].[LeaveApproval" + cPublic.g_firmcode + "] b  join [dbo].[LeaveApplication" + cPublic.g_firmcode + "] c on c.id=b.[applicationid]  join [dbo].[REJOINING0001] e on e.approvalid=b.id where c.staffid=@staffid and b.id=@id ";
                            //cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                            //cmd.Parameters.AddWithValue("@id", id.Trim());
                            ////cmd.Parameters.AddWithValue("@balleave", getBalLeave());
                            //cmd.Parameters.AddWithValue("@staffid", txtStaffID.Text);
                            //cmd.ExecuteNonQuery();
                            //if (btnEdit.Tag == "&Update")
                            //{ cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(txtCurStock.Text) - opQty + Convert.ToDecimal(txtOPStock.Text)); }
                            //else { cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(txtOPStock.Text.Trim())); }


                            sqlQuery = "select e.id,b.[StaffID],b.[StaffName],d.[from],d.[to],e.[DOR],e.[DOJ],e.[DOARR],c.[details],e.remarks from [dbo].[LeaveApplication0001] a join [Employee0001] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code  join [dbo].[LeaveApproval0001] d on d.[applicationid] = a.id join  [dbo].[REJOINING0001] e on e.approvalid=d.id where e.id = @id";
                            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                            cmd.Parameters.AddWithValue("@id", id.Trim());
                            LoadDetails(cmd, "");
                            autoCode = false;
                            lblItemCount.Text = "No of Items : " + getItemCount().ToString();
                            MessageBox.Show("New Item Saved Successfully......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            EnableButtons(true);

                            autoCode = false;
                            btnAdd.Tag = "&Add";
                            btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                            PS = "PRINT";
                            cPublic.PrintMode = "WINDOWS";

                            if (PS == "PRINT")
                            {
                                if (cPublic.PrintMode == "WINDOWS")
                                {
                                    new cWindowsPrint("[dbo].[Rejoining" + cPublic.g_firmcode + "]", "", id, "Rejoining");
                                }
                            }


                        }
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private int getBalLeave()
        {

            SqlCommand cmd = new SqlCommand("SP_GET_BALLEAVE" + cPublic.g_firmcode, cPublic.Connection);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Connection = cPublic.Connection;


            cmd.Parameters.Clear();


            cmd.Parameters.AddWithValue("@Staffid", txtStaffID.Text.Trim());


            return Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            nxtPrv = "next";


            sqlQuery = "select top 1 e.id,b.StaffID,b.StaffName,c.Details,d.[from] ,d.[to] ,e.DOR,e.DOARR,e.DOJ,e.remarks from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code join [dbo].[LeaveApproval" + cPublic.g_firmcode + "] d on d.[applicationid] = a.id join [dbo].[REJOINING" + cPublic.g_firmcode + "] e on e.approvalid=d.id where e.id > @id"
            + "  order by e.id ";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@id", id.Trim());

            LoadDetails(cmd, "");
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            switch (btnEdit.Tag.ToString())
            {
                case "&Edit":
                    if (id.Trim() != string.Empty && txtStaffID.Text.Trim() == string.Empty)
                    { MessageBox.Show("You are not Allowed to Edit this Item..."); return; }
                    btnEdit.Tag = "&Update";
                    btnEdit.BackgroundImage = global::Xpire.Properties.Resources.save1;
                    btnExit.Tag = "&Cancel";
                    btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

                    gen.EDControls(this, true);

                    EnableButtons(false);
                    btnEdit.Enabled = true;
                    btnExit.Enabled = true;


                    break;
                case "&Update":
                    if (Validation())
                    {
                        if (UpdateItem())
                        {
                            MessageBox.Show("Item Details Updated Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            EnableButtons(true);

                            btnEdit.Tag = "&Edit";
                            btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;


                        }
                        else { MessageBox.Show("Invalid Data.. Please Check...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    break;

            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            switch (btnExit.Tag.ToString())
            {
                case "E&xit":
                    this.Dispose();
                    break;
                case "&Cancel":
                    if ((MessageBox.Show("Are You Sure to Cancel...?  ", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question)).Equals(DialogResult.Yes))
                    {
                

                        gen.ClearFieldsInMe(this);
                        EnableButtons(true);

                        btnAdd.Tag = "&Add";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                        btnEdit.Tag = "&Edit";
                        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        loadDefultValues();
                        loadFirstRecord();
                        searchClick = false;
                        autoCode = false;

                    }
                    EscKey = false;

                    break;
            }
        }


        private void EnableButtons(Boolean status)
        {
            btnSearch.Enabled = status;
            btnNext.Enabled = status;
            btnPrevious.Enabled = status;
            btnEdit.Enabled = status;
            btnDelete.Enabled = status;
            btnAdd.Enabled = status;
            btnExit.Enabled = status;

        }

        private void loadDefultValues()
        {

            id = string.Empty;
            txtStaffName.Text = string.Empty;
            txtStaffID.Text = string.Empty;
            dtpDOR.Value = System.DateTime.Now;
            dtpFrom.Value = System.DateTime.Now;
            dtpTo.Value = System.DateTime.Now;
            dtpDOJ.Value = System.DateTime.Now;
            dtpDOARR.Value = System.DateTime.Now;
            txtLeaveType.Text = string.Empty;

        }

        public Boolean Validation()
        {
            Boolean status = true;
            if (txtStaffName.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Stafe Name cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtStaffName.Focus();
            }

            else if (txtStaffID.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Staff ID cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtStaffID.Focus();
            }

            else if (txtLeaveType.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Leave Type cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtLeaveType.Focus();
            }


            return status;
        }

        private int getItemCount()
        {
            sqlQuery = "select  isnull(count(*),0) from REJOINING" + cPublic.g_firmcode;
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            return Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }

        private Boolean LoadDetails(SqlCommand cmd, string type)
        {

            Boolean status = true;

            SqlDataReader dr;

            cmd.Connection = cPublic.Connection;

            dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                

                txtStaffID.Text = dr["StaffID"].ToString();

                txtStaffName.Text = dr["StaffName"].ToString();            

                dtpFrom.Text = dr["From"].ToString();

                dtpTo.Text = dr["To"].ToString();

                if (type == "new")
                {
                    id = "";

                    dtpDOR.Text = dr["To"].ToString();

                    dtpDOJ.Text = dr["To"].ToString();

                    dtpDOARR.Text = dr["To"].ToString();
                }
                else

                {
                    id = dr["id"].ToString();

                    dtpDOR.Text = dr["DOR"].ToString();

                    dtpDOJ.Text = dr["DOJ"].ToString();

                    dtpDOARR.Text = dr["DOARR"].ToString();

                    this.txtRemarks.Text = dr["remarks"].ToString();
                }


                

                this.txtLeaveType.Text = dr["Details"].ToString();

                this.Tag = "Data Loaded";

            }
            else { status = false; }
            dr.Close();

            gen.EDControls(groupBox1, false);
            if (type == "new")
            {
                gen.EDControls(groupBox3, true);
            }
            else
                gen.EDControls(groupBox3, false);
            int count = getItemCount();

            if (nxtPrv.Equals("next") && !status)
            {
                nxtPrv = string.Empty;
            }
            else if (nxtPrv.Equals("previous") && !status)
            {
                nxtPrv = string.Empty;
            }
            if (count == 0)
            { btnEdit.Enabled = false; btnDelete.Enabled = false; }
            return status;
        }

        private void loadFirstRecord()
        {
            sqlQuery = "select top 1 e.id,b.StaffID,b.StaffName,c.Details,d.[from] ,d.[to] ,e.DOR,e.DOARR,e.DOJ,e.remarks from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID] = b.[StaffID] join LOOKUP  c on a.[LeaveType] = c.code join [dbo].[LeaveApproval" + cPublic.g_firmcode + "] d on d.[applicationid] = a.id join [dbo].[REJOINING" + cPublic.g_firmcode + "] e on e.approvalid=d.id order by e.[id]";

            cmd = new SqlCommand(sqlQuery, cPublic.Connection);

            LoadDetails(cmd, "");
        }



    }
}
