﻿using CSBSSWControls;
using System.Windows.Forms;

namespace Xpire.Forms
{
    partial class ucEmployeeRegistration
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbEmployee = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtRemarks = new System.Windows.Forms.RichTextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.txtLicenseNo = new System.Windows.Forms.TextBox();
            this.Browse = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbDrvLicense = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpDOA = new System.Windows.Forms.DateTimePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.dtpDOJ = new System.Windows.Forms.DateTimePicker();
            this.txtPhoneNo = new System.Windows.Forms.MaskedTextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtMobileNo = new System.Windows.Forms.MaskedTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGrade = new System.Windows.Forms.TextBox();
            this.txtStaffID = new System.Windows.Forms.TextBox();
            this.Label13 = new System.Windows.Forms.Label();
            this.txtDepartment = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtYOP = new System.Windows.Forms.TextBox();
            this.txtPrevEmp = new System.Windows.Forms.TextBox();
            this.txtQualifications = new System.Windows.Forms.TextBox();
            this.txtAllowance = new System.Windows.Forms.TextBox();
            this.txtBasic = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtDesignation = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtStaffName = new System.Windows.Forms.TextBox();
            this.tbPersonal = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl = new System.Windows.Forms.Label();
            this.txtSpouse = new System.Windows.Forms.TextBox();
            this.cmbMarital = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbGender = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtDependant = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtReligion = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtNationality = new System.Windows.Forms.TextBox();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.txtMotherName = new System.Windows.Forms.TextBox();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbContact = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtDTAddress4 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtDPAddress4 = new System.Windows.Forms.TextBox();
            this.txtDPhoneNo = new System.Windows.Forms.MaskedTextBox();
            this.txtDTAddress3 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDPAddress3 = new System.Windows.Forms.TextBox();
            this.txtDMobileNo = new System.Windows.Forms.MaskedTextBox();
            this.txtDTAddress2 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtDPAddress2 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtDTAddress1 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtDPAddress1 = new System.Windows.Forms.TextBox();
            this.txtDName = new System.Windows.Forms.TextBox();
            this.txtDRelation = new System.Windows.Forms.TextBox();
            this.txtDEmail = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtNTAddress4 = new System.Windows.Forms.TextBox();
            this.txtNPAddress4 = new System.Windows.Forms.TextBox();
            this.txtNTAddress3 = new System.Windows.Forms.TextBox();
            this.txtNPAddress3 = new System.Windows.Forms.TextBox();
            this.txtNTAddress2 = new System.Windows.Forms.TextBox();
            this.txtNPAddress2 = new System.Windows.Forms.TextBox();
            this.txtNTAddress1 = new System.Windows.Forms.TextBox();
            this.txtNPAddress1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNPhoneNo = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtNMobileNo = new System.Windows.Forms.MaskedTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtNName = new System.Windows.Forms.TextBox();
            this.txtNRelation = new System.Windows.Forms.TextBox();
            this.txtNEmail = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tbDocuments = new System.Windows.Forms.TabPage();
            this.lisList = new System.Windows.Forms.ListView();
            this.Docid = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DocTypeCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DocTypeDetails = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DocName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IssDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ExpDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DocNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.AttchName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Attch = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Added = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Renewal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DocEmail1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DocEmail2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Place = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dtpIssue = new System.Windows.Forms.DateTimePicker();
            this.dtpExpiry = new System.Windows.Forms.DateTimePicker();
            this.txtAttachment = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtPlace = new System.Windows.Forms.TextBox();
            this.txtDocumentType = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtDocumentName = new System.Windows.Forms.TextBox();
            this.lblProduct = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.lblTaxPer = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtDocumentNo = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lblEmail2 = new System.Windows.Forms.Label();
            this.lblEmail1 = new System.Windows.Forms.Label();
            this.txtEmail2 = new System.Windows.Forms.TextBox();
            this.txtEmail1 = new System.Windows.Forms.TextBox();
            this.chkRenewal = new System.Windows.Forms.CheckBox();
            this.btnAddDoc = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnCancelEmp = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.lblItemCount = new System.Windows.Forms.Label();
            this.btnBrowseFolder = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl1.SuspendLayout();
            this.tbEmployee.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tbPersonal.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tbContact.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tbDocuments.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tbEmployee);
            this.tabControl1.Controls.Add(this.tbPersonal);
            this.tabControl1.Controls.Add(this.tbContact);
            this.tabControl1.Controls.Add(this.tbDocuments);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(67, 101);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1055, 622);
            this.tabControl1.TabIndex = 0;
            // 
            // tbEmployee
            // 
            this.tbEmployee.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbEmployee.Controls.Add(this.groupBox6);
            this.tbEmployee.Controls.Add(this.groupBox1);
            this.tbEmployee.Location = new System.Drawing.Point(4, 25);
            this.tbEmployee.Name = "tbEmployee";
            this.tbEmployee.Padding = new System.Windows.Forms.Padding(3);
            this.tbEmployee.Size = new System.Drawing.Size(1047, 593);
            this.tbEmployee.TabIndex = 0;
            this.tbEmployee.Text = "Employee Details";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtRemarks);
            this.groupBox6.Controls.Add(this.Label1);
            this.groupBox6.Location = new System.Drawing.Point(268, 467);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(562, 64);
            this.groupBox6.TabIndex = 44;
            this.groupBox6.TabStop = false;
            this.groupBox6.Visible = false;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(165, 9);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(247, 49);
            this.txtRemarks.TabIndex = 2;
            this.txtRemarks.Text = "";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(10, 18);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(62, 18);
            this.Label1.TabIndex = 43;
            this.Label1.Text = "Remarks";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.Browse);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cmbDrvLicense);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dtpDOA);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.dtpDOJ);
            this.groupBox1.Controls.Add(this.txtPhoneNo);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.txtMobileNo);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtGrade);
            this.groupBox1.Controls.Add(this.txtStaffID);
            this.groupBox1.Controls.Add(this.Label13);
            this.groupBox1.Controls.Add(this.txtDepartment);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.Label9);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Label3);
            this.groupBox1.Controls.Add(this.txtYOP);
            this.groupBox1.Controls.Add(this.txtPrevEmp);
            this.groupBox1.Controls.Add(this.txtQualifications);
            this.groupBox1.Controls.Add(this.txtAllowance);
            this.groupBox1.Controls.Add(this.txtBasic);
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.txtDesignation);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Controls.Add(this.Label2);
            this.groupBox1.Controls.Add(this.txtStaffName);
            this.groupBox1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.DimGray;
            this.groupBox1.Location = new System.Drawing.Point(268, 39);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Size = new System.Drawing.Size(562, 422);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.txtLicenseNo);
            this.panel2.Location = new System.Drawing.Point(244, 300);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(304, 33);
            this.panel2.TabIndex = 121;
            this.panel2.Visible = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(5, 7);
            this.label45.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(108, 18);
            this.label45.TabIndex = 107;
            this.label45.Text = "License Number";
            // 
            // txtLicenseNo
            // 
            this.txtLicenseNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLicenseNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLicenseNo.Location = new System.Drawing.Point(118, 6);
            this.txtLicenseNo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtLicenseNo.Name = "txtLicenseNo";
            this.txtLicenseNo.Size = new System.Drawing.Size(180, 22);
            this.txtLicenseNo.TabIndex = 13;
            // 
            // Browse
            // 
            this.Browse.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Browse.ForeColor = System.Drawing.Color.Black;
            this.Browse.Location = new System.Drawing.Point(393, 247);
            this.Browse.Name = "Browse";
            this.Browse.Size = new System.Drawing.Size(155, 23);
            this.Browse.TabIndex = 14;
            this.Browse.Text = "Browse...";
            this.Browse.UseVisualStyleBackColor = true;
            this.Browse.Click += new System.EventHandler(this.Browse_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(326, 25);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 18);
            this.label10.TabIndex = 80;
            this.label10.Text = "Date of Arrival";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(393, 107);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(155, 127);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 119;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(10, 306);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 18);
            this.label6.TabIndex = 112;
            this.label6.Text = "Driving License";
            // 
            // cmbDrvLicense
            // 
            this.cmbDrvLicense.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDrvLicense.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDrvLicense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDrvLicense.FormattingEnabled = true;
            this.cmbDrvLicense.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbDrvLicense.Location = new System.Drawing.Point(165, 303);
            this.cmbDrvLicense.Name = "cmbDrvLicense";
            this.cmbDrvLicense.Size = new System.Drawing.Size(51, 26);
            this.cmbDrvLicense.TabIndex = 12;
            this.cmbDrvLicense.SelectedIndexChanged += new System.EventHandler(this.cmbDrvLicense_SelectedIndexChanged);
            this.cmbDrvLicense.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDrvLicense_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(10, 27);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 18);
            this.label5.TabIndex = 80;
            this.label5.Text = "Date of Joining";
            // 
            // dtpDOA
            // 
            this.dtpDOA.CustomFormat = "dd/MMM/yyyy";
            this.dtpDOA.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDOA.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOA.Location = new System.Drawing.Point(435, 20);
            this.dtpDOA.Name = "dtpDOA";
            this.dtpDOA.Size = new System.Drawing.Size(113, 24);
            this.dtpDOA.TabIndex = 2;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(10, 365);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(52, 18);
            this.label34.TabIndex = 117;
            this.label34.Text = "Mobile";
            // 
            // dtpDOJ
            // 
            this.dtpDOJ.CustomFormat = "dd/MMM/yyyy";
            this.dtpDOJ.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDOJ.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOJ.Location = new System.Drawing.Point(165, 21);
            this.dtpDOJ.Name = "dtpDOJ";
            this.dtpDOJ.Size = new System.Drawing.Size(112, 24);
            this.dtpDOJ.TabIndex = 1;
            // 
            // txtPhoneNo
            // 
            this.txtPhoneNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo.Location = new System.Drawing.Point(165, 335);
            this.txtPhoneNo.Mask = "000000000000000";
            this.txtPhoneNo.Name = "txtPhoneNo";
            this.txtPhoneNo.Size = new System.Drawing.Size(139, 22);
            this.txtPhoneNo.TabIndex = 14;
            this.txtPhoneNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhoneNo_KeyPress);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(10, 337);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(66, 18);
            this.label35.TabIndex = 116;
            this.label35.Text = "Phone no";
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMobileNo.Location = new System.Drawing.Point(165, 363);
            this.txtMobileNo.Mask = "000000000000000";
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(139, 22);
            this.txtMobileNo.TabIndex = 15;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(10, 393);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(43, 18);
            this.label36.TabIndex = 118;
            this.label36.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(165, 391);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(211, 22);
            this.txtEmail.TabIndex = 16;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(10, 109);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 18);
            this.label14.TabIndex = 91;
            this.label14.Text = "Department";
            // 
            // txtGrade
            // 
            this.txtGrade.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGrade.Location = new System.Drawing.Point(435, 52);
            this.txtGrade.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtGrade.Name = "txtGrade";
            this.txtGrade.Size = new System.Drawing.Size(57, 22);
            this.txtGrade.TabIndex = 3;
            this.txtGrade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGrade_KeyDown);
            this.txtGrade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGrade_KeyPress);
            // 
            // txtStaffID
            // 
            this.txtStaffID.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStaffID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStaffID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStaffID.Location = new System.Drawing.Point(165, 53);
            this.txtStaffID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtStaffID.Name = "txtStaffID";
            this.txtStaffID.Size = new System.Drawing.Size(93, 22);
            this.txtStaffID.TabIndex = 3;
            this.txtStaffID.TabStop = false;
            this.txtStaffID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStaffID_KeyDown);
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.ForeColor = System.Drawing.Color.Black;
            this.Label13.Location = new System.Drawing.Point(10, 247);
            this.Label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(120, 18);
            this.Label13.TabIndex = 81;
            this.Label13.Text = "Year of Experience";
            // 
            // txtDepartment
            // 
            this.txtDepartment.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDepartment.Location = new System.Drawing.Point(165, 107);
            this.txtDepartment.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDepartment.Name = "txtDepartment";
            this.txtDepartment.Size = new System.Drawing.Size(216, 22);
            this.txtDepartment.TabIndex = 5;
            this.txtDepartment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDepartment_KeyDown);
            this.txtDepartment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDepartment_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(10, 278);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 18);
            this.label11.TabIndex = 79;
            this.label11.Text = "Previous Employer";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.ForeColor = System.Drawing.Color.Black;
            this.Label9.Location = new System.Drawing.Point(10, 136);
            this.Label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(93, 18);
            this.Label9.TabIndex = 79;
            this.Label9.Text = "Qualifications";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(10, 218);
            this.label42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(73, 18);
            this.label42.TabIndex = 77;
            this.label42.Text = "Allowance";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(10, 191);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 18);
            this.label4.TabIndex = 77;
            this.label4.Text = "Basic";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.Color.Black;
            this.Label3.Location = new System.Drawing.Point(10, 164);
            this.Label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(83, 18);
            this.Label3.TabIndex = 77;
            this.Label3.Text = "Designation";
            // 
            // txtYOP
            // 
            this.txtYOP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtYOP.Location = new System.Drawing.Point(165, 245);
            this.txtYOP.Name = "txtYOP";
            this.txtYOP.Size = new System.Drawing.Size(34, 22);
            this.txtYOP.TabIndex = 10;
            this.txtYOP.Text = "0";
            this.txtYOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYOP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYOP_KeyPress);
            // 
            // txtPrevEmp
            // 
            this.txtPrevEmp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrevEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrevEmp.Location = new System.Drawing.Point(165, 276);
            this.txtPrevEmp.Name = "txtPrevEmp";
            this.txtPrevEmp.Size = new System.Drawing.Size(383, 22);
            this.txtPrevEmp.TabIndex = 11;
            // 
            // txtQualifications
            // 
            this.txtQualifications.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtQualifications.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQualifications.Location = new System.Drawing.Point(165, 134);
            this.txtQualifications.Name = "txtQualifications";
            this.txtQualifications.Size = new System.Drawing.Size(216, 22);
            this.txtQualifications.TabIndex = 6;
            this.txtQualifications.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtQualifications_KeyDown);
            this.txtQualifications.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQualifications_KeyPress);
            // 
            // txtAllowance
            // 
            this.txtAllowance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAllowance.Location = new System.Drawing.Point(165, 216);
            this.txtAllowance.Name = "txtAllowance";
            this.txtAllowance.Size = new System.Drawing.Size(93, 22);
            this.txtAllowance.TabIndex = 9;
            this.txtAllowance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAllowance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalary_KeyPress);
            // 
            // txtBasic
            // 
            this.txtBasic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBasic.Location = new System.Drawing.Point(165, 189);
            this.txtBasic.Name = "txtBasic";
            this.txtBasic.Size = new System.Drawing.Size(93, 22);
            this.txtBasic.TabIndex = 8;
            this.txtBasic.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBasic.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalary_KeyPress);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(326, 52);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(46, 18);
            this.label43.TabIndex = 43;
            this.label43.Text = "Grade";
            // 
            // txtDesignation
            // 
            this.txtDesignation.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesignation.Location = new System.Drawing.Point(165, 162);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.Size = new System.Drawing.Size(216, 22);
            this.txtDesignation.TabIndex = 7;
            this.txtDesignation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDesignation_KeyDown);
            this.txtDesignation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDesignation_KeyPress);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(10, 55);
            this.label44.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(55, 18);
            this.label44.TabIndex = 44;
            this.label44.Text = "Staff ID";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(10, 82);
            this.Label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(76, 18);
            this.Label2.TabIndex = 44;
            this.Label2.Text = "Staff Name";
            // 
            // txtStaffName
            // 
            this.txtStaffName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtStaffName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStaffName.Location = new System.Drawing.Point(165, 80);
            this.txtStaffName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtStaffName.Name = "txtStaffName";
            this.txtStaffName.Size = new System.Drawing.Size(383, 22);
            this.txtStaffName.TabIndex = 4;
            // 
            // tbPersonal
            // 
            this.tbPersonal.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbPersonal.Controls.Add(this.groupBox3);
            this.tbPersonal.Location = new System.Drawing.Point(4, 25);
            this.tbPersonal.Name = "tbPersonal";
            this.tbPersonal.Padding = new System.Windows.Forms.Padding(3);
            this.tbPersonal.Size = new System.Drawing.Size(1047, 593);
            this.tbPersonal.TabIndex = 1;
            this.tbPersonal.Text = "Personal Details";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.panel1);
            this.groupBox3.Controls.Add(this.cmbMarital);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.cmbGender);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.txtDependant);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.txtReligion);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.txtAge);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txtNationality);
            this.groupBox3.Controls.Add(this.dtpDOB);
            this.groupBox3.Controls.Add(this.txtMotherName);
            this.groupBox3.Controls.Add(this.txtFatherName);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(247, 107);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(576, 350);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbl);
            this.panel1.Controls.Add(this.txtSpouse);
            this.panel1.Location = new System.Drawing.Point(257, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(298, 38);
            this.panel1.TabIndex = 1;
            this.panel1.Visible = false;
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.ForeColor = System.Drawing.Color.Black;
            this.lbl.Location = new System.Drawing.Point(5, 9);
            this.lbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(93, 18);
            this.lbl.TabIndex = 107;
            this.lbl.Text = "Spouse Name";
            // 
            // txtSpouse
            // 
            this.txtSpouse.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSpouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpouse.Location = new System.Drawing.Point(111, 7);
            this.txtSpouse.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtSpouse.Name = "txtSpouse";
            this.txtSpouse.Size = new System.Drawing.Size(180, 22);
            this.txtSpouse.TabIndex = 18;
            // 
            // cmbMarital
            // 
            this.cmbMarital.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbMarital.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbMarital.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cmbMarital.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMarital.FormattingEnabled = true;
            this.cmbMarital.Items.AddRange(new object[] {
            "Single",
            "Married"});
            this.cmbMarital.Location = new System.Drawing.Point(166, 110);
            this.cmbMarital.Name = "cmbMarital";
            this.cmbMarital.Size = new System.Drawing.Size(74, 21);
            this.cmbMarital.TabIndex = 17;
            this.cmbMarital.SelectedIndexChanged += new System.EventHandler(this.cmbMarital_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(13, 110);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 18);
            this.label8.TabIndex = 110;
            this.label8.Text = "Marital Status";
            // 
            // cmbGender
            // 
            this.cmbGender.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbGender.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGender.FormattingEnabled = true;
            this.cmbGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cmbGender.Location = new System.Drawing.Point(166, 73);
            this.cmbGender.Name = "cmbGender";
            this.cmbGender.Size = new System.Drawing.Size(74, 21);
            this.cmbGender.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(13, 73);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 18);
            this.label16.TabIndex = 110;
            this.label16.Text = "Gender";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(13, 33);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(39, 18);
            this.label24.TabIndex = 108;
            this.label24.Text = "DOB";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(13, 232);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(124, 18);
            this.label18.TabIndex = 107;
            this.label18.Text = "No. of Dependants";
            // 
            // txtDependant
            // 
            this.txtDependant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDependant.Location = new System.Drawing.Point(166, 230);
            this.txtDependant.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDependant.Name = "txtDependant";
            this.txtDependant.Size = new System.Drawing.Size(153, 22);
            this.txtDependant.TabIndex = 21;
            this.txtDependant.Text = "0";
            this.txtDependant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDependant_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(13, 195);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 18);
            this.label17.TabIndex = 107;
            this.label17.Text = "Religion";
            // 
            // txtReligion
            // 
            this.txtReligion.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtReligion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReligion.Location = new System.Drawing.Point(166, 193);
            this.txtReligion.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtReligion.Name = "txtReligion";
            this.txtReligion.Size = new System.Drawing.Size(153, 22);
            this.txtReligion.TabIndex = 20;
            this.txtReligion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReligion_KeyDown);
            this.txtReligion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReligion_KeyPress);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(327, 33);
            this.label40.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(33, 18);
            this.label40.TabIndex = 107;
            this.label40.Text = "Age";
            // 
            // txtAge
            // 
            this.txtAge.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAge.Location = new System.Drawing.Point(364, 31);
            this.txtAge.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtAge.Name = "txtAge";
            this.txtAge.ReadOnly = true;
            this.txtAge.Size = new System.Drawing.Size(36, 22);
            this.txtAge.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(13, 155);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 18);
            this.label15.TabIndex = 107;
            this.label15.Text = "Nationality";
            // 
            // txtNationality
            // 
            this.txtNationality.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtNationality.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNationality.Location = new System.Drawing.Point(166, 153);
            this.txtNationality.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtNationality.Name = "txtNationality";
            this.txtNationality.Size = new System.Drawing.Size(153, 22);
            this.txtNationality.TabIndex = 19;
            this.txtNationality.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNationality_KeyDown);
            this.txtNationality.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNationality_KeyPress);
            // 
            // dtpDOB
            // 
            this.dtpDOB.CustomFormat = "dd/MMM/yyyy";
            this.dtpDOB.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.Location = new System.Drawing.Point(166, 28);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(141, 24);
            this.dtpDOB.TabIndex = 15;
            this.dtpDOB.ValueChanged += new System.EventHandler(this.dtpDOB_ValueChanged);
            // 
            // txtMotherName
            // 
            this.txtMotherName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMotherName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMotherName.Location = new System.Drawing.Point(166, 308);
            this.txtMotherName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtMotherName.Name = "txtMotherName";
            this.txtMotherName.Size = new System.Drawing.Size(254, 22);
            this.txtMotherName.TabIndex = 23;
            // 
            // txtFatherName
            // 
            this.txtFatherName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFatherName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFatherName.Location = new System.Drawing.Point(166, 267);
            this.txtFatherName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(254, 22);
            this.txtFatherName.TabIndex = 22;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(13, 310);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(102, 18);
            this.label19.TabIndex = 106;
            this.label19.Text = "Mother\'s Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(13, 269);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 18);
            this.label7.TabIndex = 106;
            this.label7.Text = "Father\'s Name";
            // 
            // tbContact
            // 
            this.tbContact.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbContact.Controls.Add(this.groupBox5);
            this.tbContact.Controls.Add(this.groupBox4);
            this.tbContact.Location = new System.Drawing.Point(4, 25);
            this.tbContact.Name = "tbContact";
            this.tbContact.Size = new System.Drawing.Size(1047, 593);
            this.tbContact.TabIndex = 2;
            this.tbContact.Text = "Contact Details";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtDTAddress4);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.txtDPAddress4);
            this.groupBox5.Controls.Add(this.txtDPhoneNo);
            this.groupBox5.Controls.Add(this.txtDTAddress3);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.txtDPAddress3);
            this.groupBox5.Controls.Add(this.txtDMobileNo);
            this.groupBox5.Controls.Add(this.txtDTAddress2);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.txtDPAddress2);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.txtDTAddress1);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.txtDPAddress1);
            this.groupBox5.Controls.Add(this.txtDName);
            this.groupBox5.Controls.Add(this.txtDRelation);
            this.groupBox5.Controls.Add(this.txtDEmail);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Location = new System.Drawing.Point(526, 60);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(492, 478);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Dubai Contact Details";
            // 
            // txtDTAddress4
            // 
            this.txtDTAddress4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDTAddress4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDTAddress4.Location = new System.Drawing.Point(171, 441);
            this.txtDTAddress4.Name = "txtDTAddress4";
            this.txtDTAddress4.Size = new System.Drawing.Size(273, 22);
            this.txtDTAddress4.TabIndex = 49;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(15, 148);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 18);
            this.label25.TabIndex = 87;
            this.label25.Text = "Mobile";
            // 
            // txtDPAddress4
            // 
            this.txtDPAddress4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDPAddress4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDPAddress4.Location = new System.Drawing.Point(171, 315);
            this.txtDPAddress4.Name = "txtDPAddress4";
            this.txtDPAddress4.Size = new System.Drawing.Size(273, 22);
            this.txtDPAddress4.TabIndex = 45;
            // 
            // txtDPhoneNo
            // 
            this.txtDPhoneNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDPhoneNo.Location = new System.Drawing.Point(171, 104);
            this.txtDPhoneNo.Mask = "000000000000000";
            this.txtDPhoneNo.Name = "txtDPhoneNo";
            this.txtDPhoneNo.Size = new System.Drawing.Size(153, 22);
            this.txtDPhoneNo.TabIndex = 39;
            // 
            // txtDTAddress3
            // 
            this.txtDTAddress3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDTAddress3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDTAddress3.Location = new System.Drawing.Point(171, 413);
            this.txtDTAddress3.Name = "txtDTAddress3";
            this.txtDTAddress3.Size = new System.Drawing.Size(273, 22);
            this.txtDTAddress3.TabIndex = 48;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(15, 106);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(66, 18);
            this.label26.TabIndex = 86;
            this.label26.Text = "Phone no";
            // 
            // txtDPAddress3
            // 
            this.txtDPAddress3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDPAddress3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDPAddress3.Location = new System.Drawing.Point(171, 287);
            this.txtDPAddress3.Name = "txtDPAddress3";
            this.txtDPAddress3.Size = new System.Drawing.Size(273, 22);
            this.txtDPAddress3.TabIndex = 44;
            // 
            // txtDMobileNo
            // 
            this.txtDMobileNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDMobileNo.Location = new System.Drawing.Point(171, 146);
            this.txtDMobileNo.Mask = "000000000000000";
            this.txtDMobileNo.Name = "txtDMobileNo";
            this.txtDMobileNo.Size = new System.Drawing.Size(153, 22);
            this.txtDMobileNo.TabIndex = 40;
            // 
            // txtDTAddress2
            // 
            this.txtDTAddress2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDTAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDTAddress2.Location = new System.Drawing.Point(171, 385);
            this.txtDTAddress2.Name = "txtDTAddress2";
            this.txtDTAddress2.Size = new System.Drawing.Size(273, 22);
            this.txtDTAddress2.TabIndex = 47;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(15, 33);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(45, 18);
            this.label27.TabIndex = 88;
            this.label27.Text = "Name";
            // 
            // txtDPAddress2
            // 
            this.txtDPAddress2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDPAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDPAddress2.Location = new System.Drawing.Point(171, 259);
            this.txtDPAddress2.Name = "txtDPAddress2";
            this.txtDPAddress2.Size = new System.Drawing.Size(273, 22);
            this.txtDPAddress2.TabIndex = 43;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(15, 66);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(85, 18);
            this.label28.TabIndex = 88;
            this.label28.Text = "Relationship";
            // 
            // txtDTAddress1
            // 
            this.txtDTAddress1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDTAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDTAddress1.Location = new System.Drawing.Point(171, 357);
            this.txtDTAddress1.Name = "txtDTAddress1";
            this.txtDTAddress1.Size = new System.Drawing.Size(273, 22);
            this.txtDTAddress1.TabIndex = 46;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(15, 188);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 18);
            this.label29.TabIndex = 88;
            this.label29.Text = "Email";
            // 
            // txtDPAddress1
            // 
            this.txtDPAddress1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDPAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDPAddress1.Location = new System.Drawing.Point(171, 231);
            this.txtDPAddress1.Name = "txtDPAddress1";
            this.txtDPAddress1.Size = new System.Drawing.Size(273, 22);
            this.txtDPAddress1.TabIndex = 42;
            // 
            // txtDName
            // 
            this.txtDName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDName.Location = new System.Drawing.Point(171, 31);
            this.txtDName.Name = "txtDName";
            this.txtDName.Size = new System.Drawing.Size(211, 22);
            this.txtDName.TabIndex = 37;
            // 
            // txtDRelation
            // 
            this.txtDRelation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDRelation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDRelation.Location = new System.Drawing.Point(171, 65);
            this.txtDRelation.Name = "txtDRelation";
            this.txtDRelation.Size = new System.Drawing.Size(211, 22);
            this.txtDRelation.TabIndex = 38;
            // 
            // txtDEmail
            // 
            this.txtDEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDEmail.Location = new System.Drawing.Point(171, 186);
            this.txtDEmail.Name = "txtDEmail";
            this.txtDEmail.Size = new System.Drawing.Size(211, 22);
            this.txtDEmail.TabIndex = 41;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(15, 233);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(127, 18);
            this.label32.TabIndex = 84;
            this.label32.Text = "Permanent Address";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(15, 359);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(128, 18);
            this.label33.TabIndex = 85;
            this.label33.Text = "Temporary Address";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtNTAddress4);
            this.groupBox4.Controls.Add(this.txtNPAddress4);
            this.groupBox4.Controls.Add(this.txtNTAddress3);
            this.groupBox4.Controls.Add(this.txtNPAddress3);
            this.groupBox4.Controls.Add(this.txtNTAddress2);
            this.groupBox4.Controls.Add(this.txtNPAddress2);
            this.groupBox4.Controls.Add(this.txtNTAddress1);
            this.groupBox4.Controls.Add(this.txtNPAddress1);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.txtNPhoneNo);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.txtNMobileNo);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.txtNName);
            this.groupBox4.Controls.Add(this.txtNRelation);
            this.groupBox4.Controls.Add(this.txtNEmail);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Location = new System.Drawing.Point(28, 60);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(492, 478);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Native Contact Details";
            // 
            // txtNTAddress4
            // 
            this.txtNTAddress4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNTAddress4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNTAddress4.Location = new System.Drawing.Point(171, 441);
            this.txtNTAddress4.Name = "txtNTAddress4";
            this.txtNTAddress4.Size = new System.Drawing.Size(273, 22);
            this.txtNTAddress4.TabIndex = 36;
            // 
            // txtNPAddress4
            // 
            this.txtNPAddress4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNPAddress4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPAddress4.Location = new System.Drawing.Point(171, 315);
            this.txtNPAddress4.Name = "txtNPAddress4";
            this.txtNPAddress4.Size = new System.Drawing.Size(273, 22);
            this.txtNPAddress4.TabIndex = 32;
            // 
            // txtNTAddress3
            // 
            this.txtNTAddress3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNTAddress3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNTAddress3.Location = new System.Drawing.Point(171, 413);
            this.txtNTAddress3.Name = "txtNTAddress3";
            this.txtNTAddress3.Size = new System.Drawing.Size(273, 22);
            this.txtNTAddress3.TabIndex = 35;
            // 
            // txtNPAddress3
            // 
            this.txtNPAddress3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNPAddress3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPAddress3.Location = new System.Drawing.Point(171, 287);
            this.txtNPAddress3.Name = "txtNPAddress3";
            this.txtNPAddress3.Size = new System.Drawing.Size(273, 22);
            this.txtNPAddress3.TabIndex = 31;
            // 
            // txtNTAddress2
            // 
            this.txtNTAddress2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNTAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNTAddress2.Location = new System.Drawing.Point(171, 385);
            this.txtNTAddress2.Name = "txtNTAddress2";
            this.txtNTAddress2.Size = new System.Drawing.Size(273, 22);
            this.txtNTAddress2.TabIndex = 34;
            // 
            // txtNPAddress2
            // 
            this.txtNPAddress2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNPAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPAddress2.Location = new System.Drawing.Point(171, 259);
            this.txtNPAddress2.Name = "txtNPAddress2";
            this.txtNPAddress2.Size = new System.Drawing.Size(273, 22);
            this.txtNPAddress2.TabIndex = 30;
            // 
            // txtNTAddress1
            // 
            this.txtNTAddress1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNTAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNTAddress1.Location = new System.Drawing.Point(171, 357);
            this.txtNTAddress1.Name = "txtNTAddress1";
            this.txtNTAddress1.Size = new System.Drawing.Size(273, 22);
            this.txtNTAddress1.TabIndex = 33;
            // 
            // txtNPAddress1
            // 
            this.txtNPAddress1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNPAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPAddress1.Location = new System.Drawing.Point(171, 231);
            this.txtNPAddress1.Name = "txtNPAddress1";
            this.txtNPAddress1.Size = new System.Drawing.Size(273, 22);
            this.txtNPAddress1.TabIndex = 29;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(15, 148);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 18);
            this.label12.TabIndex = 87;
            this.label12.Text = "Mobile";
            // 
            // txtNPhoneNo
            // 
            this.txtNPhoneNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPhoneNo.Location = new System.Drawing.Point(171, 104);
            this.txtNPhoneNo.Mask = "000000000000000";
            this.txtNPhoneNo.Name = "txtNPhoneNo";
            this.txtNPhoneNo.Size = new System.Drawing.Size(153, 22);
            this.txtNPhoneNo.TabIndex = 26;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(15, 106);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(66, 18);
            this.label20.TabIndex = 86;
            this.label20.Text = "Phone no";
            // 
            // txtNMobileNo
            // 
            this.txtNMobileNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNMobileNo.Location = new System.Drawing.Point(171, 146);
            this.txtNMobileNo.Mask = "000000000000000";
            this.txtNMobileNo.Name = "txtNMobileNo";
            this.txtNMobileNo.Size = new System.Drawing.Size(153, 22);
            this.txtNMobileNo.TabIndex = 27;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(15, 33);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 18);
            this.label31.TabIndex = 88;
            this.label31.Text = "Name";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(15, 66);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(85, 18);
            this.label30.TabIndex = 88;
            this.label30.Text = "Relationship";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(15, 188);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 18);
            this.label21.TabIndex = 88;
            this.label21.Text = "Email";
            // 
            // txtNName
            // 
            this.txtNName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNName.Location = new System.Drawing.Point(171, 31);
            this.txtNName.Name = "txtNName";
            this.txtNName.Size = new System.Drawing.Size(211, 22);
            this.txtNName.TabIndex = 24;
            // 
            // txtNRelation
            // 
            this.txtNRelation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNRelation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNRelation.Location = new System.Drawing.Point(171, 65);
            this.txtNRelation.Name = "txtNRelation";
            this.txtNRelation.Size = new System.Drawing.Size(211, 22);
            this.txtNRelation.TabIndex = 25;
            // 
            // txtNEmail
            // 
            this.txtNEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNEmail.Location = new System.Drawing.Point(171, 186);
            this.txtNEmail.Name = "txtNEmail";
            this.txtNEmail.Size = new System.Drawing.Size(211, 22);
            this.txtNEmail.TabIndex = 28;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(15, 233);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(127, 18);
            this.label22.TabIndex = 84;
            this.label22.Text = "Permanent Address";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(15, 359);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(128, 18);
            this.label23.TabIndex = 85;
            this.label23.Text = "Temporary Address";
            // 
            // tbDocuments
            // 
            this.tbDocuments.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbDocuments.Controls.Add(this.lisList);
            this.tbDocuments.Controls.Add(this.groupBox7);
            this.tbDocuments.Controls.Add(this.groupBox9);
            this.tbDocuments.Location = new System.Drawing.Point(4, 25);
            this.tbDocuments.Name = "tbDocuments";
            this.tbDocuments.Size = new System.Drawing.Size(1047, 593);
            this.tbDocuments.TabIndex = 3;
            this.tbDocuments.Text = "Documents";
            // 
            // lisList
            // 
            this.lisList.BackColor = System.Drawing.SystemColors.Window;
            this.lisList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Docid,
            this.DocTypeCode,
            this.DocTypeDetails,
            this.DocName,
            this.IssDate,
            this.ExpDate,
            this.DocNo,
            this.AttchName,
            this.Attch,
            this.Added,
            this.Renewal,
            this.DocEmail1,
            this.DocEmail2,
            this.Place});
            this.lisList.FullRowSelect = true;
            this.lisList.GridLines = true;
            this.lisList.Location = new System.Drawing.Point(44, 232);
            this.lisList.Name = "lisList";
            this.lisList.Size = new System.Drawing.Size(956, 315);
            this.lisList.TabIndex = 27;
            this.lisList.TabStop = false;
            this.lisList.UseCompatibleStateImageBehavior = false;
            this.lisList.View = System.Windows.Forms.View.Details;
            this.lisList.SelectedIndexChanged += new System.EventHandler(this.lisList_SelectedIndexChanged);
            this.lisList.DoubleClick += new System.EventHandler(this.lisList_DoubleClick);
            this.lisList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lisList_KeyDown);
            // 
            // Docid
            // 
            this.Docid.Text = "ID";
            this.Docid.Width = 0;
            // 
            // DocTypeCode
            // 
            this.DocTypeCode.Width = 0;
            // 
            // DocTypeDetails
            // 
            this.DocTypeDetails.Text = "Doucument Type";
            this.DocTypeDetails.Width = 150;
            // 
            // DocName
            // 
            this.DocName.Text = "Document Name";
            this.DocName.Width = 300;
            // 
            // IssDate
            // 
            this.IssDate.Text = "Issue Date";
            this.IssDate.Width = 100;
            // 
            // ExpDate
            // 
            this.ExpDate.Text = "Expiry Date";
            this.ExpDate.Width = 100;
            // 
            // DocNo
            // 
            this.DocNo.Text = "Document No";
            this.DocNo.Width = 300;
            // 
            // AttchName
            // 
            this.AttchName.Width = 0;
            // 
            // Attch
            // 
            this.Attch.Text = "Attachment";
            this.Attch.Width = 0;
            // 
            // Added
            // 
            this.Added.Width = 0;
            // 
            // Renewal
            // 
            this.Renewal.Width = 0;
            // 
            // DocEmail1
            // 
            this.DocEmail1.Width = 0;
            // 
            // DocEmail2
            // 
            this.DocEmail2.Width = 0;
            // 
            // Place
            // 
            this.Place.Width = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox7.Controls.Add(this.dtpIssue);
            this.groupBox7.Controls.Add(this.dtpExpiry);
            this.groupBox7.Controls.Add(this.txtAttachment);
            this.groupBox7.Controls.Add(this.btnBrowse);
            this.groupBox7.Controls.Add(this.txtPlace);
            this.groupBox7.Controls.Add(this.txtDocumentType);
            this.groupBox7.Controls.Add(this.label37);
            this.groupBox7.Controls.Add(this.txtDocumentName);
            this.groupBox7.Controls.Add(this.lblProduct);
            this.groupBox7.Controls.Add(this.label38);
            this.groupBox7.Controls.Add(this.label41);
            this.groupBox7.Controls.Add(this.lblTaxPer);
            this.groupBox7.Controls.Add(this.label39);
            this.groupBox7.Controls.Add(this.txtDocumentNo);
            this.groupBox7.Location = new System.Drawing.Point(44, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(956, 158);
            this.groupBox7.TabIndex = 23;
            this.groupBox7.TabStop = false;
            // 
            // dtpIssue
            // 
            this.dtpIssue.CustomFormat = "dd/MM/yyyy";
            this.dtpIssue.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssue.Location = new System.Drawing.Point(97, 72);
            this.dtpIssue.Name = "dtpIssue";
            this.dtpIssue.Size = new System.Drawing.Size(100, 20);
            this.dtpIssue.TabIndex = 52;
            // 
            // dtpExpiry
            // 
            this.dtpExpiry.CustomFormat = "dd/MM/yyyy";
            this.dtpExpiry.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiry.Location = new System.Drawing.Point(304, 72);
            this.dtpExpiry.Name = "dtpExpiry";
            this.dtpExpiry.Size = new System.Drawing.Size(100, 20);
            this.dtpExpiry.TabIndex = 53;
            // 
            // txtAttachment
            // 
            this.txtAttachment.BackColor = System.Drawing.Color.White;
            this.txtAttachment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAttachment.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAttachment.ForeColor = System.Drawing.Color.Black;
            this.txtAttachment.Location = new System.Drawing.Point(304, 118);
            this.txtAttachment.MaxLength = 30;
            this.txtAttachment.Name = "txtAttachment";
            this.txtAttachment.ReadOnly = true;
            this.txtAttachment.Size = new System.Drawing.Size(305, 23);
            this.txtAttachment.TabIndex = 43;
            this.txtAttachment.TabStop = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(615, 117);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(102, 23);
            this.btnBrowse.TabIndex = 56;
            this.btnBrowse.Text = "Browse File";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtPlace
            // 
            this.txtPlace.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPlace.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlace.Location = new System.Drawing.Point(137, 120);
            this.txtPlace.Name = "txtPlace";
            this.txtPlace.Size = new System.Drawing.Size(147, 20);
            this.txtPlace.TabIndex = 55;
            // 
            // txtDocumentType
            // 
            this.txtDocumentType.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtDocumentType.Location = new System.Drawing.Point(137, 19);
            this.txtDocumentType.Name = "txtDocumentType";
            this.txtDocumentType.Size = new System.Drawing.Size(170, 20);
            this.txtDocumentType.TabIndex = 50;
            this.txtDocumentType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDocumentType_KeyDown);
            this.txtDocumentType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocumentType_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(4, 75);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(87, 16);
            this.label37.TabIndex = 29;
            this.label37.Text = "Isuue Date";
            // 
            // txtDocumentName
            // 
            this.txtDocumentName.BackColor = System.Drawing.Color.White;
            this.txtDocumentName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocumentName.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocumentName.ForeColor = System.Drawing.Color.Black;
            this.txtDocumentName.Location = new System.Drawing.Point(373, 19);
            this.txtDocumentName.MaxLength = 40;
            this.txtDocumentName.Name = "txtDocumentName";
            this.txtDocumentName.Size = new System.Drawing.Size(344, 23);
            this.txtDocumentName.TabIndex = 51;
            // 
            // lblProduct
            // 
            this.lblProduct.AutoSize = true;
            this.lblProduct.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblProduct.Location = new System.Drawing.Point(410, 76);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(105, 16);
            this.lblProduct.TabIndex = 26;
            this.lblProduct.Text = "Document No";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(317, 22);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(50, 16);
            this.label38.TabIndex = 0;
            this.label38.Text = "Name";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label41.Location = new System.Drawing.Point(6, 121);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(109, 16);
            this.label41.TabIndex = 0;
            this.label41.Text = "Place of Issue";
            // 
            // lblTaxPer
            // 
            this.lblTaxPer.AutoSize = true;
            this.lblTaxPer.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblTaxPer.Location = new System.Drawing.Point(206, 75);
            this.lblTaxPer.Name = "lblTaxPer";
            this.lblTaxPer.Size = new System.Drawing.Size(92, 16);
            this.lblTaxPer.TabIndex = 22;
            this.lblTaxPer.Text = "Expiry Date";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label39.Location = new System.Drawing.Point(4, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(121, 16);
            this.label39.TabIndex = 0;
            this.label39.Text = "Document Type";
            // 
            // txtDocumentNo
            // 
            this.txtDocumentNo.BackColor = System.Drawing.Color.White;
            this.txtDocumentNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocumentNo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocumentNo.ForeColor = System.Drawing.Color.Black;
            this.txtDocumentNo.Location = new System.Drawing.Point(521, 72);
            this.txtDocumentNo.MaxLength = 50;
            this.txtDocumentNo.Name = "txtDocumentNo";
            this.txtDocumentNo.Size = new System.Drawing.Size(197, 23);
            this.txtDocumentNo.TabIndex = 54;
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox9.Controls.Add(this.lblEmail2);
            this.groupBox9.Controls.Add(this.lblEmail1);
            this.groupBox9.Controls.Add(this.txtEmail2);
            this.groupBox9.Controls.Add(this.txtEmail1);
            this.groupBox9.Controls.Add(this.chkRenewal);
            this.groupBox9.Controls.Add(this.btnAddDoc);
            this.groupBox9.Location = new System.Drawing.Point(44, 167);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(956, 59);
            this.groupBox9.TabIndex = 25;
            this.groupBox9.TabStop = false;
            // 
            // lblEmail2
            // 
            this.lblEmail2.AutoSize = true;
            this.lblEmail2.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail2.ForeColor = System.Drawing.Color.Black;
            this.lblEmail2.Location = new System.Drawing.Point(485, 21);
            this.lblEmail2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail2.Name = "lblEmail2";
            this.lblEmail2.Size = new System.Drawing.Size(53, 18);
            this.lblEmail2.TabIndex = 120;
            this.lblEmail2.Text = "Email 2";
            this.lblEmail2.Visible = false;
            // 
            // lblEmail1
            // 
            this.lblEmail1.AutoSize = true;
            this.lblEmail1.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail1.ForeColor = System.Drawing.Color.Black;
            this.lblEmail1.Location = new System.Drawing.Point(222, 22);
            this.lblEmail1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail1.Name = "lblEmail1";
            this.lblEmail1.Size = new System.Drawing.Size(53, 18);
            this.lblEmail1.TabIndex = 120;
            this.lblEmail1.Text = "Email 1";
            this.lblEmail1.Visible = false;
            // 
            // txtEmail2
            // 
            this.txtEmail2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtEmail2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail2.Location = new System.Drawing.Point(544, 21);
            this.txtEmail2.Name = "txtEmail2";
            this.txtEmail2.Size = new System.Drawing.Size(174, 22);
            this.txtEmail2.TabIndex = 60;
            this.txtEmail2.Visible = false;
            this.txtEmail2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail2_KeyDown);
            this.txtEmail2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail2_KeyPress);
            // 
            // txtEmail1
            // 
            this.txtEmail1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtEmail1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail1.Location = new System.Drawing.Point(280, 20);
            this.txtEmail1.Name = "txtEmail1";
            this.txtEmail1.Size = new System.Drawing.Size(174, 22);
            this.txtEmail1.TabIndex = 59;
            this.txtEmail1.Visible = false;
            this.txtEmail1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail1_KeyDown);
            this.txtEmail1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail1_KeyPress_1);
            // 
            // chkRenewal
            // 
            this.chkRenewal.AutoSize = true;
            this.chkRenewal.Location = new System.Drawing.Point(108, 24);
            this.chkRenewal.Name = "chkRenewal";
            this.chkRenewal.Size = new System.Drawing.Size(75, 17);
            this.chkRenewal.TabIndex = 58;
            this.chkRenewal.Text = "Renewal";
            this.chkRenewal.UseVisualStyleBackColor = true;
            this.chkRenewal.CheckedChanged += new System.EventHandler(this.chkRenewal_CheckedChanged);
            // 
            // btnAddDoc
            // 
            this.btnAddDoc.Location = new System.Drawing.Point(7, 19);
            this.btnAddDoc.Name = "btnAddDoc";
            this.btnAddDoc.Size = new System.Drawing.Size(75, 23);
            this.btnAddDoc.TabIndex = 57;
            this.btnAddDoc.Text = "ADD";
            this.btnAddDoc.UseVisualStyleBackColor = true;
            this.btnAddDoc.Click += new System.EventHandler(this.btnAddDoc_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnNext);
            this.groupBox2.Controls.Add(this.btnSearch);
            this.groupBox2.Controls.Add(this.btnEdit);
            this.groupBox2.Controls.Add(this.btnCancelEmp);
            this.groupBox2.Controls.Add(this.btnPrevious);
            this.groupBox2.Controls.Add(this.btnPrint);
            this.groupBox2.Controls.Add(this.btnExit);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnAdd);
            this.groupBox2.Location = new System.Drawing.Point(67, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(711, 92);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.GhostWhite;
            this.btnNext.BackgroundImage = global::Xpire.Properties.Resources.next;
            this.btnNext.CausesValidation = false;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnNext.Location = new System.Drawing.Point(170, 13);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(76, 71);
            this.btnNext.TabIndex = 9;
            this.btnNext.Tag = "&Next";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.GhostWhite;
            this.btnSearch.BackgroundImage = global::Xpire.Properties.Resources.search1;
            this.btnSearch.CausesValidation = false;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnSearch.Location = new System.Drawing.Point(20, 13);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(76, 71);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Tag = "&Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.GhostWhite;
            this.btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
            this.btnEdit.CausesValidation = false;
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnEdit.Location = new System.Drawing.Point(320, 13);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(76, 71);
            this.btnEdit.TabIndex = 11;
            this.btnEdit.Tag = "&Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnCancelEmp
            // 
            this.btnCancelEmp.BackColor = System.Drawing.Color.GhostWhite;
            this.btnCancelEmp.BackgroundImage = global::Xpire.Properties.Resources.cancelemp_01;
            this.btnCancelEmp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelEmp.CausesValidation = false;
            this.btnCancelEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelEmp.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnCancelEmp.Location = new System.Drawing.Point(632, 17);
            this.btnCancelEmp.Name = "btnCancelEmp";
            this.btnCancelEmp.Size = new System.Drawing.Size(64, 63);
            this.btnCancelEmp.TabIndex = 13;
            this.btnCancelEmp.Tag = "E&xit";
            this.btnCancelEmp.UseVisualStyleBackColor = false;
            this.btnCancelEmp.Click += new System.EventHandler(this.btnCancelEmp_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.BackColor = System.Drawing.Color.GhostWhite;
            this.btnPrevious.BackgroundImage = global::Xpire.Properties.Resources.previous;
            this.btnPrevious.CausesValidation = false;
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevious.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPrevious.Location = new System.Drawing.Point(95, 13);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(76, 71);
            this.btnPrevious.TabIndex = 8;
            this.btnPrevious.Tag = "&Previous";
            this.btnPrevious.UseVisualStyleBackColor = false;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.GhostWhite;
            this.btnPrint.BackgroundImage = global::Xpire.Properties.Resources.print1;
            this.btnPrint.CausesValidation = false;
            this.btnPrint.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPrint.Location = new System.Drawing.Point(550, 13);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 71);
            this.btnPrint.TabIndex = 13;
            this.btnPrint.Tag = "E&xit";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.GhostWhite;
            this.btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnExit.Location = new System.Drawing.Point(470, 13);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 71);
            this.btnExit.TabIndex = 13;
            this.btnExit.Tag = "E&xit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.GhostWhite;
            this.btnDelete.BackgroundImage = global::Xpire.Properties.Resources.delete;
            this.btnDelete.CausesValidation = false;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnDelete.Location = new System.Drawing.Point(395, 13);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(76, 71);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Tag = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.GhostWhite;
            this.btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
            this.btnAdd.CausesValidation = false;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnAdd.Location = new System.Drawing.Point(245, 13);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(76, 71);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Tag = "&Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            // 
            // lblItemCount
            // 
            this.lblItemCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblItemCount.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemCount.ForeColor = System.Drawing.Color.Navy;
            this.lblItemCount.Location = new System.Drawing.Point(784, 66);
            this.lblItemCount.Name = "lblItemCount";
            this.lblItemCount.Size = new System.Drawing.Size(217, 22);
            this.lblItemCount.TabIndex = 101;
            this.lblItemCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnBrowseFolder
            // 
            this.btnBrowseFolder.Location = new System.Drawing.Point(800, 40);
            this.btnBrowseFolder.Name = "btnBrowseFolder";
            this.btnBrowseFolder.Size = new System.Drawing.Size(101, 23);
            this.btnBrowseFolder.TabIndex = 56;
            this.btnBrowseFolder.Text = "Browse Folder";
            this.btnBrowseFolder.UseVisualStyleBackColor = true;
            this.btnBrowseFolder.Visible = false;
            this.btnBrowseFolder.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // ucEmployeeRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.lblItemCount);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnBrowseFolder);
            this.Name = "ucEmployeeRegistration";
            this.Size = new System.Drawing.Size(1176, 726);
            this.Load += new System.EventHandler(this.ucEmployeeRegistration_Load);
            this.tabControl1.ResumeLayout(false);
            this.tbEmployee.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tbPersonal.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tbContact.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tbDocuments.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        TabControl tabControl1;
        private System.Windows.Forms.TabPage tbEmployee;
        private System.Windows.Forms.TabPage tbPersonal;
        private System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.Button btnNext;
        internal System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.Button btnEdit;
        internal System.Windows.Forms.Button btnPrevious;
        public System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TabPage tbContact;
        private System.Windows.Forms.TabPage tbDocuments;
        internal System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox txtStaffID;
        internal System.Windows.Forms.Label Label13;
        public System.Windows.Forms.TextBox txtDepartment;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label3;
        public System.Windows.Forms.TextBox txtYOP;
        public System.Windows.Forms.TextBox txtQualifications;
        public System.Windows.Forms.TextBox txtDesignation;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label2;
        public System.Windows.Forms.TextBox txtStaffName;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label5;
        public System.Windows.Forms.DateTimePicker dtpDOA;
        public System.Windows.Forms.DateTimePicker dtpDOJ;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtPrevEmp;
        public System.Windows.Forms.TextBox txtBasic;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.ComboBox cmbMarital;
        internal System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox cmbGender;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtDependant;
        internal System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtReligion;
        internal System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox txtNationality;
        public System.Windows.Forms.DateTimePicker dtpDOB;
        public System.Windows.Forms.TextBox txtFatherName;
        internal System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cmbDrvLicense;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label lbl;
        public System.Windows.Forms.TextBox txtSpouse;
        public System.Windows.Forms.TextBox txtMotherName;
        internal System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox4;
        internal System.Windows.Forms.Label label34;
        public System.Windows.Forms.MaskedTextBox txtPhoneNo;
        internal System.Windows.Forms.Label label35;
        public System.Windows.Forms.MaskedTextBox txtMobileNo;
        internal System.Windows.Forms.Label label36;
        public System.Windows.Forms.TextBox txtEmail;
        internal System.Windows.Forms.Label label12;
        public System.Windows.Forms.MaskedTextBox txtNPhoneNo;
        internal System.Windows.Forms.Label label20;
        public System.Windows.Forms.MaskedTextBox txtNMobileNo;
        internal System.Windows.Forms.Label label31;
        internal System.Windows.Forms.Label label30;
        internal System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox txtNName;
        public System.Windows.Forms.TextBox txtNRelation;
        public System.Windows.Forms.TextBox txtNEmail;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label23;
        private System.Windows.Forms.DateTimePicker dtpIssue;
        private System.Windows.Forms.DateTimePicker dtpExpiry;
        internal System.Windows.Forms.Label label37;
        internal System.Windows.Forms.Label lblProduct;
        internal System.Windows.Forms.Label lblTaxPer;
        private System.Windows.Forms.TextBox txtDocumentNo;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtDocumentType;
        private System.Windows.Forms.TextBox txtDocumentName;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox txtAttachment;
        private System.Windows.Forms.Button btnBrowse;
        internal System.Windows.Forms.ListView lisList;
        internal System.Windows.Forms.ColumnHeader DocTypeDetails;
        internal System.Windows.Forms.ColumnHeader DocName;
        internal System.Windows.Forms.ColumnHeader DocNo;
        private System.Windows.Forms.GroupBox groupBox5;
        internal System.Windows.Forms.Label label25;
        public System.Windows.Forms.MaskedTextBox txtDPhoneNo;
        internal System.Windows.Forms.Label label26;
        public System.Windows.Forms.MaskedTextBox txtDMobileNo;
        internal System.Windows.Forms.Label label27;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label label29;
        public System.Windows.Forms.TextBox txtDName;
        public System.Windows.Forms.TextBox txtDRelation;
        public System.Windows.Forms.TextBox txtDEmail;
        internal System.Windows.Forms.Label label32;
        internal System.Windows.Forms.Label label33;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button Browse;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.Label lblItemCount;
        private System.Windows.Forms.Button btnAddDoc;
        private System.Windows.Forms.ColumnHeader Docid;
        private System.Windows.Forms.ColumnHeader IssDate;
        private System.Windows.Forms.ColumnHeader ExpDate;
        private System.Windows.Forms.ColumnHeader Attch;
        private System.Windows.Forms.ColumnHeader DocTypeCode;
        private System.Windows.Forms.ColumnHeader AttchName;
        private System.Windows.Forms.ColumnHeader Added;
        private ColumnHeader Renewal;
        private CheckBox chkRenewal;
        internal Label lblEmail2;
        internal Label lblEmail1;
        public TextBox txtEmail2;
        public TextBox txtEmail1;
        private ColumnHeader DocEmail1;
        private ColumnHeader DocEmail2;
        internal Label label40;
        public TextBox txtAge;
        public Button btnPrint;
        private TextBox txtPlace;
        private Label label41;
        private ColumnHeader Place;
        internal Label label42;
        public TextBox txtAllowance;
        public TextBox txtDTAddress4;
        public TextBox txtDPAddress4;
        public TextBox txtDTAddress3;
        public TextBox txtDPAddress3;
        public TextBox txtDTAddress2;
        public TextBox txtDPAddress2;
        public TextBox txtDTAddress1;
        public TextBox txtDPAddress1;
        public TextBox txtNTAddress4;
        public TextBox txtNPAddress4;
        public TextBox txtNTAddress3;
        public TextBox txtNPAddress3;
        public TextBox txtNTAddress2;
        public TextBox txtNPAddress2;
        public TextBox txtNTAddress1;
        public TextBox txtNPAddress1;
        public Button btnCancelEmp;
        public TextBox txtGrade;
        internal Label label43;
        private RichTextBox txtRemarks;
        internal Label label44;
        private Panel panel1;
        private Panel panel2;
        internal Label label45;
        public TextBox txtLicenseNo;
        private GroupBox groupBox6;
        private Button btnBrowseFolder;
        private FolderBrowserDialog folderBrowserDialog1;
    }
}
