using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using C1.Win.C1FlexGrid;
using Xpire.Classes;

namespace Xpire.Forms
{
    public partial class ucParameters : UserControl
    {
        public ucParameters()
        {
            InitializeComponent();
        }

        cGeneral cgen = new cGeneral();
        fssgen.fssgen gen = new fssgen.fssgen();

        SqlCommand cmd = new SqlCommand("", cPublic.Connection);

        int loc_intval = 0;

        private void ucParameters_Load(object sender, EventArgs e)
        {
            int counter = 1;

            grid_initialize();

            // SET SERIAL NUMBER ACCORDING TO THE ROWS 
            for (int j = 1; j < CGrid1.Rows.Count; j++)
            {
                if (CGrid1.Rows[j].Visible)
                { CGrid1[j, "SL"] = counter++; }
            }

            SendKeys.Send("{RIGHT}");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (prFunBoolValidation())
            {
                if (MessageBox.Show("Are You Sure to Save ?", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Save_Screen();
                    cgen.setparameters();                     
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void CGrid1_BeforeEdit(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            switch (CGrid1[CGrid1.Row, 1].ToString())
            {
                case "BillType":
                    CGrid1.Cols[2].ComboList = "Cash|Credit";
                    break;
                case "FormType":
                    CGrid1.Cols[2].ComboList = "A|B";
                    break;
                case "RoundOff":
                    CGrid1.Cols[2].ComboList = "Paise|Rupees";
                    break;
                case "Modification Date":
                    CGrid1.Cols[2].ComboList = "Editable|Non Editable";
                    break;
                case "BillSize":
                    CGrid1.Cols[2].ComboList = "FullPage|HalfPage|Packing slip";
                    break;
                case "TaxIncludePrice":
                case "ZeroQtySave":
                case "AutoQty":
                case "Referable Bills":
                case "AutoPriceUpdation":
                    CGrid1.Cols[2].ComboList = "Yes|No";
                    break;
                case "PriceType":
                    CGrid1.Cols[2].ComboList = "Retail|Whole Sale|Special";
                    break;
                case "PrintType":
                    CGrid1.Cols[2].ComboList = "Plain|Printed|Packing slip";
                    break;
                case "PrintMode":
                    CGrid1.Cols[2].ComboList = "DOS|WINDOWS";
                    break;
                case "LookupDefault":
                    CGrid1.Cols[2].ComboList = "Itemcode|ItemName";
                    break;
                default:
                    CGrid1.Cols[2].ComboList = string.Empty;
                    break;
            }
        }

        private void CGrid1_EnterCell(object sender, EventArgs e)
        {
            if ((CGrid1[CGrid1.Row, 1].ToString() == "Common Stock Master") ||
                (CGrid1[CGrid1.Row, 1].ToString() == "Common Ledger") ||
                (CGrid1[CGrid1.Row, 1].ToString() == "Account Type") ||
                (CGrid1[CGrid1.Row, 1].ToString() == "Ledger System") ||
                (CGrid1[CGrid1.Row, 1].ToString() == "Firm Type") ||
                (CGrid1[CGrid1.Row, 1] + "" == ""))
                CGrid1.AllowEditing = false;

            else
                CGrid1.AllowEditing = true;
        }

        private void CGrid1_KeyPressEdit(object sender, C1.Win.C1FlexGrid.KeyPressEditEventArgs e)
        {
            if (!e.KeyChar.Equals(Convert.ToChar(Keys.Enter)) && !e.KeyChar.Equals(Convert.ToChar(Keys.Tab)))
            {
                if (e.KeyChar.Equals(Convert.ToChar(".")) && CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("MAX. POSTING DIFFERENCE") == false)
                {
                    e.Handled = true;
                    return;
                }

                if (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("LINES TO REVERSE") ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("LINES TO SKIP")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("SALES RETURN TAX")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("STOCK AGEING DAYS")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("ACCOUNTS WITHIN NO OF DAYS 1")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("ACCOUNTS WITHIN NO OF DAYS 2")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("ACCOUNTS WITHIN NO OF DAYS 3")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("ACCOUNTS WITHIN NO OF DAYS 4")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("STOCK WITHIN NO OF DAYS 1")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("STOCK WITHIN NO OF DAYS 2")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("STOCK WITHIN NO OF DAYS 3")) ||
                    (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("STOCK WITHIN NO OF DAYS 4")))
                {
                    C1FlexGrid grd = (C1FlexGrid)sender;
                    //gen.ValidFlexNumber(e, ref grd, 7, 0);
                }
                else if (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper().Equals("MAX. POSTING DIFFERENCE"))
                {
                    C1FlexGrid grd = (C1FlexGrid)sender;
                    //gen.ValidFlexNumber(e, ref grd, 7, 2);
                }
                else
                {
                    switch (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper())
                    {
                        case "ACOUNTS WITHIN":
                        case "CREDIT DAYS REMINDER":
                        case "DEBIT DAYS REMINDER":
                        case "C-FORM UPDATION DAYS REMINDER":
                        case "C-FORM UPDATION DAYS":
                        case "LOGOUT TIME(MIN.)":
                            C1FlexGrid grd = (C1FlexGrid)sender;
                            //gen.ValidFlexNumber(e, ref grd, 3, 0);
                            break;
                    }
                }
            }
        }

        private void CGrid1_RowColChange(object sender, EventArgs e)
        {
            SendKeys.Send("{RIGHT}");
            switch (CGrid1[CGrid1.RowSel, 1].ToString().ToUpper())
            {
                case "SALES RETURN TAX":
                case "GREETINGS":
                case "STOCK AGEING DAYS":
                case "ACCOUNTS WITHIN NO OF DAYS 1":
                case "ACCOUNTS WITHIN NO OF DAYS 2":
                case "ACCOUNTS WITHIN NO OF DAYS 3":
                case "ACCOUNTS WITHIN NO OF DAYS 4":
                case "MAX. POSTING DIFFERENCE":
                case "LOGOUT TIME(MIN.)":
                    SendKeys.Send("{ENTER}");
                    break;
            }
        }

        private void CGrid1_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
        {
            if (CGrid1[CGrid1.RowSel, 1].ToString().Equals("Greetings"))
            {
                TextBox tb = (TextBox)CGrid1.Editor;
                tb.MaxLength = 70;
            }
            else if (CGrid1[CGrid1.RowSel, 1].ToString().Contains("Accounts WithIn") == true)
            {
                TextBox tb = (TextBox)CGrid1.Editor;
                tb.MaxLength = 3;
            }
        }

        private void CGrid1_ValidateEdit(object sender, C1.Win.C1FlexGrid.ValidateEditEventArgs e)
        {
            if (CGrid1[e.Row, 1].ToString().ToUpper().Equals("MAX. POSTING DIFFERENCE") && CGrid1.Editor != null)
            {
                if (Information.IsNumeric(CGrid1.Editor.Text) == false)
                { CGrid1.Editor.Text = "0.00"; }
                else { CGrid1.Editor.Text = Convert.ToDecimal(CGrid1.Editor.Text).ToString("#0.00"); }
            }
        }
        
        private void grid_initialize()
        {
            CGrid1.Cols.Count = 3;
            CGrid1.Rows.Count = 100;


            CGrid1.Cols[0].Name = "SL";
            CGrid1[0, "SL"] = "SL";
            CGrid1.Cols["SL"].Width = 25;

            CGrid1.Cols[1].Name = "Name";
            CGrid1[0, "Name"] = "NAME";
            CGrid1.Cols["Name"].Width = 235;

            CGrid1.Cols[2].Name = "Values";
            CGrid1[0, "Values"] = "VALUES";
            CGrid1.Cols["Values"].Width = 110;


            int i = 1;
            SqlDataReader dr;
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text; 
            cmd.CommandText = "Select VAR_NAME,VAR_VALUES from PUBLIC_VAR Order by var_name";
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                CGrid1[i, "Name"] = dr["var_name"];
                CGrid1[i, "Values"] = dr["VAR_VALUES"];

                    if ((dr["VAR_NAME"].ToString() == "Common Stock Master") ||
                        (dr["VAR_NAME"].ToString() == "Common Ledger") ||
                        (dr["VAR_NAME"].ToString() == "Account Type") ||
                        (dr["VAR_NAME"].ToString() == "Firm Type") ||
                        (dr["VAR_NAME"].ToString() == "Ledger System"))
                    { CGrid1.Rows[i].Visible = false; }
                    else { CGrid1.Rows[i].Visible = true; }
                
                i++;
            }
            dr.Close();
            CGrid1.Rows.Count = i;
        }

        private Boolean prFunBoolValidation()
        {
            for (int j = 1; j < CGrid1.Rows.Count; j++)
            {
                if (CGrid1[j, 1].ToString().Contains("Accounts WithIn") == true)
                {
                    for (int k = 1; k < CGrid1.Rows.Count; k++)
                    {
                        if (CGrid1[k, 2].ToString() == string.Empty)
                        {
                            CGrid1[k, 2] = "0";
                        }
                    }
                    loc_intval = Convert.ToInt32(CGrid1[j, 2]);
                    if (j != 38)
                    {
                        if (CGrid1[j + 1, 2].ToString() == string.Empty)
                        {
                            CGrid1[j + 1, 2] = "0";
                        }
                        if (Information.IsNumeric(CGrid1[j + 1, 2]) == true)
                        {
                            if (loc_intval > Convert.ToInt32(CGrid1[j + 1, 2]))
                            {
                                MessageBox.Show(CGrid1[j, 1].ToString() + " Is Greater  Than " + CGrid1[j + 1, 1].ToString(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }

                    }
                }

            }
            return true;
        }

        public void Save_Screen()
        {
            for (int i = 1; i < CGrid1.Rows.Count; i++)
            {
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text; 
                cmd.CommandText ="update public_var set var_values=@value where var_name=@name";
                cmd.Parameters.AddWithValue("@value", CGrid1[i, 2]);
                cmd.Parameters.AddWithValue("@name", CGrid1[i, 1]);
                cmd.ExecuteNonQuery();
            }
            MessageBox.Show("Setting Saved Successfully...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


    }
}
