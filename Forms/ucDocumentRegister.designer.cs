namespace Xpire.Forms
{
    partial class ucRegister
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpButtons = new System.Windows.Forms.GroupBox();
            this.chkRenewed = new System.Windows.Forms.CheckBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtAttachment = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPlace = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnView = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDocumentType = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.msgLabel = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmail6 = new System.Windows.Forms.TextBox();
            this.txtEmail3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmail5 = new System.Windows.Forms.TextBox();
            this.txtEmail2 = new System.Windows.Forms.TextBox();
            this.txtEmail4 = new System.Windows.Forms.TextBox();
            this.txtEmail1 = new System.Windows.Forms.TextBox();
            this.lblItemCount = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpIssue = new System.Windows.Forms.DateTimePicker();
            this.dtpExpiry = new System.Windows.Forms.DateTimePicker();
            this.label25 = new System.Windows.Forms.Label();
            this.lblProduct = new System.Windows.Forms.Label();
            this.lblTaxPer = new System.Windows.Forms.Label();
            this.txtDocumentNo = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grpButtons.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpButtons
            // 
            this.grpButtons.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.grpButtons.Controls.Add(this.chkRenewed);
            this.grpButtons.Controls.Add(this.btnNext);
            this.grpButtons.Controls.Add(this.btnPrevious);
            this.grpButtons.Controls.Add(this.btnSearch);
            this.grpButtons.Controls.Add(this.btnExit);
            this.grpButtons.Controls.Add(this.btnEdit);
            this.grpButtons.Controls.Add(this.btnDelete);
            this.grpButtons.Controls.Add(this.btnAdd);
            this.grpButtons.Location = new System.Drawing.Point(202, 148);
            this.grpButtons.Name = "grpButtons";
            this.grpButtons.Size = new System.Drawing.Size(710, 87);
            this.grpButtons.TabIndex = 0;
            this.grpButtons.TabStop = false;
            // 
            // chkRenewed
            // 
            this.chkRenewed.AutoSize = true;
            this.chkRenewed.Location = new System.Drawing.Point(556, 51);
            this.chkRenewed.Name = "chkRenewed";
            this.chkRenewed.Size = new System.Drawing.Size(72, 17);
            this.chkRenewed.TabIndex = 7;
            this.chkRenewed.Text = "Renewed";
            this.chkRenewed.UseVisualStyleBackColor = true;
            this.chkRenewed.Visible = false;
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.GhostWhite;
            this.btnNext.BackgroundImage = global::Xpire.Properties.Resources.next;
            this.btnNext.CausesValidation = false;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnNext.Location = new System.Drawing.Point(156, 12);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(76, 71);
            this.btnNext.TabIndex = 2;
            this.btnNext.Tag = "&Next";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.BackColor = System.Drawing.Color.GhostWhite;
            this.btnPrevious.BackgroundImage = global::Xpire.Properties.Resources.previous;
            this.btnPrevious.CausesValidation = false;
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevious.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnPrevious.Location = new System.Drawing.Point(81, 12);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(76, 71);
            this.btnPrevious.TabIndex = 1;
            this.btnPrevious.Tag = "&Previous";
            this.btnPrevious.UseVisualStyleBackColor = false;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.GhostWhite;
            this.btnSearch.BackgroundImage = global::Xpire.Properties.Resources.search1;
            this.btnSearch.CausesValidation = false;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnSearch.Location = new System.Drawing.Point(6, 12);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(76, 71);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Tag = "&Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.GhostWhite;
            this.btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnExit.Location = new System.Drawing.Point(456, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 71);
            this.btnExit.TabIndex = 6;
            this.btnExit.Tag = "E&xit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.GhostWhite;
            this.btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
            this.btnEdit.CausesValidation = false;
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnEdit.Location = new System.Drawing.Point(306, 12);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(76, 71);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Tag = "&Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.GhostWhite;
            this.btnDelete.BackgroundImage = global::Xpire.Properties.Resources.delete;
            this.btnDelete.CausesValidation = false;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnDelete.Location = new System.Drawing.Point(381, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(76, 71);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Tag = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.GhostWhite;
            this.btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
            this.btnAdd.CausesValidation = false;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnAdd.Location = new System.Drawing.Point(231, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(76, 71);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Tag = "&Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox4.Controls.Add(this.txtAttachment);
            this.groupBox4.Controls.Add(this.btnBrowse);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txtPlace);
            this.groupBox4.Location = new System.Drawing.Point(4, 297);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(615, 51);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            // 
            // txtAttachment
            // 
            this.txtAttachment.BackColor = System.Drawing.Color.White;
            this.txtAttachment.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAttachment.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAttachment.ForeColor = System.Drawing.Color.Black;
            this.txtAttachment.Location = new System.Drawing.Point(316, 15);
            this.txtAttachment.MaxLength = 30;
            this.txtAttachment.Name = "txtAttachment";
            this.txtAttachment.ReadOnly = true;
            this.txtAttachment.Size = new System.Drawing.Size(191, 23);
            this.txtAttachment.TabIndex = 21;
            this.txtAttachment.TabStop = false;
            this.txtAttachment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAttachment_KeyDown);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(519, 15);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 21;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.button1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(6, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(111, 16);
            this.label9.TabIndex = 26;
            this.label9.Text = "Place Of Issue";
            this.label9.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtPlace
            // 
            this.txtPlace.BackColor = System.Drawing.Color.White;
            this.txtPlace.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPlace.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlace.ForeColor = System.Drawing.Color.Black;
            this.txtPlace.Location = new System.Drawing.Point(123, 15);
            this.txtPlace.MaxLength = 500;
            this.txtPlace.Name = "txtPlace";
            this.txtPlace.Size = new System.Drawing.Size(151, 23);
            this.txtPlace.TabIndex = 17;
            this.txtPlace.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtPlace.Enter += new System.EventHandler(this.txtProduct_Enter);
            this.txtPlace.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProduct_KeyPress);
            this.txtPlace.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox5.Controls.Add(this.btnView);
            this.groupBox5.Location = new System.Drawing.Point(621, 297);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(92, 51);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(6, 15);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 22;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.txtDocumentType);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(3, 92);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(710, 54);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // txtDocumentType
            // 
            this.txtDocumentType.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtDocumentType.Location = new System.Drawing.Point(137, 19);
            this.txtDocumentType.Name = "txtDocumentType";
            this.txtDocumentType.Size = new System.Drawing.Size(170, 20);
            this.txtDocumentType.TabIndex = 8;
            this.txtDocumentType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDocumentType_KeyDown);
            this.txtDocumentType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDocumentType_KeyPress);
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.White;
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.ForeColor = System.Drawing.Color.Black;
            this.txtName.Location = new System.Drawing.Point(373, 19);
            this.txtName.MaxLength = 40;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(331, 23);
            this.txtName.TabIndex = 9;
            this.txtName.Enter += new System.EventHandler(this.txtItmName_Enter);
            this.txtName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItmCode_KeyDown);
            this.txtName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtName_KeyPress);
            this.txtName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(317, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(4, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Document Type";
            // 
            // msgLabel
            // 
            this.msgLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.msgLabel.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            this.msgLabel.Location = new System.Drawing.Point(9, 404);
            this.msgLabel.Name = "msgLabel";
            this.msgLabel.Size = new System.Drawing.Size(321, 31);
            this.msgLabel.TabIndex = 26;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtEmail6);
            this.groupBox3.Controls.Add(this.txtEmail3);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtEmail5);
            this.groupBox3.Controls.Add(this.txtEmail2);
            this.groupBox3.Controls.Add(this.txtEmail4);
            this.groupBox3.Controls.Add(this.txtEmail1);
            this.groupBox3.Location = new System.Drawing.Point(4, 211);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(710, 80);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(470, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 16);
            this.label8.TabIndex = 26;
            this.label8.Text = "Email 6";
            this.label8.Click += new System.EventHandler(this.label1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(470, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 16);
            this.label5.TabIndex = 26;
            this.label5.Text = "Email 3";
            this.label5.Click += new System.EventHandler(this.label1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(238, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 16);
            this.label7.TabIndex = 26;
            this.label7.Text = "Email 5";
            this.label7.Click += new System.EventHandler(this.label1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(238, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 16);
            this.label4.TabIndex = 26;
            this.label4.Text = "Email 2";
            this.label4.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtEmail6
            // 
            this.txtEmail6.BackColor = System.Drawing.Color.White;
            this.txtEmail6.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.txtEmail6.ForeColor = System.Drawing.Color.Black;
            this.txtEmail6.Location = new System.Drawing.Point(537, 47);
            this.txtEmail6.MaxLength = 500;
            this.txtEmail6.Name = "txtEmail6";
            this.txtEmail6.Size = new System.Drawing.Size(166, 23);
            this.txtEmail6.TabIndex = 19;
            this.txtEmail6.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtEmail6.Enter += new System.EventHandler(this.txtProduct_Enter);
            this.txtEmail6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail6_KeyDown);
            this.txtEmail6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail6_KeyPress);
            this.txtEmail6.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // txtEmail3
            // 
            this.txtEmail3.BackColor = System.Drawing.Color.White;
            this.txtEmail3.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.txtEmail3.ForeColor = System.Drawing.Color.Black;
            this.txtEmail3.Location = new System.Drawing.Point(537, 13);
            this.txtEmail3.MaxLength = 500;
            this.txtEmail3.Name = "txtEmail3";
            this.txtEmail3.Size = new System.Drawing.Size(166, 23);
            this.txtEmail3.TabIndex = 16;
            this.txtEmail3.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtEmail3.Enter += new System.EventHandler(this.txtProduct_Enter);
            this.txtEmail3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail3_KeyDown);
            this.txtEmail3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail3_KeyPress);
            this.txtEmail3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(6, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 16);
            this.label6.TabIndex = 26;
            this.label6.Text = "Email 4";
            this.label6.Click += new System.EventHandler(this.label1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 16);
            this.label1.TabIndex = 26;
            this.label1.Text = "Email 1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtEmail5
            // 
            this.txtEmail5.BackColor = System.Drawing.Color.White;
            this.txtEmail5.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.txtEmail5.ForeColor = System.Drawing.Color.Black;
            this.txtEmail5.Location = new System.Drawing.Point(308, 47);
            this.txtEmail5.MaxLength = 500;
            this.txtEmail5.Name = "txtEmail5";
            this.txtEmail5.Size = new System.Drawing.Size(151, 23);
            this.txtEmail5.TabIndex = 18;
            this.txtEmail5.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtEmail5.Enter += new System.EventHandler(this.txtProduct_Enter);
            this.txtEmail5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail5_KeyDown);
            this.txtEmail5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail5_KeyPress);
            this.txtEmail5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // txtEmail2
            // 
            this.txtEmail2.BackColor = System.Drawing.Color.White;
            this.txtEmail2.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.txtEmail2.ForeColor = System.Drawing.Color.Black;
            this.txtEmail2.Location = new System.Drawing.Point(308, 13);
            this.txtEmail2.MaxLength = 500;
            this.txtEmail2.Name = "txtEmail2";
            this.txtEmail2.Size = new System.Drawing.Size(151, 23);
            this.txtEmail2.TabIndex = 15;
            this.txtEmail2.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtEmail2.Enter += new System.EventHandler(this.txtProduct_Enter);
            this.txtEmail2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail2_KeyDown);
            this.txtEmail2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail2_KeyPress);
            this.txtEmail2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // txtEmail4
            // 
            this.txtEmail4.BackColor = System.Drawing.Color.White;
            this.txtEmail4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail4.ForeColor = System.Drawing.Color.Black;
            this.txtEmail4.Location = new System.Drawing.Point(80, 47);
            this.txtEmail4.MaxLength = 500;
            this.txtEmail4.Name = "txtEmail4";
            this.txtEmail4.Size = new System.Drawing.Size(151, 23);
            this.txtEmail4.TabIndex = 17;
            this.txtEmail4.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtEmail4.Enter += new System.EventHandler(this.txtProduct_Enter);
            this.txtEmail4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail4_KeyDown);
            this.txtEmail4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail4_KeyPress);
            this.txtEmail4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // txtEmail1
            // 
            this.txtEmail1.BackColor = System.Drawing.Color.White;
            this.txtEmail1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail1.ForeColor = System.Drawing.Color.Black;
            this.txtEmail1.Location = new System.Drawing.Point(80, 13);
            this.txtEmail1.MaxLength = 500;
            this.txtEmail1.Name = "txtEmail1";
            this.txtEmail1.Size = new System.Drawing.Size(151, 23);
            this.txtEmail1.TabIndex = 14;
            this.txtEmail1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtEmail1.Enter += new System.EventHandler(this.txtProduct_Enter);
            this.txtEmail1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmail1_KeyDown);
            this.txtEmail1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmail1_KeyPress);
            this.txtEmail1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // lblItemCount
            // 
            this.lblItemCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblItemCount.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItemCount.ForeColor = System.Drawing.Color.Navy;
            this.lblItemCount.Location = new System.Drawing.Point(490, 403);
            this.lblItemCount.Name = "lblItemCount";
            this.lblItemCount.Size = new System.Drawing.Size(217, 22);
            this.lblItemCount.TabIndex = 27;
            this.lblItemCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox2.Controls.Add(this.dtpIssue);
            this.groupBox2.Controls.Add(this.dtpExpiry);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.lblProduct);
            this.groupBox2.Controls.Add(this.lblTaxPer);
            this.groupBox2.Controls.Add(this.txtDocumentNo);
            this.groupBox2.Location = new System.Drawing.Point(4, 148);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(709, 57);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // dtpIssue
            // 
            this.dtpIssue.CustomFormat = "dd/MM/yyyy";
            this.dtpIssue.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpIssue.Location = new System.Drawing.Point(97, 15);
            this.dtpIssue.Name = "dtpIssue";
            this.dtpIssue.Size = new System.Drawing.Size(86, 20);
            this.dtpIssue.TabIndex = 11;
            // 
            // dtpExpiry
            // 
            this.dtpExpiry.CustomFormat = "dd/MM/yyyy";
            this.dtpExpiry.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiry.Location = new System.Drawing.Point(290, 15);
            this.dtpExpiry.Name = "dtpExpiry";
            this.dtpExpiry.Size = new System.Drawing.Size(86, 20);
            this.dtpExpiry.TabIndex = 12;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(4, 18);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 16);
            this.label25.TabIndex = 29;
            this.label25.Text = "Isuue Date";
            // 
            // lblProduct
            // 
            this.lblProduct.AutoSize = true;
            this.lblProduct.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblProduct.Location = new System.Drawing.Point(382, 17);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(105, 16);
            this.lblProduct.TabIndex = 26;
            this.lblProduct.Text = "Document No";
            // 
            // lblTaxPer
            // 
            this.lblTaxPer.AutoSize = true;
            this.lblTaxPer.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblTaxPer.Location = new System.Drawing.Point(192, 17);
            this.lblTaxPer.Name = "lblTaxPer";
            this.lblTaxPer.Size = new System.Drawing.Size(92, 16);
            this.lblTaxPer.TabIndex = 22;
            this.lblTaxPer.Text = "Expiry Date";
            // 
            // txtDocumentNo
            // 
            this.txtDocumentNo.BackColor = System.Drawing.Color.White;
            this.txtDocumentNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDocumentNo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDocumentNo.ForeColor = System.Drawing.Color.Black;
            this.txtDocumentNo.Location = new System.Drawing.Point(493, 14);
            this.txtDocumentNo.MaxLength = 50;
            this.txtDocumentNo.Name = "txtDocumentNo";
            this.txtDocumentNo.Size = new System.Drawing.Size(210, 23);
            this.txtDocumentNo.TabIndex = 13;
            this.txtDocumentNo.Enter += new System.EventHandler(this.txtProduct_Enter);
            this.txtDocumentNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProduct_KeyPress);
            this.txtDocumentNo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.lblItemCount);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.msgLabel);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Location = new System.Drawing.Point(199, 143);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(724, 451);
            this.panel2.TabIndex = 343;
            // 
            // ucRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.grpButtons);
            this.Controls.Add(this.panel2);
            this.Name = "ucRegister";
            this.Size = new System.Drawing.Size(1111, 640);
            this.Load += new System.EventHandler(this.ucStockMaster_Load);
            this.grpButtons.ResumeLayout(false);
            this.grpButtons.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpButtons;
        internal System.Windows.Forms.Button btnNext;
        internal System.Windows.Forms.Button btnPrevious;
        internal System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.Button btnEdit;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnAdd;
        public System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.CheckBox chkRenewed;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtAttachment;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDocumentType;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label msgLabel;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEmail6;
        private System.Windows.Forms.TextBox txtEmail3;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmail5;
        private System.Windows.Forms.TextBox txtEmail2;
        private System.Windows.Forms.TextBox txtEmail4;
        private System.Windows.Forms.TextBox txtEmail1;
        private System.Windows.Forms.Label lblItemCount;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtpIssue;
        private System.Windows.Forms.DateTimePicker dtpExpiry;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Label lblProduct;
        internal System.Windows.Forms.Label lblTaxPer;
        private System.Windows.Forms.TextBox txtDocumentNo;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtPlace;
    }
}
