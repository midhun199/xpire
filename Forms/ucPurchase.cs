using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using C1.Win.C1FlexGrid;
using System.Data.SqlClient;
using Microsoft.VisualBasic;


namespace Biz_Maxx
{
    public partial class ucPurchase : UserControl
    {
        public ucPurchase()
        {
            InitializeComponent();
        }
        cGeneral cGen = new cGeneral();
        fssgen.fssgen gen = new fssgen.fssgen();
        string masterTable, subTable, sp_Master, sp_Details = string.Empty, sp_Posting = string.Empty;
        string sqlQuery = string.Empty, oldSupplier = string.Empty, oldBillType = string.Empty;
        SqlCommand cmd = new SqlCommand("", cPublic.Connection);
        SqlTransaction Trans; 

        decimal p_InvAmount = 0,p_expense = 0,p_Bulk_Disc = 0;        
        int  vPurchOrderno = 0;
        Boolean chklookup = false;

        public string vTag
        {
            get { return Tag.ToString(); }
            set { Tag = value; }
        }
	

        private void set_Master_Sub_Tables()
        {
            switch (this.Tag.ToString())
            {
                case "HE":
                case "HM":
                case "HC":
                case "HP":
                    masterTable = "PURCHASE" + cPublic.g_firmcode;
                    subTable = "PITEM" + cPublic.g_firmcode;
                    sp_Posting = "PURCHASE";
                    break;
                case "UE":
                case "UM":
                case "UC":
                case "UP":
                    masterTable = "PRETURN" + cPublic.g_firmcode;
                    subTable = "PRITEM" + cPublic.g_firmcode;
                    sp_Posting = "PRETURN";
                    break;
                case "IE":
                case "IM":
                case "IC":
                case "IP":
                    masterTable = "TRANSIN" + cPublic.g_firmcode;
                    subTable = "TRITEMIN" + cPublic.g_firmcode;
                    break;
                case "OE":
                case "OM":
                case "OC":
                case "OP":
                    masterTable = "PORDER" + cPublic.g_firmcode;
                    subTable = "POITEM" + cPublic.g_firmcode;
                    break;
            }
        }

        private void set_Master_Sub_SPs()
        {
            switch (this.Tag.ToString().Substring(1, 1))
            {
                case "E":
                    sp_Master = "SP_INSERT_" + masterTable;
                    sp_Details = "SP_INSERT_" + subTable;
                    break;
                case "M":
                    sp_Master = "SP_UPDATE_" + masterTable;
                    sp_Details = "SP_INSERT_" + subTable;
                    txtNumber.Enabled = false;
                    break;
                case "C":
                    sp_Master = "SP_DELETE_" + masterTable;
                    sp_Details = "SP_DELETE_" + subTable;
                    txtNumber.Enabled = false;
                    break;
            }
        }

        private void set_others()
        {
            int number = cGen.getMaxNumber(masterTable, "orderno");
            switch (this.Tag.ToString().Substring(1, 1))
            {
                case "E":
                    number += 1;
                    txtNumber.Enabled = false;
                    break;
                case "M":
                case "C":
                case "P":
                    txtNumber.Enabled = true;
                    break;
            }
            txtNumber.Text = number.ToString();
        }

        private void grid_initialize(C1FlexGrid grid)
        {
            int vcol = 0;
            grid.Styles.Fixed.Font = new Font("Verdana", 10, FontStyle.Regular);
            grid.Styles.Fixed.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            grid.Styles.Normal.Font = new Font("Verdana", 10, FontStyle.Regular);

            grid.Cols.Count = 100;
            grid.Rows.Count = 20;

            grid.Cols[vcol++].Name = "slno";
            grid[0, "slno"] = "SlNo";
            grid.Cols["slno"].Width = 40;
            grid.Cols["slno"].DataType = typeof(string);
            grid.Cols["slno"].TextAlign = TextAlignEnum.LeftCenter;

            grid.Cols[vcol++].Name = "itmCode";
            grid[0, "itmCode"] = "Itemcode";
            grid.Cols["itmCode"].Width = 75;
            grid.Cols["itmCode"].DataType = typeof(string);
            grid.Cols["itmCode"].TextAlign = TextAlignEnum.LeftCenter;

            grid.Cols[vcol++].Name = "itmName";
            grid[0, "itmName"] = "Item Description";
            grid.Cols["itmName"].Width = 250;
            grid.Cols["itmName"].DataType = typeof(string);
            grid.Cols["itmName"].TextAlign = TextAlignEnum.LeftCenter;
            grid.Cols["itmName"].AllowEditing = true;

            grid.Cols[vcol++].Name = "bCode";
            grid[0, "bCode"] = "BarCode";
            grid.Cols["bCode"].Width = 75;
            grid.Cols["bCode"].DataType = typeof(Boolean);
            grid.Cols["bcode"].AllowEditing = false;
            grid.Cols["bcode"].Visible = false;

            grid.Cols[vcol++].Name = "Unit";
            grid[0, "Unit"] = "Unit";
            grid.Cols["Unit"].Width = 35;
            grid.Cols["Unit"].DataType = typeof(string);
            grid.Cols["Unit"].AllowEditing = true;


            grid.Cols[vcol++].Name = "UnitValue";
            grid[0, "UnitValue"] = "Unit Value";
            grid.Cols["UnitValue"].Width = 100;
            grid.Cols["UnitValue"].DataType = typeof(Decimal);
            grid.Cols["UnitValue"].Format = "#0.000";
            grid.Cols["UnitValue"].AllowEditing = false;
            grid.Cols["UnitValue"].Visible = false;

            grid.Cols[vcol++].Name = "multiunit";
            grid.Cols["multiunit"].DataType = typeof(string);
            grid.Cols["multiunit"].Visible = false;

            grid.Cols[vcol++].Name = "qty";
            grid[0, "qty"] = "QTY";
            grid.Cols["qty"].Width = 80;
            grid.Cols["qty"].DataType = typeof(Decimal);
            grid.Cols["qty"].Format = "#0.000";

            grid.Cols[vcol++].Name = "foc";
            grid[0, "foc"] = "FOC";
            grid.Cols["foc"].Width = 80;
            grid.Cols["foc"].DataType = typeof(Decimal);
            grid.Cols["foc"].Format = "#0.000";
            grid.Cols["foc"].Visible = false;

            grid.Cols[vcol++].Name = "Rate";
            grid[0, "Rate"] = "Rate";
            grid.Cols["Rate"].Width = 75;
            grid.Cols["Rate"].DataType = typeof(Decimal);
            grid.Cols["Rate"].Format = "#0.0000";
            grid.Cols["Rate"].AllowEditing = false;
            grid.Cols["Rate"].Visible = false;

            grid.Cols[vcol++].Name = "price";
            grid[0, "price"] = "Price";
            grid.Cols["price"].Width = 75;
            grid.Cols["price"].DataType = typeof(Decimal);
            grid.Cols["price"].Format = "#0.0000";
            grid.Cols["price"].AllowEditing = true ;

            grid.Cols[vcol++].Name = "taxPer";
            grid[0, "taxPer"] = "Tax %";
            grid.Cols["taxPer"].Width = 50;
            grid.Cols["taxPer"].DataType = typeof(Decimal);
            grid.Cols["taxPer"].Format = "#0.00";

            grid.Cols[vcol++].Name = "taxAmt";
            grid[0, "taxAmt"] = "Tax Amt";
            grid.Cols["taxAmt"].Width = 70;
            grid.Cols["taxAmt"].DataType = typeof(Decimal);
            grid.Cols["taxAmt"].Format = "#0.0000";
            grid.Cols["taxAmt"].Visible = false;

            grid.Cols[vcol++].Name = "cessPer";
            grid[0, "cessPer"] = "Cess %";
            grid.Cols["cessPer"].Width = 50;
            grid.Cols["cessPer"].DataType = typeof(decimal);
            grid.Cols["cessPer"].Format = "#0.00"; 
            grid.Cols["cessPer"].Visible = false ;

            grid.Cols[vcol++].Name = "cessAmt";
            grid[0, "cessAmt"] = "Cess Amt";
            grid.Cols["cessAmt"].Width = 70;
            grid.Cols["cessAmt"].DataType = typeof(decimal);
            grid.Cols["cessAmt"].Format = "#0.0000";
            grid.Cols["cessAmt"].Visible = false;

            grid.Cols[vcol++].Name = "discPer";
            grid[0, "discPer"] = "Disc %";
            grid.Cols["discPer"].Width = 50;
            grid.Cols["discPer"].DataType = typeof(Decimal);
            grid.Cols["discPer"].Format = "#0.00";
            grid.Cols["discPer"].Visible = false;

            grid.Cols[vcol++].Name = "discamt";
            grid[0, "discamt"] = "Disc Amt";
            grid.Cols["discamt"].Width = 50;
            grid.Cols["discamt"].DataType = typeof(Decimal);
            grid.Cols["discamt"].Format = "#0.0000";
            grid.Cols["discamt"].Visible = false;

            grid.Cols[vcol++].Name = "accCode";
            grid[0, "accCode"] = "A/C Code";
            grid.Cols["accCode"].Width = 65;
            grid.Cols["accCode"].DataType = typeof(String);
            grid.Cols["accCode"].AllowEditing = false;
            grid.Cols["accCode"].Visible = false;

            grid.Cols[vcol++].Name = "lc";
            grid[0, "lc"] = "Lc";
            grid.Cols["lc"].Width = 50;
            grid.Cols["lc"].DataType = typeof(Decimal);
            grid.Cols["lc"].Format = "#0.0000";
            grid.Cols["lc"].Visible = false;

            grid.Cols[vcol++].Name = "Tdiscamt";
            grid[0, "Tdiscamt"] = "Disc Amt";
            grid.Cols["Tdiscamt"].Width = 50;
            grid.Cols["Tdiscamt"].DataType = typeof(Decimal);
            grid.Cols["Tdiscamt"].Format = "#0.0000";
            grid.Cols["Tdiscamt"].Visible = false;

            grid.Cols[vcol++].Name = "netAmount";
            grid[0, "netAmount"] = "Net Amount";
            grid.Cols["netAmount"].Width = 100;
            grid.Cols["netAmount"].DataType = typeof(Decimal);
            grid.Cols["netAmount"].Format = "#0.0000";
            grid.Cols["netAmount"].AllowEditing = false;

            grid.Cols.Count = vcol;
            grid.Rows.Count = 20;

            grid.AllowFreezing = AllowFreezingEnum.Columns;
            grid.Cols.Frozen = 3;
        }

        private void fill_PurchaseOrder()
        {
            //if (this.Tag.ToString().Substring(0, 1) == "U")
            //{
            //    if (fill_Master(txtPOrderno.Text, "PURCHASE" + cPublic.g_firmcode))
            //    { fill_Details(txtPOrderno.Text, "PITEM" + cPublic.g_firmcode); this.txtPOrderno.Enabled = false; }
            //    else
            //    { MessageBox.Show("Purchase Order number Not Found..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning); this.txtPOrderno.Focus(); this.txtPOrderno.SelectAll(); }
            //}
            //else
            //{
            //    if (fill_Master(txtPOrderno.Text, "PORDER" + cPublic.g_firmcode))
            //    { fill_Details(txtPOrderno.Text, "POITEM" + cPublic.g_firmcode); this.txtPOrderno.Enabled = false; }
            //    else
            //    { MessageBox.Show("Purchase Order number Not Found..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning); this.txtPOrderno.Focus(); this.txtPOrderno.SelectAll(); }
            //}
        }

        private Boolean CheckItem(string itmcode)
        {
            bool status = false;
            sqlQuery = "select *    " + Environment.NewLine
            + "  from stockmst" + cPublic.g_firmcode + " a where (itemcode = @itmcode) ";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@itmcode", itmcode);
            DataTable tbl = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(tbl);
            if (tbl.Rows.Count > 0)
            {
                status = true;
            }
            return status;
        }

        private Boolean GetItemName(string ItemName)
        {
            string value = string.Empty;

            if (value == string.Empty)
            { value = ItemName.Trim(); }

            SqlDataReader dr;
            cmd.Parameters.Clear();
            cmd.CommandText = "select * from stockmst" + cPublic.g_firmcode + "  where itemname like @itemname and itemname<>'' ";
            cmd.Connection = cPublic.Connection;
            cmd.Parameters.AddWithValue("@itemname", "%" + value + "%");
            dr = cmd.ExecuteReader();
            if (dr.Read())
            { dr.Close(); return true; }

            dr.Close();

            return false;
        }

        private Boolean fill_Master(string number, string vmastertable)
        {
            Boolean status = false;

            sqlQuery = "select * from " + vmastertable + " where ORDERNO = @number";
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text; 
            cmd.CommandText = sqlQuery;  
            cmd.Parameters.AddWithValue("@number", number);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                this.dateInvoice.Value = Convert.ToDateTime(dr["INVDATE"].ToString());
                this.txtInvNo.Text = dr["INVNO"].ToString();
                this.txtInvAmount.Text = dr["INVAMT"].ToString();
                this.txtSupplier.Text = dr["SUPPNAME"].ToString();
                this.txtSupplier.Tag = dr["SUPPCODE"].ToString();
                oldSupplier = this.txtSupplier.Tag + "";
                this.txtDiscAmount.Text = dr["discAmt"].ToString();
                this.txtDescription.Text = dr["REMARKS"].ToString();
                this.txtExpense.Text = dr["EXPENSE"].ToString();
                this.txtRoundOff.Text = dr["roundoff"].ToString();

                if (vmastertable.Trim().ToUpper().Contains("PORDER") == false)
                {
                    if (dr["CSTBILL"].ToString() == "V")
                    { this.cmbTaxMode.Text = "VAT"; }
                    else if (dr["CSTBILL"].ToString() == "C")
                    { this.cmbTaxMode.Text = "CST"; }
                    else if (dr["CSTBILL"].ToString() == "O")
                    { this.cmbTaxMode.Text = "OTHERS"; }
                    this.dtpDate.Value = Convert.ToDateTime(dr["DATE1"].ToString());
                    this.cmbPaymentMode.SelectedIndex = Convert.ToInt16(dr["type"].ToString()) - 1;
                    oldBillType = dr["type"].ToString();
                }
                else
                {
                    this.txtExpense.Text = "0.00";
                    this.dtpDate.Value = cPublic.g_Rdate;
                }

                //if (this.Tag.ToString().Substring(1, 1) != "E")
                //{
                //    if (dr["PORDERNO"].Equals(DBNull.Value) == false)
                //    {
                //        this.txtPOrderno.Text = dr["PORDERNO"].ToString(); 
                //        vPurchOrderno = Convert.ToInt32(dr["PORDERNO"].ToString());
                //    }
                //}               

                p_InvAmount = Convert.ToDecimal(txtInvAmount.Text.Trim());
                p_Bulk_Disc = Convert.ToDecimal(txtDiscAmount.Text);
                p_expense = Convert.ToDecimal(txtExpense.Text.Trim());

                status = true;
            }
            dr.Close();
            return status;
        }

        private void fill_Details(string number, string vSubtable)
        {
            sqlQuery = "select a.*,b.taxper [st_taxper],b.multiunit,b.cessper [st_cessper] from " + vSubtable + " a, stockmst" + cPublic.g_firmcode + " b"
           + " where ORDERNO = @number and a.itemcode = b.itemcode order by a.RECNO";
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, cPublic.Connection);
            da.SelectCommand.Parameters.AddWithValue("@number", number);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cmbTaxMode.Enabled = false;
            if (dt.Rows.Count >= flxPurchaseDetails.Rows.Count - 1)
            { flxPurchaseDetails.Rows.Count = dt.Rows.Count + 5; }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                flxPurchaseDetails[i + 1, "slno"] = i + 1;
                flxPurchaseDetails[i + 1, "itmCode"] = dt.Rows[i]["ITEMCODE"].ToString();
                flxPurchaseDetails[i + 1, "itmName"] = dt.Rows[i]["itemname"].ToString();
                flxPurchaseDetails[i + 1, "bCode"] = dt.Rows[i]["BARCODE"].ToString() == "1" ? true : false;
                flxPurchaseDetails[i + 1, "qty"] = dt.Rows[i]["QTY"].ToString();
                flxPurchaseDetails[i + 1, "foc"] = dt.Rows[i]["FOC"].ToString();
                flxPurchaseDetails[i + 1, "price"] = dt.Rows[i]["PRICE"].ToString();
                flxPurchaseDetails[i + 1, "discPer"] = dt.Rows[i]["DISCPER"].ToString();
                flxPurchaseDetails[i + 1, "taxAmt"] = dt.Rows[i]["TAXAMT"].ToString();
                flxPurchaseDetails[i + 1, "Unit"] = dt.Rows[i]["Unit"].ToString();
                flxPurchaseDetails[i + 1, "UnitValue"] = dt.Rows[i]["UnitValue"].ToString();
                flxPurchaseDetails[i + 1, "multiunit"] = "N";
                if (vSubtable.Contains("POITEM") == false)
                {
                    flxPurchaseDetails[i + 1, "discamt"] = dt.Rows[i]["discamt"] + "".ToString();
                    flxPurchaseDetails[i + 1, "accCode"] = dt.Rows[i]["ACCCODE"].ToString();
                    flxPurchaseDetails[i + 1, "cessPer"] = dt.Rows[i]["CESSPER"].ToString();
                    flxPurchaseDetails[i + 1, "taxPer"] = dt.Rows[i]["TAXPER"].ToString();
                }
                else
                {
                    flxPurchaseDetails[i + 1, "discamt"] = "0.0000";

                    flxPurchaseDetails[i + 1, "accCode"] = getAccHead(cmbTaxMode.Text.Trim(), (int)Convert.ToDecimal(dt.Rows[i]["st_taxper"].ToString()));

                    flxPurchaseDetails[i + 1, "taxPer"] = dt.Rows[i]["st_TAXPER"].ToString();
                    if (cmbTaxMode.Text == "VAT")
                    { flxPurchaseDetails[i + 1, "cessPer"] = dt.Rows[i]["st_CESSPER"].ToString(); }
                    else { flxPurchaseDetails[i + 1, "cessPer"] = "0.00"; }
                }


                flxPurchaseDetails[i + 1, "cessAmt"] = dt.Rows[i]["CESSAMT"].ToString();
                flxPurchaseDetails[i + 1, "netAmount"] = dt.Rows[i]["NETAMT"].ToString();
            }
            flxPurchaseDetails.Row = dt.Rows.Count + 1;
            flxPurchaseDetails.Col = flxPurchaseDetails.Cols["itmCode"].Index;
        }

        private string getAccHead(string accCode, decimal  taxper)
        {
            string accHead = string.Empty;
            switch (this.Tag.ToString().Substring(0, 1))
            {
                case "H":
                    accHead = "31" + taxper.ToString() + accCode.Substring(0, 1);
                    break;
                case "U":
                    accHead = "32" + taxper.ToString() + accCode.Substring(0, 1);
                    break;
                case "O":
                    accHead = "33" + taxper.ToString() + accCode.Substring(0, 1);
                    break;
            }
            return accHead;
        }

        private void generalCalcs(ValidateEditEventArgs e)
        {

            if (flxPurchaseDetails[e.Row, "itmName"] + "".ToString() == string.Empty) { return; }
            if (flxPurchaseDetails.Cols[e.Col].DataType == typeof(decimal))
                if (flxPurchaseDetails.Editor.Text == "." || flxPurchaseDetails.Editor.Text == string.Empty)
                { flxPurchaseDetails.Editor.Text = "0"; }
            string colName = flxPurchaseDetails.Cols[e.Col].Name;
            switch (colName)
            {
                case "price":
                    if (Information.IsNumeric(flxPurchaseDetails.Editor.Text) == true)
                    {
                        flxPurchaseDetails[e.Row, "price"] = flxPurchaseDetails.Editor.Text;
                        if (Convert.ToDecimal(flxPurchaseDetails[e.Row, "qty"]) > 0)
                        {
                           // flxPurchaseDetails[e.Row, "TAmount"] = Convert.ToDecimal(Convert.ToDecimal(flxPurchaseDetails[e.Row, "qty"]) * Convert.ToDecimal(flxPurchaseDetails.Editor.Text)).ToString("#0.00");
                        }
                        calc_Line(e.Row);
                    }
                    break;

            }
        }

        private void calc_Line(int row)
        {
            if (row <= 0) { return; }
            if (txtInvAmount.Text.Equals("0.0000")) { txtInvAmount.Text = "1.0000"; }
            if (flxPurchaseDetails[row, "itmName"] + "" == string.Empty) { return; }
            decimal p_Qty;
            decimal p_Price;
            decimal p_Amount;
            decimal p_Amt;
            decimal p_DiscPer = 0;
            decimal p_TaxPer;
            decimal p_TaxAmt;
            decimal p_DiscAmount = 0;
            decimal p_Less;
            decimal p_NetAmount;
            decimal p_CessPer = 0;
            decimal p_CessAmt = 0;
            decimal P_spdiscamt = 0;
            decimal p_Lc = 0;

            if (p_InvAmount == 0) { p_InvAmount = 1; }
            p_Qty = Convert.ToDecimal(flxPurchaseDetails[row, "qty"].ToString());
            if (p_Qty == 0)
                return;

            p_Price = Convert.ToDecimal(flxPurchaseDetails[row, "price"].ToString());
            p_DiscPer = Convert.ToDecimal(flxPurchaseDetails[row, "discPer"].ToString());
            p_TaxPer = Convert.ToDecimal(flxPurchaseDetails[row, "taxPer"].ToString());
            p_CessPer = Convert.ToDecimal(flxPurchaseDetails[row, "cessPer"].ToString());
            P_spdiscamt = Convert.ToDecimal(flxPurchaseDetails[row, "discamt"].ToString());

            p_Amt = p_Price * p_Qty;
            if (p_Amt != 0 && p_Qty != 0)
            {
                p_Price = p_Amt / p_Qty;
                flxPurchaseDetails[row, "price"] = p_Price.ToString("#0.0000");
            }
            else if (p_Qty != 0 && p_Price != 0)
            { p_Amt = p_Qty * p_Price; }


            p_Amount = p_Amt;

            p_DiscAmount = p_Amt * p_DiscPer / 100;
            p_Amt = p_Amt - (p_Amt * p_DiscPer / 100);



            p_TaxAmt = p_Amt * p_TaxPer / 100;
            flxPurchaseDetails[row, "taxAmt"] = p_TaxAmt.ToString("#0.0000");
            p_CessAmt = p_TaxAmt * p_CessPer / 100;
            flxPurchaseDetails[row, "cessAmt"] = p_CessAmt.ToString("#0.0000");

            p_Less = ((p_expense)) / (p_InvAmount) * 100;

            p_Lc = (((p_Amt + p_TaxAmt + p_CessAmt) - (P_spdiscamt) + ((p_Amt + p_TaxAmt + p_CessAmt - (P_spdiscamt)) * p_Less / 100)) / p_Qty);

            if (cmbTaxMode.Text != "CST")
            {
                p_Lc = p_Lc - (p_TaxAmt + p_CessAmt) / p_Qty;
            }
            //else
            //{
            //    p_Lc = p_Lc;
            //}

            flxPurchaseDetails[row, "lc"] = p_Lc.ToString("#0.0000");

            p_NetAmount = (p_Amt + p_TaxAmt + p_CessAmt) - (P_spdiscamt);

            flxPurchaseDetails[row, "TdiscAmt"] = p_DiscAmount + P_spdiscamt;

            flxPurchaseDetails[row, "Rate"] = Convert.ToDecimal(p_NetAmount / p_Qty).ToString("#0.0000");

            flxPurchaseDetails[row, "netAmount"] = p_NetAmount.ToString("#0.0000");
        }

        private void calc_Bill()
        {
            decimal taxAmt = 0;
            decimal cessAmt = 0;
            decimal discAmt = 0;
            decimal amount = 0;
            decimal spdiscamt = 0;
            decimal qty = 0;
            decimal roundoff = 0;
            for (int row = 1; row < flxPurchaseDetails.Rows.Count; row++)
            {
                if (flxPurchaseDetails[row, "itmName"] + "" == string.Empty) { break; }

                qty += Convert.ToDecimal(flxPurchaseDetails[row, "qty"]);
                taxAmt += Convert.ToDecimal(flxPurchaseDetails[row, "taxAmt"]);
                discAmt += Convert.ToDecimal(flxPurchaseDetails[row, "Tdiscamt"]);
                amount += Convert.ToDecimal(flxPurchaseDetails[row, "price"]) * Convert.ToDecimal(flxPurchaseDetails[row, "qty"]);
                cessAmt += Convert.ToDecimal(flxPurchaseDetails[row, "cessAmt"]);
                spdiscamt += Convert.ToDecimal(flxPurchaseDetails[row, "discamt"]);
            }
            lblTotal.Text = Strings.Format(amount, "#0.0000");

            lblDiscount.Text = discAmt.ToString("#0.0000");
            lblaftertaxamt.Text = spdiscamt.ToString("#0.0000");

            lblTax.Text = taxAmt.ToString("#0.0000");
            lblCess.Text = cessAmt.ToString("#0.0000");

            lblroundoffpost.Text = txtRoundOff.Text;
              

            roundoff = Convert.ToDecimal(txtRoundOff.Text);

            lblInvAmount.Text = Convert.ToDecimal(amount - discAmt - spdiscamt + taxAmt + cessAmt + roundoff).ToString("#0.0000");


        }

        private void setSlNo()
        {
            for (int row = 1; row < flxPurchaseDetails.Rows.Count; row++)
                if (flxPurchaseDetails[row, "itmCode"] + "".ToString() == string.Empty) { break; }
                else { flxPurchaseDetails[row, 0] = row; }
        }

        private void save_Master_Datas(string firmcode)
        {
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@date1", dtpDate.Value.ToString("yyyy/MM/dd"));
            cmd.Parameters.AddWithValue("@suppCode", txtSupplier.Tag.ToString());
            cmd.Parameters.AddWithValue("@suppName", txtSupplier.Text.Trim());
            cmd.Parameters.AddWithValue("@invNo", txtInvNo.Text.Trim());
            cmd.Parameters.AddWithValue("@invDate", dateInvoice.Value.ToString("yyyy/MM/dd"));
            cmd.Parameters.AddWithValue("@invAmt", Convert.ToDecimal(txtInvAmount.Text.Trim()));
            cmd.Parameters.AddWithValue("@type", cmbPaymentMode.SelectedIndex + 1);
            cmd.Parameters.AddWithValue("@remarks", txtDescription.Text.Trim());
            cmd.Parameters.AddWithValue("@expense", Convert.ToDecimal(txtExpense.Text.Trim()));
            cmd.Parameters.AddWithValue("@roundOff", Convert.ToDecimal(txtRoundOff.Text.Trim()));
            cmd.Parameters.AddWithValue("@disAmt", Convert.ToDecimal(txtDiscAmount.Text.Trim()));
            cmd.Parameters.AddWithValue("@cstBill", cmbTaxMode.Text.Trim());
            cmd.Parameters.AddWithValue("@taxAmt", Convert.ToDecimal(lblTax.Text.Trim()));
            cmd.Parameters.AddWithValue("@cessAmt", Convert.ToDecimal(lblCess.Text.Trim()));
            cmd.Parameters.AddWithValue("@userID", cGen.GetUserDet());
            cmd.Parameters.AddWithValue("@cForm", "N");
            cmd.Parameters.AddWithValue("@cFormDate", dtpDate.Value.ToString("yyyy/MM/dd"));
          
            //if (this.txtPOrderno.Text.Trim() != string.Empty)
            //{ cmd.Parameters.AddWithValue("@PORDERNO", this.txtPOrderno.Text); }           

            if (cmd.CommandText.Contains("INSERT"))
            { txtNumber.Text = cmd.ExecuteScalar().ToString(); }
            else
            {
                cmd.Parameters.AddWithValue("@orderNo", txtNumber.Text.Trim());
                cmd.ExecuteNonQuery();
            }
        }

        private void save_Item_Details()
        {
            for (int i = 1; i < flxPurchaseDetails.Rows.Count; i++)
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (flxPurchaseDetails[i, "itmName"] + "".ToString() == string.Empty) { break; }
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@orderNo", txtNumber.Text.Trim());
                cmd.Parameters.AddWithValue("@date1", dtpDate.Value.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@itemCode", flxPurchaseDetails[i, "itmCode"].ToString().Trim());
                cmd.Parameters.AddWithValue("@itemName", flxPurchaseDetails[i, "itmName"].ToString().Trim());
                cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(flxPurchaseDetails[i, "qty"].ToString()));
                cmd.Parameters.AddWithValue("@foc", Convert.ToDecimal(flxPurchaseDetails[i, "foc"].ToString()));
                cmd.Parameters.AddWithValue("@price", Convert.ToDecimal(flxPurchaseDetails[i, "price"].ToString()));
                cmd.Parameters.AddWithValue("@netAmt", Convert.ToDecimal(flxPurchaseDetails[i, "netAmount"].ToString()));
                cmd.Parameters.AddWithValue("@discPer", Convert.ToDecimal(flxPurchaseDetails[i, "discPer"].ToString()));
                cmd.Parameters.AddWithValue("@discAmt", Convert.ToDecimal(flxPurchaseDetails[i, "discAmt"].ToString()));
                cmd.Parameters.AddWithValue("@taxPer", Convert.ToDecimal(flxPurchaseDetails[i, "taxPer"].ToString()));
                cmd.Parameters.AddWithValue("@taxAmt", Convert.ToDecimal(flxPurchaseDetails[i, "taxAmt"].ToString()));
                cmd.Parameters.AddWithValue("@accCode", flxPurchaseDetails[i, "accCode"].ToString().Trim());
                cmd.Parameters.AddWithValue("@cessPer", Convert.ToDecimal(flxPurchaseDetails[i, "cessPer"].ToString()));
                cmd.Parameters.AddWithValue("@cessAmt", Convert.ToDecimal(flxPurchaseDetails[i, "cessAmt"].ToString()));
                cmd.Parameters.AddWithValue("@lc", Convert.ToDecimal(flxPurchaseDetails[i, "lc"].ToString()));
                cmd.Parameters.AddWithValue("@tdiscount", Convert.ToDecimal(flxPurchaseDetails[i, "TdiscAmt"].ToString()));
                cmd.Parameters.AddWithValue("@Unit", flxPurchaseDetails[i, "Unit"].ToString().Trim());
                cmd.Parameters.AddWithValue("@UnitValue", Convert.ToDecimal(flxPurchaseDetails[i, "UnitValue"].ToString())); 

                if (cmbTaxMode.Text.Trim().ToUpper() == "CST")
                {
                    cmd.Parameters.AddWithValue("@POSTAMOUNT", Convert.ToDecimal(flxPurchaseDetails[i, "netAmount"].ToString()));
                    cmd.Parameters.AddWithValue("@VATAMT", 0);
                    cmd.Parameters.AddWithValue("@CESSPOSTAMT", 0);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@POSTAMOUNT", Convert.ToDecimal(flxPurchaseDetails[i, "netAmount"].ToString()) - Convert.ToDecimal(flxPurchaseDetails[i, "taxAmt"].ToString()) - Convert.ToDecimal(flxPurchaseDetails[i, "cessAmt"].ToString()));
                    cmd.Parameters.AddWithValue("@VATAMT", Convert.ToDecimal(flxPurchaseDetails[i, "taxAmt"].ToString()));
                    cmd.Parameters.AddWithValue("@CESSPOSTAMT", Convert.ToDecimal(flxPurchaseDetails[i, "cessAmt"].ToString()));
                }

                if (Convert.ToBoolean(flxPurchaseDetails[i, "bCode"])) { cmd.Parameters.AddWithValue("@barCode", "1"); }
                else { cmd.Parameters.AddWithValue("@barCode", "0"); }


                if (this.Tag.ToString() == "HE" && cPublic.AutoPriceUpdation.ToUpper() == "YES")
                {
                    cmd.Parameters.AddWithValue("@priceupdation", true);
                }
                else if (this.Tag.ToString() == "HM" && cPublic.AutoPriceUpdation.ToUpper() == "YES"
                    && chkPriceUpdation.Checked == true)
                {
                    cmd.Parameters.AddWithValue("@priceupdation", true);
                }

                cmd.ExecuteNonQuery();
            }
        }

        private void save_Screen()
        {
            try
            {
                for (int row = 1; row <= flxPurchaseDetails.Rows.Count; row++)
                {
                    if (flxPurchaseDetails[row, "itmName"] + "".ToString() == string.Empty) { break; }
                    calc_Line(row);
                }
                calc_Bill();
                if (validations())
                {
                    DialogResult dlg = MessageBox.Show("Sure to Save ?", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dlg == DialogResult.Yes)
                    {
                        CBillToBill cBill = new CBillToBill();
                        cBill.TransNo = txtNumber.Text.Trim();
                        cBill.TransType = Tag.ToString().Substring(0, 1);
                        cBill.TransFlag = "Y";
                        cBill.Date = dtpDate.Value;
                        cBill.InvNo = txtInvNo.Text.Trim();
                        cBill.InvDate = dateInvoice.Value;
                        cBill.BillAmt = Convert.ToDecimal(txtInvAmount.Text.Trim());
                        cBill.Cmd = cmd;
                        cBill.FirmCode = cPublic.g_firmcode;
                        cBill.Module = CBillToBill.module.Inventry;
                        if (this.Tag.ToString().Substring(1, 1) == "E")
                        { cBill.Mode = CBillToBill.mode.Entry; }
                        if (this.Tag.ToString().Substring(1, 1) == "M")
                        { cBill.Mode = CBillToBill.mode.Modify; }

                        if (this.Tag.ToString().Substring(1, 1) == "E")
                        {
                            cmd.Parameters.Clear();
                            cmd.CommandText = sp_Master;
                            cmd.CommandType = CommandType.StoredProcedure;
                            save_Master_Datas(cPublic.g_firmcode);
                        }

                        if (cmbPaymentMode.Text.Trim().ToLower() == "credit")
                        {
                            cBill.CustCode = txtSupplier.Tag + "";
                            if (!cBill.ShowSettlement()) { return; }
                        }

                        Trans = cPublic.Connection.BeginTransaction(IsolationLevel.ReadUncommitted);
                        cmd.Parameters.Clear();
                        cmd.CommandText = sp_Master;
                        cmd.Transaction = Trans;
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (this.Tag.ToString().Substring(1, 1) == "M")
                        {
                            cmd.CommandText = sp_Master;
                            save_Master_Datas(cPublic.g_firmcode);

                            cmd.Parameters.Clear();
                            cmd.CommandText = "SP_DELETE_" + subTable;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@orderno", txtNumber.Text);
                            if (vPurchOrderno > 0)
                            { cmd.Parameters.AddWithValue("@porderno", vPurchOrderno); }
                            cmd.ExecuteNonQuery();
                        }


                        cmd.CommandText = sp_Details;
                        save_Item_Details();

                        InvetryPosting(cPublic.g_firmcode);

                        cBill.Cmd = cmd;
                        cBill.SavePendingBill();

                        Trans.Commit();

                        MessageBox.Show("Entry Saved with Number " + txtNumber.Text.Trim(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        refresh_Form();
                        SetFlag();
                        SendKeys.Send("{Tab}");
                    }
                }
            }
            catch (Exception ex)
            {

                if (Trans != null)
                { Trans.Rollback(); }
                if (this.Tag.ToString().Substring(1, 1) == "E")
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "SP_DELETE_" + masterTable;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@orderNo", txtNumber.Text);
                    cmd.ExecuteNonQuery();
                }

                MessageBox.Show(ex.Message);
            }
        }

        private void InvetryPosting(string firmCode)
        {
            if (this.Tag.ToString().Substring(0, 1) == "I") { return; }
            cmd.Parameters.Clear();
            cmd.CommandText = "SP_POSTING_" + sp_Posting + firmCode;
            cmd.Parameters.AddWithValue("@orderno", txtNumber.Text.Trim());
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
        }

        private void refresh_Form()
        {
            txtNumber.Text = Convert.ToString(cGen.getMaxNumber(masterTable, "ORDERNO") + 1);
            flxPurchaseDetails.Col = 1;
            flxPurchaseDetails.Row = 1;
            flxPurchaseDetails.Clear();
            grid_initialize(flxPurchaseDetails);
            cmbPaymentMode.SelectedIndex = 1;
            // txtPOrderno.Text = string.Empty;
            txtInvNo.Text = string.Empty;
            txtSupplier.Text = string.Empty;
            txtSupplier.Tag = string.Empty;
            txtInvAmount.Text = "0.0000";
            txtDiscAmount.Text = "0.0000";
            txtExpense.Text = "0.0000";
            lblDiscount.Text = "0.0000";
            lblaftertaxamt.Text = "0.0000 ";
            lblInvAmount.Text = "0.0000";
            lblTax.Text = "0.0000";
            lblCess.Text = "0.0000";
            lblTotal.Text = "0.0000";
            txtRoundOff.Text = "0.0000";
            txtDescription.Text = string.Empty;
            cmbTaxMode.Enabled = true;
            cmbPaymentMode.Focus();
            cmbPaymentMode.SelectedIndex = 0;
            chkPriceUpdation.Checked = false;
            if (cPublic.AutoPriceUpdation.ToUpper() == "YES")
            { chkPriceUpdation.Visible = true; }
            else { chkPriceUpdation.Visible = false; }
        }

        private Boolean validations()
        {
            Boolean status = true;
            if ((Convert.ToDateTime(dtpDate.Value.ToShortDateString()) < Convert.ToDateTime(cPublic.g_OpDate.ToShortDateString())) || (Convert.ToDateTime(dtpDate.Value.ToShortDateString()) > Convert.ToDateTime(cPublic.g_ClDate.ToShortDateString())))
            { MessageBox.Show("Date is out of Financial Year. Please Check...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information); status = false; dtpDate.Focus(); }
            else if (cmbPaymentMode.SelectedItem.ToString().Equals("Credit") && txtSupplier.Text.Trim().Equals(string.Empty))
            {
                MessageBox.Show("Please select a Supplier..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtSupplier.Focus();
                status = false;
            }
            else if (cmbPaymentMode.SelectedItem.ToString().Equals("Credit") && !checkSupplier())
            { MessageBox.Show("Please Check the Supplier..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information); status = false; txtSupplier.Focus(); }
            else if (cmbPaymentMode.SelectedItem.ToString().Equals("Credit") && !checkInvoiceNo())
            { MessageBox.Show("Please Check the Invoice No..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information); status = false; txtInvNo.Focus(); }
            else if (txtInvNo.Text.Trim().Equals(string.Empty))
            { MessageBox.Show("Enter the Invoice No..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information); status = false; txtInvNo.Focus(); }
            else if (txtInvAmount.Text.Trim().Equals(string.Empty) || txtInvAmount.Text.Trim() == "0.00" || txtInvAmount.Text.Trim() == "0.0000")
            { MessageBox.Show("Enter the Invoice Amount..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information); status = false; txtInvAmount.Focus(); }
            else if (Convert.ToDecimal(txtInvAmount.Text) != Convert.ToDecimal(lblInvAmount.Text))
            {
                MessageBox.Show("Invoice Amount & BillAmount Not Tally..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information); status = false; txtInvAmount.Focus();
            }
            else if (!checkFlexGridDatas()) { status = false; }
            else if (flxPurchaseDetails[1, "itmName"] + "".ToString() == string.Empty)
            {
                status = false;
                MessageBox.Show("There are no items", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                flxPurchaseDetails.Focus();
                flxPurchaseDetails.Row = 1; flxPurchaseDetails.Col = 1; SendKeys.Send("{F2}");
            }
            //else if (Check_Master(txtPOrderno.Text, "PORDER" + cPublic.g_firmcode) == false && this.Tag.ToString().Substring(0, 1) == "H")
            //{
            //    status = false;
            //    MessageBox.Show("Purchase Order no Not Found..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning); this.txtPOrderno.Focus(); this.txtPOrderno.SelectAll();
            //}
            //else if (Check_Master(txtPOrderno.Text, "PURCHASE" + cPublic.g_firmcode) == false && this.Tag.ToString().Substring(0, 1) == "U")
            //{
            //    status = false;
            //    MessageBox.Show("Purchase No Not Found..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning); this.txtPOrderno.Focus(); this.txtPOrderno.SelectAll();
            //}
            return status;
        }

        private Boolean checkSupplier()
        {
            Boolean status = true;
            sqlQuery = "select  * from ACCOUNTS" + cPublic.g_firmcode + " where CODE = @code and HEAD = @head";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@code", txtSupplier.Tag.ToString());
            cmd.Parameters.AddWithValue("@head", txtSupplier.Text.Trim());
            if (cmd.ExecuteScalar() == null)
            { status = false; }
            return status;
        }

        private Boolean checkInvoiceNo()
        {
            Boolean status = true;
            if (this.Tag.ToString().Substring(1, 1) == "M")
                sqlQuery = "select * from " + masterTable + " where INVNO = @invNo and SUPPCODE = @supCode and ORDERNO <> @orederno";
            else
                sqlQuery = "select * from " + masterTable + " where INVNO = @invNo and SUPPCODE = @supCode";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@invNo", txtInvNo.Text.Trim());
            cmd.Parameters.AddWithValue("@supCode", txtSupplier.Tag.ToString());
            if (this.Tag.ToString().Substring(1, 1) == "M")
            { cmd.Parameters.AddWithValue("@orederno", txtNumber.Text.Trim()); }
            if (cmd.ExecuteScalar() != null) { status = false; }

            return status;
        }

        private Boolean checkFlexGridDatas()
        {
            Boolean status = true;
            for (int r = 1; r < flxPurchaseDetails.Rows.Count - 1; r++)
                if (flxPurchaseDetails[r, "itmName"] + "".ToString() == string.Empty) { break; }
                else
                {
                    string itmCode = flxPurchaseDetails[r, "itmCode"].ToString();
                    string itmName = flxPurchaseDetails[r, "itmName"].ToString();
                    if (!((flxPurchaseDetails[r, "itmCode"] + "").StartsWith("SL")))
                    {
                        if (!is_Valid_Item(itmCode, itmName))
                        {
                            status = false;
                            flxPurchaseDetails.Row = r;
                            flxPurchaseDetails.Col = flxPurchaseDetails.Cols["itmCode"].Index;
                            flxPurchaseDetails.Focus();
                            return status;
                        }
                    }

                    if (flxPurchaseDetails[r, "qty"].ToString().Equals("") ||
                       flxPurchaseDetails[r, "qty"].ToString() == "0.000" ||
                       flxPurchaseDetails[r, "qty"].ToString().Equals(".") ||
                       flxPurchaseDetails[r, "qty"].ToString() == "0")
                    {
                        status = false;
                        MessageBox.Show("Enter the Quantity...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        flxPurchaseDetails.Row = r;
                        flxPurchaseDetails.Col = flxPurchaseDetails.Cols["qty"].Index;
                        flxPurchaseDetails.Focus();
                        break;
                    }
                   
                }
            return status;
        }

        private Boolean isvalidlookupitem(string field, string code)
        {
            Boolean status = false;
            sqlQuery = "select * from LOOKUP where field1 = @field and Code = @code";
            SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@field", field);
            cmd.Parameters.AddWithValue("@code", code);
            if (cmd.ExecuteScalar() != null) { status = true; }
            return status;
        }

        private Boolean fill_Row(int row, string ItemCode)
        {
            if (row >= flxPurchaseDetails.Rows.Count - 1)
            { flxPurchaseDetails.Rows.Count = row + 5; }

            SqlDataReader dr;
            sqlQuery = "select * from stockmst" + cPublic.g_firmcode + " where itemcode = @itemCode";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@itemCode", ItemCode);
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {

                fill_DefultVAlues(row);
                if (dr["itemname"].ToString() == string.Empty)
                { dr.Close(); MessageBox.Show("ItemCode Not Found.. Please Check.", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information); return false; }
                flxPurchaseDetails[row, "itmCode"] = dr["itemcode"].ToString();
                flxPurchaseDetails[row, "itmName"] = dr["itemname"].ToString();
                flxPurchaseDetails[row, "taxPer"] = dr["taxper"].ToString();
                flxPurchaseDetails[row, "Unit"] = dr["Unit"].ToString();
                flxPurchaseDetails[row, "UnitValue"] = 1;
                flxPurchaseDetails[row, "multiunit"] = dr["multiunit"].ToString();

                if (cmbTaxMode.SelectedItem.ToString() == "VAT")
                { flxPurchaseDetails[row, "cessPer"] = dr["cessper"].ToString(); }
                else { flxPurchaseDetails[row, "cessPer"] = 0.00; }

                if (dr["multiunit"].ToString() == "Y")
                    flxPurchaseDetails.Cols["Unit"].AllowEditing = true;
                else
                    flxPurchaseDetails.Cols["Unit"].AllowEditing = false;

                flxPurchaseDetails[row, "accCode"] = getAccHead(cmbTaxMode.Text.Trim(), Convert.ToDecimal(dr["taxper"].ToString()));
           
            }
            else { MessageBox.Show("Itemcode not Found.."); dr.Close(); return false; }
            dr.Close();

            sqlQuery = "select top 1 * from PITEM" + cPublic.g_firmcode + " where ITEMCODE = @ItemCode and orderno<>@orderno order by orderno desc";
            cmd.CommandText = sqlQuery;
            cmd.Parameters.AddWithValue("@orderno", this.txtNumber.Text.Trim());
            cmd.Parameters.AddWithValue("@unit", flxPurchaseDetails[row, "Unit"]);
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {

                flxPurchaseDetails[row, "price"] = Convert.ToDecimal(dr["PRICE"].ToString()) ;
                //flxPurchaseDetails[row, "taxPer"] = dr["TAXPER"].ToString();
                //if (cmbTaxMode.SelectedItem.ToString() == "VAT")
                //{ flxPurchaseDetails[row, "cessPer"] = dr["CESSPER"].ToString(); }
                //else { flxPurchaseDetails[row, "cessPer"] = 0.00; }
            }
            dr.Close();
            return true;
        }

        private void fill_DefultVAlues(int row)
        {
            decimal bulkdisc = 0;
            flxPurchaseDetails[row, "bCode"] = true;
            flxPurchaseDetails[row, "qty"] = 0;
            flxPurchaseDetails[row, "foc"] = 0;
            flxPurchaseDetails[row, "price"] = 0;
            if (Convert.ToDecimal(txtInvAmount.Text.Trim()) == 0) { txtInvAmount.Text = "1.00"; }
            if (Convert.ToDecimal(txtDiscAmount.Text.Trim()) != 0)
            {
                bulkdisc = (Convert.ToDecimal(txtDiscAmount.Text.Trim()) / Convert.ToDecimal(txtInvAmount.Text.Trim())) * 100;
                if (bulkdisc > 100) { bulkdisc = 100; } 
            }

            flxPurchaseDetails[row, "discper"] = bulkdisc;
            flxPurchaseDetails[row, "taxPer"] = 0;
            flxPurchaseDetails[row, "taxAmt"] = 0;
            flxPurchaseDetails[row, "cessPer"] = 0;
            flxPurchaseDetails[row, "cessAmt"] = 0;
            flxPurchaseDetails[row, "netAmount"] = 0;
            flxPurchaseDetails[row, "discamt"] = 0;

            flxPurchaseDetails[row, "Unit"] = "";
            flxPurchaseDetails[row, "UnitValue"] = 1.00;
            flxPurchaseDetails[row, "multiunit"] = "N";
        }

        private void loadSuppLookup(string str)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;

            lookup.m_table = "ACCOUNTS" + cPublic.g_firmcode;
            lookup.m_fields = "HEAD,CODE,Address,Street,City,Balance";
            lookup.m_fldwidth = "350,0,0,150,150,100,0,0";
            lookup.m_strat = str;
            lookup.m_condition = "groupcode = '100012'";
            lookup.m_dispname = "Account Head, Code, Address,Street,Place,Balance";
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                txtSupplier.Text = lookup.m_values[0].ToString();
                txtSupplier.Tag = lookup.m_values[1].ToString();
                SendKeys.Send("{Enter}");
            }
            else
            {
                txtSupplier.Text = string.Empty;
                txtSupplier.Tag = string.Empty;
            }
        }

        private void enableControls(Boolean status)
        {
            txtNumber.Enabled = status;
            dtpDate.Enabled = status;
            dateInvoice.Enabled = status;
           // txtPOrderno.Enabled = status;
            txtInvNo.Enabled = status;
            txtSupplier.Enabled = status;
            txtInvAmount.Enabled = status;
            txtDiscAmount.Enabled = status;
            txtDescription.Enabled = status;
            txtExpense.Enabled = status;
            cmbPaymentMode.Enabled = status;
            cmbTaxMode.Enabled = status;
            btnSave.Enabled = status;
        }

        private Boolean Check_Master(string number, string vmastertable)
        {
            Boolean status = false;

            if (number.Trim() == string.Empty) { return true; }
            if (this.Tag.ToString().Substring(1, 1) != "E") { return true; }

            sqlQuery = "select * from " + vmastertable + " where ORDERNO = @number";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@number", number);
            if (cmd.ExecuteScalar() != null)
            { status = true; }
            return status;
        }

        private Boolean is_Valid_Item(string itemCode, string itemName)
        {
            sqlQuery = "select * from stockmst" + cPublic.g_firmcode + " where itemcode = @itmcode";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@itmcode", itemCode);
            if (cmd.ExecuteScalar() == null)
            {
                MessageBox.Show("Invalid Itemcode..  Please Check..");
                return false;
            }
            return true;
        }

        private Boolean delete_Purchase_Bill()
        {            
            Boolean status = false;
            DataTable dt = new DataTable();
            try
            {
                Trans = cPublic.Connection.BeginTransaction();
                cmd.Parameters.Clear();
                cmd.Transaction = Trans;
                cmd.CommandText = sp_Master; 
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@orderNo", txtNumber.Text.Trim());
                cmd.ExecuteNonQuery();

                cmd.CommandText = sp_Details;
                //if (this.txtPOrderno.Text.Trim() != string.Empty && this.Tag.ToString().Substring(0, 1) == "H")
                //{ cmd.Parameters.AddWithValue("@porderNo", Convert.ToInt64(this.txtPOrderno.Text.Trim())); }
                cmd.ExecuteNonQuery();

                InvetryPosting(cPublic.g_firmcode);

                CBillToBill cBill = new CBillToBill();
                cBill.TransNo = txtNumber.Text.Trim();
                cBill.TransType = Tag.ToString().Substring(0, 1);
                cBill.TransFlag = "Y";
                cBill.Date = dtpDate.Value;
                cBill.InvNo = txtInvNo.Text.Trim();
                cBill.InvDate = dateInvoice.Value;
                cBill.BillAmt = Convert.ToDecimal(txtInvAmount.Text.Trim());
                cBill.Cmd = cmd;
                cBill.FirmCode = cPublic.g_firmcode;
                cBill.Module = CBillToBill.module.Inventry;
                cBill.Mode = CBillToBill.mode.Cancel;
                cBill.SavePendingBill();


                Trans.Commit();

                status = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Trans.Rollback();
            }
            return status;
        }


        private void ucPurchase_Load(object sender, EventArgs e)
        {
            Load_Screen();

            // Create the ToolTip and associate with the Form container.
            ToolTip toolTip1 = new ToolTip();

            // Set up the delays for the ToolTip.
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 1000;
            toolTip1.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip1.ShowAlways = true;

            // Set up the ToolTip text for the Button and Checkbox.
            toolTip1.SetToolTip(this.btnSerach, "Search");
            toolTip1.SetToolTip(this.btnNew, "New");
            toolTip1.SetToolTip(this.btnSave, "Save");
            toolTip1.SetToolTip(this.btnCancel, "Cancel");
            toolTip1.SetToolTip(this.btnExit, "Exit");
            toolTip1.SetToolTip(this.btnSavePrint, "Print");

        }

        private void Load_Screen()
        {
            grid_initialize(flxPurchaseDetails);
            set_Master_Sub_Tables();
            set_Master_Sub_SPs();
            set_others();
            cmbPaymentMode.SelectedIndex = 0;
            cmbTaxMode.Text = "Vat";
            grid_initialize(flxPurchaseDetails);
            refresh_Form();


           
        }

        private void txtNumber_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.SelectAll();
        }

        private void txtNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;
                lookup.m_table = masterTable;
                lookup.m_fields = "ORDERNO,DATE1,SUPPNAME,INVNO,INVAMT";
                lookup.m_dispname = "ORDERNO,DATE,SUPPLIER,INVNO,INVAMT";
                lookup.m_condition = "";
                lookup.m_order = "ORDERNO desc";
                lookup.m_fldwidth = "100,100,250,100,100";
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtNumber.Text = lookup.m_values[0].ToString();
                    SendKeys.Send("{ENTER}");
                }
            }
        }

        private void txtNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= Convert.ToChar(Keys.D0) && e.KeyChar <= Convert.ToChar(Keys.D9) || e.KeyChar == Convert.ToChar(Keys.Back)) { }
            else { e.Handled = true; }

            //if (e.KeyChar == Convert.ToChar(Keys.Enter) && txtPOrderno.Text.Trim() != string.Empty)
            //{
            //    fill_PurchaseOrder();
            //}
        }

        private void txtNumber_Validating(object sender, CancelEventArgs e)
        {
            if (!cGen.isFormClosing())
            {
                if (txtNumber.Text == string.Empty)
                {
                    MessageBox.Show("Please type Purchase Entry Number.......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    enableControls(false);
                    txtNumber.Enabled = true ;
                    flxPurchaseDetails.Enabled = false;
                    btnSave.Enabled = false;
                    btnSavePrint.Enabled = false;
                    btnCancel.Enabled = false;  
                 //   txtNumber.Focus();
                }
                else
                {
                    if (fill_Master(txtNumber.Text, masterTable))
                    {
                        fill_Details(txtNumber.Text, subTable);

                        checkSupplier();

                        enableControls(true); txtNumber.Enabled = false; cmbTaxMode.Enabled = false;
                        
                        flxPurchaseDetails.Enabled = true;
                        btnSave.Enabled = true;
                        btnSavePrint.Enabled = true;
                        btnCancel.Enabled = true ;  
                    }
                    else
                    {
                        MessageBox.Show("Purchase Entry Not Found..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //txtNumber.Focus();

                        enableControls(false);
                        txtNumber.Enabled = true;
                        flxPurchaseDetails.Enabled = false;
                        btnSave.Enabled = false;
                        btnSavePrint.Enabled = false;
                        btnCancel.Enabled = false;  
                    }
                }
            }
        }

        private void txtTax_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;

            TextBox tx = new TextBox();

            tx.Text = tb.Text;

            gen.ValidDecimalNumber(e, ref tx, 10, 4);
        }

        private void txtTax_Validating(object sender, CancelEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (Information.IsNumeric(tb.Text.Trim()) == false)
            { tb.Text = "0.0000"; }
            else
            { tb.Text = Convert.ToDecimal(tb.Text.Trim()).ToString("##,###0.0000"); }

            switch (tb.Name)
            {
                case "txtInvAmount":
                    p_InvAmount = Convert.ToDecimal(txtInvAmount.Text.Trim());
                    break;
                case "txtDiscAmount":
                    p_Bulk_Disc = Convert.ToDecimal(txtDiscAmount.Text);
                    break;
                case "txtExpense":
                    p_expense = Convert.ToDecimal(txtExpense.Text.Trim());
                    break;
            }

            for (int i = 1; i < flxPurchaseDetails.Rows.Count; i++)
            {
                if ((flxPurchaseDetails[i, "itmName"] + "").Trim() == string.Empty) { break; }
                calc_Line(i);
            }
            calc_Bill();
        }

        private void flxPurchaseDetails_BeforeRowColChange(object sender, RangeEventArgs e)
        {
            calc_Line(e.OldRange.r1);
            if (e.OldRange.r1 != e.NewRange.r1) { calc_Bill(); }
            if (flxPurchaseDetails.Cols["price"].Index == e.OldRange.c1)
            { calc_Bill(); }
        }

        private void flxPurchaseDetails_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{F2}");
        }

        private void flxPurchaseDetails_Enter(object sender, EventArgs e)
        {
            //if (txtInvNo.Text.Trim() == string.Empty)
            //{
            //    MessageBox.Show("Enter invoice Number...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtInvNo.Focus();
            //    return;
            //}
            //else if (txtInvAmount.Text.Trim() != string.Empty && Convert.ToDecimal(txtInvAmount.Text) == 0)
            //{
            //    MessageBox.Show("Enter invoice amount", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtInvAmount.Focus();
            //    return;
            //}
            SendKeys.Send("{F2}");
        }

        private void flxPurchaseDetails_EnterCell(object sender, EventArgs e)
        {
            int row = flxPurchaseDetails.Row;
            int col = flxPurchaseDetails.Col;

            if (flxPurchaseDetails[flxPurchaseDetails.Row - 1, "itmName"] + "".ToString() == string.Empty)
            { flxPurchaseDetails.Row -= 1; }

            if (flxPurchaseDetails[flxPurchaseDetails.Row, "itmName"] + "".ToString().Trim() == string.Empty)
            { flxPurchaseDetails.Col = flxPurchaseDetails.Cols["itmCode"].Index; }

            if (flxPurchaseDetails[flxPurchaseDetails.Row, "itmCode"] + "".ToString() != string.Empty)
            {
                flxPurchaseDetails.Cols["taxPer"].AllowEditing = true;
                flxPurchaseDetails.Cols["cessPer"].AllowEditing = true;
            }
            if (flxPurchaseDetails.ColSel == flxPurchaseDetails.Cols["Unit"].Index)
            {
                if (flxPurchaseDetails[flxPurchaseDetails.Row, "multiunit"] + "".ToUpper() == "Y" )
                { flxPurchaseDetails.Cols["Unit"].AllowEditing = true; }
                else
                { flxPurchaseDetails.Cols["Unit"].AllowEditing = false; }
            }
            if (flxPurchaseDetails.Col == flxPurchaseDetails.Cols["itmName"].Index && flxPurchaseDetails[flxPurchaseDetails.Row, "itmCode"] + "" != "")
            {
                if (!((flxPurchaseDetails[flxPurchaseDetails.Row, "itmCode"] + "").StartsWith("SL")))
                {
                    SendKeys.Send("{Tab}");
                }
            }

            if (flxPurchaseDetails.Cols[flxPurchaseDetails.Col].AllowEditing == false &&
                flxPurchaseDetails.Cols["bCode"].Index != flxPurchaseDetails.Col)
            { SendKeys.Send("{Tab}"); }
            else
            {
                if (flxPurchaseDetails.Cols["price"].Index == flxPurchaseDetails.Col)
                { flxPurchaseDetails.Cols["price"].AllowEditing = true; }
            }

            

            SendKeys.Send("{F2}");
        }

        private void flxPurchaseDetails_KeyDownEdit(object sender, KeyEditEventArgs e)
        {
            if (flxPurchaseDetails.Cols["itmCode"].Index == e.Col && e.KeyCode == Keys.F5)
            {
                string fields = string.Empty;
                string dispName = string.Empty;
                string width = string.Empty;
                if (cPublic.LookupDefault == "Itemcode")
                {
                    fields = "itemcode,itemname,Qty,rprofit1,taxper,cessper,groupcode";
                    dispName = "Item Code, Item Name,Qty, Retail, Tax %, Cess %,Group";
                    width = "80,250,80,80,100,50,100";
                }
                else
                {
                    fields = "itemname,itemcode,qty,rprofit1,taxper,cessper,groupcode";
                    dispName = "Item Name, Item Code, Qty, Retail, Tax %, Cess %,Group";
                    width = "250,80,80,80,100,50,100";
                }
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;
                lookup.m_table = "stockmst" + cPublic.g_firmcode;
                lookup.m_fields = fields;
                lookup.m_dispname = dispName;
                lookup.m_fldwidth = width;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    if (cPublic.LookupDefault == "Itemcode") { flxPurchaseDetails.Editor.Text = lookup.m_values[0].ToString(); }
                    else { flxPurchaseDetails.Editor.Text = lookup.m_values[1].ToString(); }
                    SendKeys.Send("{Tab}");
                }
            }

            if (flxPurchaseDetails.Cols["itmName"].Index == e.Col && e.KeyCode == Keys.F5)
            {
                chklookup = true;
                string fields = string.Empty;
                string dispName = string.Empty;
                string width = string.Empty;
                fields = "itemname,itemcode,qty,rprofit1,taxper,cessper,groupcode";
                dispName = "Item Name, Item Code, Qty, Retail, Tax %, Cess %,Group";
                width = "250,80,80,80,100,50,100";
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;
                lookup.m_table = "stockmst" + cPublic.g_firmcode;
                lookup.m_search = true;
                lookup.m_fields = fields;
                lookup.m_dispname = dispName;
                lookup.m_fldwidth = width;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    flxPurchaseDetails.Editor.Text = lookup.m_values[0].ToString();
                    flxPurchaseDetails[e.Row, "itmCode"] = lookup.m_values[1].ToString();
                    SendKeys.Send("{Tab}");
                }
            }
            if (flxPurchaseDetails.Cols["Unit"].Index == e.Col && e.KeyCode == Keys.F5)
            {
                string table = "(select unit,1 value,mrp from stockmst" + cPublic.g_firmcode + "  where  itemcode='" + flxPurchaseDetails[e.Row, "itmCode"].ToString() + "' " + Constants.vbCrLf
                + " union all " + Constants.vbCrLf
                + " select unit,value,mrp from multiunit" + cPublic.g_firmcode + " where  itemcode='" + flxPurchaseDetails[e.Row, "itmCode"].ToString() + "' ) Sn ";
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;
                lookup.m_table = table;
                lookup.m_fields = "unit,value,mrp";
                lookup.m_dispname = "Unit,Value,Mrp";
                lookup.m_fldwidth = "100,100,100";
                lookup.m_condition = " ";
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    flxPurchaseDetails.Editor.Text = lookup.m_values[0].ToString();
                    flxPurchaseDetails[e.Row, "Unit"] = lookup.m_values[0].ToString();
                    if (Convert.ToDecimal(lookup.m_values[1].ToString()) == 0)
                    { flxPurchaseDetails[e.Row, "UnitValue"] = 1.00; }
                    else
                    { flxPurchaseDetails[e.Row, "UnitValue"] = Convert.ToDecimal(lookup.m_values[1].ToString()).ToString("#0.0000"); }
                    SendKeys.Send("{Tab}");
                }
            }
        }

        private void flxPurchaseDetails_KeyPressEdit(object sender, KeyPressEditEventArgs e)
        {
            C1FlexGrid cfg = (C1FlexGrid)sender;
            if (cfg.Editor == null) { return; }
            //if (flxPurchaseDetails.Cols["price"].Index == e.Col) { e.Handled = true; }
            if (flxPurchaseDetails.Cols[e.Col].DataType == typeof(decimal))
            {
                if (flxPurchaseDetails.Cols["qty"].Index == e.Col || flxPurchaseDetails.Cols["foc"].Index == e.Col)
                { gen.ValidFlexNumber(e, ref  cfg, 10, 3); }
                else if (flxPurchaseDetails.Cols["discamt"].Index == e.Col)
                { }
                else { gen.ValidFlexNumber(e, ref cfg, 10, 4); }
            }
        }

        private void flxPurchaseDetails_LeaveCell(object sender, EventArgs e)
        {
            if (flxPurchaseDetails[1, "itmName"] + "".ToString() != string.Empty) { cmbTaxMode.Enabled = false; }
            else { cmbTaxMode.Enabled = true; }
            setSlNo();
            if (flxPurchaseDetails.Cols["itmCode"].Index == flxPurchaseDetails.Col)
                if (flxPurchaseDetails[flxPurchaseDetails.Row, "itmCode"] + "".ToString() == "0")
                { btnSave.Focus(); }
        }

        private void flxPurchaseDetails_SetupEditor(object sender, RowColEventArgs e)
        {
            TextBox tb = (TextBox)flxPurchaseDetails.Editor;
            if (flxPurchaseDetails.Cols["itmCode"].Index == e.Col)
            { tb.CharacterCasing = CharacterCasing.Upper; tb.MaxLength = 13; }
            if (flxPurchaseDetails.Cols["itmName"].Index == e.Col)
            { tb.MaxLength = 40; }
          }

        private void flxPurchaseDetails_ValidateEdit(object sender, ValidateEditEventArgs e)
        {
            if (flxPurchaseDetails[e.Row, "itmName"] + "".ToString() == string.Empty)
            { flxPurchaseDetails[e.Row, e.Col] = ""; }
            if (e.Row + 4 > flxPurchaseDetails.Rows.Count)
            { flxPurchaseDetails.Rows.Count += 1; }
            if (flxPurchaseDetails.Cols["discamt"].Index == e.Col && flxPurchaseDetails.Editor != null)
            {
                if (Information.IsNumeric(flxPurchaseDetails.Editor.Text) == false)
                { flxPurchaseDetails.Editor.Text = "0.0000"; }
            }

            if (!cGen.isFormClosing())
            {
                if (flxPurchaseDetails.Cols["itmCode"].Index == e.Col)
                {
                    if (flxPurchaseDetails.Editor != null && flxPurchaseDetails.Editor.Text.Trim() != "")
                    {
                        flxPurchaseDetails.Editor.Text = flxPurchaseDetails.Editor.Text.ToUpper();
                        if (flxPurchaseDetails.Editor.Text.Trim() != (flxPurchaseDetails[e.Row, e.Col] + "").ToString().Trim())
                        {
                            if (CheckItem(flxPurchaseDetails.Editor.Text.Trim()))
                            {
                                if (is_Valid_Item(flxPurchaseDetails.Editor.Text.Trim(), ""))
                                {
                                    if (fill_Row(e.Row, flxPurchaseDetails.Editor.Text.Trim()))
                                    {
                                        e.Cancel = false;
                                    }
                                    else
                                    {
                                        e.Cancel = true;
                                        flxPurchaseDetails[e.Row, e.Col] = "";
                                        TextBox tb = (TextBox)flxPurchaseDetails.Editor; tb.SelectAll();
                                        return;
                                    }
                                }
                                else
                                {
                                    e.Cancel = true;
                                    flxPurchaseDetails[e.Row, e.Col] = "";
                                    TextBox tb = (TextBox)flxPurchaseDetails.Editor; tb.SelectAll();
                                    return;
                                }
                            }
                            else
                            {
                                e.Cancel = true;
                                TextBox tb = (TextBox)flxPurchaseDetails.Editor; tb.SelectAll();
                            }
                        }
                        else { e.Cancel = false; }
                    }
                    else
                    {
                        if (flxPurchaseDetails[e.Row, "itmname"] + "".ToString() != string.Empty)
                        {
                            flxPurchaseDetails.Rows.Remove(e.Row);
                            flxPurchaseDetails.Editor.Text = flxPurchaseDetails[e.Row, e.Col] + "".ToString();
                            if (flxPurchaseDetails.Rows.Count < 23)
                            { flxPurchaseDetails.Rows.Count += 1; }
                            SendKeys.Send("{F2}");
                        }
                    }
                }
                else if (flxPurchaseDetails.Cols["itmName"].Index == e.Col)
                {
                    if (flxPurchaseDetails.Editor != null && flxPurchaseDetails.Editor.Text.Trim() != string.Empty)
                    {
                        if (chklookup == false)
                        {
                            if (!((flxPurchaseDetails[e.Row, "itmCode"] + "").StartsWith("SL")))
                            {
                                if (!GetItemName(flxPurchaseDetails.Editor.Text.Trim()))
                                {
                                    MessageBox.Show("No Items Found !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    e.Cancel = true; ((TextBox)flxPurchaseDetails.Editor).SelectAll(); return;
                                }
                                if (flxPurchaseDetails[e.Row, "itmName"] + "".ToString() != flxPurchaseDetails.Editor.Text && flxPurchaseDetails[e.Row, "itmCode"] + "" == string.Empty)
                                {
                                    string value = string.Empty;

                                    string ItemName = flxPurchaseDetails.Editor.Text.Trim();

                                    if (value == string.Empty)
                                    { value = flxPurchaseDetails.Editor.Text.Trim(); }

                                    frmlookup lookup = new frmlookup();
                                    lookup.m_con = cPublic.Connection;
                                    lookup.m_caption = "Item Details";
                                    lookup.m_searchmrp = true;
                                    lookup.m_table = "stockmst" + cPublic.g_firmcode;

                                    lookup.m_condition = "itemname like '%" + value + "%' and itemname<>''";
                                    lookup.m_fields = "itemname,itemcode,eancode,qty,rprofit1,taxper,cessper,groupcode,acccode,firmcode";
                                    lookup.m_dispname = "Item Name, Item Code,Sec. Code, Qty, Retail, Tax %, Cess %,Group,AccCode,Firm";
                                    lookup.m_fldwidth = "250,80,80,80,100,50,0,100,70,50";
                                    SendKeys.Send("{Tab}");
                                    lookup.ShowDialog();
                                    if (lookup.m_values.Count > 0)
                                    {
                                        if (fill_Row(e.Row, (lookup.m_values[2].ToString())))
                                        {
                                            flxPurchaseDetails.Editor.Text = lookup.m_values[0].ToString();
                                            if (!((flxPurchaseDetails[e.Row, "itmCode"] + "").StartsWith("SL")))
                                            {
                                                e.Cancel = false;
                                                SendKeys.Send("{Tab}");
                                            }
                                            else
                                            { flxPurchaseDetails.Cols["itmName"].AllowEditing = true; }

                                        }
                                        else
                                        {
                                            if (fill_Row(e.Row, (lookup.m_values[1].ToString())))
                                            {
                                                flxPurchaseDetails.Editor.Text = lookup.m_values[0].ToString();
                                                if (!((flxPurchaseDetails[e.Row, "itmCode"] + "").StartsWith("SL")))
                                                {
                                                    e.Cancel = false;
                                                    SendKeys.Send("{Tab}");
                                                }
                                                else
                                                { flxPurchaseDetails.Cols["itmName"].AllowEditing = true; }
                                            }
                                            else
                                            { e.Cancel = true; ((TextBox)flxPurchaseDetails.Editor).SelectAll(); }
                                        }
                                    }
                                    else { e.Cancel = true; ((TextBox)flxPurchaseDetails.Editor).SelectAll(); }
                                }

                            }
                            else
                            {
                                if (!((flxPurchaseDetails[e.Row, "itmCode"] + "").StartsWith("SL")))
                                { SendKeys.Send("{Tab}"); }
                                else
                                { flxPurchaseDetails.Cols["itmName"].AllowEditing = true; }
                            }

                        }
                        else
                        {

                            if (flxPurchaseDetails[e.Row, "itmCode"] + "".ToString() != "")
                            {
                                fill_Row(e.Row, flxPurchaseDetails[e.Row, "itmCode"].ToString());

                                if (!((flxPurchaseDetails[e.Row, "itmCode"] + "").StartsWith("SL")))
                                { SendKeys.Send("{Tab}"); }
                                else
                                { flxPurchaseDetails.Cols["itmName"].AllowEditing = true; }

                                e.Cancel = false;
                            }
                            else
                            { e.Cancel = true; ((TextBox)flxPurchaseDetails.Editor).SelectAll(); }
                            chklookup = false;
                        }
                    }
                    if ((flxPurchaseDetails[e.Row, "itmCode"] + "" != "") && flxPurchaseDetails.Editor != null && flxPurchaseDetails.Editor.Text.Trim() == string.Empty)
                    {
                        e.Cancel = true;
                    }
                    else
                        e.Cancel = false;
                }
                else if (flxPurchaseDetails.Cols["Unit"].Index == e.Col && flxPurchaseDetails[e.Row, "itmCode"] + "".ToString() != string.Empty && flxPurchaseDetails[e.Row, "multiunit"] + "".ToString().ToUpper() == "Y")
                {
                    if (flxPurchaseDetails.Editor != null && flxPurchaseDetails.Editor.Text.Trim() != "")
                    {
                        flxPurchaseDetails.Editor.Text = flxPurchaseDetails.Editor.Text.ToUpper().Trim();
                        if (!isvalidunit(flxPurchaseDetails[e.Row, "itmCode"] + "".ToString(), flxPurchaseDetails.Editor.Text.Trim(), e.Row))
                        {
                            e.Cancel = true;
                            TextBox tb = (TextBox)flxPurchaseDetails.Editor; tb.SelectAll();
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                        TextBox tb = (TextBox)flxPurchaseDetails.Editor; tb.SelectAll();
                    }
                }
            }
            generalCalcs(e);
            flxPurchaseDetails.Cols["rate"].AllowEditing = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            save_Screen();
        }

        private void btnSavePrint_Click(object sender, EventArgs e)
        {
            save_Screen();
        }

        private void txtSupplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { loadSuppLookup(""); }
        }

        private void txtSupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (cmbPaymentMode.SelectedItem.ToString().Trim().Equals("Credit"))
            {
                if (!e.KeyChar.Equals('\r') && !e.KeyChar.Equals('\b'))
                {
                    e.Handled = true;
                    loadSuppLookup(txtSupplier.Text.Trim() + e.KeyChar.ToString());
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.Tag = this.Tag.ToString().Substring(0, 1) + "M";
            Load_Screen();
            txtNumber.Focus();
        }

        private void Delete_Screen()
        {
            if (this.Tag.ToString().Substring(1, 1) == "C")
            {
                DialogResult dlg;
                dlg = MessageBox.Show("Are you sure to Cancel this Entry..?", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dlg == DialogResult.Yes)
                {
                    if (delete_Purchase_Bill())
                    {
                        MessageBox.Show("   Entry Cancelled Successfully..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        refresh_Form();

                        SetFlag();
                    }
                }               
            }
        }

        private void SetFlag()
        {
            this.Tag = this.Tag.ToString().Substring(0, 1) + "E";
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.Tag = this.Tag.ToString().Substring(0, 1) + "E";
            Load_Screen();

            flxPurchaseDetails.Enabled = true;
            btnSave.Enabled = true;
            btnSavePrint.Enabled = true;
            btnCancel.Enabled = false;

            enableControls(true);
            txtNumber.Enabled = false;  
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Tag = this.Tag.ToString().Substring(0, 1) + "C";
            set_Master_Sub_SPs();
            Delete_Screen();
        }
        private Boolean isvalidunit(string itemcode, string unit, int row)
        {
            Boolean status = false;
            sqlQuery = "Select * from  (select itemcode,unit,1 value,mrp from stockmst" + cPublic.g_firmcode + " where itemcode=@itemcode and unit=@unit " + Constants.vbCrLf
                + " union all " + Constants.vbCrLf
                + " select itemcode,unit,value,mrp from multiunit" + cPublic.g_firmcode + "  where itemcode=@itemcode and unit=@unit   ) Sn " + Constants.vbCrLf
                + "  ";
            SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@itemcode", itemcode);
            cmd.Parameters.AddWithValue("@unit", unit);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                status = true;
                flxPurchaseDetails[row, "Unit"] = dr["unit"].ToString();
                if (Convert.ToDecimal(dr["value"].ToString()) == 0)
                { flxPurchaseDetails[row, "UnitValue"] = 1.00; }
                else
                { flxPurchaseDetails[row, "UnitValue"] = Convert.ToDecimal(dr["value"].ToString()).ToString("#0.0000"); }
            }
            dr.Close();
            return status;
        }

        private void btnSerach_MouseHover(object sender, EventArgs e)
        {
            btnSerach.BackgroundImage = global::LotzInventory.Properties.Resources.search_ovr;
        }

        private void btnSerach_MouseLeave(object sender, EventArgs e)
        {
            btnSerach.BackgroundImage = global::LotzInventory.Properties.Resources.search;
        }

        private void btnNew_MouseHover(object sender, EventArgs e)
        {
            btnNew.BackgroundImage = global::LotzInventory.Properties.Resources.new_ovr;
        }

        private void btnNew_MouseLeave(object sender, EventArgs e)
        {
            btnNew.BackgroundImage = global::LotzInventory.Properties.Resources.new2;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            btnSave.BackgroundImage = global::LotzInventory.Properties.Resources.save_ovr;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            btnSave.BackgroundImage = global::LotzInventory.Properties.Resources.save1;
        }

        private void btnSavePrint_MouseHover(object sender, EventArgs e)
        {
            btnSavePrint.BackgroundImage = global::LotzInventory.Properties.Resources.saveandprint_ovr;
        }

        private void btnSavePrint_MouseLeave(object sender, EventArgs e)
        {
            btnSavePrint.BackgroundImage = global::LotzInventory.Properties.Resources.saveandprint;
        }

        private void btnCancel_MouseHover(object sender, EventArgs e)
        {
            btnCancel.BackgroundImage = global::LotzInventory.Properties.Resources.cancel_ovr;
        }

        private void btnCancel_MouseLeave(object sender, EventArgs e)
        {
            btnCancel.BackgroundImage = global::LotzInventory.Properties.Resources.cancel1;
        }

        private void btnExit_MouseHover(object sender, EventArgs e)
        {
            btnExit.BackgroundImage = global::LotzInventory.Properties.Resources.exit_ovr;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.BackgroundImage = global::LotzInventory.Properties.Resources.exit1;
        }
  


    }
}
