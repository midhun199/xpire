using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using Xpire.Classes;

namespace Xpire.Forms
{
    public partial class ucCommonMaster : UserControl
    {
        public ucCommonMaster()
        {
            InitializeComponent();
        }

        SqlCommand cmd = new SqlCommand("",cPublic.Connection );
        SqlDataReader rd = null;
        cConnection con = new cConnection();
        cGeneral cgen = new cGeneral();
        fssgen.fssgen gen = new fssgen.fssgen();

        private void FILL_LISTVIEW()
        {
            this.txtCode.MaxLength = 10;
            this.txtDetails.MaxLength = 39;
            switch (treList.SelectedNode.Text.ToString())
            {
                case "TAXPER":
                case "CESSPER":
                    this.txtCode.MaxLength = 5;
                    this.txtDetails.MaxLength = 12;
                    break;
                case "SCHEDULE":
                case "UNIT":
                    this.txtCode.MaxLength = 3;
                    this.txtDetails.MaxLength = 12;
                    break;
                case "MANUFACTURER":
                    this.txtCode.MaxLength = 100;
                    this.txtDetails.MaxLength = 100;
                    break;
                case "SALES PERSON":
                    this.txtCode.MaxLength = 100;
                    this.txtDetails.MaxLength = 100;
                    break;
                default:
                    txtCode.MaxLength = 10;
                    txtDetails.MaxLength = 40;
                    break;
            }
            this.lisList.Items.Clear();
            if (this.treList.SelectedNode.Text.Trim() == "" || this.treList.SelectedNode.Level == 0)
            {
                return;
            }

            fill_Listview();
        }

        private bool Search()
        {
            bool vCheck = false;
            try
            {
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text; 
                cmd.CommandText = "SELECT CODE,DETAILS FROM LOOKUP WHERE CODE=@CODE AND FIELD1=@FIELD1 AND DETAILS=@DETAILS ";
                cmd.Parameters.AddWithValue("@CODE", this.txtCode.Text.Trim());
                cmd.Parameters.AddWithValue("@FIELD1", this.treList.SelectedNode.Text.Trim());
                cmd.Parameters.AddWithValue("@DETAILS", txtDetails.Text.Trim());
                rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    vCheck = true;
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            {

            }
            return vCheck;
        }

        private void fill_Listview()
        {
            int vCount = 0;
            try
            {
                cmd.Connection = cPublic.Connection;
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT CODE,DETAILS FROM LOOKUP WHERE FIELD1=@FIELD1 ";
                cmd.Parameters.AddWithValue("@FIELD1", this.treList.SelectedNode.Text.Trim());

                rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    vCount++;
                    ListViewItem item1 = new ListViewItem(vCount.ToString(), 0);
                    item1.SubItems.Add(rd["CODE"].ToString());
                    item1.SubItems.Add(rd["DETAILS"].ToString());
                    this.lisList.Items.Add(item1);
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), cPublic.messagename);
            }
            finally
            {

            }
        }

        private string Search_Code(string database)
        {
            string vCheck = null;
            try
            {
                cmd.Connection = cPublic.Connection;
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT CODE,DETAILS FROM "+ database + ".dbo.LOOKUP WHERE CODE=@CODE AND FIELD1=@FIELD1 ";
                cmd.Parameters.AddWithValue("@CODE", this.txtCode.Text.Trim());
                cmd.Parameters.AddWithValue("@FIELD1", this.treList.SelectedNode.Text.Trim());
                rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    vCheck = rd["DETAILS"].ToString();
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            { }

            return vCheck;
        }

        private void SELECTED_CHANGE()
        {
            ListView.SelectedListViewItemCollection tbsitems =
            this.lisList.SelectedItems;

            foreach (ListViewItem item in tbsitems)
            {
                this.txtCode.Text = item.SubItems[1].Text;
                this.txtDetails.Text = item.SubItems[2].Text;
            }
        }

        private bool Save_Screen()
        {
            if (this.treList.SelectedNode.Text.Trim() == string.Empty || this.treList.SelectedNode.Level == 0)
            {
                MessageBox.Show("Please Select Node !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.treList.Focus();
                return false;
            }
            else if (this.txtCode.Text.Trim() == string.Empty )
            {
                MessageBox.Show("Please Enter Code !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtCode.Focus();
                return false;
            }
            else if (this.txtDetails.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Enter Details !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtDetails.Focus();
                return false;
            }
            try
            {

                string currentdatabase = cPublic.g_PrjCode + cPublic.g_compcode;

                cgen.ServerDet();
                con.ConnectMe(cPublic.password, cPublic.Sqluserid, cPublic.g_PrjCode + "000", cPublic.Sqlservername);
                string sql = "select dispname ,code from company order by dispname";
                SqlDataAdapter da = new SqlDataAdapter(sql, cPublic.Connection);
                DataSet ds = new DataSet();
                da.Fill(ds);

                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    string database = cPublic.g_PrjCode + row["code"].ToString();
                    
                    con.ConnectMe(cPublic.password, cPublic.Sqluserid, database, cPublic.Sqlservername);

                    if (Search_Code(database) == null)
                    {
                        cmd.CommandText = "INSERT INTO " + database + ".dbo.LOOKUP (CODE, DETAILS, FIELD1) VALUES (@CODE, @DETAILS, @FIELD1) ";


                    }
                    else
                    {
                        cmd.CommandText = "UPDATE " + database + ".dbo.LOOKUP SET DETAILS=@DETAILS WHERE FIELD1=@FIELD1 AND CODE=@CODE ";
                    }
                    cmd.Parameters.Clear();
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@FIELD1", this.treList.SelectedNode.Text.Trim());
                    cmd.Parameters.AddWithValue("@CODE", this.txtCode.Text.Trim());
                    cmd.Parameters.AddWithValue("@DETAILS", this.txtDetails.Text.Trim());
                    cmd.ExecuteNonQuery();
                }


                con.ConnectMe(cPublic.password, cPublic.Sqluserid, currentdatabase, cPublic.Sqlservername);
                //if (Search_Code() == null)
                //{
                //    cmd.CommandText = "INSERT INTO LOOKUP (CODE, DETAILS,  FIELD1) VALUES (@CODE, @DETAILS, @FIELD1) ";


                //}
                //else
                //{
                //    cmd.CommandText = "UPDATE LOOKUP SET DETAILS=@DETAILS WHERE FIELD1=@FIELD1 AND CODE=@CODE ";
                //}
                //cmd.Parameters.Clear();
                //cmd.CommandType = CommandType.Text;
                //cmd.Parameters.AddWithValue("@FIELD1", this.treList.SelectedNode.Text.Trim());
                //cmd.Parameters.AddWithValue("@CODE", this.txtCode.Text.Trim());
                //cmd.Parameters.AddWithValue("@DETAILS", this.txtDetails.Text.Trim());
                //cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            finally
            { }

            Screen_clear();

            return true;
        }

        private void DELETE_SCREEN()
        {
            string criteria = string.Empty;
            Boolean deleted = false;

            if (this.txtCode.Text.Trim() == "")
            {
                MessageBox.Show("Please Enter Code !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.txtCode.Focus();
                return;
            }
            else if (this.treList.SelectedNode.Text.Trim() == "" || this.treList.SelectedNode.Level == 0)
            {
                MessageBox.Show("Please Select Node !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.treList.Focus();
                return;
            }

            switch (this.treList.SelectedNode.Text)
            {
                case "DOCUMENT TYPE":

                    criteria = " where DocumentType = @code";
                    if (!checkExistancy("EmployeeDocument" + cPublic.g_firmcode, criteria))
                    {
                        deleted = DeleteLookUp();
                    }
                    else
                        MessageBox.Show("Already Used this Record  , cannot Delete this Entry....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                case "EMAIL":

                    criteria = " where email1 = @code";
                    if (!checkExistancy("Details" + cPublic.g_firmcode, criteria))
                    {
                        deleted = DeleteLookUp();
                    }
                    else
                        MessageBox.Show("Already Used this Record  , cannot Delete this Entry....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                case "LEAVE TYPE":

                    criteria = " where LeaveType = @code";
                    if (!checkExistancy("LeaveApplication" + cPublic.g_firmcode, criteria))
                    {
                        deleted = DeleteLookUp();
                    }
                    else
                        MessageBox.Show("Already Used this Record  , cannot Delete this Entry....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break; 
                default:

                    criteria = "where " + this.treList.SelectedNode.Text.Trim().ToLower() + " = @code";
                    if (!checkExistancy("Employee" + cPublic.g_firmcode, criteria))
                    {
                            deleted = DeleteLookUp();
                    }
                    else
                        MessageBox.Show("Already Used this Record  , cannot Delete this Entry....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    break;
            }
            if (deleted)
                MessageBox.Show("Record has been Deleted...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);

            Screen_clear();
        }

        private Boolean checkExistancy(String tableName, string criteria)
        {
            Boolean status = false;
            string sqlQuery = "select * from " + tableName + " " + criteria;
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sqlQuery;
            if (tableName.Equals("LOOKUP"))
                cmd.Parameters.AddWithValue("@FIELD1", this.treList.SelectedNode.Text);
            cmd.Parameters.AddWithValue("@CODE", txtCode.Text.Trim());
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
                status = true;
            dr.Close();
            return status;
        }

        private Boolean DeleteLookUp()
        {
            if (MessageBox.Show(txtCode.Text.Trim() + " ,Do you want to Delete this Entry..?", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            { return false; }
            Boolean status = false;
            try
            {
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Delete from LOOKUP Where FIELD1=@FIELD1 and CODE=@CODE";
                cmd.Parameters.AddWithValue("@field1", this.treList.SelectedNode.Text.Trim());
                cmd.Parameters.AddWithValue("@CODE", txtCode.Text.Trim());
                cmd.ExecuteNonQuery();

                status = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, cPublic.messagename,MessageBoxButtons.OK,MessageBoxIcon.Warning  );
            }
            return status;
        }

        private void ucCommonMaster_Load(object sender, EventArgs e)
        {
            treList.Nodes.Clear();
            treList.Nodes.Add("USER SETTINGS");
            Font vFont = new Font("Microsoft Sans Serif", (float)8.25, FontStyle.Bold);
            treList.Nodes[0].NodeFont = vFont;
            //treList.Nodes[0].Nodes.Add("TAXPER");
            //treList.Nodes[0].Nodes.Add("CESSPER");
            //treList.Nodes[0].Nodes.Add("MANUFACTURER");
            //treList.Nodes[0].Nodes.Add("PRODUCT");
            //treList.Nodes[0].Nodes.Add("GROUP");
            //treList.Nodes[0].Nodes.Add("COMMODITY");
            //treList.Nodes[0].Nodes.Add("SCHEDULE");
            //treList.Nodes[0].Nodes.Add("UNIT");
            treList.Nodes[0].Nodes.Add("GRADE");
            treList.Nodes[0].Nodes.Add("DOCUMENT TYPE");
            treList.Nodes[0].Nodes.Add("DEPARTMENT");
            treList.Nodes[0].Nodes.Add("QUALIFICATION");
            treList.Nodes[0].Nodes.Add("DESIGNATION");
            treList.Nodes[0].Nodes.Add("NATIONALITY");
            treList.Nodes[0].Nodes.Add("COUNTRY");
            treList.Nodes[0].Nodes.Add("LEAVE TYPE");
            treList.Nodes[0].Nodes.Add("RELIGION");
            treList.Nodes[0].Nodes.Add("EMAIL");
            treList.Nodes[0].Expand();

            this.treList.SelectedNode = this.treList.Nodes[0].Nodes[0];

            if (treList.Nodes[0].Level > 0)
            { this.Text = "LookUp Entry - " + treList.Nodes[0].Text; }
            else { this.Text = "LookUp Entry"; }

            FILL_LISTVIEW();
            txtCode.Text = "";
            txtDetails.Text = "";
        }

        private void Screen_clear()
        {
            this.txtCode.Text = string.Empty;

            this.txtDetails.Text = string.Empty;

            this.txtCode.Focus();
        }

        private void ucCommonMaster_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Escape)
            //{
                //  this.Close();
            //}
            //else if (e.KeyCode == Keys.Enter)
            //{
            //    SendKeys.Send("{TAB}");
            //}
        }

        private void treList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level > 0)
            { this.Text = "LookUp Entry - " + e.Node.Text; }
            else { this.Text = "LookUp Entry"; }

            FILL_LISTVIEW();
            txtCode.Text = "";
            txtDetails.Text = "";
        }
        
        private void treList_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (txtCode.Text != "" && txtDetails.Text != "")
            {
                if (Search() == true)
                {
                    FILL_LISTVIEW();
                    txtCode.Text = string.Empty ;
                    txtDetails.Text = string.Empty;
                    return;
                }
                else
                {
                    if (MessageBox.Show("Are You Sure to Save This Entry ?", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (Save_Screen() == true)
                        {
                            FILL_LISTVIEW();
                            txtCode.Text = string.Empty;
                            txtDetails.Text = string.Empty;
                            return;
                        }
                    }
                }
            }
        }

        private void treList_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            this.txtCode.Focus();
        }

        private void lisList_Click(object sender, EventArgs e)
        {
            SELECTED_CHANGE();
        }

        private void lisList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SELECTED_CHANGE();  
        }

        private void txtCode_Enter(object sender, EventArgs e)
        {
            TextBox tbs = (TextBox)sender;
            tbs.SelectAll();
        }

        private void txtCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (treList.SelectedNode.Text.Trim().ToUpper() == "TAXPER" || treList.SelectedNode.Text.Trim().ToUpper() == "CESSPER")
            {
                gen.ValidDecimalNumber(e, ref  txtCode, 3, 2);
                if (Information.IsNumeric(txtCode.Text.Trim()) == true)
                {
                    Strings.FormatNumber(txtCode.Text.Trim(), 2, TriState.True, TriState.True, TriState.True);
                }
            }
        }

        private void txtCode_Validating(object sender, CancelEventArgs e)
        {
            if (treList.SelectedNode.Text.Trim().ToUpper() == "TAXPER" || treList.SelectedNode.Text.Trim().ToUpper() == "CESSPER" && txtCode.Text.Trim() != string.Empty)
            {
                if (Microsoft.VisualBasic.Information.IsNumeric(txtCode.Text.Trim()) == false)
                {
                    this.txtCode.Text = "0.00";
                }
                else
                {
                    txtCode.Text = Convert.ToDecimal(txtCode.Text.Trim()).ToString("#0.00");
                }
            }
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select DETAILS FROM LOOKUP WHERE code=@code  and FIELD1=@FIELD1";
            cmd.Parameters.AddWithValue("@code", txtCode.Text);
            cmd.Parameters.AddWithValue("@FIELD1", this.treList.SelectedNode.Text.Trim());
            string tbs = cmd.ExecuteScalar() + "".ToString();
            if (tbs.Trim() != string.Empty)
            {
                this.txtDetails.Text = tbs;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Save_Screen() == true)
            {
                FILL_LISTVIEW();
                this.txtCode.Focus();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Boolean exiItem = true;
            for (int item = 0; item <= lisList.Items.Count - 1; item++)
            {
                if (txtCode.Text.ToString() == lisList.Items[item].SubItems[1].Text)
                {
                    exiItem = false;
                }
            }
            if (exiItem == false)
            {
                DELETE_SCREEN();
                FILL_LISTVIEW();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            btnSave.BackgroundImage = global::Xpire.Properties.Resources.save_ovr;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            btnSave.BackgroundImage = global::Xpire.Properties.Resources.save1;
        }

        private void btnDelete_MouseHover(object sender, EventArgs e)
        {
            btnDelete.BackgroundImage = global::Xpire.Properties.Resources.cancel_ovr;
        }

        private void btnDelete_MouseLeave(object sender, EventArgs e)
        {
            btnDelete.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            btnClose.BackgroundImage = global::Xpire.Properties.Resources.exit_ovr;
        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
            btnClose.BackgroundImage = global::Xpire.Properties.Resources.exit1;
        }



    }
}
