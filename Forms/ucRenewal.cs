﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Xpire.Classes;
using System.Collections;
using System.IO;
using System.Drawing.Imaging;
using Xpire.DataGrid;

namespace Xpire.Forms
{
    public partial class ucRenewal : UserControl
    {

        string sql = string.Empty;
        SqlCommand cmd;
        cConnection con = new cConnection();
        DataRow row;
        int rowindex = 0;

        ArrayList _al = new ArrayList();
        DataSet ds;
        DateTimePicker cellDateTimePicker;
        public ucRenewal()
        {
            InitializeComponent();
        }

        private void UserControl1_Load(object sender, EventArgs e)
        {
            GridInit();

            //this.BackColor = Color.Transparent;
            //this.BackgroundImage = SetImageOpacity(this.BackgroundImage, 0.25F);
        }

        public Image SetImageOpacity(Image image, float opacity)
        {
            Bitmap bmp = new Bitmap(image.Width, image.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                ColorMatrix matrix = new ColorMatrix();
                matrix.Matrix33 = opacity;
                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default,
                                                  ColorAdjustType.Bitmap);
                g.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height),
                                   0, 0, image.Width, image.Height,
                                   GraphicsUnit.Pixel, attributes);
            }
            return bmp;
        }

        public void GridInit()
        {
            this.cellDateTimePicker = new DateTimePicker();

            this.cellDateTimePicker.ValueChanged += new EventHandler(cellDateTimePickerValueChanged);

            this.cellDateTimePicker.Visible = false;

            //this.dataGridView1.Controls.Add(cellDateTimePicker);


            sql = "select distinct a.[id],a.[Name],a.[IssDate] as 'Issue Date',a.[ExpDate] as 'Expiry Date',c.Details as 'Type',a.[DocumentNo] as 'Document No','' as Attachment,d.empid "
          + "from [dbo].[Details" + cPublic.g_firmcode + "] a join [dbo].[Prior" + cPublic.g_firmcode + "] b on a.id=b.[detid] join LOOKUP c on a.documenttype=c.code join [dbo].[Docref" + cPublic.g_firmcode + "] d on d.docid=a.id  where b.sent=1 ";
            cmd = new SqlCommand(sql, cPublic.Connection);
            cmd.ExecuteNonQuery();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds);

            BindingSource bs = new BindingSource();

            bs.DataSource = ds.Tables[0];

            dataGridView1.DataSource = bs;

            DataGridViewTextBoxColumn idcolumn = new DataGridViewTextBoxColumn();
            idcolumn.DataPropertyName = "id";
            idcolumn.Name = "id";

            dataGridView1.Columns.Add(idcolumn);

            DataGridViewTextBoxColumn Namecolumn = new DataGridViewTextBoxColumn();
            Namecolumn.DataPropertyName = "Name";
            Namecolumn.Name = "Name";

            dataGridView1.Columns.Add(Namecolumn);

            CalendarColumn issdatecolumn = new CalendarColumn();
            issdatecolumn.DataPropertyName = "Issue Date";
            issdatecolumn.Name = "Issue Date";

            dataGridView1.Columns.Add(issdatecolumn);

            CalendarColumn expdatecolumn = new CalendarColumn();
            expdatecolumn.DataPropertyName = "Expiry Date";
            expdatecolumn.Name = "Expiry Date";

            dataGridView1.Columns.Add(expdatecolumn);

            DataGridViewTextBoxColumn typecolumn = new DataGridViewTextBoxColumn();
            typecolumn.DataPropertyName = "Type";
            typecolumn.Name = "Type";

            dataGridView1.Columns.Add(typecolumn);

            DataGridViewTextBoxColumn docnocolumn = new DataGridViewTextBoxColumn();
            docnocolumn.DataPropertyName = "Document No";
            docnocolumn.Name = "Document No";

            dataGridView1.Columns.Add(docnocolumn);

            DataGridViewTextBoxColumn attchcolumn = new DataGridViewTextBoxColumn();
            attchcolumn.DataPropertyName = "Attachment";
            attchcolumn.Name = "Attachment";

            dataGridView1.Columns.Add(attchcolumn);

            DataGridViewTextBoxColumn empidcolumn = new DataGridViewTextBoxColumn();
            empidcolumn.DataPropertyName = "empid";
            empidcolumn.Name = "empid";

            dataGridView1.Columns.Add(empidcolumn);

            if (!dataGridView1.Columns.Contains("btn"))
            {

                DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
                dataGridView1.Columns.Add(btn);
                btn.HeaderText = "Browse";
                btn.Text = "Click Here";
                btn.Name = "btn";
                btn.UseColumnTextForButtonValue = true;
            }

            dataGridView1.Columns["Name"].Width = 200;
            dataGridView1.Columns["Issue Date"].Width = 80;
            dataGridView1.Columns["Expiry Date"].Width = 80;
            dataGridView1.Columns["Document No"].Width = 140;
            dataGridView1.Columns["Type"].Width = 100;
            dataGridView1.Columns["empid"].Visible = false;
            dataGridView1.Columns["id"].Visible = false;

            this.dataGridView1.AllowUserToResizeColumns = false;

            dataGridView1.Columns.Cast<DataGridViewColumn>().ToList().ForEach(f => f.SortMode = DataGridViewColumnSortMode.NotSortable);

            dataGridView1.AllowUserToAddRows = false;

            dataGridView1.AllowUserToDeleteRows = false;

            this.dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Yellow;

            this.dataGridView1.DefaultCellStyle.SelectionBackColor = Color.Black;

            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.Olive;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold);

            SetBorderAndGridlineStyles();

            dataGridView1.Columns["Name"].DefaultCellStyle.ForeColor = Color.Red;

            dataGridView1.Columns["Name"].DefaultCellStyle.Font = new Font("Arial", 8, FontStyle.Bold);


            dataGridView1.Columns["Issue Date"].DefaultCellStyle.ForeColor = Color.Red;

            dataGridView1.Columns["Issue Date"].DefaultCellStyle.Font = new Font("Arial", 8, FontStyle.Bold);


            dataGridView1.Columns["Expiry Date"].DefaultCellStyle.ForeColor = Color.Red;

            dataGridView1.Columns["Expiry Date"].DefaultCellStyle.Font = new Font("Arial", 8, FontStyle.Bold);


            dataGridView1.Columns["Document No"].DefaultCellStyle.ForeColor = Color.Red;

            dataGridView1.Columns["Document No"].DefaultCellStyle.Font = new Font("Arial", 8, FontStyle.Bold);


            dataGridView1.Refresh();

            this.Controls.Add(this.dataGridView1);
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 ) return;
            dataGridView1.Rows[e.RowIndex].Cells["Attachment"].ReadOnly = true;
            dataGridView1.Rows[e.RowIndex].Cells["Type"].ReadOnly = true;
        
            if (e.ColumnIndex == 0)
            {

                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.CheckFileExists = true;
                openFileDialog.AddExtension = true;

                openFileDialog.Filter = "All files (*.*)|*.*|PDF files (*.pdf)|*.pdf|JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";

                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                    rowindex = e.RowIndex;

                 
                    ds.Tables[0].Rows[rowindex]["Attachment"]= openFileDialog.FileName;

                    this.dataGridView1.Refresh();

                }
            }

            if (e.ColumnIndex == 3 || e.ColumnIndex == 4)
            {
                Rectangle tempRect = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, false);

                cellDateTimePicker.Location = tempRect.Location;

                cellDateTimePicker.Width = tempRect.Width;

                cellDateTimePicker.Visible = true;
            }

            else
            { cellDateTimePicker.Visible = false; }


        }

        void cellDateTimePickerValueChanged(object sender, EventArgs e)
        {


            //DataTable table = dataGridView1.DataSource as DataTable;
            //DataRow row = table.NewRow();
            //row = ((DataRowView)dataGridView1.CurrentRow.DataBoundItem).Row;

            //if (row.RowState == DataRowState.Unchanged)
            //    row.SetModified();
            dataGridView1.CurrentCell.Value = cellDateTimePicker.Value.ToString("dd/MM/yyyy");
            cellDateTimePicker.Visible = false;

            dataGridView1.Refresh();

        }

        public void Save()
        {
            SqlTransaction sqlTran = cPublic.Connection.BeginTransaction(IsolationLevel.ReadCommitted);

            try
            {
                dataGridView1.Refresh();

                dataGridView1.Sort(dataGridView1.Columns["Name"], System.ComponentModel.ListSortDirection.Descending);

                dataGridView1.Sort(dataGridView1.Columns["Name"], System.ComponentModel.ListSortDirection.Ascending);

                dataGridView1.Sort(dataGridView1.Columns["Issue Date"], System.ComponentModel.ListSortDirection.Descending);

                dataGridView1.Sort(dataGridView1.Columns["Issue Date"], System.ComponentModel.ListSortDirection.Ascending);

                dataGridView1.Sort(dataGridView1.Columns["Expiry Date"], System.ComponentModel.ListSortDirection.Descending);

                dataGridView1.Sort(dataGridView1.Columns["Expiry Date"], System.ComponentModel.ListSortDirection.Ascending);

                dataGridView1.Sort(dataGridView1.Columns["Document No"], System.ComponentModel.ListSortDirection.Descending);

                dataGridView1.Sort(dataGridView1.Columns["Document No"], System.ComponentModel.ListSortDirection.Ascending);


                ucRegister ucRegister = new ucRegister();

                DataTable dt = null;

                var dataRow = ds.Tables[0].AsEnumerable().Where(x => x.Field<string>("Attachment") != string.Empty);

                if (dataRow.Count() != 0)
                {
                    dt = dataRow.CopyToDataTable<DataRow>();
                }


                //DataTable changedRecordsTable = ds.Tables[0].GetChanges(DataRowState.Modified);

                if (dt != null)
                {

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (!string.IsNullOrEmpty(dr["Attachment"].ToString()))
                        {

                            

                            FileStream fStream = File.OpenRead(dr["Attachment"].ToString());

                            byte[] contents = new byte[fStream.Length];

                            fStream.Read(contents, 0, (int)fStream.Length);

                            fStream.Close();

                            FileInfo fi = new FileInfo(dr["Attachment"].ToString());

                            cmd.Parameters.Clear();

                            cmd.Transaction = sqlTran;

                            cmd.Parameters.AddWithValue("@name", dr["Name"].ToString());

                            cmd.Parameters.AddWithValue("@issuedate", Convert.ToDateTime(dr["Issue Date"].ToString()).ToString("yyyy/MM/dd"));

                            cmd.Parameters.AddWithValue("@expirydate", Convert.ToDateTime(dr["Expiry Date"].ToString()).ToString("yyyy/MM/dd"));

                            cmd.Parameters.AddWithValue("@docno", dr["Document No"].ToString());

                            cmd.Parameters.AddWithValue("@filename", fi.Name);

                            cmd.Parameters.AddWithValue("@attachment", contents);

                            cmd.Parameters.AddWithValue("@id", dr["empid"].ToString());

                            cmd.CommandText = "UPDATE EmployeeDocument" + cPublic.g_firmcode + "  SET  Name=@name,[IssDate]=@issuedate,[ExpDate]=@expirydate,[DocumentNo]=@docno,FileName=@filename,[Attachment]=@attachment where id= @id";

                            cmd.ExecuteNonQuery();

                            //ucRegister.CopyFile(dr["Attachment"].ToString());

                            //dr["Attachment"] = ucRegister.dest;

                            cmd.Parameters.Clear();

                            cmd.Transaction = sqlTran;

                            cmd.Parameters.AddWithValue("@name", dr["Name"].ToString());

                            cmd.Parameters.AddWithValue("@issuedate", Convert.ToDateTime(dr["Issue Date"].ToString()).ToString("yyyy/MM/dd"));

                            cmd.Parameters.AddWithValue("@expirydate", Convert.ToDateTime(dr["Expiry Date"].ToString()).ToString("yyyy/MM/dd"));

                            cmd.Parameters.AddWithValue("@docno", dr["Document No"].ToString());

                            //cmd.Parameters.AddWithValue("@attachment", dr["Attachment"].ToString());

                            cmd.Parameters.AddWithValue("@id", dr["id"].ToString());

                            cmd.CommandText = "UPDATE Details" + cPublic.g_firmcode + "  SET  Name=@name,[IssDate]=@issuedate,[ExpDate]=@expirydate,[DocumentNo]=@docno where id= @id";

                            cmd.ExecuteNonQuery();



                            cmd.Parameters.Clear();

                            cmd.Transaction = sqlTran;

                            cmd.Parameters.AddWithValue("@id", dr["id"].ToString());

                            cmd.CommandText = "UPDATE prior" + cPublic.g_firmcode + "  SET  sent=0 where detid= @id";

                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            sqlTran.Rollback();
                            MessageBox.Show("Please select attachment.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }

                    }


                    sqlTran.Commit();

                    MessageBox.Show("Item Details Updated Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    GridInit();
                }
                else
                {
                    sqlTran.Rollback();
                }
            }
            catch (Exception ex)
            {
                sqlTran.Rollback();
                MessageBox.Show(ex.ToString());
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DeleteCellsIfNotInEditMode();
            }

        }

        private void DeleteCellsIfNotInEditMode()
        {
            if (!dataGridView1.CurrentCell.IsInEditMode)
            {

                if (dataGridView1.CurrentCell.OwningColumn.Name == "Attachment")
                {

                    dataGridView1.CurrentCell.Value = "";
                  
                }
            }
        }

        private void SetBorderAndGridlineStyles()
        {
            this.dataGridView1.GridColor = Color.BlueViolet;
            this.dataGridView1.BorderStyle = BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle =
                DataGridViewCellBorderStyle.SunkenVertical;
            this.dataGridView1.RowHeadersBorderStyle =
                DataGridViewHeaderBorderStyle.Sunken;
            this.dataGridView1.ColumnHeadersBorderStyle =
                DataGridViewHeaderBorderStyle.Sunken;
        }

    }
}
