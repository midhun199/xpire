﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;

using System.Text.RegularExpressions;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Forms
{
    public partial class ucEmployeeDetails : UserControl
    {

        SqlDataReader rdr = null;
        DataTable dtable = new DataTable();
 
        DataSet ds = new DataSet();
        SqlCommand cmd = null;
        DataTable dt = new DataTable();
        string id = string.Empty;
        string sqlQuery = string.Empty, nxtPrv = string.Empty;
        Boolean searchClick = false, autoCode = false, EscKey = false;
        cGeneral gen = new cGeneral();
        public ucEmployeeDetails()
        {
            InitializeComponent();
        }

        private Boolean LoadDetails(SqlCommand cmd)
        {
            //CSP s = new CSP();
            //s.job();


            Boolean status = true;
            SqlDataReader dr;
            cmd.Connection = cPublic.Connection;
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                id = dr["id"].ToString();
                txtStaffID.Text = dr["StaffID"].ToString();
                txtStaffName.Text = dr["StaffName"].ToString();
                cmbGender.Text = dr["Gender"].ToString();
                dtpDOB.Text = dr["DOB"].ToString();
                txtFatherName.Text = dr["FatherName"].ToString();
                
                txtPAddress.Text = dr["PermanentAddress"].ToString();
                txtTAddress.Text = dr["TemporaryAddress"].ToString();
               
                txtPhoneNo.Text = dr["PhoneNo"].ToString();
                txtMobileNo.Text = dr["MobileNo"].ToString();
                dtpDateOfJoining.Text = dr["DateOfJoining"].ToString();
                txtYOP.Text = dr["YearOfExperience"].ToString();
                txtEmail.Text = dr["Email"].ToString();

                object theValue = dr["Picture"];

                if (DBNull.Value != theValue)
                {
                    byte[] data = (byte[])dr["Picture"];
                    MemoryStream ms = new MemoryStream(data);
                pictureBox1.Image = Image.FromStream(ms);
                }
                
                this.txtNationality.Tag = dr["NatCode"].ToString();
                this.txtNationality.Text = dr["NatDetails"].ToString();
                //chkRenewed.Checked = Convert.ToBoolean(dr["Renewal"].ToString());                this.txtDocumentType.Text = dr["details"].ToString();
                this.txtDepartment.Tag = dr["DepCode"].ToString();
                this.txtDepartment.Text = dr["DepDetails"].ToString();
                this.txtDesignation.Tag = dr["DesCode"].ToString();
                this.txtDesignation.Text = dr["DesDetails"].ToString();
                this.txtQualifications.Tag = dr["QlfCode"].ToString();
                this.txtQualifications.Text = dr["QlfDetails"].ToString();
                this.Tag = "Data Loaded";

            }
            else { status = false; }
            dr.Close();

            gen.EDControls(GroupBox1, false);
            //gen.EDControls(groupBox2, false);

            int count = getItemCount();
            if (nxtPrv.Equals("next") && !status)
            {
                nxtPrv = string.Empty;
            }
            else if (nxtPrv.Equals("previous") && !status)
            {
                nxtPrv = string.Empty;
            }
            if (count == 0)
            { btnEdit.Enabled = false; btnDelete.Enabled = false; }
            return status;
        }

        private void loadFirstRecord()
        {
            sqlQuery = "select top 1 a.id,a.[StaffID],a.[StaffName],a.DOB,b.Code DepCode ,b.Details DepDetails,a.Gender,a.FatherName,a.PermanentAddress,a.TemporaryAddress,a.PhoneNo,a.MobileNo, a.DateOfJoining,d.Code QlfCode, d.Details QlfDetails, a.YearOfExperience,c.Code DesCode, c.Details DesDetails, a.Email,a.Picture,e.Code NatCode, e.Details NatDetails from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code  order by a.[id]";

            cmd = new SqlCommand(sqlQuery, cPublic.Connection);

            LoadDetails(cmd);
        }

        public bool varification(string verify)
        {
            return Regex.IsMatch(verify, "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
        }

        private void EnableButtons(Boolean status)
        {
            btnSearch.Enabled = status;
            btnNext.Enabled = status;
            btnPrevious.Enabled = status;
            btnEdit.Enabled = status;
            btnDelete.Enabled = status;
            btnAdd.Enabled = status;
            btnExit.Enabled = status;
       
        }

        private void loadDefultValues()
        {

            id = string.Empty;
            txtStaffID.Text = string.Empty;
            txtStaffName.Text = string.Empty;
            cmbGender.Text = string.Empty;
            dtpDOB.Value = System.DateTime.Now;
            dtpDateOfJoining.Value = System.DateTime.Now;
            txtFatherName.Text = string.Empty;
            txtPAddress.Text = string.Empty;
            txtTAddress.Text = string.Empty;
            txtPhoneNo.Text = string.Empty;
            txtMobileNo.Text = string.Empty;
            txtDepartment.Text = string.Empty;
            txtDesignation.Text = string.Empty;
            txtQualifications.Text = string.Empty;
            txtYOP.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtNationality.Text = string.Empty;
            pictureBox1.Image = null;
        }

        public Boolean Validation()
        {
            Boolean status = true;
            if (txtStaffName.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Stafe Name cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtStaffName.Focus();
            }

            else if (cmbGender.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Gender cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbGender.Focus();
            }
           
            else if (txtFatherName.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Father's Name cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtFatherName.Focus();
            }
            else if (txtPAddress.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Permenant Adress cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPAddress.Focus();
            }
            else if (txtTAddress.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Temperoray Adress cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTAddress.Focus();
            }
            else if (txtPhoneNo.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Phone Number cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPhoneNo.Focus();
            }
            else if (txtMobileNo.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Mobile Number cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMobileNo.Focus();
            }
            else if (txtDepartment.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Department cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDepartment.Focus();
            }
            else if (txtQualifications.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Qualifications cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtQualifications.Focus();
            }
            else if (txtDesignation.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Designation cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDesignation.Focus();
            }
          

            else if (!(txtEmail.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtEmail.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail.Focus();
                }
            }

            return status;
        }

      

        private Boolean ItmCodeVaidation()
        {
            bool status = true;
            if (!btnAdd.Tag.Equals("&Add"))
            {
                sqlQuery = "select * from Details" + cPublic.g_firmcode + " where id = @itemCode";
                SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@itemCode", id.Trim());
                if (LoadDetails(cmd)) { status = false; }
            }
            return status;
        }

        private int getItemCount()
        {
            sqlQuery = "select  isnull(count(*),0) from Employee" + cPublic.g_firmcode;
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            return Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }

        private void Items(SqlCommand cmd)
        {

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id.Trim());
            cmd.Parameters.AddWithValue("@Staffid", txtStaffID.Text.Trim());
            cmd.Parameters.AddWithValue("@Staffname", txtStaffName.Text.Trim());

            cmd.Parameters.AddWithValue("@department", txtDepartment.Tag);
            cmd.Parameters.AddWithValue("@gender", cmbGender.Text.Trim());
            cmd.Parameters.AddWithValue("@fathername", txtFatherName.Text.Trim());
            cmd.Parameters.AddWithValue("@nationality", txtNationality.Tag);
            cmd.Parameters.AddWithValue("@permanentaddress", txtPAddress.Text.Trim());
            cmd.Parameters.AddWithValue("@temporaryaddress", txtTAddress.Text.Trim());
            cmd.Parameters.AddWithValue("@Phoneno", txtPhoneNo.Text.Trim());
            cmd.Parameters.AddWithValue("@mobileno", txtMobileNo.Text.Trim());
            cmd.Parameters.AddWithValue("@dateofjoining", Convert.ToDateTime(dtpDateOfJoining.Value));
            cmd.Parameters.AddWithValue("@qualification", txtQualifications.Tag);
            cmd.Parameters.AddWithValue("@yearofexperience", Convert.ToInt32( txtYOP.Text.Trim()));
            cmd.Parameters.AddWithValue("@designation", txtDesignation.Tag);
            cmd.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
            cmd.Parameters.AddWithValue("@DOB", Convert.ToDateTime(dtpDOB.Value));
         

            MemoryStream ms = new MemoryStream();
            Bitmap bmpImage = new Bitmap(pictureBox1.Image);

            bmpImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            byte[] data = ms.GetBuffer();
            SqlParameter p = new SqlParameter("@Picture", SqlDbType.Image);
            p.Value = data;
            cmd.Parameters.Add(p);
        }

        public Boolean UpdateItem()
        {
            SqlTransaction sqlTrans = null;
            Boolean status = true;

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;


                cmd.Parameters.Clear();
                cmd.CommandText = "SP_UPDATE_EMPLOYEE" + cPublic.g_firmcode;

                Items(cmd);

                cmd.ExecuteNonQuery();

                sqlQuery = "select top 1 a.id,a.[StaffID],a.[StaffName],a.DOB,b.Code DepCode ,b.Details DepDetails,a.Gender,a.FatherName,a.PermanentAddress,a.TemporaryAddress,a.PhoneNo,a.MobileNo, a.DateOfJoining,d.Code QlfCode, d.Details QlfDetails, a.YearOfExperience,c.Code DesCode, c.Details DesDetails, a.Email,a.Picture,e.Code NatCode, e.Details NatDetails from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code where a.id = @id";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@id", id.Trim());
                LoadDetails(cmd);
            }
            catch (Exception ex)
            {
                sqlTrans.Rollback();
                MessageBox.Show(ex.Message);
                status = false;
            }
            return status;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchClick = true;
            btnExit.Tag = "&Cancel";
            btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
            gen.ClearFieldsInMe(this);
            txtStaffID.Enabled = true;
            txtStaffID.Focus();
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnNext.Enabled = false;
            btnPrevious.Enabled = false;
            //msgLabel.Text = "Press  F5  for Select an Item.";
            //msgLabel.ForeColor = System.Drawing.Color.MidnightBlue;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            nxtPrv = "previous";
            //msgLabel.Text = string.Empty;

            sqlQuery = "select top 1 a.id,a.[StaffID],a.[StaffName],a.DOB,b.Code DepCode ,b.Details DepDetails,a.Gender,a.FatherName,a.PermanentAddress,a.TemporaryAddress,a.PhoneNo,a.MobileNo, a.DateOfJoining,d.Code QlfCode, d.Details QlfDetails, a.YearOfExperience,c.Code DesCode, c.Details DesDetails, a.Email,a.Picture,e.Code NatCode, e.Details NatDetails from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code where a.id < @id"
            + "  order by a.id desc";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@id", id.Trim());
            LoadDetails(cmd);
        }

        public static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            chars =
            "123456789".ToCharArray();
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[maxSize];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
        private void auto()
        {
            txtStaffID.Text = "E-" + GetUniqueKey(6);
        }
        private void delete_records()
        {

            //try
            //{


            //    int RowsAffected = 0;
          
            //    string ct = "select StaffID from Attendance where StaffID=@find";


            //    cmd = new SqlCommand(ct);

            //    cmd.Connection = cPublic.Connection;

            //    cmd.Parameters.Add(new SqlParameter("@find", System.Data.SqlDbType.NChar, 15, "StaffID"));


            //    cmd.Parameters["@find"].Value = txtStaffID.Text;


            //    rdr = cmd.ExecuteReader();

            //    if (rdr.Read())
            //    {
            //        MessageBox.Show("Unable to delete..Already in use", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        clear();


            //        if ((rdr != null))
            //        {
            //            rdr.Close();
            //        }
            //        return;
            //    }
          
            //    string cm = "select StaffID from EmployeePayment where StaffID=@find";


            //    cmd = new SqlCommand(cm);

            //    cmd.Connection = cPublic.Connection;

            //    cmd.Parameters.Add(new SqlParameter("@find", System.Data.SqlDbType.NChar, 15, "StaffID"));


            //    cmd.Parameters["@find"].Value = txtStaffID.Text;


            //    rdr = cmd.ExecuteReader();

            //    if (rdr.Read())
            //    {
            //        MessageBox.Show("Unable to delete..Already in use", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        clear();


            //        if ((rdr != null))
            //        {
            //            rdr.Close();
            //        }
            //        return;
            //    }
              


            //    string cq = "delete from Employee where StaffID=@DELETE1;";


            //    cmd = new SqlCommand(cq);

            //    cmd.Connection = cPublic.Connection;

            //    cmd.Parameters.Add(new SqlParameter("@DELETE1", System.Data.SqlDbType.NChar, 15, "StaffID"));


            //    cmd.Parameters["@DELETE1"].Value = txtStaffID.Text;
            //    RowsAffected = cmd.ExecuteNonQuery();

            //    if (RowsAffected > 0)
            //    {
            //        MessageBox.Show("Successfully deleted", "Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 
            //        clear();
            //    }
            //    else
            //    {
            //        MessageBox.Show("No Record found", "Sorry", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   
            //        clear();

            //    }

            //        if (con.State == ConnectionState.Open)
            //        {
            //            con.Close();
            //        }

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }
        private void EmployeeDetails_Load(object sender, EventArgs e)
        {
            //msgLabel.BorderStyle = BorderStyle.None;
            lblItemCount.BorderStyle = BorderStyle.None;


            this.Tag = "No Records";

            loadFirstRecord();
            lblItemCount.Text = "No of Employees : " + getItemCount().ToString();

            if (this.Tag.Equals("No Records"))
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
            }


        }

     

        private void Department_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'DEPARTMENT'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtDepartment.Text = lookup.m_values[1].ToString();
                    txtDepartment.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                switch (btnAdd.Tag.ToString())
                {
                    case "&Add":
                     
                        btnAdd.Tag = "&Save";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.save1;
                        btnExit.Tag = "&Cancel";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
                       
                        gen.EDControls(this, true);


                        EnableButtons(false);
                        btnAdd.Enabled = true;
                        btnExit.Enabled = true;
                      

                        loadDefultValues();



                        break;
                    case "&Save":
                        if (Validation())
                        {
                            autoCode = true;
                            if(txtStaffID.Text==string.Empty)
                            { auto(); }
                            SqlCommand cmd = new SqlCommand("SP_INSERT_EMPLOYEE" + cPublic.g_firmcode, cPublic.Connection);

                            cmd.CommandType = CommandType.StoredProcedure;
                          
                            cmd.Connection = cPublic.Connection;


                            cmd.Parameters.Clear();


                            Items(cmd);

                            id = cmd.ExecuteScalar().ToString();


                       

                            //if (btnEdit.Tag == "&Update")
                            //{ cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(txtCurStock.Text) - opQty + Convert.ToDecimal(txtOPStock.Text)); }
                            //else { cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(txtOPStock.Text.Trim())); }


                            sqlQuery = "select a.id,a.[StaffID],a.[StaffName],a.DOB,b.Code DepCode ,b.Details DepDetails,a.Gender,a.FatherName,a.PermanentAddress,a.TemporaryAddress,a.PhoneNo,a.MobileNo, a.DateOfJoining,d.Code QlfCode, d.Details QlfDetails, a.YearOfExperience,c.Code DesCode, c.Details DesDetails, a.Email,a.Picture,e.Code NatCode, e.Details NatDetails from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code  where a.id = @id";
                            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                            cmd.Parameters.AddWithValue("@id", id.Trim());
                            LoadDetails(cmd);
                            autoCode = false;
                            lblItemCount.Text = "No of Items : " + getItemCount().ToString();
                            MessageBox.Show("New Item Saved Successfully......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            EnableButtons(true);

                            autoCode = false;
                            btnAdd.Tag = "&Add";
                            btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;

                        }
                        break;
                }
               
      

               

               
             

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        
        }
        

      

        private void Browse_Click(object sender, EventArgs e)
        {

            var _with1 = openFileDialog1;

            _with1.Filter = ("Images |*.png; *.bmp; *.jpg;*.jpeg; *.gif; *.ico");
            _with1.FilterIndex = 4;

            //Clear the file name
            openFileDialog1.FileName = "";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
               pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
            }
        }

        private void txtStaffName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        private void cmbGender_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        private void txtFatherName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        private void txtEmail_Validating(object sender, CancelEventArgs e)
        {
            System.Text.RegularExpressions.Regex rEMail = new System.Text.RegularExpressions.Regex(@"^[a-zA-Z][\w\.-]{2,28}[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (txtEmail.Text.Length > 0)
            {
                if (!rEMail.IsMatch(txtEmail.Text))
                {
                    MessageBox.Show("invalid email address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail.SelectAll();
                    e.Cancel = true;
                }
            }
        }

        private void txtYOP_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(Char.IsNumber(e.KeyChar) || e.KeyChar == 8);
        }

      

 


        private void btnNext_Click(object sender, EventArgs e)
        {
            nxtPrv = "next";
          

            sqlQuery = "select top 1 a.id,a.[StaffID],a.[StaffName],a.DOB,b.Code DepCode ,b.Details DepDetails,a.Gender,a.FatherName,a.PermanentAddress,a.TemporaryAddress,a.PhoneNo,a.MobileNo, a.DateOfJoining,d.Code QlfCode, d.Details QlfDetails, a.YearOfExperience,c.Code DesCode, c.Details DesDetails, a.Email,a.Picture,e.Code NatCode, e.Details NatDetails from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code where a.id > @id"
            + "  order by a.id ";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@id", id.Trim());

            LoadDetails(cmd);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            switch (btnEdit.Tag.ToString())
            {
                case "&Edit":
                    if (id.Trim() != string.Empty && txtStaffID.Text.Trim() == string.Empty)
                    { MessageBox.Show("You are not Allowed to Edit this Item..."); return; }
                    btnEdit.Tag = "&Update";
                    btnEdit.BackgroundImage = global::Xpire.Properties.Resources.save1;
                    btnExit.Tag = "&Cancel";
                    btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
                    
                    gen.EDControls(this, true);

                    EnableButtons(false);
                    btnEdit.Enabled = true;
                    btnExit.Enabled = true;


                    break;
                case "&Update":
                    if (Validation())
                    {
                        if (UpdateItem())
                        {
                            MessageBox.Show("Item Details Updated Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            EnableButtons(true);

                            btnEdit.Tag = "&Edit";
                            btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;


                        }
                        else { MessageBox.Show("Invalid Data.. Please Check...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    break;

            }
        }

      

        private void btnExit_Click(object sender, EventArgs e)
        {
            switch (btnExit.Tag.ToString())
            {
                case "E&xit":
                    this.Dispose();
                    break;
                case "&Cancel":
                    if ((MessageBox.Show("Are You Sure to Cancel...?  ", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question)).Equals(DialogResult.Yes))
                    {
                        //if (btnAdd.Tag.Equals("&Save"))
                        //{
                        //    SqlCommand cmd = new SqlCommand("SP_DELETE_STOCKMST" + cPublic.g_firmcode, cPublic.Connection);
                        //    cmd.CommandType = CommandType.StoredProcedure;
                        //    cmd.Parameters.AddWithValue("@itemcode", id.Trim());
                        //    cmd.ExecuteNonQuery();
                        //}

                        gen.ClearFieldsInMe(this);
                        EnableButtons(true);

                        btnAdd.Tag = "&Add";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                        btnEdit.Tag = "&Edit";
                        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        loadDefultValues();
                        loadFirstRecord();
                        searchClick = false;
                        autoCode = false;

                    }
                    EscKey = false;
                    
                    break;
            }
        }

        private void txtStaffID_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchClick)
            {
                if (e.KeyCode.Equals(Keys.F5))
                {
                    string fields = string.Empty;
                    string dispName = string.Empty;
                    string width = string.Empty;
                    //if (cPublic.LookupDefault == "Itemcode")
                    //{
                    fields = "id,StaffID,StaffName";
                    dispName = "id,Staff ID,Name";
                    width = "0,100,150";
                    //}
                    //else
                    //{
                    //    fields = "itemname,itemcode,qty,Rate,taxper,cessper";
                    //    dispName = "Item Name, Item Code, Qty, Rate, Tax %, Cess %";
                    //    width = "350,80,80,100,50,60";
                    //}
                    frmlookup lookup = new frmlookup();
                    lookup.m_table = "Employee" + cPublic.g_firmcode;
                    lookup.m_fields = fields;
                    lookup.m_dispname = dispName;
                    lookup.m_fldwidth = width;
                    lookup.m_con = cPublic.Connection;
                    lookup.ShowDialog();
                    if (lookup.m_values.Count > 0)
                    {
                        //if (cPublic.LookupDefault == "Itemcode")
                        //{
                        id = lookup.m_values[0].ToString();
                        txtStaffID.Text = lookup.m_values[1].ToString();
                        txtStaffName.Text = lookup.m_values[2].ToString();
                        //}
                        //else
                        //{
                        //    id = lookup.m_values[1].ToString();
                        //    txtItmName.Text = lookup.m_values[0].ToString();
                        //}
                        sqlQuery = "select a.id,a.[StaffID],a.[StaffName],a.DOB,b.Code DepCode ,b.Details DepDetails,a.Gender,a.FatherName,a.PermanentAddress,a.TemporaryAddress,a.PhoneNo,a.MobileNo, a.DateOfJoining,d.Code QlfCode, d.Details QlfDetails, a.YearOfExperience,c.Code DesCode, c.Details DesDetails, a.Email,a.Picture,e.Code NatCode, e.Details NatDetails from[dbo].[Employee" + cPublic.g_firmcode + "] a join LOOKUP b on a.[Department]= b.code join LOOKUP  c on a.[Designation]= c.code join LOOKUP  d on a.[Qualification]= d.code join LOOKUP  e on a.[nationality]= e.code  where a.id = @id";
                        SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                        cmd.Parameters.AddWithValue("@id", id.Trim());
                        LoadDetails(cmd);
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnDelete.Enabled = true;
                        btnNext.Enabled = true;
                        btnPrevious.Enabled = true;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        searchClick = false;
                    }
                }

            }
        }

        private void txtNationality_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'NATIONALITY'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "0,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtNationality.Text = lookup.m_values[1].ToString();
                    txtNationality.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                delete_records();


            }
        }

        private void txtQualifications_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'QUALIFICATION'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtQualifications.Text = lookup.m_values[1].ToString();
                    txtQualifications.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void txtDesignation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'DESIGNATION'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtDesignation.Text = lookup.m_values[1].ToString();
                    txtDesignation.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

    }
}