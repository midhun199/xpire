﻿using System.Windows.Forms;
namespace Biz_Maxx
{
    partial class ucLabour
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucLabour));
            this.Panel4 = new Panel();
            this.btnSerach = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSavePrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.Panel1 = new Panel();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtRemarks = new RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtCustomer = new TextBox();
            this.txtKM = new TextBox();
            this.txtVehicleNo = new TextBox();
            this.txtMake = new TextBox();
            this.txtOrderno = new TextBox();
            this.KM = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.lblmake = new System.Windows.Forms.Label();
            this.FlexSalesDetails = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.Panel3 = new Panel();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblBillAmt = new System.Windows.Forms.Label();
            this.Panel2 = new Panel();
            this.txtSalesPerson = new TextBox();
            this.txtRoundoff = new TextBox();
            this.txtexpense = new TextBox();
            this.txtSplDiscAmt = new TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            //((System.ComponentModel.ISupportInitialize)(this.Panel4)).BeginInit();
            //this.Panel4.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel1)).BeginInit();
            //this.Panel1.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtRemarks)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtKM)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtVehicleNo)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtOrderno)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.FlexSalesDetails)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel3)).BeginInit();
            //this.Panel3.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel2)).BeginInit();
            //this.Panel2.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtSalesPerson)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtRoundoff)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtexpense)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtSplDiscAmt)).BeginInit();
            //this.SuspendLayout();
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));

            this.Panel4.Controls.Add(this.btnSerach);
            this.Panel4.Controls.Add(this.btnNew);
            this.Panel4.Controls.Add(this.btnPrint);
            this.Panel4.Controls.Add(this.btnCancel);
            this.Panel4.Controls.Add(this.btnExit);
            this.Panel4.Controls.Add(this.btnSavePrint);
            this.Panel4.Controls.Add(this.btnSave);
            this.Panel4.Location = new System.Drawing.Point(11, 3);

            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1079, 82);
            this.Panel4.TabIndex = 16;
            // 
            // btnSerach
            // 
            this.btnSerach.BackColor = System.Drawing.Color.White;
            this.btnSerach.BackgroundImage = global::LotzInventory.Properties.Resources.search;
            this.btnSerach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSerach.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSerach.ForeColor = System.Drawing.Color.Maroon;
            this.btnSerach.Location = new System.Drawing.Point(7, 7);
            this.btnSerach.Margin = new System.Windows.Forms.Padding(4);
            this.btnSerach.Name = "btnSerach";
            this.btnSerach.Size = new System.Drawing.Size(76, 71);
            this.btnSerach.TabIndex = 20;
            this.btnSerach.TabStop = false;
            this.btnSerach.UseVisualStyleBackColor = false;
            this.btnSerach.Click += new System.EventHandler(this.btnSerach_Click);
            this.btnSerach.MouseLeave += new System.EventHandler(this.btnSerach_MouseLeave);
            this.btnSerach.MouseHover += new System.EventHandler(this.btnSerach_MouseHover);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.White;
            this.btnNew.BackgroundImage = global::LotzInventory.Properties.Resources.new2;
            this.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ForeColor = System.Drawing.Color.Maroon;
            this.btnNew.Location = new System.Drawing.Point(82, 7);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(76, 71);
            this.btnNew.TabIndex = 19;
            this.btnNew.TabStop = false;
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.btnNew.MouseLeave += new System.EventHandler(this.btnNew_MouseLeave);
            this.btnNew.MouseHover += new System.EventHandler(this.btnNew_MouseHover);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.BackgroundImage = global::LotzInventory.Properties.Resources.print1;
            this.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Maroon;
            this.btnPrint.Location = new System.Drawing.Point(307, 7);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 71);
            this.btnPrint.TabIndex = 18;
            this.btnPrint.TabStop = false;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.btnPrint.MouseLeave += new System.EventHandler(this.btnPrint_MouseLeave);
            this.btnPrint.MouseHover += new System.EventHandler(this.btnPrint_MouseHover);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.White;
            this.btnCancel.BackgroundImage = global::LotzInventory.Properties.Resources.cancel1;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.Maroon;
            this.btnCancel.Location = new System.Drawing.Point(382, 7);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(76, 71);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.TabStop = false;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnCancel.MouseLeave += new System.EventHandler(this.btnCancel_MouseLeave);
            this.btnCancel.MouseHover += new System.EventHandler(this.btnCancel_MouseHover);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundImage = global::LotzInventory.Properties.Resources.exit1;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Maroon;
            this.btnExit.Location = new System.Drawing.Point(457, 7);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 71);
            this.btnExit.TabIndex = 22;
            this.btnExit.TabStop = false;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseLeave += new System.EventHandler(this.btnExit_MouseLeave);
            this.btnExit.MouseHover += new System.EventHandler(this.btnExit_MouseHover);
            // 
            // btnSavePrint
            // 
            this.btnSavePrint.BackColor = System.Drawing.Color.White;
            this.btnSavePrint.BackgroundImage = global::LotzInventory.Properties.Resources.saveandprint;
            this.btnSavePrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSavePrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavePrint.ForeColor = System.Drawing.Color.Maroon;
            this.btnSavePrint.Location = new System.Drawing.Point(232, 7);
            this.btnSavePrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnSavePrint.Name = "btnSavePrint";
            this.btnSavePrint.Size = new System.Drawing.Size(76, 71);
            this.btnSavePrint.TabIndex = 17;
            this.btnSavePrint.TabStop = false;
            this.btnSavePrint.UseVisualStyleBackColor = false;
            this.btnSavePrint.Click += new System.EventHandler(this.btnSavePrint_Click);
            this.btnSavePrint.MouseLeave += new System.EventHandler(this.btnSavePrint_MouseLeave);
            this.btnSavePrint.MouseHover += new System.EventHandler(this.btnSavePrint_MouseHover);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.BackgroundImage = global::LotzInventory.Properties.Resources.save1;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Maroon;
            this.btnSave.Location = new System.Drawing.Point(157, 7);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 71);
            this.btnSave.TabIndex = 16;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseLeave += new System.EventHandler(this.btnSave_MouseLeave);
            this.btnSave.MouseHover += new System.EventHandler(this.btnSave_MouseHover);
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));

            this.Panel1.Controls.Add(this.dtpDate);
            this.Panel1.Controls.Add(this.txtRemarks);
            this.Panel1.Controls.Add(this.label1);
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Controls.Add(this.txtCustomer);
            this.Panel1.Controls.Add(this.txtKM);
            this.Panel1.Controls.Add(this.txtVehicleNo);
            this.Panel1.Controls.Add(this.txtMake);
            this.Panel1.Controls.Add(this.txtOrderno);
            this.Panel1.Controls.Add(this.KM);
            this.Panel1.Controls.Add(this.Label4);
            this.Panel1.Controls.Add(this.label3);
            this.Panel1.Controls.Add(this.Label7);
            this.Panel1.Controls.Add(this.lblmake);
            this.Panel1.Location = new System.Drawing.Point(11, 88);
    
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(1079, 135);
            this.Panel1.TabIndex = 17;
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd/MM/yyyy";
            this.dtpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(321, 11);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(137, 21);
            this.dtpDate.TabIndex = 6;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(601, 10);
            this.txtRemarks.Name = "txtRemarks";

            this.txtRemarks.Size = new System.Drawing.Size(480, 114);
            this.txtRemarks.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(498, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 15);
            this.label1.TabIndex = 98;
            this.label1.Text = "Customer Address";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(15, 13);
            this.Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(60, 15);
            this.Label2.TabIndex = 86;
            this.Label2.Text = "Invoice No";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(96, 102);
            this.txtCustomer.Name = "txtCustomer";

            this.txtCustomer.Size = new System.Drawing.Size(362, 22);
            this.txtCustomer.TabIndex = 4;
            this.txtCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomer_KeyDown);
            this.txtCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCustomer_KeyPress);
            // 
            // txtKM
            // 
            this.txtKM.Location = new System.Drawing.Point(321, 72);
            this.txtKM.Name = "txtKM";

            this.txtKM.Size = new System.Drawing.Size(137, 22);
            this.txtKM.TabIndex = 3;
            //this.txtKM.EditValueChanged += new System.EventHandler(this.txtKM_EditValueChanged);
            // 
            // txtVehicleNo
            // 
            this.txtVehicleNo.Location = new System.Drawing.Point(96, 72);
            this.txtVehicleNo.Name = "txtVehicleNo";

            this.txtVehicleNo.Size = new System.Drawing.Size(137, 22);
            this.txtVehicleNo.TabIndex = 2;
            // 
            // txtMake
            // 
            this.txtMake.Location = new System.Drawing.Point(96, 41);
            this.txtMake.Name = "txtMake";

            this.txtMake.Size = new System.Drawing.Size(137, 22);
            this.txtMake.TabIndex = 1;
            // 
            // txtOrderno
            // 
            this.txtOrderno.Location = new System.Drawing.Point(96, 10);
            this.txtOrderno.Name = "txtOrderno";

            this.txtOrderno.Size = new System.Drawing.Size(137, 22);
            this.txtOrderno.TabIndex = 0;
            this.txtOrderno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOrderno_KeyPress);
            // 
            // KM
            // 
            this.KM.AutoSize = true;
            this.KM.BackColor = System.Drawing.Color.Transparent;
            this.KM.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KM.ForeColor = System.Drawing.Color.Black;
            this.KM.Location = new System.Drawing.Point(253, 75);
            this.KM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KM.Name = "KM";
            this.KM.Size = new System.Drawing.Size(28, 15);
            this.KM.TabIndex = 92;
            this.KM.Text = "KM";
            this.KM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.BackColor = System.Drawing.Color.Transparent;
            this.Label4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(15, 104);
            this.Label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(54, 15);
            this.Label4.TabIndex = 87;
            this.Label4.Text = "Customer";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(15, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 15);
            this.label3.TabIndex = 92;
            this.label3.Text = "Vehicle No";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.BackColor = System.Drawing.Color.Transparent;
            this.Label7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.ForeColor = System.Drawing.Color.Black;
            this.Label7.Location = new System.Drawing.Point(253, 13);
            this.Label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(30, 15);
            this.Label7.TabIndex = 89;
            this.Label7.Text = "Date";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblmake
            // 
            this.lblmake.AutoSize = true;
            this.lblmake.BackColor = System.Drawing.Color.Transparent;
            this.lblmake.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmake.ForeColor = System.Drawing.Color.Black;
            this.lblmake.Location = new System.Drawing.Point(15, 45);
            this.lblmake.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblmake.Name = "lblmake";
            this.lblmake.Size = new System.Drawing.Size(35, 15);
            this.lblmake.TabIndex = 92;
            this.lblmake.Text = "Make";
            this.lblmake.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FlexSalesDetails
            // 
            this.FlexSalesDetails.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.FlexSalesDetails.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.FlexSalesDetails.AutoResize = false;
            this.FlexSalesDetails.BackColor = System.Drawing.SystemColors.Window;
            this.FlexSalesDetails.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.FixedSingle;
            this.FlexSalesDetails.ColumnInfo = "31,1,0,0,0,85,Columns:0{Width:26;}\t";
            this.FlexSalesDetails.ExtendLastCol = true;
            this.FlexSalesDetails.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.Solid;
            this.FlexSalesDetails.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FlexSalesDetails.KeyActionEnter = C1.Win.C1FlexGrid.KeyActionEnum.None;
            this.FlexSalesDetails.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross;
            this.FlexSalesDetails.Location = new System.Drawing.Point(11, 223);
            this.FlexSalesDetails.Name = "FlexSalesDetails";
            this.FlexSalesDetails.Rows.Count = 20;
            this.FlexSalesDetails.Rows.MinSize = 21;
            this.FlexSalesDetails.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.FlexSalesDetails.Size = new System.Drawing.Size(1078, 262);
            this.FlexSalesDetails.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("FlexSalesDetails.Styles"));
            this.FlexSalesDetails.TabIndex = 6;
            this.FlexSalesDetails.BeforeRowColChange += new C1.Win.C1FlexGrid.RangeEventHandler(this.FlexSalesDetails_BeforeRowColChange);
            this.FlexSalesDetails.EnterCell += new System.EventHandler(this.FlexSalesDetails_EnterCell);
            this.FlexSalesDetails.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.FlexSalesDetails_SetupEditor);
            this.FlexSalesDetails.ValidateEdit += new C1.Win.C1FlexGrid.ValidateEditEventHandler(this.FlexSalesDetails_ValidateEdit);
            this.FlexSalesDetails.KeyDownEdit += new C1.Win.C1FlexGrid.KeyEditEventHandler(this.FlexSalesDetails_KeyDownEdit);
            this.FlexSalesDetails.KeyPressEdit += new C1.Win.C1FlexGrid.KeyPressEditEventHandler(this.FlexSalesDetails_KeyPressEdit);
            this.FlexSalesDetails.Click += new System.EventHandler(this.FlexSalesDetails_Click);
            this.FlexSalesDetails.Enter += new System.EventHandler(this.FlexSalesDetails_Enter);
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
      
            this.Panel3.Controls.Add(this.lblAmount);
            this.Panel3.Controls.Add(this.lblBillAmt);
            this.Panel3.Location = new System.Drawing.Point(576, 488);
  
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(515, 146);
            this.Panel3.TabIndex = 88;
            this.Panel3.TabStop = true;
            // 
            // lblAmount
            // 
            this.lblAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 46F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.ForeColor = System.Drawing.Color.Black;
            this.lblAmount.Location = new System.Drawing.Point(52, 55);
            this.lblAmount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(471, 85);
            this.lblAmount.TabIndex = 8;
            this.lblAmount.Text = "0.00";
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBillAmt
            // 
            this.lblBillAmt.AutoSize = true;
            this.lblBillAmt.Font = new System.Drawing.Font("Times New Roman", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillAmt.ForeColor = System.Drawing.Color.Black;
            this.lblBillAmt.Location = new System.Drawing.Point(13, 12);
            this.lblBillAmt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBillAmt.Name = "lblBillAmt";
            this.lblBillAmt.Size = new System.Drawing.Size(182, 39);
            this.lblBillAmt.TabIndex = 7;
            this.lblBillAmt.Text = "Bill Amount";
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));

            this.Panel2.Controls.Add(this.txtSalesPerson);
            this.Panel2.Controls.Add(this.txtRoundoff);
            this.Panel2.Controls.Add(this.txtexpense);
            this.Panel2.Controls.Add(this.txtSplDiscAmt);
            this.Panel2.Controls.Add(this.label5);
            this.Panel2.Controls.Add(this.label32);
            this.Panel2.Controls.Add(this.Label12);
            this.Panel2.Controls.Add(this.label9);
            this.Panel2.Location = new System.Drawing.Point(13, 488);

            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(561, 146);
            this.Panel2.TabIndex = 87;
            // 
            // txtSalesPerson
            // 
            this.txtSalesPerson.Location = new System.Drawing.Point(130, 109);
            this.txtSalesPerson.Name = "txtSalesPerson";

            this.txtSalesPerson.Size = new System.Drawing.Size(137, 22);
            this.txtSalesPerson.TabIndex = 10;
            // 
            // txtRoundoff
            // 

            this.txtRoundoff.Location = new System.Drawing.Point(131, 77);
            this.txtRoundoff.Name = "txtRoundoff";

            this.txtRoundoff.Size = new System.Drawing.Size(137, 22);
            this.txtRoundoff.TabIndex = 9;
            // 
            // txtexpense
            // 
       
            this.txtexpense.Location = new System.Drawing.Point(131, 46);
            this.txtexpense.Name = "txtexpense";
        
            this.txtexpense.Size = new System.Drawing.Size(137, 22);
            this.txtexpense.TabIndex = 8;
            // 
            // txtSplDiscAmt
            // 
  
            this.txtSplDiscAmt.Location = new System.Drawing.Point(130, 13);
            this.txtSplDiscAmt.Name = "txtSplDiscAmt";

            this.txtSplDiscAmt.Size = new System.Drawing.Size(137, 22);
            this.txtSplDiscAmt.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(15, 112);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 91;
            this.label5.Text = "Sales Person";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(15, 47);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(47, 15);
            this.label32.TabIndex = 90;
            this.label32.Text = "Expense";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.BackColor = System.Drawing.Color.Transparent;
            this.Label12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.ForeColor = System.Drawing.Color.Black;
            this.Label12.Location = new System.Drawing.Point(15, 79);
            this.Label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(59, 15);
            this.Label12.TabIndex = 83;
            this.Label12.Text = "Round Off";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(15, 15);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 15);
            this.label9.TabIndex = 65;
            this.label9.Text = "Discount";
            // 
            // ucLabour
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.FlexSalesDetails);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.Panel4);
            this.Name = "ucLabour";
            this.Size = new System.Drawing.Size(1111, 640);
            this.Load += new System.EventHandler(this.ucSales_Load);
            //((System.ComponentModel.ISupportInitialize)(this.Panel4)).EndInit();
            //this.Panel4.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.Panel1)).EndInit();
            //this.Panel1.ResumeLayout(false);
            //this.Panel1.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtRemarks)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtKM)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtVehicleNo)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtOrderno)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.FlexSalesDetails)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel3)).EndInit();
            //this.Panel3.ResumeLayout(false);
            //this.Panel3.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel2)).EndInit();
            //this.Panel2.ResumeLayout(false);
            //this.Panel2.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtSalesPerson)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtRoundoff)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtexpense)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtSplDiscAmt)).EndInit();
            //this.ResumeLayout(false);

        }

        #endregion

        private Panel Panel4;
        internal System.Windows.Forms.Button btnSerach;
        internal System.Windows.Forms.Button btnNew;
        internal System.Windows.Forms.Button btnPrint;
        internal System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Button btnSavePrint;
        internal System.Windows.Forms.Button btnSave;
        private Panel Panel1;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private RichTextBox txtRemarks;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label Label2;
        private TextBox txtCustomer;
        private TextBox txtMake;
        private TextBox txtOrderno;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label lblmake;
        private C1.Win.C1FlexGrid.C1FlexGrid FlexSalesDetails;
        private TextBox txtVehicleNo;
        internal System.Windows.Forms.Label label3;
        private Panel Panel3;
        internal System.Windows.Forms.Label lblAmount;
        internal System.Windows.Forms.Label lblBillAmt;
        private Panel Panel2;
        private TextBox txtSalesPerson;
        private TextBox txtRoundoff;
        private TextBox txtexpense;
        private TextBox txtSplDiscAmt;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label32;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label label9;
        private TextBox txtKM;
        internal System.Windows.Forms.Label KM;
    }
}
