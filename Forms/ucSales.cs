using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using C1.Win.C1FlexGrid;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using System.IO;
using Biz_Maxx.BL;
using Biz_Maxx.Forms;


namespace Biz_Maxx
{
    public partial class ucSales : UserControl
    {
        public ucSales()
        {
            InitializeComponent();
  
        }

        decimal tdiscount = 0, tamount = 0;

        string firmCode = cPublic.g_firmcode, query = string.Empty;
        private string CashCred = string.Empty; 
        private string PS = string.Empty;

        string mastertable1, subtable1;
        string SP_INSERT_TABLE, SP_UPDATE_TABLE, SP_CANCEL_TABLE;
        string SP_INSERT_SUBTABLE, SP_DELETE_SUBTABLE;

        fssgen.fssgen gen = new fssgen.fssgen();
        cGeneral cgen = new cGeneral();

        SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);
        SqlDataReader dr = null;

        DateTime olddate;
        int counterflag = 0;
        TextBox txtBox = null;

        bool saveFlg = false;
        private string prnOrderNo = string.Empty;
        private Int64 masterOrder = 0;
        private string ordernumber = string.Empty;
        private string Bill_Type=string.Empty ;
        private string Price_Type = string.Empty;
        private string Cus_cod = string.Empty;
        private string Price_Name = string.Empty;

        private CustomerBO customerbo = new CustomerBO();

        public string vTag
        {
            get { return Tag.ToString(); }
            set { Tag = value; }
        }

        private void grid_initialize(C1FlexGrid grid)
        {
            int vcol = 0;
            grid.Styles.Fixed.Font = new Font("Verdana", 10, FontStyle.Regular);
            grid.Styles.Fixed.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter;
            grid.Styles.Normal.Font = new Font("Verdana", 10, FontStyle.Regular);

            grid.Cols.Count = 100;
            grid.Rows.Count = 20;

            grid.Cols[vcol++].Name = "slno";
            grid[0, "slno"] = "SlNo";
            grid.Cols["slno"].Width = 40;
            grid.Cols["slno"].DataType = typeof(string);
            grid.Cols["slno"].TextAlign = TextAlignEnum.LeftCenter;

            grid.Cols[vcol++].Name = "itmCode";
            grid[0, "itmCode"] = "Itemcode";
            grid.Cols["itmCode"].Width = 75;
            grid.Cols["itmCode"].DataType = typeof(string);
            grid.Cols["itmCode"].TextAlign = TextAlignEnum.LeftCenter;

            grid.Cols[vcol++].Name = "itmName";
            grid[0, "itmName"] = "Item Description";
            grid.Cols["itmName"].Width = 250;
            grid.Cols["itmName"].DataType = typeof(string);
            grid.Cols["itmName"].TextAlign = TextAlignEnum.LeftCenter;
            grid.Cols["itmName"].AllowEditing = false ;

            grid.Cols[vcol++].Name = "bCode";
            grid[0, "bCode"] = "BarCode";
            grid.Cols["bCode"].Width = 75;
            grid.Cols["bCode"].DataType = typeof(Boolean);
            grid.Cols["bcode"].AllowEditing = false;
            grid.Cols["bcode"].Visible = false;

            grid.Cols[vcol++].Name = "Unit";
            grid[0, "Unit"] = "Unit";
            grid.Cols["Unit"].Width = 75;
            grid.Cols["Unit"].DataType = typeof(string );
            grid.Cols["Unit"].AllowEditing = true ;
            grid.Cols["Unit"].Visible = true;
            grid.Cols["Unit"].TextAlign = TextAlignEnum.LeftCenter;

            grid.Cols[vcol++].Name = "UnitValue";
            grid[0, "UnitValue"] = "UnitValue";
            grid.Cols["UnitValue"].Width = 75;
            grid.Cols["UnitValue"].DataType = typeof(decimal );
            grid.Cols["UnitValue"].AllowEditing = false;
            grid.Cols["UnitValue"].Visible = false;

            FlexSalesDetails.Cols[vcol++].Name = "multiunit";
            FlexSalesDetails[0, "multiunit"] = "multiunit";
            FlexSalesDetails.Cols["multiunit"].Width = 0;
            FlexSalesDetails.Cols["multiunit"].Visible = false;

            grid.Cols[vcol++].Name = "qty";
            grid[0, "qty"] = "QTY";
            grid.Cols["qty"].Width = 80;
            grid.Cols["qty"].DataType = typeof(Decimal);
            grid.Cols["qty"].Format = "#0.000";

            grid.Cols[vcol++].Name = "foc";
            grid[0, "foc"] = "FOC";
            grid.Cols["foc"].Width = 80;
            grid.Cols["foc"].DataType = typeof(Decimal);
            grid.Cols["foc"].Format = "#0.000";
            grid.Cols["foc"].Visible = false;

            grid.Cols[vcol++].Name = "Rate";
            grid[0, "Rate"] = "Rate";
            grid.Cols["Rate"].Width = 75;
            grid.Cols["Rate"].DataType = typeof(Decimal);
            grid.Cols["Rate"].Format = "#0.00";
            grid.Cols["Rate"].AllowEditing = true ; 

            grid.Cols[vcol++].Name = "price";
            grid[0, "price"] = "Price";
            grid.Cols["price"].Width = 75;
            grid.Cols["price"].DataType = typeof(Decimal);
            grid.Cols["price"].Format = "#0.00";

            grid.Cols[vcol++].Name = "taxPer";
            grid[0, "taxPer"] = "Tax %";
            grid.Cols["taxPer"].Width = 50;
            grid.Cols["taxPer"].DataType = typeof(Decimal);
            grid.Cols["taxPer"].Format = "#0.00";
            grid.Cols["taxPer"].AllowEditing = false;

            grid.Cols[vcol++].Name = "taxAmt";
            grid[0, "taxAmt"] = "Tax Amt";
            grid.Cols["taxAmt"].Width = 70;
            grid.Cols["taxAmt"].DataType = typeof(Decimal);
            grid.Cols["taxAmt"].Format = "#0.00";
            grid.Cols["taxAmt"].Visible = false;

            grid.Cols[vcol++].Name = "cessPer";
            grid[0, "cessPer"] = "Cess %";
            grid.Cols["cessPer"].Width = 50;
            grid.Cols["cessPer"].DataType = typeof(decimal);
            grid.Cols["cessPer"].Format = "#0.00";
            grid.Cols["cessPer"].Visible = false ;

            grid.Cols[vcol++].Name = "cessAmt";
            grid[0, "cessAmt"] = "Cess Amt";
            grid.Cols["cessAmt"].Width = 70;
            grid.Cols["cessAmt"].DataType = typeof(decimal);
            grid.Cols["cessAmt"].Format = "#0.00";
            grid.Cols["cessAmt"].Visible = false;

            grid.Cols[vcol++].Name = "discPer";
            grid[0, "discPer"] = "Disc %";
            grid.Cols["discPer"].Width = 50;
            grid.Cols["discPer"].DataType = typeof(Decimal);
            grid.Cols["discPer"].Format = "#0.00";
            grid.Cols["discPer"].Visible = false;


            grid.Cols[vcol++].Name = "discamt";
            grid[0, "discamt"] = "Disc Amt";
            grid.Cols["discamt"].Width = 50;
            grid.Cols["discamt"].DataType = typeof(Decimal);
            grid.Cols["discamt"].Format = "#0.00";
            grid.Cols["discamt"].Visible = false;

            grid.Cols[vcol++].Name = "accCode";
            grid[0, "accCode"] = "A/C Code";
            grid.Cols["accCode"].Width = 65;
            grid.Cols["accCode"].DataType = typeof(String);
            grid.Cols["accCode"].AllowEditing = false;
            grid.Cols["accCode"].Visible = false;

            grid.Cols[vcol++].Name = "Tdiscamt";
            grid[0, "Tdiscamt"] = "Disc Amt";
            grid.Cols["Tdiscamt"].Width = 50;
            grid.Cols["Tdiscamt"].DataType = typeof(Decimal);
            grid.Cols["Tdiscamt"].Format = "#0.00";
            grid.Cols["Tdiscamt"].Visible = false;

            grid.Cols[vcol++].Name = "netAmount";
            grid[0, "netAmount"] = "Net Amount";
            grid.Cols["netAmount"].Width = 100;
            grid.Cols["netAmount"].DataType = typeof(Decimal);
            grid.Cols["netAmount"].Format = "#0.00";
            grid.Cols["netAmount"].AllowEditing = false;

            grid.Cols.Count = vcol;
            grid.Rows.Count = 20;

            grid.AllowFreezing = AllowFreezingEnum.Columns;
            grid.Cols.Frozen = 3;


            if (cPublic.TaxIncludePrice == true)
            { grid.Cols["Price"].Visible = false ; }
            else
            { grid.Cols["Rate"].Visible = false; }
        }

        private void FormProperty()
        {
            //if (this.Tag.ToString().Substring(0, 1) == "N")
            //{
            //    this.BackColor = System.Drawing.Color.SkyBlue;
            //    panel4.BackColor = Color.SkyBlue;
            //}
            //else
            //{
            //    this.BackColor = System.Drawing.Color.DeepSkyBlue;
            //}

            lblBillAmt.Visible = true;
            lblsorder.Visible = false;
            txtFromBill.Visible = false;
            lblBillAmt.Text = "Bill Amount";
            lblAmount.ForeColor = Color.MidnightBlue;
            FlexSalesDetails.ForeColor = Color.Black;
            FlexSalesDetails.Styles.Alternate.ForeColor = Color.Black;
            FlexSalesDetails.Styles.Focus.ForeColor = Color.Black;
            FlexSalesDetails.Styles.Highlight.ForeColor = Color.Black;
            FlexSalesDetails.Styles.Normal.ForeColor = Color.Black;
            FlexSalesDetails.Styles.Search.ForeColor = Color.Black;
            FlexSalesDetails.Styles.Frozen.ForeColor = Color.Black;
            FlexSalesDetails.Styles.Fixed.ForeColor = Color.Black;
            FlexSalesDetails.Styles.Fixed.BackColor = Color.PowderBlue;
            FlexSalesDetails.Styles.Focus.BackColor = System.Drawing.Color.MistyRose;
            FlexSalesDetails.Styles.Highlight.BackColor = System.Drawing.Color.PapayaWhip;

        }

        private void defaultvalsel()
        {
            btnUpdate.Visible = false;
            cmbFormType.Enabled = true;
            txtexpense.Text = "0.00";



            cmbFormType.Items.Clear();

            cmbFormType.Items.AddRange(new object[] { "A", "B" });


            if (this.Tag.ToString().Substring(1, 1) == "E")
            {
                FlexSalesDetails.Row = 1;
                FlexSalesDetails.Col = FlexSalesDetails.Cols["itmCode"].Index;
            }

            cmbFormType.Text = cPublic.FormType;
            cmbBillType.Text = cPublic.BillType;
            cmbPriceType.Text = cPublic.PriceType;

            if (this.Tag.ToString().Substring(1, 1) == "E")
            {
                dtpDate.Value = cPublic.g_Rdate;
            }
        }

        private void CtrlsEnbl(Boolean enable)
        {
            cmbPriceType.Enabled = enable;
            cmbBillType.Enabled = enable;
            txtexpense.Enabled = enable;
            txtRemarks.Enabled = enable;
            txtCustomer.Enabled = enable;
            btnUpdate.Enabled = enable;
            btnSave.Enabled = enable;
            btnSavePrint.Enabled = enable;
            FlexSalesDetails.Enabled = enable;
            txtSalesPerson.Enabled = enable;  
        }

        private bool fill_Datas(int OrderNum, string Param, bool vDefault)
        {
            if (cgen.isFormClosing())
            { return true; }

            int rwIncr = 1;
            cmd.Connection = cPublic.Connection;
            Cursor.Current = Cursors.WaitCursor;
            CashCred = string.Empty;
            string mainTab = "";
            string childTab = "";

            if (Param.ToUpper() == "NORMAL")
            { mainTab = mastertable1; childTab = subtable1; }
            else if (Param.ToUpper() == "FROMBILL")
            { mainTab = "SALES"; childTab = "SITEM"; }
            else if (Param.ToUpper() == "QTN")
            { mainTab = "QTN"; childTab = "QTNITEM"; }
            else if (Param.ToUpper() == "SORDER")
            { mainTab = "SORDER"; childTab = "SOITEM"; }


            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@strorder", OrderNum);

            cmd.Connection = cPublic.Connection;
            cmd.CommandText = "SELECT * from " + mainTab + firmCode + "  where orderno=@strorder ";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                if ((mainTab.ToUpper().Contains("SORDER") == true || mainTab.ToUpper().Contains("QTN") == true) && vDefault == false)
                {
                    dtpDate.Value = cPublic.g_Rdate;
                    olddate = cPublic.g_Rdate;
                }
                else
                {
                    dtpDate.Value = Convert.ToDateTime(dr["DATE1"]);
                    olddate = Convert.ToDateTime(dr["DATE1"]);
                }
                if (Param.ToUpper() == "FROMBILL")
                {
                    dtpDate.Value = cPublic.g_Rdate;
                    olddate = cPublic.g_Rdate;
                }
                cmbFormType.Text = dr["FORM"].ToString();

                if (Param.ToUpper() != "FROMBILL")
                {
                    if (this.Tag.ToString().Substring(0, 1) == "N")
                    {
                        if (dr["SALESORDERNO"].Equals(DBNull.Value) == false)
                        { this.txtFromBill.Text = dr["SALESORDERNO"].ToString(); }

                    }
                }

                //BILL TYPE RE-VALIDATION
                //-----------------------
                if (dr["BILLTYPE"].ToString() == "1")
                {
                    cmbBillType.Text = "CASH";
                }
                else
                {
                    cmbBillType.Text = "CREDIT";
                }

                //PRICE TYPE RE-VALIDATION
                //------------------------
                switch (dr["PRICETYPE"].ToString())
                {
                    case "1":
                        cmbPriceType.Text = "Retail";
                        break;
                    case "2":
                        cmbPriceType.Text = "Whole Sale";
                        break;
                    case "3":
                        cmbPriceType.Text = "Special";
                        break;
                    case "4":
                        cmbPriceType.Text = "Project Price";
                        break;
                }

                txtCustomer.Tag = dr["CUSTCODE"].ToString();
                txtCustomer.Text = dr["CUSTNAME"].ToString();
                customerbo.CustomerName   = dr["PCUSTNAME"].ToString();
                customerbo.Address   = dr["PADDRESS"].ToString();
                customerbo.ContactNo   = dr["PCONTACTNO"].ToString();


                txtSalesPerson.Tag = dr["SALESPERSON"].ToString();
                txtSalesPerson.Text  = dr["SALESPERSON"].ToString();
                txtRoundoff.Text = dr["ROUNDOFF"].ToString();
                txtexpense.Text = Convert.ToDecimal(dr["expense"] + "").ToString("0.00");
                txtRoundoff.ReadOnly = true;
                lblAmount.Text = dr["BILLAMT"].ToString();
                txtRemarks.Text = dr["REMARKS1"].ToString();
                labTax.Text = dr["TAXAMT"].ToString();
                lblCess.Text = dr["CESSAMT"].ToString();
                lbltotaldiscount.Text = Convert.ToDecimal(Convert.ToDecimal(dr["bildiscamt"].ToString()) + Convert.ToDecimal(dr["spdiscamt"].ToString())).ToString("#0.00");
                
                dr.Close();
                saveFlg = true;

                if (this.Tag.ToString() == "SM")
                {
                    if (cPublic.SalesEntryDate == "Editable")
                        dtpDate.Enabled = true;
                    else dtpDate.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("Order Number Not Found", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                saveFlg = false;
                dr.Close();

                if (Param.ToUpper() == "NORMAL")
                { txtOrderno.Enabled = true; txtOrderno.Focus(); txtOrderno.SelectAll(); CtrlsEnbl(false); }

                return false;
            }
            if (txtCustomer.Tag + "".ToString() == "CANCEL")
            {
                if (mastertable1.Trim().ToUpper() == mainTab.Trim().ToUpper())
                {
                    if (MessageBox.Show("Order Number Was Cancelled , Sure to Modify", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        saveFlg = false;
                        CtrlsEnbl(false);
                        refresh();

                        txtOrderno.Focus();
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show("Order Number Was Cancelled ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    saveFlg = false;
                    CtrlsEnbl(false);
                    refresh();

                    txtOrderno.Focus();
                    dr.Close();
                    return false;
                }
            }

            dr.Close();
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cPublic.Connection;
            cmd.Parameters.AddWithValue("@strorder", OrderNum);
            cmd.CommandText = "select a.*,b.MULTIUNIT from " + childTab + firmCode + " a, "
            + " stockmst" + firmCode + " b where a.orderno=@strorder  "
            + " and a.itemcode=b.itemcode order by a.recno";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    FlexSalesDetails[rwIncr, "SLNo"] = rwIncr;

                    FlexSalesDetails[rwIncr, "Rate"] = dr["RATE"].ToString();
                    FlexSalesDetails[rwIncr, "itmCode"] = dr["ITEMCODE"].ToString();
                    FlexSalesDetails[rwIncr, "itmName"] = dr["ITEMNAME"].ToString();
                    FlexSalesDetails[rwIncr, "NetAmount"] = dr["NetAmt"].ToString();

                    FlexSalesDetails[rwIncr, "Qty"] = dr["QTY"].ToString();
                    FlexSalesDetails[rwIncr, "foc"] = dr["FOC"].ToString();
                    FlexSalesDetails[rwIncr, "Unit"] = dr["Unit"].ToString();
                    FlexSalesDetails[rwIncr, "UnitValue"] = dr["UnitValue"].ToString();
                    FlexSalesDetails[rwIncr, "multiunit"] = "N";
                    FlexSalesDetails[rwIncr, "Price"] = dr["PRICE"].ToString();
                    FlexSalesDetails[rwIncr, "taxPer"] = dr["TAXPER"].ToString();
                    FlexSalesDetails[rwIncr, "TaxAmt"] = dr["TAXAMT"].ToString();
                    FlexSalesDetails[rwIncr, "cessPer"] = dr["CESSPER"].ToString();

                    FlexSalesDetails[rwIncr, "CessAmt"] = dr["CESSAMT"].ToString();
                   // FlexSalesDetails[rwIncr, "Amount"] = dr["ITEMTOTAL"].ToString();
                    FlexSalesDetails[rwIncr, "DISCPER"] = dr["DISCPER"].ToString();
                    FlexSalesDetails[rwIncr, "discAmt"] = dr["DISCAMT"].ToString();

                    counterflag = rwIncr;
                    rwIncr++;
                    if (FlexSalesDetails.Rows.Count <= rwIncr + 2)
                    { FlexSalesDetails.Rows.Add(); }
                }
            }
            dr.Close();

            FlexSalesDetails.Row = counterflag + 2;
            FlexSalesDetails.Col = FlexSalesDetails.Cols["itmCode"].Index;
            btnUpdate.Visible = false;

            dr.Close();

            CalcSpec();
            Cursor.Current = Cursors.Default;
            SendKeys.Send("{Tab}");
            return true;
        }

        private void CalcSpec()
        {
            if (txtSplDiscAmt.Text.Trim() != string.Empty)
            {
                txtSplDiscAmt.Text = Strings.Format(Convert.ToDecimal(txtSplDiscAmt.Text.Trim()), "######0.00");
                for (int i = 1; i <= counterflag; i++)
                { CalcLine(i, FlexSalesDetails); }
                Calc_Bill(FlexSalesDetails);
            }
            else
            {
                txtSplDiscAmt.Focus();
                txtSplDiscAmt.Text = "0.00";
                txtSplDiscAmt.SelectAll();
            }
        }

        private void CalcLine(int rw, C1FlexGrid flxName)
        {
            decimal  s_Qty = 0;
            decimal s_Price = 0;
            decimal s_tax = 0;
            decimal s_spdiscamt = 0;
            decimal s_udiscper = 0;
            decimal s_udiscamt = 0;
            decimal s_Amount = 0;
            decimal s_Netamt = 0;
            decimal s_rate = 0;
            decimal s_Cess = 0;
            decimal s_Taxper = 0;
            decimal s_Cessper = 0;
            decimal s_itemtotal = tamount;
            decimal s_weight = 0;
            decimal s_wtcalc = 0;
            if (s_itemtotal == 0) { s_itemtotal = 1; }

            if (flxName[rw, "Qty"] == null)
            { return; }

            if (cPublic.TaxIncludePrice ==true )
            {
                if ((Convert.ToString(flxName[rw, "Rate"]) == "") ||
                    (Convert.ToString(flxName[rw, "Rate"]) == "."))
                { s_rate = 0; }
                else
                { s_rate = Convert.ToDecimal(flxName[rw, "Rate"]); }
            }
            else
            {
                if ((Convert.ToString(flxName[rw, "Price"]) == "") ||
                    (Convert.ToString(flxName[rw, "Price"]) == "."))
                { s_Price = 0; }
                else
                { s_Price = Convert.ToDecimal(flxName[rw, "Price"]); }
            }
           
            if ((Convert.ToString(flxName[rw, "Qty"]) == "") ||
                (Convert.ToString(flxName[rw, "Qty"]) == "."))
            { s_Qty = 0; }
            else
            { s_Qty = Convert.ToDecimal(flxName[rw, "Qty"]); }

            if (Convert.ToString(flxName[rw, "taxPer"]) == "")
            { s_Taxper = 0; }
            else
            { s_Taxper = Convert.ToDecimal(flxName[rw, "taxPer"]); }

            if (Convert.ToString(flxName[rw, "cessPer"]) == "")
            { s_Cessper =0; }
            else
            { s_Cessper = Convert.ToDecimal(flxName[rw, "cessPer"]); }

            if (Convert.ToString(flxName[rw, "DISCPER"]) == "")
            { s_udiscper = 0; }
            else
            { s_udiscper = Convert.ToDecimal(flxName[rw, "DISCPER"]); }

            if (cPublic.TaxIncludePrice == true)
            {
                s_Amount = s_Qty * s_rate;
                if (s_Qty != 0)
                {
                    //s_Price = (s_rate - (s_udiscamt / s_Qty) - (s_spdiscamt / s_Qty)) / ((100 + s_Taxper) + (s_Taxper * s_Cessper / 100)) * 100;
                    s_Price = s_rate / ((100 + s_Taxper) + (s_Taxper * s_Cessper / 100)) * 100;
                    s_Amount = s_Qty * s_Price;
                    s_udiscamt = s_Amount * s_udiscper / 100;
                    s_spdiscamt = s_Amount / s_itemtotal * Convert.ToDecimal(txtSplDiscAmt.Text.Trim());

                    s_Price = s_Price - (s_udiscamt / s_Qty) - (s_spdiscamt / s_Qty);
                }
                else
                { flxName[rw, "Price"] = 0.00; }
            }
            else
            {
                s_Amount = s_Qty * s_Price;

                s_udiscamt = s_Price * s_udiscper / 100;
                s_spdiscamt =s_Amount / s_itemtotal * Convert.ToDecimal(txtSplDiscAmt.Text.Trim());
                if (s_Qty != 0)
                {
                    s_Price = s_Price - s_udiscamt - s_spdiscamt;
                    s_tax = (s_Price + (s_udiscamt / s_Qty) + (s_spdiscamt / s_Qty)) * s_Taxper / 100;
                    s_Cess = s_tax * (s_Cessper / 100);
                    s_rate = s_Price + s_tax;
                }
                else
                { flxName[rw, "Rate"] = 0.00; }

                s_udiscamt = s_udiscamt * s_Qty;
            }

            s_wtcalc = s_Qty * s_weight;

            s_Amount = s_Qty * s_Price;
            s_tax = s_Amount * (s_Taxper / 100);

            s_Cess = s_tax * (s_Cessper / 100);

            s_Netamt = s_Amount + s_tax + s_Cess;
            s_Amount = s_Qty * s_rate;

            //LOADING VALUES TO THE GRID
            //--------------------------
            //flxName[rw, "Amount"] = s_Amount.ToString("######0.00");
            if (cPublic.TaxIncludePrice == true)
            { flxName[rw, "Price"] = s_Price.ToString("######0.00"); }
            else { flxName[rw, "Rate"] = s_rate.ToString("######0.00"); }
            //flxName[rw, "Discount"] = s_spdiscamt.ToString("######0.00");
            flxName[rw, "TaxAmt"] = s_tax.ToString("######0.00");
            flxName[rw, "CessAmt"] = s_Cess.ToString("######0.00");
            flxName[rw, "NetAmount"] = s_Netamt.ToString("######0.00");
            flxName[rw, "discAmt"] = s_udiscamt.ToString("######0.00");

        }

        private void Calc_Bill(C1FlexGrid flxName)
        {
            decimal j = 0;
            decimal ttaxamt = 0;
            decimal tCessamt = 0;
            decimal tnetamt = 0;
            tamount = 0;
            tdiscount = 0;
            decimal tudiscount = 0;
            decimal expense = Convert.ToDecimal(txtexpense.Text);
            for (int i = 1; i <= flxName.Rows.Count - 1; i++)
            {
                if (flxName[i, "itmName"] + "" == "") { break; }

                //tamount += Convert.ToDecimal(flxName[i, "amount"]);
                tamount += Convert.ToDecimal(flxName[i, "price"]) * Convert.ToDecimal(flxName[i, "Qty"]);
                tudiscount += Convert.ToDecimal(flxName[i, "discAmt"]);
                //tdiscount += Convert.ToDecimal(flxName[i, "Discount"]);
                ttaxamt += Convert.ToDecimal(flxName[i, "TaxAmt"]);
                tCessamt += Convert.ToDecimal(flxName[i, "CessAmt"]);
                tnetamt += Convert.ToDecimal(flxName[i, "NetAmount"]);
            }
            tnetamt = tnetamt + expense;
            lblAmount.Text = tnetamt.ToString("###0.00");
            labTax.Text = ttaxamt.ToString("###0.00");
            lblCess.Text = tCessamt.ToString("###0.00");
            lbltotaldiscount.Text = Convert.ToDecimal(tudiscount + tdiscount).ToString("#0.00");
            if (cPublic.RoundOff == "Paise")
            {
                txtRoundoff.Text = Strings.Format(Math.Round(Math.Round(tnetamt, 1) - tnetamt, 2), "########0.00"); // * NEARST 10 PAISE
                lblAmount.Text = Strings.Format((tnetamt + (Convert.ToDecimal(txtRoundoff.Text.Trim()))), "######0.00");
            }
            else if (cPublic.RoundOff == "Rupees")
            {
                j = Math.Round(tnetamt, 1);
                j = Math.Round(j);
                txtRoundoff.Text = Strings.Format((j - tnetamt), "########0.00");  // '* NEARST 1 RUPEE
                lblAmount.Text = Strings.Format(j, "########0.00");
            }
        }

        private void refresh()
        {
            if (FlexSalesDetails.Editor != null)
            {
                FlexSalesDetails.Editor.Text = string.Empty;
            }

            FlexSalesDetails.Clear();

            FlexSalesDetails.Row = 1;
            FlexSalesDetails.Col = 1;

            grid_initialize(FlexSalesDetails);
            FlexSalesDetails.Focus();
            FlexSalesDetails.Row = 1;
            FlexSalesDetails.Col = 1;

            if (this.Tag.ToString() == "SE")
            { txtFromBill.Visible = false; lblsorder.Visible = false; }
            else if (this.Tag.ToString() == "NE")
            { txtFromBill.Visible = true; txtFromBill.Enabled = true; lblsorder.Visible = true; }

            switch (this.Tag.ToString().Substring(1, 1))
            {
                case "E":
                    txtOrderno.Text = (ordernum()).ToString();
                    FlexSalesDetails.Row = 1;
                    FlexSalesDetails.Col = FlexSalesDetails.Cols["itmCode"].Index;
                    break;
                case "M":
                case "C":
                case "P":
                    txtOrderno.Text = (ordernum()).ToString();
                    defaultvalsel();
                    break;
            }

            txtRemarks.Text = string.Empty;
            txtCustomer.Text = string.Empty;
            txtCustomer.Tag = string.Empty;
            txtFromBill.Text = string.Empty;
            txtSalesPerson.Text = string.Empty;
            txtSalesPerson.Tag = string.Empty;
            lblCess.Text = "0.00";

            lblBillAmt.Text = "Bill Amount";
            labTax.Text = "0.00";
            lblAmount.Text = "0.00";
            txtRoundoff.Text = "0.00";
            lbltotaldiscount.Text = "0.00";

            txtexpense.Text = "0.00";
            PS = string.Empty;
            cmbBillType.Text = cPublic.BillType;
            customerbo.CustomerName = string.Empty;
            customerbo.Address  = string.Empty;
            customerbo.ContactNo  = string.Empty;

            cmbBillType.Focus();
        }

        private int ordernum()
        {
            tableSel();
            switch (this.Tag.ToString().Substring(1, 1))
            {
                case "M":
                case "C":
                case "P":
                    query = "select isnull(max(orderno),0) from " + mastertable1 + firmCode + "   ";
                    break;
                case "E":
                    query = "select isnull(max(orderno),0)+1 from " + mastertable1 + firmCode + "   ";
                    break;
            }
            cmd.Parameters.Clear();
            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        private void tableSel()//FUNCTION FOR SELECTIN THE TABLE
        {
            switch (this.Tag.ToString().Substring(0, 1))
            {
                case "S":  //========== Sales ==========\\               
                    mastertable1 = "sales";
                    subtable1 = "sitem";
                    break;

                case "N":  //========== Sales Return ==========\\
                    mastertable1 = "sreturn";
                    subtable1 = "sritem";
                    break;

                case "Q":  //========== Quotation  ==========\\                                     
                    mastertable1 = "qtn";
                    subtable1 = "qtnitem";
                    break;

                case "T":  //========== Transferout  ==========\\              
                    mastertable1 = "transfer";
                    subtable1 = "tritem";
                    break;

                case "O":  //========== SalesOrder  ==========\\               
                    mastertable1 = "sorder";
                    subtable1 = "soitem";
                    break;

                case "D":  //========== SalesOrder  ==========\\                               
                    mastertable1 = "damage";
                    subtable1 = "ditem";
                    break;
            }
        }

        private void Cust_LookUp(string str)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;
            lookup.m_table = "ACCOUNTS" + firmCode;
            lookup.m_fields = "HEAD,CODE,Address,STREET,CITY,BALANCE";
            lookup.m_condition = " groupcode=200021";
            lookup.m_caption = "Customer";
            lookup.m_dispname = "HEAD,CODE,ADDRESS,STREET,CITY,BALANCE";
            lookup.m_fldwidth = "350,0,150,150,75,100";
            lookup.m_strat = str;
            txtCustomer.ForeColor = Color.Black;
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                txtCustomer.Text = lookup.m_values[0].ToString();
                txtCustomer.Tag = lookup.m_values[1].ToString();
                Cus_cod = lookup.m_values[1].ToString();

                SendKeys.Send("{TAB}");
            }
            else
            { txtCustomer.Tag = ""; txtCustomer.Text=string.Empty; }

            for (int rw = 1; rw <= counterflag; rw++)
            {
                CalcLine(rw, FlexSalesDetails);
            }
            Calc_Bill(FlexSalesDetails);
        }

        public void Bill_Save()
        {
            string test = string.Empty;
            string str = string.Empty;

            cmd.Parameters.Clear();
            counterflag = 0;
            gridindex(FlexSalesDetails);
            tableSel();

            if (!Save_Validation())
            { return; }

            try
            {
                switch (this.Tag.ToString().Substring(1, 1))
                {
                    case "E":     //========= MASTER TABLE INSERTION ============\\
                        saveFlg = false;
                        if (MessageBox.Show("Sure To Save This Bill", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString() == "Yes")
                        {
                            # region PENDING BILL

                            CBillToBill cBill = new CBillToBill();
                            cBill.TransNo = txtOrderno.Text.Trim();
                            cBill.TransType = Tag.ToString().Substring(0, 1);
                            cBill.TransFlag = "Y";
                            cBill.Date = dtpDate.Value;
                            cBill.BillAmt = Convert.ToDecimal(lblAmount.Text.Trim());
                            cBill.Cmd = cmd;
                            cBill.FirmCode = firmCode;
                            cBill.Module = CBillToBill.module.Inventry;
                            cBill.Mode = CBillToBill.mode.Entry;

                            if (cmbBillType.Text.Trim().ToLower() == "credit")
                            {
                                cBill.CustCode = txtCustomer.Tag + "";
                                if (!cBill.ShowSettlement()) { return; }
                            }

                            # endregion

                            saveFlg = true;
                            cmd.Parameters.Clear();
                            cmd.CommandText = SP_INSERT_TABLE + "" + firmCode;
                            cmd.CommandType = CommandType.StoredProcedure;
                            Master_Param(false, firmCode);
                            txtOrderno.Text = cmd.ExecuteScalar() + "";
                            prnOrderNo = txtOrderno.Text.Trim();
                            masterOrder = Convert.ToInt64(txtOrderno.Text.Trim());
                            SUB_Table_insert(FlexSalesDetails, firmCode);
                            InvetryPosting(cmd, txtOrderno.Text.Trim(), "+", firmCode);

                            cBill.SavePendingBill();

                            str = "Bill Saved Successfully   Bill No: " + txtOrderno.Text;
                        }
                        else
                        {
                            FlexSalesDetails.Focus();
                            FlexSalesDetails.Row = counterflag + 1;
                            FlexSalesDetails.Col = FlexSalesDetails.Cols["itmCode"].Index;
                            return;
                        }
                        break;

                    case "M":       //========= MASTER TABLE MODIFICATION ============\\

                        prnOrderNo = txtOrderno.Text.Trim();
                        ordernumber = this.txtOrderno.Text.Trim();
                        saveFlg = false;


                        if (MessageBox.Show("Sure To Modify This Bill", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString() == "Yes")
                        {

                            # region PENDING BILL

                            CBillToBill cBill = new CBillToBill();
                            cBill.TransNo = txtOrderno.Text.Trim();
                            cBill.TransType = Tag.ToString().Substring(0, 1);
                            cBill.TransFlag = "Y";
                            cBill.Date = dtpDate.Value;
                            cBill.BillAmt = Convert.ToDecimal(lblAmount.Text.Trim());
                            cBill.Cmd = cmd;
                            cBill.FirmCode = firmCode;
                            cBill.Module = CBillToBill.module.Inventry;
                            cBill.Mode = CBillToBill.mode.Modify;

                            if (cmbBillType.Text.Trim().ToLower() == "credit")
                            {
                                cBill.CustCode = txtCustomer.Tag + "";
                                if (!cBill.ShowSettlement()) { return; }
                            }

                            # endregion

                            # region Modify Master
                            masterOrder = Convert.ToInt64(txtOrderno.Text.Trim());

                            InvetryPosting(cmd, txtOrderno.Text.Trim(), "-", firmCode);
                            Delete_Posting(firmCode, txtOrderno.Text.Trim());
                            cmd.Parameters.Clear();
                            cmd.CommandText = SP_UPDATE_TABLE + firmCode;
                            cmd.CommandType = CommandType.StoredProcedure;
                            Master_Param(false, firmCode);
                            cmd.Parameters.AddWithValue("@orderno", Convert.ToInt64(txtOrderno.Text.Trim()));
                            cmd.ExecuteNonQuery();
                            # endregion


                            SUB_Table_insert(FlexSalesDetails, firmCode);
                            InvetryPosting(cmd, txtOrderno.Text.Trim(), "+", firmCode);

                            saveFlg = true;

                            cBill.SavePendingBill();

                            str = "Bill Modified Successfully   Bill No: " + txtOrderno.Text.Trim();
                        }
                        else
                        { saveFlg = false; return; }

                        break;

                    case "C":      //========= MASTER TABLE CANCELLATION ============\\
                        saveFlg = false;

                        cmd.Parameters.Clear();
                        cmd.CommandType = CommandType.Text;

                        if (MessageBox.Show("Sure To Cancel This Bill", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).ToString() == "Yes")
                        {
                            InvetryPosting(cmd, txtOrderno.Text.Trim(), "-", firmCode);
                            Delete_Posting(firmCode, txtOrderno.Text.Trim());
                            cmd.Parameters.Clear();
                            cmd.CommandText = SP_CANCEL_TABLE + firmCode;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@orderno", Convert.ToInt64(txtOrderno.Text.Trim()));
                            cmd.Parameters.AddWithValue("@custcode", "CANCEL");
                            cmd.Parameters.AddWithValue("@billamt", 0);
                            cmd.Parameters.AddWithValue("@modiuser", cgen.GetUserDet());
                            cmd.ExecuteNonQuery();

                            CBillToBill cBill = new CBillToBill();
                            cBill.TransNo = txtOrderno.Text.Trim();
                            cBill.TransType = Tag.ToString().Substring(0, 1);
                            cBill.TransFlag = "Y";
                            cBill.Date = dtpDate.Value;
                            cBill.BillAmt = Convert.ToDecimal(lblAmount.Text.Trim());
                            cBill.Cmd = cmd;
                            cBill.FirmCode = firmCode;
                            cBill.Module = CBillToBill.module.Inventry;
                            cBill.Mode = CBillToBill.mode.Cancel;

                            cBill.SavePendingBill();


                            masterOrder = Convert.ToInt64(txtOrderno.Text.Trim());

                            str = "Bill Cancelled Successfully   Bill No: " + txtOrderno.Text.Trim();
                            saveFlg = true;
                        }
                        else
                        { saveFlg = true; return; }

                        break;


                    case "P":
                        if (MessageBox.Show("Sure To Print This Bill", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString() == "Yes")
                        {
                            ordernumber = txtOrderno.Text.Trim();
                            prnOrderNo = txtOrderno.Text.Trim();
                            PS = "PRINT";
                            saveFlg = true;
                        }
                        else
                        { saveFlg = true; return; }

                        break;
                }


                #region Print

                int notepadpath = (Convert.ToInt32(1000 * VBMath.Rnd()) + 1);

                if (Directory.Exists(cPublic.FilePath + "\\" + notepadpath + ".txt"))
                {
                    Directory.Delete(cPublic.FilePath + "\\" + notepadpath + ".txt");
                }

                string path = cPublic.FilePath + "\\" + notepadpath + ".txt";

                if (PS == "PRINT")
                {
                    if (cPublic.PrintMode == "WINDOWS")
                    {
                        new cWindowsPrint(mastertable1 + firmCode, subtable1 + firmCode, Convert.ToInt32(prnOrderNo));
                    }
                    else
                    {
                        cBillPrints billprints = new cBillPrints();
                        if (cPublic.PrintType == "Printed")
                        {
                            StreamWriter tstream = File.CreateText(path);
                            billprints.BillPlains(tstream, mastertable1 + firmCode, subtable1 + firmCode, Convert.ToInt32(prnOrderNo));

                            tstream.Flush();
                            tstream.Close();

                            cgen.RepPrint(notepadpath.ToString());
                        }
                        else if (cPublic.PrintType == "Plain")
                        {
                            StreamWriter tstream = File.CreateText(path);

                            billprints.BillPlains(tstream, mastertable1 + firmCode, subtable1 + firmCode, Convert.ToInt32(prnOrderNo));
                            tstream.Flush();
                            tstream.Close();
                            if (notepadpath != 0)
                            {
                                cgen.RepPrint(notepadpath.ToString());
                            }
                        }
                        else
                        {
                            StreamWriter tstream = File.CreateText(path);

                            billprints.PBill(Convert.ToInt32(prnOrderNo), mastertable1 + firmCode, subtable1 + firmCode, tstream, cPublic.PrintType);
                            tstream.Flush();
                            tstream.Close();
                            if (notepadpath != 0)
                            {
                                cgen.RepPrint(notepadpath.ToString());
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                str = ex.ToString();
                MessageBox.Show(ex.Message, cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            finally
            {
                Cursor.Current = Cursors.Arrow;
            }
        }

        private void Delete_Posting(string FirmCode,  string Order)
        {
            cmd.Parameters.Clear();
            cmd.CommandText = SP_DELETE_SUBTABLE + FirmCode;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@orderno", Convert.ToInt64(Order));
            cmd.ExecuteNonQuery();
        }

        private void Master_Param(bool cancel, string vfirmcode)
        {
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@date1", dtpDate.Value.ToString("yyyy-MM-dd"));
            if (this.Tag.ToString().Substring(1, 1) == "M")
            {
                cmd.Parameters.AddWithValue("@modiuser", cgen.GetUserDet());
            }
            else
            {
                cmd.Parameters.AddWithValue("@userid", cgen.GetUserDet());
            }
            if (Information.IsNumeric(this.txtFromBill.Text.Trim()) == true && this.Tag.ToString().Substring(0, 1) == "N")
            { cmd.Parameters.AddWithValue("@SORDERNO", Convert.ToInt64(txtFromBill.Text.Trim())); }

            cmd.Parameters.AddWithValue("@form", cmbFormType.Text.Trim());
            cmd.Parameters.AddWithValue("@billtype", Bill_Type);
            cmd.Parameters.AddWithValue("@pricetype", Price_Type);
            cmd.Parameters.AddWithValue("@custname", txtCustomer.Text.Trim());

            cmd.Parameters.AddWithValue("@pcustname", customerbo.CustomerName  );
            cmd.Parameters.AddWithValue("@paddress", customerbo.Address  );
            cmd.Parameters.AddWithValue("@pcontactno", customerbo.ContactNo  );

            cmd.Parameters.AddWithValue("@SALESPERSON", txtSalesPerson.Tag + "");
            cmd.Parameters.AddWithValue("@remarks1", txtRemarks.Text.Trim());
            if (cancel == false)
            {
                cmd.Parameters.AddWithValue("@custcode", Cus_cod);
                cmd.Parameters.AddWithValue("@billamt", Convert.ToDecimal(lblAmount.Text.Trim()));
                cmd.Parameters.AddWithValue("@spdiscamt", tdiscount);
                cmd.Parameters.AddWithValue("@bildiscamt", Convert.ToDecimal(txtSplDiscAmt.Text.Trim()));
                cmd.Parameters.AddWithValue("@itemtotal", tamount);
                cmd.Parameters.AddWithValue("@cessamt", Convert.ToDecimal(lblCess.Text.Trim()));
                cmd.Parameters.AddWithValue("@taxamt", Convert.ToDecimal(labTax.Text.Trim()));
                cmd.Parameters.AddWithValue("@roundoff", Convert.ToDecimal(txtRoundoff.Text.Trim()));
                cmd.Parameters.AddWithValue("@expense", Convert.ToDecimal(txtexpense.Text.Trim()));
            }
            else if (cancel == true)
            {
                cmd.Parameters.AddWithValue("@custcode", "CANCEL");
                cmd.Parameters.AddWithValue("@billamt", 0);
                cmd.Parameters.AddWithValue("@spdiscamt", 0);
                cmd.Parameters.AddWithValue("@bildiscper", 0);
                cmd.Parameters.AddWithValue("@bildiscamt", 0);
                cmd.Parameters.AddWithValue("@itemtotal", 0);
                cmd.Parameters.AddWithValue("@cessamt", 0);
                cmd.Parameters.AddWithValue("@taxamt", 0);
                cmd.Parameters.AddWithValue("@roundoff", 0);
                cmd.Parameters.AddWithValue("@expense", 0);
            }
        }

        private void SUB_Table_insert(C1FlexGrid flxName, String Firmcode)
        {
            try
            {
                for (int rw = 1; rw < flxName.Rows.Count; rw++)
                {
                    if (flxName[rw, "itmName"] + "" == string.Empty) { break; }
                    cmd.Parameters.Clear();
                    cmd.CommandText = SP_INSERT_SUBTABLE + "" + Firmcode;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (Convert.ToDecimal(flxName[rw, "Qty"]) != 0)
                    {
                        cmd.Parameters.AddWithValue("@date1", dtpDate.Value.ToString("yyyy-MM-dd"));
                        cmd.Parameters.AddWithValue("@orderno", Convert.ToInt64(txtOrderno.Text));
                        cmd.Parameters.AddWithValue("@rate", Convert.ToDecimal(flxName[rw, "Rate"]));
                        cmd.Parameters.AddWithValue("@billtype", Bill_Type);
                        cmd.Parameters.AddWithValue("@itemcode", Convert.ToString(flxName[rw, "itmCode"]));
                        cmd.Parameters.AddWithValue("@itemname", Convert.ToString(flxName[rw, "itmName"]));
                        cmd.Parameters.AddWithValue("@netamt", Convert.ToDecimal(flxName[rw, "NetAmount"]));
                        cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(flxName[rw, "Qty"]));
                        cmd.Parameters.AddWithValue("@foc", Convert.ToDecimal(flxName[rw, "foc"]));
                        cmd.Parameters.AddWithValue("@UnitValue", Convert.ToDecimal(flxName[rw, "UnitValue"]));
                        cmd.Parameters.AddWithValue("@Unit", Convert.ToString(flxName[rw, "Unit"]));
                        cmd.Parameters.AddWithValue("@price", Convert.ToDecimal(flxName[rw, "Price"]));
                        cmd.Parameters.AddWithValue("@taxper", Convert.ToDecimal(flxName[rw, "taxPer"]));
                        cmd.Parameters.AddWithValue("@taxamt", Convert.ToDecimal(flxName[rw, "TaxAmt"]));
                        cmd.Parameters.AddWithValue("@cessper", Convert.ToDecimal(flxName[rw, "cessPer"]));
                        cmd.Parameters.AddWithValue("@cessamt", Convert.ToDecimal(flxName[rw, "CessAmt"]));
                        cmd.Parameters.AddWithValue("@discper", Convert.ToDecimal(flxName[rw, "DISCPER"]));
                        //cmd.Parameters.AddWithValue("@discamt", Convert.ToDecimal(flxName[rw, "discAmt"]) + Convert.ToDecimal(flxName[rw, "Discount"]));
                        cmd.Parameters.AddWithValue("@itemtotal", Convert.ToDecimal(flxName[rw, "Price"]) * Convert.ToDecimal(flxName[rw, "Qty"]));                        
                        cmd.ExecuteNonQuery();
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Delete_Posting(string FirmCode, string Series, string Order)
        {
            cmd.Parameters.Clear();
            cmd.CommandText = SP_DELETE_SUBTABLE + FirmCode;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@orderno", Convert.ToInt64(Order));
            cmd.ExecuteNonQuery();
        }

        private void gridindex(C1FlexGrid flxName)
        {
            for (int i = 1; i < flxName.Rows.Count - 1; i++)
            {
                if (flxName[i, "itmName"] + "" == "") { break; }
                flxName[i, "slno"] = i;
                counterflag = i;
            }
        }

        private void InvetryPosting(SqlCommand cmd, string orderno, string type, string vfirmcode)
        {
            if (this.Tag.ToString().Substring(0, 1) != "S" && this.Tag.ToString().Substring(0, 1) != "N") { return; }
            cmd.Parameters.Clear();
            cmd.CommandText = "SP_POSTING_" + mastertable1 + vfirmcode;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@orderno", Convert.ToInt64(orderno));
            if (this.Tag.ToString().Substring(1, 1) == "M" && type == "-")
            {
                cmd.Parameters.AddWithValue("@date", olddate.ToString("yyyy-MM-dd"));
            }
            else
            {
                cmd.Parameters.AddWithValue("@date", this.dtpDate.Value.ToString("yyyy-MM-dd"));
            }
            cmd.Parameters.AddWithValue("@type", type.Trim());
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
        }
      
        private Boolean Save_Validation()
        {
            cmd.Parameters.Clear();
            cmd.Connection = cPublic.Connection;
            DateTime dt;

            if ((this.Tag.ToString().Substring(1, 1) != "C") && (this.Tag.ToString().Substring(1, 1) != "P"))
            {

                // ITEM VALIDATION
                //----------------
                if (counterflag == 0)
                {
                    MessageBox.Show("Select an Item \nPress F5 for lookup.", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    FlexSalesDetails.Focus();
                    saveFlg = false;
                    return false;
                }

                //FINANCIAL YEAR DATE VALIDATION
                //------------------------------                
                dt = Convert.ToDateTime(dtpDate.Value.ToString("yyyy-MM-dd"));
                if ((dt < cPublic.g_OpDate) || (dt > cPublic.g_ClDate))
                {
                    MessageBox.Show("Out of Financial Year.  Please check", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    saveFlg = false;
                    return false;
                }             

                for (int rw = 1; rw <= counterflag; rw++)
                {
                    CalcLine(rw, FlexSalesDetails);
                    
                    string itemName = FlexSalesDetails[rw, "itmName"] + "".ToString();
                    if (!chek_Item(FlexSalesDetails[rw, "itmCode"] + "".ToString(), itemName))
                    {
                        MessageBox.Show("Item Not Found.   Please Check.", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        FlexSalesDetails.Focus();
                        FlexSalesDetails.Row = rw;
                        FlexSalesDetails.Col = FlexSalesDetails.Cols["itmCode"].Index;
                        saveFlg = false;
                        return false;
                    }

                

                    if (this.Tag.ToString() != "NE")
                    {
                            if ((FlexSalesDetails[rw, "Qty"].ToString() == "0.000") ||
                                (FlexSalesDetails[rw, "Qty"] == null) ||
                                (FlexSalesDetails[rw, "Qty"].ToString() == "") ||
                                (FlexSalesDetails[rw, "Qty"].ToString() == "0"))
                            {
                                MessageBox.Show("Cannot Save Bill Without Quantity.   Bill No: " + txtOrderno.Text.Trim(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                saveFlg = false;
                                FlexSalesDetails.Focus();
                                FlexSalesDetails.Row = rw;
                                FlexSalesDetails.Col = FlexSalesDetails.Cols["Qty"].Index;
                                return false;
                            }
                    }

                
                    if (cPublic.TaxIncludePrice == true)
                    {
                        if ((FlexSalesDetails[rw, "Rate"].ToString() == "0.00") ||
                                (FlexSalesDetails[rw, "Rate"] == null) ||
                                (FlexSalesDetails[rw, "Rate"].ToString() == "") ||
                                (FlexSalesDetails[rw, "Rate"].ToString() == "0"))
                        {
                            MessageBox.Show("Cannot Save Bill Without A Rate.  Bill No: " +  txtOrderno.Text.Trim(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            saveFlg = false;
                            FlexSalesDetails.Focus();
                            FlexSalesDetails.Row = rw;
                            FlexSalesDetails.Col = FlexSalesDetails.Cols["Rate"].Index;
                            return false;
                        }
                    }
                    else
                    {
                        if ((FlexSalesDetails[rw, "Price"].ToString() == "0.00") ||
                                (FlexSalesDetails[rw, "Price"] == null) ||
                                (FlexSalesDetails[rw, "Price"].ToString() == "") ||
                                (FlexSalesDetails[rw, "Price"].ToString() == "0"))
                        {
                            MessageBox.Show("Cannot Save Bill Without A Price.  Bill No: " +  txtOrderno.Text.Trim(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            saveFlg = false;
                            FlexSalesDetails.Focus();
                            FlexSalesDetails.Row = rw;
                            FlexSalesDetails.Col = FlexSalesDetails.Cols["Price"].Index;
                            return false;
                        }
                    }

                    if (!chek_Item_qty(FlexSalesDetails[rw, "itmCode"] + "".ToString(), Convert.ToDecimal ( FlexSalesDetails[rw, "Qty"])) && this.Tag.ToString() == "SE")
                    {
                        MessageBox.Show("Quantity Not Found.   Please Check.", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        FlexSalesDetails.Focus();
                        FlexSalesDetails.Row = rw;
                        FlexSalesDetails.Col = FlexSalesDetails.Cols["Qty"].Index;
                        saveFlg = false;
                        return false;
                    }


                    if (cPublic.TaxIncludePrice==true )
                    {
                        if ((FlexSalesDetails[rw, "Rate"] == null) || (FlexSalesDetails[rw, "Rate"].ToString() == ""))
                        {
                            MessageBox.Show("Cannot Save Bill Without A Rate.  Bill No: " +  txtOrderno.Text.Trim(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            saveFlg = false;
                            FlexSalesDetails.Focus();
                            FlexSalesDetails.Row = rw;
                            FlexSalesDetails.Col = FlexSalesDetails.Cols["Rate"].Index;
                            return false;
                        }
                    }
                    else
                    {
                        if ((FlexSalesDetails[rw, "Price"] == null) || (FlexSalesDetails[rw, "Price"].ToString() == ""))
                        {
                            MessageBox.Show("Cannot Save Bill Without A Price.  Bill No: " +  txtOrderno.Text.Trim(), cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            saveFlg = false;
                            FlexSalesDetails.Focus();
                            FlexSalesDetails.Row = rw;
                            FlexSalesDetails.Col = FlexSalesDetails.Cols["Price"].Index;
                            return false;
                        }
                    }                   
                }

                Calc_Bill(FlexSalesDetails);

                if (cmbBillType.Text.Trim() == "CREDIT")
                {
                    if ((txtCustomer.Text.Trim() == "") || (txtCustomer.Text.Trim() == "Press (F5 or +) keys for LookUp"))
                    {
                        MessageBox.Show("Enter/Select Customer Name", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCustomer.Focus();
                        txtCustomer.SelectAll();
                        saveFlg = false;
                        return false;
                    }
                    else
                    {
                       cmd.Parameters.Clear();
                       cmd.CommandType = CommandType.Text;
                       cmd.CommandText = "select * from accounts" + firmCode + " where code=@code ";
                       cmd.Parameters.AddWithValue("@code", txtCustomer.Tag + "");
                        dr =cmd.ExecuteReader();
                        if (dr.Read())
                        {
                            Cus_cod = dr["CODE"].ToString();
                            dr.Close();
                        }
                        else
                        {
                            MessageBox.Show("Customer Name Does Not Exist", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            saveFlg = false;
                            txtCustomer.Focus();
                            dr.Close();
                            return false;
                        }
                        dr.Close();
                    }
                }
                else
                {
                   cmd.Parameters.Clear();
                   cmd.CommandType = CommandType.Text;
                   cmd.CommandText = "select * from accounts" + firmCode + " where code=@code";
                   cmd.Parameters.AddWithValue("@code", Convert.ToString(txtCustomer.Tag + ""));
                    dr =cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        Cus_cod = dr["CODE"].ToString();
                        dr.Close();
                    }
                    dr.Close();
                }

                

                //BILL TYPE VALIDATION
                //-------------------
                if (cmbBillType.Text.Trim() == "CASH")
                {
                    Bill_Type = "1";
                }
                else
                {
                    Bill_Type = "2";
                }


                //PRICE TYPE VALIDATION
                //---------------------
                switch (cmbPriceType.Text.Trim())
                {
                    case "Retail":
                        Price_Type = "1";
                        break;
                    case "Whole Sale":
                        Price_Type = "2";
                        break;
                    case "Special":
                        Price_Type = "3";
                        break;
                    case "Project Price":
                        Price_Type = "4";
                        break;

                }



                // TOTAL AMOUNT VALIDATION
                //-------------------------
                if (Convert.ToDecimal(lblAmount.Text.Trim()) <= 0 )
                {
                    MessageBox.Show("Amount Should Be Greater Than Zero", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    saveFlg = false;
                    return false;
                }

                

            }

            return true;
        }

        private Boolean chek_Item(string itmCode, string itemName)
        {
            Boolean status = false;
            string query = "select * from stockmst" + firmCode + " where itemcode = @itmcode";
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@itmcode", itmCode);
            if (cmd.ExecuteScalar() != null) { status = true; }
            return status;
        }

        private Boolean chek_Item_qty(string itmCode, decimal  qty)
        {
            Boolean status = false;
            string query = "select * from stockmst" + firmCode + " where itemcode = @itmcode and qty>=@qty ";
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@itmcode", itmCode);
            cmd.Parameters.AddWithValue("@qty", qty);
            if (cmd.ExecuteScalar() != null) { status = true; }
            return status;
        }

        private void load_Item_Lookup(int row)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;
            lookup.m_caption = "Item Details";
            lookup.m_table = "Stockmst" + firmCode + "";
            lookup.m_condition = "itemname<>'' ";
            if (cPublic.LookupDefault == "Itemcode")
            {
                lookup.m_fields = "itemcode,itemname,unit,qty,rprofit1,taxper,cessper,groupcode";
                lookup.m_dispname = "Item Code, Item Name,Unit,Qty,Retail, Tax %, Cess %,Group";
                lookup.m_fldwidth = "120,300,100,100,80,60,60,100";
                lookup.m_order = "itemcode";
            }
            else
            {
                lookup.m_fields = "itemname,itemcode,unit,qty,rprofit1,taxper,cessper,groupcode";
                lookup.m_dispname = "Item Name,Item Code,Unit,Qty,Retail, Tax %, Cess %,Group";
                lookup.m_fldwidth = "300,120,100,100,80,60,60,100";
                lookup.m_order = "itemname";
            }
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                if (cPublic.LookupDefault == "Itemcode")
                { FlexSalesDetails.Editor.Text = lookup.m_values[0].ToString(); }
                else
                { FlexSalesDetails.Editor.Text = lookup.m_values[1].ToString(); }
                SendKeys.Send("{Tab}");
            }
        }

        private void load_Cess_Lookup(int row)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;
            lookup.m_caption = "Cess Details";
            lookup.m_condition = "Field1='CESSPER'";
            lookup.m_table = "LOOKUP";
            lookup.m_fields = "Code,Details";
            lookup.m_dispname = "Code,Details";
            lookup.m_fldwidth = "0,250";
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                FlexSalesDetails.Editor.Text = lookup.m_values[0].ToString();
                SendKeys.Send("{Tab}");
            }
        }

        private void Load_ItemName_LookUp(string condition)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_table = "STOCKMST"+firmCode +"";
            lookup.m_con = cPublic.Connection;
            lookup.m_condition = condition;
            if (cPublic.LookupDefault == "Itemcode")
            {
                lookup.m_fields = "itemcode,itemname,unit,qty,rprofit1,taxper,cessper,groupcode";
                lookup.m_dispname = "Item Code, Item Name,Unit,Qty,Retail, Tax %, Cess %,Group";
                lookup.m_fldwidth = "80,250,100,100,60,60,60,100";
                lookup.m_order = "itemcode";
            }
            else
            {
                lookup.m_fields = "itemname,itemcode,unit,qty,rprofit1,taxper,cessper,groupcode";
                lookup.m_dispname = "Item Name,Item Code,Unit,Qty,Retail, Tax %, Cess %,Group";
                lookup.m_fldwidth = "250,80,100,100,60,60,60,100";
                lookup.m_order = "itemname";
            }
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                if (FlexSalesDetails.Col == FlexSalesDetails.Cols["itmCode"].Index)
                {
                    if (cPublic.LookupDefault == "Itemcode")
                    { FlexSalesDetails.Editor.Text = lookup.m_values[0].ToString(); }
                    else { FlexSalesDetails.Editor.Text = lookup.m_values[1].ToString(); }
                }
                else if (FlexSalesDetails.Col == FlexSalesDetails.Cols["itmName"].Index)
                {
                    if (cPublic.LookupDefault == "Itemcode")
                    {
                        FlexSalesDetails[FlexSalesDetails.Row, "itmCode"] = lookup.m_values[0].ToString();
                        FlexSalesDetails.Editor.Text = lookup.m_values[1].ToString();
                    }
                    else
                    {
                        FlexSalesDetails[FlexSalesDetails.Row, "itmCode"] = lookup.m_values[1].ToString();
                        FlexSalesDetails.Editor.Text = lookup.m_values[0].ToString();
                    }
                }
            }
            else
            {
                if (FlexSalesDetails.Col == FlexSalesDetails.Cols["itmName"].Index)
                { FlexSalesDetails.Editor.Text = string.Empty; }
            }
        }

        private Boolean fillRow(string itemCode, int row)
        {
            Price_Validation();
            string valBilltype = string.Empty;

            if (cmbPriceType.Text == "Retail") { valBilltype = "1"; }
            else if (cmbPriceType.Text == "Whole Sale") { valBilltype = "2"; }
            else if (cmbPriceType.Text == "Special") { valBilltype = "3"; }
            else { valBilltype = "4"; }

            string query = "select *," + Price_Name + " as rprofit from  Stockmst" + firmCode + "   where itemcode=@itemcode and itemname<>''  "
            + "";
            cmd.CommandText = query;
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@itemcode", itemCode);
            SqlDataAdapter dAPtR = new SqlDataAdapter(cmd);
            DataSet DAsET = new DataSet();
            DAsET.Clear();
            dAPtR.Fill(DAsET);
            if (DAsET.Tables[0].Rows.Count > 0)
            {
                if (!((FlexSalesDetails[FlexSalesDetails.Row, "itmCode"] + "").StartsWith("SL")))
                { FlexSalesDetails[row, "itmName"] = DAsET.Tables[0].Rows[0]["itemname"]; }
                if (cPublic.TaxIncludePrice == true)
                { FlexSalesDetails[row, "Rate"] = DAsET.Tables[0].Rows[0]["rprofit"]; }
                else { FlexSalesDetails[row, "Price"] = DAsET.Tables[0].Rows[0]["rprofit"]; }
                FlexSalesDetails[row, "taxPer"] = DAsET.Tables[0].Rows[0]["taxper"];
                FlexSalesDetails[row, "cessPer"] = DAsET.Tables[0].Rows[0]["cessper"];
                FlexSalesDetails[row, "Unit"] = DAsET.Tables[0].Rows[0]["unit"];
                FlexSalesDetails[row, "UnitValue"] = 1;
                if (cPublic.AutoQty == "YES")
                { FlexSalesDetails[row, "Qty"] = "1.000"; }
                else { FlexSalesDetails[row, "Qty"] = "0"; }
                FlexSalesDetails[row, "foc"] = "0";


                if (DAsET.Tables[0].Rows[0]["multiunit"] + "".ToUpper() == "Y" )
                { FlexSalesDetails.Cols["Unit"].AllowEditing = true; }
                else { FlexSalesDetails.Cols["Unit"].AllowEditing = false; }


                FlexSalesDetails[row, "multiunit"] = DAsET.Tables[0].Rows[0]["multiunit"];


                FlexSalesDetails[row, "DISCPER"] = "0.00";

              
            }
            else
            { MessageBox.Show("Item Not Found..", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information); return false; }

            return true;
        }

        private void Price_Validation()
        {
            Price_Name = "";
            switch (cmbPriceType.Text.Trim())
            {
                case "Retail":
                    Price_Name = "rprofit1";
                    break;
                case "Whole Sale":
                    Price_Name = "rprofit2";
                    break;
                case "Special":
                    Price_Name = "rprofit3";
                    break;
                case "":
                    Price_Name = "projectprice";
                    break;
            }
        }

        private Boolean TaxChk(string Value)
        {
            cmd.Parameters.Clear();
            Value = Convert.ToDecimal(Value).ToString("###0.00");
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM LOOKUP where code=@code and field1='TAXPER'";
            cmd.Parameters.AddWithValue("@code", Value);
            if (cmd.ExecuteScalar() != null)
            { return true; }
            else
            { return false; }
        }

        private Boolean CessChk(string Value)
        {
            cmd.Parameters.Clear();
            Value = Convert.ToDecimal(Value).ToString("###0.00");
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM LOOKUP where code=@code and field1='CESSPER'";
            cmd.Parameters.AddWithValue("@code", Value);
            if (cmd.ExecuteScalar() != null) { return true; }
            else { return false; }
        }

        private void load_Orderno()
        {
            switch (this.Tag.ToString())
            {
                case "SE":  //========== Sales Entry ==========\\ 
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = false;
                    break;
                case "SM":  //========== Sales Modify ==========\\ 
                case "SC":  //========== Sales Cancel ==========\\
                case "SP":  //========== Sales Print ==========\\
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = true;
                    break;

                case "NE":  //========== Salesreturn Entry ==========\\
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = false;
                    break;
                case "NM":  //========== Salesreturn Modify ==========\\
                case "NC":  //========== Salesreturn Cancel ==========\\
                case "NP":  //========== Salesreturn Print ==========\\
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = true;
                    break;

                case "QE":  //========== Quotation Entry ==========\\
                    mastertable1 = "qtn";
                    subtable1 = "qtnitem";
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = false;
                    break;
                case "QM":  //========== Quotation Modify ==========\\                    
                case "QC":  //========== Quotation Cancel ==========\\                  
                case "QP":  //========== Quotation Print ==========\\
                    mastertable1 = "qtn";
                    subtable1 = "qtnitem";
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = true;
                    break;

                case "OE":  //========== SalesOrder Entry ==========\\
                    mastertable1 = "sorder";
                    subtable1 = "soitem";
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = false;
                    break;
                case "OM":  //========== SalesOrder Modify ==========\\                   
                case "OC":  //========== SalesOrder Cancel ==========\\                   
                case "OP":  //========== SalesOrder Print ==========\\
                    mastertable1 = "sorder";
                    subtable1 = "soitem";
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = true;
                    break;

                case "DE":  //========== SalesOrder Entry ==========\\
                    mastertable1 = "damage";
                    subtable1 = "ditem";
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = false;
                    break;
                case "DM":  //========== SalesOrder Modify ==========\\                   
                case "DC":  //========== SalesOrder Cancel ==========\\                   
                case "DP":  //========== SalesOrder Print ==========\\
                    mastertable1 = "damage";
                    subtable1 = "ditem";
                    txtOrderno.Text = (ordernum()).ToString();
                    txtOrderno.Enabled = true;
                    break;
            }

            if (cmbFormType.Text == "A") { btnOtherDetails.Visible = false; }
            else { btnOtherDetails.Visible = true; }
        }

        private Boolean CheckMultiUnit(int row, string Itemcode)
        {
            decimal vrate = 0;
            string sql = "select RPROFIT1, rprofit2, rprofit3 from stockmst" + firmCode + "  where  " + Constants.vbCrLf
                 + " itemcode='" + gen.SQLFormat(Itemcode) + "' " + Constants.vbCrLf
                 + " ";
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            cmd.Parameters.Clear();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ds.Clear();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count <= 0)
            {
                MessageBox.Show("Unit-Id not found, Press F5 for Lookup !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {
                if (cmbPriceType.Text.ToUpper() == "RETAIL")
                {
                    vrate = Convert.ToDecimal(ds.Tables[0].Rows[0]["RPROFIT1"]);
                    if (cPublic.TaxIncludePrice == true)
                    { FlexSalesDetails[row, "Rate"] = vrate.ToString("#0.00"); }
                    else { FlexSalesDetails[row, "Price"] = vrate.ToString("#0.00"); }
                }
                else if (cmbPriceType.Text.ToUpper() == "WHOLE SALE")
                {
                    vrate = Convert.ToDecimal(ds.Tables[0].Rows[0]["RPROFIT2"]);
                    if (cPublic.TaxIncludePrice == true)
                    { FlexSalesDetails[row, "Rate"] = vrate.ToString("#0.00"); }
                    else { FlexSalesDetails[row, "Price"] = vrate.ToString("#0.00"); }
                }
                else if (cmbPriceType.Text.ToUpper() == "SPECIAL")
                {
                    vrate = Convert.ToDecimal(ds.Tables[0].Rows[0]["RPROFIT3"]);
                    if (cPublic.TaxIncludePrice == true)
                    { FlexSalesDetails[row, "Rate"] = vrate.ToString("#0.00"); }
                    else { FlexSalesDetails[row, "Price"] = vrate.ToString("#0.00"); }
                }
            }
            return true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void Load_Screen()
        {
            FormProperty();
            grid_initialize(FlexSalesDetails);
            refresh();
            defaultvalsel();
            switch (this.Tag.ToString())
            {
                case "SE":  //========== Sales Entry ==========\\                   
                    mastertable1 = "SALES";
                    subtable1 = "SITEM";
                    SP_INSERT_TABLE = "SP_INSERT_SALES";
                    SP_INSERT_SUBTABLE = "SP_INSERT_SITEM";
                    //btnSave.Text = "&Save";
                    lblsorder.Visible = false;
                    txtFromBill.Visible = false;
                    break;
                case "SM":  //========== Sales Modify ==========\\
                    mastertable1 = "SALES";
                    subtable1 = "SITEM";
                    SP_UPDATE_TABLE = "SP_UPDATE_SALES";
                    SP_CANCEL_TABLE = "SP_CANCEL_SALES";
                    SP_DELETE_SUBTABLE = "SP_DELETE_SITEM";
                    SP_INSERT_TABLE = "SP_INSERT_SALES";
                    SP_INSERT_SUBTABLE = "SP_INSERT_SITEM";
                    //btnSave.Text = "&Update";
                    CtrlsEnbl(false);
                    break;
                case "SC":  //========= Sales Cancel ==========\\
                    mastertable1 = "SALES";
                    subtable1 = "SITEM";
                    SP_CANCEL_TABLE = "SP_CANCEL_SALES";
                    SP_DELETE_SUBTABLE = "SP_DELETE_SITEM";
                    //btnSave.Text = "&Cancel";
                    CtrlsEnbl(false);
                    break;
                case "SP":  //========= Sales Print ==========\\
                    mastertable1 = "SALES";
                    subtable1 = "SITEM";
                    CtrlsEnbl(false);
                    break;
                case "NE":  //========= Salesreturn Entry ==========\\
                    mastertable1 = "sreturn";
                    subtable1 = "sritem";
                    SP_INSERT_TABLE = "SP_INSERT_SRETURN";
                    SP_INSERT_SUBTABLE = "SP_INSERT_SRITEM";
                    //btnSave.Text = "&Save";
                    txtRoundoff.Enabled = false;
                     lblsorder.Visible = true;
                     txtFromBill.Visible = true;
                    break;
                case "NM":  //========= Salesreturn Modify ==========\\
                    mastertable1 = "sreturn";
                    subtable1 = "sritem";
                    SP_UPDATE_TABLE = "SP_UPDATE_SRETURN";
                    SP_INSERT_SUBTABLE = "SP_INSERT_SRITEM";
                    SP_DELETE_SUBTABLE = "SP_DELETE_SRITEM";
                    SP_CANCEL_TABLE = "SP_CANCEL_SRETURN";
                    SP_INSERT_TABLE = "SP_INSERT_SRETURN";
                    //btnSave.Text = "&Update";
                    CtrlsEnbl(false);
                    break;
                case "NC":  //========= Salesreturn Cancel ==========\\
                    mastertable1 = "sreturn";
                    subtable1 = "sritem";
                    SP_CANCEL_TABLE = "SP_CANCEL_SRETURN";
                    SP_DELETE_SUBTABLE = "SP_DELETE_SRITEM";
                    //btnSave.Text = "&Cancel";
                    CtrlsEnbl(false);
                    break;
                case "NP":  //========= Salesreturn Print ==========\\
                    mastertable1 = "sreturn";
                    subtable1 = "sritem";
                    CtrlsEnbl(false);
                    break;

                case "QE":  //========= Quotation Entry ==========\\
                    mastertable1 = "qtn";
                    subtable1 = "qtnitem";
                    SP_INSERT_TABLE = "SP_INSERT_QTN";
                    SP_INSERT_SUBTABLE = "SP_INSERT_QTNITEM";
                    //btnSave.Text = "&Save";
                    lblQty1.Visible = false;
                    lblcurrentQty.Visible = false;
                    break;
                case "QM":  //========= Quotation Modify ==========\\
                    mastertable1 = "qtn";
                    subtable1 = "qtnitem";
                    SP_UPDATE_TABLE = "SP_UPDATE_QTN";
                    SP_DELETE_SUBTABLE = "SP_DELETE_QTNITEM";
                    SP_INSERT_SUBTABLE = "SP_INSERT_QTNITEM";
                    //btnSave.Text = "&Update";
                    lblQty1.Visible = false;
                    lblcurrentQty.Visible = false;

                    CtrlsEnbl(false);
                    break;
                case "QC":  //========= Quotation Cancel ==========\\
                    mastertable1 = "qtn";
                    subtable1 = "qtnitem";
                    SP_CANCEL_TABLE = "SP_CANCEL_QTN";
                    SP_DELETE_SUBTABLE = "SP_DELETE_QTNITEM";
                    //btnSave.Text = "&Cancel";
                    lblQty1.Visible = false;
                    lblcurrentQty.Visible = false;
                    CtrlsEnbl(false);
                    break;
                case "QP":  //========= Quotation Print ==========\\
                    mastertable1 = "qtn";
                    subtable1 = "qtnitem";
                    CtrlsEnbl(false);
                    break;
                case "OE":  //========= SalesOrder Entry ==========\\
                    mastertable1 = "sorder";
                    subtable1 = "soitem";
                    SP_INSERT_TABLE = "SP_INSERT_SORDER";
                    SP_INSERT_SUBTABLE = "SP_INSERT_SOITEM";
                    //btnSave.Text = "&Save";
                    break;
                case "OM":  //========= SalesOrder Modify ==========\\
                    mastertable1 = "sorder";
                    subtable1 = "soitem";
                    SP_UPDATE_TABLE = "SP_UPDATE_SORDER";
                    SP_DELETE_SUBTABLE = "SP_DELETE_SOITEM";
                    SP_INSERT_SUBTABLE = "SP_INSERT_SOITEM";
                    //btnSave.Text = "&Update";
                    CtrlsEnbl(false);
                    break;
                case "OC":  //========= SalesOrder Cancel ==========\\
                    mastertable1 = "sorder";
                    subtable1 = "soitem";
                    SP_CANCEL_TABLE = "SP_CANCEL_SORDER";
                    SP_DELETE_SUBTABLE = "SP_DELETE_SOITEM";
                    //btnSave.Text = "&Cancel";
                    CtrlsEnbl(false);
                    break;
                case "OP":  //========= SalesOrder Print ==========\\
                    mastertable1 = "sorder";
                    subtable1 = "soitem";
                    CtrlsEnbl(false);
                    break;

                case "DE":  //========= SalesOrder Entry ==========\\
                    mastertable1 = "damage";
                    subtable1 = "ditem";
                    SP_INSERT_TABLE = "SP_INSERT_DAMAGE";
                    SP_INSERT_SUBTABLE = "SP_INSERT_DITEM";
                    //btnSave.Text = "&Save";
                    break;
                case "DM":  //========= SalesOrder Modify ==========\\
                    mastertable1 = "damage";
                    subtable1 = "ditem";
                    SP_UPDATE_TABLE = "SP_UPDATE_DAMAGE";
                    SP_DELETE_SUBTABLE = "SP_DELETE_DITEM";
                    SP_INSERT_SUBTABLE = "SP_INSERT_DITEM";
                    //btnSave.Text = "&Update";
                    CtrlsEnbl(false);
                    break;
                case "DC":  //========= SalesOrder Cancel ==========\\
                    mastertable1 = "damage";
                    subtable1 = "ditem";
                    SP_CANCEL_TABLE = "SP_CANCEL_DAMAGE";
                    SP_DELETE_SUBTABLE = "SP_DELETE_DITEM";
                    //btnSave.Text = "&Cancel";
                    CtrlsEnbl(false);
                    break;

                case "TE":  //========== TRANSFER Entry ==========\\                   
                    mastertable1 = "TRANSFER";
                    subtable1 = "TRITEM";
                    SP_INSERT_TABLE = "SP_INSERT_TRANSFER";
                    SP_INSERT_SUBTABLE = "SP_INSERT_TRITEM";
                    //btnSave.Text = "&Save";
                    lblsorder.Visible = true;
                    txtFromBill.Visible = true;
                    break;
                case "TM":  //========== TRANSFER Modify ==========\\
                    mastertable1 = "TRANSFER";
                    subtable1 = "TRITEM";
                    SP_UPDATE_TABLE = "SP_UPDATE_TRANSFER";
                    SP_CANCEL_TABLE = "SP_CANCEL_TRANSFER";
                    SP_DELETE_SUBTABLE = "SP_DELETE_TRITEM";
                    SP_INSERT_TABLE = "SP_INSERT_TRANSFER";
                    SP_INSERT_SUBTABLE = "SP_INSERT_TRITEM";
                    //btnSave.Text = "&Update";
                    CtrlsEnbl(false);
                    break;
                case "TC":  //========= TRANSFER Cancel ==========\\
                    mastertable1 = "TRANSFER";
                    subtable1 = "TRITEM";
                    SP_CANCEL_TABLE = "SP_CANCEL_TRANSFER";
                    SP_DELETE_SUBTABLE = "SP_DELETE_TRITEM";
                    //btnSave.Text = "&Cancel";
                    CtrlsEnbl(false);
                    break;
                case "TP":  //========= TRANSFER Print ==========\\
                    mastertable1 = "TRANSFER";
                    subtable1 = "TRITEM";
                    CtrlsEnbl(false);
                    break;

            }

            load_Orderno();
            
        }

        private void txtOrderno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (txtOrderno.Text.Trim() != string.Empty)
                {
                    txtOrderno.Enabled = false;
                    txtRoundoff.Enabled = false;
                    CtrlsEnbl(true);
                    fill_Datas(Convert.ToInt32(txtOrderno.Text.Trim()), "NORMAL", true);
                }
                else
                { txtOrderno.Focus(); }

                if ((this.Tag.ToString().Substring(1, 1) == "C") || (this.Tag.ToString().Substring(1, 1) == "P"))
                {
                    if (saveFlg)
                    { btnSave_Click(sender, e); }
                }
            }
            else
            {
                txtBox = ((TextBox)sender);

                TextBox tx = new TextBox();

                tx.Text = txtBox.Text;

                gen.ValidDecimalNumber(e, ref tx, 6, 0);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            PS = string.Empty;
            PS = "SAVE";
            Bill_Save();

            switch (this.Tag.ToString().Substring(1, 1))
            {
                case "E":
                case "M":
                case "C":
                case "P":
                    if (saveFlg)
                    {
                        SetFlag();
                        refresh();
                    }
                    break;
            }
        }

        private void txtSplDiscAmt_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtBox = ((TextBox)sender);

            TextBox tx = new TextBox();

            tx.Text = txtBox.Text;

            gen.ValidDecimalNumber(e, ref tx, 12, 2);
        }

        private void txtSplDiscAmt_Leave(object sender, EventArgs e)
        {
            txtBox = ((TextBox)sender);
            if (Information.IsNumeric(txtBox.Text) == false) { txtBox.Text = "0.00"; }
            if (txtBox.Text.Trim() != string.Empty)
            {
                txtBox.Text = Strings.Format(Convert.ToDecimal(txtBox.Text.Trim()), "######0.00");
                for (int i = 1; i <= counterflag; i++)
                { CalcLine(i, FlexSalesDetails); }
                Calc_Bill(FlexSalesDetails);
            }
            else
            {
                txtBox.Focus();
                txtBox.Text = "0.00";
                txtBox.SelectAll();
            }
        }

       

        private void FlexSalesDetails_BeforeRowColChange(object sender, RangeEventArgs e)
        {
            CalcSpec();
        }

        private void FlexSalesDetails_Click(object sender, EventArgs e)
        {
            SendKeys.Send("{F2}");
        }

        private void FlexSalesDetails_Enter(object sender, EventArgs e)
        {
            SendKeys.Send("{F2}");
        }

        private void FlexSalesDetails_EnterCell(object sender, EventArgs e)
        {
            FlexSalesDetails.Styles.Focus.BackColor = System.Drawing.Color.MistyRose;
            if (FlexSalesDetails.Row < 1) { return; }
            if (FlexSalesDetails[FlexSalesDetails.Row - 1, "itmName"] + "".ToString().Trim() == string.Empty)
            { FlexSalesDetails.Row -= 1; return; }            

            if ((FlexSalesDetails[FlexSalesDetails.Row, "itmCode"] + "").Trim() == string.Empty &&
                (FlexSalesDetails[FlexSalesDetails.Row, "itmName"] + "").Trim() == string.Empty &&
                FlexSalesDetails.Col > FlexSalesDetails.Cols["itmName"].Index)
            { FlexSalesDetails.Col = FlexSalesDetails.Cols["itmCode"].Index; }

            if (FlexSalesDetails.Col == FlexSalesDetails.Cols["itmName"].Index && FlexSalesDetails[FlexSalesDetails.Row, "itmCode"] + "".ToString() != string.Empty)
            {
                if (!(FlexSalesDetails[FlexSalesDetails.Row, FlexSalesDetails.Cols["itmCode"].Index]).ToString().StartsWith("SL"))
                { SendKeys.Send("{Tab}"); }
            }    

            if (cPublic.TaxIncludePrice ==true )
            {
                if (FlexSalesDetails.ColSel == FlexSalesDetails.Cols["Rate"].Index)
                {
                    if (FlexSalesDetails[FlexSalesDetails.Row, "itmCode"] + "" != "")
                    {
                        if (!Information.IsNumeric(FlexSalesDetails[FlexSalesDetails.Row, "Rate"] + ""))
                        { FlexSalesDetails[FlexSalesDetails.Row, "Rate"] = "0"; }
                    }
                }
            }
            else
            {
                if (FlexSalesDetails.ColSel == FlexSalesDetails.Cols["Price"].Index)
                {
                    if (FlexSalesDetails[FlexSalesDetails.Row, "itmCode"] + "" != "")
                    {
                        if (!Information.IsNumeric(FlexSalesDetails[FlexSalesDetails.Row, "Price"] + ""))
                        { FlexSalesDetails[FlexSalesDetails.Row, "Price"] = "0"; }
                    }
                }
            }

            if (FlexSalesDetails.ColSel == FlexSalesDetails.Cols["Unit"].Index)
            {
                if (FlexSalesDetails[FlexSalesDetails.Row, "multiunit"] + "".ToUpper() == "Y" )
                { FlexSalesDetails.Cols["Unit"].AllowEditing = true; }
                else
                { FlexSalesDetails.Cols["Unit"].AllowEditing = false; }
            }
            //if (FlexSalesDetails.ColSel == FlexSalesDetails.Cols["taxPer"].Index)
            //{
            //    FlexSalesDetails.Cols["taxPer"].AllowEditing = true;
            //}
            //if (FlexSalesDetails.ColSel == FlexSalesDetails.Cols["cessPer"].Index)
            //{
            //    FlexSalesDetails.Cols["cessPer"].AllowEditing = true;
            //}

            if (FlexSalesDetails.Cols[FlexSalesDetails.Col].AllowEditing == false) { SendKeys.Send("{Tab}"); }
            SendKeys.Send("{F2}");
        }

        private void FlexSalesDetails_KeyDownEdit(object sender, KeyEditEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                if (FlexSalesDetails.Cols["itmCode"].Index == e.Col) { load_Item_Lookup(e.Row); }
                else if (FlexSalesDetails.Cols["cessPer"].Index == e.Col) { load_Cess_Lookup(e.Row); }
                else if (FlexSalesDetails.Cols["itmName"].Index == e.Col) { Load_ItemName_LookUp(string.Empty); SendKeys.Send("{Tab}"); }
                else if (FlexSalesDetails.Cols["Unit"].Index == e.Col) { load_Multi_Lookup(e.Row, FlexSalesDetails[e.Row, "itmCode"] + ""); }
            }
            else if ((e.Shift) && e.KeyCode == Keys.Tab)
            {
                if ((FlexSalesDetails.Col == FlexSalesDetails.Cols["itmCode"].Index) && (FlexSalesDetails.Row == 1))
                { txtRemarks.Focus(); }
            }
            if (e.KeyCode == Keys.Enter && FlexSalesDetails.Cols["itmName"].Index == e.Col) { e.Handled = true; }

            if (FlexSalesDetails.Cols["itmCode"].Index == e.Col && (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab))
                if (FlexSalesDetails.Editor != null)
                    if (FlexSalesDetails.Editor.Text == "0")
                    { e.Handled = true;  btnSavePrint.Focus();  }
                    else if (FlexSalesDetails.Editor.Text == "00")
                    { e.Handled = true;  btnSave.Focus(); }
                    else { e.Handled = false; }


            if (e.Control == true && e.KeyCode == Keys.G)
            { FlexSalesDetails.Cols["godown"].Visible = true; }
            if (cPublic.TaxIncludePrice ==true )
            {
                if (FlexSalesDetails.Col == FlexSalesDetails.Cols["Rate"].Index && e.KeyCode == Keys.Enter)
                { SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}"); SendKeys.Send("{TAB}"); }
            }
            else
            {
                if (FlexSalesDetails.Col == FlexSalesDetails.Cols["Price"].Index && e.KeyCode == Keys.Enter)
                { SendKeys.Send("{TAB}"); }
            }

            if (e.KeyCode == Keys.F10)
            {
                e.Handled = true; btnSave.Focus();
            }
        }

        private void FlexSalesDetails_KeyPressEdit(object sender, KeyPressEditEventArgs e)
        {
            C1FlexGrid FlexGrd = (C1FlexGrid)sender;
            if (FlexGrd.Editor == null) { return; }
            if (e.KeyChar != Convert.ToChar(Keys.Enter) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                if (e.Col == FlexSalesDetails.Cols["Qty"].Index)  //=========== NUMERIC NUMBER VALIDATION FOR "Qty" COLUMN ==============\\
                { gen.ValidFlexNumber(e, ref FlexGrd, 6, 3); }

                if (e.Col == FlexSalesDetails.Cols["foc"].Index)  //=========== NUMERIC NUMBER VALIDATION FOR "foc" COLUMN ==============\\
                { gen.ValidFlexNumber(e, ref FlexGrd, 6, 3); }
             
                if (e.Col == FlexSalesDetails.Cols["Rate"].Index)  //=========== NUMERIC NUMBER VALIDATION FOR "Rate" COLUMN ==============\\
                { gen.ValidFlexNumber(e, ref FlexGrd, 7, 2); }

                if (e.Col == FlexSalesDetails.Cols["Price"].Index)  //=========== NUMERIC NUMBER VALIDATION FOR "Rate" COLUMN ==============\\
                { gen.ValidFlexNumber(e, ref FlexGrd, 7, 2); }

                if (e.Col == FlexSalesDetails.Cols["cessPer"].Index)  //=========== NUMERIC NUMBER VALIDATION FOR "cessPer" COLUMN ==============\\
                { gen.ValidFlexNumber(e, ref FlexGrd, 3, 2); }
            }
        }

        private void FlexSalesDetails_SetupEditor(object sender, RowColEventArgs e)
        {
            TextBox tbs = (TextBox)FlexSalesDetails.Editor;
            if (FlexSalesDetails.Cols["itmCode"].Index == e.Col)
            { tbs.CharacterCasing = CharacterCasing.Upper; }
            if (FlexSalesDetails.Cols["itmName"].Index == e.Col)
            { tbs.MaxLength = 40; }
            else { tbs.CharacterCasing = CharacterCasing.Normal; }
        }

        private void FlexSalesDetails_ValidateEdit(object sender, ValidateEditEventArgs e)
        {
            if (FlexSalesDetails.Rows.Count - 4 < e.Row)
            { FlexSalesDetails.Rows.Count += 1; }
            if (!cgen.isFormClosing() )
            {
                if (FlexSalesDetails.Cols["itmCode"].Index == e.Col)
                {
                    if (FlexSalesDetails.Editor != null && FlexSalesDetails.Editor.Text.Trim() != string.Empty)
                    {
                        FlexSalesDetails.Editor.Text = FlexSalesDetails.Editor.Text.Trim().ToUpper();

                        if (FlexSalesDetails[e.Row, "itmCode"] + "".ToString().Trim() != FlexSalesDetails.Editor.Text.Trim())
                        {
                            if (fillRow(FlexSalesDetails.Editor.Text.Trim(), e.Row))
                            {
                                e.Cancel = false;

                                if (cPublic.TaxIncludePrice == true)
                                {
                                    if (!Information.IsNumeric(FlexSalesDetails[e.Row, "Rate"] + "".ToString().Trim()))
                                    { FlexSalesDetails[e.Row, "Rate"] = 0; }
                                }
                                else
                                {
                                    if (!Information.IsNumeric(FlexSalesDetails[e.Row, "Price"] + "".ToString().Trim()))
                                    { FlexSalesDetails[e.Row, "Price"] = 0; }
                                }
                            }
                            else
                            { e.Cancel = true; ((TextBox)FlexSalesDetails.Editor).SelectAll(); }

                        }
                    }
                    else
                    {
                        if (FlexSalesDetails[e.Row, "itmName"] + "".ToString().Trim() != string.Empty)
                        {
                            FlexSalesDetails.Rows.Remove(e.Row);
                            FlexSalesDetails.Editor.Text = FlexSalesDetails[e.Row, "itmCode"] + "".ToString();
                            if (FlexSalesDetails.Rows.Count - 4 < 30)
                            { FlexSalesDetails.Rows.Count += 1; }

                            e.Cancel = true;

                            ((TextBox)FlexSalesDetails.Editor).SelectAll();
                        }
                    }
                }
                else if (FlexSalesDetails.Cols["Unit"].Index == e.Col)
                {
                    FlexSalesDetails.Editor.Text = FlexSalesDetails.Editor.Text.Trim().ToUpper();
                    if ((FlexSalesDetails[e.Row, "Unit"] + "").Trim().ToUpper() != FlexSalesDetails.Editor.Text.Trim().ToUpper())
                    {
                        if ((!CheckMultiUnit(FlexSalesDetails.Editor.Text.Trim(), e.Row, FlexSalesDetails[e.Row, "itmCode"] + "")) || FlexSalesDetails.Editor.Text.Trim() == "")
                        { e.Cancel = true; ((TextBox)FlexSalesDetails.Editor).SelectAll(); }
                        else { e.Cancel = false; }
                    }
                }
                else if (FlexSalesDetails.Cols["DISCPER"].Index == e.Col)
                {
                    if ((FlexSalesDetails[e.Row, "DISCPER"] + "").Trim().ToUpper() != FlexSalesDetails.Editor.Text.Trim().ToUpper())
                    {
                        if (!Information.IsNumeric(FlexSalesDetails.Editor.Text))
                        { FlexSalesDetails.Editor.Text = "0"; }
                        else { e.Cancel = false; }
                    }
                }
                else if (FlexSalesDetails.Cols["Qty"].Index == e.Col)
                {
                        if (!Information.IsNumeric(FlexSalesDetails.Editor.Text))
                        { FlexSalesDetails.Editor.Text = "0"; }
                        if (this.Tag.ToString().Substring(0, 1) != "N")
                        {
                            if (Convert.ToDecimal(FlexSalesDetails.Editor.Text) == 0)
                            { { e.Cancel = true; ((TextBox)FlexSalesDetails.Editor).SelectAll(); } }
                            else { e.Cancel = false; }
                        }                   
                }
                else if (FlexSalesDetails.Cols["foc"].Index == e.Col)
                {
                        if (!Information.IsNumeric(FlexSalesDetails.Editor.Text))
                        { FlexSalesDetails.Editor.Text = "0"; }
                        //if (this.Tag.ToString().Substring(0, 1) != "N")
                        //{
                        //    if (Convert.ToDecimal(FlexSalesDetails.Editor.Text) == 0)
                        //    { { e.Cancel = true; ((TextBox)FlexSalesDetails.Editor).SelectAll(); } }
                        //    else { e.Cancel = false; }
                        //}                   
                }
                else if (FlexSalesDetails.Cols["Rate"].Index == e.Col && cPublic.TaxIncludePrice==true )
                {
                    if (!Information.IsNumeric(FlexSalesDetails.Editor.Text))
                    { FlexSalesDetails.Editor.Text = "0"; }
                    if (Convert.ToDecimal(FlexSalesDetails.Editor.Text + "") == 0)
                    { e.Cancel = true; ((TextBox)FlexSalesDetails.Editor).SelectAll(); }
                    else { e.Cancel = false; }
                }
                else if (FlexSalesDetails.Cols["Price"].Index == e.Col && cPublic.TaxIncludePrice == false )
                {
                    if (!Information.IsNumeric(FlexSalesDetails.Editor.Text))
                    { FlexSalesDetails.Editor.Text = "0"; }
                    if (Convert.ToDecimal(FlexSalesDetails.Editor.Text + "") == 0)
                    { e.Cancel = true; ((TextBox)FlexSalesDetails.Editor).SelectAll(); }
                    else { e.Cancel = false; }
                }               
                else if (FlexSalesDetails.Cols["taxPer"].Index == e.Col)
                {
                    if (!TaxChk(FlexSalesDetails.Editor.Text.Trim()))
                    {
                        MessageBox.Show(" Tax % Not  Found \n Press F5 For Lookup", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true; ((TextBox)FlexSalesDetails.Editor).SelectAll();
                    }
                }
                else if (FlexSalesDetails.Cols["cessPer"].Index == e.Col)
                {
                    if (!CessChk(FlexSalesDetails.Editor.Text.Trim()))
                    {
                        MessageBox.Show(" Cess % Not  Found \n Press F5 For Lookup", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true; ((TextBox)FlexSalesDetails.Editor).SelectAll();
                    }
                }
                else if (FlexSalesDetails.Cols["itmName"].Index == e.Col)
                {
                    if ((FlexSalesDetails[e.Row, "itmCode"] + "" != "") && FlexSalesDetails.Editor != null && FlexSalesDetails.Editor.Text.Trim() == string.Empty)
                    { e.Cancel = true; }
                    else
                    { e.Cancel = false; }
                }
               
            }
            gridindex(FlexSalesDetails);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SqlCommand sel = new SqlCommand();
            string valBilltyp = string.Empty;
            Price_Validation();
            counterflag = 0;
            gridindex(FlexSalesDetails);

            if (cmbPriceType.Text == "Retail") { valBilltyp = "1"; }
            else if (cmbPriceType.Text == "Whole Sale") { valBilltyp = "2"; }
            else if (cmbPriceType.Text == "Special") { valBilltyp = "3"; }
            else { valBilltyp = "4"; }

            for (int i = 1; i <= counterflag; i++)
            {
                if ((FlexSalesDetails[i, "itmCode"] != null) && (FlexSalesDetails[i, "itmCode"].ToString() != ""))
                {
                    CheckMultiUnit( i, FlexSalesDetails[i, "itmCode"] + "");
                    CalcLine(i, FlexSalesDetails);
                }

                Calc_Bill(FlexSalesDetails);

                btnUpdate.Visible = false;
            }
        }

        private void cmbPriceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (FlexSalesDetails[1, "itmName"] + "" != string.Empty)
                btnUpdate.Visible = true;
        }

        private void txtCustomer_Enter(object sender, EventArgs e)
        {
            if (cmbBillType.Text.Trim() == "CREDIT")
                if (txtCustomer.Text == string.Empty)
                {
                    txtCustomer.ForeColor = Color.Gray;
                    txtCustomer.Text = "Press (F5 or +) keys for LookUp";
                    txtCustomer.SelectAll();
                }
                else
                { txtCustomer.ForeColor = Color.Black; }
        }

        private void txtCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { Cust_LookUp(string.Empty); }
        }

        private void txtCustomer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (cmbBillType.SelectedItem.ToString().Trim().Equals("CREDIT"))
            {
                if ((e.KeyChar.Equals('\b')))
                {
                    e.Handled = true;
                    txtCustomer.Text = string.Empty;
                    Cust_LookUp(txtCustomer.Text.Trim());
                }

                if (e.KeyChar.Equals('+'))
                {
                    e.Handled = true;
                    txtCustomer.Text = string.Empty;
                    Cust_LookUp("");
                    return;
                }
                if (!(e.KeyChar.Equals('\b')) && (!(e.KeyChar.Equals('\r'))))
                {
                    e.Handled = true;
                    txtCustomer.Text = string.Empty;
                    Cust_LookUp(txtCustomer.Text.Trim() + e.KeyChar.ToString());
                }
            }
            else
            {
                if (txtCustomer.ForeColor == Color.Gray)
                { txtCustomer.ForeColor = Color.Black; txtCustomer.Text = string.Empty; }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Tag = this.Tag.ToString().Substring(0, 1) + "C";
            btnSave_Click(sender, e);
        }

        private void btnSerach_Click(object sender, EventArgs e)
        {
            this.Tag = this.Tag.ToString().Substring(0, 1) + "M";
            Load_Screen();
            txtOrderno.Focus();
        }

        private void ucSales_Load(object sender, EventArgs e)
        {
            Load_Screen();

            ToolTip toolTip1 = new ToolTip();
            // Set up the delays for the ToolTip.
            toolTip1.AutoPopDelay = 1000;
            toolTip1.InitialDelay = 500;
            toolTip1.ReshowDelay = 500;
            // Force the ToolTip text to be displayed whether or not the form is active.
            toolTip1.ShowAlways = true;

            // Set up the ToolTip text for the Button and Checkbox.
            toolTip1.SetToolTip(this.btnSerach, "Search");
            toolTip1.SetToolTip(this.btnNew, "New");
            toolTip1.SetToolTip(this.btnSave, "Save");
            toolTip1.SetToolTip(this.btnCancel, "Cancel");
            toolTip1.SetToolTip(this.btnExit, "Exit");
            toolTip1.SetToolTip(this.btnSavePrint, "Save&Print");
            toolTip1.SetToolTip(this.btnPrint, "Print");
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            this.Tag = this.Tag.ToString().Substring(0, 1) + "P";
            PS = "PRINT";
            Bill_Save();
            switch (this.Tag.ToString().Substring(1, 1))
            {
                case "E":
                case "M":
                case "C":
                case "P":
                    if (saveFlg)
                        SetFlag();
                        refresh();
                    break;
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.Tag = this.Tag.ToString().Substring(0, 1) + "E";
            Load_Screen();
        }

        private void btnSavePrint_Click(object sender, EventArgs e)
        {
            PS = "PRINT";
            Bill_Save();
            switch (this.Tag.ToString().Substring(1, 1))
            {
                case "E":
                case "M":
                case "C":
                case "P":
                    if (saveFlg)
                        SetFlag();
                        refresh();
                    break;
            }
        }

        private void SetFlag()
        {
            this.Tag = this.Tag.ToString().Substring(0, 1) + "E"; 
        }

        private void txtSalesPerson_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { LoadLookup(txtSalesPerson,"SALES PERSON",string.Empty); }
        }

        private void txtSalesPerson_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar.Equals('\b')))
            {
                e.Handled = true;
                txtSalesPerson.Text = string.Empty;
                LoadLookup(txtSalesPerson, "SALES PERSON", txtSalesPerson.Text.Trim());
            }

            if (e.KeyChar.Equals('+'))
            {
                e.Handled = true;
                txtSalesPerson.Text = string.Empty;
                LoadLookup(txtSalesPerson, "SALES PERSON", "");
                return;
            }
            if (!(e.KeyChar.Equals('\b')) && (!(e.KeyChar.Equals('\r'))))
            {
                e.Handled = true;
                txtSalesPerson.Text = string.Empty;
                LoadLookup(txtSalesPerson,"SALES PERSON", txtSalesPerson.Text.Trim() + e.KeyChar.ToString());
            }
        }

        private void txtSalesPerson_Enter(object sender, EventArgs e)
        {
            if (txtSalesPerson.Text == string.Empty)
            {
                txtSalesPerson.ForeColor = Color.Gray;
                txtSalesPerson.Text = "Press (F5 or +) keys for LookUp";
                txtSalesPerson.SelectAll();
            }
            else
            { txtSalesPerson.ForeColor = Color.Black; }
        }

        private void LoadLookup(TextBox txtBx, string option, string start)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;

            lookup.m_condition = "Field1 = '" + option + "'";
            lookup.m_table = "LOOKUP";
            lookup.m_fields = "Code,Details";
            lookup.m_dispname = "Code,Details";
            lookup.m_fldwidth = "75,150";
            lookup.m_strat = start;
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                txtBx.Text = lookup.m_values[0].ToString();
                txtBx.Tag = lookup.m_values[0].ToString();

                SendKeys.Send("{TAB}");
            }
        }

        # region CheckMultiUnit
        private Boolean CheckMultiUnit(string multiUnit, Int32 row, string Itemcode)
        {
            string sql = "SELECT * FROM (select unit,1 value, RPROFIT1, RPROFIT3 from stockmst" + firmCode + "  where  " + Constants.vbCrLf
                 + " itemcode='" + gen.SQLFormat(Itemcode) + "' " + Constants.vbCrLf
                 + " union all  select unit,value,RPROFIT1,RPROFIT3 from multiunit" + firmCode + " where "
                 + " itemcode='" + gen.SQLFormat(Itemcode) + "' ) Sn where unit='" + gen.SQLFormat(multiUnit) + "'";
            SqlCommand sel = new SqlCommand(sql, cPublic.Connection);
            SqlDataAdapter da = new SqlDataAdapter(sel);
            DataSet ds = new DataSet();
            ds.Clear();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count <= 0)
            {
                MessageBox.Show("Unit-Id not found, Press F5 for Lookup !", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else
            {
                FlexSalesDetails[row, "Unit"] = ds.Tables[0].Rows[0]["UNIT"] + "";
                FlexSalesDetails[row, "UnitValue"] = ds.Tables[0].Rows[0]["VALUE"] + "";

                if (cmbPriceType.Text.ToUpper() == "RETAIL")
                {
                    FlexSalesDetails[row, "Rate"] = Convert.ToDecimal(ds.Tables[0].Rows[0]["RPROFIT1"]).ToString("#0.00");
                }
                else if (cmbPriceType.Text.ToUpper() == "WHOLE SALE")
                {
                    FlexSalesDetails[row, "Rate"] = Convert.ToDecimal(ds.Tables[0].Rows[0]["RPROFIT3"]).ToString("#0.00");
                }
                else
                {
                    FlexSalesDetails[row, "Rate"] = Convert.ToDecimal(ds.Tables[0].Rows[0]["RPROFIT1"]).ToString("#0.00");
                }

            }
            return true;
        }
        # endregion


        # region load_Multi_Lookup(int row)
        // FUNCTION FOR LOADING THE LOOKUP AND FILL CORRESPONDING DATA
        private void load_Multi_Lookup(int row, string itemcode)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;
            lookup.m_caption = "Unit Details";
            lookup.m_table = "(select unit,1 value, RPROFIT1, RPROFIT3 from stockmst" + firmCode + "  where  " + Constants.vbCrLf
                + " itemcode='" + gen.SQLFormat(itemcode) + "' " + Constants.vbCrLf
                + " union all  select unit,value,RPROFIT1,RPROFIT3 from multiunit" + firmCode + " where "
                + " itemcode='" + gen.SQLFormat(itemcode) + "' ) Sn ";

            lookup.m_fields = "UNIT,VALUE,RPROFIT1,RPROFIT3";
            lookup.m_dispname = "Unit Details, Value, Retail, Whole-Sale ";
            lookup.m_fldwidth = "150,120,120,120,120,120";
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                FlexSalesDetails.Editor.Text = lookup.m_values[0].ToString();
                FlexSalesDetails[row, "Unit"] = lookup.m_values[0].ToString();
                FlexSalesDetails[row, "UnitValue"] = lookup.m_values[1].ToString();

                if (cmbPriceType.Text.ToUpper() == "WHOLE SALE")
                { FlexSalesDetails[row, "Rate"] = lookup.m_values[3].ToString(); }

                FlexSalesDetails[row, "Rate"] = lookup.m_values[2].ToString(); 
                SendKeys.Send("{Tab}");
            }
        }
        # endregion

        private void cmbFormType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbFormType.Text  == "A")
            { btnOtherDetails.Visible = false; }
            else { btnOtherDetails.Visible = true; }
        }

        private void btnOtherDetails_Click(object sender, EventArgs e)
        {
            frmOtherDetails otherdetails = new frmOtherDetails(customerbo);
            otherdetails.ShowDialog();
        }

        private void lblAmount_Click(object sender, EventArgs e)
        {

        }

        private void btnSerach_MouseHover(object sender, EventArgs e)
        {
            btnSerach.BackgroundImage = global::LotzInventory.Properties.Resources.search_ovr;
        }

        private void btnSerach_MouseLeave(object sender, EventArgs e)
        {
            btnSerach.BackgroundImage = global::LotzInventory.Properties.Resources.search;
        }


        private void btnNew_MouseHover(object sender, EventArgs e)
        {
            btnNew.BackgroundImage = global::LotzInventory.Properties.Resources.new_ovr;
        }

        private void btnNew_MouseLeave(object sender, EventArgs e)
        {
            btnNew.BackgroundImage = global::LotzInventory.Properties.Resources.new2;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            btnSave.BackgroundImage = global::LotzInventory.Properties.Resources.save_ovr;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            btnSave.BackgroundImage = global::LotzInventory.Properties.Resources.save1;
        }

        private void btnSavePrint_MouseHover(object sender, EventArgs e)
        {
            btnSavePrint.BackgroundImage = global::LotzInventory.Properties.Resources.saveandprint_ovr;
        }

        private void btnSavePrint_MouseLeave(object sender, EventArgs e)
        {
            btnSavePrint.BackgroundImage = global::LotzInventory.Properties.Resources.saveandprint;
        }

        private void btnPrint_MouseHover(object sender, EventArgs e)
        {
            btnPrint.BackgroundImage = global::LotzInventory.Properties.Resources.print_ovr;
        }

        private void btnPrint_MouseLeave(object sender, EventArgs e)
        {
            btnPrint.BackgroundImage = global::LotzInventory.Properties.Resources.print1;
        }

        private void btnCancel_MouseHover(object sender, EventArgs e)
        {
            btnCancel.BackgroundImage = global::LotzInventory.Properties.Resources.cancel_ovr;
        }

        private void btnCancel_MouseLeave(object sender, EventArgs e)
        {
            btnCancel.BackgroundImage = global::LotzInventory.Properties.Resources.cancel1;
        }

        private void btnExit_MouseHover(object sender, EventArgs e)
        {
            btnExit.BackgroundImage = global::LotzInventory.Properties.Resources.exit_ovr;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.BackgroundImage = global::LotzInventory.Properties.Resources.exit1;
        }

      

       
    }
}
