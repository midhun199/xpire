using System.Windows.Forms;
namespace Biz_Maxx
{
    partial class ucPurchase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucPurchase));
            this.Panel1 = new Panel();
            this.txtRoundOff = new Biz_Maxx.NumericTextBox();
            this.cmbTaxMode = new ComboBox();
            this.cmbPaymentMode = new ComboBox();
            this.txtExpense = new TextBox();
            this.txtDiscAmount = new TextBox();
            this.lblExpense = new System.Windows.Forms.Label();
            this.lblRoundOff = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTaxMode = new System.Windows.Forms.Label();
            this.txtInvAmount = new TextBox();
            this.dateInvoice = new System.Windows.Forms.DateTimePicker();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.txtInvNo = new TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPymtMode = new System.Windows.Forms.Label();
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.lblInvNumber = new System.Windows.Forms.Label();
            this.txtDescription = new RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSupplier = new TextBox();
            this.txtNumber = new TextBox();
            this.Panel3 = new Panel();
            this.lblInvAmount = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.chkPriceUpdation = new System.Windows.Forms.CheckBox();
            this.label26 = new System.Windows.Forms.Label();
            this.lblTotal = new Label();
            this.lblDiscount = new Label();
            this.lblCess = new Label();
            this.Panel2 = new Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblroundoffpost = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.lblaftertaxamt = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblTax = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.Panel4 = new Panel();
            this.btnSerach = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSavePrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.flxPurchaseDetails = new C1.Win.C1FlexGrid.C1FlexGrid();
            //((System.ComponentModel.ISupportInitialize)(this.Panel1)).BeginInit();
            //this.Panel1.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtRoundOff)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbTaxMode)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbPaymentMode)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtExpense)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtDiscAmount)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtInvAmount)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtInvNo)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtSupplier)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel3)).BeginInit();
            //this.Panel3.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel2)).BeginInit();
            //this.Panel2.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel4)).BeginInit();
            //this.Panel4.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.flxPurchaseDetails)).BeginInit();
            //this.SuspendLayout();
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));

            this.Panel1.Controls.Add(this.txtRoundOff);
            this.Panel1.Controls.Add(this.cmbTaxMode);
            this.Panel1.Controls.Add(this.cmbPaymentMode);
            this.Panel1.Controls.Add(this.txtExpense);
            this.Panel1.Controls.Add(this.txtDiscAmount);
            this.Panel1.Controls.Add(this.lblExpense);
            this.Panel1.Controls.Add(this.lblRoundOff);
            this.Panel1.Controls.Add(this.label6);
            this.Panel1.Controls.Add(this.lblTaxMode);
            this.Panel1.Controls.Add(this.txtInvAmount);
            this.Panel1.Controls.Add(this.dateInvoice);
            this.Panel1.Controls.Add(this.dtpDate);
            this.Panel1.Controls.Add(this.label2);
            this.Panel1.Controls.Add(this.label4);
            this.Panel1.Controls.Add(this.lblDate);
            this.Panel1.Controls.Add(this.txtInvNo);
            this.Panel1.Controls.Add(this.label5);
            this.Panel1.Controls.Add(this.lblPymtMode);
            this.Panel1.Controls.Add(this.lblSupplierName);
            this.Panel1.Controls.Add(this.lblInvNumber);
            this.Panel1.Controls.Add(this.txtDescription);
            this.Panel1.Controls.Add(this.label1);
            this.Panel1.Controls.Add(this.txtSupplier);
            this.Panel1.Controls.Add(this.txtNumber);
            this.Panel1.Location = new System.Drawing.Point(11, 81);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(1080, 135);
            this.Panel1.TabIndex = 0;
            // 
            // txtRoundOff
            // 
            this.txtRoundOff.DecimalLength = 4;
            this.txtRoundOff.DefaultValue = 0;

            this.txtRoundOff.Location = new System.Drawing.Point(604, 96);
            this.txtRoundOff.Name = "txtRoundOff";
            this.txtRoundOff.NumericLength = 10;

            this.txtRoundOff.Size = new System.Drawing.Size(150, 20);
            this.txtRoundOff.TabIndex = 11;
            // 
            // cmbTaxMode
            // 
            this.cmbTaxMode.Location = new System.Drawing.Point(604, 14);
            this.cmbTaxMode.Name = "cmbTaxMode";
            //this.cmbTaxMode.Properties.Buttons.AddRange(new Controls.EditorButton[] {
            //new Controls.EditorButton(Controls.ButtonPredefines.Combo)});
            this.cmbTaxMode.Size = new System.Drawing.Size(150, 20);
            this.cmbTaxMode.TabIndex = 8;
            // 
            // cmbPaymentMode
            // 
            this.cmbPaymentMode.Location = new System.Drawing.Point(112, 38);
            this.cmbPaymentMode.Name = "cmbPaymentMode";
            //this.cmbPaymentMode.Properties.Buttons.AddRange(new Controls.EditorButton[] {
            //new Controls.EditorButton(Controls.ButtonPredefines.Combo)});
            this.cmbPaymentMode.Items.AddRange(new object[] {
            "Cash",
            "Credit"});
            this.cmbPaymentMode.Size = new System.Drawing.Size(150, 20);
            this.cmbPaymentMode.TabIndex = 2;
            // 
            // txtExpense
            // 
            this.txtExpense.Location = new System.Drawing.Point(604, 68);
            this.txtExpense.Name = "txtExpense";

            this.txtExpense.Size = new System.Drawing.Size(150, 22);
            this.txtExpense.TabIndex = 10;
            this.txtExpense.Enter += new System.EventHandler(this.txtNumber_Enter);
            this.txtExpense.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTax_KeyPress);
            this.txtExpense.Validating += new System.ComponentModel.CancelEventHandler(this.txtTax_Validating);
            // 
            // txtDiscAmount
            // 
            this.txtDiscAmount.Location = new System.Drawing.Point(604, 40);
            this.txtDiscAmount.Name = "txtDiscAmount";
      
            this.txtDiscAmount.Size = new System.Drawing.Size(150, 22);
            this.txtDiscAmount.TabIndex = 9;
            this.txtDiscAmount.Enter += new System.EventHandler(this.txtNumber_Enter);
            this.txtDiscAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTax_KeyPress);
            this.txtDiscAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtTax_Validating);
            // 
            // lblExpense
            // 
            this.lblExpense.AutoSize = true;
            this.lblExpense.BackColor = System.Drawing.Color.Transparent;
            this.lblExpense.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblExpense.ForeColor = System.Drawing.Color.Black;
            this.lblExpense.Location = new System.Drawing.Point(528, 73);
            this.lblExpense.Name = "lblExpense";
            this.lblExpense.Size = new System.Drawing.Size(52, 15);
            this.lblExpense.TabIndex = 117;
            this.lblExpense.Text = "Expenses";
            // 
            // lblRoundOff
            // 
            this.lblRoundOff.AutoSize = true;
            this.lblRoundOff.BackColor = System.Drawing.Color.Transparent;
            this.lblRoundOff.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblRoundOff.ForeColor = System.Drawing.Color.Black;
            this.lblRoundOff.Location = new System.Drawing.Point(528, 98);
            this.lblRoundOff.Name = "lblRoundOff";
            this.lblRoundOff.Size = new System.Drawing.Size(59, 15);
            this.lblRoundOff.TabIndex = 118;
            this.lblRoundOff.Text = "Round Off";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(528, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 15);
            this.label6.TabIndex = 116;
            this.label6.Text = "Discount";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaxMode
            // 
            this.lblTaxMode.AutoSize = true;
            this.lblTaxMode.BackColor = System.Drawing.Color.Transparent;
            this.lblTaxMode.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTaxMode.ForeColor = System.Drawing.Color.Black;
            this.lblTaxMode.Location = new System.Drawing.Point(528, 15);
            this.lblTaxMode.Name = "lblTaxMode";
            this.lblTaxMode.Size = new System.Drawing.Size(56, 15);
            this.lblTaxMode.TabIndex = 119;
            this.lblTaxMode.Text = "Tax Mode";
            // 
            // txtInvAmount
            // 
            this.txtInvAmount.Location = new System.Drawing.Point(377, 66);
            this.txtInvAmount.Name = "txtInvAmount";
 
            this.txtInvAmount.Size = new System.Drawing.Size(139, 22);
            this.txtInvAmount.TabIndex = 7;
            this.txtInvAmount.Enter += new System.EventHandler(this.txtNumber_Enter);
            this.txtInvAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTax_KeyPress);
            this.txtInvAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtTax_Validating);
            // 
            // dateInvoice
            // 
            this.dateInvoice.CustomFormat = "dd/MM/yyyy";
            this.dateInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateInvoice.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateInvoice.Location = new System.Drawing.Point(377, 39);
            this.dateInvoice.Name = "dateInvoice";
            this.dateInvoice.Size = new System.Drawing.Size(139, 21);
            this.dateInvoice.TabIndex = 6;
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd/MM/yyyy";
            this.dtpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(377, 12);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(139, 21);
            this.dtpDate.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(275, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 15);
            this.label2.TabIndex = 112;
            this.label2.Text = "Invoice Amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(275, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 15);
            this.label4.TabIndex = 111;
            this.label4.Text = "Inv. Date";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(275, 15);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 15);
            this.lblDate.TabIndex = 110;
            this.lblDate.Text = "Date";
            // 
            // txtInvNo
            // 
            this.txtInvNo.Location = new System.Drawing.Point(112, 66);
            this.txtInvNo.Name = "txtInvNo";

            this.txtInvNo.Size = new System.Drawing.Size(150, 22);
            this.txtInvNo.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label5.Location = new System.Drawing.Point(6, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 15);
            this.label5.TabIndex = 108;
            this.label5.Text = "Inv. No.";
            // 
            // lblPymtMode
            // 
            this.lblPymtMode.AutoSize = true;
            this.lblPymtMode.BackColor = System.Drawing.Color.Transparent;
            this.lblPymtMode.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblPymtMode.Location = new System.Drawing.Point(6, 39);
            this.lblPymtMode.Name = "lblPymtMode";
            this.lblPymtMode.Size = new System.Drawing.Size(82, 15);
            this.lblPymtMode.TabIndex = 106;
            this.lblPymtMode.Text = "Payment Mode";
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.AutoSize = true;
            this.lblSupplierName.BackColor = System.Drawing.Color.Transparent;
            this.lblSupplierName.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSupplierName.Location = new System.Drawing.Point(6, 98);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(79, 15);
            this.lblSupplierName.TabIndex = 107;
            this.lblSupplierName.Text = "Supplier Name";
            // 
            // lblInvNumber
            // 
            this.lblInvNumber.AutoSize = true;
            this.lblInvNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblInvNumber.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblInvNumber.Location = new System.Drawing.Point(6, 12);
            this.lblInvNumber.Name = "lblInvNumber";
            this.lblInvNumber.Size = new System.Drawing.Size(46, 15);
            this.lblInvNumber.TabIndex = 105;
            this.lblInvNumber.Text = "Number";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(841, 10);
            this.txtDescription.Name = "txtDescription";

            this.txtDescription.Size = new System.Drawing.Size(232, 114);
            this.txtDescription.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(785, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 15);
            this.label1.TabIndex = 98;
            this.label1.Text = "Remarks";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSupplier
            // 
            this.txtSupplier.Location = new System.Drawing.Point(112, 95);
            this.txtSupplier.Name = "txtSupplier";

            this.txtSupplier.Size = new System.Drawing.Size(404, 22);
            this.txtSupplier.TabIndex = 4;
            this.txtSupplier.Enter += new System.EventHandler(this.txtNumber_Enter);
            this.txtSupplier.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSupplier_KeyDown);
            this.txtSupplier.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSupplier_KeyPress);
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(112, 10);
            this.txtNumber.Name = "txtNumber";

            this.txtNumber.Size = new System.Drawing.Size(150, 22);
            this.txtNumber.TabIndex = 1;
            this.txtNumber.Enter += new System.EventHandler(this.txtNumber_Enter);
            this.txtNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNumber_KeyDown);
            this.txtNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumber_KeyPress);
            this.txtNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtNumber_Validating);
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
 
            this.Panel3.Controls.Add(this.lblInvAmount);
            this.Panel3.Controls.Add(this.label28);
            this.Panel3.Location = new System.Drawing.Point(575, 494);

            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(516, 132);
            this.Panel3.TabIndex = 107;
            this.Panel3.TabStop = true;
            // 
            // lblInvAmount
            // 
            this.lblInvAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblInvAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 46F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInvAmount.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblInvAmount.Location = new System.Drawing.Point(202, 59);
            this.lblInvAmount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInvAmount.Name = "lblInvAmount";
            this.lblInvAmount.Size = new System.Drawing.Size(292, 60);
            this.lblInvAmount.TabIndex = 109;
            this.lblInvAmount.Text = "0.00";
            this.lblInvAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Courier New", 18F, System.Drawing.FontStyle.Bold);
            this.label28.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label28.Location = new System.Drawing.Point(6, 13);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(180, 27);
            this.label28.TabIndex = 108;
            this.label28.Text = "Grand Total ";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(11, 17);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(32, 15);
            this.label34.TabIndex = 49;
            this.label34.Text = "Total";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(11, 39);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(51, 15);
            this.label33.TabIndex = 51;
            this.label33.Text = "Discount";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(11, 59);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(30, 15);
            this.label29.TabIndex = 53;
            this.label29.Text = "Cess";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(98, 59);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(10, 15);
            this.label32.TabIndex = 54;
            this.label32.Text = ":";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(98, 39);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(10, 15);
            this.label25.TabIndex = 52;
            this.label25.Text = ":";
            // 
            // chkPriceUpdation
            // 
            this.chkPriceUpdation.AutoSize = true;
            this.chkPriceUpdation.BackColor = System.Drawing.Color.Transparent;
            this.chkPriceUpdation.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.chkPriceUpdation.ForeColor = System.Drawing.Color.Black;
            this.chkPriceUpdation.Location = new System.Drawing.Point(13, 100);
            this.chkPriceUpdation.Name = "chkPriceUpdation";
            this.chkPriceUpdation.Size = new System.Drawing.Size(99, 19);
            this.chkPriceUpdation.TabIndex = 55;
            this.chkPriceUpdation.Text = "Price Updation";
            this.chkPriceUpdation.UseVisualStyleBackColor = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(98, 17);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(10, 15);
            this.label26.TabIndex = 50;
            this.label26.Text = ":";
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(128, 16);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(22, 13);
            this.lblTotal.TabIndex = 56;
            this.lblTotal.Text = "0.00";
            // 
            // lblDiscount
            // 
            this.lblDiscount.Location = new System.Drawing.Point(128, 37);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(22, 13);
            this.lblDiscount.TabIndex = 57;
            this.lblDiscount.Text = "0.00";
            // 
            // lblCess
            // 
            this.lblCess.Location = new System.Drawing.Point(128, 60);
            this.lblCess.Name = "lblCess";
            this.lblCess.Size = new System.Drawing.Size(22, 13);
            this.lblCess.TabIndex = 58;
            this.lblCess.Text = "0.00";
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
       
 
            this.Panel2.Controls.Add(this.label3);
            this.Panel2.Controls.Add(this.label38);
            this.Panel2.Controls.Add(this.lblroundoffpost);
            this.Panel2.Controls.Add(this.label55);
            this.Panel2.Controls.Add(this.lblaftertaxamt);
            this.Panel2.Controls.Add(this.label40);
            this.Panel2.Controls.Add(this.label24);
            this.Panel2.Controls.Add(this.lblTax);
            this.Panel2.Controls.Add(this.label31);
            this.Panel2.Controls.Add(this.lblCess);
            this.Panel2.Controls.Add(this.lblDiscount);
            this.Panel2.Controls.Add(this.lblTotal);
            this.Panel2.Controls.Add(this.label26);
            this.Panel2.Controls.Add(this.chkPriceUpdation);
            this.Panel2.Controls.Add(this.label25);
            this.Panel2.Controls.Add(this.label32);
            this.Panel2.Controls.Add(this.label29);
            this.Panel2.Controls.Add(this.label33);
            this.Panel2.Controls.Add(this.label34);
            this.Panel2.Location = new System.Drawing.Point(11, 494);

            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(561, 132);
            this.Panel2.TabIndex = 106;
            this.Panel2.TabStop = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(372, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 15);
            this.label3.TabIndex = 118;
            this.label3.Text = ":";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(372, 17);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(10, 15);
            this.label38.TabIndex = 120;
            this.label38.Text = ":";
            // 
            // lblroundoffpost
            // 
            this.lblroundoffpost.BackColor = System.Drawing.Color.Transparent;
            this.lblroundoffpost.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblroundoffpost.ForeColor = System.Drawing.Color.Black;
            this.lblroundoffpost.Location = new System.Drawing.Point(386, 59);
            this.lblroundoffpost.Name = "lblroundoffpost";
            this.lblroundoffpost.Size = new System.Drawing.Size(99, 16);
            this.lblroundoffpost.TabIndex = 123;
            this.lblroundoffpost.Text = "0.00";
            this.lblroundoffpost.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(307, 59);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(53, 15);
            this.label55.TabIndex = 122;
            this.label55.Text = "Roundoff";
            // 
            // lblaftertaxamt
            // 
            this.lblaftertaxamt.BackColor = System.Drawing.Color.Transparent;
            this.lblaftertaxamt.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblaftertaxamt.ForeColor = System.Drawing.Color.Black;
            this.lblaftertaxamt.Location = new System.Drawing.Point(386, 17);
            this.lblaftertaxamt.Name = "lblaftertaxamt";
            this.lblaftertaxamt.Size = new System.Drawing.Size(99, 16);
            this.lblaftertaxamt.TabIndex = 121;
            this.lblaftertaxamt.Text = "0.00";
            this.lblaftertaxamt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(307, 17);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(52, 15);
            this.label40.TabIndex = 119;
            this.label40.Text = "Disc.A.T";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(372, 39);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 15);
            this.label24.TabIndex = 117;
            this.label24.Text = ":";
            // 
            // lblTax
            // 
            this.lblTax.BackColor = System.Drawing.Color.Transparent;
            this.lblTax.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTax.ForeColor = System.Drawing.Color.Black;
            this.lblTax.Location = new System.Drawing.Point(386, 39);
            this.lblTax.Name = "lblTax";
            this.lblTax.Size = new System.Drawing.Size(99, 16);
            this.lblTax.TabIndex = 118;
            this.lblTax.Text = "0.00";
            this.lblTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(307, 39);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(24, 15);
            this.label31.TabIndex = 116;
            this.label31.Text = "Tax";
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));

            this.Panel4.Controls.Add(this.btnSerach);
            this.Panel4.Controls.Add(this.btnNew);
            this.Panel4.Controls.Add(this.btnCancel);
            this.Panel4.Controls.Add(this.btnExit);
            this.Panel4.Controls.Add(this.btnSavePrint);
            this.Panel4.Controls.Add(this.btnSave);
            this.Panel4.Location = new System.Drawing.Point(11, 3);
  
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1079, 82);
            this.Panel4.TabIndex = 14;
            // 
            // btnSerach
            // 
            this.btnSerach.BackColor = System.Drawing.Color.White;
            this.btnSerach.BackgroundImage = global::LotzInventory.Properties.Resources.search1;
            this.btnSerach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSerach.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnSerach.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSerach.ForeColor = System.Drawing.Color.Maroon;
            this.btnSerach.Location = new System.Drawing.Point(7, 7);
            this.btnSerach.Margin = new System.Windows.Forms.Padding(4);
            this.btnSerach.Name = "btnSerach";
            this.btnSerach.Size = new System.Drawing.Size(76, 71);
            this.btnSerach.TabIndex = 16;
            this.btnSerach.TabStop = false;
            this.btnSerach.Tag = "";
            this.btnSerach.UseVisualStyleBackColor = false;
            this.btnSerach.Click += new System.EventHandler(this.btnSearch_Click);
            this.btnSerach.MouseLeave += new System.EventHandler(this.btnSerach_MouseLeave);
            this.btnSerach.MouseHover += new System.EventHandler(this.btnSerach_MouseHover);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.White;
            this.btnNew.BackgroundImage = global::LotzInventory.Properties.Resources.new2;
            this.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ForeColor = System.Drawing.Color.Maroon;
            this.btnNew.Location = new System.Drawing.Point(82, 7);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(76, 71);
            this.btnNew.TabIndex = 17;
            this.btnNew.TabStop = false;
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.btnNew.MouseLeave += new System.EventHandler(this.btnNew_MouseLeave);
            this.btnNew.MouseHover += new System.EventHandler(this.btnNew_MouseHover);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.White;
            this.btnCancel.BackgroundImage = global::LotzInventory.Properties.Resources.delete;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.Maroon;
            this.btnCancel.Location = new System.Drawing.Point(307, 7);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(76, 71);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.TabStop = false;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnCancel.MouseLeave += new System.EventHandler(this.btnCancel_MouseLeave);
            this.btnCancel.MouseHover += new System.EventHandler(this.btnCancel_MouseHover);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundImage = global::LotzInventory.Properties.Resources.exit1;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Maroon;
            this.btnExit.Location = new System.Drawing.Point(382, 7);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 71);
            this.btnExit.TabIndex = 19;
            this.btnExit.TabStop = false;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseLeave += new System.EventHandler(this.btnExit_MouseLeave);
            this.btnExit.MouseHover += new System.EventHandler(this.btnExit_MouseHover);
            // 
            // btnSavePrint
            // 
            this.btnSavePrint.BackColor = System.Drawing.Color.White;
            this.btnSavePrint.BackgroundImage = global::LotzInventory.Properties.Resources.saveandprint;
            this.btnSavePrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSavePrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavePrint.ForeColor = System.Drawing.Color.Maroon;
            this.btnSavePrint.Location = new System.Drawing.Point(232, 7);
            this.btnSavePrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnSavePrint.Name = "btnSavePrint";
            this.btnSavePrint.Size = new System.Drawing.Size(76, 71);
            this.btnSavePrint.TabIndex = 15;
            this.btnSavePrint.TabStop = false;
            this.btnSavePrint.UseVisualStyleBackColor = false;
            this.btnSavePrint.Click += new System.EventHandler(this.btnSavePrint_Click);
            this.btnSavePrint.MouseLeave += new System.EventHandler(this.btnSavePrint_MouseLeave);
            this.btnSavePrint.MouseHover += new System.EventHandler(this.btnSavePrint_MouseHover);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.BackgroundImage = global::LotzInventory.Properties.Resources.save1;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Maroon;
            this.btnSave.Location = new System.Drawing.Point(157, 7);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 71);
            this.btnSave.TabIndex = 14;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseLeave += new System.EventHandler(this.btnSave_MouseLeave);
            this.btnSave.MouseHover += new System.EventHandler(this.btnSave_MouseHover);
            // 
            // flxPurchaseDetails
            // 
            this.flxPurchaseDetails.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.flxPurchaseDetails.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.flxPurchaseDetails.AutoResize = false;
            this.flxPurchaseDetails.BackColor = System.Drawing.SystemColors.Window;
            this.flxPurchaseDetails.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.FixedSingle;
            this.flxPurchaseDetails.ColumnInfo = "31,1,0,0,0,85,Columns:0{Width:26;}\t";
            this.flxPurchaseDetails.ExtendLastCol = true;
            this.flxPurchaseDetails.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.Solid;
            this.flxPurchaseDetails.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flxPurchaseDetails.KeyActionEnter = C1.Win.C1FlexGrid.KeyActionEnum.None;
            this.flxPurchaseDetails.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross;
            this.flxPurchaseDetails.Location = new System.Drawing.Point(11, 221);
            this.flxPurchaseDetails.Name = "flxPurchaseDetails";
            this.flxPurchaseDetails.Rows.Count = 20;
            this.flxPurchaseDetails.Rows.MinSize = 21;
            this.flxPurchaseDetails.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.flxPurchaseDetails.Size = new System.Drawing.Size(1080, 268);
            this.flxPurchaseDetails.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("flxPurchaseDetails.Styles"));
            this.flxPurchaseDetails.TabIndex = 13;
            this.flxPurchaseDetails.BeforeRowColChange += new C1.Win.C1FlexGrid.RangeEventHandler(this.flxPurchaseDetails_BeforeRowColChange);
            this.flxPurchaseDetails.LeaveCell += new System.EventHandler(this.flxPurchaseDetails_LeaveCell);
            this.flxPurchaseDetails.EnterCell += new System.EventHandler(this.flxPurchaseDetails_EnterCell);
            this.flxPurchaseDetails.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.flxPurchaseDetails_SetupEditor);
            this.flxPurchaseDetails.ValidateEdit += new C1.Win.C1FlexGrid.ValidateEditEventHandler(this.flxPurchaseDetails_ValidateEdit);
            this.flxPurchaseDetails.KeyDownEdit += new C1.Win.C1FlexGrid.KeyEditEventHandler(this.flxPurchaseDetails_KeyDownEdit);
            this.flxPurchaseDetails.KeyPressEdit += new C1.Win.C1FlexGrid.KeyPressEditEventHandler(this.flxPurchaseDetails_KeyPressEdit);
            this.flxPurchaseDetails.Click += new System.EventHandler(this.flxPurchaseDetails_Click);
            this.flxPurchaseDetails.Enter += new System.EventHandler(this.flxPurchaseDetails_Enter);
            // 
            // ucPurchase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flxPurchaseDetails);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel1);
            this.Name = "ucPurchase";
            this.Size = new System.Drawing.Size(1111, 643);
            this.Load += new System.EventHandler(this.ucPurchase_Load);
            //((System.ComponentModel.ISupportInitialize)(this.Panel1)).EndInit();
            //this.Panel1.ResumeLayout(false);
            //this.Panel1.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtRoundOff)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbTaxMode)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbPaymentMode)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtExpense)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtDiscAmount)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtInvAmount)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtInvNo)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtSupplier)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel3)).EndInit();
            //this.Panel3.ResumeLayout(false);
            //this.Panel3.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel2)).EndInit();
            //this.Panel2.ResumeLayout(false);
            //this.Panel2.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel4)).EndInit();
            //this.Panel4.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.flxPurchaseDetails)).EndInit();
            //this.ResumeLayout(false);

        }

        #endregion

        private Panel Panel1;
        private RichTextBox txtDescription;
        internal System.Windows.Forms.Label label1;
        private TextBox txtSupplier;
        private TextBox txtNumber;
        private Panel Panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPymtMode;
        private System.Windows.Forms.Label lblSupplierName;
        private System.Windows.Forms.Label lblInvNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblDate;
        private TextBox txtInvNo;
        private TextBox txtInvAmount;
        private System.Windows.Forms.DateTimePicker dateInvoice;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private TextBox txtExpense;
        private TextBox txtDiscAmount;
        private System.Windows.Forms.Label lblExpense;
        private System.Windows.Forms.Label lblRoundOff;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTaxMode;
        internal System.Windows.Forms.Label lblInvAmount;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox chkPriceUpdation;
        private System.Windows.Forms.Label label26;
        private Label lblTotal;
        private Label lblDiscount;
        private Label lblCess;
        private Panel Panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblroundoffpost;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label lblaftertaxamt;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblTax;
        private System.Windows.Forms.Label label31;
        private Panel Panel4;
        internal System.Windows.Forms.Button btnSerach;
        internal System.Windows.Forms.Button btnNew;
        internal System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Button btnSavePrint;
        internal System.Windows.Forms.Button btnSave;
        private ComboBox cmbTaxMode;
        private ComboBox cmbPaymentMode;
        private NumericTextBox txtRoundOff;
        private C1.Win.C1FlexGrid.C1FlexGrid flxPurchaseDetails;
    }
}
