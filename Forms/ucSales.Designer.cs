

using System.Windows.Forms;
namespace Biz_Maxx
{
    partial class ucSales
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucSales));
            this.Panel1 = new Panel();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cmbPriceType = new ComboBox();
            this.cmbBillType = new ComboBox();
            this.cmbFormType = new ComboBox();
            this.txtRemarks = new RichTextBox();
            this.btnOtherDetails = new Button();
            this.btnUpdate = new Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtCustomer = new TextBox();
            this.txtFromBill = new TextBox();
            this.txtOrderno = new TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.lblsorder = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Panel2 = new Panel();
            this.txtSalesPerson = new TextBox();
            this.txtRoundoff = new TextBox();
            this.txtexpense = new TextBox();
            this.txtSplDiscAmt = new TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lbltotaldiscount = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblCess = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labTax = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.lblQty1 = new System.Windows.Forms.Label();
            this.lblcurrentQty = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Panel3 = new Panel();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblBillAmt = new System.Windows.Forms.Label();
            this.Panel4 = new Panel();
            this.btnSerach = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSavePrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.FlexSalesDetails = new C1.Win.C1FlexGrid.C1FlexGrid();
            //((System.ComponentModel.ISupportInitialize)(this.Panel1)).BeginInit();
            //this.Panel1.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.cmbPriceType)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbBillType)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbFormType)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtRemarks)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtFromBill)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtOrderno)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel2)).BeginInit();
            //this.Panel2.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtSalesPerson)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtRoundoff)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtexpense)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtSplDiscAmt)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel3)).BeginInit();
            //this.Panel3.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel4)).BeginInit();
            //this.Panel4.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.FlexSalesDetails)).BeginInit();
            //this.SuspendLayout();
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
            
            
            this.Panel1.Controls.Add(this.dtpDate);
            this.Panel1.Controls.Add(this.cmbPriceType);
            this.Panel1.Controls.Add(this.cmbBillType);
            this.Panel1.Controls.Add(this.cmbFormType);
            this.Panel1.Controls.Add(this.txtRemarks);
            this.Panel1.Controls.Add(this.btnOtherDetails);
            this.Panel1.Controls.Add(this.btnUpdate);
            this.Panel1.Controls.Add(this.label8);
            this.Panel1.Controls.Add(this.label1);
            this.Panel1.Controls.Add(this.Label2);
            this.Panel1.Controls.Add(this.txtCustomer);
            this.Panel1.Controls.Add(this.txtFromBill);
            this.Panel1.Controls.Add(this.txtOrderno);
            this.Panel1.Controls.Add(this.Label4);
            this.Panel1.Controls.Add(this.Label6);
            this.Panel1.Controls.Add(this.Label7);
            this.Panel1.Controls.Add(this.lblsorder);
            this.Panel1.Controls.Add(this.label13);
            this.Panel1.Location = new System.Drawing.Point(11, 88);
        
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(1079, 135);
            this.Panel1.TabIndex = 0;
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd/MM/yyyy";
            this.dtpDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(354, 42);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(137, 21);
            this.dtpDate.TabIndex = 6;
            // 
            // cmbPriceType
            // 
            this.cmbPriceType.Location = new System.Drawing.Point(354, 71);
            this.cmbPriceType.Name = "cmbPriceType";
            //this.cmbPriceType.Properties.Buttons.AddRange(new Controls.EditorButton[] {
            //new Controls.EditorButton(Controls.ButtonPredefines.Combo)});
            this.cmbPriceType.Items.AddRange(new object[] {
            "Retail",
            "Whole Sale",
            "Special"});
            this.cmbPriceType.Size = new System.Drawing.Size(137, 20);
            this.cmbPriceType.TabIndex = 7;
            this.cmbPriceType.SelectedIndexChanged += new System.EventHandler(this.cmbPriceType_SelectedIndexChanged);
            // 
            // cmbBillType
            // 
            this.cmbBillType.Location = new System.Drawing.Point(96, 71);
            this.cmbBillType.Name = "cmbBillType";
            //this.cmbBillType.Properties.Buttons.AddRange(new Controls.EditorButton[] {
            //new Controls.EditorButton(Controls.ButtonPredefines.Combo)});
            this.cmbBillType.Items.AddRange(new object[] {
            "CASH",
            "CREDIT"});
            this.cmbBillType.Size = new System.Drawing.Size(137, 20);
            this.cmbBillType.TabIndex = 2;
            // 
            // cmbFormType
            // 
            this.cmbFormType.Location = new System.Drawing.Point(96, 40);
            this.cmbFormType.Name = "cmbFormType";
            //this.cmbFormType.Properties.Buttons.AddRange(new Controls.EditorButton[] {
            //new Controls.EditorButton(Controls.ButtonPredefines.Combo)});
            this.cmbFormType.Size = new System.Drawing.Size(137, 20);
            this.cmbFormType.TabIndex = 1;
            this.cmbFormType.SelectedIndexChanged += new System.EventHandler(this.cmbFormType_SelectedIndexChanged);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(601, 10);
            this.txtRemarks.Name = "txtRemarks";

            this.txtRemarks.Size = new System.Drawing.Size(480, 114);
            this.txtRemarks.TabIndex = 9;
            // 
            // btnOtherDetails
            // 
    
            this.btnOtherDetails.Location = new System.Drawing.Point(494, 99);
            this.btnOtherDetails.Name = "btnOtherDetails";
            this.btnOtherDetails.Size = new System.Drawing.Size(75, 23);
            this.btnOtherDetails.TabIndex = 4;
            this.btnOtherDetails.Text = "....";
            this.btnOtherDetails.Click += new System.EventHandler(this.btnOtherDetails_Click);
            // 
            // btnUpdate
            // 

            this.btnUpdate.Location = new System.Drawing.Point(494, 70);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 8;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(249, 71);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 15);
            this.label8.TabIndex = 100;
            this.label8.Text = "Price Type";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(537, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 15);
            this.label1.TabIndex = 98;
            this.label1.Text = "Remarks";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(15, 13);
            this.Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(60, 15);
            this.Label2.TabIndex = 86;
            this.Label2.Text = "Invoice No";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(96, 100);
            this.txtCustomer.Name = "txtCustomer";

            this.txtCustomer.Size = new System.Drawing.Size(395, 22);
            this.txtCustomer.TabIndex = 3;
            this.txtCustomer.Enter += new System.EventHandler(this.txtCustomer_Enter);
            this.txtCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomer_KeyDown);
            this.txtCustomer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCustomer_KeyPress);
            // 
            // txtFromBill
            // 
            this.txtFromBill.Location = new System.Drawing.Point(354, 10);
            this.txtFromBill.Name = "txtFromBill";

            this.txtFromBill.Size = new System.Drawing.Size(137, 22);
            this.txtFromBill.TabIndex = 5;
            // 
            // txtOrderno
            // 
            this.txtOrderno.Location = new System.Drawing.Point(96, 10);
            this.txtOrderno.Name = "txtOrderno";
    
            this.txtOrderno.Size = new System.Drawing.Size(137, 22);
            this.txtOrderno.TabIndex = 0;
            this.txtOrderno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOrderno_KeyPress);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.BackColor = System.Drawing.Color.Transparent;
            this.Label4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(15, 104);
            this.Label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(54, 15);
            this.Label4.TabIndex = 87;
            this.Label4.Text = "Customer";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.BackColor = System.Drawing.Color.Transparent;
            this.Label6.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.ForeColor = System.Drawing.Color.Black;
            this.Label6.Location = new System.Drawing.Point(15, 42);
            this.Label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(62, 15);
            this.Label6.TabIndex = 88;
            this.Label6.Text = "Form Type";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.BackColor = System.Drawing.Color.Transparent;
            this.Label7.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.ForeColor = System.Drawing.Color.Black;
            this.Label7.Location = new System.Drawing.Point(249, 44);
            this.Label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(30, 15);
            this.Label7.TabIndex = 89;
            this.Label7.Text = "Date";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblsorder
            // 
            this.lblsorder.AutoSize = true;
            this.lblsorder.BackColor = System.Drawing.Color.Transparent;
            this.lblsorder.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsorder.ForeColor = System.Drawing.Color.Black;
            this.lblsorder.Location = new System.Drawing.Point(249, 13);
            this.lblsorder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblsorder.Name = "lblsorder";
            this.lblsorder.Size = new System.Drawing.Size(73, 15);
            this.lblsorder.TabIndex = 92;
            this.lblsorder.Text = "Sales Inv. No.";
            this.lblsorder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(15, 74);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 15);
            this.label13.TabIndex = 91;
            this.label13.Text = "Bill Type";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));

            this.Panel2.Controls.Add(this.txtSalesPerson);
            this.Panel2.Controls.Add(this.txtRoundoff);
            this.Panel2.Controls.Add(this.txtexpense);
            this.Panel2.Controls.Add(this.txtSplDiscAmt);
            this.Panel2.Controls.Add(this.label3);
            this.Panel2.Controls.Add(this.label32);
            this.Panel2.Controls.Add(this.lbltotaldiscount);
            this.Panel2.Controls.Add(this.label31);
            this.Panel2.Controls.Add(this.label11);
            this.Panel2.Controls.Add(this.lblCess);
            this.Panel2.Controls.Add(this.label10);
            this.Panel2.Controls.Add(this.labTax);
            this.Panel2.Controls.Add(this.Label12);
            this.Panel2.Controls.Add(this.lblQty1);
            this.Panel2.Controls.Add(this.lblcurrentQty);
            this.Panel2.Controls.Add(this.label9);
            this.Panel2.Location = new System.Drawing.Point(12, 486);

            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(561, 146);
            this.Panel2.TabIndex = 11;
            // 
            // txtSalesPerson
            // 
            this.txtSalesPerson.Location = new System.Drawing.Point(130, 109);
            this.txtSalesPerson.Name = "txtSalesPerson";

            this.txtSalesPerson.Size = new System.Drawing.Size(137, 22);
            this.txtSalesPerson.TabIndex = 14;
            this.txtSalesPerson.Enter += new System.EventHandler(this.txtSalesPerson_Enter);
            this.txtSalesPerson.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSalesPerson_KeyDown);
            this.txtSalesPerson.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalesPerson_KeyPress);
            // 
            // txtRoundoff
            // 
            this.txtRoundoff.Text = "0.00";
            this.txtRoundoff.Location = new System.Drawing.Point(131, 77);
            this.txtRoundoff.Name = "txtRoundoff";
           
            this.txtRoundoff.Size = new System.Drawing.Size(137, 22);
            this.txtRoundoff.TabIndex = 13;
            // 
            // txtexpense
            // 
            this.txtexpense.Text = "0.00";
            this.txtexpense.Location = new System.Drawing.Point(131, 46);
            this.txtexpense.Name = "txtexpense";
 
            this.txtexpense.Size = new System.Drawing.Size(137, 22);
            this.txtexpense.TabIndex = 12;
            this.txtexpense.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSplDiscAmt_KeyPress);
            this.txtexpense.Leave += new System.EventHandler(this.txtSplDiscAmt_Leave);
            // 
            // txtSplDiscAmt
            // 

            this.txtSplDiscAmt.Location = new System.Drawing.Point(130, 13);
            this.txtSplDiscAmt.Name = "txtSplDiscAmt";

            this.txtSplDiscAmt.Size = new System.Drawing.Size(137, 22);
            this.txtSplDiscAmt.TabIndex = 11;
            this.txtSplDiscAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSplDiscAmt_KeyPress);
            this.txtSplDiscAmt.Leave += new System.EventHandler(this.txtSplDiscAmt_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(15, 112);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 91;
            this.label3.Text = "Sales Person";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(15, 47);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(47, 15);
            this.label32.TabIndex = 90;
            this.label32.Text = "Expense";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbltotaldiscount
            // 
            this.lbltotaldiscount.BackColor = System.Drawing.Color.Transparent;
            this.lbltotaldiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotaldiscount.ForeColor = System.Drawing.Color.Black;
            this.lbltotaldiscount.Location = new System.Drawing.Point(405, 113);
            this.lbltotaldiscount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbltotaldiscount.Name = "lbltotaldiscount";
            this.lbltotaldiscount.Size = new System.Drawing.Size(145, 18);
            this.lbltotaldiscount.TabIndex = 89;
            this.lbltotaldiscount.Text = "0.00";
            this.lbltotaldiscount.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(335, 114);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 15);
            this.label31.TabIndex = 88;
            this.label31.Text = "Discount";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(335, 82);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 15);
            this.label11.TabIndex = 87;
            this.label11.Text = "Cess Amount";
            // 
            // lblCess
            // 
            this.lblCess.BackColor = System.Drawing.Color.Transparent;
            this.lblCess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCess.ForeColor = System.Drawing.Color.Black;
            this.lblCess.Location = new System.Drawing.Point(405, 82);
            this.lblCess.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCess.Name = "lblCess";
            this.lblCess.Size = new System.Drawing.Size(145, 18);
            this.lblCess.TabIndex = 86;
            this.lblCess.Text = "0.00";
            this.lblCess.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(335, 51);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 15);
            this.label10.TabIndex = 85;
            this.label10.Text = "Tax Amount";
            // 
            // labTax
            // 
            this.labTax.BackColor = System.Drawing.Color.Transparent;
            this.labTax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTax.ForeColor = System.Drawing.Color.Black;
            this.labTax.Location = new System.Drawing.Point(405, 49);
            this.labTax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labTax.Name = "labTax";
            this.labTax.Size = new System.Drawing.Size(145, 18);
            this.labTax.TabIndex = 84;
            this.labTax.Text = "0.00";
            this.labTax.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.BackColor = System.Drawing.Color.Transparent;
            this.Label12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.ForeColor = System.Drawing.Color.Black;
            this.Label12.Location = new System.Drawing.Point(15, 79);
            this.Label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(59, 15);
            this.Label12.TabIndex = 83;
            this.Label12.Text = "Round Off";
            // 
            // lblQty1
            // 
            this.lblQty1.AutoSize = true;
            this.lblQty1.BackColor = System.Drawing.Color.Transparent;
            this.lblQty1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQty1.ForeColor = System.Drawing.Color.Black;
            this.lblQty1.Location = new System.Drawing.Point(335, 16);
            this.lblQty1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQty1.Name = "lblQty1";
            this.lblQty1.Size = new System.Drawing.Size(67, 15);
            this.lblQty1.TabIndex = 66;
            this.lblQty1.Text = "Current Qty";
            // 
            // lblcurrentQty
            // 
            this.lblcurrentQty.BackColor = System.Drawing.Color.Transparent;
            this.lblcurrentQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblcurrentQty.ForeColor = System.Drawing.Color.Black;
            this.lblcurrentQty.Location = new System.Drawing.Point(405, 15);
            this.lblcurrentQty.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblcurrentQty.Name = "lblcurrentQty";
            this.lblcurrentQty.Size = new System.Drawing.Size(145, 18);
            this.lblcurrentQty.TabIndex = 67;
            this.lblcurrentQty.Text = "0.000";
            this.lblcurrentQty.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(15, 15);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 15);
            this.label9.TabIndex = 65;
            this.label9.Text = "Discount";
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));
  
            this.Panel3.Controls.Add(this.lblAmount);
            this.Panel3.Controls.Add(this.lblBillAmt);
            this.Panel3.Location = new System.Drawing.Point(575, 486);

            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(515, 146);
            this.Panel3.TabIndex = 86;
            this.Panel3.TabStop = true;
            // 
            // lblAmount
            // 
            this.lblAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 46F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.ForeColor = System.Drawing.Color.Black;
            this.lblAmount.Location = new System.Drawing.Point(52, 55);
            this.lblAmount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(471, 85);
            this.lblAmount.TabIndex = 8;
            this.lblAmount.Text = "0.00";
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBillAmt
            // 
            this.lblBillAmt.AutoSize = true;
            this.lblBillAmt.Font = new System.Drawing.Font("Times New Roman", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillAmt.ForeColor = System.Drawing.Color.Black;
            this.lblBillAmt.Location = new System.Drawing.Point(13, 12);
            this.lblBillAmt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBillAmt.Name = "lblBillAmt";
            this.lblBillAmt.Size = new System.Drawing.Size(182, 39);
            this.lblBillAmt.TabIndex = 7;
            this.lblBillAmt.Text = "Bill Amount";
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(213)))), ((int)(((byte)(217)))));

            this.Panel4.Controls.Add(this.btnSerach);
            this.Panel4.Controls.Add(this.btnNew);
            this.Panel4.Controls.Add(this.btnPrint);
            this.Panel4.Controls.Add(this.btnCancel);
            this.Panel4.Controls.Add(this.btnExit);
            this.Panel4.Controls.Add(this.btnSavePrint);
            this.Panel4.Controls.Add(this.btnSave);
            this.Panel4.Location = new System.Drawing.Point(11, 3);

            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1079, 82);
            this.Panel4.TabIndex = 15;
            // 
            // btnSerach
            // 
            this.btnSerach.BackColor = System.Drawing.Color.White;
            this.btnSerach.BackgroundImage = global::LotzInventory.Properties.Resources.search;
            this.btnSerach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSerach.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSerach.ForeColor = System.Drawing.Color.Maroon;
            this.btnSerach.Location = new System.Drawing.Point(7, 7);
            this.btnSerach.Margin = new System.Windows.Forms.Padding(4);
            this.btnSerach.Name = "btnSerach";
            this.btnSerach.Size = new System.Drawing.Size(76, 71);
            this.btnSerach.TabIndex = 20;
            this.btnSerach.TabStop = false;
            this.btnSerach.UseVisualStyleBackColor = false;
            this.btnSerach.Click += new System.EventHandler(this.btnSerach_Click);
            this.btnSerach.MouseLeave += new System.EventHandler(this.btnSerach_MouseLeave);
            this.btnSerach.MouseHover += new System.EventHandler(this.btnSerach_MouseHover);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.White;
            this.btnNew.BackgroundImage = global::LotzInventory.Properties.Resources.new2;
            this.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ForeColor = System.Drawing.Color.Maroon;
            this.btnNew.Location = new System.Drawing.Point(82, 7);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(76, 71);
            this.btnNew.TabIndex = 19;
            this.btnNew.TabStop = false;
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            this.btnNew.MouseLeave += new System.EventHandler(this.btnNew_MouseLeave);
            this.btnNew.MouseHover += new System.EventHandler(this.btnNew_MouseHover);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.BackgroundImage = global::LotzInventory.Properties.Resources.print1;
            this.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Maroon;
            this.btnPrint.Location = new System.Drawing.Point(307, 7);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(76, 71);
            this.btnPrint.TabIndex = 18;
            this.btnPrint.TabStop = false;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.btnPrint.MouseLeave += new System.EventHandler(this.btnPrint_MouseLeave);
            this.btnPrint.MouseHover += new System.EventHandler(this.btnPrint_MouseHover);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.White;
            this.btnCancel.BackgroundImage = global::LotzInventory.Properties.Resources.cancel1;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.Maroon;
            this.btnCancel.Location = new System.Drawing.Point(382, 7);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(76, 71);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.TabStop = false;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnCancel.MouseLeave += new System.EventHandler(this.btnCancel_MouseLeave);
            this.btnCancel.MouseHover += new System.EventHandler(this.btnCancel_MouseHover);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundImage = global::LotzInventory.Properties.Resources.exit1;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.CausesValidation = false;
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Maroon;
            this.btnExit.Location = new System.Drawing.Point(457, 7);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 71);
            this.btnExit.TabIndex = 22;
            this.btnExit.TabStop = false;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseLeave += new System.EventHandler(this.btnExit_MouseLeave);
            this.btnExit.MouseHover += new System.EventHandler(this.btnExit_MouseHover);
            // 
            // btnSavePrint
            // 
            this.btnSavePrint.BackColor = System.Drawing.Color.White;
            this.btnSavePrint.BackgroundImage = global::LotzInventory.Properties.Resources.saveandprint;
            this.btnSavePrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSavePrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavePrint.ForeColor = System.Drawing.Color.Maroon;
            this.btnSavePrint.Location = new System.Drawing.Point(232, 7);
            this.btnSavePrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnSavePrint.Name = "btnSavePrint";
            this.btnSavePrint.Size = new System.Drawing.Size(76, 71);
            this.btnSavePrint.TabIndex = 17;
            this.btnSavePrint.TabStop = false;
            this.btnSavePrint.UseVisualStyleBackColor = false;
            this.btnSavePrint.Click += new System.EventHandler(this.btnSavePrint_Click);
            this.btnSavePrint.MouseLeave += new System.EventHandler(this.btnSavePrint_MouseLeave);
            this.btnSavePrint.MouseHover += new System.EventHandler(this.btnSavePrint_MouseHover);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.White;
            this.btnSave.BackgroundImage = global::LotzInventory.Properties.Resources.save1;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Maroon;
            this.btnSave.Location = new System.Drawing.Point(157, 7);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 71);
            this.btnSave.TabIndex = 16;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseLeave += new System.EventHandler(this.btnSave_MouseLeave);
            this.btnSave.MouseHover += new System.EventHandler(this.btnSave_MouseHover);
            // 
            // FlexSalesDetails
            // 
            this.FlexSalesDetails.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.FlexSalesDetails.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.FlexSalesDetails.AutoResize = false;
            this.FlexSalesDetails.BackColor = System.Drawing.SystemColors.Window;
            this.FlexSalesDetails.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.FixedSingle;
            this.FlexSalesDetails.ColumnInfo = "31,1,0,0,0,85,Columns:0{Width:26;}\t";
            this.FlexSalesDetails.ExtendLastCol = true;
            this.FlexSalesDetails.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.Solid;
            this.FlexSalesDetails.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FlexSalesDetails.KeyActionEnter = C1.Win.C1FlexGrid.KeyActionEnum.None;
            this.FlexSalesDetails.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross;
            this.FlexSalesDetails.Location = new System.Drawing.Point(11, 223);
            this.FlexSalesDetails.Name = "FlexSalesDetails";
            this.FlexSalesDetails.Rows.Count = 20;
            this.FlexSalesDetails.Rows.MinSize = 21;
            this.FlexSalesDetails.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.FlexSalesDetails.Size = new System.Drawing.Size(1078, 262);
            this.FlexSalesDetails.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("FlexSalesDetails.Styles"));
            this.FlexSalesDetails.TabIndex = 10;
            this.FlexSalesDetails.BeforeRowColChange += new C1.Win.C1FlexGrid.RangeEventHandler(this.FlexSalesDetails_BeforeRowColChange);
            this.FlexSalesDetails.EnterCell += new System.EventHandler(this.FlexSalesDetails_EnterCell);
            this.FlexSalesDetails.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.FlexSalesDetails_SetupEditor);
            this.FlexSalesDetails.ValidateEdit += new C1.Win.C1FlexGrid.ValidateEditEventHandler(this.FlexSalesDetails_ValidateEdit);
            this.FlexSalesDetails.KeyDownEdit += new C1.Win.C1FlexGrid.KeyEditEventHandler(this.FlexSalesDetails_KeyDownEdit);
            this.FlexSalesDetails.KeyPressEdit += new C1.Win.C1FlexGrid.KeyPressEditEventHandler(this.FlexSalesDetails_KeyPressEdit);
            this.FlexSalesDetails.Click += new System.EventHandler(this.FlexSalesDetails_Click);
            this.FlexSalesDetails.Enter += new System.EventHandler(this.FlexSalesDetails_Enter);
            // 
            // ucSales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.FlexSalesDetails);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel1);
            this.Name = "ucSales";
            this.Size = new System.Drawing.Size(1111, 640);
            this.Load += new System.EventHandler(this.ucSales_Load);
            //((System.ComponentModel.ISupportInitialize)(this.Panel1)).EndInit();
            //this.Panel1.ResumeLayout(false);
            //this.Panel1.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.cmbPriceType)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbBillType)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmbFormType)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtRemarks)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtCustomer)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtFromBill)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtOrderno)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel2)).EndInit();
            //this.Panel2.ResumeLayout(false);
            //this.Panel2.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.txtSalesPerson)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtRoundoff)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtexpense)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.txtSplDiscAmt)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.Panel3)).EndInit();
            //this.Panel3.ResumeLayout(false);
            //this.Panel3.PerformLayout();
            //((System.ComponentModel.ISupportInitialize)(this.Panel4)).EndInit();
            //this.Panel4.ResumeLayout(false);
            //((System.ComponentModel.ISupportInitialize)(this.FlexSalesDetails)).EndInit();
            //this.ResumeLayout(false);

        }

        #endregion

        private Panel Panel1;
        internal System.Windows.Forms.Label Label2;
        private TextBox txtFromBill;
        private TextBox txtOrderno;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label lblsorder;
        internal System.Windows.Forms.Label label13;
        private Button btnUpdate;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label1;
        private Panel Panel2;
        private TextBox txtSalesPerson;
        private TextBox txtRoundoff;
        private TextBox txtexpense;
        private TextBox txtSplDiscAmt;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label32;
        internal System.Windows.Forms.Label lbltotaldiscount;
        internal System.Windows.Forms.Label label31;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label lblCess;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label labTax;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label lblQty1;
        internal System.Windows.Forms.Label lblcurrentQty;
        internal System.Windows.Forms.Label label9;
        private Panel Panel3;
        internal System.Windows.Forms.Label lblAmount;
        internal System.Windows.Forms.Label lblBillAmt;
        private Button btnOtherDetails;
        private RichTextBox txtRemarks;
        private TextBox txtCustomer;
        private Panel Panel4;
        internal System.Windows.Forms.Button btnSerach;
        internal System.Windows.Forms.Button btnNew;
        internal System.Windows.Forms.Button btnPrint;
        internal System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Button btnSavePrint;
        internal System.Windows.Forms.Button btnSave;
        private ComboBox cmbBillType;
        private ComboBox cmbFormType;
        private ComboBox cmbPriceType;
        private C1.Win.C1FlexGrid.C1FlexGrid FlexSalesDetails;
        private System.Windows.Forms.DateTimePicker dtpDate;
    }
}
