namespace Xpire.Forms
{
    partial class ucParameters
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucParameters));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.CGrid1 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.Window;
            this.btnClose.CausesValidation = false;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(202, 407);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.AutoEllipsis = true;
            this.btnSave.BackColor = System.Drawing.SystemColors.Window;
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(121, 407);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.GroupBox1.Controls.Add(this.CGrid1);
            this.GroupBox1.ForeColor = System.Drawing.Color.DarkRed;
            this.GroupBox1.Location = new System.Drawing.Point(3, 3);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(399, 398);
            this.GroupBox1.TabIndex = 11;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Parameters Details";
            // 
            // CGrid1
            // 
            this.CGrid1.AutoSearch = C1.Win.C1FlexGrid.AutoSearchEnum.FromCursor;
            this.CGrid1.BackColor = System.Drawing.SystemColors.Window;
            this.CGrid1.ColumnInfo = resources.GetString("CGrid1.ColumnInfo");
            this.CGrid1.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
            this.CGrid1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CGrid1.ForeColor = System.Drawing.Color.Black;
            this.CGrid1.HighLight = C1.Win.C1FlexGrid.HighLightEnum.WithFocus;
            this.CGrid1.Location = new System.Drawing.Point(3, 16);
            this.CGrid1.Name = "CGrid1";
            this.CGrid1.Rows.Count = 22;
            this.CGrid1.Rows.MinSize = 20;
            this.CGrid1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CGrid1.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row;
            this.CGrid1.ShowSort = false;
            this.CGrid1.Size = new System.Drawing.Size(393, 376);
            this.CGrid1.Styles = new C1.Win.C1FlexGrid.CellStyleCollection(resources.GetString("CGrid1.Styles"));
            this.CGrid1.TabIndex = 0;
            this.CGrid1.RowColChange += new System.EventHandler(this.CGrid1_RowColChange);
            this.CGrid1.EnterCell += new System.EventHandler(this.CGrid1_EnterCell);
            this.CGrid1.BeforeEdit += new C1.Win.C1FlexGrid.RowColEventHandler(this.CGrid1_BeforeEdit);
            this.CGrid1.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(this.CGrid1_SetupEditor);
            this.CGrid1.ValidateEdit += new C1.Win.C1FlexGrid.ValidateEditEventHandler(this.CGrid1_ValidateEdit);
            this.CGrid1.KeyPressEdit += new C1.Win.C1FlexGrid.KeyPressEditEventHandler(this.CGrid1_KeyPressEdit);
            // 
            // ucParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.GroupBox1);
            this.Name = "ucParameters";
            this.Size = new System.Drawing.Size(406, 438);
            this.Load += new System.EventHandler(this.ucParameters_Load);
            this.GroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal C1.Win.C1FlexGrid.C1FlexGrid CGrid1;
        public System.Windows.Forms.Button btnClose;
    }
}
