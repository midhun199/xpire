using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using System.Diagnostics;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Management;

using System.Net;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Forms
{

    public partial class ucRegister : UserControl
    {
        public ucRegister()
        {
            InitializeComponent();

           
        }

        string id = string.Empty;

        string sqlQuery = string.Empty, nxtPrv = string.Empty;

        int recNo = 0;
        decimal oldOp = 0, opQty = 0;

        Boolean searchClick = false, autoCode = false, EscKey = false;

        fssgen.fssgen fGen = new fssgen.fssgen();
        cGeneral gen = new cGeneral();
        SqlCommand cmd = new SqlCommand(string.Empty, cPublic.Connection);
        bool isrenewed=false;
        string fileName = string.Empty;
        string sourcePath = string.Empty;
        string targetPath = string.Empty;

        string sourceFile = string.Empty;
        public string destFile = string.Empty;

        internal void ClearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);


            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.IsReadOnly = false;
                //FileStream stream = null;

                //try
                //{
                //    using (stream = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read, FileShare.None))
                //    {
                //        // File/Stream manipulating code here
                //    }
                //}
                //catch (IOException)
                //{
                   
                  
                //}
                //finally
                //{
                //    if (stream != null)
                //        stream.Close();
                //}


                fi.Delete();
            }

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                ClearFolder(di.FullName);
                di.Delete();
            }
        }
        private Boolean LoadDetails(SqlCommand cmd)
        {
            //CSP s = new CSP();
            //s.job();

            ClearFolder(Application.StartupPath + @"\Files");

            Boolean status = true;
            SqlDataReader dr;
            cmd.Connection = cPublic.Connection;
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                id = dr["id"].ToString(); 
                txtName.Text = dr["name"].ToString();
                txtDocumentNo.Text = dr["DocumentNo"].ToString();
                dtpIssue.Text = dr["IssDate"].ToString();
                dtpExpiry.Text = dr["ExpDate"].ToString();

                txtAttachment.Text = dr["filename"].ToString();

                object theValue = dr["attachment"];

                if (DBNull.Value != theValue)
                {
                    byte[] data = (byte[])dr["attachment"];
                    //MemoryStream ms = new MemoryStream(data);
                    //pictureBox1.Image = Image.FromStream(ms);
                    //item1.SubItems.Add(data.ToString());
                    string temfilename = Application.StartupPath + @"\Files\" + @dr["filename"].ToString();

                    if (!File.Exists(temfilename))
                    {


                        using (System.IO.FileStream fs = new System.IO.FileStream(temfilename, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite))

                        {
                            fs.Write(data, 0, data.Length);
                            fs.Flush();
                            fs.Close();


                        }
                    }
                }
                txtPlace.Text = dr["PlaceofIss"].ToString();

                string[] words = dr["email"].ToString().Split(';');

                txtEmail1.Text = string.Empty;
                txtEmail2.Text = string.Empty;
                txtEmail3.Text = string.Empty;
                txtEmail4.Text = string.Empty;
                txtEmail5.Text = string.Empty;
                txtEmail6.Text = string.Empty;


                if (words.Length > 0)
                    txtEmail1.Text = words[0];
                if (words.Length >1)
                    txtEmail2.Text = words[1];
                if (words.Length>2)
                    txtEmail3.Text = words[2];
                if (words.Length > 3)
                    txtEmail4.Text = words[3];
                if (words.Length > 4)
                    txtEmail5.Text = words[4];
                if (words.Length > 5)
                    txtEmail6.Text = words[5];

                //chkRenewed.Checked = Convert.ToBoolean(dr["Renewal"].ToString());                this.txtDocumentType.Text = dr["details"].ToString();
                this.txtDocumentType.Tag = dr["code"].ToString();
                this.txtDocumentType.Text = dr["details"].ToString();
                this.Tag = "Data Loaded";

            }
            else { status = false; }
            dr.Close();

            gen.EDControls(groupBox1, false);
            gen.EDControls(groupBox2, false);
            gen.EDControls(groupBox3, false);
            gen.EDControls(groupBox4, false);
            int count = getItemCount();
            if (nxtPrv.Equals("next") && !status)
            {
                nxtPrv = string.Empty;
            }
            else if (nxtPrv.Equals("previous") && !status)
            {
                nxtPrv = string.Empty;
            }
            if (count == 0)
            { btnEdit.Enabled = false; btnDelete.Enabled = false; }
            return status;
        }

        private int getItemCount()
        {
            sqlQuery = "select  isnull(count(*),0) from Details" + cPublic.g_firmcode;
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            return Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }

        //private void loadDoucmentType()
        //{
        //    sqlQuery = "select * from DocumentType" + cPublic.g_firmcode + " order by DocumentName";
        //    cmd = new SqlCommand(sqlQuery, cPublic.Connection);
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);
        //    cmbType.DataSource = ds.Tables[0];
        //    cmbType.DisplayMember = "DocumentName";
        //    cmbType.ValueMember = "id";
        //}

        private void loadFirstRecord()
        {
            sqlQuery = "select top 1 a.id,a.Name,a.DocumentNo,a.IssDate,a.ExpDate,a.PlaceofIss,a.Email,b.Code,b.Details,ISNULL(a.filename,d.FileName) filename,ISNULL(a.Attachment,d.Attachment) Attachment from Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code left outer join [dbo].[Docref" + cPublic.g_firmcode + "] c on c.docid=a.id left outer join [dbo].[EmployeeDocument" + cPublic.g_firmcode+"] d on d.id =c.empid  order by a.Name ";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            LoadDetails(cmd);
        }

        private void EnableButtons(Boolean status)
        {
            btnSearch.Enabled = status;
            btnNext.Enabled = status;
            btnPrevious.Enabled = status;
            btnEdit.Enabled = status;
            btnDelete.Enabled = status;
            btnAdd.Enabled = status;
            btnExit.Enabled = status;
            btnBrowse.Enabled = status;
        }

        private void loadDefultValues()
        {
           
            id = string.Empty;
            txtName.Text = string.Empty;
            txtDocumentType.Text = string.Empty;
            dtpIssue.Value = System.DateTime.Now;
            dtpExpiry.Value = System.DateTime.Now;
            txtDocumentNo.Text = string.Empty;
            txtEmail1.Text = string.Empty;
            txtEmail2.Text = string.Empty;
            txtEmail3.Text = string.Empty;
            txtEmail4.Text = string.Empty;
            txtEmail5.Text = string.Empty;
            txtEmail6.Text = string.Empty;
            txtAttachment.Text = string.Empty;
            txtPlace.Text = string.Empty;
        }

        private Boolean isItmPossibleForDelete()
        {
            Boolean status = true;

            string itmCode = id.Trim();
            if (isItemPresent("Prior" + cPublic.g_firmcode, id))
            { status = false; }
          

            return status;
        }


       

        private Boolean isItemPresent(string tableName, string itmCode)
        {
            Boolean status = false;
            sqlQuery = "select id from " + tableName + " where detid = @itemCode and sent=1";
            SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@itemCode", id);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
                status = true;
            dr.Close();
            return status;
        }

        private void LoadLookup(TextBox txtBx, string option, string start)
        {
            frmlookup lookup = new frmlookup();
            lookup.m_con = cPublic.Connection;

            lookup.m_condition = "Field1 = '" + option + "'";
            lookup.m_table = "LOOKUP";
            lookup.m_fields = "Code,Details";
            lookup.m_dispname = "Code,Details";
            lookup.m_fldwidth = "75,150";
            lookup.m_strat = start;
            lookup.ShowDialog();
            if (lookup.m_values.Count > 0)
            {
                txtBx.Text = lookup.m_values[0].ToString();
                txtBx.Tag = lookup.m_values[1].ToString();

                SendKeys.Send("{TAB}");
            }
        }

        private void NumberValidation(TextBox sender)
        {
            if (sender.Text.Trim().Equals(string.Empty) || sender.Text.Equals("."))
            { sender.Text = "0.00"; }
            else
            { sender.Text = Convert.ToDecimal(sender.Text.Trim()).ToString("##########0.00"); }
        }

        public Boolean Validation()
        {
            Boolean status = true;
            if (txtDocumentType.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("TYPE cannot be Blank....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDocumentType.Focus();
            }
            else if (txtName.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Item Name cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtName.Focus();
            }
            //else if (DocNameExists("Details" + cPublic.g_firmcode, txtName.Text.Trim()))
            //{
            //    status = false;
            //    MessageBox.Show("Document Name Exists.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    txtName.Focus();
            //}
            else if (txtDocumentNo.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Document Number cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDocumentNo.Focus();
            }
             
            else if (txtPlace.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Place Of Issue cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPlace.Focus();
            }
            else if (txtAttachment.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Attachment cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtAttachment.Focus();
            }
            else if (txtEmail1.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Email cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEmail1.Focus();
            }
            else if (!(txtEmail1.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtEmail1.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail1.Focus();
                }
            }
            if (!(txtEmail2.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtEmail2.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail2.Focus();
                }
            }
            if (!(txtEmail3.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtEmail3.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail3.Focus();
                }
            }
            if (!(txtEmail4.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtEmail4.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail4.Focus();
                }
            }
            if (!(txtEmail5.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtEmail5.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail5.Focus();
                }
            }
            if (!(txtEmail6.Text.Trim().Equals(string.Empty)))
            {
                if (!varification(txtEmail6.Text.Trim()))
                {
                    status = false;
                    MessageBox.Show("Invalid Email Format.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail6.Focus();
                }
            }
           

            return status;
        }

         
        public bool varification(string verify)
        {
            return Regex.IsMatch(verify, "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
        }
           

        private Boolean isInLookUP(string field, string code)
        {
            Boolean status = false;
            sqlQuery = "select * from LOOKUP where field1 = @field and Code = @code";
            SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@field", field);
            cmd.Parameters.AddWithValue("@code", code);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
                status = true;
            dr.Close();
            return status;
        }
        private void InsertCode()
        {
            SqlCommand cmd = new SqlCommand("SP_INSERT_DETAILS" + cPublic.g_firmcode, cPublic.Connection);
            cmd.CommandType = CommandType.StoredProcedure;
            if (autoCode)
            {
                id = cmd.ExecuteScalar().ToString();
                if (id == string.Empty)
                {
                    MessageBox.Show("Item Code Series is Maximum. Please Contact your Software Vender.. ", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    sqlQuery = "select * from Details" + cPublic.g_firmcode + " where id = '9999999'";
                    cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                    LoadDetails(cmd);
                    EnableButtons(true);

                    btnAdd.Tag = "&Add";
                    btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                    btnEdit.Tag = "&Edit";
                    btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                    btnExit.Tag = "E&xit";
                    btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                    return;
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@id", id.Trim());
                cmd.Connection = cPublic.Connection;
                cmd.CommandText = "SP_INSERT_DETAILS" + cPublic.g_firmcode;
                cmd.ExecuteNonQuery();
            }



            gen.EDControls(groupBox1, true);
            gen.EDControls(groupBox2, true);
            gen.EDControls(groupBox3, true);
            gen.EDControls(groupBox4, true);
           
        }

        private Boolean ItmCodeVaidation()
        {
            bool status = true;
            if (!btnAdd.Tag.Equals("&Add"))
            {
                sqlQuery = "select * from Details" + cPublic.g_firmcode + " where id = @itemCode";
                SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@itemCode", id.Trim());
                if (LoadDetails(cmd)) { status = false; }
            }
            return status;
        }
        string email;
        private const string SAVE_DIR = @"E:";

        public static string dest;

        public void CopyFile(string attachment)
        {
            try
            {

                fileName = Path.GetFileName(attachment);

                sourcePath = Path.GetDirectoryName(attachment);

                sourceFile = System.IO.Path.Combine(sourcePath, fileName);


                foreach (Process proc in Process.GetProcessesByName(fileName))
                {
                    if (proc.ProcessName.Contains(attachment))
                    {
                        proc.Kill();
                    }
                }


                if (Environment.MachineName.ToUpper() == cPublic.Servermachinename.ToUpper())
                {
                    if (!Directory.Exists(cPublic.drive + "Xpire"))
                    {
                        Directory.CreateDirectory(cPublic.drive + "Xpire");
                        DirectorySecurity sec = Directory.GetAccessControl(cPublic.drive + "Xpire");
                        SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                        sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                        Directory.SetAccessControl(cPublic.drive + "Xpire", sec);
                        destFile = System.IO.Path.Combine(cPublic.drive + "Xpire", fileName);

                        dest = destFile;
                        System.IO.File.Copy(sourceFile, destFile, true);

                    }
                    else
                    {

                        DirectorySecurity sec = Directory.GetAccessControl(cPublic.drive + "Xpire");
                        SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);

                        sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                        Directory.SetAccessControl(cPublic.drive + "Xpire", sec);
                        destFile = System.IO.Path.Combine(cPublic.drive + "Xpire", fileName);
                        dest = destFile;
                        if (!File.Exists(destFile))
                        {
                            System.IO.File.Copy(sourceFile, destFile, true);
                        }
                    }

                }
                else
                {
                    try
                    {
                        destFile = System.IO.Path.Combine("\\\\192.168.1.146\\Xpire", fileName);
                        dest = destFile;
                        string domainname = System.Environment.UserDomainName;
                        NetworkCredential myCredentials;
                        if (domainname == "Lenovo-PC")
                        {
                            myCredentials = new NetworkCredential("Lenovo-PC\\cityexpertssoft", "123456");
                            //using (new NetworkConnection(@"\\server", myCredentials))
                            using (new NetworkConnection(@"\\192.168.1.146", myCredentials))
                            {
                                ////System.IO.File.Copy(sourceFile, destFile, true);
                                //if ((!Directory.Exists("\\\\192.168.1.146\\Xpire")))
                                //{


                                //    //string activeDir = "\\\\server\\d3\\";
                                //    //string username = "FOURSQUARE\\mithun";
                                //    //string password = "mithundas";

                                //    //AddConnection(activeDir, username, password);

                                //    //string newPath = System.IO.Path.Combine(activeDir, userID);

                                //    //System.IO.DriveInfo di = new System.IO.DriveInfo("\\\\server\\E\\");
                                //    System.IO.Directory.CreateDirectory("\\\\192.168.1.138\\C\\Xpire");

                                //    //Directory.CreateDirectory("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Maxx");
                                //    //SaveACopyfileToServer(fileName, "\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Users");

                                //    //DirectorySecurity sec = Directory.GetAccessControl("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Users");
                                //    //SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                                //    //sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                //    //Directory.SetAccessControl(System.IO.Path.Combine("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Users", fileName), sec);
                                //    destFile = System.IO.Path.Combine("\\\\192.168.1.138\\C\\Xpire", fileName);
                                //    dest = destFile;
                                //    System.IO.File.Copy(sourceFile, destFile, true);

                                //}
                                if ((!File.Exists(destFile)))
                                {
                                    //DirectorySecurity sec = Directory.GetAccessControl(targetPath);
                                    //SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                                    //sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                    //Directory.SetAccessControl(System.IO.Path.Combine("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Users", fileName), sec);

                                    System.IO.File.Copy(sourceFile, destFile, true);
                                }
                            }
                        }
                        else
                        {

                            if ((!Directory.Exists("\\\\192.168.1.146\\C\\Xpire")))
                            {


                                //string activeDir = "\\\\server\\d3\\";
                                //string username = "FOURSQUARE\\mithun";
                                //string password = "mithundas";

                                //AddConnection(activeDir, username, password);

                                //string newPath = System.IO.Path.Combine(activeDir, userID);

                                //System.IO.DriveInfo di = new System.IO.DriveInfo("\\\\server\\E\\");
                                System.IO.Directory.CreateDirectory("\\\\192.168.1.146\\C\\Xpire");

                                //Directory.CreateDirectory("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Maxx");
                                //SaveACopyfileToServer(fileName, "\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Users");

                                //DirectorySecurity sec = Directory.GetAccessControl("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Users");
                                //SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                                //sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                //Directory.SetAccessControl(System.IO.Path.Combine("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Users", fileName), sec);
                                destFile = System.IO.Path.Combine("\\\\192.168.1.138\\C\\Xpire", fileName);
                                dest = destFile;
                                System.IO.File.Copy(sourceFile, destFile, true);

                            }
                            else if ((!File.Exists(destFile)))
                            {
                                //DirectorySecurity sec = Directory.GetAccessControl(targetPath);
                                //SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                                //sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                                //Directory.SetAccessControl(System.IO.Path.Combine("\\\\" + cPublic.Servermachinename + "\\" + cPublic.drive.Substring(0, 1) + "\\" + "Users", fileName), sec);

                                System.IO.File.Copy(sourceFile, destFile, true);
                            }

                        }


                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                        return;
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public static void AddConnection(string resource, string username, string password)
        {
            NETRESOURCE nr = new NETRESOURCE();
            nr.RemoteName = resource;
            uint err = WNetAddConnection2W(ref nr, password, username, 0);
            //if (err != 0)
            //    throw new RemoteDirectoryException(string.Format("WNetAddConnection2 failed with error: #{0}", err));
        }

        private struct NETRESOURCE
        {
            public uint Scope;
            public uint Type;
            public uint DisplayType;
            public uint Usage;
            public string LocalName;
            public string RemoteName;
            public string Comment;
            public string Provider;
        }

        [DllImport("mpr.dll", CharSet = CharSet.Unicode)]
        private extern static uint WNetAddConnection2W(ref NETRESOURCE lpNetResource, string lpPassword, string lpUsername, uint dwFlags);

        private void Items(SqlCommand cmd)
        {
            cmd.Parameters.Clear();

            //CopyFile(txtAttachment.Text);
            FileStream fStream = File.OpenRead(txtAttachment.Text);

            byte[] contents = new byte[fStream.Length];

            fStream.Read(contents, 0, (int)fStream.Length);

            fStream.Close();

            FileInfo fi = new FileInfo(txtAttachment.Text);

           

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id.Trim());
            cmd.Parameters.AddWithValue("@name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@issuedate", Convert.ToDateTime(dtpIssue.Value));

            cmd.Parameters.AddWithValue("@expirydate", Convert.ToDateTime(dtpExpiry.Value));
            cmd.Parameters.AddWithValue("@docno", txtDocumentNo.Text.Trim());
            cmd.Parameters.AddWithValue("@filename", fi.Name);
            cmd.Parameters.AddWithValue("@attachment", contents);
            cmd.Parameters.AddWithValue("@doctype", txtDocumentType.Tag);
            cmd.Parameters.AddWithValue("@userid", gen.GetUserDet());
            cmd.Parameters.AddWithValue("@PlaceofIss", txtPlace.Text);

            if (!string.IsNullOrEmpty(txtEmail1.Text.Trim()))
            {
                email = txtEmail1.Text.Trim();
            }
            if (!string.IsNullOrEmpty(txtEmail2.Text.Trim()))
            {
                email = email+";"+txtEmail2.Text.Trim();
            }
            if (!string.IsNullOrEmpty(txtEmail3.Text.Trim()))
            {
                email = email + ";" + txtEmail3.Text.Trim();
            }
            if (!string.IsNullOrEmpty(txtEmail4.Text.Trim()))
            {
                email = email + ";" + txtEmail4.Text.Trim();
            }
            if (!string.IsNullOrEmpty(txtEmail5.Text.Trim()))
            {
                email = email + ";" + txtEmail5.Text.Trim();
            }
            if (!string.IsNullOrEmpty(txtEmail6.Text.Trim()))
            {
                email = email + ";" + txtEmail6.Text.Trim();
            }
               cmd.Parameters.AddWithValue("@email", email);

               if (chkRenewed.Checked == true)
               {
                   isrenewed = true;
               }
               else
                   isrenewed = false;

               //cmd.Parameters.AddWithValue("@isrenewal", isrenewed);
        }

        

        public Boolean UpdateItem()
        {
            SqlTransaction sqlTrans = null;
            Boolean status = true;

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;


                cmd.Parameters.Clear();
                cmd.CommandText = "SP_UPDATE_DETAILS" + cPublic.g_firmcode;

                Items(cmd);

                cmd.ExecuteNonQuery();

                sqlQuery = "select a.id,a.Name,a.DocumentNo,a.IssDate,a.ExpDate,a.PlaceofIss,a.Email,b.Code,b.Details,ISNULL(a.filename,d.FileName) filename,ISNULL(a.Attachment,d.Attachment) Attachment from Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code left outer join [dbo].[Docref" + cPublic.g_firmcode + "] c on c.docid=a.id left outer join [dbo].[EmployeeDocument" + cPublic.g_firmcode + "] d on d.id =c.empid   where a.id = @id";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@id", id.Trim());
                LoadDetails(cmd);
            }
            catch (Exception ex)
            {
                sqlTrans.Rollback();
                MessageBox.Show(ex.Message);
                status = false;
            }
            return status;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchClick = true;
            btnExit.Tag = "&Cancel";
            btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
            gen.ClearFieldsInMe(this);
            txtName.Enabled = true;
            txtName.Focus();
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnNext.Enabled = false;
            btnPrevious.Enabled = false;
            msgLabel.Text = "Press  F5  for Select an Item.";
            msgLabel.ForeColor = System.Drawing.Color.MidnightBlue;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            nxtPrv = "previous";
            msgLabel.Text = string.Empty;

            sqlQuery = "select top 1 a.id,a.Name,a.DocumentNo,a.IssDate,a.ExpDate,a.PlaceofIss,a.Email,b.Code,b.Details,ISNULL(a.filename,d.FileName) filename,ISNULL(a.Attachment,d.Attachment) Attachment from Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code left outer join [dbo].[Docref" + cPublic.g_firmcode + "] c on c.docid=a.id left outer join [dbo].[EmployeeDocument" + cPublic.g_firmcode + "] d on d.id =c.empid  where a.id < @id"
            + "  order by id desc";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@id", id.Trim());
            LoadDetails(cmd);
        }

        private void ucStockMaster_Load(object sender, EventArgs e)
        {
            msgLabel.BorderStyle = BorderStyle.None;
            lblItemCount.BorderStyle = BorderStyle.None;
            
           
            this.Tag = "No Records";
           
            loadFirstRecord();
            lblItemCount.Text = "No of Items : " + getItemCount().ToString();

            if (this.Tag.Equals("No Records"))
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            nxtPrv = "next";
            msgLabel.Text = string.Empty;

            sqlQuery = "select top 1 a.id,a.Name,a.DocumentNo,a.IssDate,a.ExpDate,a.PlaceofIss,a.Email,b.Code,b.Details,ISNULL(a.filename,d.FileName) filename,ISNULL(a.Attachment,d.Attachment) Attachment from Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code left outer join [dbo].[Docref" + cPublic.g_firmcode + "] c on c.docid=a.id left outer join [dbo].[EmployeeDocument" + cPublic.g_firmcode + "] d on d.id =c.empid  where a.id > @id"
            + "  order by a.id ";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@id", id.Trim());
         
            LoadDetails(cmd);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            switch (btnAdd.Tag.ToString())
            {
                case "&Add":
                    oldOp = 0;
                    btnAdd.Tag = "&Save";
                    btnAdd.BackgroundImage = global::Xpire.Properties.Resources.save1;
                    btnExit.Tag = "&Cancel";
                    btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
                    msgLabel.Text = string.Empty;
                    gen.EDControls(this, true);
                  

                    EnableButtons(false);
                    btnAdd.Enabled = true;
                    btnExit.Enabled = true;
                    btnBrowse.Enabled = true;

                    loadDefultValues();
                  

                   
                    break;
                case "&Save":
                    if (Validation())
                    {
                        autoCode = true;
                         SqlCommand cmd = new SqlCommand("SP_INSERT_DETAILS" + cPublic.g_firmcode, cPublic.Connection);
                   cmd.CommandType = CommandType.StoredProcedure;
                        
                
               
                cmd.Connection = cPublic.Connection;


                cmd.Parameters.Clear();
               

                Items(cmd);

                id = cmd.ExecuteScalar().ToString();

                //if (btnEdit.Tag == "&Update")
                //{ cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(txtCurStock.Text) - opQty + Convert.ToDecimal(txtOPStock.Text)); }
                //else { cmd.Parameters.AddWithValue("@qty", Convert.ToDecimal(txtOPStock.Text.Trim())); }


                sqlQuery = "select a.id,a.Name,a.DocumentNo,a.IssDate,a.ExpDate,a.PlaceofIss,a.Email,b.Code,b.Details,ISNULL(a.filename,d.FileName) filename,ISNULL(a.Attachment,d.Attachment) Attachment from Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code left outer join [dbo].[Docref" + cPublic.g_firmcode + "] c on c.docid=a.id left outer join [dbo].[EmployeeDocument" + cPublic.g_firmcode + "] d on d.id =c.empid  where a.id = @id";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@id", id.Trim());
                LoadDetails(cmd);
                        autoCode = false;
                            lblItemCount.Text = "No of Items : " + getItemCount().ToString();
                            MessageBox.Show("New Item Saved Successfully......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            EnableButtons(true);

                            autoCode = false;
                            btnAdd.Tag = "&Add";
                            btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                       
                    }
                    break;
            }
        }

        private Boolean DocNameExists(string tableName, string docname)
        {
            Boolean status = false;
            sqlQuery = "select id from " + tableName + " where name=@docname ";
            SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@docname", docname);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
                status = true;
            dr.Close();
            return status;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            //if (this.Tag.Equals("Data Loaded"))
            //{
                switch (btnEdit.Tag.ToString())
                {
                    case "&Edit":
                        if (id.Trim() != string.Empty && txtName.Text.Trim() == string.Empty)
                        { MessageBox.Show("You are not Allowed to Edit this Item..."); return; }
                        if (!isItmPossibleForDelete())
                    { MessageBox.Show("You are not Allowed to Edit this Item..."); return; }
                    btnEdit.Tag = "&Update";
                        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.save1;
                        btnExit.Tag = "&Cancel";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
                        msgLabel.Text = string.Empty;
                        gen.EDControls(this, true);
                       
                        EnableButtons(false);
                        btnEdit.Enabled = true;
                        btnExit.Enabled = true;
                        btnBrowse.Enabled = true;


                       

                        //if (Convert.ToInt32(cPublic.g_UserLevel) < 8) { txtOPStock.Enabled = false; } else { txtOPStock.Enabled = true; }


                      

                        break;
                    case "&Update":
                        if (Validation())
                        {
                            if (UpdateItem())
                            {
                                MessageBox.Show("Item Details Updated Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                EnableButtons(true);

                                btnEdit.Tag = "&Edit";
                                btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                                btnExit.Tag = "E&xit";
                                btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;


                            }
                            else { MessageBox.Show("Invalid Data.. Please Check...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error); }
                        }
                        break;

                }
            //}
            //else { MessageBox.Show("You have to Select an Item Before Edit...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information); }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to Delete this ITEM...", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(DialogResult.Yes))
            {
                //if (isItmPossibleForDelete())
                //{
                    SqlCommand cmd = new SqlCommand("SP_DELETE_DETAILS" + cPublic.g_firmcode, cPublic.Connection);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", id.Trim());
                    cmd.ExecuteNonQuery();

                    try
                    {
                        int count = getItemCount();
                        lblItemCount.Text = "No of Items : " + count.ToString();

                        nxtPrv = "next";
                        msgLabel.Text = string.Empty;
                        cmd.Connection = cPublic.Connection;
                        sqlQuery = "select top 1 a.id,a.Name,a.DocumentNo,a.IssDate,a.ExpDate,a.PlaceofIss,a.Email,b.Code,b.Details,ISNULL(a.filename,d.FileName) filename,ISNULL(a.Attachment,d.Attachment) Attachment from Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code left outer join [dbo].[Docref" + cPublic.g_firmcode + "] c on c.docid=a.id left outer join [dbo].[EmployeeDocument" + cPublic.g_firmcode + "] d on d.id =c.empid  where a.id > @id"
                        + " order by id ";
                        cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                        cmd.Parameters.AddWithValue("@id", id.Trim());
                       
                        LoadDetails(cmd);


                        if (count == 0) { loadDefultValues(); }
                        MessageBox.Show("Record Deleted Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    { MessageBox.Show(ex.Message); }
                //}
                //else
                //{
                //    MessageBox.Show("You cannot Delete this Item... A Transaction found in " + cPublic.g_firmname + "");
                //}
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            switch (btnExit.Tag.ToString())
            {
                case "E&xit":
                    this.Dispose();
                    break;
                case "&Cancel":
                    if ((MessageBox.Show("Are You Sure to Cancel...?  ", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question)).Equals(DialogResult.Yes))
                    {
                        if (btnAdd.Tag.Equals("&Save"))
                        {
                            //SqlCommand cmd = new SqlCommand("SP_DELETE_STOCKMST" + cPublic.g_firmcode, cPublic.Connection);
                            //cmd.CommandType = CommandType.StoredProcedure;
                            //cmd.Parameters.AddWithValue("@itemcode", id.Trim());
                            //cmd.ExecuteNonQuery();
                        }

                        gen.ClearFieldsInMe(this);
                        EnableButtons(true);

                        btnAdd.Tag = "&Add";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                        btnEdit.Tag = "&Edit";
                        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        loadDefultValues();
                        loadFirstRecord();
                        searchClick = false;
                        autoCode = false;

                    }
                    EscKey = false;
                    msgLabel.Text = string.Empty;
                    break;
            }
        }

        private void txtDocumentType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                Cust_LookUp(sender, e, "DOCUMENT TYPE", (TextBox)txtDocumentType);

            }
            if (e.KeyCode == Keys.Delete)
            {
                txtDocumentType.Tag = string.Empty;
            }
        }
        private void txtItmCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchClick)
            {
                if (e.KeyCode.Equals(Keys.F5))
                {
                    Cust_LookUp(sender, e, "DETAILS", (TextBox)txtName);
                }
            }
            if (e.KeyCode == Keys.Delete)
            {
                txtName.Tag = string.Empty;
            }
        }
 
        private void txtEmail1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail1);
            }
            if (e.KeyCode == Keys.Delete)
            {
                txtEmail1.Tag = string.Empty;
            }
        }

        private void txtEmail2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail2);
            }
            if (e.KeyCode == Keys.Delete)
            {
                txtEmail2.Tag = string.Empty;
            }
        }

        private void txtEmail3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail3);
            }
            if (e.KeyCode == Keys.Delete)
            {
                txtEmail3.Tag = string.Empty;
            }
        }

        private void txtEmail4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail4);
            }
            if (e.KeyCode == Keys.Delete)
            {
                txtEmail4.Tag = string.Empty;
            }
        }

        private void txtEmail5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail5);
            }
            if (e.KeyCode == Keys.Delete)
            {
                txtEmail5.Tag = string.Empty;
            }
        }

        private void txtEmail6_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail6);
            }
            if (e.KeyCode == Keys.Delete)
            {
                txtEmail6.Tag = string.Empty;
            }
        }
        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (searchClick)
            {
                if (e.KeyChar != Convert.ToChar(Keys.Enter))
                    if (txtName.Text.Length == 0)
                        if (e.KeyChar == 27)
                        { }
                        else if (txtName.Text == "")
                            if (e.KeyChar != Convert.ToChar(Keys.Back))
                                Cust_LookUp(sender, e, "DETAILS", (TextBox)txtName);
                e.Handled = true;
            }
        }


        private void txtDocumentType_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtDocumentType.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
                    else if (txtDocumentType.Text == "")
                        if (e.KeyChar != Convert.ToChar(Keys.Back))
                            Cust_LookUp(sender, e, "DOCUMENT TYPE", (TextBox)txtDocumentType);
            e.Handled = true;
        }

        private void txtEmail1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtEmail1.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
                    else if (txtEmail1.Text == "")
                        if (e.KeyChar != Convert.ToChar(Keys.Back))
                            Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail1);
            e.Handled = true;
        }

        private void txtEmail2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtEmail2.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
                    else if (txtEmail2.Text == "")
                        if (e.KeyChar != Convert.ToChar(Keys.Back))
                            Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail2);
            e.Handled = true;
        }

        private void txtEmail3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtEmail3.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
                    else if (txtEmail3.Text == "")
                        if (e.KeyChar != Convert.ToChar(Keys.Back))
                            Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail3);
            e.Handled = true;
        }

        private void txtEmail4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtEmail4.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
                    else if (txtEmail4.Text == "")
                        if (e.KeyChar != Convert.ToChar(Keys.Back))
                            Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail4);
            e.Handled = true;
        }

        private void txtEmail5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtEmail5.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
                    else if (txtEmail5.Text == "")
                        if (e.KeyChar != Convert.ToChar(Keys.Back))
                            Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail5);
            e.Handled = true;
        }

        private void txtEmail6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != Convert.ToChar(Keys.Enter))
                if (txtEmail6.Text.Length == 0)
                    if (e.KeyChar == 27)
                    { }
                    else if (txtEmail6.Text == "")
                        if (e.KeyChar != Convert.ToChar(Keys.Back))
                            Cust_LookUp(sender, e, "EMAIL", (TextBox)txtEmail6);
            e.Handled = true;
        }

        private void Cust_LookUp(object sender, EventArgs e, string Field, TextBox txtboxname)
        {
            if (Field != "DETAILS")
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;
                if ((e.GetType().ToString() == typeof(KeyEventArgs).ToString()))
                {
                    lookup.m_strat = "";
                }
                else
                {
                    lookup.m_strat = ((KeyPressEventArgs)e).KeyChar.ToString();
                    ((KeyPressEventArgs)e).KeyChar = Convert.ToChar(Keys.None);
                }

                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = '" + Field + "'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Details,Code";
                lookup.m_dispname = "Details,Code";
                lookup.m_fldwidth = "150,75";
                //lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtboxname.Text = lookup.m_values[0].ToString();
                    txtboxname.Tag = lookup.m_values[1].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
            else
            {
                frmlookup lookup = new frmlookup();
                string fields = string.Empty;
                string dispName = string.Empty;
                string width = string.Empty;
                if ((e.GetType().ToString() == typeof(KeyEventArgs).ToString()))
                {
                    lookup.m_strat = "";
                }
                else
                {
                    lookup.m_strat = ((KeyPressEventArgs)e).KeyChar.ToString();
                    ((KeyPressEventArgs)e).KeyChar = Convert.ToChar(Keys.None);
                }
                fields = "Name,DocumentNo,id";
                dispName = "Name,Document No,ID";
                width = "350,100,0";

               
                lookup.m_table = "DETAILS" + cPublic.g_firmcode;
                lookup.m_fields = fields;
                lookup.m_dispname = dispName;
                lookup.m_fldwidth = width;
                lookup.m_con = cPublic.Connection;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {

                    id = lookup.m_values[2].ToString();
                    txtName.Text = lookup.m_values[0].ToString();

                    sqlQuery = "select a.id,a.Name,a.DocumentNo,a.IssDate,a.ExpDate,a.PlaceofIss,a.Email,b.Code,b.Details,ISNULL(a.filename,d.FileName) filename,ISNULL(a.Attachment,d.Attachment) Attachment from Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code left outer join [dbo].[Docref" + cPublic.g_firmcode + "] c on c.docid=a.id left outer join [dbo].[EmployeeDocument" + cPublic.g_firmcode + "] d on d.id =c.empid  where a.id = @id";
                    SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                    cmd.Parameters.AddWithValue("@id", id.Trim());
                    LoadDetails(cmd);
                    btnAdd.Enabled = true;
                    btnEdit.Enabled = true;
                    btnDelete.Enabled = true;
                    btnNext.Enabled = true;
                    btnPrevious.Enabled = true;
                    btnExit.Tag = "E&xit";
                    btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                    searchClick = false;
                }
            }

        }
        private void txtItmCode_Validating(object sender, CancelEventArgs e)
        {
            if (!gen.isFormClosing() && !EscKey)
            {
                if (searchClick)
                {
                    if (id.Trim() != string.Empty)
                    {
                        sqlQuery = "select TOP 1 * from STOCKMST" + cPublic.g_firmcode + " where itemcode like @itemCode order by itemcode ";
                        SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                        cmd.Parameters.AddWithValue("@itemCode", id.Trim() + "%");
                        if (!LoadDetails(cmd))
                        {
                            MessageBox.Show("Item Code not Found.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                           
                        }
                        else
                        {
                            btnAdd.Enabled = true;
                            btnEdit.Enabled = true;
                            btnDelete.Enabled = true;
                            btnNext.Enabled = true;
                            btnPrevious.Enabled = true;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                            searchClick = false;
                        }
                    }

                }


                else if (btnAdd.Tag.Equals("&Save"))
                {
                    if (id.Trim() == "")
                    {
                        autoCode = true;
                        InsertCode();
                        autoCode = false;
                    }
                    else if (!ItmCodeVaidation())
                    {
                        MessageBox.Show("Choose another Item Code.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        EnableButtons(true);
                       
                        btnAdd.Tag = "&Add";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                    }
                    else
                    {
                        InsertCode();
                        txtName.Focus();
                    }
                }
            }
        }

        private void txtItmName_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.SelectAll();
            msgLabel.Text = string.Empty;
        }

        private void txtUnit_Enter(object sender, EventArgs e)
        {
            //msgLabel.Text = "Press  F5  for Select Unit";
            //msgLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            //txtManufacturer.SelectAll();
            //if (txtManufacturer.Text.Trim() == "")
            //{
            //    TextBox tb = (TextBox)sender;
            //    LoadLookup(tb, "UNIT", string.Empty);
            //}
        }

        private void txtManufacturer_Enter(object sender, EventArgs e)
        {
            msgLabel.Text = "Press  F5  for Select Manufacturer";
            msgLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            //txtManufacturer.SelectAll();
            //if (txtManufacturer.Text.Trim() == "")
            //{
            //    TextBox tb = (TextBox)sender;
            //    LoadLookup(tb, "MANUFACTURER", string.Empty);
            //}
        }

        private void txtProduct_Enter(object sender, EventArgs e)
        {
            //msgLabel.Text = "Press  F5  for Select Product";
            //msgLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            //txtDocumentNo.SelectAll();
            //if (txtDocumentNo.Text.Trim() == "")
            //{
            //    TextBox tb = (TextBox)sender;
            //    LoadLookup(tb, "PRODUCT", string.Empty);
            //}
        }

        private void txtGroup_Enter(object sender, EventArgs e)
        {
            //msgLabel.Text = "Press  F5  for Select Group";
            //msgLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            //txtGroup.SelectAll();
            //if (txtGroup.Text.Trim() == "")
            //{
            //    TextBox tb = (TextBox)sender;
            //    LoadLookup(tb, "GROUP", string.Empty);
            //}
        }

        private void txtCommodity_Enter(object sender, EventArgs e)
        {
            //msgLabel.Text = "Press  F5  for Select Commodity.";
            //msgLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            //txtCommodity.SelectAll();
        }

        private void txtSchedule_Enter(object sender, EventArgs e)
        {
            //msgLabel.Text = "Press  F5  for Select Schedule.";
            //msgLabel.ForeColor = System.Drawing.Color.MidnightBlue;
            //txtSchedule.SelectAll();
        }

        private void txtUnit_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode.Equals(Keys.F5))
            //{
            //    TextBox tb = (TextBox)sender;
            //    LoadLookup(tb, "UNIT", string.Empty);
            //}
        }

        private void txtManufacturer_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode.Equals(Keys.F5))
            //{
            //    TextBox tb = (TextBox)sender;
            //    LoadLookup(tb, "MANUFACTURER", string.Empty);
            //}
        }


        private void txtGroup_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode.Equals(Keys.F5))
            //{
            //    TextBox tb = (TextBox)sender;
            //    LoadLookup(tb, "GROUP", string.Empty);
            //}
        }

        private void txtCommodity_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode.Equals(Keys.F5))
            //{
            //    TextBox tb = (TextBox)sender;
            //    LoadLookup(tb, "COMMODITY", string.Empty);
            //}
        }

        private void txtSchedule_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode.Equals(Keys.F5))
            //{
            //    TextBox tb = (TextBox)sender;
            //    LoadLookup(tb, "SCHEDULE", string.Empty);
            //}
        }

        private void txtUnit_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((Strings.Asc(e.KeyChar) >= 97 && Strings.Asc(e.KeyChar) <= 122) || (Strings.Asc(e.KeyChar) >= 65 && Strings.Asc(e.KeyChar) <= 90) || (Strings.Asc(e.KeyChar) >= 48 && Strings.Asc(e.KeyChar) <= 57) || Strings.Asc(e.KeyChar) == 32)
            //{
            //    TextBox tb = (TextBox)sender;
            //    e.Handled = true;
            //    string start = string.Empty;
            //    if (tb.SelectionLength == tb.Text.Length) { start = e.KeyChar.ToString(); }
            //    else { start = e.KeyChar.ToString(); }
            //    LoadLookup(tb, "UNIT", start);
            //}
        }

        private void txtManufacturer_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((Strings.Asc(e.KeyChar) >= 97 && Strings.Asc(e.KeyChar) <= 122) || (Strings.Asc(e.KeyChar) >= 65 && Strings.Asc(e.KeyChar) <= 90) || (Strings.Asc(e.KeyChar) >= 48 && Strings.Asc(e.KeyChar) <= 57) || Strings.Asc(e.KeyChar) == 32)
            //{
            //    TextBox tb = (TextBox)sender;
            //    e.Handled = true;
            //    string start = string.Empty;
            //    if (tb.SelectionLength == tb.Text.Length) { start = e.KeyChar.ToString(); }
            //    else { start = e.KeyChar.ToString(); }
            //    LoadLookup(tb, "MANUFACTURER", start);
            //}
        }

        private void txtProduct_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((Strings.Asc(e.KeyChar) >= 97 && Strings.Asc(e.KeyChar) <= 122) || (Strings.Asc(e.KeyChar) >= 65 && Strings.Asc(e.KeyChar) <= 90) || (Strings.Asc(e.KeyChar) >= 48 && Strings.Asc(e.KeyChar) <= 57) || Strings.Asc(e.KeyChar) == 32)
            //{
            //    TextBox tb = (TextBox)sender;
            //    e.Handled = true;
            //    string start = string.Empty;
            //    if (tb.SelectionLength == tb.Text.Length) { start = e.KeyChar.ToString(); }
            //    else { start = e.KeyChar.ToString(); }
            //    LoadLookup(tb, "PRODUCT", start);
            //}
        }

        private void txtGroup_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((Strings.Asc(e.KeyChar) >= 97 && Strings.Asc(e.KeyChar) <= 122) || (Strings.Asc(e.KeyChar) >= 65 && Strings.Asc(e.KeyChar) <= 90) || (Strings.Asc(e.KeyChar) >= 48 && Strings.Asc(e.KeyChar) <= 57) || Strings.Asc(e.KeyChar) == 32)
            //{
            //    TextBox tb = (TextBox)sender;
            //    e.Handled = true;
            //    string start = string.Empty;
            //    if (tb.SelectionLength == tb.Text.Length) { start = e.KeyChar.ToString(); }
            //    else { start = e.KeyChar.ToString(); }
            //    LoadLookup(tb, "GROUP", start);
            //}
        }

        private void txtCommodity_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((Strings.Asc(e.KeyChar) >= 97 && Strings.Asc(e.KeyChar) <= 122) || (Strings.Asc(e.KeyChar) >= 65 && Strings.Asc(e.KeyChar) <= 90) || (Strings.Asc(e.KeyChar) >= 48 && Strings.Asc(e.KeyChar) <= 57) || Strings.Asc(e.KeyChar) == 32)
            //{
            //    TextBox tb = (TextBox)sender;
            //    e.Handled = true;
            //    string start = string.Empty;
            //    if (tb.SelectionLength == tb.Text.Length) { start = e.KeyChar.ToString(); }
            //    else { start = e.KeyChar.ToString(); }
            //    LoadLookup(tb, "COMMODITY", start);
            //}
        }

        private void txtSchedule_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((Strings.Asc(e.KeyChar) >= 97 && Strings.Asc(e.KeyChar) <= 122) || (Strings.Asc(e.KeyChar) >= 65 && Strings.Asc(e.KeyChar) <= 90) || (Strings.Asc(e.KeyChar) >= 48 && Strings.Asc(e.KeyChar) <= 57) || Strings.Asc(e.KeyChar) == 32)
            //{
            //    TextBox tb = (TextBox)sender;
            //    e.Handled = true;
            //    string start = string.Empty;
            //    if (tb.SelectionLength == tb.Text.Length) { start = e.KeyChar.ToString(); }
            //    else { start = e.KeyChar.ToString(); }
            //    LoadLookup(tb, "SCHEDULE", start);
            //}
        }

        private void txtItmName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab)
            {
                if (txtName.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Item Name Cannot be Blank...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtName.Focus();
                }
            }
        }

        private void txtRate_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.SelectAll();
        }

        private void txtCess_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            fGen.ValidDecimalNumber(e, ref tb, 17, 2);
        }

        private void txtTax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { LoadLookup((TextBox)sender, "TAXPER", string.Empty); }
        }

        private void txtCess_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.F5))
            { LoadLookup((TextBox)sender, "CESSPER", string.Empty); }
        }

        private void txtTax_Validating(object sender, CancelEventArgs e)
        {
            //NumberValidation((TextBox)sender);
            //if (!isInLookUP("TAXPER", txtTax.Text.Trim()))
            //{
            //    MessageBox.Show("Tax Percentage, Not Found. Press F5 for Lookup", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    //txtTax.Focus();
            //    //txtTax.SelectAll();
            //}
            //else
            //{
            //    //if (Convert.ToDecimal(txtTax.Text.Trim()) > 0)
            //    //{ txtCess.Text = "1.00"; }
            //    //else
            //    //{
            //    //    txtCess.Text = "0.00";
            //    //}
            //    msgLabel.Text = string.Empty;
            //}
        }

        private void txtCess_Validating(object sender, CancelEventArgs e)
        {
            //NumberValidation((TextBox)sender);
            //if (!isInLookUP("CESSPER", txtCess.Text.Trim()))
            //{
            //    MessageBox.Show("Cess Percentage, Not Found. Press F5 for Lookup.", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    //txtCess.Focus();
            //    //txtCess.SelectAll();
            //}
            //else
            //{
            //    msgLabel.Text = string.Empty;
            //}
        }

        private void txtSpecial_Validating(object sender, CancelEventArgs e)
        {
            NumberValidation((TextBox)sender);
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            //if (cmbmultiunit.Text == "N") { return; }

            //frmMultiUnit multiunit = new frmMultiUnit(this.id.Trim(), this.txtItmName.Text.Trim());
            //multiunit.unitid = txtUnit.Text;
            //multiunit.mrps = txtMRP.Text;
            //multiunit.retail = txtRetail.Text;
            //multiunit.wholesale = txtWholeSale.Text;
            //multiunit.special = txtSpecial.Text;
            ////multiunit.project = txtProjectPrice.Text;
            ////multiunit.coolie = txtCoolie.Text;
            ////multiunit.weight = txtweight.Text;
            //multiunit.ShowDialog();
        }

        private void btnSet_Enter(object sender, EventArgs e)
        {
            //if (cmbmultiunit.Text == "N")
            //{ SendKeys.Send("{TAB}"); }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
       
        private void button1_Click(object sender, EventArgs e)
        {
      


            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.CheckFileExists = true;

            openFileDialog.AddExtension = true;

         

            openFileDialog.Filter = "All files (*.*)|*.*|PDF files (*.pdf)|*.pdf|JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //foreach (string fileName in openFileDialog.FileNames)
                //{
                //    pdfFiles.Add(fileName);
                //}

                txtAttachment.Text = openFileDialog.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
           
            if (!string.IsNullOrEmpty(txtAttachment.Text))
            {
                
                    System.Diagnostics.Process.Start(Application.StartupPath + @"\Files\" + @txtAttachment.Text);
               
            }
          
         
        }

        private void btnPrior_Click(object sender, EventArgs e)
        {

        }

       



        //----------------------------------------------
        // Define the PeekMessage API call
        //----------------------------------------------

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.F))
            {
                MessageBox.Show("What the Ctrl+F?");
                return true;
            }
            if (keyData == (Keys.Control | Keys.D))
            {
                if (MessageBox.Show("Are you sure to Delete this ITEM...", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2).Equals(DialogResult.Yes))
                {
                    if (isItmPossibleForDelete())
                    {
                        SqlCommand cmd = new SqlCommand("SP_DELETE_DETAILS" + cPublic.g_firmcode, cPublic.Connection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", id.Trim());
                        cmd.ExecuteNonQuery();

                        try
                        {
                            int count = getItemCount();
                            lblItemCount.Text = "No of Items : " + count.ToString();

                            nxtPrv = "next";
                            msgLabel.Text = string.Empty;
                            cmd.Connection = cPublic.Connection;
                            sqlQuery = "select top 1 a.id,a.Name,a.DocumentNo,a.IssDate,a.ExpDate,a.PlaceofIss,a.Email,b.Code,b.Details,ISNULL(a.filename,d.FileName) filename,ISNULL(a.Attachment,d.Attachment) Attachment from Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code left outer join [dbo].[Docref" + cPublic.g_firmcode + "] c on c.docid=a.id left outer join [dbo].[EmployeeDocument" + cPublic.g_firmcode + "] d on d.id =c.empid  where a.id > @id"
                            + " order by id ";
                            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                            cmd.Parameters.AddWithValue("@id", id.Trim());

                            LoadDetails(cmd);


                            if (count == 0) { loadDefultValues(); }
                            MessageBox.Show("Record Deleted Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (Exception ex)
                        { MessageBox.Show(ex.Message); }
                    }
                    else
                    {
                        MessageBox.Show("You cannot Delete this Item... A Transaction found in " + cPublic.g_firmname + "");
                    }
                }
            }
            //if(keyData == (Keys.Escape))
            //{
            //     this.Dispose();
            //}
            if (keyData == (Keys.Escape))
            {
                if (btnExit.Tag.ToString() == "&Cancel")
                {
                    if ((MessageBox.Show("Are You Sure to Cancel...?  ", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question)).Equals(DialogResult.Yes))
                    {
                        if (btnAdd.Tag.Equals("&Save"))
                        {
                            SqlCommand cmd = new SqlCommand("SP_DELETE_STOCKMST" + cPublic.g_firmcode, cPublic.Connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@itemcode", id.Trim());
                            cmd.ExecuteNonQuery();
                        }

                        gen.ClearFieldsInMe(this);
                        EnableButtons(true);

                        btnAdd.Tag = "&Add";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                        btnEdit.Tag = "&Edit";
                        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        loadDefultValues();
                        loadFirstRecord();
                        searchClick = false;
                        autoCode = false;

                    }
                    EscKey = false;
                    msgLabel.Text = string.Empty;
                }
                    
            }
            if (keyData== (Keys.Control | Keys.N))       // Ctrl-S Save
            {
                oldOp = 0;

                btnAdd.Tag = "&Save";

                btnAdd.BackgroundImage = global::Xpire.Properties.Resources.save1;

                btnExit.Tag = "&Cancel";

                btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

                msgLabel.Text = string.Empty;

                gen.EDControls(this, true);

                EnableButtons(false);

                btnAdd.Enabled = true;

                btnExit.Enabled = true;

                btnBrowse.Enabled = true;

                txtDocumentType.Focus();

                loadDefultValues();

          

            }
            if (keyData== (Keys.Control | Keys.E))
            {
                if (btnAdd.Tag.ToString() != "&Save")
                {
                    if (id.Trim() != string.Empty && txtName.Text.Trim() == string.Empty)

                    { MessageBox.Show("You are not Allowed to Edit this Item..."); return false; }

                    btnEdit.Tag = "&Update";

                    btnEdit.BackgroundImage = global::Xpire.Properties.Resources.save1;

                    btnExit.Tag = "&Cancel";

                    btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

                    msgLabel.Text = string.Empty;

                    gen.EDControls(this, true);

                    EnableButtons(false);

                    btnEdit.Enabled = true;

                    btnExit.Enabled = true;

                    btnBrowse.Enabled = true;

                    txtDocumentType.Focus();
                }
         
            }

            if (keyData == (Keys.Control | Keys.S))
            {
                if (Validation())
                {
                    if (btnEdit.Tag.ToString() == "&Update")
                    {
                        if (UpdateItem())
                        {
                            MessageBox.Show("Item Details Updated Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            EnableButtons(true);

                            btnEdit.Tag = "&Edit";
                            btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;


                        }
                        else { MessageBox.Show("Invalid Data.. Please Check...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    else if (btnAdd.Tag.ToString() == "&Save")
                    {
                        autoCode = true;

                        SqlCommand cmd = new SqlCommand("SP_INSERT_DETAILS" + cPublic.g_firmcode, cPublic.Connection);

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Connection = cPublic.Connection;

                        cmd.Parameters.Clear();

                        Items(cmd);

                        id = cmd.ExecuteScalar().ToString();

                        sqlQuery = "select a.id,a.Name,a.DocumentNo,a.IssDate,a.ExpDate,a.PlaceofIss,a.Email,b.Code,b.Details,ISNULL(a.filename,d.FileName) filename,ISNULL(a.Attachment,d.Attachment) Attachment from Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code left outer join [dbo].[Docref" + cPublic.g_firmcode + "] c on c.docid=a.id left outer join [dbo].[EmployeeDocument" + cPublic.g_firmcode + "] d on d.id =c.empid  where a.id = @id";

                        cmd = new SqlCommand(sqlQuery, cPublic.Connection);

                        cmd.Parameters.AddWithValue("@id", id.Trim());

                        LoadDetails(cmd);

                        autoCode = false;

                        lblItemCount.Text = "No of Items : " + getItemCount().ToString();

                        MessageBox.Show("New Item Saved Successfully......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        EnableButtons(true);

                        autoCode = false;

                        btnAdd.Tag = "&Add";

                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;

                        btnExit.Tag = "E&xit";

                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                    }

                }
            }

            const int WM_KEYDOWN = 0x100;
            var keyCode = (Keys)(msg.WParam.ToInt32() &
                                  Convert.ToInt32(Keys.KeyCode));
            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.V)
                && (ModifierKeys == Keys.Control)
                && txtName.Focused)
            {
                txtName.Paste();
                return true;
            }


            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.V)
                && (ModifierKeys == Keys.Control)
                && txtDocumentNo.Focused)
            {
                txtDocumentNo.Paste();
                return true;
            }



            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.V)
                && (ModifierKeys == Keys.Control)
                && txtEmail1.Focused)
            {
                txtEmail1.Paste();
                return true;
            }


            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.V)
                && (ModifierKeys == Keys.Control)
                && txtEmail2.Focused)
            {
                txtEmail2.Paste();
                return true;
            }

            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.V)
              && (ModifierKeys == Keys.Control)
              && txtEmail3.Focused)
            {
                txtEmail3.Paste();
                return true;
            }

            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.V)
             && (ModifierKeys == Keys.Control)
             && txtEmail4.Focused)
            {
                txtEmail4.Paste();
                return true;
            }

            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.V)
            && (ModifierKeys == Keys.Control)
            && txtEmail5.Focused)
            {
                txtEmail5.Paste();
                return true;
            }

            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.V)
            && (ModifierKeys == Keys.Control)
            && txtEmail6.Focused)
            {
                txtEmail6.Paste();
                return true;
            }







            keyCode = (Keys)(msg.WParam.ToInt32() &
                                  Convert.ToInt32(Keys.KeyCode));
            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.C)
                && (ModifierKeys == Keys.Control)
                && txtName.Focused)
            {
                txtName.Copy();
                return true;
            }


            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.C)
                && (ModifierKeys == Keys.Control)
                && txtDocumentNo.Focused)
            {
                txtDocumentNo.Copy();
                return true;
            }



            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.C)
                && (ModifierKeys == Keys.Control)
                && txtEmail1.Focused)
            {
                txtEmail1.Copy();
                return true;
            }


            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.C)
                && (ModifierKeys == Keys.Control)
                && txtEmail2.Focused)
            {
                txtEmail2.Copy();
                return true;
            }

            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.C)
              && (ModifierKeys == Keys.Control)
              && txtEmail3.Focused)
            {
                txtEmail3.Copy();
                return true;
            }

            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.C)
             && (ModifierKeys == Keys.Control)
             && txtEmail4.Focused)
            {
                txtEmail4.Copy();
                return true;
            }

            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.C)
            && (ModifierKeys == Keys.Control)
            && txtEmail5.Focused)
            {
                txtEmail5.Copy();
                return true;
            }

            if ((msg.Msg == WM_KEYDOWN && keyCode == Keys.C)
            && (ModifierKeys == Keys.Control)
            && txtEmail6.Focused)
            {
                txtEmail6.Copy();
                return true;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

       
        private struct MSG
        {
            public IntPtr hwnd;
            public int message;
            public IntPtr wParam;
            public IntPtr lParam;
            public int time;
            public int pt_x;
            public int pt_y;
        }


        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool PeekMessage([In, Out] ref MSG msg,
            HandleRef hwnd, int msgMin, int msgMax, int remove);

        private void txtName_KeyUp(object sender, KeyEventArgs e)
        {
           
        }

        private void txtAttachment_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

       

        //----------------------------------------------

        /// <summary> 
        /// Trap any keypress before child controls get hold of them
        /// </summary>
        /// <param name="m">Windows message</param>
        /// <returns>True if the keypress is handled</returns>
        //protected override bool ProcessKeyPreview(ref Message m)
        //{
        //    const int WM_KEYDOWN = 0x100;
        //    const int WM_KEYUP = 0x101;
        //    const int WM_CHAR = 0x102;
        //    const int WM_SYSCHAR = 0x106;
        //    const int WM_SYSKEYDOWN = 0x104;
        //    const int WM_SYSKEYUP = 0x105;
        //    const int WM_IME_CHAR = 0x286;

        //    KeyEventArgs e = null;

        //    if ((m.Msg != WM_CHAR) && (m.Msg != WM_SYSCHAR) && (m.Msg != WM_IME_CHAR))
        //    {
        //        e = new KeyEventArgs(((Keys)((int)((long)m.WParam))) | ModifierKeys);
        //        if ((m.Msg == WM_KEYDOWN) || (m.Msg == WM_SYSKEYDOWN))
        //        {
        //            TrappedKeyDown(e);
        //        }
        //        //else
        //        //{
        //        //    TrappedKeyUp(e);
        //        //}

        //        // Remove any WM_CHAR type messages if supresskeypress is true.
        //        if (e.SuppressKeyPress)
        //        {
        //            this.RemovePendingMessages(WM_CHAR, WM_CHAR);
        //            this.RemovePendingMessages(WM_SYSCHAR, WM_SYSCHAR);
        //            this.RemovePendingMessages(WM_IME_CHAR, WM_IME_CHAR);
        //        }

        //        if (e.Handled)
        //        {
        //            return e.Handled;
        //        }
        //    }
        //    return base.ProcessKeyPreview(ref m);
        //}

        //private void RemovePendingMessages(int msgMin, int msgMax)
        //{
        //    if (!this.IsDisposed)
        //    {
        //        MSG msg = new MSG();
        //        IntPtr handle = this.Handle;
        //        while (PeekMessage(ref msg,
        //        new HandleRef(this, handle), msgMin, msgMax, 1))
        //        {
        //        }
        //    }
        //}

        /// <summary>
        /// This routine gets called if a keydown has been trapped 
        /// before a child control can get it.
        /// </summary>
        /// <param name="e"></param>
        //private void TrappedKeyDown(KeyEventArgs e)
        //{
        //    if (e.Control && e.KeyCode == Keys.N)       // Ctrl-S Save
        //    {
        //        oldOp = 0;

        //        btnAdd.Tag = "&Save";

        //        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.save1;

        //        btnExit.Tag = "&Cancel";

        //        btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

        //        msgLabel.Text = string.Empty;

        //        gen.EDControls(this, true);

        //        EnableButtons(false);

        //        btnAdd.Enabled = true;

        //        btnExit.Enabled = true;

        //        btnBrowse.Enabled = true;

        //        loadDefultValues();

        //        e.Handled = true;

        //        e.SuppressKeyPress = true;

        //    }
        //    if (e.Control && e.KeyCode == Keys.E)
        //    {
        //        if (id.Trim() != string.Empty && txtName.Text.Trim() == string.Empty)

        //        { MessageBox.Show("You are not Allowed to Edit this Item..."); return; }

        //        btnEdit.Tag = "&Update";

        //        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.save1;

        //        btnExit.Tag = "&Cancel";

        //        btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

        //        msgLabel.Text = string.Empty;

        //        gen.EDControls(this, true);

        //        EnableButtons(false);

        //        btnEdit.Enabled = true;

        //        btnExit.Enabled = true;

        //        btnBrowse.Enabled = true;

        //        e.Handled = true;

        //        e.SuppressKeyPress = true;
        //    }

        //    if (e.Control && e.KeyCode == Keys.S)
        //    {
        //        if (Validation())
        //        {
        //            if (btnEdit.Tag == "&Update")
        //            {
        //                if (UpdateItem())
        //                {
        //                    MessageBox.Show("Item Details Updated Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //                    EnableButtons(true);

        //                    btnEdit.Tag = "&Edit";
        //                    btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
        //                    btnExit.Tag = "E&xit";
        //                    btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;


        //                }
        //                else { MessageBox.Show("Invalid Data.. Please Check...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error); }
        //            }
        //            else if (btnAdd.Tag == "&Save")
        //            {
        //                autoCode = true;

        //                SqlCommand cmd = new SqlCommand("SP_INSERT_DETAILS" + cPublic.g_firmcode, cPublic.Connection);

        //                cmd.CommandType = CommandType.StoredProcedure;

        //                cmd.Connection = cPublic.Connection;

        //                cmd.Parameters.Clear();

        //                Items(cmd);

        //                id = cmd.ExecuteScalar().ToString();

        //                sqlQuery = "select * from  Details" + cPublic.g_firmcode + " a join LOOKUP b on a.documenttype=b.code where id = @id";

        //                cmd = new SqlCommand(sqlQuery, cPublic.Connection);

        //                cmd.Parameters.AddWithValue("@id", id.Trim());

        //                LoadDetails(cmd);

        //                autoCode = false;

        //                lblItemCount.Text = "No of Items : " + getItemCount().ToString();

        //                MessageBox.Show("New Item Saved Successfully......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);

        //                EnableButtons(true);

        //                autoCode = false;

        //                btnAdd.Tag = "&Add";

        //                btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;

        //                btnExit.Tag = "E&xit";

        //                btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
        //            }

        //        }
        //    }
        //    if (e.KeyCode == Keys.A)
        //    {
        //        e.Handled = true;
        //        e.SuppressKeyPress = true;
        //    }
        //}
    }
}
