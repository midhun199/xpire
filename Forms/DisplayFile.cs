﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Xpire.Forms
{
    public partial class DisplayFile : Form
    {

        string Filename = string.Empty;
        

        public DisplayFile(string filename)
        {
            InitializeComponent();

            this.Filename = filename;

            axAcroPDF1.src = this.Filename;

            axAcroPDF1.setShowToolbar(true);
        }
    }
}
