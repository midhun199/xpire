﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using System.Text.RegularExpressions;
using System.Drawing.Printing;
using Xpire.BL;
using Xpire.Classes;
using Xpire.Settings;

namespace Xpire.Forms
{
    public partial class ucLeaveApplication : UserControl
    {

        SqlDataReader rdr = null;
        DataTable dtable = new DataTable();

        DataSet ds = new DataSet();
        SqlCommand cmd = null;
        DataTable dt = new DataTable();
        string id = string.Empty;
        string sqlQuery = string.Empty, nxtPrv = string.Empty;
        Boolean searchClick = false, autoCode = false, EscKey = false;
        private string PS = string.Empty;
        cGeneral gen = new cGeneral();
        public static int balleave = 0;

       
        public ucLeaveApplication()
        {
            InitializeComponent();

           

        }

        private Boolean LoadDetails(SqlCommand cmd)
        {

            Boolean status = true;

            SqlDataReader dr;

            cmd.Connection = cPublic.Connection;

            dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                id = dr["id"].ToString();

                txtStaffID.Text = dr["StaffID"].ToString();

                txtStaffName.Text = dr["StaffName"].ToString();
              
                dtpDOA.Text = dr["DOA"].ToString();
 
                dtpFrom.Text = dr["From"].ToString();

                dtpTo.Text = dr["To"].ToString();

                this.txtLeaveType.Tag = dr["LeaveTypeCode"].ToString();

                this.txtLeaveType.Text = dr["LeaveTypeDetails"].ToString();

                this.txtCOV.Tag = dr["CovTypeCode"].ToString();

                this.txtCOV.Text = dr["CovTypeDetails"].ToString();

                this.txtRemarks.Text = dr["remarks"].ToString();

                this.Tag = "Data Loaded";

            }
            else { status = false; }
            dr.Close();

            gen.EDControls(groupBox1, false);
            //gen.EDControls(groupBox2, false);

            int count = getItemCount();

            if (nxtPrv.Equals("next") && !status)
            {
                nxtPrv = string.Empty;
            }
            else if (nxtPrv.Equals("previous") && !status)
            {
                nxtPrv = string.Empty;
            }
            if (count == 0)
            { btnEdit.Enabled = false; btnDelete.Enabled = false; }
            return status;
        }

        private void loadFirstRecord()
        {
            sqlQuery = "select top 1 a.id,b.[StaffID],b.[StaffName],a.DOA,a.[From]  ,a.[To] ,c.Code LeaveTypeCode, c.Details LeaveTypeDetails,e.Code CovTypeCode, e.Details CovTypeDetails,a.remarks from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID]= b.[StaffID] join LOOKUP  c on a.[LeaveType]= c.code join LOOKUP  e on a.[cov]= e.code order by a.[id]";

            cmd = new SqlCommand(sqlQuery, cPublic.Connection);

            LoadDetails(cmd);
        }

        public bool varification(string verify)
        {
            return Regex.IsMatch(verify, "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
        }

        private void EnableButtons(Boolean status)
        {
            btnSearch.Enabled = status;
            btnNext.Enabled = status;
            btnPrevious.Enabled = status;
            btnEdit.Enabled = status;
            btnDelete.Enabled = status;
            btnAdd.Enabled = status;
            btnExit.Enabled = status;

        }

        private void loadDefultValues()
        {

            id = string.Empty;
            txtStaffName.Text = string.Empty;
            txtStaffID.Text = string.Empty;
            dtpDOA.Value = System.DateTime.Now;
            dtpFrom.Value = System.DateTime.Now;
            dtpTo.Value = System.DateTime.Now;
            txtLeaveType.Text = string.Empty;
            txtCOV.Text = string.Empty;

        }

        public Boolean Validation()
        {
            Boolean status = true;
            if (txtStaffID.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Stafe Name cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtStaffID.Focus();
            }

            else if (dtpFrom.Value>dtpTo.Value)
            {
                status = false;
                MessageBox.Show("From Date must be less than To Date .....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtpFrom.Focus();
            }

            else if (txtLeaveType.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Leave Type cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtLeaveType.Focus();
            }
            else if (txtCOV.Text.Trim().Equals(string.Empty))
            {
                status = false;
                MessageBox.Show("Country of visit cannot be Blank.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCOV.Focus();
            }

            return status;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchClick = true;
            btnExit.Tag = "&Cancel";
            btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;
            gen.ClearFieldsInMe(this);
            txtStaffID.Enabled = true;
            txtStaffID.Focus();
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnNext.Enabled = false;
            btnPrevious.Enabled = false;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            nxtPrv = "previous";
            //msgLabel.Text = string.Empty;

            sqlQuery = "select top 1 a.id,b.[StaffID],b.[StaffName],a.DOA,a.[From]  ,a.[To] ,c.Code LeaveTypeCode, c.Details LeaveTypeDetails,e.Code CovTypeCode, e.Details CovTypeDetails,a.remarks from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID]= b.[StaffID] join LOOKUP  c on a.[LeaveType]= c.code join LOOKUP  e on a.[cov]= e.code where a.id < @id"
            + "  order by a.id desc";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@id", id.Trim());
            LoadDetails(cmd);
        }

        private void ucLeaveApplication_Load(object sender, EventArgs e)
        {
            lblItemCount.BorderStyle = BorderStyle.None;


            this.Tag = "No Records";

            loadFirstRecord();
            lblItemCount.Text = "No of Application : " + getItemCount().ToString();

            if (this.Tag.Equals("No Records"))
            {
                btnEdit.Enabled = false;
                btnDelete.Enabled = false;
            }

        }

        private void txtStaffID_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchClick)
            {
                if (e.KeyCode.Equals(Keys.F5))
                {
                    string fields = string.Empty;
                    string dispName = string.Empty;
                    string width = string.Empty;
               
                    fields = "id,StaffID,StaffName";
                    dispName = "id,Staff ID,Name";
                    width = "0,100,150";
             
                    frmlookup lookup = new frmlookup();
                    lookup.m_table = "LeaveApplication" + cPublic.g_firmcode;
                    lookup.m_fields = fields;
                    lookup.m_dispname = dispName;
                    lookup.m_fldwidth = width;
                    lookup.m_con = cPublic.Connection;
                    lookup.ShowDialog();
                    if (lookup.m_values.Count > 0)
                    {
                      
                        id = lookup.m_values[0].ToString();
                        txtStaffID.Text = lookup.m_values[1].ToString();
                        txtStaffName.Text = lookup.m_values[2].ToString();
                     
                        sqlQuery = "select a.id,b.[StaffID],b.[StaffName],a.DOA,a.[From]  ,a.[To] ,c.Code LeaveTypeCode, c.Details LeaveTypeDetails,e.Code CovTypeCode, e.Details CovTypeDetails,a.remarks from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID]= b.[StaffID] join LOOKUP  c on a.[LeaveType]= c.code join LOOKUP  e on a.[cov]= e.code where a.id = @id";
                        SqlCommand cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                        cmd.Parameters.AddWithValue("@id", id.Trim());
                        LoadDetails(cmd);
                        btnAdd.Enabled = true;
                        btnEdit.Enabled = true;
                        btnDelete.Enabled = true;
                        btnNext.Enabled = true;
                        btnPrevious.Enabled = true;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        searchClick = false;
                    }
                }

            }

            else
            {
                if (e.KeyCode.Equals(Keys.F5))
                {
                    string fields = string.Empty;
                    string dispName = string.Empty;
                    string width = string.Empty;
                    //if (cPublic.LookupDefault == "Itemcode")
                    //{
                    fields = "StaffID,StaffName";
                    dispName = "Staff ID,Name";
                    width = "100,150";
               
                    frmlookup lookup = new frmlookup();
                    lookup.m_table = "Employee" + cPublic.g_firmcode;
                    lookup.m_fields = fields;
                    lookup.m_dispname = dispName;
                    lookup.m_fldwidth = width;
                    lookup.m_con = cPublic.Connection;
                    lookup.ShowDialog();
                    if (lookup.m_values.Count > 0)
                    {

                        txtStaffID.Text = lookup.m_values[0].ToString();

                        txtStaffName.Text = lookup.m_values[1].ToString();

                        lblBalLeave.Text= "Balance Leave : " + getBalLeave(txtStaffID.Text).ToString();

                        txtLeaveType.Focus();

                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                switch (btnAdd.Tag.ToString())
                {
                    case "&Add":

                        btnAdd.Tag = "&Save";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.save1;
                        btnExit.Tag = "&Cancel";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

                        gen.EDControls(this, true);


                        EnableButtons(false);
                        btnAdd.Enabled = true;
                        btnExit.Enabled = true;


                        loadDefultValues();



                        break;
                    case "&Save":
                        if (Validation())
                        {

                            
                            autoCode = true;

                            SqlCommand cmd = new SqlCommand("SP_INSERT_LEAVEAPPLICATION" + cPublic.g_firmcode, cPublic.Connection);

                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Connection = cPublic.Connection;


                            cmd.Parameters.Clear();


                            Items(cmd);

                            id = cmd.ExecuteScalar().ToString();


                            sqlQuery = "select a.id,b.[StaffID],b.[StaffName],a.DOA,a.[From]  ,a.[To] ,c.Code LeaveTypeCode, c.Details LeaveTypeDetails,e.Code CovTypeCode, e.Details CovTypeDetails,a.remarks from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID]= b.[StaffID] join LOOKUP  c on a.[LeaveType]= c.code join LOOKUP  e on a.[cov]= e.code  where a.id = @id";
                            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                            cmd.Parameters.AddWithValue("@id", id.Trim());
                            LoadDetails(cmd);
                            autoCode = false;
                            lblItemCount.Text = "No of Items : " + getItemCount().ToString();
                            MessageBox.Show("New Item Saved Successfully......", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            EnableButtons(true);

                            autoCode = false;
                            btnAdd.Tag = "&Add";
                            btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                            PS = "PRINT";
                            cPublic.PrintMode = "WINDOWS";

                            if (PS == "PRINT")
                            {
                                if (cPublic.PrintMode == "WINDOWS")
                                {
                                    new cWindowsPrint("[dbo].[LeaveApplication" + cPublic.g_firmcode + "]", "", id, "Leave Application");
                                }
                            }
                        }
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void btnNext_Click(object sender, EventArgs e)
        {
            nxtPrv = "next";


            sqlQuery = "select top 1 a.id,b.[StaffID],b.[StaffName],a.DOA,a.[From]  ,a.[To] ,c.Code LeaveTypeCode, c.Details LeaveTypeDetails,e.Code CovTypeCode, e.Details CovTypeDetails,a.remarks from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID]= b.[StaffID] join LOOKUP  c on a.[LeaveType]= c.code join LOOKUP  e on a.[cov]= e.code where a.id > @id"
            + "  order by a.id ";
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@id", id.Trim());

            LoadDetails(cmd);
        }

        

        private void btnEdit_Click(object sender, EventArgs e)
        {
            switch (btnEdit.Tag.ToString())
            {
                case "&Edit":

                    if(getApplicationExist()>0)
                    {
                        MessageBox.Show("You are not Allowed to Edit this Item...");
                        return;
                    }
                    if (id.Trim() != string.Empty && txtStaffID.Text.Trim() == string.Empty)
                    {
                        MessageBox.Show("You are not Allowed to Edit this Item...");
                        return;
                    }
                    btnEdit.Tag = "&Update";
                    btnEdit.BackgroundImage = global::Xpire.Properties.Resources.save1;
                    btnExit.Tag = "&Cancel";
                    btnExit.BackgroundImage = global::Xpire.Properties.Resources.cancel1;

                    gen.EDControls(this, true);

                    EnableButtons(false);
                    btnEdit.Enabled = true;
                    btnExit.Enabled = true;


                    break;
                case "&Update":
                    if (Validation())
                    {
                        if (UpdateItem())
                        {
                            MessageBox.Show("Item Details Updated Successfully.....", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            EnableButtons(true);

                            btnEdit.Tag = "&Edit";
                            btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                            btnExit.Tag = "E&xit";
                            btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;


                        }
                        else { MessageBox.Show("Invalid Data.. Please Check...", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    break;

            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            switch (btnExit.Tag.ToString())
            {
                case "E&xit":
                    this.Dispose();
                    break;
                case "&Cancel":
                    if ((MessageBox.Show("Are You Sure to Cancel...?  ", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question)).Equals(DialogResult.Yes))
                    {
                        //if (btnAdd.Tag.Equals("&Save"))
                        //{
                        //    SqlCommand cmd = new SqlCommand("SP_DELETE_STOCKMST" + cPublic.g_firmcode, cPublic.Connection);
                        //    cmd.CommandType = CommandType.StoredProcedure;
                        //    cmd.Parameters.AddWithValue("@itemcode", id.Trim());
                        //    cmd.ExecuteNonQuery();
                        //}

                        gen.ClearFieldsInMe(this);
                        EnableButtons(true);

                        btnAdd.Tag = "&Add";
                        btnAdd.BackgroundImage = global::Xpire.Properties.Resources.new2;
                        btnEdit.Tag = "&Edit";
                        btnEdit.BackgroundImage = global::Xpire.Properties.Resources.edit;
                        btnExit.Tag = "E&xit";
                        btnExit.BackgroundImage = global::Xpire.Properties.Resources.exit1;
                        loadDefultValues();
                        loadFirstRecord();
                        searchClick = false;
                        autoCode = false;

                    }
                    EscKey = false;

                    break;
            }
        }

        private void txtLeaveType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'Leave Type'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "75,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtLeaveType.Text = lookup.m_values[1].ToString();
                    txtLeaveType.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private int getApplicationExist()
        {
            sqlQuery = "select  isnull(count(*),0) from LeaveApplication" + cPublic.g_firmcode + " a join LeaveApproval" + cPublic.g_firmcode + " b on a.id=b.applicationid where b.applicationid=@id";
            
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            cmd.Parameters.AddWithValue("@id", id.Trim());
            return Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }


        private int getItemCount()
        {
            sqlQuery = "select  isnull(count(*),0) from LeaveApplication" + cPublic.g_firmcode;
            cmd = new SqlCommand(sqlQuery, cPublic.Connection);
            return Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }

        private void dtpDOA_MouseDown(object sender, MouseEventArgs e)
        {
            
            

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
       

        }

        private void txtCOV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                frmlookup lookup = new frmlookup();
                lookup.m_con = cPublic.Connection;

                lookup.m_condition = "Field1 = 'COUNTRY'";
                lookup.m_table = "LOOKUP";
                lookup.m_fields = "Code,Details";
                lookup.m_dispname = "Code,Details";
                lookup.m_fldwidth = "0,150";
                lookup.m_strat = string.Empty;
                lookup.ShowDialog();
                if (lookup.m_values.Count > 0)
                {
                    txtCOV.Text = lookup.m_values[1].ToString();
                    txtCOV.Tag = lookup.m_values[0].ToString();

                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void dtpTo_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtpFrom_ValueChanged(object sender, EventArgs e)
        {

         
        }

        private void dtpFrom_Validating(object sender, CancelEventArgs e)
        {
            templeave();
        }

        public void templeave()
        {
            if (!string.IsNullOrEmpty(txtStaffID.Text))
            {
                DateTime regoining;

                int templeave = 0;

                sqlQuery = "select top 1 CONVERT(datetime, CONVERT(varchar(10), a.[DOR], 101))  from[dbo].[REJOINING0001] a join[dbo].[LeaveApproval0001]" +
                                     "b on a.[approvalid]=b.id join[dbo].[LeaveApplication0001]" +
                                     "c on c.id=b.[applicationid] where c.staffid=@staffid order by a.id desc";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@staffid", txtStaffID.Text.Trim());

                if (cmd.ExecuteScalar() != null)
                {
                    regoining = Convert.ToDateTime(cmd.ExecuteScalar().ToString());
                    int month = (dtpFrom.Value.Month - regoining.Month) + 12 * (dtpFrom.Value.Year - regoining.Year);

                    templeave = Convert.ToInt32(month * 2.5);
                }

               


                lblBalLeave.Text = "Balance Leave : " + (getBalLeave(txtStaffID.Text) + templeave);

                balleave = getBalLeave(txtStaffID.Text) + templeave;
            }
        }

        private void dtpFrom_Validated(object sender, EventArgs e)
        {

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            PS = "PRINT";
            cPublic.PrintMode = "WINDOWS";

            if (PS == "PRINT")
            {
                if (cPublic.PrintMode == "WINDOWS")
                {
                    new cWindowsPrint("[dbo].[LeaveApplication" + cPublic.g_firmcode + "]", "", id, "Leave Application");
                }
            }
        }


        public int getBalLeave(string staffid)
        {
           
            SqlCommand cmd = new SqlCommand("SP_GET_BALLEAVE" + cPublic.g_firmcode, cPublic.Connection);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Connection = cPublic.Connection;


            cmd.Parameters.Clear();


            cmd.Parameters.AddWithValue("@Staffid", staffid);

         
            return Convert.ToInt32(cmd.ExecuteScalar().ToString());
        }
        private void Items(SqlCommand cmd)
        {

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id.Trim());
            cmd.Parameters.AddWithValue("@Staffid", txtStaffID.Text.Trim());
  

            cmd.Parameters.AddWithValue("@LeaveType", txtLeaveType.Tag);
            cmd.Parameters.AddWithValue("@cov", txtCOV.Tag);
            cmd.Parameters.AddWithValue("@from", Convert.ToDateTime(dtpFrom.Value));
            cmd.Parameters.AddWithValue("@to", Convert.ToDateTime(dtpTo.Value));
            cmd.Parameters.AddWithValue("@DOA", Convert.ToDateTime(dtpDOA.Value));
            cmd.Parameters.AddWithValue("@remarks", txtRemarks.Text.Trim());

        }

        public Boolean UpdateItem()
        {
            SqlTransaction sqlTrans = null;
            Boolean status = true;

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cPublic.Connection;


                cmd.Parameters.Clear();
                cmd.CommandText = "SP_UPDATE_LEAVEAPPLICATION" + cPublic.g_firmcode;

                Items(cmd);

                cmd.ExecuteNonQuery();

                sqlQuery = "select a.id,b.[StaffID],b.[StaffName],a.DOA,a.[From]  ,a.[To] ,c.Code LeaveTypeCode, c.Details LeaveTypeDetails,e.Code CovTypeCode, e.Details CovTypeDetails,a.remarks from [dbo].[LeaveApplication" + cPublic.g_firmcode + "] a join [Employee" + cPublic.g_firmcode + "] b on a.[StaffID]= b.[StaffID] join LOOKUP  c on a.[LeaveType]= c.code join LOOKUP  e on a.[cov]= e.code where a.id = @id";
                cmd = new SqlCommand(sqlQuery, cPublic.Connection);
                cmd.Parameters.AddWithValue("@id", id.Trim());
                LoadDetails(cmd);
            }
            catch (Exception ex)
            {
                sqlTrans.Rollback();
                MessageBox.Show(ex.Message);
                status = false;
            }
            return status;
        }

    }
}
