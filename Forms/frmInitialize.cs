﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Xpire.Classes;

namespace Xpire.Forms
{
    public partial class frmInitialize : UserControl
    {
        public frmInitialize()
        {
            InitializeComponent();


            this.btnInit.Enabled = false; 
        }

        SqlDataAdapter ap;
        DataTable dt;
        private void txtPwd_TextChanged(object sender, EventArgs e)
        {
            if (txtPwd.Text.Trim() == "YES")
            {
                this.btnInit.Enabled = true;
                this.txtPwd.Enabled = false;
            }

        }

        private void rdbAccKp_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbAccKp.Checked)
            {
                lblAcc.Text = "Set Accounts Balance To";
                grpAcc.Enabled = true;

            }
        }

        private void rdbAccClr_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbAccClr.Checked)
            {
                lblAcc.Text = "All Accounts Heads Will Be Deleted Permenently. ";
                grpAcc.Enabled = false;

            }
        }

        private void rdbStkKp_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbStkKp.Checked)
            {
                lblStk.Text = "Set Stock Balance To";
                grpStk.Enabled = true;
            }
        }

        private void rdbStkClr_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbStkClr.Checked)
            {
                lblStk.Text = "All Stock Items Will Be Deleted Permenently. ";
                grpStk.Enabled = false;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure To Close ", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question).ToString() == "Yes")
            {
                this.Dispose();
            }
        }



        DataSet ds = new DataSet();
        private void btnInit_Click(object sender, EventArgs e)
        {
            cConnection con = new cConnection();
            Int32 prIntFirm = 0;
            
            if (MessageBox.Show("Sure To Initialize", cPublic.messagename, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            SqlTransaction trans=null ;

            try
            {
                for (prIntFirm = 0; prIntFirm < cPublic.g_FirmArray.Count; prIntFirm++)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cPublic.Connection;
                    trans = cPublic.Connection.BeginTransaction();
                    cmd = cPublic.Connection.CreateCommand();
                    cmd.Transaction = trans;


                    ProgressBar1.Visible = true;
                    ProgressBar1.Value = 10;

                    # region STOCK MASTER

                    //----------------Keeping stock item and refreshing the entries according to conditions------------
                    if (rdbStkZero.Checked)//For Setting Stock as Zero
                    {

                        cmd.CommandText = "UPDATE stockmst" + cPublic.g_FirmArray[prIntFirm] + " SET opstock=0,qty=0 ";
                        cmd.ExecuteNonQuery();
                    }

                    if (rdbStkOb.Checked)//For Setting Current Stock as Opening Balance
                    {
                        cmd.CommandText = "update stockmst" + cPublic.g_FirmArray[prIntFirm] + " set qty=OPstock ";
                        cmd.ExecuteNonQuery();
                    }

                    if (rdbStkNc.Checked)//For Setting  Opening Balance as Current Stock
                    {
                    }

                    ProgressBar1.Value = 20;


                    //-----------------------Delete All Stock Items ------------------------------------------
                    if (rdbStkClr.Checked)
                    {
                        cmd.CommandText = "Delete from stockmst" + cPublic.g_FirmArray[prIntFirm];
                        cmd.ExecuteNonQuery();
                    }

                    if (chkStock.Checked == true)
                    {
                        cmd.CommandText = "UPDATE STOCKMST" + cPublic.g_FirmArray[prIntFirm] + " set OPstock=Qty ";
                        cmd.ExecuteNonQuery();
                    }
                    if (rdbLookClear.Checked)
                    {
                        cmd.CommandText = "DELETE FROM LOOKUP WHERE USERID is null";
                        cmd.ExecuteNonQuery();
                    }

                    # endregion

                    ProgressBar1.Value = 30;

                    #region for Income ,Expense,Trading Income, Trading Expense
                    if (chkOpbalance.Checked == true)
                    {
                        cmd.CommandText = "UPDATE  accounts" + cPublic.g_FirmArray[prIntFirm] + "  set opbalance=0.00 ,BALANCE= 0.00 where groupcode like '5%' or groupcode like '6%' or groupcode like '3%' or groupcode like '4%'";
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        cmd.CommandText = "UPDATE  groups" + cPublic.g_FirmArray[prIntFirm] + "  set opbalance=0.00 ,BALANCE= 0.00  where code LIKE '5%' or code like '6%' or code like '3%' or code like '4%'";
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    #endregion


                    # region Accounts

                    //--------------Keeping Accounts Table and refrehing entries according to given condition----------

                    if (rdbAccKp.Checked)//when accounts head is kept.......
                    {
                        if (rdbAccZero.Checked)//when accounts head is kept...on balance  zero
                        {
                            //CHECK FOR VOUCHER ENTRY
                            voucherDelete(cmd, false, cPublic.g_FirmArray[prIntFirm] + "");

                            //checks the accounts and set the balance to zero
                            setallaccounts(cmd, cPublic.g_FirmArray[prIntFirm] + "");

                            cmd.CommandText = "Delete from  PENDINGBILL" + cPublic.g_FirmArray[prIntFirm];
                            cmd.ExecuteNonQuery();

                            cmd.CommandText = "Delete from  PBILLSUB" + cPublic.g_FirmArray[prIntFirm];
                            cmd.ExecuteNonQuery();
                        }
                        if (chkOpbalance.Checked == true)
                        {
                            setopbalaccounts(cmd, cPublic.g_FirmArray[prIntFirm] + "", true);

                            voucherDelete(cmd, true, cPublic.g_FirmArray[prIntFirm] + "");

                            cmd.CommandText = "DELETE FROM PENDINGBILL" + cPublic.g_FirmArray[prIntFirm] + "" + "  WHERE BALANCE=0   ";
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();


                            cmd.CommandText = "DELETE from PBILLSUB" + cPublic.g_FirmArray[prIntFirm] + " ";
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            cmd.CommandText = "UPDATE PENDINGBILL" + cPublic.g_FirmArray[prIntFirm] + " SET BILLAMT=BALANCE ,RECEIVED=0,FLAG='N'    ";
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }

                        if (rdbAccOb.Checked)//when accounts head is kept......and currentbalance is updated to opbalance and opbalance is set to zero.
                        {
                            //set update the opbalance to currentbalance
                            // setopbalaccounts(cmd, cPublic.g_FirmArray[prIntFirm] + "");
                            //CHECK FOR VOUCHER ENTRY
                            voucherDelete(cmd, false, cPublic.g_FirmArray[prIntFirm] + "");


                            //SET THE OPBALANCE TO CURRENTBALANCE
                            setcurbalaccounts(cmd, cPublic.g_FirmArray[prIntFirm] + "");
                            RecalcOp(cmd, cPublic.g_FirmArray[prIntFirm] + "");
                        }
                    }

                    ProgressBar1.Value = 45;


                    //---------Delete from Accounts-------------------'

                    if (rdbAccClr.Checked)
                    {
                        //CHECK FOR VOUCHER ENTRY
                        voucherDelete(cmd, false, cPublic.g_FirmArray[prIntFirm] + "");

                        //Delete the corresponding account head the user added in the account master
                        //condition checks on wether the head is added by in userid columns

                        accountsdelete(cmd, cPublic.g_FirmArray[prIntFirm] + "");

                        cmd.CommandText = "Delete from  PENDINGBILL" + cPublic.g_FirmArray[prIntFirm];
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "Delete from  PBILLSUB" + cPublic.g_FirmArray[prIntFirm];
                        cmd.ExecuteNonQuery();

                    }

                    ProgressBar1.Value = 60;

                    #endregion


                    # region -----------------Deleting Records from SubTables -------------------------


                    cmd.CommandText = "Delete from  sales" + cPublic.g_FirmArray[prIntFirm];
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "Delete from  sitem" + cPublic.g_FirmArray[prIntFirm];
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "Delete from  sreturn" + cPublic.g_FirmArray[prIntFirm];
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "Delete from  sritem" + cPublic.g_FirmArray[prIntFirm];
                    cmd.ExecuteNonQuery();

                    //

                    //-------------------------
                    ProgressBar1.Value = 70;

                    cmd.CommandText = "Delete from  purchase" + cPublic.g_FirmArray[prIntFirm];
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "Delete from  pitem" + cPublic.g_FirmArray[prIntFirm];
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "Delete from  preturn" + cPublic.g_FirmArray[prIntFirm];
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "Delete from  pritem" + cPublic.g_FirmArray[prIntFirm];
                    cmd.ExecuteNonQuery();


                    # endregion


                    ProgressBar1.Value = 80;

                    ProgressBar1.Value = 90;

                    ProgressBar1.Value = 100;

                    trans.Commit();
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();

                Cursor.Current = Cursors.Arrow;
                MessageBox.Show(ex.Message, cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Error);
                ProgressBar1.Visible = false;
                return;
            }

            Cursor.Current = Cursors.Arrow;
            MessageBox.Show("Initialization Completed Successfully.", cPublic.messagename, MessageBoxButtons.OK, MessageBoxIcon.Information);
            ProgressBar1.Visible = false;
           
        }

        public void setopbalaccounts(SqlCommand cmd, string Firmcode)
        {

            cmd.Parameters.Clear();
            string sql = "select code  from accounts" + Firmcode + " Where code like '1%' or code like '2%' ";
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            ds.Clear();
            da.Fill(ds);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                cmd.Parameters.Clear();
                sql = "update  Accounts" + Firmcode + " set  opbalance=balance where code=@code and code like '1%' or code like '2%' ";
                cmd.CommandText = sql;
                cmd.Parameters.AddWithValue("@code", ds.Tables[0].Rows[i]["code"].ToString().Trim());
                cmd.ExecuteNonQuery();
                sql = "update  groups" + Firmcode + " set  opbalance=balance  where code like '1%' or code like '2%'  ";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

            }
        }
       

        public bool voucherDelete(SqlCommand cmd, bool deletepassedonly, string FirmCode)
        {
            if (deletepassedonly == false)
            {
                string sql = "Delete from voucher" + FirmCode + " ";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
            else
            {
                string sql = "Delete from voucher" + FirmCode + " Where recno  " + Environment.NewLine
                  + " not in (Select recno from voucher" + FirmCode + " Where chqpassed=0 and cheque=1) ";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
            return false;
        }

        public void voucherdelete(DataSet ds, SqlCommand cmd, string firmcode)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                cmd.Parameters.Clear();
                cmd.CommandText = "SP_VOUCHER_DELETE_REC" + firmcode;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TRANSTYPE", ds.Tables[0].Rows[i]["transtype"].ToString().Trim());
                cmd.Parameters.AddWithValue("@RECNO", ds.Tables[0].Rows[i]["recno"].ToString().Trim());
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
        }

        public void Billtobilldelete(SqlCommand cmd, string FirmCode)
        {
            cmd.Parameters.Clear();
            cmd.CommandText = "Delete  from PBillSub" + FirmCode;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

            cmd.CommandText = "Delete  from PendingBill" + FirmCode;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
        }

        public bool accountsdelete(SqlCommand cmd, string FirmCode)
        {
            string sql = "Delete  from accounts" + FirmCode + " where (userid !='System')and legal <>1 ";
            cmd.CommandText = sql;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            return false;
        }

        public void setallaccounts(SqlCommand cmd, string FirmCode)
        {
            cmd.Parameters.Clear();
            cmd.CommandType = CommandType.Text;
            string sql = "update  Accounts" + FirmCode + " set balance=0 , opbalance=0 ";
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            sql = "update  groups" + FirmCode + " set balance=0 , opbalance=0 ";
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
        }


        public void setopbalaccounts(SqlCommand cmd, string FirmCode, bool chque)
        {
            cmd.CommandText = "Update Accounts" + FirmCode + " Set Balance=OPBalance";
            cmd.ExecuteNonQuery();

            cmd.CommandText = "Update Groups" + FirmCode + " Set Balance=0,OPBalance=0";
            cmd.ExecuteNonQuery();

            string s1 = string.Empty, s2 = string.Empty;

            s2 = " and ((bookcode<'200003' and cheque=0 ) or (bookcode>='200003' and  chqpassed =1 and cheque=1 ))  ";

            cmd.CommandText = "update A set balance=balance+ isnull((select sum(amount) from voucher" + FirmCode + "   where code=a.code " + s1 + s2 + "  ),0) " + Environment.NewLine
                + " + isnull((select sum(amount)*-1 from voucher" + FirmCode + "   where bookcode=a.code " + s1 + s2 + "  ),0) " + Environment.NewLine
                + " from accounts" + FirmCode + "   A ";
            cmd.ExecuteNonQuery();

            cmd.CommandText = "update A set OPBalance=balance from accounts" + FirmCode + "   A where code like '1%' or code like '2%' ";
            cmd.ExecuteNonQuery();

            RecalcOp(cmd, FirmCode);
        }

        public void setcurbalaccounts(SqlCommand cmd, string FirmCode)
        {
            cmd.CommandType = CommandType.Text;
            string sql = "update  Accounts" + FirmCode + " set  OPBALANCE=BALANCE where code like '1%' or code like '2%'  ";
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            sql = "update  GROUPS" + FirmCode + " set  OPBALANCE=BALANCE  where code like '1%' or code like '2%'  ";
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
        }

        private void frmInitialize_Load(object sender, EventArgs e)
        {
            this.btnInit.Enabled = false;
        }

        private void RecalcOp(SqlCommand cmd, string Firmcode)
        {
            cmd.CommandText = "select * from Groups" + Firmcode + " Order by level1 desc ";
            ap = new SqlDataAdapter(cmd);
            dt = new DataTable();
            ap.Fill(dt);
            for (int vRow = 0; vRow < dt.Rows.Count; vRow++)
            {
                cmd.CommandText = "Update Groups" + Firmcode + "  Set OPBalance=(Select isnull(sum(opbalance),0) from Accounts" + Firmcode + "  Where GroupCode='" + dt.Rows[vRow]["Code"] + "" + "') + (Select isnull(sum(opbalance),0) from Groups" + Firmcode + " where Groupcode='" + dt.Rows[vRow]["Code"] + "" + "' ) ,"
                + " Balance=(Select isnull(sum(Balance),0) from Accounts" + Firmcode + "  Where GroupCode='" + dt.Rows[vRow]["Code"] + "" + "') + (Select isnull(sum(Balance),0) from Groups" + Firmcode + " where Groupcode='" + dt.Rows[vRow]["Code"] + "" + "' )  where code='" + dt.Rows[vRow]["Code"] + "" + "' ";
                cmd.ExecuteNonQuery();
            }
        }
       
    }
}
